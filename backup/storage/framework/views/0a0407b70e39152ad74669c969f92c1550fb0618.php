

<?php $__env->startSection('content'); ?>
<?php if(!empty($getlists->clientreq_id)): ?>

<style>
.ih{background-color: #fff;
border: 1px solid #bdbdbd;
/* padding: 15px 0px; */
height: 34px;}
.ihi{margin: 5px 0px 27px 13px;}
endTime{
display: none;
}
</style>
<div class="container-fluid">
<div class="row">
<div class="main-header">
<h4>Postion Resume Upload</h4>
<ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
<li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
</li>
<li class="breadcrumb-item"><a href="#">Postion </a>
</li>
<li class="breadcrumb-item"><a href="">Postion List</a>
</li>
</ol>
</div>
</div>
<div class="row">
<!-- <?php echo e($sessionstart); ?> -->
<?php if(Session::has('success_msg')): ?>
<div class="alert alert-success">
<?php echo e(Session::get('success_msg')); ?>

</div>
<?php endif; ?>
<div class="col-md-12">
<div class="card">
<div class="card-header">
<h5 class="card-header-text full_widt">



<a href="#" class="start_align">
<div class="">
<input class="btn btn-danger col-md-12 col-sm-12 col-xs-12 endTime" id="endTime" name="endTime" type="button" value="stop" onclick="stop();">
</div>
</a>
<!-- <?php echo e($sessionstart); ?> -->
<?php if($sessionstart) { ?>
<!-- <a href="#">
<button type="button" id="showtimer"  class="star_tim">
<i class="icofont icofont-clock-time"></i>
<span id="time"></span>
</button>
</a> -->

<div class="form-group timer align_right" id="showtimer">
<span id="time"></span>
</div>
<?php } else {  ?>

<a href="#" class="start_align">
<div class="">
<input class="btn btn-success col-md-12 col-sm-12 col-xs-12" id="startTime" name="startTime" type="button" value="start" onclick="start();">
</div>
</a>

<div class="form-group timer align_right">
<span id="time"></span>
</div>
<?php } ?>
</h5>
</div>

<div>
 
<table class="table dt">
<thead>
<tr>
<th>Candidate Name</th>
<th>Candidate Email</th>
<th>Candidate Mobile</th>
<th>Resume</th>
</tr>
</thead>
<tbody>
<?php if(!empty($getuploadCv)): ?> 
<?php $__currentLoopData = $getuploadCv; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $uploads): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<tr class="table-active entry">
<td>
<div class="form-group required">
<?php echo e($uploads->candidate_name); ?>

</div>
</td>
<td>
<div class="form-group required">
<?php echo e($uploads->candidate_email); ?>

</div>
</td>
<td>
<label class="form-group">
<?php echo e($uploads->candidate_mob); ?>

</label>
</td>
<td>
<div class="form-group required">
<?php echo e($uploads->cv_name); ?>

</div>
</td>                                               

</tr>

<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?> 

</tbody>
</table>


</div>

<div class="card-block">
<div class="row">
<div class="col-sm-12 table-responsive">
<table class="table">
<thead>
<tr>
<th>Company Name </th>
<td><?php echo e(!empty($getlists->comp_name) ? $getlists->comp_name : 'N/A'); ?></td>
<th>Position Name</th>
<td style="border-bottom: 2px solid #eceeef;">
<span class="posname"><?php echo e(!empty($getlists->clientjob_title) ? $getlists->clientjob_title : 'N/A'); ?></span>
</td>
</tr>
<td colspan="2">
<div class="arro_css">
<?php echo e(!empty($getlists->upload_position_jd) ?  htmlspecialchars_decode($getlists->upload_position_jd)  : 'N/A'); ?>

</div>
</td>
</thead>

</table>

<!-- <div class="md-input-wrapper flo op timeset" style="display: none">

<button type="submit" class="btn btn-primary waves-effect waves-light">
<span class="icofont icofont-eye-alt"></span> Upload CV
</button>
</div> -->
</div>

</div>
<div class="col-sm-12 table-responsive timeset">
<?php echo Form::open([ 'action'=>'Emp\RecController@uploadcv', 'method'=>'post', 'files'=>true   ]); ?>




<div class="controls">

<table class="table dt">
<thead>
<tr>

<th>Name</th>
<th>Phone No</th>
<th>Email</th>
<th>Upload Resume</th>
<th></th>

</tr>
</thead>
<tbody>


<tr class="table-active entry">

<td>
<div class="form-group required">
<input type="text" name="candidate_name[]" class="form-control" placeholder="Enter First Name" pattern="[A-Za-z\s]+" required="" title="First Name should only contain  letters. e.g. John" maxlength="60">
</div>
</td>
<td>
<div class="form-group required">
<input type="tel" name="candidate_mob[]" class="form-control" placeholder="Enter Your Mobile" pattern="^\d{10}$" required="" title="Your Phone Number Should Only Contain  Letters. e.g. 9999999999 and 10 Digit Number" maxlength="10">
</div>
</td>
<td>
<div class="form-group required">
<input type="email" name="candidate_email[]" class="form-control" placeholder="Enter Your Email" required="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50">
</div>
</td>
<td>
<label class="custom-file ih">
<input type="file" id="file" class="ihi" name="cv_name[]" required>
</label>
</td>
<td><span class="add_field">
<button class="btn btn-success btn-add" type="button">
<span class="icofont icofont-plus"></span>
</button>
</span></td>
</tr>
</tbody>
</table>
<div class="md-input-wrapper dt">
<button type="submit" class="btn btn-default waves-effect">Cancel
</button>
<input type="hidden" name="fkjdid" value="<?php echo e(!empty($getlists->fk_jdid) ? $getlists->fk_jdid : '0'); ?>">
<input type="hidden" name="clientreqid" value="<?php echo e(!empty($getlists->clientreq_id) ? $getlists->clientreq_id : '0'); ?>">

<?php 
$clientEncryptId = Crypt::encrypt($getlists->clientreq_id); 
 ?>
<input type="hidden" name="clientreqEncrypt" value="<?php echo e(!empty($clientEncryptId) ? $clientEncryptId : '0'); ?>">

<input type="submit" class="btn btn-primary waves-effect waves-light" value="submit">

</div>
</div>
<?php echo Form::close(); ?>

</div>


</div>
</div>





</div>
</div>





<div class="row">

<div class="col-md-12">

<div class="card" >



<div class="card-header">
<h5 class="card-header-text">Client Preview</h5>

</div>
<div class="card-block">
<div class="row">
<div class="col-sm-12 table-responsive">
<table class="table">

<tr>
<th scope="row" style="width:23%;">Company Name</th>
<td><?php echo e(!empty($getlists->comp_name) ? $getlists->comp_name : 'N/A '); ?></td>
</tr>
<tr>
<th scope="row">Company URL</th>
<td><?php echo e(!empty($getlists->company_url) ? $getlists->company_url : 'N/A'); ?></td>
</tr>
<tr>
<th scope="row" >Company Size </th>
<td>+<?php echo e(!empty($getlists->company_size) ? $getlists->company_size : 'N/A'); ?></td>
</tr>

<tr>

<td colspan="2">

<div class="md-input-wrapper flo">

<button type="submit" class="btn btn-primary waves-effect waves-light"><span class="icofont icofont-eye-alt"></span> View More
</button>
</div>
</td>
</tr>
</table>
<table class="table hid" style="">
<tr>
<th scope="row" style="width:23%;">Timings</th>
<td><?php echo e(!empty($getlists->from_time) ? $getlists->from_time : 'N/A'); ?> To  <?php echo e(!empty($getlists->to_time) ? $getlists->to_time : 'N/A'); ?></td>
</tr>
<tr>
<th scope="row">Days Working</th>
<td><?php echo e(!empty($getlists->days_working) ? $getlists->days_working : 'N/A'); ?></td>
</tr>
<tr>
<th scope="row">Industry </th>
<td><?php echo e(!empty($getlists->industry) ? $getlists->industry : 'N/A'); ?></td>
</tr>
<tr>
<th scope="row">Company’s Specialties</th>
<td><?php echo !empty($getlists->selling_point) ? $getlists->selling_point : 'N/A'; ?></td>
</tr>

<tr>
<th scope="row">Headquarters</th>
<td><?php echo e(!empty($getlists->headquarters) ? $getlists->headquarters : 'N/A'); ?></td>
</tr>
<tr>
<th scope="row">Office Address</th>
<td><?php echo e(!empty($getlists->office_address) ? $getlists->office_address : 'N/A'); ?></td>
</tr>
</table>


</div>


</div>


</div>
</div>
</div>

</div>

</div>


<script>
function stopTime()
{
$('#showtimer').hide();
$('#stopTime').hide();
}
</script>


<script type="text/javascript">
$(function(){

$('#uploadcv').on('submit',function(e){
//alert('sjkdsjkdbsjkbd');
$('.endTime').css('display','block');
$('.timeset').css('display','block');
$.ajaxSetup({
header:$('meta[name="_token"]').attr('content')
})
e.preventDefault(e);

$.ajax({

type:"POST",
url:'/uploadcv',
data:$(this).serialize(),
dataType: 'json',
success: function(data){
// console.log(data);
// $("#suggestionbox").html(data);

},
error: function(data){

}
})
});

}); 
</script>
<script type="text/javascript">

var clsStopwatch = function () {

var startAt = 0;
var lapTime = 0;


var now = function () {
return (new Date()).getTime();
};

this.start = function () {
startAt = startAt ? startAt : now();
};

this.stop = function () {
lapTime = startAt ? lapTime + now() - startAt : lapTime;
startAt = 0;
};

this.time = function () {
return lapTime + (startAt ? now() - startAt : 0);
};
};

var x = new clsStopwatch();
var $time;
var clocktimer;

function pad(num, size) {
var s = "0000" + num;
return s.substr(s.length - size);
}

function formatTime(time) {
var h = m = s = ms = 0;
var newTime = '';

h = Math.floor(time / (3600 * 1000));
time = time % (3600 * 1000);
m = Math.floor(time / (60 * 1000));
time = time % (60 * 1000);
s = Math.floor(time / 1000);
ms = time % 1000;

newTime = pad(h, 2) + ':' + pad(m, 2) + ':' + pad(s, 2);
//newTime = pad(h, 2) + ':' + pad(m, 2) + ':' + pad(s, 2) + ':' + pad(ms, 2);
return newTime;
}

function show() {
$time = document.getElementById('time');
update();
}

function update() {
$time.innerHTML = formatTime(x.time());
}

function start() {
clocktimer = setInterval("update()", 1);
//alert(clocktimer);
x.start();
$('.timeset').css('display','block');
$('.posNavName').removeAttr("href");
alert('start uploading form');
$('#startTime').hide();
$('#endTime').show();
$('.align_right').show();

$.ajax({
type:"GET",
//  url:'http://127.0.0.1:8000/gettime/1', 
url:'gettime/<?php echo e($getlists->clientreq_id); ?>',
// http://127.0.0.1:8000/get-assigned-postion/gettime/1 
data:'',
// dataType: 'json',
success: function(data){
//location.reload(true);
},
error: function(data){

}
});
}

function millisecondsToHours(amountMS) {
return amountMS / 3600000;
}

function stop() {
x.stop();
alert('end uploading form');
$('#endTime').hide();
$.ajax({

type:"GET",
//  url:'http://127.0.0.1:8000/endtime/1',
url:'endtime/<?php echo e($getlists->clientreq_id); ?>',
data:'',
//  dataType: 'json',
success: function(data){
location.reload(true);
},
error: function(data){

}
});
document.getElementById('counter').value = millisecondsToHours(x.time());
clearInterval(clocktimer);

}

//Make timer element clickable so user can manually change timer
$('span#time').bind('dblclick', function() {
$(this).attr('contentEditable', true);
}).blur(
function() {
$(this).attr('contentEditable', false);
//Need to add ability here when user clicks off time to convert to millisecond
});


//plugin to make any element text editable
$.fn.extend({
editable: function () {
$(this).each(function () {
var $el = $(this),
$edittextbox = $('<input type="text"></input>').css('min-width', $el.width()),
submitChanges = function () {
if ($edittextbox.val() !== '') {
$el.html($edittextbox.val());
$el.show();
$el.trigger('editsubmit', [$el.html()]);
$(document).unbind('click', submitChanges);
$edittextbox.detach();
}
},
tempVal;
$edittextbox.click(function (event) {
event.stopPropagation();
});

$el.dblclick(function (e) {
tempVal = $el.html();
$edittextbox.val(tempVal).insertBefore(this)
.bind('keypress', function (e) {
var code = (e.keyCode ? e.keyCode : e.which);
if (code == 13) {
submitChanges();
}
}).select();
$el.hide();
$(document).click(submitChanges);
});
});
return this;
}
});
//implement editable plugin
/*$('span#time').editable().on('editsubmit', function (event, val) {
});*/

</script>
<script>
   localStorage.setItem('clocktime',(<?php echo e($sessionstart); ?>));
   var storageTime = localStorage.getItem('clocktime');
  // alert(storageTime);
</script>
<!-- <script>
function startTimer(duration, display) {
    var timer = duration, hour, minutes, seconds;
    setInterval(function () {
    	hour =    parseInt(timer / (60*60), 10);
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        hour = hour < 10 ? "0" + hour : hour;
        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = hour + ":" + minutes + ":" + seconds;
	    timer = duration;
       
    }, 1000);
}

window.onload = function () {
var fiveMinutes =JSON.parse(<?php echo e($sessionstart); ?>);
        display = document.querySelector('#time');
    startTimer(fiveMinutes, display);

    var a=$('#time').text();
    //alert(a);
    if(a=='')
    {
      $('.timeset').css('display','block');
      $('.posNavName').removeAttr("href");
    }
};
</script> -->
<?php else: ?>
<div class="container-fluid">
<div class="row">
<div class="main-header">
<h4>Postion Resume Upload</h4>
<ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
<li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
</li>
<li class="breadcrumb-item"><a href="#">Postion </a>
</li>
<li class="breadcrumb-item"><a href="">Postion List</a>
</li>
</ol>
</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="card">
No ID Found
</div>
</div>
</div>
</div>


<?php endif; ?>
<?php $__env->stopSection(); ?>


<!-- <script type="text/javascript">
    var auto_refresh = setInterval(
    function ()
    {
    var path =	"endtime/<?php echo e($getlists->clientreq_id); ?>"
    // alert(path);

    $('#time').load('path').fadeIn("slow");
    }, 1000); 
</script>  -->
<!--  setInterval(start, 5000); -->
<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>