$(document).ready(function() {
//waves effect js start
 Waves.init();
    Waves.attach('.flat-buttons', ['waves-button']);
    Waves.attach('.float-buttons', ['waves-button', 'waves-float']);
    Waves.attach('.float-button-light', ['waves-button', 'waves-float', 'waves-light']);
    Waves.attach('.flat-buttons',['waves-button', 'waves-float', 'waves-light','flat-buttons']);
//waves effect js end



	/* --------------------------------------------------------
       Color picker - demo only
       --------------------------------------------------------   */
   

      /*Gradient Color*/
      /*Normal Color */
      $(".color-1").on('click',function() {
          $("#color").attr("href", "assets/css/color/color-1.css");
          return false;
      });
      $(".color-2").on('click',function() {
          $("#color").attr("href", "assets/css/color/color-2.css");
          return false;
      });
      $(".color-3").on('click',function() {
          $("#color").attr("href", "assets/css/color/color-3.css");
          return false;
      });
      $(".color-4").on('click',function() {
          $("#color").attr("href", "assets/css/color/color-4.css");
          return false;
      });
      $(".color-5").on('click',function() {
          $("#color").attr("href", "assets/css/color/color-5.css");
          return false;
      });
        $(".color-inverse").on('click',function() {
            $("#color").attr("href", "assets/css/color/inverse.css");
            return false;
        });


      $('.color-picker').animate({
          right: '-239px'
      });

      $('.color-picker a.handle').click(function(e) {
          e.preventDefault();
          var div = $('.color-picker');
          if (div.css('right') === '-239px') {
              $('.color-picker').animate({
                  right: '0px'
              });
          } else {
              $('.color-picker').animate({
                  right: '-239px'
              });
          }
      });
});
