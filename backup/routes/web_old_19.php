<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', ['as' => 'home', 'uses'=> 'HomeController@index']);
// Route::get('dashboard','Emp\DashController@index');
Route::get('dashboard', ['as' => 'dashboard', 'uses'=> 'Emp\DashController@index']);
Route::get('newclient',['as'=>'newclient','uses'=>'Emp\DashController@newclient']);
Route::post('newclient', ['as' => 'saveclient', 'uses' => 'Emp\DashController@saveclient']);
Route::get('listclient',['as'=>'listclient','uses'=>'Emp\DashController@listclient']);
Route::post ('assignspoc',['as'=>'assignspoc','uses'=>'Emp\DashController@assignspoc']);
//Route::post ('assignspoc',['as'=>'assignspoc','uses'=>'Emp\DashController@assignspoc']);


Route::get ('editclient/{id}',['as'=>'editclient','uses'=>'Emp\DashController@editclient']);
Route::post ('editclient/{id}',['as'=>'editclient','uses'=>'Emp\DashController@updateClient']);



Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('clientspoc',['as'=>'clientspoc','uses'=>'Emp\DashController@clientspoc']);
Route::get('positionform', ['as' => 'positionform','uses'=>'Emp\PositionController@position']);
Route::post('insertposition', ['as' => 'insertposition', 'uses' => 'Emp\PositionController@save']);

Route::post('addposition',['as'=>'addposition','uses'=>'Emp\PositionController@addposition']);
Route::get('positionlist',['as'=>'positionlist','uses'=>'Emp\PositionController@positionlist']);
Route::get('positionjd',['as'=>'positionjd','uses'=>'Emp\PositionController@positionjd']);


Route::get('client-excel',['as'=>'client-excel','uses'=>'Emp\EmployeeXLController@index']);
Route::post('exceldown',['uses'=>'Emp\EmployeeXLController@exceldown'])->name('exceldown');
Route::post('excelup',['uses'=>'Emp\EmployeeXLController@excelup'])->name('excelup');
Route::get('shortlistcv',['as'=>'shortlistcv','uses'=>'Emp\CVController@shortlistcv']);
Route::post('savecandidate',['as'=>'savecandidate','uses'=>'Emp\CVController@savecandidate']);
Route::get('getcandidatedetails/{id}',['as'=>'getcandidatedetails','uses'=>'Emp\CVController@getcandidatedetails']);
//Route::get('listclient',['as'=>'listclient','uses'=>'Emp\DashController@listclient']);
Route::get ('editclient/{id}',['as'=>'editclient','uses'=>'Emp\DashController@editclient']);
Route::post ('editclient/{id}',['as'=>'editclient','uses'=>'Emp\DashController@updateClient']);

Route::get('vacancy',['as'=>'vacancy','uses'=>'Emp\VacancyController@vacancy']);
Route::get('candidate-list',['as'=>'candidate-list','uses'=>'Emp\VacancyController@candidateList']);

Route::get('viewallassignedrecuiterposition',['as'=>'viewallassignedrecuiterposition','uses'=>'Emp\RecController@viewallassignedrecuiterposition']);
Route::get('assignclient',['as'=>'assignclient','uses'=>'Emp\PositionController@assignclient']);

// Route::get('get-assigned-postion', ['as' => 'assignedposition', 'uses'=>'Emp\RecController@getassignedposition']);
// Route::post('uploadcv', ['as' => 'uploadcv', 'uses'=>'Emp\RecController@uploadcv']);
// Route::get('gettime/{id}', ['as' => 'gettime', 'uses'=>'Emp\RecController@gettime']);
//Route::get('endtime/{id}', ['as' => 'endtime', 'uses'=>'Emp\RecController@endtime']);
Route::post('savecandidate',['as'=>'savecandidate','uses'=>'Emp\CVController@savecandidate']);

Route::get('getspoctoclient/{id}',['as'=>'getspoctoclient','uses'=>'Emp\DashController@getspoctoclient']);

//         Route::get('get-assigned-postion/{id}', ['as' => 'assignedposition', 'uses'=>'Emp\RecController@getassignedposition']);
//   Route::post('uploadcv', ['as' => 'uploadcv', 'uses'=>'Emp\RecController@uploadcv']);
//     Route::get('gettime/{id}', ['as' => 'gettime', 'uses'=>'Emp\RecController@gettime']);
// Route::get('endtime/{id}', ['as' => 'endtime', 'uses'=>'Emp\RecController@endtime']);

Route::get('get-assigned-postion/{id}', ['as' => 'assignedposition', 'uses'=>'Emp\RecController@getassignedposition']);

Route::get('get-assigned-postion', ['as' => 'assignedposition', 'uses'=>'Emp\RecController@getassignedposition']);

Route::post('get-assigned-postion/uploadcv', ['as' => 'uploadcv', 'uses'=>'Emp\RecController@uploadcv']);
Route::get('get-assigned-postion/gettime/{id}', ['as' => 'gettime', 'uses'=>'Emp\RecController@gettime']);
Route::get('get-assigned-postion/endtime/{id}', ['as' => 'endtime', 'uses'=>'Emp\RecController@endtime']);
Route::post('savecandidate',['as'=>'savecandidate','uses'=>'Emp\CVController@savecandidate']);

Route::get('getemployeebyclientid/{id}',['as'=>'getemployeebyclientid','uses'=>'Emp\DashController@getemployeebyclientid']);



Route::get('report',['as'=>'report','uses'=>'Admin\ReportController@report']);















/*
|--------------------------------------------------------------------------
| admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('admin')->group(function () {
    //Route::get('/', 'Admin\AdminController@index')->name('admin.dashboard');
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/logout','Auth\AdminLoginController@logout')->name('admin.logout');


    Route::get('dashboard','Admin\AdminController@index')->name('admin.dashboard');
    Route::get('admindashboard', 'Admin\AdminController@index')->name('admin.admindashboard');
    Route::get('adminrole', 'Admin\AdminController@adminrole')->name('admin.adminrole');
    Route::get('viewadminrole', 'Admin\AdminController@viewadminrole')->name('admin.viewadminrole');
    Route::post('addadminrole', 'Admin\AdminController@addadminrole')->name('admin.addadminrole');
    Route::get('admindepartment', 'Admin\AdminController@admindepartment')->name('admin.admindepartment');
    Route::get('viewadmindepartment', 'Admin\AdminController@viewadmindepartment')->name('admin.viewadmindepartment');
    Route::post('addadmindepartment', 'Admin\AdminController@addadmindepartment')->name('admin.addadmindepartment');
    Route::get('adminclient', 'Admin\AdminController@adminclient')->name('admin.adminclient');
    Route::get('viewadminemployes', 'Admin\AdminController@adminemployes')->name('admin.viewadminemployes');
    Route::get('employee', 'Admin\AdminController@employee')->name('admin.employee');
    Route::post('usersave', 'Admin\AdminController@addadminusers')->name('admin.usersave');
    // Route::get('editadminemployee/{id}',['as'=>'editadminemployee', 'uses'=>'Admin\AdminController@adminemployes']);
    Route::get('/editadminemployes/{id}', 'Admin\AdminController@editadminemployes')->name('admin.editadminemployes');
    Route::post('/updateadminemployes', 'Admin\AdminController@updateadminemployes')->name('admin.updateadminemployes');
    Route::get('/deleteadminemployes/{id}', 'Admin\AdminController@deleteadminemployes')->name('admin.deleteadminemployes');
    Route::get('/adminchangepassword', 'Admin\AdminController@changepassword')->name('admin.adminchangepassword');
    Route::post('/adupchangepassword', 'Admin\AdminController@adupchangepassword')->name('admin.adupchangepassword');
    Route::post('checkemail',['as'=>'checkemail','uses'=>'Admin\AdminController@checkemail']);





 
   Route::get('exceldownload',['as'=>'empexcel','uses'=>'Admin\EmployeeController@exceldownload']);
    Route::get('teamassign',['as'=>'teamassign','uses'=>'Admin\TeamController@index']);
    Route::get('allocateposition', ['as' => 'allocateposition', 'uses'=>'Admin\RecruiterController@getlist']);


    Route::get('getemployees',['as'=>'getemployees', 'uses'=>'Admin\RecruiterController@getEmpList']);

    Route::post('allocateposition', ['as' => 'allocatepositionpost', 'uses'=>'Admin\RecruiterController@postRecruiter']);

    Route::get('autocomplete', ['as' => 'autocomplete', 'uses'=>'Admin\RecruiterController@autocomplete']);
    Route::get('/team',['as'=>'team','uses'=>'Admin\TeamController@team']);
    Route::post('/team',['as'=>'team','uses'=>'Admin\TeamController@teamSave']);
    Route::get('editteam/{id}',['as'=>'editteam','uses'=>'Admin\TeamController@editteam']);
Route::post('editteam/{id}',['as'=>'editteam','uses'=>'Admin\TeamController@teamUpdate']);
Route::get('deleteteam/{id}',['as'=>'deleteteam','uses'=>'Admin\TeamController@deleteteam']);

Route::get('/teamuniquetitle',['as'=>'teamuniquetitle','uses'=>'Admin\TeamController@uniqueteam']);




    Route::get('empexcel',['as'=>'empexcel','uses'=>'Admin\EmployeeController@empexcel']);
    Route::post('excelup',['as'=>'excelup','uses'=>'Admin\EmployeeController@excelup']);
    Route::get('exceldown',['as'=>'exceldown','uses'=>'Admin\EmployeeController@exceldown']);

    Route::get('addmenu',['as'=>'addmenu','uses'=>'Admin\MenuController@addmenu']);
    Route::post('savemenu',['as'=>'savemenu','uses'=>'Admin\MenuController@savemenu']);
    Route::get('assignmenu',['as'=>'assignmenu','uses'=>'Admin\MenuController@assignmenu']);
    Route::post('getmenubyrole',['uses'=>'Admin\MenuController@getmenubyrole']);
    Route::get('getmenubyrole/{id}',array('as' => 'getmenubyrole', function($id) { return View::make('admin.assignmenu')->with('acccess', $id); }));

    Route::get('childmenu/{id}',['as'=>'childmenu','uses'=>'Admin\MenuController@getchildmenu']);
    Route::get('child',['as'=>'child','uses'=>'Admin\MenuController@getchildmenu']);
    Route::post('assignmbyrole',['as'=>'assignmbyrole','uses'=>'Admin\MenuController@assignmbyrole']);
    Route::get('viewassignedrecuiterposition',['as'=>'viewassignedrecuiterposition','uses'=>'Admin\RecruiterController@viewassignedrecuiterposition']);


    //  Route::get('get-assigned-postion', ['as' => 'assignedposition', 'uses'=>'Admin\RecruiterController@getassignedposition']);
    //  Route::post('uploadcv', ['as' => 'uploadcv', 'uses'=>'Admin\RecruiterController@uploadcv']);
    // Route::get('gettime/{id}', ['as' => 'gettime', 'uses'=>'Admin\RecruiterController@gettime']);






});