<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Model\User;
use App\Model\Role;
use App\Model\Department;
use Session;

class EmployeeController extends Controller
{
    //
    public function employee(){
    	$roles=Role::all();
    	$departments=Department::all();
    	//dd($roles);
        return view('Emp.newemp',compact('roles','departments'));
    }

    public function save(Request $request)
    {
    	//echo 'hi'; die;

        $this->validate($request,[
            'name' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'cpassword' => 'required|same:password',
            'doj' => 'required',
            'dept' => 'required',
            'role' => 'required'
        ],[
            'name.required' => 'Name field is required.',
            'phone.required' => 'Phone number field is required.|Numbers Only',
            'email.required' => 'Email field is required',
            'password.required' => 'Password field is required',
            'cpassword.required' => 'Confirm Password field is required',
//            'phone.max'=>'Phone number field must be of 10 digit only',
            'doj.required' => ' Date of joining field is required',
            'dept.required' => ' Department is required',
            'role.required' => ' Role is required'
        ]);



        $postData = $request->all();
        $role = implode(',', $request['role']);
        $pass = $request['password'];
        $cpass = $request['cpassword'];
        $user_obj= new User;
        $user_obj->name=$request['name'];
        $user_obj->emp_contactno  = $request['phone'];
        $user_obj->email=$request['email'];
        $user_obj->password=$request['password'];
        $user_obj->emp_doj=$request['doj'];
        $user_obj->emp_dept =$request['dept'];
        $user_obj->emp_role=$role;
        //print_r($user_obj); //die;
        //$user_obj->save();
       Session::flash('success_msg', 'Employee Added Successfully!');

        return redirect()->route('employee');

    }

    public function empexcel()
    {
        return view('admin/empexcel');
    }
}
