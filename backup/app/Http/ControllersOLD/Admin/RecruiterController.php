<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Adminmodel\Position;
use App\Model\Adminmodel\Employees;
use App\Model\Adminmodel\RecruiterMap;
use Carbon\Carbon;
use Session;
use DB;

class RecruiterController extends Controller
{
    
    /**To get all list of positions */
    public function getlist() 
    {   
        
        $employees = Employees::all();
        $positions = DB::table('tbl_clientjd_master')
                    ->join('tbl_clients', 'tbl_clientjd_master.clientjob_compid', '=', 'tbl_clients.client_id')
                    ->join('users', 'tbl_clientjd_master.clientjob_empid', '=', 'users.id')
                    ->get();

        return view('admin.positionallocate', [
            'positions'=>$positions,
            'employees'=>$employees
        ]);
    }

    public function autocomplete(Request $request)
    {
        $data = Employees::select("name")->where("name","LIKE","%{$request->input('query')}%")->get();
        return response()->json($data);
    }


    /**To assign recruiter */
    public function postRecruiter(Request $request)
    {

    $clientJobId = $request->input('clientjobid'); 
    foreach ($request['assignposition'] as $key=>$value)
    {
        DB::table('tbl_clientjdrecruitermap')->insert([
            'fk_jdid'=>$clientJobId,
            'fk_empid'=>$value,
            'clientreq_createdon'=>Carbon::now(),
            'clientreq_updatedon'=>Carbon::now()
        ]);

    }   
    Session::flash('success_msg', 'Recuiter Allocated Successfully!');
    return redirect()->route('allocateposition');
       
    }


    /**Get all assined recruiters on given position */
    public function getassignedposition() 
    {

        return view('admin.allpositionassigned');
    }

}
