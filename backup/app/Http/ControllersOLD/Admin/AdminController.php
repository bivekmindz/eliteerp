<?php 

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Model\Client as Client;
use App\Model\Adminmodel\Role as Role;
use App\Model\Adminmodel\Department as Department;
use App\Model\Adminmodel\Employees as Employees;
use Illuminate\Support\Facades\Auth;
use App\Model\User;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;

 
use Illuminate\Http\Request;
 
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $redirectTo = '/admin/dashboard';
    protected $redirectAfterLogout = '/admin/login';

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
 
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd(session(['adminsess'=>'abcd']));


        return view('admin.adashboard');
    }

     public function adminclient()
    {
        $clients=Client::orderBy('created_at','desc')->paginate(500);
        return view('admin.adminclient',compact('clients'))
            ->with('i', (request()->input('page', 1) - 1) * 500);

        //  return view('admin.adminselectrole');
    }
    //View Role
    public function adminrole()
    {
        $roles=Role::orderBy('role_name','asc')->paginate(500);
        return view('admin.adminselectrole',compact('roles'))
            ->with('i', (request()->input('page', 1) - 1) * 500);

      //  return view('admin.adminselectrole');
    }

    //Add Role
      public function viewadminrole()
    {
        return view('admin.viewrole');
    }
    //Add Role Table
    public function addadminrole(Request $request)
    {
        $this->validate($request,[
            'role_name' => 'required',
        ],[
            'role_name.required' => 'Name field is required.',
        ]);

        $postData = $request->all();
        $client_obj= new Role;
        $client_obj->role_name=$request['role_name'];
        $client_obj->role_status = 1;
       // //  dd($client_obj);
        $client_obj->save();

        Session::flash('success_msg', 'Role Added Successfully!');

        return redirect()->route('admin.viewadminrole');
    }
//View Role
    public function admindepartment()
    {
        $departments=Department::orderBy('dept_name','asc')->paginate(50);
        return view('admin.adminselectdepartment',compact('departments'))
            ->with('i', (request()->input('page', 1) - 1) * 50);

        //  return view('admin.adminselectrole');
    }
//View Department
    public function viewadmindepartment()
    {
        return view('admin.viewdepartment');
    }
//Add Department
    public function addadmindepartment(Request $request)
    {
        $this->validate($request,[
            'department_name' => 'required',
        ],[
            'department_name.required' => 'Department Name field is required.',
        ]);

        $postData = $request->all();
        $client_obj= new Department;
        $client_obj->dept_name=$request['department_name'];
        $client_obj->dept_status = 1;
        // //  dd($client_obj);
        $client_obj->save();

        Session::flash('success_msg', 'Department Added Successfully!');

        return redirect()->route('admin.viewadmindepartment');
    }

    //View Employees
    public function adminemployes()
    {
       // dd('gdfgdfg');

        $employees=Employees::orderBy('name','desc')->paginate(50);
        return view('admin.adminselectemployes',compact('employees'))
            ->with('i', (request()->input('page', 1) - 1) * 50);

        //  return view('admin.adminselectrole');
    }

     //
    public function employee(){
        $roles=Role::all();
        $departments=Department::all();
        //dd($roles);

        return view('admin.newemp',compact('roles','departments'));
    }

    //Add User
    public function addadminusers(Request $request)
    {
       

        $this->validate($request,[
            'name' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'cpassword' => 'required|same:password',
            'doj' => 'required',
            'dept' => 'required',
            'role' => 'required'
        ],[
            'name.required' => 'Name field is required.',
            'phone.required' => 'Phone number field is required.|Numbers Only',
            'email.required' => 'Email field is required',
            'password.required' => 'Password field is required',
            'cpassword.required' => 'Confirm Password field is required',
//            'phone.max'=>'Phone number field must be of 10 digit only',
            'doj.required' => ' Date of joining field is required',
            'dept.required' => ' Department is required',
            'role.required' => ' Role is required'
        ]);


        $postData = $request->all();
        $role = implode(',', $request['role']);
        $pass = $request['password'];
        $cpass = $request['cpassword'];
        $user_obj= new User;
        $user_obj->name=$request['name'];
        $user_obj->emp_contactno  = $request['phone'];
        $user_obj->email=$request['email'];
        $user_obj->password=bcrypt($request['password']);
        $user_obj->emp_doj=$request['doj'];
        $user_obj->emp_dept =$request['dept'];
        $user_obj->emp_role=$role;
       // print_r($user_obj); die;
        $user_obj->save();

       Session::flash('success_msg', 'Employee Added Successfully!');

        return redirect()->route('admin.viewadminemployes');
    }

    public function deleteadminemployes()
    {
        dd('gdfgdf');
    }

}
