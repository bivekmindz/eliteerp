<?php
/**
 * Created by PhpStorm.
 * User: mindz
 * Date: 29/1/18
 * Time: 2:05 PM
 */

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Model\Adminmodel\Menu;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Model\Adminmodel\Role as Roles;

class MenuController extends Controller
{
    public function addmenu()
    {
        return view('admin/addmenu');

    }

    public function savemenu(Request $request)
    {
        $this->validate($request,[
            'parentmenu' => 'required',
        ],[
            'parentmenu.required' => 'Parent menu field is required.',
        ]);

        $parentid=DB::table('tbl_menu')->insertGetId(
            [
                'menuname'=>$request['parentmenu'],

            ]);
        $child=$request['childmenu'];
        foreach ($child as $key => $value)
        {
            DB::table('tbl_menu')
                ->insert(
                    ['menuname'=>$value,
                        'menuparentid'=>$parentid
                    ]);
        }

        Session::flash('success_msg','Menu created successfully');
        return redirect()->route('addmenu');


    }

    public function assignmenu()
    {
        $role=Roles::all();

        $parents=DB::table('tbl_menu as m1')
            ->join('tbl_menu as m2')
            ->select('m1.menuname','m1.id')
            ->where(['menuparentid'=>0])->get();

        dd($parents);

        return view('admin.assignmenu',compact('role'));

    }

}