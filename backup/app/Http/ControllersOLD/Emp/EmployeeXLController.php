<?php

namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use PHPExcel_IOFactory;
//use PHPExcel_Settings;
use App\Exceptions\ApplicationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use App\Model\User;
use App\Model\Role;
use App\Model\Client;
use App\Model\Adminmodel\Department as Dept;
use Session;
use DB;

//require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;



class EmployeeXLController extends Controller
{
    public function index()
    {

        $dept = Dept::all();
        return view('Emp.empXl',compact('dept'));
    }
    //
    public function exceldown()
    {
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('Maarten Balliauw')
            ->setLastModifiedBy('Maarten Balliauw')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Company Name')
            ->setCellValue('B1', 'Contact Person')
            ->setCellValue('C1', 'Contact Number')
            ->setCellValue('D1', 'Email')
            ->setCellValue('E1', 'Designation');



        $spreadsheet->getActiveSheet()->setTitle('Client Excel');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="01simple.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');
        exit;


    }

    public function excelup(Request $request)
    {
        $assigneeid=Auth::user()->id;
        $inputFileName =$request['upfile'];
        //dd( $inputFileName);
        $helper = new Sample();
        //dd($helper);
        $helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
        $spreadsheet = IOFactory::load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $excel_row=array_shift($sheetData);
       // dd($sheetData);
       
       foreach ($sheetData as $key=>$value)
        {
         DB::table('tbl_clients')
        ->insert([
           'comp_name'=>$value['A'],
           'contact_name'=>$value['B'],
           'phone'=>$value['C'],
           'email'=>$value['D'],
          'designation'=>$value['E'],
          'clientsuid'=>$assigneeid,
          'client_catid'=>$request['catid']
           ]);
        }
        // $client=new Client();

        // foreach ($sheetData as $key=>$value)
        // {

        //     $client->comp_name=$value['A'];
        //     $client->contact_name=$value['B'];
        //     $client->phone=$value['C'];
        //     $client->email=$value['D'];
        //     $client->designation=$value['E'];
        //     $client->save();

        // }

        Session::flash('success_msg', 'Excel Uploaded  Successfully!');

        return redirect()->route('client-excel');
        //dd('fgfdg');


    }
//




    public function employeexl(){

        return view('Emp.empXl');
    }



    public function empxlupload( Request $request ){

        $master_array_key = [
            'seller_id' => null,
            'rate_card_title' => 'required',
            'valid_from' => 'required',
            'valid_to' => 'required',
            'terms_condition' => null,
            'post_type' => null,
            'overwrite' => null,
            'user_time_zone' => null
        ];


        if (!$request->hasFile('uploadFile')) {

            //   throw new ApplicationException([], ["uploadFile needs to be specified"]);
        }

        //$path = $request->file;

        $path = $request->file('file');
        //Load Excel
        //$objPHPExcel = PHPExcel_IOFactory::load($file);


        PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);

        $objPHPExcel = PHPExcel_IOFactory::load($path);

        $masterSheet = $objPHPExcel->getSheet(0);
        //    echo "<pree>";
        //print_r($masterSheet);
        //echo "</pre>";
        $topRow = $masterSheet->getHighestRow();
        $topColumn = $masterSheet->getHighestColumn();
        $master = [];
        for ($row = 1; $row <= $topRow; ++$row) {
            $masterRows = $masterSheet->rangeToArray('A' . $row . ':' . $topColumn . $row, NULL, TRUE, FALSE);

            array_push($master, $masterRows[0]);
        }


        foreach ($master as $key => $value) {

            $client = new Client();
            $client->comp_name=$value[1];
            $client->contact_name=$value[2];
//$client->phone=$value[4];
            $client->email=$value[5];
            $client->save();
        }

        return redirect("employeexl")->with("status","File Upload Successfully.");
    }


    public function userloyeexl(){
        return view('Emp.userXl');
    }

    public function userxlupload( Request $request ){

        $path = $request->file('file');
        PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
        $objPHPExcel = PHPExcel_IOFactory::load($path);
        $masterSheet = $objPHPExcel->getSheet(0);
        $topRow = $masterSheet->getHighestRow();
        $topColumn = $masterSheet->getHighestColumn();
        $master = [];
        for ($row = 1; $row <= $topRow; ++$row) {
            $masterRows = $masterSheet->rangeToArray('A' . $row . ':' . $topColumn . $row, NULL, TRUE, FALSE);

            array_push($master, $masterRows[0]);
        }
        foreach ($master as $key => $value) {
            $user = new User();
            $user->name=$value[0];
// $user->emp_contactno=$value[4];
            $user->email = $value[3];
//$user->password=$value[5];
            $user->emp_doj=date('Y-m-d',strtotime($value[2]));
//$user->emp_dept=$value[1];
            $user->emp_role=$value[5];
            $user->save();
        }
        return redirect("employeexl")->with("status","File Upload Successfully.");

    }






    public function save(Request $request)
    {
        //echo 'hi'; die;

//         $this->validate($request,[
//             'cname' => 'required',
//             'cpn' => 'required',
//             'email' => 'required|email|unique:users',
//             'phone' => 'required|numeric',
//             'deptid' => 'required',
//             'desig' => 'required'
//         ],[
//             'cname.required' => 'Company name field is required.',
//             'cpn.required' => 'Contact person name is required',
//             'email.required' => 'Email field is required',
//             'phone.required' => 'Phone number field is required.',
// //            'phone.max'=>'Phone number field must be of 10 digit only',
//             'desig.required' => ' Designation field is required',
//             'deptid.required' => ' Select Category',
//         ]);



        $postData = $request->all();
        print_r($postData); die;
        $client_obj= new Client;
        $client_obj->client_catid=$request['deptid'];
        $client_obj->comp_name = $request['cname'];
        $client_obj->phone=$request['phone'];
        $client_obj->contact_name=$request['cpn'];
        $client_obj->email=$request['email'];
        $client_obj->designation=$request['desig'];
        //  dd($client_obj);
        $client_obj->save();

        Session::flash('success_msg', 'Client Added Successfully!');

        return redirect()->route('newclient');

    }





}
