<?php

namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Model\Adminmodel\Position;
use App\Model\Adminmodel\Department;
use App\Model\Client as Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Session;
use Log;
use Storage;

class PositionController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        //$assigneeid=Auth::user()->id;
    }

    public function position()
    {
        $assigneeid=Auth::user()->id;
        $departments=Department::all();
        $client=DB::table('tbl_clientspockmap as c')
            ->join('tbl_clients as t','c.fk_clientid','=','t.client_id')
            ->select('c.fk_clientid','t.comp_name')
            ->where(['c.assignee_emp_id'=>$assigneeid])
            ->distinct('c.fk_clientid')
            ->get();
        $clients=Client::all();
        //dd($client);

        $emp=DB::table('users')
            ->select('name','id')
            ->whereRaw('FIND_IN_SET(3,emp_role)','emp_role')
            ->get();

        $target=DB::table('tbl_clientjdrecruitermap as c')
            ->join('users as u','u.id','=','c.fk_empid')
            ->select('u.name')
            ->where(['c.fk_assigneeid'=>$assigneeid])
            ->distinct('c.fk_assigneeid')
            ->get();
       //dd($target);
       // dd($emp);
        return view('Emp.position',compact('departments','client','emp','target','clients'));
    }

    public function addposition(Request $request)
    {

        /**JD upload starts*/
        $file = $request->file('upload_position_jd');
        $date = new \DateTime();
        
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $dirStorage = DIRECTORY_SEPARATOR . $year . DIRECTORY_SEPARATOR 
                        . $month . DIRECTORY_SEPARATOR . $day;
        
        Storage::disk('local')->makeDirectory($dirStorage);
        $fileName = uniqid() . "-" . str_replace(" ", "-", $file->getClientOriginalName());
        $filepath = $dirStorage . DIRECTORY_SEPARATOR . $fileName;
        Storage::disk('local')->put($filepath,file_get_contents($file->getRealPath()));
        /**JD upload starts*/


        $this->validate($request,[
            'dept' => 'required',
            'compname'=>'required',
            'jobtitle' => 'required',
            'otherinfo' => 'required',
            'upload_position_jd' => 'required',
            'noofpos' => 'required|numeric',
            
        ], 
        [
            'dept.required' => 'Department field is required.',
            'compname.required'=>'company name field is required list',
            'jobtitle.required' => 'Jobtitle field is required.',
            'otherinfo.required' => 'Job Posting Closing Date field is required',
            'upload_position_jd' => 'Please Upload Job Decription',
            'noofpos.required' => 'Number of Positions field is required'
        ]);
        $assigneeid=Auth::user()->id;
        DB::table('tbl_clientjd_master')->insert([
            'clientjob_deptid'=>$request['dept'],
            'clientjob_compid'=>$request['clientname'],
            'clientjob_title'  => $request['jobtitle'],
            'upload_position_jd' => $fileName,
            'clientjob_noofposition'=>$request['noofpos'],
            'drive'=>1,
            'clientjob_empid'=>$assigneeid,
            'clientjob_otherinfo'=>$request['otherinfo'],
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()

        ]);

        Session::flash('success_msg', 'Position Added Successfully!');

        return redirect()->route('positionform');

    }


    public function positionlist()
    {
        $assigneeid=Auth::user()->id;
        $clients=DB::table('tbl_clientjd_master as c')
        ->join('tbl_clients as cs','c.clientjob_compid','=','cs.client_id')
        ->select('c.*')
        ->where(['c.clientjob_empid'=>$assigneeid])
        ->get();
        
        
        //dd($pos);
        return view('Emp.listposition',
            [
            'clients'=>$clients
            ]);
    }


}
