<?php

namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use PHPExcel_IOFactory;
//use PHPExcel_Settings;
use App\Exceptions\ApplicationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use App\Model\User;
use App\Model\Role;
use App\Model\Client;
use App\Model\Adminmodel\Department as Dept;
use Illuminate\Support\Facades\Crypt;
use Session;
use DB;
use File;
//require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;



class ClientXLController extends Controller
{
    public function index()
    {

        $dept = Dept::all();
        return view('Emp.empXl',compact('dept'));
    }

    
    //
    public function exceldown()
    {


        
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('Maarten Balliauw')
            ->setLastModifiedBy('Maarten Balliauw')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Company Name')
            ->setCellValue('B1', 'Contact Person')
            ->setCellValue('C1', 'Contact Number')
            ->setCellValue('D1', 'Email')
            ->setCellValue('E1', 'Designation')
            ->setCellValue('F1', 'Company URL')
            ->setCellValue('G1', 'Company Size')
            ->setCellValue('H1', 'Timings From')
            ->setCellValue('I1', 'Timings To')
            ->setCellValue('J1', 'Days Working')
            ->setCellValue('K1', 'Industry')
            ->setCellValue('L1', 'Company’s Specialties')
            ->setCellValue('M1', 'Headquarters')
            ->setCellValue('N1', 'Office Address');
            
             for($k=2;$k <1000;$k++) {
            $spreadsheet->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
            $validation = $spreadsheet->getActiveSheet()->getCell('J2')->getDataValidation();
            $validation->setType(DataValidation::TYPE_WHOLE);
            $validation->setErrorStyle(DataValidation::STYLE_STOP);
            $validation->setAllowBlank(true);
            $validation->setShowInputMessage(true);
            $validation->setShowErrorMessage(true);
            $validation->setErrorTitle('Input error');
            $validation->setError('Only numbers between 1 and 7 are allowed!');
            $validation->setPromptTitle('Allowed input');
            $validation->setPrompt('Only numbers between 1 and 7 are allowed.');
            $validation->setFormula1(1);
            $validation->setFormula2(7);
            $spreadsheet->getActiveSheet()->getCell('J'.$k)->setDataValidation($validation);
        }




        $spreadsheet->getActiveSheet()->setTitle('Client Excel');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="ClientExcel.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');
        exit;


    }



    public function excelup(Request $request)
    {
       
        if(!isset($request['upfile']))
        {
            Session::flash('error_msg', 'Please select excel file to upload.');
            return redirect()->route('client-excel');
        }
        
        if(isset($request['upfile']))
        {
           $extension = File::extension($request['upfile']->getClientOriginalName());
           if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
              //'Your file is a valid xls or csv file'
           }else {
              Session::flash('error_msg', 'Please select excel file (xlsx,xls,csv) to upload.');
              return redirect()->route('client-excel');
           }
        }

        $assigneeid = Auth::user()->id;
        $inputFileName = $request['upfile'];
        //dd( $inputFileName);
        $helper = new Sample();
        //dd($helper);
       // $helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
        $spreadsheet = IOFactory::load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $excel_row = array_shift($sheetData);


        foreach ($sheetData as $key => $value)
        {
            //dd($value);
            $clients = DB::table('tbl_clients')
                        ->select('client_id')
                        ->where('email',$value['D'])
                        ->get();

            if($clients->isEmpty()){
                DB::table('tbl_clients')
                    ->insert([
                        'comp_name' => $value['A'],
                        'contact_name' => $value['B'],
                        'phone' => $value['C'],
                        'email' => $value['D'],
                        'designation' => $value['E'],
                        'company_url' => $value['F'],
                        'company_size' => $value['G'],
                        'from_time' => $value['H'],
                        'to_time' => $value['I'],
                        'days_working' => $value['J'],
                        'industry' => $value['K'],
                        'company_specialties' => $value['L'],
                        'headquarters' => $value['M'],
                        'office_address' => $value['N'],
                        'clientsuid' => $assigneeid,
                        'client_catid' => $request['catid'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'client_catid' => $request['catid'],
                ]);
            }
        }
        // $client=new Client();

        // foreach ($sheetData as $key=>$value)
        // {

        //     $client->comp_name=$value['A'];
        //     $client->contact_name=$value['B'];
        //     $client->phone=$value['C'];
        //     $client->email=$value['D'];
        //     $client->designation=$value['E'];
        //     $client->save();

        // }

        Session::flash('success_msg', 'Excel Uploaded  Successfully!');
        return redirect()->route('client-excel');
    }


    public function editclient($client_id)
    {
//
        $clientid = Crypt::decrypt($client_id);
        $client_details = DB::table('tbl_clients')
            ->select('*')
            ->where('client_id',$clientid)
            ->get();

        return view('Emp.editclient',compact('client_details'));
    }


    public function updateClient($client_id,Request $request)
    {
       // dd($request);
        $this->validate($request,[
            'comp_name' => 'required',
            'company_url' => 'required',
            'email' => 'required|email|unique:users',
            'contact_name' => 'required',
            'designation' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'company_size' => 'required',
            'from_timings' => 'required',
            'to_timings'=>'required',
            'days_working' => 'required',
            'company_size' => 'required',
            'industry' => 'required',
            'headquarters' => 'required',
            'company_specialties' => 'required',
            'office_address' => 'required',
            'selling_point' => 'required',
        ],[
            'comp_name.required' => 'Company name field is required.',
            'company_url.required' => 'Company Url is required.',
            'email.required' => 'Email field is required.',
            'contact_name.required' => 'Contact number field is required.',
            'designation.required' => 'Designation field is required.',
            'phone.required' => 'Phone number field is required.',
            'phone.max'=>'Phone number field must be of 10 digit only.',
            'company_size.required' => 'Company Size is required.',
            'from_timings.required' => 'From Timings is required.',
            'to_timings.required' => 'To Timings is required.',
            'days_working.required' => 'Days of working field is required',
            'industry.required' => 'Industry field is required',
            'headquarters.required' => 'Headquarters field is required',
            'company_specialties.required' => 'Company Specialties field is required',
            'office_address.required' => 'Office address field is required',
            'selling_point.required' => 'Selling Point field is required',
        ]);


        $assigneeid = Auth::user()->id;

        $clientid = Crypt::decrypt($client_id);

        $client_obj = new Client();

        DB::table('tbl_clients')
            ->where('client_id',$clientid)
            ->update(['comp_name' => $request['comp_name'],
                'company_url' => $request['company_url'],
                'contact_name' => $request['contact_name'],
                'designation' => $request['designation'],
                'phone' => $request['phone'],
                'alternate_phone' => $request['alternate_phone'],
                'email' => $request['email'],
                'company_size' => $request['company_size'],
                'from_time' => $request['from_timings'],
                'to_time' => $request['to_timings'],
                'days_working' => $request['days_working'],
                'industry' => $request['industry'],
                'headquarters' => $request['headquarters'],
                'company_specialties' => $request['company_specialties'],
                'office_address' => $request['office_address'],
                'selling_point' => $request['selling_point']]);

        Session::flash('success_msg', 'Client Updated Successfully!');
        return redirect()->route('listclient');
    }





   





}
