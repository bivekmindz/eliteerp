<?php

namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Model\Adminmodel\Position;
use App\Model\Adminmodel\Department;
use App\Model\Client as Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Session;
use Log;
use Storage;

class PositionController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        //$assigneeid=Auth::user()->id;
    }

    public function position()
    {
        $assigneeid=Auth::user()->id;
        $departments=Department::all();
        $client=DB::table('tbl_clientspockmap as c')
            ->join('tbl_clients as t','c.fk_clientid','=','t.client_id')
            ->select('c.fk_clientid','t.comp_name')
            ->where(['c.assignee_emp_id'=>$assigneeid])
            ->distinct('c.fk_clientid')
            ->get();
        $clients=Client::all();
        //dd($client);

        $emp=DB::table('users')
            ->select('name','id')
            ->whereRaw('FIND_IN_SET(3,emp_role)','emp_role')
            ->get();

        $target=DB::table('tbl_clientjdrecruitermap as c')
            ->join('users as u','u.id','=','c.fk_empid')
            ->select('u.name')
            ->where(['c.fk_assigneeid'=>$assigneeid])
            ->distinct('c.fk_assigneeid')
            ->get();
       //dd($target);
       // dd($emp);
        return view('Emp.position',compact('departments','client','emp','target','clients'));
    }

    public function addposition(Request $request)
    {

        $clientJd = $request->input('upload_position_jd');
        $dom = new \DomDocument();
        $dom->loadHtml($clientJd, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
        $clientJd = $dom->saveHTML();
        // foreach($images as $k => $img){
        //     dd($img);
        //     $data = $img->getAttribute('src');
        //     list($type, $data) = explode(';', $data);
        //     list(, $data)      = explode(',', $data);
        //     $data = base64_decode($data);
        //     $image_name= "/upload/" . time().$k.'.png';
        //     $path = public_path() . $image_name;
        //     file_put_contents($path, $data);
        //     $img->removeAttribute('src');
        //     $img->setAttribute('src', $image_name);

        // }

        
        //dd($clientJd);

        /**JD upload starts*/
        // $file = $request->file('upload_position_jd');
        // $date = new \DateTime();
        
        // $year = date('Y');
        // $month = date('m');
        // $day = date('d');
        // $dirStorage = DIRECTORY_SEPARATOR . $year . DIRECTORY_SEPARATOR 
        //                 . $month . DIRECTORY_SEPARATOR . $day;
        
        // Storage::disk('local')->makeDirectory($dirStorage);
        // $fileName = uniqid() . "-" . str_replace(" ", "-", $file->getClientOriginalName());
        // $filepath = $dirStorage . DIRECTORY_SEPARATOR . $fileName;
        // Storage::disk('local')->put($filepath,file_get_contents($file->getRealPath()));
        /**JD upload starts*/


        $this->validate($request,[
            'dept' => 'required',
            'compname'=>'required',
            'jobtitle' => 'required',
            'upload_position_jd' => 'required',
            'noofpos' => 'required|numeric',
            
        ], 
        [
            'dept.required' => 'Department field is required.',
            'compname.required'=>'company name field is required list',
            'jobtitle.required' => 'Jobtitle field is required.',
            'upload_position_jd' => 'Please Upload Job Decription',
            'noofpos.required' => 'Number of Positions field is required'
        ]);
        $assigneeid=Auth::user()->id;
        DB::table('tbl_clientjd_master')->insert([
            'clientjob_deptid'=>$request['dept'],
            'clientjob_compid'=>$request['compname'],
            'clientjob_title'  => $request['jobtitle'],
            'upload_position_jd' => $clientJd,
            'clientjob_noofposition'=>$request['noofpos'],
            'drive'=>1,
            'clientjob_empid'=>$assigneeid,
            'clientjob_otherinfo'=>$request['otherinfo'],
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()

        ]);

        Session::flash('success_msg', 'Position Added Successfully!');

        return redirect()->route('positionform');

    }


    public function positionlist()
    {
        $assigneeid=Auth::user()->id;
        $pos=DB::table('tbl_clientjd_master as m')
          ->join('tbl_clients as c','m.clientjob_compid','=','c.client_id')
        ->select('m.*','c.comp_name')
        ->where(['clientjob_empid'=>$assigneeid])
        ->get();
        //dd($pos);
        return view('Emp.listposition',['pos'=>$pos]);

    }

  public function positionjd(Request $request)
    {
        $position = DB::table('tbl_clientjd_master')
            ->select('upload_position_jd')
            ->where(['clientjob_id' => $request->id])
            ->first();

        return json_encode($position->upload_position_jd);
    }

}
