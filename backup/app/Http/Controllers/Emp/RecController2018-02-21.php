<?php

namespace App\Http\Controllers\Emp;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Model\Adminmodel;
use App\Model\Adminmodel\Employees;
use App\Model\User;
use App\Model\Admin;
use Carbon\Carbon;
use Session;
use Storage;
use DB;
use Mail;


class RecController extends Controller
{
    
    
public function viewallassignedrecuiterposition()
 {
     
    $pos= DB::table('tbl_clientjd_master as c')
         ->join('tbl_clientjdrecruitermap as m','c.clientjob_id','=','m.fk_jdid')
        ->join('users as u','u.id','=','m.fk_empid')
        ->join('tbl_clients as cm','cm.client_id','=','c.clientjob_compid')
         ->select('m.*','c.*','u.name','cm.comp_name')
         ->get();
    
    return view('Emp.viewallasignedrec',['pos'=>$pos]);

 }
 
  public function uploadcv(Request $request) 
    {


        $encryptId = $request->clientreqEncrypt;
        //dd($encryptId);

        $file = $request->file('cv_name');
        $recruiterId=Auth::user()->id;
       
        $positionId = $request->fkjdid;
        $recruiterMapId = $request->clientreqid;
        
        for($i=0; $i<count($request->candidate_name); $i++) {
           
            $candidateName = $request->candidate_name[$i];
            $candidateMob = $request->candidate_mob[$i];
            $candidateEmail = $request->candidate_email[$i];

            $date = new \DateTime();
        
            $candidateCv = 'uploadcv';
            $year = date('Y');
            $month = date('m');
            $day = date('d');
            $dirStorage = DIRECTORY_SEPARATOR . $candidateCv . DIRECTORY_SEPARATOR . $year . DIRECTORY_SEPARATOR 
                            . $month . DIRECTORY_SEPARATOR . $day;
            
            Storage::disk('local')->makeDirectory($dirStorage);
            $fileName = uniqid() . "-" . str_replace(" ", "-", $request->file('cv_name')[$i]->getClientOriginalName());
            $filepath = $dirStorage . DIRECTORY_SEPARATOR . $fileName;
            Storage::disk('local')->put($filepath,file_get_contents($request->file('cv_name')[$i]->getRealPath()));
            /**JD upload starts*/
           
            DB::table('tbl_recruiter_cv')->insert([
            'recruiter_id'=>$recruiterId,
            'r_map_id'=>$recruiterMapId,
            'position_id'  => $positionId,
            'cv_name' => $fileName,
            'candidate_name'=>$candidateName,
            'candidate_mob'=>$candidateMob,
            'candidate_email'=>$candidateEmail,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()

            ]);

            

        }
        Session::flash('success_msg', 'Candidate resume uploaded. Successfully to start another position please close this position');
        return redirect('get-assigned-postion/'.$encryptId);
        //return redirect('dashboard');
      
        
    }
    
     public function getassignedposition($id) 
    {

        
        $clientid = Crypt::decrypt($id);
        $recruiterId = Auth::user()->id;


        $getList = DB::table('tbl_clientjd_master as tcm')
                    ->join('tbl_clients as tc', 'tcm.clientjob_compid', '=', 'tc.client_id')
                    ->join('tbl_clientjdrecruitermap as tcp', 'tcm.clientjob_id', '=', 'tcp.fk_jdid')
                    ->select('tcm.clientjob_title','tcp.clientreq_id', 'tc.*', 'tcm.upload_position_jd', 'tcp.fk_jdid', 'tcp.clientreq_id')
                    ->where('tcm.clientjob_id', '=', $clientid)
                    ->first();
       
        $getuploadedCv = DB::table('tbl_recruiter_cv as trc')
                            ->join('tbl_clientjdrecruitermap as tcm', 'trc.recruiter_id', '=', 'tcm.fk_empid')
                            ->join('users as u', 'tcm.fk_empid', '=', 'u.id')
                            ->select('trc.cv_name', 'trc.candidate_name', 'trc.candidate_email', 'trc.candidate_mob', 'tcm.fk_empid', 'u.id','trc.position_id','tcm.fk_jdid')
                            ->where('u.id', '=', $recruiterId)
                            ->where('trc.position_id', '=', $clientid)
                            ->get();       

        $data = Session::all();

        if(isset($data['user']['starttime']))
        {
            $timestamp = strtotime( $data['user']['starttime']) + 60*60;

            $time = date('H:i:s', $timestamp);

            $date1 = $data['user']['starttime'];
            $date2 = $time;
            $date3 = date('H:i:s');$datestime= round((strtotime($date2) - strtotime($date3)) );
        }
        else
        {
            $datestime=0;
        }           
        
        return view('Emp.allpositionassigned', [
            'getlists'=>$getList,
            'sessionstart'=>$datestime,
            'getuploadCv'=>$getuploadedCv
        ]);
    }
    



    public function gettime($id)
    {

        $mytime = Carbon::now();
        $currentTime = $mytime->toTimeString();
        $recruiterId = Auth::user()->id;

        $checkTime = DB::table('tbl_clientrec_start_endtime as tcse')
                        ->select('tcse.*')
                        ->where('clientstart_recuiterid', '=', $recruiterId)
                        ->where('clientstart_stoptime', '=', 'NULL')
                        ->count();
        

        if( $checkTime == 0 ) {


            DB::table('tbl_clientjdrecruitermap')
            ->where('clientreq_id', '=', $id)
            ->update([
                'clientreq_starttime'=>$currentTime
            ]);
        


        DB::table('tbl_clientrec_start_endtime')->insert([
            'clientreq_id'=>$id,
            'clientstart_starttime'=>$currentTime,
            'clientstart_recuiterid'=>$recruiterId

        ]);

        $dataemp = Employees::select("*")->where('id','=',"{$recruiterId}")->get();
        $dataadmin = Admin::select("*")->where('id','=',"1")->get();
        $emailuser= $dataemp[0]['email'];
        $emailadmin= $dataadmin[0]['email'];

        $data = [
            'name' => $dataemp[0]['name'],
            'email' =>$dataemp[0]['email'],
            'adminname' =>$dataadmin[0]['name'],

            'emailadmin' =>$emailadmin

        ];


        Mail::send('Emp.starttimemail', $data, function($message) use ($data)
        {
            $message->from($data['email'],$data['name']);
            $message->to($data['emailadmin'],$data['adminname']);
            $message->subject('Started work on position');
        });
        $datas = Session::all();

        Session::put('user.starttime', $currentTime);
        return 'yes';
        } else{
            return 'No';
        }               
        


        
    }


    public function endtime($id)
    {

        $getcv =    DB::table('tbl_recruiter_cv')
            ->select('tbl_recruiter_cv.*' )
            ->where('r_map_id', $id)->get();


        $getjobid = DB::table('tbl_clientrec_start_endtime')
            ->where('clientreq_id', '=', $id)
            ->select('tbl_clientrec_start_endtime.*')
            ->orderBy('clientstartid', 'desc')

            ->first();

        $mytime = Carbon::now();
        $currentTime = $mytime->toTimeString();
        DB::table('tbl_clientjdrecruitermap')
            ->where('clientreq_id', '=', $id)
            ->update([
                'clientreq_stoptime'=>$currentTime
            ]);
        $recruiterId = Auth::user()->id;

        DB::table('tbl_clientrec_start_endtime')
            ->where('clientstartid', '=', $getjobid->clientstartid)
            ->update([
                'clientstart_stoptime'=>$currentTime
            ]);


        $dataemp = Employees::select("*")->where('id','=',"{$recruiterId}")->get();
        $dataadmin = Admin::select("*")->where('id','=',"1")->get();

        $emailuser= $dataemp[0]['email'];
        $emailadmin= $dataadmin[0]['email'];

        $datas = Session::all();
        Session::forget('user.starttime');
        $data = [
            'name' => $dataemp[0]['name'],
            'email' =>$dataemp[0]['email'],
            'adminname' =>$dataadmin[0]['name'],
            'getcv' =>$getcv,
            'urldata' =>URL('/'),
            'emailadmin' =>$emailadmin

        ];

        Mail::send('Emp.endtimemail', $data, function($message) use ($data)
        {
            $message->from($data['email'],$data['name']);
            $message->to($data['emailadmin'],$data['adminname']);
            $message->subject('Endtime work on position');
        });



    }

    public function sendMail() 
    {

        $getcv = DB::table('tbl_recruiter_cv')
                ->select('tbl_recruiter_cv.*' )
                ->where('r_map_id', 4)
                ->get();

        if($getcv == 0)
        {

            Mail::send('Emp.sendmailtoadmin', $data, function($message) use ($data)
            {
                $message->from('gurpreetsingh@mindztechnology.com','Gurpreet Singh');
                $message->to('shalini@mindztechnology.com','Shalini');
                $message->subject('User has not submit any Resume');
            });


        }        
    }


    // public function upload_file(Request $request)
    // {
    //     dd('jkdasdjksbajkdbasjkdsa');
    // }
    
    
    
}