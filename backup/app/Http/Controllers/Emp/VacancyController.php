<?php
/**
 * Created by PhpStorm.
 * User: mindz
 * Date: 24/1/18
 * Time: 11:53 AM
 */

namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;
use App\Model\User as User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class VacancyController extends Controller
{
    	public function __construct()
    {
      $this->middleware('auth');
    }
    
    
     public function vacancy()
    {
        $vacancy = DB::table('tbl_clients')
                    ->join('tbl_clientjd_master', 'tbl_clients.client_id','=','tbl_clientjd_master.clientjob_empid')
                    ->select('tbl_clients.comp_name','tbl_clientjd_master.*')
                    ->get(); 
        
        foreach($vacancy as $key => $val){
            $totalCount = DB::table('tbl_recruiter_cv')
                          ->select('*')
                          ->where('position_id',$val->clientjob_id)
                          ->count();
            $vacancy[$key]->totalCount = $totalCount;              
        }
        return view('Emp.vacancylist',compact('vacancy'));
    }

    public function candidateList()
    {
        $candidate = DB::table('tbl_recruiter_cv')
                        ->join('users','users.id','=','tbl_recruiter_cv.recruiter_id')
                        ->join('tbl_clientjd_master', 'tbl_recruiter_cv.position_id','=','tbl_clientjd_master.clientjob_id')
                        ->join('tbl_clients','tbl_clients.client_id','=','tbl_clientjd_master.clientjob_empid')
                        ->select('tbl_clients.*','users.*','tbl_recruiter_cv.*','tbl_clientjd_master.*')
                        ->get(); 
        
        return view('Emp.candidatelist',compact('candidate'));
    }
}