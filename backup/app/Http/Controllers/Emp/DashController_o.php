<?php

namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Model\Client as Client;
use App\Model\User as User;
use App\Model\Adminmodel\Department as Department;

use Session;

class DashController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //$assigneeid=Auth::user()->id;
    }

    public function index(Request $request)
    {
        //dd( $request);

        return view('Emp.dashboard');

    }
    public function newclient()
    {
        $dept=Department::all();

        return view('Emp.newclient',compact('dept'));

    }

    public function saveclient(Request $request)
    {

        $this->validate($request,[
            'cname' => 'required',
            'cpn' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required|numeric',
            'deptid' => 'required',
            'desig' => 'required'
        ],[
            'cname.required' => 'Company name field is required.',
            'cpn.required' => 'Contact person name is required',
            'email.required' => 'Email field is required',
            'phone.required' => 'Phone number field is required.',
//            'phone.max'=>'Phone number field must be of 10 digit only',
            'desig.required' => ' Designation field is required',
            'deptid.required' => ' Select Category',
        ]);

//        $this->validate($request,
//            ['cname'=>'bail|required',
//            'cpn'=>'required',
//            'phone'=>'required|numeric',
//            'email'=>'required',
//            'deptid'=>'required',
//            'desig'=>'required'
//            ]);
        $assigneeid=Auth::user()->id;

        $postData = $request->all();
        $client_obj= new Client;
        $client_obj->client_catid=$request['deptid'];
        $client_obj->comp_name = $request['cname'];
        $client_obj->phone=$request['phone'];
        $client_obj->contact_name=$request['cpn'];
        $client_obj->email=$request['email'];
        $client_obj->designation=$request['desig'];
        $client_obj->clientsuid=$assigneeid;
        //  dd($client_obj);
        $client_obj->save();

        Session::flash('success_msg', 'Client Added Successfully!');

        return redirect()->route('newclient');

    }

    public function listclient(Request $request)
    {
        // $client=Client::paginate(5);
        // $client=DB::table()
        $assigneeid=Auth::user()->id;
//        $clients=Client::where(
//            ['clientsuid'=>$assigneeid])
//            ->orderBy('created_at','desc')
//            ->paginate(50);
//        dd($clients);
        $clients=  DB::table('tbl_clients as c')
            ->join('tbl_department as d','c.client_catid','=','d.dept_id')
            ->select('d.dept_name','c.*')
            ->where(['c.clientsuid'=>$assigneeid])
            ->get();
        $user=User::all();
        return view('Emp.listclient',compact('clients','user'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    public function assignspoc($client_id,Request $request)
    {
        $this->validate($request,[
            'empid' => 'required',

        ],[
            'empid.required' => 'Select  Employees from the list.',
        ]);

        $clientid=Crypt::decrypt($client_id);
        $assigneeid=Auth::user()->id;
        //dd($request['empid']);
        foreach ($request['empid'] as $key=>$value)
        {
//            DB::table('users')->where(['id'=> $value])
//                ->update([
//                'empclient_id' => $clientid,
//                'assignee_id' => $assigneeid
//                ]);
            // $client->create();
            DB::table('tbl_clientspockmap')->insert([
                'fk_clientid'=>$clientid,
                'fk_empid'=>$value,
                'assignee_emp_id'=>$assigneeid
            ]);

        }

        Session::flash('success_msg', 'Spoc Allocated Successfully!');

        return redirect()->route('listclient');



    }

    public function clientspoc()
    {
        $assigneeid=Auth::user()->id;
        //$clients=Client::where(['clientsuid'=>$assigneeid])->orderBy('created_at','desc')->paginate(50);
        // dd($clients);
        // $emp=DB::table('tbl_clientspockmap')->select('name','id')->where('id')
        $clientemp   =DB::table('tbl_clientspockmap as cs')
            ->join('tbl_clients as c','cs.fk_clientid','=','c.client_id')
            ->join('users as u','cs.fk_empid','=','u.id')
            ->select('c.comp_name','u.name')
            ->where('cs.assignee_emp_id','=',$assigneeid)
            ->groupby('c.comp_name','u.name')
            ->get();
        //dd($abc);
        return view('Emp.clientspoc',compact('clients','clientemp'));
    }



    public function clientexcel()
    {
        return view('Emp.clientexcel');
    }
    
   

    public function updateClient($client_id,Request $request)
    {
        $this->validate($request,[
            'comp_name' => 'required',
            'company_url' => 'required',
            'email' => 'required|email|unique:users',
            'contact_name' => 'required',
            'designation' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'company_size' => 'required',
            'timings' => 'required',
            'days_working' => 'required',
            'company_size' => 'required',
            'industry' => 'required',
            'headquarters' => 'required',
            'company_specialties' => 'required',
            'office_address' => 'required',
            'selling_point' => 'required',
        ],[
            'comp_name.required' => 'Company name field is required.',
            'company_url.required' => 'Company Url is required.',
            'email.required' => 'Email field is required.',
            'contact_name.required' => 'Contact number field is required.',
            'designation.required' => 'Designation field is required.',
            'phone.required' => 'Phone number field is required.',
            'phone.max'=>'Phone number field must be of 10 digit only.',
            'company_size.required' => 'Company Size is required.',
            'timings.required' => 'Timings is required.',
            'days_working.required' => 'Days of working field is required',
            'industry.required' => 'Industry field is required',
            'headquarters.required' => 'Headquarters field is required',
            'company_specialties.required' => 'Company Specialties field is required',
            'office_address.required' => 'Office address field is required',
            'selling_point.required' => 'Selling Point field is required',
        ]);


        $assigneeid = Auth::user()->id;
        
        $clientid = Crypt::decrypt($client_id);

        $client_obj = new Client();
              
        DB::table('tbl_clients')
            ->where('client_id',$clientid)
            ->update(['comp_name' => $request['comp_name'],
                'company_url' => $request['company_url'],
                'contact_name' => $request['contact_name'],
                'designation' => $request['designation'],
                'phone' => $request['phone'],
                'email' => $request['email'],
                'company_size' => $request['company_size'],
                'timings' => $request['timings'],
                'days_working' => $request['days_working'],
                'industry' => $request['industry'],
                'headquarters' => $request['headquarters'],
                'company_specialties' => $request['company_specialties'],
                'office_address' => $request['office_address'],
                'selling_point' => $request['selling_point']]);
        
        Session::flash('success_msg', 'Client Updated Successfully!');
        return redirect()->route('listclient');
    }
    
     public function editclient($client_id,Request $request)
    {
        $clientid = Crypt::decrypt($client_id);
        $client_details = DB::table('tbl_clients')
                            ->select('*')
                            ->where('client_id',$clientid)
                            ->get();
        
        return view('Emp.editclient',compact('client_details'));
    }


}
