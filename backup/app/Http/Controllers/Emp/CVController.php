<?php
/**
 * Created by PhpStorm.
 * User: mindz
 * Date: 2/2/18
 * Time: 3:39 PM
 */

namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Mail;
use Session;



class CVController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //$assigneeid=Auth::user()->id;
    }

    public function shortlistcv()
    {
        $assigneeid=Auth::user()->id;
        // dd($assigneeid);
        $pos=DB::table('tbl_recruiter_cv as c')
            ->join('tbl_clientjd_master as p','p.clientjob_id','=','c.position_id')
            ->select('c.position_id','p.clientjob_title','p.clientjob_noofposition',DB::raw('COUNT(c.position_id) as total_upload_profile'))
            ->where(['c.cv_status'=>1,'p.clientjob_empid'=>$assigneeid])

            ->groupBy('c.position_id','p.clientjob_title','p.clientjob_noofposition')
            ->get();

        return view('Emp.shortlistcv',['pos'=>$pos]);

    }



    public function getcandidatedetails($id)
    {
       // dd('fdfdf');

        $assigneeid=Auth::user()->id;
        $candidate=DB::table('tbl_recruiter_cv')
            ->select('*')
            ->where([
                ['cv_status', '=', '1'],
                ['position_id', '=', $id],
            ])
            ->get();
        echo $candidate;


    }


    public function savecandidate(Request $request)
    {

        //dd($request->all());
        echo '<pre>' ;
        $loginid = Auth::user()->id;
        $candidate_id_unarray = [];
        $recruiter_id_unarray = [];
        ///dd($request->all());

        //all check box ids
        foreach ($request['uncheckbox_data'] as $key=>$value)
        {
            $candidate_recruiter_array = explode('/',$value);
            $candidate_recruiter_array_uncheckids[]=array($candidate_recruiter_array[0],$candidate_recruiter_array[1]); //all uncheck/check id

            $candidate_id_unarray[] = $candidate_recruiter_array[0];
            $recruiter_id_unarray[] = $candidate_recruiter_array[1];
        }

       //  dd( $candidate_recruiter_array_uncheckids);


        $candidate_id_array = [];
        $recruiter_id_array = [];

//when no checkbox is checked or no profile is shortlisted
        if(empty($request['checkbox_data']))
        {


            $i = 0;
            foreach ($recruiter_id_unarray as $key => $value) {
                $unchecked_checkbox_array[$value][] = $candidate_id_unarray[$i];
                $i++;
            }
            //dd($unchecked_checkbox_array);

            foreach($unchecked_checkbox_array as $kkk=>$vvv)
            {

                $rec_email = DB::table('tbl_recruiter_cv as rc')
                    ->join('users as u', 'rc.position_id', '=', 'u.id')
                    ->select('u.email', 'u.name')
                    ->where(['rc.recruiter_id' => $kkk])
                    ->first();
                //dd($vvv);

                $complete_unshortlist = DB::table('tbl_recruiter_cv as rc')
                    ->select('rc.candidate_name')
                    ->whereIn('rc.id', $vvv)
                    // ->where(['rc.id'=>$v])
                    ->get();
                $complete_shortlist=0;

                $sendMail = $this->sendMail($complete_shortlist,$complete_unshortlist,$request['poid'],$loginid,$rec_email );



                foreach($candidate_recruiter_array_uncheckids as $key=>$value)
                {
                    DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$value[0]])
                        ->update(['c.cv_status'=>3]);
                }



            }


            session::flash('success','Message Sent');
            return redirect()->route('shortlistcv');


        }//end of empty($request['checkbox_data'])

//dd('fdfdf');


        if(!empty($request['checkbox_data']))
        {


            foreach ($request['checkbox_data'] as $key => $value)
            {
                $candidate_recruiter_array = explode('/', $value);
                $candidate_recruiter_array_checkids[] = array($candidate_recruiter_array[0], $candidate_recruiter_array[1]);
                $candidate_id_array[] = $candidate_recruiter_array[0];
                $recruiter_id_array[] = $candidate_recruiter_array[1];
            }
            //dd($candidate_recruiter_array_checkids);


            $diff = array_diff(array_map('json_encode', $candidate_recruiter_array_uncheckids), array_map('json_encode', $candidate_recruiter_array_checkids));

// Json decode the result
            $diff = array_map('json_decode', $diff);

           // dd($diff);

            if(empty($diff)) //when all checkboxs are selected
            {
                $i = 0;
                foreach ($recruiter_id_array as $key => $value)
                {
                    $data[$value][] = $candidate_id_array[$i];
                    $i++;
                }

                foreach ($data as $key => $val)
                {

                    $rec_email = DB::table('tbl_recruiter_cv as rc')
                        ->join('users as u', 'rc.position_id', '=', 'u.id')
                        ->select('u.email', 'u.name')
                        ->where(['rc.recruiter_id' => $key])
                        ->first();

                    $complete_shortlist = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->whereIn('rc.id', $val)
                        // ->where(['rc.id'=>$v])
                        ->get();
                    $complete_unshortlist=0;

                    $sendMail = $this->sendMail($complete_shortlist,$complete_unshortlist,$request['poid'],$loginid,$rec_email );



                    foreach($candidate_recruiter_array_checkids as $key=>$value)
                    {
                        DB::table('tbl_recruiter_cv as c')
                            ->where(['c.id'=>$value[0]])
                            ->update(['c.cv_status'=>2]);
                    }


                }//end foreach
                session::flash('success','Message Sent');
                return redirect()->route('shortlistcv');



            }
            
       



            foreach ($diff as $key => $value)
            {
                $candidate_rec_uncheck[] = $diff[$key][0];
                $recruiter_uncheck[] = $diff[$key][1];
            }
         


            //unchecked check box
            $j = 0;
            foreach ($recruiter_uncheck as $key => $val)
            {
                $data1[$val][] = $candidate_rec_uncheck[$j];
                $j++;
            }
    
            $i = 0;
            foreach ($recruiter_id_array as $key => $value)
            {
                $data[$value][] = $candidate_id_array[$i];
                $i++;
            }
            //   print_r($data1);//unchecked chekbox
            $merged = $this->merge_common_keys($data,$data1);
       
            foreach ($merged as $key=>$val)
            {
                
                $pro_unshort_one ='';
                $pro_unshort_two ='';

                $rec_email = DB::table('tbl_recruiter_cv as rc')
                    ->join('users as u', 'rc.position_id', '=', 'u.id')
                    ->select('u.email', 'u.name')
                    ->where(['rc.recruiter_id' => $key])
                    ->first();
                if(!empty($val[0])) {

                    $pro_unshort_one = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->whereIn('rc.id', $val[0])
                        // ->where(['rc.id'=>$v])
                        ->get();
                    //  print_r($pro_unshort_one);
                }
                if(!empty($val[1])) {

                    $pro_unshort_two = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->whereIn('rc.id', $val[1])
                        // ->where(['rc.id'=>$v])
                        ->get();
                    // print_r($pro_unshort_two);
                }
             //   dd('fdfdf');
                
                
                     

                $sendMail = $this->sendMail($pro_unshort_one,$pro_unshort_two,$request['poid'],$loginid,$rec_email );
                 

                //dd($candidate_recruiter_array_checkids);

                foreach($candidate_recruiter_array_checkids as $key=>$value)
                {
                    DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$value[0]])
                        ->update(['c.cv_status'=>2]);
                }
                
                //dd($diff);

                foreach($diff as $key=>$value)
                {
                    DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$value[0]])
                        ->update(['c.cv_status'=>3]);
                }





            }
            session::flash('success','Message Sent');
            return redirect()->route('shortlistcv');

        }//end of foreach




    }//end of save candidate






    //end of if(!empty($request['checkbox_data']))


    public  function merge_common_keys()
    {
        $arr = func_get_args();
        $num = func_num_args();

        $keys = array();
        $i = 0;
        for ($i=0; $i<$num; ++$i)
        {
            $keys = array_merge($keys, array_keys($arr[$i]));
        }
        $keys = array_unique($keys);

        $merged = array();

        foreach ($keys as $key)
        {
            $merged[$key] = array();
            for($i=0; $i<$num; ++$i)
            {
                $merged[$key][] = isset($arr[$i][$key]) ? $arr[$i][$key] : '';
            }
        }
        return $merged;
    }


    public  function sendMail($proSort=Null,$proUnsort=Null ,$reuest,$loginid,$rec_email)
    {

        $mail_data = [
            //'position' => $position,
            'profile' => $proSort,
            'unprofile' => $proUnsort,
            'position' => $reuest


        ];


        //dd($mail_data);


        $from_email = DB::table('users  as u')
            ->select('u.email')
            ->where(['u.id' => $loginid])
            ->get();
         //  dd($from_email);

        $from = $from_email[0]->email;
        
        $to = $rec_email->email; //'vijitha@mindztechnology.com';
        $toname = $rec_email->name; //vijitha
       // $to = 'shalini29081991@gmail.com';
        //$toname = 'vijitha';
        
        //dd($to);
        

          Mail::send('Emp.mails.shortlistcvmail', ['mail_data' => $mail_data], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite erp');
            $message->to($to, $toname);
            $message->subject('Short Listed Profiles');

        });
       
    }

}






