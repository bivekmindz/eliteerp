<?php

namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Model\Adminmodel\Position;
use App\Model\Adminmodel\Department;
use App\Model\Client as Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Session;
use Mail;
use Log;
use Storage;

class PositionController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        //$assigneeid=Auth::user()->id;
    }

    public function position()
    {
        $assigneeid=Auth::user()->id;
        $departments=Department::all();
        $client=DB::table('tbl_clientspockmap as c')
            ->join('tbl_clients as t','c.fk_clientid','=','t.client_id')
            ->select('c.fk_clientid','t.comp_name')
            ->where(['c.fk_empid'=>$assigneeid])
            ->distinct('c.fk_clientid')
            ->get();

//       $client=Client::all();
      //  dd($client);

        $emp=DB::table('users')
            ->select('name','id')
            ->whereRaw('FIND_IN_SET(3,emp_role)','emp_role')
            ->get();

        $target=DB::table('tbl_clientjdrecruitermap as c')
            ->join('users as u','u.id','=','c.fk_empid')
            ->select('u.name')
            ->where(['c.fk_assigneeid'=>$assigneeid])
            ->distinct('c.fk_assigneeid')
            ->get();
       //dd($target);
       // dd($emp);
        return view('Emp.position',compact('departments','client','emp','target'));
    }

    public function addposition(Request $request)
    {

        $clientJd = $request->input('upload_position_jd');
        $dom = new \DomDocument();
        $dom->loadHtml($clientJd, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
        $clientJd = $dom->saveHTML();


        $this->validate($request,[
            'dept' => 'required',
            'compname'=>'required',
            'jobtitle' => 'required',
            'upload_position_jd' => 'required',
            'noofpos' => 'required|numeric',
            
        ], 
        [
            'dept.required' => 'Department field is required.',
            'compname.required'=>'company name field is required list',
            'jobtitle.required' => 'Jobtitle field is required.',
            'upload_position_jd' => 'Please Upload Job Decription',
            'noofpos.required' => 'Number of Positions field is required'
        ]);
        $assigneeid=Auth::user()->id;


        DB::table('tbl_clientjd_master')->insert([
            'clientjob_deptid'=>$request['dept'],
            'clientjob_compid'=>$request['compname'],
            'clientjob_title'  => $request['jobtitle'],
            'upload_position_jd' => strip_tags($clientJd),
            'clientjob_noofposition'=>$request['noofpos'],
            'drive'=>1,
            'clientjob_empid'=>$assigneeid,
            'clientjob_otherinfo'=>$request['otherinfo'],
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()

        ]);

        $comp=DB::table('tbl_clients as c')
            ->select('c.comp_name')
            ->where('c.client_id',$request['compname'])
            ->first();
        //dd($comp);

        $dept=DB::table('tbl_department as d')
            ->select('d.dept_name')
            ->where('d.dept_id',$request['dept'])->first();


        $from_email = DB::table('users  as u')
            ->select('u.email')
            ->where(['u.id' => $assigneeid])
            ->get();
      
        $from = $from_email[0]->email;
        $to=DB::table('admins')->select('email','name')->first();
        

      //  $to = $positiondetails['to']->email;
      // echo $to;
     //   $toname = $positiondetails['to']->name;
       // dd($toname);
     //   $from=$positiondetails['from'];
        
        $positiondetails=[ 'company_name'=> $comp,
                          'department'=>$dept,
                          'details'=>$request,
                          'from'=>$from,
                          'to'=>$to
                  ];

      
    //   dd($positiondetails);

      //  return view('Emp.mails.newpositionmail',['mail_data'=>$positiondetails]);

       $sendmail= $this->sendMail($positiondetails);

        Session::flash('success_msg', 'Position Added Successfully!');

        return redirect()->route('positionform');

    }


    public function positionlist()
    {
        $assigneeid=Auth::user()->id;
        $pos=DB::table('tbl_clientjd_master as m')
          ->join('tbl_clients as c','m.clientjob_compid','=','c.client_id')
        ->select('m.*','c.comp_name')
        ->where(['clientjob_empid'=>$assigneeid])

        ->get();
        //dd($pos);
        return view('Emp.listposition',['pos'=>$pos]);

    }


     public function assignclient()
    {
        $assigneeid=Auth::user()->id;
        //dd($assigneeid); 
        $pos=DB::table('tbl_clientspockmap as m')
          ->join('users as u','m.assignee_emp_id','=','u.id')
          ->join('tbl_clients as c','c.client_id','=','m.fk_clientid')
          ->leftjoin('users as us','us.id','=','m.fk_empid')
          ->select('c.*','m.fk_empid','us.name')
          ->where(['fk_empid'=>$assigneeid])
          //->groupBy('m.fk_clientid')
          ->get();
          //dd($pos);
          return view('Emp.viewassignclient',['assignlist'=>$pos]);

    }

    public function positionjd(Request $request)
    {
        $position = DB::table('tbl_clientjd_master')
            ->select('upload_position_jd')
            ->where(['clientjob_id' => $request->id])
            ->first();
        $pos = str_replace("\n", "", $position->upload_position_jd);
        $finalPos = stripslashes($pos);
        //@php
      //  $tag = htmlspecialchars($position->upload_position_jd);

       // @endphp

        //dd($tag);

        return $pos;
    }
    
    public  function sendMail($positiondetails)
    {
      // dd($positiondetails);
    
        $to =$positiondetails['to']->email;
      $toname =$positiondetails['to']->name;
    //   // dd($toname);
      $from=$positiondetails['from'];
        
        //  $to='shalini@mindztechnology.com';
        // $toname="shalini";
        // $from='shalini@mindztechnology.com';


     
    
        Mail::send('Emp.mails.newpositionmail', ['mail_data' => $positiondetails], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('New Added Position');

        });
    }


}
