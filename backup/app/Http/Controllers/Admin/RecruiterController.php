<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Adminmodel\Position;
use App\Model\Adminmodel\Employees;
use App\Model\User as User;
use App\Model\Adminmodel\RecruiterMap;
use Carbon\Carbon;
use Session;
use Storage;
use DB;
use App\Model\Admin;
use Mail;

class RecruiterController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**To get all list of positions */
    public function getlist()
    {

        $employees = DB::table('users as u')
            ->leftjoin('tbl_clientjdrecruitermap as tcp','tcp.fk_empid','=','u.id')
            ->select('u.name','u.id','tcp.fk_empid')
            ->where('tcp.fk_jdid', '!=', 3)
            ->groupBy('u.name','u.id','tcp.fk_empid');
            

         $remainingEmployees = DB::table('users as u')
            ->leftjoin('tbl_clientjdrecruitermap as tcp','tcp.fk_empid','=','u.id')
            ->select('u.name','u.id','tcp.fk_empid')
            ->whereRaw('FIND_IN_SET(?,u.emp_role)',[3])
            ->whereNull('tcp.fk_empid')
            ->union($employees)
            ->get();


         
        $positions = DB::table('tbl_clientjd_master')
            ->join('tbl_clients', 'tbl_clientjd_master.clientjob_compid', '=', 'tbl_clients.client_id')
            ->join('users', 'tbl_clientjd_master.clientjob_empid', '=', 'users.id')
            ->get();

        return view('admin.positionallocate', [
            'positions'=>$positions,
            'employees'=>$remainingEmployees
        ]);
    }


    public function getEmpList(Request $request)
    {

        $clientjob_id = $request->data;
        
        $employees = DB::table('users as u')
            ->leftjoin('tbl_clientjdrecruitermap as tcp','tcp.fk_empid','=','u.id')
            ->select('u.name','u.id','tcp.fk_empid')
            ->where('tcp.fk_jdid', '!=', $clientjob_id)
            ->groupBy('u.name','u.id','tcp.fk_empid');
               
        $remainingEmployees = DB::table('users as u')
            ->leftjoin('tbl_clientjdrecruitermap as tcp','tcp.fk_empid','=','u.id')
            ->select('u.name','u.id','tcp.fk_empid')
            ->whereRaw('FIND_IN_SET(?,u.emp_role)',[3])
            ->whereNull('tcp.fk_empid')
            ->union($employees)
            ->get();
  //          ->toSql();

//dd($remainingEmployees);
        $var ='';
        



        foreach ($remainingEmployees as $key => $remployees) {
        $var .='<option value="'.$remployees->id.'">'.$remployees->name.'</option>';
        }  
        return $var;   
        
    }

    public function autocomplete(Request $request)
    {
        $data = Employees::select("name")->where("name","LIKE","%{$request->input('query')}%")->get();
        return response()->json($data);
    }


    /**To assign recruiter */
    public function postRecruiter(Request $request)
    {

        $clientJobId = $request->input('clientjobid');
        $getAdminID = Auth::user()->id;
        
        foreach($request['assignposition'] as $key=>$value)
        {

            $dataemp = Employees::select("*")->where('id','=',"{$value}")->get();
            $dataadmin = Admin::select("*")->where('id','=',"1")->get();
            
            $emailuser= $dataemp[0]['email'];
            $emailadmin= $dataadmin[0]['email'];

            $data = [
                'name' => $dataemp[0]['name'],
                'email' =>$dataemp[0]['email'],
                'positionlist' =>URL('positionlist'),
                'urldata' =>URL('/'),
                'emailadmin' =>$emailadmin,
                'clientjob_title' => $request['clientjob_title']
            ];
            
        // dd($data['name']);
       // $data['email']='mail.mindztechnology.com';
    
          
          //  return view('admin.positionreqmail',$data);

            Mail::send('admin.positionreqmail', $data, function($message) use ($data)
            {
                $message->from($data['emailadmin']);
                $message->to($data['email'],$data['name']);
                $message->subject('Assignment of position to Recruiter');
            });



            DB::table('tbl_clientjdrecruitermap')->insert([
                'fk_jdid'=>$clientJobId,
                'fk_empid'=>$value,
                'fk_assigneeid'=>$getAdminID,
                'clientreq_createdon'=>Carbon::now(),
                'clientreq_updatedon'=>Carbon::now()
            ]);

        }
        Session::flash('success_msg', 'Recuiter Allocated Successfully!');
        return redirect()->route('allocateposition');

    }


    /**Get all assined recruiters on given position */
    public function getassignedposition()
    {

        $getList = DB::table('tbl_clientjd_master as tcm')
            ->join('tbl_clients as tc', 'tcm.clientjob_compid', '=', 'tc.client_id')
            ->join('tbl_clientjdrecruitermap as tcp', 'tcm.clientjob_id', '=', 'tcp.fk_jdid')
            ->select('tcm.clientjob_title','tcp.clientreq_id', 'tcm.upload_position_jd', 'tcp.fk_jdid', 'tcp.clientreq_id', 'tc.*')
            ->first();

        return view('admin.allpositionassigned', [
            'getlists'=>$getList
        ]);
    }



    public function viewassignedrecuiterposition()
    {
        //dd(Auth::user()->id) ;
        $pos= DB::table('tbl_clientjd_master as c')
            ->join('tbl_clientjdrecruitermap as m','c.clientjob_id','=','m.fk_jdid')
            ->join('users as u','u.id','=','m.fk_empid')
            ->join('tbl_clients as cm','cm.client_id','=','c.clientjob_compid')
            ->select('m.*','c.*','u.name','cm.comp_name')
            ->get();
        // dd($pos);
        return view('admin.viewassignposrec',['pos'=>$pos]);

    }

}
