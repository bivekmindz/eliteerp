<?php
/**
 * Created by PhpStorm.
 * User: mindz
 * Date: 17/2/18
 * Time: 10:07 AM
 */

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use Mail;


class ReportController extends Controller
{
    public function report()
    {
        $dt=Carbon::now();


       $report= DB::table('tbl_recruiter_cv as rr')
            ->join('users as u','u.id','=','rr.recruiter_id')
            ->join('tbl_clientjd_master as cjm','rr.position_id','=','cjm.clientjob_id')
           ->join('users as us','cjm.clientjob_empid','=','us.id')
            ->select('u.name as recruiter_name','u.id','cjm.clientjob_title as pos','cjm.clientjob_id',DB::raw('count(rr.position_id) as upload_cv'),'us.name as spoc',DB::raw("(SELECT count(*) FROM tbl_recruiter_cv as rrr
                          WHERE rrr.recruiter_id = u.id and rrr.position_id=cjm.clientjob_id and rrr.cv_status=2
                        ) as sent_to_client"))
           ->whereRaw('Date(rr.created_at) = CURDATE()')
           ->groupBy('rr.position_id','rr.recruiter_id','u.name','cjm.clientjob_title','us.name','u.id','cjm.clientjob_id')

           ->get();

       // $to= DB::table('admins')->select('email','name')->first();

        $to='shalini@mindztechnology.com';
        $from='shalini@mindztechnology.com';

        $reports=[ 'report'=> $report,
            'from'=>$from,
            'to'=>$to
        ];


      //  return view('Emp.mails.submisssion',['mail_data'=>$reports]);

       $sendmail= $this->sendMail($reports);

   dd('msg sent successfully');



    }

    public  function sendMail($reports)
    {
        // $to= DB::table('admins')->select('email','name')->first();

        $to='shalini29081991@gmail.com';
        $toname="shalini";
        $from='shalini@mindztechnology.com';


        Mail::send('Emp.mails.submisssion',  ['mail_data' => $reports], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Submission Report');

        });
    }


}