<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Model\User;
use App\Model\Role;
use App\Model\Department;
use App\Model\Adminmodel\Team;
use Session;
use DB;
use File;

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\Crypt;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column;
use PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\Rule;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;
use PhpOffice\PhpSpreadsheet\NamedRange;
use PHPExcel_Cell;
use PHPExcel_Cell_DataType;
use PHPExcel_Cell_IValueBinder;
use PHPExcel_Cell_DefaultValueBinder;

use PHPExcel; 
use PHPExcel_IOFactory;



class EmployeeController extends Controller
{
    //
    public function employee(){
    	$roles = Role::all();
    	$departments=Department::all();
    	//dd($roles);
        return view('Emp.newemp',compact('roles','departments'));
    }

    public function save(Request $request)
    {
    	//echo 'hi'; die;

        $this->validate($request,[
            'name' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'cpassword' => 'required|same:password',
            'doj' => 'required',
            'dept' => 'required',
            'role' => 'required'
        ],[
            'name.required' => 'Name field is required.',
            'phone.required' => 'Phone number field is required.|Numbers Only',
            'email.required' => 'Email field is required',
            'password.required' => 'Password field is required',
            'cpassword.required' => 'Confirm Password field is required',
//            'phone.max'=>'Phone number field must be of 10 digit only',
            'doj.required' => ' Date of joining field is required',
            'dept.required' => ' Department is required',
            'role.required' => ' Role is required'
        ]);



        $postData = $request->all();
        $role = implode(',', $request['role']);
        $pass = $request['password'];
        $cpass = $request['cpassword'];
        $user_obj= new User;
        $user_obj->name=$request['name'];
        $user_obj->emp_contactno  = $request['phone'];
        $user_obj->email=$request['email'];
        $user_obj->password=$request['password'];
        $user_obj->emp_doj=$request['doj'];
        $user_obj->emp_dept =$request['dept'];
        $user_obj->emp_role=$role;
        //print_r($user_obj); //die;
        //$user_obj->save();
       Session::flash('success_msg', 'Employee Added Successfully!');

        return redirect()->route('employee');

    }

    public function empexcel()
    {
        $users = User::all();
        return view('admin/empexcel',compact('users'));
    }

    public function excelup(Request $request)
    {
        if(!isset($request['upfile'])){
            Session::flash('error_msg', 'Please select excel file to upload.');
            return redirect()->route('empexcel');
        }
        
        if(isset($request['upfile']))
        {
           $extension = File::extension($request['upfile']->getClientOriginalName());
           if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
              //'Your file is a valid xls or csv file'
           }else {
              Session::flash('error_msg', 'Please select excel file (xlsx,xls,csv) to upload.');
              return redirect()->route('empexcel');
           }
        }

        $inputFileName = $request['upfile'];
        $helper = new Sample();
       // $helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
        $spreadsheet = IOFactory::load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $excel_row = array_shift($sheetData);
        $m=0;
        //dd($sheetData);
        foreach ($sheetData as $key=>$value)
        {
             $roleId = DB::table('tbl_role as tr')
                         ->select('tr.role_id')
                        ->where('role_name',$value['A'])
                         ->first();
               // dd($roleId);      
                 $password = bcrypt($value['D']);
                $id1=DB::table('users')->insert(
                [
                'emp_role' => $roleId->role_id,
                'name' => $value['B'] ? $value['B'] : '' ,
                'email' => $value['C'] ? $value['C'] : "" ,
                'password' => $password ? $password : "" ,
                'emp_contactno' => $value['E'] ? $value['E'] : "",   
                'emp_doj' => $value['F'] ? $value['F'] : "" ,
                'emp_dob' => $value['G'] ? $value['G'] : "",

                 ]
            );
            
               $id2 = DB::getPdo()->lastInsertId();
               
               
               if($value['H']==0)
               {

                DB::table('tbl_team')->insert([
                     'team_lead_id'=> $id2 ? $id2 : "0" ,
                     'team_name'=> $value['I']

                ]);

               }
               else
               {
                DB::table('tbl_team')->insert(
               [
               'team_members_id'=> $id2 ? $id2 : "0" ,
               'team_name'=>$value['I']
                  ]);

               }

     $m++;  
        }
        Session::flash('success_msg', 'Excel Uploaded  Successfully!');
        return redirect()->route('empexcel');
    }


   
      
    


 public function exceldownload()
 {
    //-----------Va Define-----------------------//
     $allArr               = array();
     $templevel            = 0;  
     $newkey               = 0;
     $grouparr[$templevel] = "";
  //-----------------------------------------------//
/* $role=['BD','SPOC','REC','HR'];*/

    $response['array_test']  = array(
       '1'=>'BD',
       '2'=>'SPOC',
       '3'=>'Recruiter',
      );
    foreach($response['array_test'] as $key => $value){
     $fuelID[] = $key;
     $fuelName[] = $value;
    //p($fuelName); exit;
   }
      $fuelProID = implode(',',$fuelID);
       $fuelProName = implode(',',$fuelName);

       $featurevaluee1[]    = 'test';
 
      $excelHeadArrqq1 = array_merge($featurevaluee1);
      $excelHeadArr1 = array_unique($excelHeadArrqq1);
      
      $oldfinalAttrData['test']       = $fuelProName;
      $oldfinalAttrData2['test']      = $fuelName;

       $finalAttrData  = array_merge($oldfinalAttrData);
       $finalAttrData2 = $oldfinalAttrData2;
       $attCount = array();
       foreach($finalAttrData as $k=>$v){
       $valueCount   = count(explode(",", $v));
       $attCount[$k] = $valueCount+1;

      } 
             $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
    $objWorkSheet  = $objPHPExcel->createSheet();
    $objPHPExcel->setActiveSheetIndex(0);

    $finalExcelArr1 = array_merge($excelHeadArr1);
//p($finalAttrData2);exit;
     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
        $j      = 2;
       
        for($i=0;$i<count($finalExcelArr1);$i++){
         $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
         foreach ($finalAttrData2 as $key => $value) {
          foreach ($value as $k => $v) {

            if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
            }
        }
      }  
    }
           $arrPart2 = array('Role','Name','Email','Password','Contact No','DOJ','DOB','Team Lead','Team Name');
          if(!empty($excelHeadArr)){
             $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
         }else{
            $finalExcelArr = array_merge($arrPart2);
         }
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');
      
    $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
 
 //Set border style for active worksheet
for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

          for($k=2;$k <1000;$k++)
          {
 $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
    $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
 
    $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
    $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
    $objValidation24->setAllowBlank(false);
    $objValidation24->setShowInputMessage(true);
    $objValidation24->setShowErrorMessage(true);
    $objValidation24->setShowDropDown(true);
    $objValidation24->setErrorTitle('Input error');
    $objValidation24->setError('Value is not in list.');
    $objValidation24->setPromptTitle('Pick from list');
    $objValidation24->setPrompt('Please pick a value from the drop-down list.');
    $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
    $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
  
  }//secfor


}

        $filename  = "EmployeeExcel.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
die("a");
        exit;
          
}




} // end class