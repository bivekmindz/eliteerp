<?php
/**
 * Created by PhpStorm.
 * User: mindz
 * Date: 24/1/18
 * Time: 11:53 AM
 */

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Model\User as User;
use App\Model\Adminmodel\Team as Team;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use \Crypt;

class TeamController extends Controller
{
	public function __construct()
    {
      $this->middleware('auth:admin');
    }

    public function index()
    {
        return view('admin.teamassign');
    }
    
    public function team()
    {
        
      $user = User::all();
      
        $teamlisting = Team::all();
        
        if(!empty($teamlisting))
        {
        foreach($teamlisting as $key => $val)
        {
            $names = '';
            $name = '';

            $team_member = '';
            $team_members = '';
            $teamUsers = explode(',',$val->team_lead_id);
            foreach($teamUsers as $keys => $value)
            {
                $name = User::where('id',$value) 
                        ->select('name')
                        ->get();
                       
                $names .= $name[0]->name.',';
               
                
                $teamlisting[$key]->team_lead_id = rtrim($names,',');
            }
            
           // dd($names);

            $teamMembers = explode(',',$val->team_members_id);
            //dd($teamMembers);
            foreach($teamMembers as $kes => $values)
            {
                $team_member = User::where('id',$values) 
                        ->select('name')
                        ->get();
                $team_members .= $team_member[0]->name.',';
                $teamlisting[$key]->team_members_id = rtrim($team_members,',');
            }
            
        }
        
        }
          
       
        return view('admin.defineteam',['user'=>$user,'teamlisting'=>$teamlisting]);
    }

    public function editteam(Request $request,$id)
    {
        $teamDetails = DB::table('tbl_team')
                        ->select('*')
                        ->where('id',Crypt::decrypt($id))
                        ->first();
        
        //dd($teamDet);
        $user = User::all();
        return view('admin.editteam',compact('user','teamlisting','teamDetails'));
    }

    public function teamUpdate($teamId,Request $request)
    {
        $id = Crypt::decrypt($teamId);
        
        //dd($request->teammember);
        $user = User::all();

        $team = Team::find($id);
        $team->team_lead_id = '';
        $team->team_members_id = '';
        $team->team_name = $request->teamname;

        $teamleaders = $request->teamleader;
        foreach($teamleaders as $key => $val){
            $team->team_lead_id .= $val.',';
        }
        $lead_ids = trim($team->team_lead_id,',');
        $team->team_lead_id = $lead_ids;
        
        $teammember = $request->teammember;
        foreach($teammember as $key => $val){
            $team->team_members_id .= $val.',';
        }
        $member_ids = trim($team->team_members_id,',');
        $team->team_members_id = $member_ids;

        $team->team_status = 1;

        $now = Carbon::now();
        $team->team_created_on = $now;
        $team->team_updated_on = $now;
        $team->save();
        
        $teamlisting = Team::all();
        foreach($teamlisting as $key => $val){
            $names = '';
            $name = '';

            $team_member = '';
            $team_members = '';
            $teamUsers = explode(',',$val->team_lead_id);
            foreach($teamUsers as $keys => $value){
                $name = User::where('id',$value) 
                        ->select('name')
                        ->get();
                $names .= $name[0]->name.',';
                $teamlisting[$key]->team_lead_id = rtrim($names,',');
            }

            $teamMembers = explode(',',$val->team_members_id);
            foreach($teamMembers as $kes => $values){
                $team_member = User::where('id',$values) 
                        ->select('name')
                        ->get();
                $team_members .= $team_member[0]->name.',';
                $teamlisting[$key]->team_members_id = rtrim($team_members,',');
            }
        }
       
         return redirect('admin/team')->with('msg', 'Team updated successfully.');
    }

    public function deleteteam($id)
    {
        $id = Crypt::decrypt($id);
        $team = Team::find($id);
        $team->delete();
        return redirect('admin/team')->with('msg', 'Team deleted successfully.');
    }

    public function uniqueteam()
    {
       echo 1;
       die('/');
    }

    public function teamSave(Request $request)
    {
    	//dd($request->teammember);
        $user = User::all();

        $team = new Team();
        $team->team_name = $request->teamname;

        $teamleaders = $request->teamleader;
        foreach($teamleaders as $key => $val){
            $team->team_lead_id .= $val.',';
        }
        $lead_ids = trim($team->team_lead_id,',');
        $team->team_lead_id = $lead_ids;

        $teammember = $request->teammember;
        foreach($teammember as $key => $val){
            $team->team_members_id .= $val.',';
        }
        $member_ids = trim($team->team_members_id,',');
        $team->team_members_id = $member_ids;

        $team->team_status = 1;

        $now = Carbon::now();
        $team->team_created_on = $now;
        $team->team_updated_on = $now;
        $team->save();
        
        $teamlisting = Team::all();
        foreach($teamlisting as $key => $val){
            $names = '';
            $name = '';

            $team_member = '';
            $team_members = '';
            $teamUsers = explode(',',$val->team_lead_id);
            foreach($teamUsers as $keys => $value){
                $name = User::where('id',$value) 
                        ->select('name')
                        ->get();
                $names .= $name[0]->name.',';
                $teamlisting[$key]->team_lead_id = rtrim($names,',');
            }

            $teamMembers = explode(',',$val->team_members_id);
            foreach($teamMembers as $kes => $values){
                $team_member = User::where('id',$values) 
                        ->select('name')
                        ->get();
                $team_members .= $team_member[0]->name.',';
                $teamlisting[$key]->team_members_id = rtrim($team_members,',');
            }
        }
       
        return view('admin.defineteam',compact('user','teamlisting'));
    }
    
     public function vacancy()
    {
        $vacancy = DB::table('tbl_clients')
                    ->join('tbl_clientjd_master', 'tbl_clients.client_id','=','tbl_clientjd_master.clientjob_empid')
                    ->select('tbl_clients.comp_name','tbl_clientjd_master.*')
                    ->get(); 
        
        foreach($vacancy as $key => $val){
            $totalCount = DB::table('tbl_recruiter_cv')
                          ->select('*')
                          ->where('position_id',$val->clientjob_id)
                          ->count();
            $vacancy[$key]->totalCount = $totalCount;              
        }
        return view('admin.vacancylist',compact('vacancy'));
    }

    public function candidateList()
    {
        $candidate = DB::table('tbl_recruiter_cv')
                        ->join('users','users.id','=','tbl_recruiter_cv.recruiter_id')
                        ->join('tbl_clientjd_master', 'tbl_recruiter_cv.position_id','=','tbl_clientjd_master.clientjob_id')
                        ->join('tbl_clients','tbl_clients.client_id','=','tbl_clientjd_master.clientjob_empid')
                        ->select('tbl_clients.*','users.*','tbl_recruiter_cv.*','tbl_clientjd_master.*')
                        ->get(); 
        
        return view('admin.candidatelist',compact('candidate'));
    }






}