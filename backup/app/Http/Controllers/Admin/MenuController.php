<?php
/**
 * Created by PhpStorm.
 * User: mindz
 * Date: 29/1/18
 * Time: 2:05 PM
 */

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request as Request;
//use Request;
use Illuminate\Http\RedirectResponse;
use App\Model\Adminmodel\Menu;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Model\Adminmodel\Role as Role;

class MenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function addmenu()
    {

        return view('admin/addmenu');

    }

    public function savemenu(Request $request)
    {
        $this->validate($request,[
            'parentmenu' => 'required',
        ],[
            'parentmenu.required' => 'Parent menu field is required.',
        ]);

        $parentid=DB::table('tbl_menu')->insertGetId(
            [
                'menuname'=>$request['parentmenu'],

            ]);
        $child=$request['childmenu'];
        foreach ($child as $key => $value)
        {
            DB::table('tbl_menu')
                ->insert(
                    ['menuname'=>$value,
                        'menuparentid'=>$parentid
                    ]);
        }

        Session::flash('success_msg','Menu created successfully');
        return redirect()->route('addmenu');


    }

    public function assignmenu()
    {
        $role=Role::all();
        //dd($role);
        $menu=DB::table('tbl_menu as m1')
                  ->select('m1.*')->
                  where('m1.menuparentid','=','0')
                  ->get();
                 // dd($menu);
        //dd($menu);
        return view('admin.amenu',compact('role','menu'));

    }

    public function getmenubyrole(Request $request)
    {

        $role=$request['roleid'];
       // dd($roleid);
        $r=$request->all();
        //dd($role);
        $access=DB::table('tbl_menu_role_assigned as m1')
            ->join('tbl_menu as m2','m1.menuid','=','m2.id')
            ->select('m1.*','m2.menuname','m2.menuparentid')
            ->where(['m1.roleid'=>$role])
            ->get();
       // dd($access);

         $role=Role::all();


        return view('admin.viewmenubyrole',compact('role','access'));

    }

     public function getchildmenu($id)
    {

            $child=DB::table('tbl_menu as m')
                ->select('m.*')
                #->whereRaw('FIND_IN_SET('1,4',menuparentid)')
                ->whereIn('m.menuparentid', [$id])
                #->whereRaw('FIND_IN_SET(?, menuparentid)', [1,4])
                #->whereRaw(find_in_set('m.menuparentid','1'))
        #->whereRaw('FIND_IN_SET("$id", menuparentid)')->toSql();
                ->get();
             #echo $child;
       $str='';

       foreach ($child as $key=>$value) {
           $str .= '<option value="'.$value->id.'">'.$value->menuname.'</option>';
       }
     echo $str;

    }

    public function assignmbyrole(Request $request)
    {
         $this->validate($request,[
            'roleid' => 'required',
            'parentid' => 'required',
            'childid' => 'required',
            
        ],[
            'roleid.required' => 'Select Role ',
            'parentid.required' => 'Select Parent menu',
            'childid.required' => 'Select Child menu',
           
        ]);




        $id=array_merge($request['parentid'],$request['childid']);
        $roleid=$request['roleid'];

        foreach ($id as $value)
        {
            $count=DB::table('tbl_menu_role_assigned as m')
            ->where(['m.roleid'=>$roleid,'menuid'=>$value])
            ->count();
            //dd($count);
            if($count==0)
            {
                DB::table('tbl_menu_role_assigned')
                    ->insert(
                        ['roleid'=>$roleid, 'menuid'=>$value
                        ]);

            }


        }

        Session::flash('success_msg','Menu Assigned  successfully');
        return redirect()->route('assignmenu');



    }

    public function viewmenubyrole()
    {
        $menu=DB::table('tbl_menu_role_assigned as m1')
            ->join('tbl_menu as m2','m1.menuid','=','m2.id')
            ->select('m1.*','m2.menuname','m2.menuparentid')
            ->get();
       // dd($menu);
        $role=Role::all();
        return view('admin.viewmenubyrole',['menu'=>$menu,'role'=>$role]);
    }

}