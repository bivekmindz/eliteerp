<?php
/**
 * Created by PhpStorm.
 * User: mindz
 * Date: 29/1/18
 * Time: 2:05 PM
 */

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Model\Adminmodel\Menu;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\Model\Adminmodel\Role as Role;

class MenuController extends Controller
{
    public function addmenu()
    {
        return view('admin/addmenu');

    }

    public function savemenu(Request $request)
    {
        $this->validate($request,[
            'parentmenu' => 'required',
        ],[
            'parentmenu.required' => 'Parent menu field is required.',
        ]);

        $parentid=DB::table('tbl_menu')->insertGetId(
            [
                'menuname'=>$request['parentmenu'],

            ]);
        $child=$request['childmenu'];
        foreach ($child as $key => $value)
        {
            DB::table('tbl_menu')
                ->insert(
                    ['menuname'=>$value,
                        'menuparentid'=>$parentid
                    ]);
        }

        Session::flash('success_msg','Menu created successfully');
        return redirect()->route('addmenu');


    }

    public function assignmenu()
    {
        $role=Role::all();
        $menu=DB::table('tbl_menu as m1')->select('m1.*')->get();
        return view('admin.assignmenu',compact('role','menu'));

    }

    public function getmenubyrole(Request $request)
    {
       // echo ($a);
        $access = DB::table('tbl_menu as m')
            ->select('m.roleid','m.id','m.menuname','m.menuparentid','m.access_create','m.access_view','m.access_update','m.access_delete')
            ->where('roleid','=',$request['roleid'])
            ->get();
        $role=Role::all();

        $menu=DB::table('tbl_menu as m1')->select('m1.*')->get();
        $aa=$access->toArray();
        //dd($aa);
        return view('admin.assignmenu')->with(['access'=> $aa,'role'=>$role,'menu'=>$menu]);
        //return Redirect()->route('getmenubyrole',array('access'=>$access));


    }

}