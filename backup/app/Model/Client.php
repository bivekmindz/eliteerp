<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table="tbl_clients";
    protected $fillable=['comp_name','contact_name','phone','email'];

public function user()
   {
       return $this->belongsTo('App\Model\User','clientsuid','id');

   }

}
