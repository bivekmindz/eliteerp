<?php

namespace App\Model\Adminmodel;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table="tbl_department";
    protected $fillable=['dept_name','dept_status'];

}
