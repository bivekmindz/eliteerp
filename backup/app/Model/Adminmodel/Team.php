<?php

namespace App\Model\Adminmodel;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'tbl_team';
    public $timestamps = false;
    protected $fillable = ['id','team_name','team_lead_id','team_status',
    						'team_created_on','team_updated_on'];

}
