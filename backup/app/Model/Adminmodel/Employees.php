<?php

namespace App\Model\Adminmodel;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $table="users";
    protected $fillable=['role_id','name','emp_contactno','email','password','emp_doj','emp_dept','emp_role','emp_status'];

}
