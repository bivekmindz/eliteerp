@extends('layouts.app')

@section('content')
    <section class="login p-fixed d-flex text-center bg-primary common-img-bg">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12">
                    <div class="login-card card-block">
                        <form class="md-float-material" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            <div class="text-center">
                                <img src="images/logo.png"> </div>
                            <h3 class="text-center txt-primary">
                                Employee Login
                            </h3>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email">E-Mail Address</label>

                                <div class="md-input-wrapper">
                                    <input id="email" type="email" class="md-form-control" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" >Password</label>

                                <div class="md-input-wrapper">
                                    <input id="password" type="password" class="md-form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-6 col-xs-12">
                                    <div class="rkmd-checkbox checkbox-rotate checkbox-ripple m-b-25">
                                        {{--<label class="input-checkbox checkbox-primary">
                                            <input type="checkbox" id="checkbox">
                                            <span class="checkbox"></span>
                                        </label>--}}
                                        <div class="captions">{{--Remember Me--}}</div>

                                    </div>
                                </div>
                                <div class="col-sm-6 col-xs-12 forgot-phone text-right">
                                    <a href="{{ route('password.request') }}" class="text-right f-w-600"> Forget Password?</a>
                                </div>
                            </div>

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <input type="submit" class="btn btn-primary" name="Login" value="Login">

                                    </input>


                                </div>
                            </div>
                            <!-- <div class="card-footer"> -->
                            {{--<div class="col-sm-12 col-xs-12 text-center">--}}
                                {{--<span class="text-muted">Don't have an account?</span>--}}
                                {{--<a href="" class="f-w-600 p-l-5">Sign up Now</a>--}}
                            {{--</div>--}}

                            <!-- </div> -->
                        </form>
                        <!-- end of form -->
                    </div>
                    <!-- end of login-card -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>
@endsection
