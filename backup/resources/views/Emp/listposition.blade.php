@extends('Emp.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
             @inject('position', 'App\Component\CommonComponent')
       @php 
            $breadcrumb = $position->breadcrumbs();
            //print_r($breadcrumb); exit;
            //print_r($breadcrumb[0]->menuname); exit;
        @endphp
                <h4>Position Listing</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">{{$breadcrumb[0]->parentmenu}}</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">{{$breadcrumb[0]->menuname}}</a>
                    </li>
                </ol>
            </div>
        </div>



       
            <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">All Vacancies</h5>
                           
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Job Title</th>
                                            <th>Company Name</th>
                                            <th>Total Positions</th>
                                             <th>view Positon JD</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                           $i=1;
                                         @endphp
                                          @foreach($pos as $key=>$value)
                                        <tr class="table-active">
                                           
                                             <td>{{  $i++   }}</td>
                                            <td>{{ $value->clientjob_title }}</td>
                                            <td>{{ $value->comp_name }} </td>
                                            <td>{{ $value->clientjob_noofposition  }}</td>
                                            <td>
                                                <div class="btn-group btn-group-sm" style="float: none;">
                                                    <button type="button" onclick="upload_position_jd({{ $value->clientjob_id }});" class="btn btn-info btn-lg"
                                                            style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span>
                                                    </button>
                                                </div>
                                            </td>
                                        
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
             </div>
             
             
             
            
            </div>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Jd Position Details</h4>
                    </div>
                    <div class="modal-body" id="positionJd">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <script>
            function upload_position_jd(clientjob_id) {
                $.ajax({
                    url: 'positionjd',
                    data: { id:clientjob_id },
                    success: function(result){
                        $("#positionJd").html(result);
                    }
                });
                $('#myModal').modal('show');
            }
        </script>
@endsection



