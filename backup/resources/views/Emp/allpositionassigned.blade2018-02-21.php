@extends('Emp.layouts.master')

@section('content')
@if(!empty($getlists->clientreq_id))

<style>
  .ih{background-color: #fff;
      border: 1px solid #bdbdbd;
      /* padding: 15px 0px; */
      height: 34px;}
  .ihi{margin: 5px 0px 27px 13px;}
</style>
<div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Postion Resume Upload</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Postion </a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Postion List</a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
                @if(Session::has('success_msg'))
                    <div class="alert alert-success">
                        {{ Session::get('success_msg') }}
                    </div>
                @endif
            <div class="col-md-12">
            <div class="card">
                       <div class="card-header">
                       <h5 class="card-header-text">
                               <!-- <div style="display:none;" id="showtimer">Registration closes in <span id="time">60:00</span> minutes!</div> -->

                               {{--<div  id="showtimer"> Uploading Time will be over in  <span id="time">{{$sessionstart}}</span> minutes!</div>--}}

                               <a href="#">
                                   <button type="button" id="endTime" name="endTime" class="star_tim1" onclick="myFunctionendtime()">
                                       <i class="icofont icofont-clock-time"></i> End Time
                                   </button>
                               </a>

                               @if($sessionstart)
                          
                                   <a href="#">
                                       <button type="button" id="showtimer"  class="star_tim">
                                           <i class="icofont icofont-clock-time"></i> Uploading Time will be over in <span id="time"></span> minutes!</div>
                </button>
                </a>
                @else
                    <a href="#">
                        <button type="button" id="startTime" name="startTime" class="star_tim" onclick="myFunction()">
                            <i class="icofont icofont-clock-time"></i> Start Time
                        </button>
                    </a>
                    <a href="#">
                        <button type="button" id="showtimer" style="display:none;" class="star_tim">
                            <i class="icofont icofont-clock-time"></i>Uploading Time will be over in <span id="time">60:00</span> minutes!</div>
                </button>
                </a>
                @endif

                </h5>
                            </div>

                          <div>
                          
                                  <table class="table dt">
                                       <thead>
                                       <tr>
                                          
                                           <th>Candidate Name</th>
                                           <th>Candidate Email</th>
                                           <th>Candidate Mobile</th>
                                           <th>Resume</th>
                                           
                                       </tr>
                                       </thead>
                                       <tbody>
                                        @if(!empty($getuploadCv)) 
                              @foreach($getuploadCv as $key => $uploads)
                                      <tr class="table-active entry">
                                          <td>
                                          <div class="form-group required">
                                            {{ $uploads->candidate_name }}
                                         </div>
                                        </td>
                                        <td>
                                           <div class="form-group required">
                                            {{ $uploads->candidate_email }}
                                        </div>
                                        </td>
                                       <td>
                                       <label class="form-group">
                                           {{$uploads->candidate_mob}}
                                       </label>
                                       </td>
                                        <td>
                                           <div class="form-group required">
                                            {{ $uploads->cv_name }}
                                            </div>
                                        </td>                                               
                                           
                                       </tr>

                                       @endforeach
                          @endif 
                                       
                                      </tbody>
                                     </table>
                               
                            
                         </div>
                               
                               <div class="card-block">
                           <div class="row">
                               <div class="col-sm-12 table-responsive">
                                   <table class="table">
                                        <thead>
                                        <tr>
                                                <th>Company Name </th>
                                                <td>{{ !empty($getlists->comp_name) ? $getlists->comp_name : 'N/A'  }}</td>
                                                <th>Position Name</th>
                                                <td style="border-bottom: 2px solid #eceeef;">
                                                  <span class="posname">{{ !empty($getlists->clientjob_title) ? $getlists->clientjob_title : 'N/A' }}</span>
                                                  </td>
                                            </tr>
                                        <td colspan="2">
                                        <div class="arro_css">
                                            {{ !empty($getlists->upload_position_jd) ?  htmlspecialchars_decode($getlists->upload_position_jd)  : 'N/A'  }}
                                        </div>
                                        </td>
                                        </thead>
                                      
                                   </table>
                                    
                               <div class="md-input-wrapper flo op timeset" style="display: none">
                                  
                                <button type="submit" class="btn btn-primary waves-effect waves-light">
                                    <span class="icofont icofont-eye-alt"></span> Upload CV
                                </button>
                                </div>
                               </div>
                              
                           </div>
                           <div class="col-sm-12 table-responsive timeset" style="display: none">
                                 {!! Form::open([ 'action'=>'Emp\RecController@uploadcv', 'method'=>'post', 'files'=>true   ]) !!}
                                 
                                 
                        
                           <div class="controls">
                          
                                   <table class="table dt">
                                       <thead>
                                       <tr>
                                          
                                           <th>Name</th>
                                           <th>Phone No</th>
                                           <th>Email</th>
                                           <th>Upload Resume</th>
                                           <th></th>
                                           
                                       </tr>
                                       </thead>
                                       <tbody>
                                       
                                       
                                       <tr class="table-active entry">
                                           <!-- <td></td> -->
                                           <td>
                                           <div class="form-group required">
                                            <input type="text" name="candidate_name[]" class="form-control" placeholder="Enter First Name" pattern="[A-Za-z\s]+" required="" title="First Name should only contain  letters. e.g. John" maxlength="60">
                                            <!-- <input type="text" name="candidate_name[]" class="form-control" required> -->
                                            </div>
                                        </td>
                                          <td>
                                          <div class="form-group required">
                                            <input type="tel" name="candidate_mob[]" class="form-control" placeholder="Enter Your Mobile" pattern="^\d{10}$" required="" title="Your Phone Number Should Only Contain  Letters. e.g. 9999999999 and 10 Digit Number" maxlength="10">
                                         </div>
                                        </td>
                                        <td>
                                           <div class="form-group required">
                                            <input type="email" name="candidate_email[]" class="form-control" placeholder="Enter Your Email" required="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50">
                                        </div>
                                        </td>
                                       <td>
                                       <label class="custom-file ih">
                                           <input type="file" id="file" class="ihi" name="cv_name[]" required>
                                       </label>
                                       </td>
                                           <td><span class="add_field">
                            <button class="btn btn-success btn-add" type="button">
                                <span class="icofont icofont-plus"></span>
                            </button>
                        </span></td>
                                            
                                           
                                       </tr>

                                       
                                       
                                      </tbody>
                                     </table>
                                     <div class="md-input-wrapper dt">
                                   <button type="submit" class="btn btn-default waves-effect">Cancel
                                   </button>
                                    <!-- <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                                   </button> -->
                                   <input type="hidden" name="fkjdid" value="{{ !empty($getlists->fk_jdid) ? $getlists->fk_jdid : '0'  }}">
                                   <input type="hidden" name="clientreqid" value="{{ !empty($getlists->clientreq_id) ? $getlists->clientreq_id : '0' }}">

                                   @php
                                   $clientEncryptId = Crypt::encrypt($getlists->clientreq_id); 
                                   @endphp
                                   <input type="hidden" name="clientreqEncrypt" value="{{ !empty($clientEncryptId) ? $clientEncryptId : '0' }}">

                                   <input type="submit" class="btn btn-primary waves-effect waves-light" value="submit">
                                  
                               </div>
                                    </div>
                                    {!! Form::close() !!}
                               </div>
                               
                               
                       </div>
                       </div>
                       
                       
                       
                       
                    
                   </div>
                   
                   
            </div>
            
            
            
            <div class="row">
           
            <div class="col-md-12">
            
            <div class="card" >
            
            
            
                       <div class="card-header">
                           <h5 class="card-header-text">Client Preview</h5>
                          
                       </div>
                       <div class="card-block">
                           <div class="row">
                               <div class="col-sm-12 table-responsive">
                                   <table class="table">
                                       
                                        <tr>
   <th scope="row" style="width:23%;">Company Name</th>
   <td>{{ !empty($getlists->comp_name) ? $getlists->comp_name : 'N/A ' }}</td>
 </tr>
 <tr>
   <th scope="row">Company URL</th>
   <td>{{ !empty($getlists->company_url) ? $getlists->company_url : 'N/A' }}</td>
 </tr>
 <tr>
   <th scope="row" >Company Size </th>
   <td>+{{ !empty($getlists->company_size) ? $getlists->company_size : 'N/A' }}</td>
 </tr>
 
 <tr>
 
   <td colspan="2">
   
    <div class="md-input-wrapper flo">
                                  
        <button type="submit" class="btn btn-primary waves-effect waves-light"><span class="icofont icofont-eye-alt"></span> View More
        </button>
    </div>
   </td>
 </tr>
    </table>
<table class="table hid" style="">
<tr>
   <th scope="row" style="width:23%;">Timings</th>
   <td>{{ !empty($getlists->from_time) ? $getlists->from_time : 'N/A' }} To  {{ !empty($getlists->to_time) ? $getlists->to_time : 'N/A' }}</td>
 </tr>
 <tr>
   <th scope="row">Days Working</th>
   <td>{{ !empty($getlists->days_working) ? $getlists->days_working : 'N/A' }}</td>
 </tr>
 <tr>
   <th scope="row">Industry </th>
   <td>{{ !empty($getlists->industry) ? $getlists->industry : 'N/A' }}</td>
 </tr>
  <tr>
   <th scope="row">Company’s Specialties</th>
   <td>{!! !empty($getlists->selling_point) ? $getlists->selling_point : 'N/A' !!}</td>
 </tr>
 
  <tr>
   <th scope="row">Headquarters</th>
   <td>{{ !empty($getlists->headquarters) ? $getlists->headquarters : 'N/A' }}</td>
 </tr>
   <tr>
   <th scope="row">Office Address</th>
   <td>{{ !empty($getlists->office_address) ? $getlists->office_address : 'N/A' }}</td>
 </tr>
    </table>
                                   
                                   
                 </div>
                 
                 
             </div>
             
             
         </div>
     </div>
</div>

</div>

</div>
           
  
<script>
    function stopTime()
    {
        $('#showtimer').hide();
        $('#stopTime').hide();
    }
</script>
        <script>
     function myFunction()
    {
        setTimeout(function(){
            alert('start uploading form');
            $('#startTime').hide();
            $.ajax({
              type:"GET",
              //  url:'http://127.0.0.1:8000/gettime/1', 
             url:'gettime/{{ $getlists->clientreq_id }}',
             // http://127.0.0.1:8000/get-assigned-postion/gettime/1 
              data:'',
             // dataType: 'json',
              success: function(data){
              location.reload(true);
              },
              error: function(data){

              }
            })
           //location.reload(true);
        });
    }

    function myFunctionendtime()
    {
        setTimeout(function(){
            alert('end uploading form');
            $('#endTime').hide();
            $.ajax({

                type:"GET",
              //  url:'http://127.0.0.1:8000/endtime/1',
               url:'endtime/{{ $getlists->clientreq_id }}',
                data:'',
              //  dataType: 'json',
                success: function(data){
                   location.reload(true);
                },
                error: function(data){

                }
            })
            //  $('#showtimer').show();
            //   $('#stopTime').show();
          // location.reload(true);
        });
    }
</script>
<script type="text/javascript">
$(function(){

    $('#uploadcv').on('submit',function(e){
        $.ajaxSetup({
            header:$('meta[name="_token"]').attr('content')
        })
        e.preventDefault(e);

            $.ajax({

            type:"POST",
            url:'/uploadcv',
            data:$(this).serialize(),
            dataType: 'json',
            success: function(data){
                console.log(data);
            },
            error: function(data){

            }
        })
    });
}); 
</script>


<script>
function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

window.onload = function () {
var fiveMinutes =JSON.parse({{$sessionstart}});
        display = document.querySelector('#time');
    startTimer(fiveMinutes, display);

    var a=$('#time').text();
    //alert(a);
    if(a=='')
    {
      $('.timeset').css('display','block');
      $('.posNavName').removeAttr("href");
    }
};
</script>
@else
<div class="container-fluid">
<div class="row">
<div class="main-header">
<h4>Postion Resume Upload</h4>
<ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
<li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
</li>
<li class="breadcrumb-item"><a href="#">Postion </a>
</li>
<li class="breadcrumb-item"><a href="">Postion List</a>
</li>
</ol>
</div>
</div>

<div class="row">
<!-- @if(Session::has('error'))
<div class="alert alert-danger">
{{ Session::get('error') }}
</div>
@endif -->
<div class="col-md-12">
<div class="card">
No ID Found
</div>
</div>
</div>
</div>
                
                
@endif
@endsection