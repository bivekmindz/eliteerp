@extends('Emp.layouts.master')
@section('content')
<style>
    .error{
      color:red;
    }
    .form-control.error{
      color:black;
    }
</style>
<head>
    <meta charset="UTF-8">
    <title>bootstrap4</title>
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.7.0/summernote.css" rel="stylesheet"> -->
</head>

<div class="container-fluid">
    <div class="row">
    <div class="main-header">
     @inject('position', 'App\Component\CommonComponent')
       @php 
            $breadcrumb = $position->breadcrumbs();
            //print_r($breadcrumb); exit;
            //print_r($breadcrumb[0]->menuname); exit;
        @endphp
    <h4>Add Position</h4>
    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
        <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
        </li>
        <li class="breadcrumb-item"><a href="javascript:void(0)">{{$breadcrumb[0]->parentmenu}}</a>
        </li>
        <li class="breadcrumb-item"><a href="">{{$breadcrumb[0]->menuname}}</a>
        </li>
    </ol>
    </div>
</div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
               <div class="card-header"><h5 class="card-header-text">Add Position Form</h5>

               </div>
               @if(Session::has('success_msg'))
                  <div class="alert alert-success">
                     {{ Session::get('success_msg') }}
                  </div>
            @endif

            <div class="card-block">
               <!--{!! Form::open([ 'action'=>'Emp\PositionController@addposition', 'method'=>'post','name'=>'position', 'files'=>true ]) !!}-->
          <form method="post" name="position" action="{{ URL::to('addposition') }}" enctype="multipart/form-data">
                    <div class="row">
                        @if(count($errors))
                           <div class="alert alert-danger">
                              <strong>Whoops!</strong> There were some problems with your input.
                              <br/>
                              <ul>
                                 @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                 @endforeach
                              </ul>
                           </div>
                        @endif
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Type<span class="error"> * </span> </label>
                              <div class="col-sm-8">
                                 <select class="form-control" id="dept" name="dept">
                                       <option value="">Please Select Department</option>
                                    @foreach($departments as $kk => $value)
                                       <option value="{{$value->dept_id}}">{{$value->dept_name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>


                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Company Name<span class="error"> * </span></label>
                              <div class="col-sm-8">
                                 <select class="form-control" id="compname" name="compname">
                                       <option value="">Please Select Company</option>
                                    @foreach($client as $kk => $value)
                                       <option value="{{$value->fk_clientid}}">{{$value->comp_name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>

                     </div>

                     <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Position Title <span class="error"> * </span></label>
                              <div class="col-sm-8">
                                <input class="form-control" type="text" value="" id="jobtitle" name="jobtitle">
                               </div>
                           </div>
                        </div>

                        <!-- <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Upload JD</label>
                              <div class="col-sm-8">
                                {!! Form::file('upload_position_jd', '', ['id'=>'file', 'class'=>'custom-file-input']) !!} -->
                                <!-- <input type="file" id="file" class="custom-file-input" name="upload_jd">
                                <span class="custom-file-control"></span> -->
                                <!-- </label>
                              </div>
                           </div>
                        </div> -->

                        <!-- <div class="col-lg-6">
                            <div class="form-group row">
                                <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Add JD</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control ckeditor" id="exampleTextarea" rows="3" name="upload_position_jd"></textarea>
                                </div>
                            </div>
                        </div> -->
                    </div>

                     <div class="row">

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Number of Positions<span class="error"> * </span> </label>
                              <div class="col-sm-8">
                                 <input class="form-control" type="text" value="" id="noofpos" name="noofpos">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">

                                 <div class="checkbox-color checkbox-success ">
                                    <input id="checkbox3" type="checkbox" name="drive">
                                    <label for="checkbox3">
                                       Drive
                                    </label>
                                 </div>
                              </label>
                                <div class="col-sm-8">
                                        <div class="form-group">
                                            <div class="input-group date" id="datetimepicker1">
                                            <input type="text" class="form-control" placeholder="Drive Date" name="drivedate">
                                            <span class="input-group-addon">
                                                    <span class="icofont icofont-ui-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                </div>
                           </div>
                        </div>
                        <!-- <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Other Information </label>
                              <div class="col-sm-8">
                                 <textarea class="form-control" id="exampleTextarea" rows="1" name="otherinfo"></textarea>
                              </div>
                           </div>
                        </div> -->

                     </div>
                    <div class="row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Add JD<span class="error"> * </span> </label>
                    </div>  
                    <div class="row">
                    <textarea name="upload_position_jd" id="summernote" class="summernote form-control"></textarea>
                    </div> 
                     <!-- <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">

                                 <div class="checkbox-color checkbox-success ">
                                    <input id="checkbox3" type="checkbox" name="drive">
                                    <label for="checkbox3">
                                       Drive
                                    </label>
                                 </div>
                              </label>
                              <div class="col-sm-8">

                                 <div class="form-group">
                                    <div class="input-group date" id="datetimepicker1">
                                       <input type="text" class="form-control" placeholder="Drive Date" name="drivedate">
                                       <span class="input-group-addon">
                        <span class="icofont icofont-ui-calendar"></span>
                    </span>
                                    </div>
                                 </div>


                              </div>
                           </div>
                        </div> -->



                     </div>

                     <div class="md-input-wrapper">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                        </button>
                     </div>
                    <!-- {!! Form::close() !!} -->
                    </form>
               </div>
            </div>
         </div>
         <!-- Textual inputs ends -->
      </div>

   </div>
   </div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>

<script>
    $('#summernote').summernote();

    // $('#summernote').summernote({
    //     placeholder: 'Hello bootstrap 4',
    //     tabsize: 2,
    //     height: 100
    //   });
</script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script>
    $(function() {
        $("form[name='position']").validate({
            ignore: [],
            rules: {
              dept: "required",
              compname: "required",
              jobtitle: "required",
              noofpos: "required",
              upload_position_jd: "required",
            },
            messages: {
              dept: "Please enter department.",
              compname: "Please enter company name.",
              jobtitle: "Please enter company url.",
              noofpos: "Please enter number of position.",
              upload_position_jd: "Please enter upload position jd.",
            },
            submitHandler: function(form) {
              form.submit();
            }
        });
    });

    $(document).ready(function() {
      $("#noofpos").keydown(function (e) {
          if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
              (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
              (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
          }
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
              e.preventDefault();
          }
      });
    });
</script>
@endsection
