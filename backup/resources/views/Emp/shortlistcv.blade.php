@extends('Emp.layouts.master')

@section('content')


    <!-- Container-fluid starts -->
    <!-- Main content starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Shortlist Profile</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href=""><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Positions</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Shortlisting of CV</a>
                    </li>
                </ol>
            </div>



        </div>

        <div class="row">

            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">SPOC Shortlisting</h5>

                    </div>
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{ Session::get('success') }}
                        </div>
                    @endif
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Position Name</th>
                                        <th>No. Of Positions</th>
                                        <th>Total Uploaded Profile</th>

                                        <th>View</th>
                                        <th></th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    @php
                                        $i=1;
                                    @endphp
                                    @foreach($pos as $key=>$value)
                                    <tr >
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $value->clientjob_title }}
<input type="hidden" value="{{$value->clientjob_title}}"  id="pos">
                                        </td>
                                        <td>{{ $value->clientjob_noofposition }}</td>
                                        <td>{{ $value->total_upload_profile }}</td>

                                        <td><button type="button" value="{{ $value->position_id  }}" class="det_css one-click " onclick="getcandidatedetails(this.value),getposition('<?php echo $value->clientjob_title ?>')" ><span class="icofont icofont-eye-alt"></span></button> </td>



                                    </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="row">

            <div class="col-md-12">

                <div class="card hide_css">
                    <div class="card-header">
                        <h5 class="card-header-text">All Candidate List</h5>

                    </div>
                    {!! Form::open([ 'action'=>'Emp\CVController@savecandidate', 'method'=>'post', 'files'=>true ]) !!}
                    <div class="card-block">
                        <div class="row" >
                            <div class="col-sm-12 table-responsive">
                                <table class="table"  >
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Candidate's Name</th>
                                        <th>Mobile Number</th>
                                        <th>Download Resume</th>
                                        <th>Status</th>

                                    </tr>
                                    </thead>
                                    <tbody id="candilist">
                                    
                                    </tbody>
<input type="hidden" value="" id="poid" name="poid">
                                </table>
                               
                            </div>


                        </div>
                        <div class="md-input-wrapper">

                            <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit">
                            </input>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>

        </div>





    </div>
    <script>

     function getposition(id){
        var ids  = id;
       $('#poid').val(ids);

     }

        function getcandidatedetails(val)
        {
          
          
           var  id=val;

            $.ajax({
                url: 'getcandidatedetails/'+id,
                type:'GET',
                error: function(xhr) {
                    alert("An error occured: " + xhr.status + " " + xhr.statusText);
                },
                success: function(data)
                {
                   var data1= JSON.parse(data);
                   // console.log(data.length);

                    var trHTML = '';
                    var j=1;


                  //  $.each(data, function (i, data) {
                        for (i = 0; i < data1.length; i++) {
                             var str = data1[i]["created_at"];
                            var str = str.split(" ");
                            var aa =str[0].split("-");

                            var con="{{ config('constants.APP_URLS') }}";

                           var url=con+'/storage/app/uploadcv/'+aa[0]+"/"+aa[1]+"/"+aa[2]+"/"+data1[i]["cv_name"];


                            trHTML += "<tr>" +
                                "<td>"+j+++"</td>" +
                                "<td>"+data1[i]["candidate_name"]+"</td>" +
                                "<td>"+data1[i]["candidate_mob"]+"</td>" +
                                '<td><a href='+url+' download>                        ' +
                                'Download </a></td>' +
                                '<td> ' +
                                ' <div class="form-check">\n' +
                                ' <div >\n' +
                                '<input id="checkbox1" type="checkbox" name= "checkbox_data[]" value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'>\n' +
                                '<input type="hidden" name= "uncheckbox_data[]" value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'>\n' +
                                ' <label for="checkbox1" class="text-primary che_fon">\n' +
                                '                                                        Sent to Client\n' +
                                '                                                    </label>\n' +
                                '                                                </div>\n' +
                                '                                            </div></td>' +
                                '</tr>';
                        }
             //      });

      $('#candilist').html(trHTML);




                }




            });

        }
    </script>
    <script type="text/javascript">

            $("input:checkbox:not(:checked)").each(function () {
                alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
            });

    </script>

    @endsection