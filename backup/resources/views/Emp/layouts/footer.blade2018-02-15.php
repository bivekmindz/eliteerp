<script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ URL::asset('js/tether.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('js/waves.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
<script src="{{ URL::asset('js/menu.js') }}"></script>
<script>
$(function() {
$('.multiselect-ui').multiselect({
includeSelectAllOption: true,
enableCaseInsensitiveFiltering:true,
});
});
</script>

<script>
    $(".one-click").click(function(){
        $(".hide_css").slideToggle(700);
    });
</script>
<script src="{{ URL::asset('js/sel.js') }}"></script><script>
                $(function()
                {
                    $(document).on('click', '.btn-add', function(e)
                    {
                        e.preventDefault();

                        var controlForm = $('.controls form:first'),
                            currentEntry = $(this).parents('.entry:first'),
                            newEntry = $(currentEntry.clone()).appendTo(controlForm);

                        newEntry.find('input').val('');
                        controlForm.find('.entry:not(:last) .btn-add')
                            .removeClass('btn-add').addClass('btn-remove')
                            .removeClass('btn-success').addClass('btn-danger')
                            .html('<span class="icofont icofont-minus"></span>');
                    }).on('click', '.btn-remove', function(e)
                    {
                        $(this).parents('.entry:first').remove();

                        e.preventDefault();
                        return false;
                    });
                });

            </script>
            
            <script>
                $(function()
                {
                    $(document).on('click', '.btn-add', function(e)
                    {
                        e.preventDefault();

                        var controlForm = $('.controls table:first'),
                            currentEntry = $(this).parents('.entry:first'),
                            newEntry = $(currentEntry.clone()).appendTo(controlForm);

                        newEntry.find('input').val('');
                        controlForm.find('.entry:not(:last) .btn-add')
                            .removeClass('btn-add').addClass('btn-remove')
                            .removeClass('btn-success').addClass('btn-danger')
                            .html('<span class="icofont icofont-minus"></span>');
                    }).on('click', '.btn-remove', function(e)
                    {
                        $(this).parents('.entry:first').remove();

                        e.preventDefault();
                        return false;
                    });
                });

            </script>

<script>
$(".flo").click(function(){
    $(".hid").slideToggle(700);
});
</script>

<script>
$(".op").click(function(){
    $(".dt").slideToggle(700);
});
</script>

<script>
$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});
</script>
