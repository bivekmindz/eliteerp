
<!DOCTYPE html>
<html lang="en">

<head>
 @include('Emp.layouts.head')
</head>
<body class="sidebar-mini fixed" onload="show();">
@include('Emp.layouts.header')
            <!-- Sidebar Menu-->
@include('Emp.layouts.nav')
    <!-- Sidebar chat end-->

<div class="content-wrapper">
        @yield('content')
</div>
@include('Emp.layouts.footer')

</body>


</html>
