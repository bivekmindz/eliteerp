
<ul class="sidebar-menu">
    <li class="nav-level">Dashboard</li>
    
    @inject('userRole','App\Component\CommonComponent')
    @php 
        $roles = $userRole->getRole();
        
        $ones = '';
        $two = '';
        $three = '';
        
        if (strpos($roles->emp_role,',') !== false) {
            $emp_role = explode(',',$roles->emp_role);
            if(in_array(1,$emp_role))
            {
               $ones = 1;
            }else{
               $ones = '';
            }

            if (in_array(2,$emp_role))
            {
               $two = 2;
            }else{
               $two = '';
            }

            if (in_array(3,$emp_role))
            {
               $three = 3;
            }else{
               $three = '';
            }
        }else{
            $ones = Auth::user()->emp_role;
            $two = Auth::user()->emp_role;
            $three = Auth::user()->emp_role;
        }
    @endphp
     
    @if($ones == 1)
   @php
       $bdn = $userRole->nav($ones);
   @endphp

@include('Emp.layouts.nav.bd', ['nav' => $bdn])
        {{--@include('Emp.layouts.nav.bd')--}}
    @endif
    
    @if($two == 2)
@php
    $spocn = $userRole->nav($two);
@endphp

@include('Emp.layouts.nav.bd', ['nav' => $spocn])
        {{--@include('Emp.layouts.nav.spoc')--}}
    @endif

    @if($three == 3)
@php
    $recn = $userRole->nav($three);

@endphp

@include('Emp.layouts.nav.bd',array('nav' => $recn))
        @include('Emp.layouts.nav.recruiter')

    @endif

</ul>
</section>
</aside>
<!-- Sidebar chat start -->

<div class="showChat_inner">
    <div class="media chat-inner-header">
        <a class="back_chatBox">
            <i class="icofont icofont-rounded-left"></i> Josephin Doe
        </a>
    </div>
    <div class="media chat-messages">
        <a class="media-left photo-table" href="#!">
            <img class="media-object img-circle m-t-5" src="assets/images/avatar-1.png" alt="Generic placeholder image">
            <div class="live-status bg-success"></div>
        </a>
        <div class="media-body chat-menu-content">
            <div class="">
                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                <p class="chat-time">8:20 a.m.</p>
            </div>
        </div>
    </div>
    <div class="media chat-messages">
        <div class="media-body chat-menu-reply">
            <div class="">
                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                <p class="chat-time">8:20 a.m.</p>
            </div>
        </div>
        <div class="media-right photo-table">
            <a href="#!">
                <img class="media-object img-circle m-t-5" src="assets/images/avatar-2.png" alt="Generic placeholder image">
                <div class="live-status bg-success"></div>
            </a>
        </div>
    </div>
    <div class="media chat-reply-box">
        <div class="md-input-wrapper">
            <input type="text" class="md-form-control" id="inputEmail" name="inputEmail" >
            <label>Share your thoughts</label>
            <span class="highlight"></span>
            <span class="bar"></span>  <button type="button" class="chat-send waves-effect waves-light">
                <i class="icofont icofont-location-arrow f-20 "></i>
            </button>

            <button type="button" class="chat-send waves-effect waves-light">
                <i class="icofont icofont-location-arrow f-20 "></i>
            </button>
        </div>

    </div>
</div>
<!-- Sidebar chat end-->






























