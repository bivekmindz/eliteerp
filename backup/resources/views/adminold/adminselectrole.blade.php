@extends('admin.layouts.master')

@section('content')
    <div class="row">

    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Role Listing</h5>

            </div>
            <div class="card-block">
                <div class="row">
                <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                        <div class="pull-right;">{{ $roles->links() }}</div>              
                        </div>
                    </div>

                    <div class="col-sm-12 table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Role Name</th>



                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                                  $temp = $roles ; 
                            @endphp
                            @foreach($roles as $kk => $roles)
                            <tr class="table-active">


                                <td> {{  $i++   }}</td>

                                <td>{{  $roles->role_name }}</td>

                               </tr>

                            @endforeach

                            </tbody>
                        </table>

                          <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                        <div class="pull-right;">{{ $temp->links() }}</div>              
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
    @endsection