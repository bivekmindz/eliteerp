
<!DOCTYPE html>
<html lang="en">

<head>
 @include('admin.layouts.head')
</head>
<body class="sidebar-mini fixed">
@include('admin.layouts.header')
            <!-- Sidebar Menu-->
@include('admin.layouts.nav')
    <!-- Sidebar chat end-->

<div class="content-wrapper">

    <!-- Container-fluid starts -->
    <!-- Main content starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Dashboard</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Forms</a>
                    </li>
                    <li class="breadcrumb-item"><a href="general-elements-bootstrap.html">General Elements</a>
                    </li>
                </ol>
            </div>



        </div>

        @yield('content')
</div>
@include('admin.layouts.footer')

</body>


</html>
