@extends('admin.layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Candidate Resume Upload </h5>
            </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Position Name</th>
                                    <th>Client Name</th>
                                    <th>Target</th>
                                    <th>View JD</th>
                                    <th>Company Profile</th>
                                    <th>Upload Profile</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i=1;
                                @endphp
                                @foreach( $getlists as $key => $list )
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ !empty($list->clientjob_title) ? $list->clientjob_title : 'N/A' }}</td>
                                        <td>{{ !empty($list->comp_name) ? $list->comp_name : 'N/A' }}</td>
                                        <td>5</td>
                                        <td>
                                            <button type="button" class="det_css " ><span class="icofont icofont-eye-alt"></span></button>
                                        </td>
                                        <td>
                                            <button type="button" class="det_css add_io1 " ><span class="icofont icofont-download-alt"></span></button>
                                        </td>
                                        <td>
                                            <button type="button" class="one-click add_io" ><span class="icofont icofont-medical-sign"></span></button>
                                        </td>
                                        <td>
                                            <button type="button" class="star_tim">Start Time </button>
                                        </td>
                                    </tr>
                                @endforeach    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card hide_css">
            <div class="card-header">
                <h5 class="card-header-text">Client Listing</h5>
            </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Name</th>
                                    <th>Phone No</th>
                                    <th>Upload Resume</th>
                                    <th><button type="button" class="btn-add btn btr btn-xs">Add</button></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr class="table-active">
                                    <td>1</td>
                                    <td><div class="form-group required">
                    <input type="text" class="form-control">
                </div></td>
                                    <td><div class="form-group required">
                    <input type="text" class="form-control">
                </div></td>
                                    <td><label for="file" class="custom-file" >
                                        <input type="file" id="file" class="custom-file-input">
                                        <span class="custom-file-control"></span>
                                    </label></td>
                                    <td><button type="button" class="btn-remove btn tra btn-xs"><i class="icofont icofont-ui-delete fa-2x" aria-hidden="true"></i></button></td>
                                    
                                    
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td><div class="form-group required">
                    <input type="text" class="form-control">
                </div></td>
                                    <td><div class="form-group required">
                    <input type="text" class="form-control">
                </div></td>
                                    <td><label for="file" class="custom-file" >
                                        <input type="file" id="file" class="custom-file-input">
                                        <span class="custom-file-control"></span>
                                    </label></td>
                                    <td><button type="button" class="btn-remove btn tra btn-xs"><i class="icofont icofont-ui-delete fa-2x" aria-hidden="true"></i></button></td>
                                    
                                    
                                </tr>
                                <tr class="table-success">
                                    <td>3</td>
                                    <td><div class="form-group required">
                    <input type="text" class="form-control">
                </div></td>
                                    <td><div class="form-group required">
                    <input type="text" class="form-control">
                </div></td>
                                    <td><label for="file" class="custom-file" >
                                        <input type="file" id="file" class="custom-file-input">
                                        </div>                                    <span class="custom-file-control"></span>
                                    </label></td>
                                    <td><button type="button" class="btn-remove btn tra btn-xs"><i class="icofont icofont-ui-delete fa-2x" aria-hidden="true"></i></button></td>
                                    
                                    
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td><div class="form-group required">
                    <input type="text" class="form-control">
                </div></td>
                                    <td><div class="form-group required">
                    <input type="text" class="form-control">
                </div></td>
                                    <td><label for="file" class="custom-file" >
                                        <input type="file" id="file" class="custom-file-input">
                                        <span class="custom-file-control"></span>
                                    </label></td>
                                    <td><button type="button" class="btn-remove btn tra btn-xs"><i class="icofont icofont-ui-delete fa-2x" aria-hidden="true"></i></button></td>
                                    
                                </tr>
                                
                                </tbody>
                            </table>
                        </div>
                        
                        
                    </div>
                    
                    <div class="md-input-wrapper">
                        <button type="submit" class="btn btn-default waves-effect">Cancel
                        </button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                        </button>
                    </div>
                </div>
            </div>
    </div>
</div>

@endsection
