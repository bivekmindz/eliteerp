@extends('admin.layouts.master')
@section('content')
<div class="row">
   <!-- Textual inputs starts -->
   <div class="col-lg-12">
      <div class="card">
         <div class="card-header">
            <h5 class="card-header-text">User Creation Form</h5>
         </div>
         <!-- end of modal -->
         <div class="card-block">
            <form  action="{{ route('admin.usersave')  }}"  method="POST" >
              @if(count($errors))
                <div class="alert alert-danger">
                  <strong>Whoops!</strong> There were some problems with your input.
                  <br/>
                  <ul>
                      @foreach($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
              @endif
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
               <div class="row">
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Name</label>
                        <div class="col-sm-8">
                           <input class="form-control" type="text" value="" id="example-text-input" name="name" required>
                           <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Contact Number</label>
                        <div class="col-sm-8">
                           <input class="form-control" type="text" value="" id="example-text-input" name="phone" required>
                           <span class="text-danger">{{ $errors->first('phone') }}</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Email</label>
                        <div class="col-sm-8">
                           <input class="form-control" type="text" value="" id="example-text-input" name="email" required>
                           <span class="text-danger">{{ $errors->first('email') }}</span>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Password</label>
                        <div class="col-sm-8">
                           <input class="form-control" type="password" value="" id="example-text-input" name="password" required>
                           <span class="text-danger">{{ $errors->first('password') }}</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Confirm Password </label>
                        <div class="col-sm-8">
                           <input class="form-control" type="password" value="" id="example-text-input" name="cpassword" required>
                           <span class="text-danger">{{ $errors->first('cpassword') }}</span>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Data of Joining</label>
                        <div class="col-sm-8">
                           <input class="form-control" type="date" value="" id="example-text-input" name="doj" class="document" required>
                           <span class="text-danger">{{ $errors->first('doj') }}</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Department </label>
                        <div class="col-sm-8">
                           <select class="form-control " id="exampleSelect1" name="dept" required>
                            @foreach($departments as $kk => $val)
                              <option value="{{$val->dept_id}}">{{$val->dept_name}}</option>
                            @endforeach
                           </select>
                           <span class="text-danger">{{ $errors->first('dept') }}</span>
                        </div>
                     </div>
                  </div>

                  
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Role </label>
                        <div class="col-sm-8">
                           <div class="form-group">


                              <select id="dates-field2" class="multiselect-ui form-control" multiple="multiple" name="role[]" required>
                                  @foreach($roles as $kk => $roles)
                                  <option value="{{ $roles->role_id  }}">{{ $roles->role_name }}</option>
                                  @endforeach
                              </select>
                              <span class="text-danger">{{ $errors->first('role') }}</span>                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="md-input-wrapper">
                  <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit" onclick="return Validate()">
                  </input>
               </div>
            </form>
         </div>
      </div>
   </div>
   <!-- Textual inputs ends -->
</div>
</div>
</div>

@endsection
