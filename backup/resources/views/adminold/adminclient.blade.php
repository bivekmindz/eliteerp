@extends('admin.layouts.master')

@section('content')
    <div class="row">

    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Client Listing</h5>

            </div>
            <div class="card-block">
                <div class="row">

                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                             <div class="pull-right;">{{ $clients->links() }}</div>               
                        </div>
                    </div>

                    <div class="col-sm-12 table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Category</th>
                                <th>Company Name</th>
                                <th>Contact Person Name</th>
                                <th>Designation</th>
                                <th>Phone</th>
                                <th>Email</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                                $temp = $clients ;
                            @endphp
                            @foreach($clients as $kk => $clients)
                            <tr class="table-active">


                                <td> {{  $i++   }}</td>
                                <td> @if($clients->client_catid==1)
                                         {{ "IT" }}
                                         @else
                                          {{ "NON IT"  }}
                                         @endif


                                </td>
                                <td>{{  $clients->comp_name }}</td>
                                <td>{{  $clients->contact_name }}</td>
                                <td>{{ $clients->designation }}</td>
                                <td>{{  $clients->phone }}</td>
                                <td>{{ $clients->email }}</td>
                               </tr>

                            @endforeach

                            </tbody>
                        </table>
<div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                            <div class="pull-right;">{{ $temp->links() }}</div>                
                        </div>
                    </div>                    

                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
    @endsection