@extends('admin.layouts.master')

@section('content')
    <div class="row">

    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Department  Listing</h5>

            </div>
            <div class="card-block">
                <div class="row">

                <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                        <div class="pull-right;">{{ $departments->links() }}</div>              
                        </div>
                    </div>
                    <div class="col-sm-12 table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Department Name</th>



                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                                    $temp = $departments ; 
                            @endphp
                            @foreach($departments as $kk => $departments)
                            <tr class="table-active">


                                <td> {{  $i++   }}</td>

                                <td>{{  $departments->dept_name }}</td>

                               </tr>

                            @endforeach

                            </tbody>
                        </table>

                       <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                        <div class="pull-right;">{{ $temp->links() }}</div>              
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
    @endsection