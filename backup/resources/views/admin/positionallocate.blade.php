@extends('admin.layouts.master')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               <div class="main-header">
            <h4>Positions</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
               <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
               </li>
               <li class="breadcrumb-item"><a href="javascript:void(0)">Position</a>
               </li>
               <li class="breadcrumb-item"><a href="">Allocate Position to Recuiter</a>
               </li>
            </ol>
         </div>
                @if(Session::has('success_msg'))
                    <div class="alert alert-success">
                        {{ Session::get('success_msg') }}
                    </div>
                @endif
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Position Name</th>
                                    <th>Company Name</th>
                                    <th>Drive</th>
                                    <th>No Of Positions</th>
                                    <th>SPOC</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i=1;
                                @endphp

                                @if(!empty($positions))
                                @foreach($positions as $key => $p)
                                    <tr class="table-active">
                                        <td>{{ $i++ }}</td>
                                        <td>{{ !empty($p->clientjob_title) ? $p->clientjob_title : 'N/A' }}</td>
                                        <td>{{ !empty($p->comp_name) ? $p->comp_name : 'N/A' }}</td>
                                        <td>N/A</td>
                                        <td>{{ !empty($p->clientjob_noofposition) ? $p->clientjob_noofposition : 'N/A' }}</td>
                                        <td>{{ !empty($p->name) ? $p->name : 'N/A' }}</td>
                                        <td>
                                            <div class="btn-group btn-group-sm" style="float: none;">
                                                <button type="button" onclick="assignposition({{$p->clientjob_id}},'{{$p->clientjob_title}}')" href="#success" data-toggle="modal" class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;">
                                                    <span class="icofont icofont-ui-edit"></span>
                                                </button>
                                            </div>
                                        </td>    
                                        </tr>

                                        @endforeach
                                        @endif
                </tbody>
                </table>
                        </div>
                    </div>
                </div>
                
                
                
            </div>
        </div>
    </div>
    </div>
@if(!empty($p))
@foreach($positions as $key => $p)
    
<div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h1><i class="glyphicon glyphicon-thumbs-up"></i>Assign RECRUITER</h1>
                </div>
                <div class="modal-body">
                    {!! Form::open(array('action' => "Admin\RecruiterController@postRecruiter", 'method' => 'post')) !!}
                    <div class="form-group row">
                        <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Position Name</label>
                        <div class="col-sm-9">
                            <input class="form-control" id="pop_company_name" name="clientjob_title" type="text" value="{{$p->clientjob_title}}" id="example-text-input">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Assign Recruiter</label>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <select id="ajaxhr" multiple="multiple"  name="assignposition[]" class="multiselect-ui form-control " required>
                                    <!-- @foreach( $employees as $key=>$emp )
                                        <option value="{{ $emp->id }}">{{ $emp->name }}</option>
                                    @endforeach -->
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="md-input-wrapper">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary waves-effect waves-light" value="submit">
                            <input type="hidden" name="clientjobid" id="clientjobid" value="{{$p->clientjob_id}}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endforeach
@endif

<script>
   
   function assignposition(id,c_name){
    $.ajax({

           url: "{{ route('getemployees') }}",
           type: "GET",
           data: 'data='+id,
           beforeSend: function() {
           // alert('gurpreet1');
           },        
           success: function(response) {
            //alert(response);
            $('#clientjobid').val(id);
            $('#pop_company_name').val(c_name);
            $('#ajaxhr').html(response);
            $('#ajaxhr').multiselect('rebuild');
           }, 
           error: function() {
              alert("Something Went Wrong");
           }
        });

    }
</script>    
@endsection
