@extends('admin.layouts.master')
@section('content')
<head>
  <meta charset="UTF-8">
  <title>bootstrap4</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
  <link href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">
  <style type="text/css">
      .parsley-errors-list li {color:#f00;}
  </style>
</head>

<div class="container-fluid">
    <div class="row">
    <div class="main-header">
    <h4>Add Position</h4>
    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
        <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
        </li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">Clients</a>
        </li>
        <li class="breadcrumb-item"><a href="">Add Position</a>
        </li>
    </ol>
    </div>
</div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
               <div class="card-header"><h5 class="card-header-text">Add Position Form</h5>

               </div>
               @if(Session::has('success_msg'))
                  <div class="alert alert-success">
                     {{ Session::get('success_msg') }}
                  </div>
            @endif

            <div class="card-block">
               {!! Form::open([ 'action'=>'Admin\AdminController@addpositionadmin','data-parsley-validate', 'method'=>'post', 'onsubmit'=>'return checkposition();' ,'files'=>true ]) !!}
                    <div class="row">
                        @if(count($errors))
                           <div class="alert alert-danger">
                              <strong>Whoops!</strong> There were some problems with your input.
                              <br/>
                              <ul>
                                 @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                 @endforeach
                              </ul>
                           </div>
                        @endif
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">


                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Type </label>
                              <div class="col-sm-8">
                                 <select class="form-control" id="exampleSelect1" name="dept" >
                                    @foreach($departments as $kk => $value)
                                       <option value="{{$value->dept_id}}">{{$value->dept_name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>


                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Company Name</label>
                              <div class="col-sm-8">
                                 <select class="form-control" id="exampleSelect1" name="compname" >
                                    @foreach($client as $kk => $value)
                                       <option value="{{$value->fk_clientid}}">{{$value->comp_name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>

                     </div>

                     <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Position Title <span style="color:red;">*</span> </label>  
                              <div class="col-sm-8">
                                <input class="form-control postitle" placeholder="Enter Job Title" type="text" value="" id="example-text-input" name="jobtitle" >
                               </div>
                               <span class="title_msg"></span>
                           </div>
                        </div>
                    </div>

                     <div class="row">

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">No of Positions </label>
                              <div class="col-sm-8">
                                 <input class="form-control" placeholder="Enter No. of Positions" type="text" value="" id="example-text-input" name="noofpos">
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">

                                 <div class="checkbox-color checkbox-success ">
                                    <input id="checkbox3" type="checkbox" name="drive">
                                    <label for="checkbox3">
                                       Drive
                                    </label>
                                 </div>
                              </label>
                                <div class="col-sm-8">
                                        <div class="form-group">
                                            <div class="input-group date" id="datetimepicker1">
                                            <input type="text" class="form-control" placeholder="Drive Date" name="drivedate">
                                            <span class="input-group-addon">
                                                    <span class="icofont icofont-ui-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                </div>
                           </div>
                        </div>
                      </div>
                    <div class="row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Add Job Description <span style="color:red;font-weight: bold;">*</span></label>
                        <span class="summernote_msg"></span>
                    </div>  
                    <div class="row">
                    <textarea name="upload_position_jd" id="summernote" class="summernote form-control" ></textarea>

                    </div> 
                    </div>
                     <div class="md-input-wrapper">
                        <button type="submit" class="btn btn-primary waves-effect waves-light"  >Submit
                        </button>
                     </div>
                    {!! Form::close() !!} 
               </div>
            </div>
         </div>
      </div>
   </div>
   </div>
<!-- <script src="http://parsleyjs.org/dist/parsley.js"></script> -->
<!-- <script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script> -->
<script type="text/javascript">
  function checkposition()
{
  var postitle = $('.postitle').val();
  //var summernote = $('#summernote').val();
 var summernote = $('#summernote').val();


  //alert(summernote); 
  if(postitle=='')
  {
     //alert('hellooo');
     $(".title_msg").html("Please enter job title.").css({ 'color': 'red' });
     return false;
   } else{

     $(".title_msg").html(" ").css({ 'color': 'red' });
   }

  if(summernote=='')
  {
    
     $(".summernote_msg").html("Please enter Job Description.").css({ 'color': 'red' });
     return false;
   } else{

     $(".summernote_msg").html(" ").css({ 'color': 'red' });
   }
  
}

</script>
@endsection
