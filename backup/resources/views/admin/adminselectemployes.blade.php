@extends('admin.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Employee Listing</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Employee</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">View All Employees</a>
                    </li>
                </ol>
            </div>
        </div>
    <div class="row">
    <div class="col-md-12">
    <style>
/*        .pagination li{
            display: inline-block;
        }*/
    </style>

        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Employees Listing</h5>

            </div>
            <div class="card-block">
               

                <div class="row">
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                        <div class="pull-right;">{{ $employees->links() }}</div>
                        </div>
                    </div>

                    <div class="col-sm-12 table-responsive">
                        <span class="text-danger">
                    @if(session('message'))
                                {{session('message')}}
                            @endif
</span>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th> Name</th>
                                <th> Contact No</th>
                                <th> Email</th>
                                <th> Date Of Joining</th>
                                <th>Action</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                                $temp = $employees ;  
                            @endphp
                            @foreach($employees as $kk => $employees)
                            <tr class="table-active">


                                <td> {{  $i++   }}</td>

                                <td> {{  $employees->name }}</td>
                                <td>{{  $employees->emp_contactno }}</td>
                                <td>{{  $employees->email }}</td>
                                <td>{{  $employees->emp_doj }}</td>
                                <td>
                                   <a href="{{ route('admin.editadminemployes',$employees->id) }}" class="btn btn-primary">Edit </a>
                                   <a href="{{ route('admin.deleteadminemployes',$employees->id) }}" class="btn btn-danger">Delete </a>
                                </td>
                               </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                            <div class="pull-right;">{{ $temp->links() }}</div>
                        </div>
                    </div>                    

                </div>
            </div>
        </div>
    </div>

</div>
    @endsection