@extends('admin.layouts.master')

@section('content')
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>

"<style>
.ir{    overflow: overlay;}
</style>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Assign Menu</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Menu</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Assign Menu</a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <!-- Textual inputs starts -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h5 class="card-header-text">Assign Menu</h5>

                    </div>


                    <div class="card-block">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    @if(Session::has('success_msg'))
                                        <div class="alert alert-success">
                                            {{ Session::get('success_msg') }}
                                        </div>
                                    @endif

                                    {!! Form::open([ 'action'=>'Admin\MenuController@assignmbyrole', 'method'=>'post','data-parsley-validate' ]) !!}
                                     @if(count($errors))
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.
                                    <br/>
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">



                                    <div class="col-sm-4">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Role Type </label>
                                        <div class="col-sm-8">
                                        <div class="form-group {{ $errors->has('roleid') ? 'has-error' : '' }}">

                                            <select   name="roleid" class="form-control" id="roleid" required>
                                             <option value="">Select Role</option>

                                                @foreach($role as $key=>$value)
                                                    <option value="{{ $value->role_id }}">{{ $value->role_name }}</option>
                                                @endforeach
                                            </select>
                                              <span class="text-danger">{{ $errors->first('roleid') }}</span>
                                        </div>
                                        </div>

                                    </div>




                                    <div class="col-sm-4">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Parent Menu </label>
                                        <div class="col-sm-8">
                                        <div class="form-group  {{ $errors->has('parentid') ? 'has-error' : '' }}">

                                            <select  name="parentid[]" class="form-control" id="parentid" onchange="getchildmenu();" required>
                                            <option value="">Select parent menu</option>
                                                @foreach($menu as$key=>$value)
                                                    <option value="{{$value->id}}">{{$value->menuname}}</option>
                                                @endforeach
                                            </select>
                                            </div>
                                              <span class="text-danger">{{ $errors->first('parentid') }}</span>
                                        </div>
                                    </div>




                                    <div class="col-sm-4"  id="child" >
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Child Menus </label>
                                        <div class="col-sm-8">
                                        <div class="form-group  {{ $errors->has('childid') ? 'has-error' : '' }}">

                                            <select multiple="multiple" name="childid[]" class="form-control" id="childid"  required>


                                            </select>
 
  </div>
                                        </div>
                                          <span class="text-danger">{{ $errors->first('childid') }}</span>
                                    </div>


                                     <div class="col-sm-4">
                                     <div class="col-sm-8">
                                        <div class="form-group">
                                    <input type="submit" class="btn btn-primary waves-effect waves-light" name="rolesubmit" value="Save"  id="rolesubmit"  >
                                    </input>
                                    </div>
                                    </div>
                                   

                                </div>
                                 {!! Form::close() !!}
                                
                                

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endsection
        <script>
            function getchildmenu()
            {
                //alert('gfdh');
                var p=$('#parentid').val();
               // alert(p);
                var url_data = p;
                if($('#parentid').val()==0){
                    $('#child').hide();
                }else{
                    $('#child').show();
                }

                $.ajax({
                    url: 'childmenu/'+url_data,
                    type: 'get',
                    success: function(data){
                      //alert(data);
                       // $('#childid').empty();
                        $("#childid").html(data);

                    }
                });



            }


        </script>

