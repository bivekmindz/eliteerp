@extends('admin.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Role List</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Role</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">View All Roles</a>
                    </li>
                </ol>
            </div>
        </div>
    <div class="row">

    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Role Listing</h5>

            </div>
            <div class="card-block">
                <div class="row">
                <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                        <div class="pull-right;">{{ $roles->links() }}</div>              
                        </div>
                    </div>

                    <div class="col-sm-12 table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Role Name</th>



                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                                  $temp = $roles ; 
                            @endphp
                            @foreach($roles as $kk => $roles)
                            <tr class="table-active">


                                <td> {{  $i++   }}</td>

                                <td>{{  $roles->role_name }}</td>

                               </tr>

                            @endforeach

                            </tbody>
                        </table>

                          <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                        <div class="pull-right;">{{ $temp->links() }}</div>              
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
    @endsection