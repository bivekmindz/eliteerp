@extends('admin.layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View All Position Allocation To Recruiter</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">Position</a>
                </li>
                <li class="breadcrumb-item"><a href="">View Position Assigned</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">View All Position Assigned To Recruiter</h5>
                </div>
                @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
                @endif
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Position Name</th>
                                    <th>Company Name</th>
                                    <th>Drive</th>
                                    <th>No Of Positions</th>
                                    <th>Assigned Recuriter Name</th>
                                    {{--<th>Action</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                $i=1;
                                @endphp
                                @foreach($pos as $key => $p)
                                <tr class="table-active">
                                    <td>{{ $i++ }}</td>
                                    <td>{{ !empty($p->clientjob_title) ? $p->clientjob_title : 'N/A' }}</td>
                                    <td>{{ !empty($p->comp_name) ? $p->comp_name : 'N/A' }}</td>
                                    <td>N/A</td>
                                    <td>{{ !empty($p->clientjob_noofposition) ? $p->clientjob_noofposition : 'N/A' }}</td>
                                    <td>{{ !empty($p->name) ? $p->name : 'N/A' }}</td>

                </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>



@endsection
