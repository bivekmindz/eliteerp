@extends('admin.layouts.master')

@section('content')
    <script>

        function showdiv()
        {
            var role=$('#roleid').val();
            var url_data = role;
            //alert(role);exit;
            $.ajax({
                type: "GET",
                url: 'getmenubyrole/' + url_data,
                success: function( data )
                {
                    console.log(data);

//                    var col = [];
//                    for (var i = 0; i < data.length; i++) {
//
//
//
//                        }

//                    var data_array = JSON.parse(data);
//                    console.log( data_array.data );
//                    (data_array.data).forEach(function(item){
//
//                        console.log(item.id);
//                    });



                    //alert( data_array.menuname );

                   // $('#assignmenulist').html(data);
                    //$(".newArrow").parent().addClass("expend");
                    $("#assignmenulist").append(data);
                    $('#assignmenulist').show();

                }
/*                    [{"id":1,"menuname":"newparentmenu","menuparentid":0,"roleid":1,"access_to":0,"access_create":1,"access_view":1,"access_update":0,"access_delete":0,"status":1,"createdon":"2018-01-31 10:19:54","updatedon":null}]*/


            });
           // $('#assignmenulist').show();
        }

    </script>

    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Assign Menu</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Admin Login</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Assign Menu</a>
                    </li>
                </ol>
            </div>
        </div>




        <div class="row">
            <!-- Textual inputs starts -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h5 class="card-header-text">Assign Menu</h5>

                    </div>

                    <div class="card-block">

                        {!! Form::open(array('action' => 'Admin\MenuController@getmenubyrole', 'method' => 'post')); !!}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Role Type </label>
                                        <div class="col-sm-8">


                                            <div class="form-group">


                                                <select  class="multiselect-ui form-control"  name="roleid" id="roleid">
                                                    @foreach($role as $key=>$value)
                                                    <option value="{{ $value->role_id }}">{{ $value->role_name }}</option>
                                                    @endforeach

                                                </select>


                                            </div>
                                            <input type="submit" class="btn btn-primary waves-effect waves-light" name="rolesubmit" value="Go Assign Menu"  id="rolesubmit"  >
                                            </input>

                                        </div>
                                    </div>

                                </div>

                            </div>

                    {!! Form::close(); !!}

                        @if(!empty($access))
                            <div class="row" id="assignmenulist" >
                                {{ $access }}

                                @php
                                    $vid=array('1','9','10');

                                @endphp
                    <div class="menu_add">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            @foreach($menu as $key=>$value)
                                @if($value->menuparentid==0)



                                            <tr>

                                                <td width="4%">
                                                    {{--{{ $access->id }}--}}


                                                    <input class="chec" type="checkbox"  @if(in_array($value->id,$vid))
                                                        {{ 'checked' }}
                                                            @endif ></td>


                                                <td colspan="10">

                                                        {{ $value->menuname }}

                                                </td>
                                            </tr>
                                        @foreach($menu as $key=>$val)
                                            @if($val->menuparentid==$value->id)
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td width="4%">

                                                    <input class="chec" type="checkbox" value="{{ $val->id }}" name="childid"  @if(in_array($val->id,$vid))
                                                        {{ 'checked' }}
                                                            @endif></td>

                                                        <td width="14%">{{ $val->menuname }}</td>

                                                @if($val->access_create==1)
                                                    <td width="4%"><input class="chec" type="checkbox" value="{{ $val->access_create }}" checked ></td>
                                                @else
                                                    <td width="4%"><input class="chec" type="checkbox" value="{{ $val->access_create }}" ></td>
                                                @endif
                                                <td width="12%">Create</td>



                                                @if($val->access_view==1)
                                                        <td width="4%"><input class="chec" type="checkbox"  value="{{ $val->access_view }}" checked>
                                                @else
                                                    <td width="4%"><input class="chec" type="checkbox"  value="{{ $val->access_view }}" ></td>
                                                @endif
                                                <td width="12%">View</td>



                                                @if($val->access_update==1)
                                                        <td width="4%"><input class="chec" type="checkbox" value="{{ $val->access_update }}" checked></td>
                                                @else
                                                    <td width="4%"><input class="chec" type="checkbox" value="{{ $val->access_update }}" ></td>
                                                @endif
                                                <td width="12%"> Update</td>


                                                @if($val->access_delete==1)
                                                    <td width="4%"><input class="chec" type="checkbox" value="{{ $val->access_delete }}" checked></td>
                                                @else
                                                    <td width="4%"><input class="chec" type="checkbox" value="{{ $val->access_delete }}" ></td>
                                                @endif
                                                <td width="12%"> Delete</td>



                                            </tr>
                                            @endif
                                        @endforeach
                                            @endif

                                            @endforeach

                                    </table>

                                </div>




                            <div class="md-input-wrapper">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>
                            </div>




                    </div>
                            @endif

                    </div>

                </div>
            </div>
            <!-- Textual inputs ends -->
        </div>



    </div>
    @endsection
