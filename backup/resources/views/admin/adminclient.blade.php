@extends('admin.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Client List</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Clients</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">View All Clients</a>
                    </li>
                </ol>
            </div>
        </div>
    <div class="row">

    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Client Listing</h5>

            </div>
            <div class="card-block">
                <div class="row">

                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                             <div class="pull-right;"></div>
                        </div>
                    </div>

                    <div class="col-sm-12 table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Category</th>
                                <th>Company Name</th>
                                <th>Contact Person Name</th>
                                <th>Designation</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Created by</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                                $temp = $clients ;
                            @endphp
                            @foreach($clients as $kk => $clients)
                            <tr class="table-active">


                                <td> {{  $i++   }}</td>
                                <td> @if($clients->client_catid==1)
                                         {{ "IT" }}
                                         @else
                                          {{ "NON IT"  }}
                                         @endif


                                </td>
                                <td>{{  $clients->comp_name }}</td>
                                <td>{{  $clients->contact_name }}</td>
                                <td>{{ $clients->designation }}</td>
                                <td>{{  $clients->phone }}</td>
                                <td>{{ $clients->email }}</td>
                                <td>{{ $clients->user->name }}</td>
                               </tr>

                            @endforeach

                            </tbody>
                        </table>
<div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                            <div class="pull-right;"></div>
                        </div>
                    </div>                    

                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
    @endsection