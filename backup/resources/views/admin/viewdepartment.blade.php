@extends('admin.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Add new Department</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Department</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Add All Department</a>
                    </li>
                </ol>
            </div>
        </div>

    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><h5 class="card-header-text">Add Department</h5>

                </div>
                @if(Session::has('success_msg'))
                    <div class="alert alert-success">
                        {{ Session::get('success_msg') }}
                    </div>
            @endif

            <!-- end of modal -->
                <div class="card-block">
                    <form  action="{{ route('admin.addadmindepartment') }}"  name='department' method="POST" onsubmit="return validatedept();" >
                        @if(count($errors))
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.
                                <br/>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">


                        <div class="form-group row {{ $errors->has('department_name') ? 'has-error' : '' }}">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Department Name<span class="error"> * </span></label>
                            <div class="col-sm-5">
                                <input class="form-control department" type="text" value="" id="example-text-input"  name="department_name"/>
                                <span class="depart"></span>
                                <span class="text-danger">{{ $errors->first('department_name') }}</span>
                            </div>
                        </div>


                        <div class="md-input-wrapper">
                            <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit">
                            </input>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- Textual inputs ends -->
    </div>
    </div>
    </div>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script>
    $(function() {
        $("form[name='department']").validate({
            rules: {
              department_name: "required",
            },
            messages: {
              department_name: "Please enter department.",
            },
            submitHandler: function(form) {
              form.submit();
            }
        });
    });

        function validatedept()
    {
        var department=$('.department').val();
       // alert(role); return false;
        if(department=="")
    {
     $(".depart").html("Please enter department name!.").css({ 'color': 'red' });
     return false;
   }else{
     $(".depart").html(" ").css({ 'color': 'red' });
   }
    }
</script> 
</script>
@endsection
