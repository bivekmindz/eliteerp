@extends('admin.layouts.master')

@section('content')
<div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Postion Resume Upload</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Postion </a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Postion List</a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
            <div class="card">
                       <div class="card-header">
                       <h5 class="card-header-text">
                       <!-- <div style="display:none;" id="showtimer">Registration closes in <span id="time">60:00</span> minutes!</div> -->
                            <a href="#">
                                <button type="button" id="startTime" name="startTime" class="star_tim" onclick="myFunction()">
                                    <i class="icofont icofont-clock-time"></i> Start Time 
                                </button>
                            </a>
                            <a href="#">
                                <button type="button" id="endTime" name="endTime" class="star_tim1" >
                                    <i class="icofont icofont-clock-time"></i> End Time 
                                </button>
                            </a>
                            <a href="#">
                                <button type="button" id="showtimer" style="display:none;" class="star_tim">
                                    <i class="icofont icofont-clock-time"></i> Uploading Time will be over in <span id="time">60:00</span> minutes!</div>
                                </button>
                            </a>
                            </h5>

                          <div class="f-right">
                                  
                               </div>
                       </div>
                       <div class="card-block">
                           <div class="row">
                               <div class="col-sm-12 table-responsive">
                                   <table class="table">
                                        <thead>
                                        <tr>
                                                <th>Company Name </th>
                                                <td>{{ $getlists->comp_name }}</td>
                                                <th>Position Name</th>
                                                <td style="border-bottom: 2px solid #eceeef;">{{ $getlists->clientjob_title }}</td>
                                            </tr>
                                        <td colspan="2">
                                        <div class="arro_css">
                                            {!! htmlspecialchars_decode($getlists->upload_position_jd) !!} 
                                        </div>
                                        </td>
                                        </thead>
                                      
                                   </table>
                                    
                               <div class="md-input-wrapper flo op">
                                  
                                <button type="submit" class="btn btn-primary waves-effect waves-light">
                                    <span class="icofont icofont-eye-alt"></span> Upload CV
                                </button>
                                </div>
                               </div>
                              
                           </div>
                           <div class="col-sm-12 table-responsive">
                                   <table class="table dt">
                                       <thead>
                                       <tr>
                                           <th>S.N</th>
                                           <th>Name</th>
                                           <th>Phone No</th>
                                           <th>Email</th>
                                           <th>Upload Resume</th>
                                           <th>
                                             <button type="button" class="btn btn-success btn-add">Add</button>
                                           </th>
                                           
                                       </tr>
                                       </thead>
                                       <tbody>
                                       <div class="controls">
                                       {!! Form::open(array('action' => "Admin\RecruiterController@uploadcv", 'method' => 'post', 'files'=>true, 'id'=>'uploadcv' )) !!}
                                       <tr class="table-active">
                                           <td>1</td>
                                           <td>
                                           <div class="form-group required">
                                            <input type="text" name="candidate_name" class="form-control">
                                            </div>
                                        </td>
                                          <td>
                                          <div class="form-group required">
                                            <input type="text" name="candidate_mob" class="form-control">
                                        </div>
                                        </td>
                                           <td>
                                           <div class="form-group required">
                                            <input type="text" name="candidate_email" class="form-control">
                                        </div>
                                        </td>
                                           <td>
                                           <label for="file" class="custom-file">
                                               <input type="file" id="file" name="cv_name" class="custom-file-input">
                                               <span class="custom-file-control"></span>
                                           </label>
                                           </td>
                                           <td><button type="button" class="btn-remove btn tra btn-xs"><i class="icofont icofont-ui-delete fa-2x" aria-hidden="true"></i></button></td>
                                            
                                           
                                       </tr>
                                       
                                       <tr>
                                      
                                       </tr>
                                       <tr>
                                           <td colspan="6">
                                           <div class="md-input-wrapper">
                                   <button type="submit" class="btn btn-default waves-effect">Cancel
                                   </button>
                                    <!-- <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                                   </button> -->
                                   <input type="hidden" name="fkjdid" value="{{$getlists->fk_jdid}}">
                                   <input type="hidden" name="clientreqid" value="{{$getlists->clientreq_id}}">

                                   <input type="submit" class="btn btn-primary waves-effect waves-light" value="submit">
                                  
                               </div>
                                   
                                           </td>
                                       </tr>
                                      
                                       {!! Form::close() !!}
                                       </div>
                                       </tbody>
                                       
                                      
                                   </table>
                                    
                               </div>
                               
                               
                       </div>
                       
                       
                       
                    
                   </div>
            </div>
           
           </div>
           
           
           
           
           <div class="row">
           
            <div class="col-md-12">
            
            <div class="card" >
            
            
            
                       <div class="card-header">
                           <h5 class="card-header-text">Client Preview</h5>
                          
                       </div>
                       <div class="card-block">
                           <div class="row">
                               <div class="col-sm-12 table-responsive">
                                   <table class="table">
                                       
                                        <tr>
   <th scope="row" style="width:23%;">Company Name</th>
   <td>Mindz Technology</td>
 </tr>
 <tr>
   <th scope="row">Company URL</th>
   <td>http://www.mindztechnology.com/</td>
 </tr>
 <tr>
   <th scope="row" >Company Size </th>
   <td>+150</td>
 </tr>
 
 <tr>
 
   <td colspan="2">
   
    <div class="md-input-wrapper flo">
                                  
                                    <button type="submit" class="btn btn-primary waves-effect waves-light"><span class="icofont icofont-eye-alt"></span> View More
                                   </button>
                               </div>
   </td>
 </tr>

 
                                   </table>
                                   
                                   
                                   <table class="table hid" style="">
                                       
                                       
 
 
 
    <tr>
   <th scope="row" style="width:23%;">Timings</th>
   <td>1:00 PM - 11:00 PM</td>
 </tr>
 <tr>
   <th scope="row">Days Working</th>
   <td>5 days</td>
 </tr>
 <tr>
   <th scope="row">Industry </th>
   <td>Information Technology and Services</td>
 </tr>
  <tr>
   <th scope="row">About the Company</th>
   <td>
  <div class="text-he">
   <p>Webnish is conceived to take every business to an all-new level of success and growth, by equipping them with a complete and functional website.</p>
    <p>The team at Webnish is conscious of the challenges that small and medium businesses face in their attempts to get a professionally executed website, showcasing their products and services. Having worked closely with online media companies and web businesses in the US, Europe and India, we have come up with this innovative solution that lets you build and update websites quickly and with ease.</p>
    
    <p>Your online success and growth is at the core of our belief and very close to our heart. We are committed to work with you and help you realize your business objectives. We are able to accomplish this with the dedicated effort of our team who are passionate about developing best-in-class industry-specific solutions that deliver real business value to our clients.</p>
    <p>Webnish is the fastest growing revolutionary company providing instant website solutions for small and medium businesses (SMBs). We are a team of talented and experienced IT professionals.</p>
    <p>We are conscious of the challenges that small and medium businesses face their attempts to get professionally executed websites. We are able to accomplish this with the help of  our dedicated team, who are passionate about developing the best in the industry-specific solutions that produce real value to our clients.</p>
    <p>Refer our website www.webnish.com  to know about the company.</p>
    <p>Our Story</p>
    <p>Webnish is striving hard to bridge the digital divide for Small and Medium Enterprises (SMEs), who are deprived of the power of a website, since its inception in 2013. We aspire to reach out to as many enterprises as possible and take every business to an all-new level of success and growth, by equipping them with a high-utility and functional website.
Webnish has been fortunate to cater to a multitude of firms from diverse backgrounds, domains and geographies. We have powered till date over 2,500 websites. Our journey has taught us some invaluable lessons, including the limitations of SMEs and challenges faced by different industry sectors. We are happy to pass on to you our hard-earned knowledge, gathered from working closely with hundreds of customers.</p>
<p>Our mission is to power your business with a great website that will expand your reach and elevate your business success.</p>

<p>Our mission is to power your business with a great website that will expand your reach and elevate your business success.</p>
    
  </div>
   </td>
 </tr>
 
  <tr>
   <th scope="row">Company’s Specialties</th>
   <td>Business Websites</td>
 </tr>
 
  <tr>
   <th scope="row">Headquarters</th>
   <td>Bangalore, Karnataka</td>
 </tr>
   <tr>
   <th scope="row">Office Address</th>
   <td>#53, 54 & 77, 78, Anjaneyalu Plaza, Old Mangammanapalaya Road, Off Hosur Main Road, Bengaluru - 560 068, India.</td>
 </tr>
                          </table>
                                   
                                   
                               </div>
                               
                               
                           </div>
                           
                           
                       </div>
                   </div>
            </div>
            
           </div>
         
    

@endsection
<script>
    function myFunction() 
    {
        setTimeout(function(){
            alert('start uploading form');
            $('#startTime').hide();
            $.ajax({

                type:"GET",
                url:'gettime/{{ $getlists->clientreq_id }}',
                data:'',
                dataType: 'json',
                success: function(data){
                    console.log(data);
                },
                error: function(data){

                }
            })
            $('#showtimer').show();
            $('#stopTime').show();   
        });
    }
</script>
<script type="text/javascript">
$(function(){

    $('#uploadcv').on('submit',function(e){
        $.ajaxSetup({
            header:$('meta[name="_token"]').attr('content')
        })
        e.preventDefault(e);

            $.ajax({

            type:"POST",
            url:'/uploadcv',
            data:$(this).serialize(),
            dataType: 'json',
            success: function(data){
                console.log(data);
            },
            error: function(data){

            }
        })
    });
}); 
</script>


<script>
function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
        }
    }, 1000);
}

window.onload = function () {
    var fiveMinutes = 60 * 60,
        display = document.querySelector('#time');
    startTimer(fiveMinutes, display);
};
</script>