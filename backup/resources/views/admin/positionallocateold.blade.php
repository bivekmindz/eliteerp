@extends('admin.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Postion Allocate form</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">New Employee Creation</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">New Employee Creation</a>
                    </li>
                </ol>
            </div>
        </div>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#boot-multiselect-demo').multiselect({
        includeSelectAllOption: true,
        buttonWidth: 250,
        enableFiltering: true
    });
    });
</script>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Assign Recruiter</h5>
            </div>
            @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
            @endif
        <div class="card-block">
            <div class="row">
                <div class="col-sm-12 table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>S.N</th>
                                <th>Position Name</th>
                                <th>Company Name</th>
                                <th>Drive</th>
                                <th>No Of Positions</th>
                                <th>SPOC</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody> 
                        @php
                            $i=1;
                        @endphp
                        @foreach($positions as $key => $p)
                            <tr class="table-active">
                                <td>{{ $i++ }}</td>
                                <td>{{ !empty($p->clientjob_title) ? $p->clientjob_title : 'N/A' }}</td>
                                <td>{{ !empty($p->comp_name) ? $p->comp_name : 'N/A' }}</td>
                                <td>N/A</td>
                                <td>{{ !empty($p->clientjob_noofposition) ? $p->clientjob_noofposition : 'N/A' }}</td>
                                <td>{{ !empty($p->name) ? $p->name : 'N/A' }}</td>
                                <td>
                                <div class="btn-group btn-group-sm" style="float: none;">
                                <button type="button" href="#success{{$p->id}}" data-toggle="modal" class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;">
                                <span class="icofont icofont-ui-edit"></span>
                                </button>
             
                             </div>
                        </div>
                    </div>
                                </div></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
             </div>
        </div>
        <div class="modal fade" id="success{{$p->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h1><i class="glyphicon glyphicon-thumbs-up"></i>Assign RECRUITER</h1>
                </div>
                <div class="modal-body">
                {!! Form::open(array('action' => "Admin\RecruiterController@postRecruiter", 'method' => 'post')); !!}
                    <div class="form-group row">
                        <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Position Name</label>
                        <div class="col-sm-9">
                            <input class="form-control" name="clientjob_title" type="text" value="{{$p->clientjob_title}}" id="example-text-input">
                        </div>
                    </div>
                                    
                    <div class="form-group row">
                        <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Assign Recruiter</label>
                            <div class="col-sm-9">
                                <div class="form-group">
                                <select id="boot-multiselect-demo" multiple="multiple"  name="assignposition[]" class="multiselect-ui form-control">
                                    @foreach( $employees as $key=>$emp )    
                                        <option value="{{ $emp->id }}">{{ $emp->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                   <div class="modal-footer">
                <div class="md-input-wrapper">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary waves-effect waves-light" value="submit">
                    <input type="hidden" name="clientjobid" value="{{$p->clientjob_id}}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>
                {!! Form::close(); !!}
                </div>
            </div>
</div>
</div>
</div>
             
                
@endsection
