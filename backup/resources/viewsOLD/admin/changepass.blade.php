@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>CHANGE PASSWORD</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Clients</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Client List</a>
                    </li>
                </ol>
            </div>
        </div>
<div class="row">
   <!-- Textual inputs starts -->
   <div class="col-lg-12">
      <div class="card">
         <div class="card-header">
            <h5 class="card-header-text">Change Password</h5>
         </div>
         <!-- end of modal -->
         <div class="card-block">
            <form id="form-change-password" action="{{ route('admin.adminchangepassword')  }}"  method="POST" >
              @if(count($errors))
                <div class="alert alert-danger">
                  <strong>Whoops!</strong> There were some problems with your input.
                  <br/>
                  <ul>
                      @foreach($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
              @endif
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
               <div class="row">
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Current Password</label>
                        <div class="col-sm-8">
                             <input type="password" class="form-control" id="current-password" name="current-password" placeholder="Password" required="required">
                           <span class="text-danger">{{ $errors->first('current-password') }}</span>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">New Password</label>
                        <div class="col-sm-8">
                           <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="required">
                           <span class="text-danger">{{ $errors->first('password') }}</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Re-enter Password</label>
                        <div class="col-sm-8">
                             <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Re-enter Password" required="required">
                           <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                        </div>
                     </div>
                  </div>
                 
               </div>
              
             
               <div class="md-input-wrapper">
                  <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit" onclick="return Validate()">
                  </input>
               </div>
            </form>
         </div>
      </div>
   </div>
   <!-- Textual inputs ends -->
</div>
</div>
</div>

@endsection
