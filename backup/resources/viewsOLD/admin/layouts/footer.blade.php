<script src="{{ URL::asset('js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ URL::asset('js/tether.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('js/waves.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
<script src="{{ URL::asset('js/menu.js') }}"></script>

<script type="text/javascript">
    $(function() {
        $('.multiselect-ui').multiselect({
            includeSelectAllOption: true
        });
    });
</script>
<script type="text/javascript">
$(".one-click").click(function(){
    $(".hide_css").slideToggle(700);
});
</script>
<script src="{{ URL::asset('js/sel.js') }}"></script>


            <script>
                $(function()
                {
                    $(document).on('click', '.btn-add', function(e)
                    {
                        e.preventDefault();

                        var controlForm = $('.controls form:first'),
                            currentEntry = $(this).parents('.entry:first'),
                            newEntry = $(currentEntry.clone()).appendTo(controlForm);

                        newEntry.find('input').val('');
                        controlForm.find('.entry:not(:last) .btn-add')
                            .removeClass('btn-add').addClass('btn-remove')
                            .removeClass('btn-success').addClass('btn-danger')
                            .html('<span class="icofont icofont-minus"></span>');
                    }).on('click', '.btn-remove', function(e)
                    {
                        $(this).parents('.entry:first').remove();

                        e.preventDefault();
                        return false;
                    });
                });

            </script>

