@extends('admin.layouts.master')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>view Role</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">New Employee Creation</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">New Employee Creation</a>
                    </li>
                </ol>
            </div>
        </div>

    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><h5 class="card-header-text">Add Role</h5>

                </div>
                @if(Session::has('success_msg'))
                    <div class="alert alert-success">
                        {{ Session::get('success_msg') }}
                    </div>
            @endif

            <!-- end of modal -->
                <div class="card-block">
                    <form  action="{{ route('admin.addadminrole')  }}"  method="POST" >
                        @if(count($errors))
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.
                                <br/>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">


                        <div class="form-group row {{ $errors->has('role_name') ? 'has-error' : '' }}">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Role Name</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" value="" id="example-text-input"  name="role_name" required>
                                <span class="text-danger">{{ $errors->first('role_name') }}</span>
                            </div>
                        </div>


                        <div class="md-input-wrapper">
                            <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit">
                            </input>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- Textual inputs ends -->
    </div>




    </div>

    </div>

@endsection
