@extends('admin.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Assign Menu</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Admin Login</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Assign Menu</a>
                    </li>
                </ol>
            </div>



        </div>




        <div class="row">



            <!-- Textual inputs starts -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h5 class="card-header-text">Assign Menu</h5>

                    </div>



                    <div class="card-block">
                        <form >

                            <div class="row">


                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Role Type </label>
                                        <div class="col-sm-8">


                                            <div class="form-group">


                                                <select id="dates-field2" class="multiselect-ui form-control" multiple="multiple">
                                                    @foreach($role as $key=>$value)
                                                    <option value="{{ $value->role_id }}">{{ $value->role_name }}</option>
                                                        @endforeach

                                                </select>


                                            </div>
                                        </div>
                                    </div>

                                </div>


                                {{--<div class="col-lg-6">--}}
                                    {{--<div class="form-group row">--}}
                                        {{--<label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Employee Name</label>--}}
                                        {{--<div class="col-sm-8">--}}
                                            {{--<select class="form-control " id="exampleSelect1">--}}
                                                {{--<option>Rahul</option>--}}
                                                {{--<option>Ankit</option>--}}
                                                {{--<option>Kapil</option>--}}

                                            {{--</select>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            </div>

                            <div class="row">

                                <div class="menu_add">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        @foreach()
                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td colspan="10"> Clients</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="2">&nbsp;</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%">Add new client</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>




                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%">Assign SPOC</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>


                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td colspan="10"> Vacancies</td>
                                        </tr>


                                        <tr>
                                            <td rowspan="5">&nbsp;</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%"> Add New Position</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>

                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%"> Position Allocation</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>

                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%"> Vacancy List</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>


                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%"> My Positions</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>

                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%">SPOC Shortlisting</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>



                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td colspan="10">  Candidate Details</td>
                                        </tr>


                                        <tr>
                                            <td rowspan="2">&nbsp;</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%">Candidate Status</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>

                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%"> Joining List</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>




                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td colspan="10">Admin Login</td>
                                        </tr>


                                        <tr>
                                            <td rowspan="6">&nbsp;</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%">Add Employee</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>

                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%"> Client Excel Upload</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>

                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%"> Assign Monthly Target</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>

                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%"> Define Team</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>

                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%"> Assign Team Target</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>

                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="14%"> Assign Menu</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">Create</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%">View</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Update</td>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td width="12%"> Delete</td>
                                        </tr>



                                        <tr>
                                            <td width="4%"><input class="chec" type="checkbox"></td>
                                            <td colspan="10">Report</td>
                                        </tr>



                                    </table>

                                </div>

                            </div>


                            <div class="md-input-wrapper">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>
                            </div>



                        </form>
                    </div>
                </div>
            </div>
            <!-- Textual inputs ends -->
        </div>



    </div>
    @endsection
