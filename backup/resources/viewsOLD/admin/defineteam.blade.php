@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>TEAM</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Team</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Add Team</a>
                    </li>
                </ol>
            </div>
        </div>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#teamleader').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });

            $('#teammember').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });
        });
    </script>
    <div class="row">
        <!-- Textual inputs starts -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">Add New Team</h5>
                </div>

                <!-- end of modal -->
                <div class="card-block">
                    <form action="{{ route('team')  }}" onsubmit="return validateForm();" id="team" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Team Name</label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="teamname" value="" id="teamname">
                            </div>
                            <span class="error" style="display:none;" id="team_msg">Please enter team name.</span>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Team Leader</label>
                            <div class="col-sm-5">
                                <div class="">
                                    <select id="teamleader" name="teamleader[]" class="multiselect-ui form-control" multiple="multiple">
                                        @foreach($user as $kk => $users)
                                            <option value="{{ $users->id  }}">{{ $users->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Team Member</label>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <select id="teammember" name="teammember[]" class="multiselect-ui form-control" multiple="multiple">
                                        @foreach($user as $kk => $users)
                                            <option value="{{ $users->id  }}">{{ $users->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="md-input-wrapper">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Textual inputs ends -->
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">SPOC Detail</h5>

                </div>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Team Name</th>
                                    <th>Team Lead</th>
                                    <th>Team Members</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $i = 0;
                                @endphp
                                @foreach($teamlisting as $key => $teamlist)
                                    <tr class="table-active">
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $teamlist->team_name }}</td>
                                        <td>{{ $teamlist->team_lead_id }}</td>
                                        <td>{{ $teamlist->team_members_id }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


<style>
    .error{
        color:red;
    }
</style>

<script>
    function validateForm() {
        var teamname = $("#teamname").val();
        if(teamname == '') {
            alert('Please enter team name.');
            return false;
        }

        var teamleader = $("#teamleader").val();
        var lea = teamleader.toString();
        var tealead = lea.split(',');

        if(teamleader == '') {
            alert('Please enter team leader.');
            return false;
        }else if(teamleader.length > 2){
            alert('Please enter team leader less than 2.');
            return false;
        }

        var teammember = $("#teammember").val();
        if(teammember == '') {
            alert('Please enter team member.');
            return false;
        }

        var member = teammember.toString();
        var teamem = member.split(',');
        var j = 0;
        for(var i = 0;i<tealead.length;i++){
            console.log(tealead[i]);
            var index = teamem.indexOf(tealead[i]);
            if(index != -1){
                j++;
            }
        }

        if(j!=0){
            alert('PLease select team member different from team leaders.');
            return false;
        }
    }
</script>

