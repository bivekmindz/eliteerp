@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Employee Excel upload</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Employee excel uplaod</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Employee Excel</a>
                    </li>
                </ol>
            </div>



        </div>




        <div class="row">



            <!-- Textual inputs starts -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h5 class="card-header-text">Employee Excel Upload</h5>

                    </div>

                    <!-- end of modal -->
                    <div class="card-block">
                        <form >

                            <div class="row">


                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label"></label>
                                        <div class="col-sm-8">
                                            <button type="button" class="btn  btn-success  waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel">Download Excel
                                                <span class="badge"><i class="icofont icofont-file-excel"></i></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Upload Excel</label>
                                        <div class="col-sm-8">
                                            <label for="file" class="custom-file">
                                                <input type="file" id="file" class="custom-file-input">
                                                <span class="custom-file-control"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="md-input-wrapper">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>
                            </div>






                        </form>
                    </div>
                </div>
            </div>
            <!-- Textual inputs ends -->
        </div>


        <div class="row">

            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Employee List</h5>

                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Name</th>
                                        <th>Contact Number</th>
                                        <th>Email</th>
                                        <th>Department</th>
                                        <th>Role</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="table-active">
                                        <td>1</td>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                        <td>Ducky</td>
                                        <td>@mdo</td>

                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                        <td>Ducky</td>
                                        <td>@mdo</td>

                                    </tr>
                                    <tr class="table-success">
                                        <td>3</td>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                        <td>Ducky</td>
                                        <td>@mdo</td>

                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                        <td>Ducky</td>
                                        <td>@mdo</td>

                                    </tr>
                                    <tr class="table-warning">
                                        <td>5</td>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                        <td>Ducky</td>
                                        <td>@mdo</td>

                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                        <td>Ducky</td>
                                        <td>@mdo</td>

                                    </tr>
                                    <tr class="table-danger">
                                        <td>7</td>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                        <td>Ducky</td>
                                        <td>@mdo</td>

                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                        <td>Ducky</td>
                                        <td>@mdo</td>

                                    </tr>
                                    <tr class="table-info">
                                        <td>9</td>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                        <td>Ducky</td>
                                        <td>@mdo</td>

                                    </tr>

                                    <tr>
                                        <td>10</td>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                        <td>Ducky</td>
                                        <td>@mdo</td>

                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>




    </div>



@endsection