<script src="{{ URL::asset('js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ URL::asset('js/tether.min.js') }}"></script>
<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('js/waves.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
<script src="{{ URL::asset('js/menu.js') }}"></script>
<script>
$(function() {
$('.multiselect-ui').multiselect({
includeSelectAllOption: true
});
});
</script>
<script src="{{ URL::asset('js/sel.js') }}"></script>