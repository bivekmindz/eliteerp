
<ul class="sidebar-menu">
    <li class="nav-level">Navigation</li>
    <li class="active treeview">
        <a class="waves-effect waves-dark" href="#">
            <i class="icon-speedometer"></i><span> Clients</span><i class="icon-arrow-down"></i>
        </a>
        <ul class="treeview-menu">
            <li class="active"><a class="waves-effect waves-dark" href="{{ URL::to('newclient') }}"><i class="icon-arrow-right"></i><span>Add New Clients </span></a></li>
            <li class=""><a class="waves-effect waves-dark" href="{{ URL::to('listclient') }}"><i class="icon-arrow-right"></i><span>View All Clients </span></a></li>
            <li class=""><a class="waves-effect waves-dark" href="{{ URL::to('clientspoc') }}"><i class="icon-arrow-right"></i><span>Client assigned to spocs </span></a></li>
            <li><a class="waves-effect waves-dark" href="{{ URL::to('client-excel') }}"><i class="icon-arrow-right"></i><span>Client Excel upload </span></a></li>

        </ul>
    </li>



    <li class="treeview">
        <a class="waves-effect waves-dark" href="#"><i class="icon-picture"></i><span> Vacancies</span><i class="icon-arrow-down"></i></a>
        <ul class="treeview-menu">
            <li ><a class="waves-effect waves-dark" href="{{ URL::to('positionform') }}"><i class="icon-arrow-right"></i> Add New Position</a></li>
            <li><a class="waves-effect waves-dark" href="{{ URL::to('positionlist') }}"><i class="icon-arrow-right"></i> Vacancy List</a></li>
            <li><a class="waves-effect waves-dark" href="animation.html"><i class="icon-arrow-right"></i> My Positions</a></li>

        </ul>
    </li>

    <li class="treeview"><a class="waves-effect waves-dark" href="#!"><i class="icon-picture"></i><span>
Admin Login</span><i class="icon-arrow-down"></i></a>
        <ul class="treeview-menu">
            <li><a class="waves-effect waves-dark" href="contact-card.html"><i class="icon-arrow-right"></i> Employee Excel Upload</a></li>
            <li><a class="waves-effect waves-dark" href="contact-details.html"><i class="icon-arrow-right"></i> Client Excel Upload</a></li>
            <li><a class="waves-effect waves-dark" href="animation.html"><i class="icon-arrow-right"></i> Change Password</a></li>
            <li><a class="waves-effect waves-dark" href="animation.html"><i class="icon-arrow-right"></i> Assign Menu</a></li>

        </ul>
    </li>




</ul>
</section>
</aside>
<!-- Sidebar chat start -->

<div class="showChat_inner">
    <div class="media chat-inner-header">
        <a class="back_chatBox">
            <i class="icofont icofont-rounded-left"></i> Josephin Doe
        </a>
    </div>
    <div class="media chat-messages">
        <a class="media-left photo-table" href="#!">
            <img class="media-object img-circle m-t-5" src="assets/images/avatar-1.png" alt="Generic placeholder image">
            <div class="live-status bg-success"></div>
        </a>
        <div class="media-body chat-menu-content">
            <div class="">
                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                <p class="chat-time">8:20 a.m.</p>
            </div>
        </div>
    </div>
    <div class="media chat-messages">
        <div class="media-body chat-menu-reply">
            <div class="">
                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                <p class="chat-time">8:20 a.m.</p>
            </div>
        </div>
        <div class="media-right photo-table">
            <a href="#!">
                <img class="media-object img-circle m-t-5" src="assets/images/avatar-2.png" alt="Generic placeholder image">
                <div class="live-status bg-success"></div>
            </a>
        </div>
    </div>
    <div class="media chat-reply-box">
        <div class="md-input-wrapper">
            <input type="text" class="md-form-control" id="inputEmail" name="inputEmail" >
            <label>Share your thoughts</label>
            <span class="highlight"></span>
            <span class="bar"></span>  <button type="button" class="chat-send waves-effect waves-light">
                <i class="icofont icofont-location-arrow f-20 "></i>
            </button>

            <button type="button" class="chat-send waves-effect waves-light">
                <i class="icofont icofont-location-arrow f-20 "></i>
            </button>
        </div>

    </div>
</div>
<!-- Sidebar chat end-->






























