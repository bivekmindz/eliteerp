@extends('Emp.layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>Client Excel Upload</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Admin Login</a>
                </li>
                <li class="breadcrumb-item"><a href="">Client Excel</a>
                </li>
            </ol>
        </div>



    </div>




    <div class="row">



        <!-- Textual inputs starts -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><h5 class="card-header-text">Upload Excel </h5>

                </div>

                <!-- end of modal -->
                <div class="card-block">
                    <form >

                        <div class="row">


                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label"></label>
                                    <div class="col-sm-8">
                                        <button type="button" class="btn  btn-success  waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel">Download Excel
                                            <span class="badge"><i class="icofont icofont-file-excel"></i></span>
                                        </button>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Upload Excel</label>
                                    <div class="col-sm-8">
                                        <label for="file" class="custom-file">
                                            <input type="file" id="file" class="custom-file-input">
                                            <span class="custom-file-control"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="md-input-wrapper">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Textual inputs ends -->
    </div>

</div>
@endsection