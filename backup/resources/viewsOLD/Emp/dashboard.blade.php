@extends('Emp.layouts.master')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>Dashboard</h4>
        </div>
    </div>
    <!-- 4-blocks row start -->
    <div class="row m-b-30 dashboard-header">
        <div class="col-lg-3 col-sm-6">
            <div class="dashboard-primary bg-primary">
                <div class="sales-primary">
                    <i class="icon-bubbles"></i>
                    <div class="f-right">
                        <h2 class="counter">4500</h2>
                        <span>Total Visitors</span>
                    </div>
                </div>
                <div class="bg-dark-primary">
                    <p class="week-sales">LAST WEEK'S SALES</p>
                    <p class="total-sales">432</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="bg-success dashboard-success">
                <div class="sales-success">
                    <i class="icon-speedometer"></i>
                    <div class="f-right">
                        <h2 class="counter">3521</h2>
                        <span>Total Sales</span>
                    </div>
                </div>
                <div class="bg-dark-success">
                    <p class="week-sales">LAST WEEK'S SALES</p>
                    <p class="total-sales ">432</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">
                    <i class="icon-basket-loaded"></i>
                    <div class="f-right">
                        <h2 class="counter">1085</h2>
                        <span>New Orders</span>
                    </div>
                </div>
                <div class="bg-dark-warning">
                    <p class="week-sales">LAST WEEK'S SALES</p>
                    <p class="total-sales">84</p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">
                    <i class="icon-clock"></i>
                    <div class="f-right">
                        <h2 id="system-clock"></h2>

                    </div>
                </div>
                <div class="bg-dark-facebook">
                    <p class="week-sales">Visitors</p>
                    <p class="total-sales">432</p>
                </div>
            </div>
        </div>

    </div>
    <!-- 4-blocks row end -->

    <!-- 1-3-block row start -->
    <div class="row">
        <div class="col-lg-9 col-md-8">
            <div class="col-sm-12 card">
                <div class="card-block">
                    <h6 class="m-b-20">Website Stats</h6>
                    <div id="website-stats" style="height: 420px"></div>
                </div>

            </div>
        </div>
        <div class="col-lg-3 col-md-4">
            <div class="col-sm-12 card dashboard-product">
                <span>Total Products</span>
                <h2 class="dashboard-total-products counter">4800</h2>
                <span class="label label-warning">Sales</span>Arriving Today
                <div class="side-box bg-warning">
                    <i class="icon-social-soundcloud"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4">
            <div class="col-sm-12 card dashboard-product">
                <span>Conversion Rate</span>
                <h2 class="dashboard-total-products counter">37,500</h2>
                <span class="label label-primary">Views</span>View Today
                <div class="side-box bg-primary">
                    <i class="icon-social-soundcloud"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-4">
            <div class="col-sm-12 card dashboard-product">
                <span>Total Profit</span>
                <h2 class="dashboard-total-products">$<span class="counter">30,780</span></h2>
                <span class="label label-success">Sales</span>Reviews
                <div class="side-box bg-success">
                    <i class="icon-bubbles"></i>
                </div>
            </div>
        </div>
    </div>

</div>

</div>

    @endsection