@extends('Emp.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Client Excel Upload</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href=""><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Vacancies</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Upload Resume</a>
                    </li>
                </ol>
            </div>



        </div>
        <div class="row">
            <!-- Textual inputs starts -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h5 class="card-header-text">Upload Excel </h5>
                    </div>
                    @if(Session::has('success_msg'))
                        <div class="alert alert-success">
                            {{ Session::get('success_msg') }}
                        </div>
                @endif

                <!-- end of modal -->
                    <div class="card-block">


                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label"></label>
                                    <div class="col-sm-8">
                                        <form action="{{ URL::to('exceldown') }}"  method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel">Download Excel
                                                <span class="badge"><i class="icofont icofont-file-excel"></i></span>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <form action="{{ URL::to('excelup') }}"  method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group row {{ $errors->has('catid') ? 'has-error' : '' }}">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Category</label>
                                            <div class="col-sm-8">
                                                <select class="form-control" id="catid" name="catid" required>
                                                    @foreach($dept as $key=>$value)
                                                        <option value="{{ $value->dept_id }}">{{ $value->dept_name }}</option>
                                                    @endforeach

                                                </select>

                                                <span class="text-danger">{{ $errors->first('deptid') }}</span>
                                            </div>
                                        </div>


                                        <div class="form-group row {{ $errors->has('deptid') ? 'has-error' : '' }}">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Upload Excel</label>
                                            <div class="col-sm-8">
                                                <label for="file" class="custom-file">
                                                    <input type="file" id="file" class="custom-file-input" name="upfile">
                                                    <span class="custom-file-control"></span>
                                                </label>
                                                <div class="md-input-wrapper">
                                                    <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit">
                                                    </input>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
            <!-- Textual inputs ends -->
        </div>
    </div>
    </div>
@endsection