@extends('Emp.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Client Assigned to SPOC</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Clients</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Client Assigned to spocs</a>
                    </li>
                </ol>
            </div>
        </div>



        <div class="row">

            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Client With Assigned spoc</h5>

                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <!-- <th>Category</th> -->
                                        <th>Company Name</th>
                                        <!-- <th>Contact Person Name</th> -->

                                        <th>Assigned SPOC</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @php
                                        $i=1;
                                    @endphp
                                    @foreach($clientemp as $kk => $clients)
                                        <tr class="table-active">
                                            <td>{{  $i++   }}</td>
                                            <td>{{  $clients->comp_name }}</td>
                                            <td>{{ $clients->name }}</td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
            </div>




        </div>



        @endsection=