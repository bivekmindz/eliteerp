@extends('Emp.layouts.master')
@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="main-header">
        <h4>Add Position</h4>
        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
            <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
            </li>
            <li class="breadcrumb-item"><a href="#">Vacancies</a>
            </li>
            <li class="breadcrumb-item"><a href="">Add Position</a>
            </li>
        </ol>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
               <div class="card-header"><h5 class="card-header-text">Add Position Form</h5>

               </div>
               @if(Session::has('success_msg'))
                  <div class="alert alert-success">
                     {{ Session::get('success_msg') }}
                  </div>
            @endif

            <div class="card-block">
               {!! Form::open([ 'action'=>'Emp\PositionController@addposition', 'method'=>'post', 'files'=>true ]) !!}
                    <div class="row">
                        @if(count($errors))
                           <div class="alert alert-danger">
                              <strong>Whoops!</strong> There were some problems with your input.
                              <br/>
                              <ul>
                                 @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                 @endforeach
                              </ul>
                           </div>
                        @endif
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">


                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Client Category </label>
                              <div class="col-sm-8">
                                 <select class="form-control" id="exampleSelect1" name="dept" >
                                    @foreach($departments as $kk => $value)
                                       <option value="{{$value->dept_id}}">{{$value->dept_name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>


                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Client Name</label>
                              <div class="col-sm-8">
                                 <select class="form-control" id="exampleSelect1" name="compname" >
                                    @foreach($clients as $kk => $value)
                                       <option value="{{$value->client_id}}">{{$value->comp_name}}</option>
                                    @endforeach
                                 </select>
                              </div>
                           </div>
                        </div>

                     </div>

                     <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Job Title </label>
                              <div class="col-sm-8">
                                <input class="form-control" type="text" value="" id="example-text-input" name="jobtitle">
                               </div>
                           </div>
                        </div>

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Upload JD</label>
                              <div class="col-sm-8">
                                {!! Form::file('upload_position_jd', '', ['id'=>'file', 'class'=>'custom-file-input']) !!}
                                <!-- <input type="file" id="file" class="custom-file-input" name="upload_jd">
                                <span class="custom-file-control"></span> -->
                                </label>
                              </div>
                           </div>
                        </div>

                     </div>

                     <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Number of Positions </label>
                              <div class="col-sm-8">
                                 <input class="form-control" type="text" value="" id="example-text-input" name="noofpos">
                              </div>
                           </div>
                        </div>

                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Other Information </label>
                              <div class="col-sm-8">
                                 <textarea class="form-control" id="exampleTextarea" rows="1" name="otherinfo"></textarea>
                              </div>
                           </div>
                        </div>

                     </div>


                     <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group row">
                              <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">

                                 <div class="checkbox-color checkbox-success ">
                                    <input id="checkbox3" type="checkbox" name="drive">
                                    <label for="checkbox3">
                                       Drive
                                    </label>
                                 </div>
                              </label>
                              <div class="col-sm-8">

                                 <div class="form-group">
                                    <div class="input-group date" id="datetimepicker1">
                                       <input type="text" class="form-control" placeholder="Drive Date" name="drivedate">
                                       <span class="input-group-addon">
                        <span class="icofont icofont-ui-calendar"></span>
                    </span>
                                    </div>
                                 </div>


                              </div>
                           </div>
                        </div>



                     </div>

                     <div class="md-input-wrapper">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                        </button>
                     </div>
                    {!! Form::close() !!} 
                    <!-- </form> -->
               </div>
            </div>
         </div>
         <!-- Textual inputs ends -->
      </div>

   </div>
   </div>
@endsection