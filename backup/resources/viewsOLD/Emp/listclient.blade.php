@extends('Emp.layouts.master')

@section('content')
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#boot-multiselect-demo').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });
        });
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Client List</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Clients</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Client List</a>
                    </li>
                </ol>
            </div>
        </div>



        <div class="row">

            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Client Listing</h5>
                        @if(Session::has('success_msg'))
                            <div class="alert alert-success">
                                {{ Session::get('success_msg') }}
                            </div>
                        @endif

                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Category</th>
                                        <th>Company Name</th>
                                        <th>Contact Person Name</th>
                                        <th>Designation</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Assign SPOC</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    @php
                                        $i=1;
                                    @endphp
                                    @foreach($clients as $kk => $clients)
                                        <tr class="table-active">
                                        <td>{{  $i++   }}</td>
                                        <td> @if($clients->client_catid==1)
                                                {{ "IT" }}
                                            @else
                                                {{ "NON IT"  }}
                                            @endif
                                        </td>
                                        <td>{{  $clients->comp_name }}</td>
                                        <td>{{  $clients->contact_name }}</td>
                                        <td>{{ $clients->designation }}</td>
                                        <td>{{  $clients->phone }}</td>
                                        <td>{{ $clients->email }}</td>
                                        <td><div class="btn-group btn-group-sm" style="float: none;">
                                                <button type="button" href="#success-{{ $clients->client_id  }}" data-toggle="modal" class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span></button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="success-{{ $clients->client_id  }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header modal-header-success">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                                <h1><i class="glyphicon glyphicon-thumbs-up"></i>Assign SPOC</h1>
                                                            </div>
                                                            <div class="modal-body">
                                                                <form action="{{ route('assignspoc',['client_id' =>  Crypt::encrypt($clients->client_id) ] ) }} " method="POST"  >
                                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                    <div class="form-group row">
                                                                        <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Company Name</label>
                                                                        <div class="col-sm-9">
                                                                            <input class="form-control" type="text"  id="example-text-input" name="cname" id="cname" value=" {{ $clients->comp_name }} " >
                                                                        </div>
                                                                    </div>


                                                                    <div class="form-group row">
                                                                        <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Employee List</label>
                                                                        <div class="col-sm-9">
                                                                            <div class="form-group">
                                                                                <select id="boot-multiselect-demo" multiple="multiple"  name="empid[]" class="multiselect-ui form-control">
                                                                                    @foreach( $user as $key=>$users )
                                                                                        <option value="{{ $users->id }}">{{ $users->name }}</option>
                                                                                    @endforeach
                                                                                </select>



                                                                            </div>

                                                                        </div>

                                                                    </div>



                                                            <div class="modal-footer">

                                                                <div class="md-input-wrapper">
                                                                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                                                                    <input type="submit" class="btn btn-primary waves-effect waves-light" value="submit">
                                                                    </input>

                                                                </div>
                                                            </div>

                                                            </form>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
                                            </div>
                                        </td>


                                    </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
            </div>




        </div>



    @endsection