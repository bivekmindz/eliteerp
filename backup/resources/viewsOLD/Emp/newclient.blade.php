@extends('Emp.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Add New Client</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Clients</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Add New Client</a>
                    </li>
                </ol>
            </div>



        </div>

        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h5 class="card-header-text">Add Client</h5>

                    </div>
                    @if(Session::has('success_msg'))
                        <div class="alert alert-success">
                            {{ Session::get('success_msg') }}
                        </div>
                @endif

                <!-- end of modal -->
                    <div class="card-block">
                        <form  action="{{ route('saveclient')  }}"  method="POST" >
                            @if(count($errors))
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.
                                    <br/>
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                         @endforeach
                                    </ul>
                                </div>
                            @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group row {{ $errors->has('deptid') ? 'has-error' : '' }}">

                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Category</label>
                                <div class="col-sm-5">
                                    <select class="form-control" id="deptid" name="deptid" required>
                                        @foreach($dept as $key=>$value)
                                            <option value="{{ $value->dept_id }}">{{ $value->dept_name }}</option>
                                        @endforeach

                                    </select>
                                    <span class="text-danger">{{ $errors->first('deptid') }}</span>
                                </div>
                            </div>

                            <div class="form-group row {{ $errors->has('cname') ? 'has-error' : '' }}">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Company Name</label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="" id="example-text-input"  name="cname" required>
                                    <span class="text-danger">{{ $errors->first('cname') }}</span>
                                </div>
                            </div>

                            <div class="form-group row {{ $errors->has('cpn') ? 'has-error' : '' }}">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Contact Person Name</label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="" id="example-text-input" name="cpn" required>
                                    <span class="text-danger">{{ $errors->first('cpn') }}</span>

                                </div>
                            </div>



                            <div class="form-group row   {{ $errors->has('desig') ? 'has-error' : '' }}">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Designation</label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="" id="example-text-input" name="desig"  id="desig" required >
                                    <span class="text-danger">{{ $errors->first('desig') }}</span>

                                </div>
                            </div>

                            <div class="form-group row   {{ $errors->has('phone') ? 'has-error' : '' }}">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Phone</label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="" id="example-text-input" name="phone" required>
                                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                                </div>
                            </div>


                            <div class="form-group row  {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Email</label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="" id="example-text-input" name="email" required >
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Upload Company Profile</label>
                                <div class="col-sm-5">
                                    <label for="file" class="custom-file" style="width:100%;">
                                        <input type="file" id="file" class="custom-file-input">
                                        <span class="custom-file-control"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="md-input-wrapper">
                                <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit">
                                </input>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- Textual inputs ends -->
        </div>




    </div>

    </div>

@endsection
