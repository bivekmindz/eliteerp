@extends('Emp.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Position Listing</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Position</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Position Listing</a>
                    </li>
                </ol>
            </div>
        </div>



        <div class="row">

            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Position Listing</h5>

                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Category</th>
                                        <th>Company Name</th>
                                        <th>Contact Person Name</th>
                                        <th>Designation</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Assign SPOC</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    @php
                                        $i=1;
                                    @endphp
                                    {{$clients}}
                                    @foreach($clients as $kk => $c)
                                        <tr class="table-active">
                                            <td>{{  $i++   }}</td>
                                            <td> 
                                           N/A
                                            </td>
                                            <td>{{  $c->comp_name }}</td>
                                            <td>{{  $c->contact_name }}</td>
                                            <td>{{ $c->designation }}</td>
                                            <td>{{ $c->phone }}</td>
                                            <td>{{ $c->email }}</td>
                                            <td></td>


                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
            </div>




        </div>



@endsection