<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\App;

Route::get('/', 'Auth\LoginController@showLoginForm');

Auth::routes();


Route::get('/home', ['as' => 'home', 'uses'=> 'HomeController@index']);
Route::get('report',['as'=>'report','uses'=>'Admin\ReportController@report']);
Route::get('unauthorized',['as'=>'unauthorized','uses'=>'HomeController@unauthaccess']);
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('changepasword', '\App\Http\Controllers\Auth\LoginController@changepasword');
Route::get('dashboard', ['as' => 'dashboard', 'uses'=> 'Emp\DashController@index']);
Route::post('dashboard', ['as' => 'dashboard', 'uses'=> 'Emp\DashController@index']);
Route::get('dashboardval', ['as' => 'dashboardval', 'uses'=> 'Emp\DashController@dashboardval']);
Route::get('profile',['as'=>'profile','uses'=>'Emp\DashController@profile']);
Route::get('reset',['as'=>'reset','uses'=>'Emp\DashController@reset']);
Route::get('passwordreset',['as'=>'passwordreset','uses'=>'Emp\DashController@passwordreset']);
Route::post('passwordreset',['as'=>'passwordreset','uses'=>'Emp\DashController@updatepasswordreset']);
Route::get('/cvparsing',['as'=>'cvparsing','uses'=>'Emp\RecController@cvsearch']);
Route::get('cvparsing/profiles',['as'=>'cvparsing','uses'=>'Emp\RecController@cvparsing']);


Route::get('chat',['as'=>'chat','uses'=>'Emp\ChatController@index']);
Route::post('insertchat',['as'=>'insertchat','uses'=>'Emp\ChatController@insertchat']);
Route::post('insertchatenter',['as'=>'insertchatenter','uses'=>'Emp\ChatController@insertchatenter']);
Route::post('autorefresh',['as'=>'autorefresh','uses'=>'Emp\ChatController@autorefresh']);
Route::get('chat/{id}',['as'=>'chat','uses'=>'Emp\ChatController@index']);


 Route::get('detailprofile', ['as' => 'detailprofile', 'uses'=>'Emp\RecController@detailprofile']);


Route::get('uploadallcv', ['as' => 'uploadallcv', 'uses'=>'Emp\Cvscript@uploadallcv']);


Route::get('breaktime', ['uses'=>'Emp\RecController@breaktime']);
Route::get('breaktimeback', ['as' => 'breaktimeback', 'uses'=>'Emp\RecController@breaktimeback']);
Route::get('reasonfordelay', ['as' => 'reasonfordelay', 'uses'=>'Emp\RecController@reasonfordelay']);
Route::post('clientstart', ['as' => 'clientstart', 'uses'=>'Emp\RecController@clientstart']);
// Route::post('cvparsing','Emp\RecController@cvparsing']);
Route::get('alluserswithbreak', ['as' => 'alluserswithbreak', 'uses'=>'Admin\ReportController@alluserswithbreak']);
Route::get('resupldelayreport', ['as' => 'resupldelayreport', 'uses'=>'Admin\ReportController@resupldelayreport']);
Route::get('adsearch', ['as' => 'adsearch', 'uses'=>'Emp\SearchController@advancesearch']);
Route::post('advancesearchmail', ['as' => 'advancesearchmail', 'uses'=>'Emp\SearchController@advancemail']);
Route::get('allocatedcvs', ['as' => 'allocatedcvs', 'uses'=>'Emp\DriveController@allocatedcvs']);
Route::get('viewallocatedcvdetail/{assignfrom}/{assignto}', ['as' => 'viewallocatedcvdetail', 'uses'=>'Emp\DriveController@viewallocatedcvdetail']);

Route::post('submission',  ['as' => 'submission', 'uses'=>'Emp\RecController@submission']);


Route::get('myallocatedcvs/', ['as' => 'myallocatedcvs', 'uses'=>'Emp\DriveController@myallocatedcvs']);
Route::get('viewmyallocatedcvdetail/{assignfrom}/{assignto}', ['as' => 'viewmyallocatedcvdetail', 'uses'=>'Emp\DriveController@viewmyallocatedcvdetail']);

Route::post('allcvdetupdate', ['as' => 'allcvdetupdate', 'uses'=>'Emp\DriveController@allcvdetupdate']);

Route::get('viewmyallocatedcvvalue/{assignfrom}/{assignto}', ['as' => 'viewmyallocatedcvvalue', 'uses'=>'Emp\DriveController@viewmyallocatedcvvalue']);

Route::post('changecvstatus', ['as' => 'changecvstatus', 'uses'=>'Emp\DriveController@changecvstatus']);

Route::post('changecvafterfollstatus', ['as' => 'changecvafterfollstatus', 'uses'=>'Emp\DriveController@changecvafterfollstatus']);

Route::group(['middleware' => ['role']], function () {

    //common functions to all users
    
    //BD CONTROLLERS
    Route::get('getresumesreport/{id}/{fid}/{cid}/{did}',['as'=>'getresumesreport','uses'=>'Emp\RecController@getresumesreport']);


      Route::post('searchusers',['as'=>'searchusers','uses'=>'Emp\RecController@searchusers']);
         Route::post('searchclients',['as'=>'searchclients','uses'=>'Emp\RecController@searchclients']);
 Route::post('searchposition',['as'=>'searchposition','uses'=>'Emp\RecController@searchposition']);
    Route::get('getrecruiters/{id}',['as'=>'getrecruiters','uses'=>'Emp\RecController@getrecruiters']);
    Route::get('getunassignedrecruiters/{id}',['as'=>'getunassignedrecruiters','uses'=>'Emp\RecController@getunassignedrecruiters']);
    Route::post('delassrecruiters',['as'=>'delassrecruiters','uses'=>'Emp\RecController@delassrecruiters']);

    Route::post('delposition',['as'=>'delposition','uses'=>'Emp\RecController@delposition']);
    
    Route::post('updateassrecruiters',['as'=>'updateassrecruiters','uses'=>'Emp\RecController@updateassrecruiters']);

Route::get('cvsubmissionreport',['as'=>'cvsubmissionreport','uses'=>'Emp\RecController@cvsubmission']);


    Route::get('newclient',['as'=>'newclient','uses'=>'Emp\DashController@newclient']);
    Route::post('newclient',['as' => 'saveclient', 'uses' => 'Emp\DashController@saveclient']);
   
    Route::post('ajaxlistclient',['as'=>'ajaxlistclient','uses'=>'Emp\DashController@ajaxlistclient']);
    
    Route::get('clientlists',['as'=>'clientlists','uses'=>'Emp\DashController@clientlists']);
    Route::post('ajaxclientlists',['as'=>'ajaxclientlists','uses'=>'Emp\DashController@ajaxclientlists']);


    Route::post ('assignspoc',['as'=>'assignspoc','uses'=>'Emp\DashController@assignspoc']);
    Route::post ('updateassignspoc',['as'=>'updateassignspoc','uses'=>'Emp\DashController@updateassignspoc']);
        Route::post ('updateunassignspoc',['as'=>'updateunassignspoc','uses'=>'Emp\DashController@updateunassignspoc']);

    Route::get('editclient/{id}',['uses'=>'Emp\ClientXLController@editclient']);
    Route::post('editclient/{id}',['uses'=>'Emp\ClientXLController@updateClient']);
    Route::get('clientspoc',['as'=>'clientspoc','uses'=>'Emp\DashController@clientspoc']);
    Route::post('ajaxclientspoc',['as'=>'ajaxclientspoc','uses'=>'Emp\DashController@ajaxclientspoc']);


    // Route::get('unassignspoc',['as'=>'unassignspoc','uses'=>'Emp\DashController@clientspoc']);
    
    Route::get('client-excel',['as'=>'client-excel','uses'=>'Emp\ClientXLController@index']);
    Route::post('exceldown1',['uses'=>'Emp\ClientXLController@exceldown'])->name('exceldown1');
    Route::post('clientlistexceldown',['uses'=>'Emp\ClientXLController@clientlistexceldown'])->name('clientlistexceldown');
    Route::post('excellistclientdownload',['uses'=>'Emp\ClientXLController@excellistclientdownload'])->name('excellistclientdownload');
    Route::post('excellistpositiondownload',['uses'=>'Emp\ClientXLController@excellistpositiondownload'])->name('excellistpositiondownload');

    Route::post('excelup1',['uses'=>'Emp\ClientXLController@excelup'])->name('excelup1');
    Route::get('getspoctoclient/{id}',['as'=>'getspoctoclient','uses'=>'Emp\DashController@getspoctoclient']);
    Route::get('getassignspoctoclient/{id}',['as'=>'getassignspoctoclient','uses'=>'Emp\DashController@getassignspoctoclient']);

    Route::get('getemployeebyclientid/{id}',['as'=>'getemployeebyclientid','uses'=>'Emp\DashController@getemployeebyclientid']);
 //Route::get('dashboardval', ['as' => 'dashboard', 'uses'=> 'Emp\DashController@index']);

    Route::post('excellistclientspocdownload',['uses'=>'Emp\ClientXLController@excellistclientspocdownload'])->name('excellistclientspocdownload');

    Route::post('excellistassignrecruiterdownload',['uses'=>'Emp\ClientXLController@excellistassignrecruiterdownload'])->name('excellistassignrecruiterdownload');

    Route::post('excellistassignclientdownload',['uses'=>'Emp\ClientXLController@excellistassignclientdownload'])->name('excellistassignclientdownload');

// Route::post('excellistclientspocdownload',['as'=>'excellistclientspocdownload','uses'=>'Emp\DashController@excellistclientspocdownload']);




//SPOC Controllers


    Route::post('assigncvcall',['as'=>'assigncvcall','uses'=>'Emp\DriveController@assigncvcall']);
    Route::get('positiondrive',['as'=>'positiondrive','uses'=>'Emp\DriveController@positiondrive']);
    Route::post('ajaxpositiondrive',['as'=>'ajaxpositiondrive','uses'=>'Emp\DriveController@ajaxpositiondrive']); 
    Route::get('positionform', ['as' => 'positionform','uses'=>'Emp\PositionController@position']);
    Route::post('insertposition', ['as' => 'insertposition', 'uses' => 'Emp\PositionController@save']);
    Route::post('addposition',['as'=>'addposition','uses'=>'Emp\PositionController@addposition']);
    Route::get('listallposition',['as'=>'listallposition','uses'=>'Emp\PositionController@allposition']);

    Route::get('positionlist',['as'=>'positionlist','uses'=>'Emp\PositionController@positionlist']);
    Route::post('positionlisting',['as'=>'positionlisting','uses'=>'Emp\PositionController@test_ajax']);
    Route::get('positionjd/{id}',['as'=>'positionjd','uses'=>'Emp\PositionController@positionjd']);

    Route::get('editposition/{id}',['uses'=>'Emp\PositionController@editposition']);
    Route::post('editposition/{id}',['uses'=>'Emp\PositionController@updateposition']);


    Route::get('shortlistcv',['as'=>'shortlistcv','uses'=>'Emp\CVController@shortlistcv']);
 Route::post('cvshortlisting',['as'=>'cvshortlisting','uses'=>'Emp\CVController@cvshortlisting']);
    

    Route::post('savecandidate',['as'=>'savecandidate','uses'=>'Emp\CVController@savecandidate']);
   

    Route::get('cvparsing/getcandidatedetails/{id}',['as'=>'cvparsing/getcandidatedetails','uses'=>'Emp\CVController@getcandidatedetails']);

    Route::get('vacancy',['as'=>'vacancy','uses'=>'Emp\VacancyController@vacancy']);
    Route::post('ajaxvacancy',['as'=>'ajaxvacancy','uses'=>'Emp\VacancyController@ajaxvacancy']);
    Route::get('candidate-list',['as'=>'candidate-list','uses'=>'Emp\VacancyController@candidateList']);
    Route::post('ajaxcandidate-list',['as'=>'ajaxcandidate-list','uses'=>'Emp\VacancyController@ajaxcandidateList']);
    Route::get('viewallassignedrecuiterposition',['as'=>'viewallassignedrecuiterposition','uses'=>'Emp\RecController@viewallassignedrecuiterposition']);
    Route::post('ajaxviewallassignedrecuiterposition',['as'=>'ajaxviewallassignedrecuiterposition','uses'=>'Emp\RecController@ajaxviewallassignedrecuiterposition']);
    Route::get('assignclient',['as'=>'assignclient','uses'=>'Emp\PositionController@assignclient']);
    Route::post('ajaxassignclient',['as'=>'ajaxassignclient','uses'=>'Emp\PositionController@ajaxassignclient']);
    Route::get('cvlist',['as'=>'cvlist','uses'=>'Emp\CVController@cvlist']);
    Route::post('cvupload',['as'=>'cvupload','uses'=>'Emp\CVController@cvupload']);
    Route::get('getcandidatedetails1',['as'=>'getcandidatedetails1','uses'=>'Emp\CVController@getcandidatedetails1']);
    Route::post('cvtime', ['as' => 'assignedposition', 'uses'=>'Emp\CVController@cvtime']);



//Recuriter Controllers
    Route::get('get-assigned-postion/{id}', ['as' => 'assignedposition', 'uses'=>'Emp\RecController@getassignedposition']);

    Route::post('searchcandidate',['as'=>'searchcandidate','uses'=>'Emp\RecController@searchcandidate']);

 Route::get('edit-assigned-postion/{id}/{fid}', ['as' => 'edit-assigned-postion', 'uses'=>'Emp\CVController@editassignedpostion']);


Route::post('edituploadcv', ['uses'=>'Emp\CVController@edituploadcv']);

    // Route::get('get-assigned-postion/{id}', ['as' => 'assignedposition', 'uses'=>'Emp\RecController@getassignedposition']);
   // Route::get('allreasonfordelay', ['as' => 'allreasonfordelay', 'uses'=>'Emp\PositionController@allreasonfordelay']);

    Route::post('get-assigned-postion/uploadcv', ['as' => 'uploadcv', 'uses'=>'Emp\RecController@uploadcv']);
    Route::get('get-assigned-postion/gettime/{id}', ['uses'=>'Emp\RecController@gettime']);
    Route::get('get-assigned-postion/endtime/{id}', ['as' => 'endtime', 'uses'=>'Emp\RecController@endtime']);
    Route::post('duplicatecvupload',['as'=>'duplicatecvupload','uses'=>'Emp\RecController@duplicatecvupload']);
   
    Route::post('resubmission', ['as' => 'resubmission', 'uses'=>'Emp\RecController@resubmission']);

    
Route::post('view', ['as' => 'view', 'uses'=>'Emp\RecController@view']);
Route::post('poscheck', ['as' => 'poscheck', 'uses'=>'Emp\RecController@poscheck']);
 Route::post('viewupdatespocstatus', ['as' => 'viewupdatespocstatus', 'uses'=>'Emp\RecController@viewupdatespocstatus']);  

 Route::get('searchrofile',['as'=>'searchprofile','uses'=>'Emp\SearchCandidate@index']);


Route::post('searchcandidateprofile',['as'=>'searchcandidateprofile','uses'=>'Emp\SearchCandidate@searchcandidate']);


  

Route::get('get-assigned-postion/sendAutomaticMail/{id}',['as'=>'sendAutomaticMail','uses'=>'Emp\RecController@sendAutoMail']);

    Route::get('testtimer',['as'=>'testtimer','uses'=>'Emp\RecController@testautotime']);

    Route::post('saveresume',['as'=>'saveresume','uses'=>'Emp\RecController@saveresume']);
});
Route::get('positionassignedreqbyspoc',['as'=>'positionassignedreqbyspoc','uses'=>'Emp\PositionController@positionassignedreqbyspoc']);

Route::get('submissionlist',['as'=>'submissionlist','uses'=>'Emp\PositionController@submissionlist']);


Route::post('positionallocate', 'Emp\PositionController@positionallocate')->name('admin.admindashboard');
Route::group(['prefix' => 'admin','middleware'=>['role']],function () {
    Route::get('admindashboard', 'Admin\AdminController@index')->name('admin.admindashboard');
    Route::get('adminrole', 'Admin\AdminController@adminrole')->name('admin.adminrole');
    Route::get('viewadminrole', 'Admin\AdminController@viewadminrole')->name('admin.viewadminrole');
    Route::post('addadminrole', 'Admin\AdminController@addadminrole')->name('admin.addadminrole');
    Route::get('admindepartment', 'Admin\AdminController@admindepartment')->name('admin.admindepartment');
    Route::get('viewadmindepartment', 'Admin\AdminController@viewadmindepartment')->name('admin.viewadmindepartment');
    Route::post('addadmindepartment', 'Admin\AdminController@addadmindepartment')->name('admin.addadmindepartment');
    Route::get('adminclient', 'Admin\AdminController@adminclient')->name('admin.adminclient');
    
    Route::get('viewadminemployes', 'Admin\AdminController@adminemployes')->name('admin.viewadminemployes');
    Route::post('ajaxviewadminemployes', 'Admin\AdminController@ajaxadminemployes')->name('admin.ajaxviewadminemployes');


Route::get('search', 'Admin\AdminController@search');


    Route::get('employee', 'Admin\AdminController@employee')->name('admin.employee');
    Route::post('usersave', 'Admin\AdminController@addadminusers')->name('admin.usersave');
    // Route::get('editadminemployee/{id}',['as'=>'editadminemployee', 'uses'=>'Admin\AdminController@adminemployes']);
    Route::get('/editadminemployes/{id}', 'Admin\AdminController@editadminemployes')->name('admin.editadminemployes');
    Route::post('/updateadminemployes', 'Admin\AdminController@updateadminemployes')->name('admin.updateadminemployes');
    Route::get('/deleteadminemployes/{id}', 'Admin\AdminController@deleteadminemployes')->name('admin.deleteadminemployes');
    Route::get('/adminchangepassword', 'Admin\AdminController@changepassword')->name('admin.adminchangepassword');
    Route::post('/adupchangepassword', 'Admin\AdminController@adupchangepassword')->name('admin.adupchangepassword');
    Route::post('checkemail',['as'=>'checkemail','uses'=>'Admin\AdminController@checkemail']);
    Route::get('exceldownload',['as'=>'empexcel','uses'=>'Admin\EmployeeController@exceldownload']);
    Route::get('excelattendancedownload',['as'=>'excelattendancedownload','uses'=>'Admin\EmployeeController@excelattendancedownload']);
     Route::get('excelclientdownload',['as'=>'excelclientdownload','uses'=>'Admin\EmployeeController@excelclientdownload']); 
    Route::get('teamassign',['as'=>'teamassign','uses'=>'Admin\TeamController@index']);
    Route::get('allocateposition', ['as' => 'allocateposition', 'uses'=>'Admin\RecruiterController@getlist']);
    Route::get('getemployees',['as'=>'getemployees', 'uses'=>'Admin\RecruiterController@getEmpList']);
    Route::post('allocateposition', ['as' => 'allocatepositionpost', 'uses'=>'Admin\RecruiterController@postRecruiter']);
    Route::get('autocomplete', ['as' => 'autocomplete', 'uses'=>'Admin\RecruiterController@autocomplete']);
    Route::get('/team',['as'=>'team','uses'=>'Admin\TeamController@team']);
    Route::post('/team',['as'=>'team','uses'=>'Admin\TeamController@teamSave']);
    Route::get('editteam/{id}',['as'=>'editteam','uses'=>'Admin\TeamController@editteam']);
    Route::post('editteam/{id}',['as'=>'editteam','uses'=>'Admin\TeamController@teamUpdate']);
    Route::get('deleteteam/{id}',['as'=>'deleteteam','uses'=>'Admin\TeamController@deleteteam']);
    Route::post('teamuniquetitle',['as'=>'teamuniquetitle','uses'=>'Admin\TeamController@uniqueteam']);
    Route::get('hrattupanddown',['as'=>'hrattupanddown','uses'=>'Admin\EmployeeController@hrattupanddown']);

    Route::get('empexcel',['as'=>'empexcel','uses'=>'Admin\EmployeeController@empexcel']);
    Route::post('excelup',['as'=>'excelup','uses'=>'Admin\EmployeeController@excelup']);
    Route::get('exceldown',['as'=>'exceldown','uses'=>'Admin\EmployeeController@exceldown']);

    Route::get('addmenu',['as'=>'addmenu','uses'=>'Admin\MenuController@addmenu']);
    Route::post('savemenu',['as'=>'savemenu','uses'=>'Admin\MenuController@savemenu']);
    Route::get('assignmenu',['as'=>'assignmenu','uses'=>'Admin\MenuController@assignmenu']);
    Route::post('getmenubyrole',['uses'=>'Admin\MenuController@getmenubyrole']);
    Route::get('getmenubyrole/{id}',array('as' => 'getmenubyrole', function($id) { return View::make('admin.assignmenu')->with('acccess', $id); }));

    Route::get('childmenu/{id}',['as'=>'childmenu','uses'=>'Admin\MenuController@getchildmenu']);
    Route::get('parentmenu/{id}',['as'=>'parentmenu','uses'=>'Admin\MenuController@getparentmenu']);

    Route::get('child',['as'=>'child','uses'=>'Admin\MenuController@getchildmenu']);
    Route::post('assignmbyrole',['as'=>'assignmbyrole','uses'=>'Admin\MenuController@assignmbyrole']);
    Route::get('viewassignedrecuiterposition',['as'=>'viewassignedrecuiterposition','uses'=>'Admin\RecruiterController@viewassignedrecuiterposition']);
    Route::post('addpositionadmin',['as'=>'addpositionadmin','uses'=>'Admin\AdminController@addpositionadmin']);
    Route::get('adminAddPosition',['as'=>'adminAddPosition','uses'=>'Admin\AdminController@adminPosition']);
    Route::get('adminposlist',['as'=>'adminposlist','uses'=>'Admin\AdminController@adminposlist']);
    Route::get('adminpositionjd',['as'=>'adminpositionjd','uses'=>'Admin\AdminController@adminpositionjd']);
    Route::get('attendanceexcel',['as'=>'attendanceexcel','uses'=>'Admin\EmployeeController@attendanceexcel']);

    Route::post('excelattendanceup',['as'=>'excelattendanceup','uses'=>'Admin\EmployeeController@excelattendanceup']);
    Route::get('fworkemployee',['as'=>'fworkemployee','uses'=>'Admin\EmployeeController@fworkemployee']);

 
});








 Route::get('Sevenoclockclientreports', ['as' => 'Sevenoclockclientreports', 'uses'=>'Admin\ReportsController@Sevenoclockclientreports']);



 Route::get('joiningpipelinereports', ['as' => 'joiningpipelinereports', 'uses'=>'Admin\ReportsController@joiningpipelinereports']);


 Route::get('recuiterjoiningpipelinereports', ['as' => 'recuiterjoiningpipelinereports', 'uses'=>'Admin\ReportsController@recuiterjoiningpipelinereports']);
 Route::get('lunchbreakreports', ['as' => 'lunchbreakreports', 'uses'=>'Admin\ReportsController@lunchbreakreports']);

Route::get('getspockstatus/{id}/{recruiterid}/{cvstatus}/{posiid}',['as'=>'getspockstatus','uses'=>'Admin\ReportsController@getspockstatus']);




Route::post('updateclientstatuspopup', ['as' => 'updateclientstatuspopup', 'uses'=>'Admin\ReportsController@updateclientstatuspopup']);

Route::post('updatespockresume', ['as' => 'updatespockresume', 'uses'=>'Admin\ReportsController@updatespockresume']);
Route::post('updaterecresume', ['as' => 'updaterecresume', 'uses'=>'Admin\ReportsController@updaterecresume']);

Route::post('updateclientstatuspopupresume', ['as' => 'updateclientstatuspopupresume', 'uses'=>'Admin\ReportsController@updateclientstatuspopupresume']);


Route::post('updateclientstatusrecuiter', ['as' => 'updateclientstatusrecuiter', 'uses'=>'Admin\ReportsController@updateclientstatusrecuiter']);


 Route::get('recuiterpositionreports', ['as' => 'recuiterpositionreports', 'uses'=>'Admin\ReportsController@recuiterpositionreports']);

Route::get('getresumesdatareport/{id}/{fid}/{cid}/{nid}/{mid}',['as'=>'getresumesreportdata','uses'=>'Admin\ReportsController@getresumesreportdata']);


Route::get('getresumespopup/{id}',['as'=>'getresumespopup','uses'=>'Admin\ReportsController@getresumespopup']);

 Route::get('shortlistcv/getcandidatedetails/{id}',['as'=>'getcandidatedetails','uses'=>'Emp\CVController@getcandidatedetails']);
Route::get('getcandidatedetails/{id}',['as'=>'getcandidatedetails','uses'=>'Emp\CVController@getcandidatedetails']);
 Route::get('listclient',['as'=>'listclient','uses'=>'Emp\DashController@listclient']);

Route::get('getspockposition/{id}',['as'=>'getspockposition','uses'=>'Admin\ReportsController@getspockposition']);

Route::get('viewmyallocatedcvdetail/{nid}/getspockposition/{id}',['as'=>'getspockposition','uses'=>'Admin\ReportsController@getspockposition']);


Route::get('viewmyallocatedcvdetail/{poid}/getspockstatusallcv/{id}/{recruiterid}/{cvstatus}/{posiid}',['as'=>'getspockstatusallcv','uses'=>'Admin\ReportsController@getspockstatusallcv']);
Route::post('updatespockresume', ['as' => 'updatespockresume', 'uses'=>'Admin\ReportsController@updatespockresume']);


 Route::get('recuiterofferreports', ['as' => 'recuiterofferreports', 'uses'=>'Admin\ReportsController@recuiterofferreports']);

 Route::get('Spocofferreports', ['as' => 'Spocofferreports', 'uses'=>'Admin\ReportsController@Spocofferreports']);
Route::post('updatecrecuiterdata', ['as' => 'updatecrecuiterdata', 'uses'=>'Admin\ReportsController@updatecrecuiterdata']);
Route::get('getresumesrecuiterofferreport/{id}', ['as' => 'getresumesrecuiterofferreport', 'uses'=>'Admin\ReportsController@getresumesrecuiterofferreport']);

Route::post('updaterecfollowup', ['as' => 'updaterecfollowup', 'uses'=>'Admin\ReportsController@updaterecfollowup']);

  Route::get('sendMail',['as'=>'sendMail','uses'=>'Emp\StartmailController@sendMail']);
/*  viewmyallocatedcvdetail/8/
|--------------------------------------------------------------------------
| admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
//*/
//Route::prefix('admin')->group(function () {
//    //Route::get('/', 'Admin\AdminController@index')->name('admin.dashboard');
//    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
//    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
//    Route::get('/logout','Auth\AdminLoginController@logout')->name('admin.logout');
//
//
//    Route::get('dashboard','Admin\AdminController@index')->name('admin.dashboard');
//
//
//});
