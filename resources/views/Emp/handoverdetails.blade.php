@extends('Emp.layouts.master')

@section('content')

<style type="text/css">
  #search_filter_filter{float:left!important;}
</style>


<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>Hand Over Details</h4>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
                @endif
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                       
                            
                     <div class="table-responsive table_green siro">       
                            <table class="table table-bordered" id="">
                                <thead>
                                <tr>
                                    <th>S No</th>
                                                <th>Old Recuiter Name</th>
                                                <th>Position Name</th>
                                                 <th>New Recuiter Name</th>
                                                 <th>Hand Over Date</th>
                                              
                                </tr>
                                </thead>
                                <tbody id="suggesstion-box">
                                @php
                                $i=1;
                                @endphp
                                  @foreach($emp_list as $key => $val)
                               
                                <tr  >
                                     <td>{{  $i++ }}</td>
                                            <td>{{  $val->oldrec }}</td>
                                              <td>{{  $val->posname }}</td>
                                                <td>{{  $val->newrec }}</td>
                                                 <td>{{  $val->posh_createdat }}</td>
                </tr>
               
                @endforeach
                </tbody>
                </table>
            </div>  </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
function getclientreq_id(clientreq_id)
{
 //   alert();
    $("#clientreq_id").val(clientreq_id);
 
}
        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
</script>
@endsection

