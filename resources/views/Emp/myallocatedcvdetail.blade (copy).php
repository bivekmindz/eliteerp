@extends('Emp.layouts.master1')

@section('content')

<style type="text/css">
  #search_filter_filter{float:left!important;}
</style>


<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View CV List</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Client Call CV List</a>
                </li>
                <li class="breadcrumb-item"><a href="">Allocated CVs List Details</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
                @endif
                <div class="card-block">
                    <form action="{{ URL::to('allocatedcvupdate') }}" method="post"> 
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                        <div id="search_filter_filter" class="dataTables_filter"><label>
<input type="submit" name="submit" value="change status">
<!-- <button type="button"  id="buttonClass" class="btn btn-primary waves-effect waves-light">Change Status</button> -->

             <select name="changestatus"  onchange="changestatusalloc()" id='select_id'>
       <option value='-1' >Search Status</option>
        <option value="Pending">Pending</option>
         <option value="Not Going" >Not Going</option>
        <option value="Not Reachable">Not Reachable</option>
       <option value="Not Responding">Not Responding</option>
         <option value="Confirm">Confirm</option>
</select>        <!--      <input type="submit" name="submit" value="change status"> --></label>
<input type="hidden" name="clientid" value="{{$clientid}}" id='clientid'>
<input type="hidden" name="recruiterId" value="{{$recruiterId}}" id='recruiterId'>

                   </div>
                            <table class="table" id="">
                                <thead>
                                <tr>
                                    <th></th>
                                  
                                    <th>CandidateName</th>
                                    <th>CV Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>FollowUp Status</th>
                                    <th>Action</th> 
                                </tr>
                                </thead>
                                <tbody id="suggesstion-box">
                                @php
                                $i=1;
                                @endphp
                                @foreach($getuploadedCv as $key => $p)
                            <?php
                         

  if($p->cv_status=='1'){$cvstatus="Not Seen";}elseif($p->cv_status=='2'){$cvstatus="Send To Client";}
        elseif($p->cv_status=='3'){$cvstatus="Rejected";}elseif($p->cv_status=='4'){$cvstatus="Offer";}
            elseif($p->cv_status=='5'){$cvstatus="Pipeline";}elseif($p->cv_status=='6'){$cvstatus="Join";}elseif($p->cv_status=='7'){$cvstatus="On The Way";}elseif($p->cv_status=='8'){$cvstatus="Interviewed";}elseif($p->cv_status=='9'){$cvstatus="Not Going";}elseif($p->cv_status=='10'){$cvstatus="Declined";}elseif($p->cv_status=='15'){$cvstatus="Hold";}elseif($p->cv_status=='16'){$cvstatus="Drop";}else{$cvstatus="Pending";}



?>   
                                <tr >
                                    <td><input type="checkbox" name="checkboxid" value="{{$p->id}}"></td>
                                   
                                    <td>{{ !empty($p->candidate_name) ? $p->candidate_name : 'N/A' }}</td>
                                    <td>{{ !empty($p->cv_name) ? $p->cv_name : 'N/A' }}</td>
                                    <td>{{ !empty($p->candidate_email) ? $p->candidate_email : 'N/A' }}</td>
                                    <td>{{ !empty($p->candidate_mob) ? $p->candidate_mob : 'N/A' }}</td>
                                    <td>{{ $p->tca_followupstatus }}</td>
                                    <td>
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="tcaid[]" value="{{$p->tca_id}}">
        


<div id="place{{$p->tca_id}}"></div> 
{{ !empty($p->cv_status) ? $cvstatus : 'N/A' }}
                                    </td>

                                    <td class='title'><button type='button'  class='det_css' data-toggle='modal' data-target='#myModal' onclick='getposiionvalue(<?php echo $p->position_id; ?>),getupdatespock(<?php echo $p->id; ?>,<?php echo $p->recruiter_id; ?>,<?php echo $p->cv_status; ?>,<?php echo $p->position_id; ?>)(this.value),getpositionspock(<?php echo $p->id; ?>,<?php echo $p->recruiter_id; ?>,<?php echo $p->cv_status; ?>)' ><span class='icofont icofont-eye-alt'></span></button> 
  <select name="changestatusval[]" onchange="changestatusdata({{$p->id}})"id="select_id{{$p->id}}">
                                            <option>Change Status</option>
                            <option value="{{$p->id}}/{{$p->position_id}}/7" >On The Way</option>
              <option value="{{$p->id}}/{{$p->position_id}}/8">Interviewed</option>
              <option value="{{$p->id}}/{{$p->position_id}}/9">Not Going</option>
                <option value="{{$p->id}}/{{$p->position_id}}/17">Not Responding</option>
                 <option value="{{$p->id}}/{{$p->position_id}}/18">Confirmed</option>
                                        </select> 


                                     </td>







                </tr>
              
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
</div>
</div>


<div class="container">
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Status</h4>
        </div>

           <form method="post" action="{{ URL::to('updateclientstatuspopup') }}">
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="form-group">
                          <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Select Status:

                          </label>
                          <select class="form-control col-xs-6" required id="recruiterspockup" name="recruiter[]"  onchange="changestatuspopupdata(this.value)" >
                             <option value="">Select Status</option>                   
                          </select>
                            <input type="text" name="clientreq_posid" id="placepopupval" >
                          <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                         
                        </div>
                        <div id="placepopup"></div>
                     
                    </div>
                </div>
            </div>
           
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="button" class="btn btn-default" value="Submit" id="buttonClassstpop">
            </div>
        </form>



        <div id=""></div>
      
      </div>      
    </div>
  </div> 

  </div>
    

<div class="container">
  <div class="modal hide fade" id="myModalcvstattus" role="dialog">
      <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select  Resume Status</h4>
        </div>

           <form method="post" action="{{ URL::to('updateassignpopupresume') }}">
            <div class="modal-body">
              <table width="100%" border="1">
               
<tr><td id="recruiterspockupcvstattus"></td></tr>
              </table>
                
                        <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                
                </div>
                 <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-default" value="Submit">
            </div>
            </div>
           
        </form>
      
      </div>      
    </div>
</div> 
</div>


<script>
   
function changestatusalloc()
{
    dstatus = document.getElementById("select_id").value;
      clientid = document.getElementById("clientid").value;
        recruiterId = document.getElementById("recruiterId").value;
      
//alert(posid);
 $.ajax({
            url: '{{URL::route('viewmyalocatedcvdetailsajax')}}',
            type: 'post',
            data:{
                '_token': "{{ csrf_token() }}",
                'status':dstatus,
                  'clientid':clientid,
                    'recruiterId':recruiterId,
                   
              },
            success: function(data)
            {
                 $('#suggesstion-box').html('');
                 $('#suggesstion-box').html(data);
            }
        });

}

 
    function changefirststatus(ids)
    {
        var status=$('#status').val();
        //alert(ids);
        $.ajax({
            url: '{{URL::route('changecvstatus')}}',
            type: 'post',
            data:{
                '_token': "{{ csrf_token() }}",
                'key':ids,
                'status':status,
              },
            success: function(data)
            {
                if(data==1){
                    location.reload();
                }
            }
        });
    }

</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#buttonClass").click(function() {
            var favorite = [];
            $.each($("input[name='checkboxid']:checked"), function(){            
                favorite.push($(this).val());
            });
            var valuejoin=favorite.join(",");
         
      //    alert( posid);
          
  $.ajax({

      type: 'POST',
                      url: '{{URL::route('getallocatedresumespopupdata')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'valuejoin':valuejoin,
                      
                       
                     
                      },

        success: function(data){
           $('#myModalcvstattus').modal('show');
     //    alert(data);
            $("#recruiterspockupcvstattus").empty();
            $("#recruiterspockupcvstattus").append(data);
          // $('#recruiterspockup').multiselect('rebuild');
        },
        error: function(data){

        }
    });

        });
    });
</script>
<script>
  
        $("#buttonClassstpop").click(function() {

var sel = document.getElementById('recruiterspockup').value;
var expectedsaleryval = document.getElementById('expectedsalery').value;
var Doj = document.getElementById('Doj').value;
var expDoj = document.getElementById('expDoj').value;
var count = document.getElementById('count').value;
var countval = document.getElementById('countval').value;
var placepopupval = document.getElementById('placepopupval').value;

   $.ajax({
                      type: 'POST',
                      url: '{{URL::route('updatemyallocatedresume')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'sel':sel,
                        'expectedsalery':expectedsaleryval,
                        'Doj':Doj,
                        'expDoj':expDoj,
                        'count':count,
                          'countval':countval,
                        'placepopupval':placepopupval,
                   
                      },
                      success: function(data){
                      // alert(data);
                       $('#myModal').modal('hide');
                        $("#suggesstion-box").show();
                         $("#suggesstion-box").html(data);
                    }
                    });


 

           });
    

</script>
  <script>

function changestatuspopupdata(id)
{
var str = id;
var res = str.split("/");
//alert(str);

//Then read the values from the array where 0 is the first
var myvar = res[0] + ":" + res[1] + ":" + res[2];
  // d = document.getElementById("select_id").value;
  if(res[2]=='4')
  {
 document.getElementById("placepopup").innerHTML="<input type='text' name='expectedsalery[]'  placeholder='Expected Salary'  class='form-control' id='expectedsalery' ><input type='date' name='Doj[]' class='form-control'  placeholder='Date Of Joining' id='Doj'><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining' id='expDoj' class='form-control'  ><input type='hidden' name='count[]' class='form-control col-xs-6'  value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  else

{


   if(res[2]=='5')
  {
 document.getElementById("placepopup").innerHTML="<input type='hidden' name='expectedsalery[]' class='form-control'  placeholder='Expected Salary' id='expectedsalery'><input type='hidden'class='form-control'  name='Doj[]'  placeholder='Date Of Joining'   id='Doj'><input type='date'  class='form-control' name='expDoj[]'  placeholder='Expected Date Of Joining'  id='expDoj'  ><input type='hidden' name='count[]' value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  else
  {


   if(res[2]=='6')
  {
    document.getElementById("placepopup").innerHTML="<input type='text' name='expectedsalery[]' class='form-control'  placeholder='Expected Salary'  id='expectedsalery'><input type='date' name='Doj[]'   class='form-control'  placeholder='Date Of Joining' id='Doj' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'    id='expDoj' ><input type='hidden' name='count[]' value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  else
  {
    document.getElementById("placepopup").innerHTML="<input type='hidden' name='expectedsalery[]' class='form-control col-xs-6' placeholder='Expected Salary' id='expectedsalery' ><input type='hidden' name='Doj[]'  placeholder='Date Of Joining'  id='Doj' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'  id='expDoj' ><input type='hidden' name='count[]' value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  }
}
 }

function getposiionvalue(id)
{

 var ids = id;
 //alert(ids);

// alert(document.location.host);
    $.ajax({

        type:"GET",

        url:"getspockposition/"+ids,
  
        data:'',
        success: function(data){
        // alert(data);
        document.getElementById("placepopupval").value = data;

        },
        error: function(data){

        }
    });}

function getupdatespock(eid,recruiter_id,cv_status,posid)
{

 var idds = eid;
 var recruiterid = recruiter_id;
 var cvstatus = cv_status;
 var posid = posid;
 //alert(cvstatus);
    $.ajax({
        type: 'POST',
                  url: '{{URL::route('getspockstatusallcv')}}',
                  data:{
                    'idds': idds,
                    'recruiterid': recruiterid,
                       'cvstatus': cvstatus,
                          'posid': posid,
                    '_token': "{{ csrf_token() }}",
                },


     
        success: function(data){
        // alert(data);
            $("#recruiterspockup").empty();
            $("#recruiterspockup").append(data);
          // $('#recruiterspockup').multiselect('rebuild');
        },
        error: function(data){

        }
    });}
function add(ids)
{
   
    document.getElementById("place"+ids).innerHTML="<input type='text' value=''>"
}
</script>
<script type="text/javascript">
        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
</script>

<!-- <script type="text/javascript">
    function abc(){
        alert('hi');
    }
</script> -->
<script>
    

function changestatusdata(id)
{
var str = id;
var res = str.split("/");
//alert(str);

//Then read the values from the array where 0 is the first
var myvar = res[0] + ":" + res[1] + ":" + res[2];
  // d = document.getElementById("select_id").value;
  if(res[2]=='4')
  {
 document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='text' name='expectedsalery'  placeholder='Expected Salary' class='form-control' ><input type='date' name='Doj' class='form-control'  placeholder='Date Of Joining' ><input type='hidden' name='expDoj'  placeholder='Expected Date Of Joining'   ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else

{


   if(res[2]=='5')
  {
 document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='hidden' name='expectedsalery'   placeholder='Expected Salary' ><input type='hidden' name='Doj'  placeholder='Date Of Joining'   ><input type='date' class='form-control'  name='expDoj'  placeholder='Expected Date Of Joining'   ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else
  {


   if(res[2]=='6')
  {
    document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='text' name='expectedsalery' placeholder='Expected Salary' class='form-control'  ><input type='date' name='Doj'   class='form-control'   placeholder='Date Of Joining' ><input type='hidden' name='expDoj'  placeholder='Expected Date Of Joining'    ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else
  {
    document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='hidden' name='expectedsalery'  placeholder='Expected Salary'class='form-control'  ><input type='hidden' name='Doj'  placeholder='Date Of Joining'   ><input type='hidden' name='expDoj'  placeholder='Expected Date Of Joining' ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  }
}
 }

    function changestatus(ids)
    {
        var status=$('#select_id').val();
        alert(status);
        $.ajax({
            url: '{{URL::route('changecvstatus')}}',
            type: 'post',
            data:{
                '_token': "{{ csrf_token() }}",
                'key':ids,
                'status':status,
              },
            success: function(data)
            {
                if(data==1){
                    location.reload();
                }
            }
        });
    }

</script>

@endsection

