.lin-add{ width:100%;     padding: 10px 15px; float:left;}
.lin-add h2{    margin-bottom: 0;
    font-size: 1rem;
    color: rgba(51, 51, 51, 0.85);
    text-transform: uppercase;
    font-weight: 600;
    display: inline-block;
    vertical-align: middle;}
.left-link{ width:100%; float:left;}
.left-link h5{     font-size: 14px;
    font-weight: 600;
    width: 100%;
    float: left;
    padding-top: 26px;}
.left-link ul{ list-style:none; padding:0px;}
.left-link ul li{ display:inline-block; padding: 0px 0px 10px;}
.left-link ul li i{ margin-right:15px;}
.left-link .pro-dcre{ width:100%; padding: 15px 0px 0px; float:left;}
.left-link .pro-dcre ul{list-style:none; padding:0px;}
.left-link .pro-dcre ul li{ display:inline-block; padding: 0px 0px 6px; font-size:12px;}
.left-link .pro-dcre ul li span{ margin-right:20px; font-weight:600;}
.right-link{ width:100%; float:left;}
.right-link .top-oiu{ width:100%;     padding-bottom: 15px; text-align:center; float:left;}
.right-link .top-oiu img{    border-radius: 50px;}
.right-link  .dat-down{ width:100%; float:left;}
.right-link  .dat-down p{ margin-bottom:15px;}
.dat-down { width:100%; float:left;}
.dat-down ul{ list-style:none;}
.dat-down ul li{ display:inline-block;}