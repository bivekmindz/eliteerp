@extends('Emp.layouts.master')

@section('content')

<style type="text/css">
  #search_filter_filter{float:left!important;}
</style>


<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>Hand Over</h4>
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
                @endif
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                       
                            
                     <div class="table-responsive table_green siro">       
                            <table class="table table-bordered" id="">
                                <thead>
                           <tr>
                                    <th width="33">S No</th>
                                                <th width="112">Candidate Name</th>
                                                <th width="327">Position Name</th>
                                                 <th width="296">Client Name</th>
                                                  <th width="188">Action</th>

                                </tr> 

                                </thead>
                                <tbody id="suggesstion-box">
                                @php
                                $i=1;
                                @endphp
                                  @foreach($data as $key => $val)
                                @php  if(count($val['position'])>0) {  @endphp
                                <tr  >
                                     <td>{{  $i++ }}</td>
                                            <td>{{  $val['username'] }}</td>
                                            <td colspan="3">
                                            <table width="100%">
                                                   @foreach($val['position'] as $key2 => $valpos)
                                                <tr><td width="40%">   {{  ($valpos->clientjob_title) }} </td>
                                                <td width="37%">   {{  ($valpos->clientname) }}</td>
<td width="23%">
  <?php  if($valpos->poshand>0){
  ?> <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal" onclick="getclientreq_id({{  ($valpos->clientreq_id) }},{{  ($val['userid']) }});">Re Hand Over</button><?php
  
  } else {?>
    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal" onclick="getclientreq_id({{  ($valpos->clientreq_id) }},{{  ($val['userid']) }});">Hand Over</button>
<?php } ?>
     </td>

                                                </tr>
@endforeach
                                            </table>
                                          </td>
                </tr>
                @php
                              }
                                @endphp
                @endforeach
                </tbody>
                </table>
            </div>  </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<div class="container">
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Assigned To</h4>
        </div>
        <form method="post" action="{{ URL::to('inserthandover') }}">
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="form-group">
                          <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Select Employee:</label>
                          <select class=" form-control col-xs-6" required name="recruiter">
                            <option value="">Select </option>
                            @foreach($emp_list as $keyr => $empval)
                            <option value="{{ $empval->id }}">{{ $empval->name }}</option>
                            @endforeach
                          </select>
                          <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
                          <input type="hidden" name="positionreq_id" id="positionreq_id" value="">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-default" value="Submit">
            </div>
        </form>
      </div>      
    </div>
  </div> 
</div>
<script type="text/javascript">
function getclientreq_id(clientreq_id)
{
 //   alert();
    $("#clientreq_id").val(clientreq_id);
 
}
        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
</script>
@endsection

