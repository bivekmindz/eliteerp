@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        
        <style>
            .autogen{    width: 100%;
    float: left;
    max-height: 137px;
    overflow-y: auto;
    z-index: 999;
    background-color: #fff;}
    .autogen ul {padding: 0px;
    line-height: 35px; border: 1px solid  #e5e5e5;}
     .autogen ul li {padding: 0px 8px; border-bottom: 1px solid #e5e5e5;}
     .autogen ul li:last-child{ border-bottom:none;}
        </style>
           
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
        

        <div class="row">
            <div class="main-header">
                <h4>Recuiter Joining And Pipeline  Report</h4>
             
            </div>
        </div>



       
            <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Recuiter Joining And Pipeline Report</h5>
                           
                        </div>
                        <div class="card-block">


                              <div class="card-block">
                                 <form action=""  method="get" enctype="multipart/form-data"  autocomplete="off">
                                <div class="row">

     <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">User </label>
                                        <div class="col-sm-8">
      
            <input type="text" name="user" id="country" class="form-control" placeholder="Enter user Name"  autocomplete="off" />  



                <div id="countryList" class="autogen"></div> 
          
          
          </div>
                                    </div>
                                </div>


 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Start Date </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" value="" id="dojnew" name="doj" class="document" required/> </div>
                                    </div>
                                </div>

 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">End Date </label>
                                        <div class="col-sm-8">
                                             <input class="form-control" type="text" value="" id="doend" name="doend" class="document" required/>
                                    </div>
                                </div>
                                
                                

                                 <div class="col-lg-6">
                                 <div class="form-group row">

                                        <div class="col-sm-12">
                                        
                                        </div>
                                        
                                          <div class="col-sm-12">
                                       
                                        </div>
                                    </div>
                                    
                                  
                                 
                                    </div>
                                 </div>

                                </div>



  <div class="md-input-wrapper">
                                <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>
                                
                                <a href="recuiterjoiningpipelinereports"  class="btn btn-primary waves-effect waves-light">Cancel</a>
                            </div>




                                 



                                </form>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                  



  @if(!empty($data))
            @foreach($data as $value)

   <?php 
   $subm=0;
 if(count($value['client'])>0) {
?>

<table width="100%" border="1" cellspacing="0" cellpadding="0">

<tr>
    <td width="10%" style="border:none;" height="30" align="left"><strong style="color:#1f2ccc;">{{ $value['username']}},</strong></td>
    <td colspan="5" style="border:none;"><p>&nbsp;</p>
    <p>&nbsp;</p></td>
   
  </tr>
  
  <tr>
    <th height="39" width="30%"  scope="col" rowspan="2">Client</th>
  
    <th width="16%" scope="col" rowspan="2">Pipe Lined</th>
    <th width="13%" scope="col" colspan="2" style='text-align: center'>Offered<br>
   </th>
      <th width="30%" scope="col" colspan="2" style='text-align: center'>Joined<br>
    </th>
  
   
  </tr>
  <tr>
   
    <th width="13%" scope="col">Target
   </th>
    <th width="13%" scope="col">Joined
   </th>
    <th width="13%" scope="col">Target
   </th>
      <th width="30%" scope="col">Joined
    </th>
  
   
  </tr>


<?php  foreach($value['client'] as $keyus => $k){?>
<tr>
  <td><?php echo $k->comp_name; ?></td>
  <td><?php echo  $k->piplined; ?></td>
  <td><?php echo  $k->Offered; ?></td>
   <td><?php echo  $k->Offered; ?></td>
    <td><?php echo  $k->joined; ?></td>
   <td><?php echo  $k->joined; ?></td>
</tr>

<?php } ?>


  <!-- 
<?php //echo  $c = count($value['userreport']);?>

<?php if(!empty($value['userreport'])) { ?>
<?php  foreach($value['userreport'] as $keyus => $k){?>
  <tr>
     <td><?php if(!empty($value['client'][$keyus])) { echo $value['client'][$keyus]->comp_name; }  ?>
     
     
     
    
     </td>
      
        <td><?php if(!empty($value['position'][$keyus])) { ?><a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value['empcode'];?>,<?php echo $value['position'][$keyus]->clientjob_id;?>,'<?php echo $from; ?>','<?php echo $to; ?>')" > <?php echo $value['position'][$keyus]->clientjob_title; }  ?></a></td>
      
     <td><?php echo($k->trtr);  ?></td>
     <td><?php if(!empty($value['userreport2'][$keyus])) { echo $value['userreport2'][$keyus]->sendclient; } else { echo "0"; } ?></td>
        <td><?php if(!empty($value['userreport2'][$keyus])) { echo $value['userreport2'][$keyus]->sendclient; } else { echo "0"; } ?></td>
        
       
   </tr>
   
   
   
   
   
<?php } } ?> -->
    




</table>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<?php } ?>
    @endforeach
            @endif
            
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
             </div>
             
             
             
            
            </div>
            
            <div class="container">
   <div class="modal fade" id="myModalnnn" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Resumes</h4>
        </div>
           <div class="modal-body" >
       <table width="90%" border="1" cellspacing="0" cellpadding="0">
         
            <tbody id="recruiter">
          
            </tbody>
             
            
        </table>
        </div>   
      </div>      
    </div>
  </div> 
</div>



        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Jd Position Details</h4>
                    </div>
                    <div class="modal-body" id="positionJd">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <script>
            function upload_position_jd(clientjob_id) {
                $.ajax({
                    url: 'adminpositionjd',
                    data: { id:clientjob_id },
                    success: function(result){
                        $("#positionJd").html(result);
                    }
                });
                $('#myModal').modal('show');
            }
    </script>
     <script>
    </script>

        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">

    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>

<script>  
 $(document).ready(function(){  
      $('#country').keyup(function(){  
           var query = $(this).val();  
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('searchusers')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                      // alert(data);
                         $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                      }
                    });
                    
                    
              /*  $.ajax({  
                     method:"POST",  
                     url:"searchusers",  
                    
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                     }  
                });  */
           }  
      });  
      $(document).on('click', 'li', function(){  
           $('#country').val($(this).text());  
           $('#countryList').fadeOut();  
      });  
 });  
 </script>  
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">

    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>


<script>
     $(document).ready(function () {  
          $('#doend').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>
<script>
     $(document).ready(function () {  
          $('#dojnew').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>


@endsection



