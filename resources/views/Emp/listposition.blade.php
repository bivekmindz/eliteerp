@extends('Emp.layouts.master')

@section('content')
<style>
    .open>.dropdown-menu {
      width: 580px;
}
</style>
<script>

$(document).ready(function(){
    $("#filter_area").keyup(function(){
        var xxx = $(this).val();

       // alert(xxx);

          $.ajax({
                      type: 'POST',
                      url: '{{URL::route('positionlisting')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'key':xxx,
                      },
                      success: function(data){
                     //  alert(data);
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);

                         
                      }
                    });



        /*$.ajax({
        headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
        type: "POST",
        url: "positionlisting",
        data:'keyword=11',
        // beforeSend: function(){
        //      $("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
        // },
        success: function(data){
            alert(22);
            console.log(1111);
             // $("#suggesstion-box").show();
             // $("#suggesstion-box").html(data);
             // $("#search-box").css("background","#FFF");
        }
        });*/
    });
});



// function selectCountry(val) {
// $("#search-box").val(val);
// $("#suggesstion-box").hide();
// }
</script>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
             @inject('position', 'App\Component\CommonComponent')
       @php 
            $breadcrumb = $position->breadcrumbs();
            //print_r($breadcrumb); exit;
            //print_r($breadcrumb[0]->menuname); exit;
        @endphp
       
                <h4>Position Listing</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">{{$breadcrumb[0]->parentmenu}}</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">{{$breadcrumb[0]->menuname}}</a>
                    </li>
                </ol>
            </div>
        </div>



     
            <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">All Vacancies</h5>
                            <form action="{{ URL::to('excellistpositiondownload') }}"  method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel" value="Download Excel" name="downexcel">
                                             
                                            </input>
                                        </form>
                           
                        </div>
                        @if(Session::has('success_msg'))
                         <div class="alert alert-success">
                           {{ Session::get('success_msg') }}
                         </div>
                         @endif
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12">
                                
                                <div class="list-podi">
                                <div class="list-podi-inn">
                           
                                <div >
                                     <div class="table-responsive table_green siro">       
                            <table class="table table-bordered" id="">
                                        <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Job Title</th>
                                            <th>Company Name</th>
                                            <th>Total Positions</th>
                                            <th>Experience</th>
                                            <th>Revelent Positions</th>
                                            <th>Primary Skill</th>
                                            <th>Secondary Skill</th>
                                            <th>Assign </th>
                                            <th>Edit Position </th>
                                            <th>Re-Create </th>

                                        </tr>
                                        </thead>
                                        <tbody id="suggesstion-box">
                                            @php
                                           $i=1;
                                         @endphp
                                          @foreach($pos as $key=>$value)
                                        <tr >
                                           
                                             <td>{{  $i++   }}</td>
                                            <td>{{ $value->clientjob_title }}</td>
                                            <td>{{ $value->comp_name }} </td>
                                            <td>{{ $value->clientjob_noofposition  }}</td>
                                            <td>{{ $value->experience  }}Year</td>
                                            <td>{{ $value->domain_experience  }} Year</td>
                                            <td>{{ $value->primary_skills  }}</td>
                                            <td>{{ $value->secondary_skills  }}</td>
                                            <td>
                                                <div class="btn-group btn-group-sm" style="float: none;">
                                                    <button type="button" onclick="upload_position_jd({{ $value->clientjob_id }},'{{ $value->clientjob_title }}');" class="tabledit-edit-button btn btn-primary waves-effect waves-light"
                                                            style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span>
                                                    </button>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="{{ URL::to('editposition/'.($value->clientjob_id )) }}">
                                                 <div class="btn-group btn-group-sm" style="float:none;"> 
                                                <button type="button"  class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;">
                                                <span class="icofont icofont-ui-edit"></span>
                                                </button>
                                                </div>
                                                </a>
                                            </td>
                                            <td>
                                                <?php

                                                if(($value->drive_shortlist=='Drive') && ($value->drive_time<$currentDate) && ($value->drive_time!='')){?>

                                                    <a href="{{ URL::to('createposition/'.($value->clientjob_id )) }}">
                                                 <div class="btn-group btn-group-sm" style="float:none;"> 
                                                <button type="button"  class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;" >Re-Create
                                               
                                                </button>
                                                
                                                </div>
                                                </a>

                                               <?php } ?>

                        <!-- && ($value->drive_time<$currentDate) && ($value->drive_time!='') -->                        
 <?php
//echo $value->lineup_to.'----'.$currentDate;
                                                if(($value->drive_shortlist=='DaliyLineUp') && ($value->lineup_to<$currentDate) && ($value->lineup_to!='') ){?>

                                                    <a href="{{ URL::to('createposition/'.($value->clientjob_id )) }}">
                                                 <div class="btn-group btn-group-sm" style="float:none;"> 
                                                <button type="button"  class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;" >Re-Create
                                               
                                                </button>
                                                
                                                </div>
                                                </a>

                                               <?php } ?>
                                               
                                            <?php

                                                if(($value->drive_shortlist=='Shortlist') && ($value->shortlist_time<$currentDate) && ($value->shortlist_time!='')){?>

                                                    <a href="{{ URL::to('createposition/'.($value->clientjob_id )) }}">
                                                 <div class="btn-group btn-group-sm" style="float:none;"> 
                                                <button type="button"  class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;" >Re-Create
                                               
                                                </button>
                                                
                                                </div>
                                                </a>

                                               <?php } ?>
     
                                                 <?php

                                                if(((($value->drive_shortlist=='Shortlist') || ($value->drive_shortlist=='Drive')) && ($value->shortlist_time=='') && ($value->drive_time=='')) || ($value->drive_shortlist=='')){?>

                                                    <a href="{{ URL::to('createposition/'.($value->clientjob_id )) }}">
                                                 <div class="btn-group btn-group-sm" style="float:none;"> 
                                                <button type="button"  class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;" >Re-Create
                                               
                                                </button>
                                                
                                                </div>
                                                </a>

                                               <?php } ?>

                                            </td>
                                        
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                    </div>
                                     </div>
                                
                                </div>
                                </div>
                                 </div>
                                
                            </div>
                        </div>
                    </div>
             </div>
    </div>

        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" action="positionallocate">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Jd Position Assign</h4>
                        </div>
                        <div class="modal-body" id="">Recruiter
                            <select class="multiselect-ui form-control" name="req[]" id="positionrecruiter" multiple>
                                <option value=""> Choose Recruiter</option>
                            </select>
                        <br>Position
                            <input type="text" name="posid" id="cplace" class="form-control">
                            <input type="hidden" name="aid" id="cid" class="form-control">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script>
            function upload_position_jd(clientjob_id,cname) {
                document.getElementById('cplace').value = cname;
                document.getElementById('cid').value = clientjob_id;
                $.ajax({
                    url: 'positionjd/'+clientjob_id,
                    success: function(result){
                        $("#positionrecruiter").empty();
                        $("#positionrecruiter").append(result);
                        $('#positionrecruiter').multiselect('rebuild');
                    }
                });
                $('#myModal').modal('show');
            }
        </script>
        <script>
            function edit_position_jd(clientjob_id,cname) {
                document.getElementById('cplace').value = cname;
                document.getElementById('cid').value = clientjob_id;
                $.ajax({
                    url: 'positionjd/'+clientjob_id,
                   /// data: { id:clientjob_id },
                    success: function(result){
                        $("#positionJd").html(result);
                    }
                });
                $('#myModal').modal('show');
            }
        </script>
        <script type="text/javascript">
        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
        </script>


@endsection



