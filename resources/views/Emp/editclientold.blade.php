@extends('Emp.layouts.master')

@section('content')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">
    <style>
      .error{ 
        color:red;
      }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Edit Client</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Clients</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Edit New Client</a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Edit Client</h5>
                    </div>
                    @if(Session::has('success_msg'))
                        <div class="alert alert-success">
                            {{ Session::get('success_msg') }}
                        </div>
                    @endif
                    <div class="card-block">
                                <form method="post" name="client" action="{{ URL::to('editclient/'.Crypt::encrypt($client_details[0]->client_id)) }}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Company Name </label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="comp_name" name="comp_name" value="{{ (Input::old('comp_name')) ? Input::old('comp_name') : $client_details[0]->comp_name }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('comp_name') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Company URL</label>
                                            <div class="col-sm-8">
                                               <input class="form-control" type="text" id="company_url" name="company_url" value="{{ (Input::old('company_url')) ? Input::old('company_url') : $client_details[0]->company_url }}" id="example-text-input">
                                               <span class="text-danger">{{ $errors->first('company_url') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Contact Person Name</label>
                                            <div class="col-sm-8">
                                               
                                           <input class="form-control" type="text" id="contact_name" name="contact_name" value="{{ (Input::old('contact_name')) ? Input::old('contact_name') : $client_details[0]->contact_name }}" id="example-text-input">
                                           <span class="text-danger">{{ $errors->first('contact_name') }}</span>
                                        
                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Designation</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="designation" name="designation" value="{{ (Input::old('designation')) ? Input::old('designation') : $client_details[0]->designation }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('designation') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Phone</label>
                                            <div class="col-sm-8">
                                              <input class="form-control" type="text" id="phone" maxlength="15" name="phone" value="{{ (Input::old('phone')) ? Input::old('phone') : $client_details[0]->phone }}" id="example-text-input">
                                              <span class="text-danger">{{ $errors->first('phone') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Email</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="email" name="email" value="{{ (Input::old('email')) ? Input::old('email') : $client_details[0]->email }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Company Size</label>
                                            <div class="col-sm-8">
                                               
                                           <input class="form-control" type="text" id="company_size" name="company_size" value="{{ (Input::old('company_size')) ? Input::old('company_size') : $client_details[0]->company_size }}" id="example-text-input">
                                            <span class="text-danger">{{ $errors->first('company_size') }}</span>
                                    
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                         <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Timings</label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="timings" name="timings" value="{{ (Input::old('timings')) ? Input::old('timings') : $client_details[0]->timings }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('timings') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Days Working</label>
                                            <div class="col-sm-8">
                                               
                                           <input class="form-control" type="text" id="days_working" name="days_working" value="{{ (Input::old('days_working')) ? Input::old('days_working') : $client_details[0]->days_working }}" id="example-text-input">
                                            <span class="text-danger">{{ $errors->first('days_working') }}</span>
                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Industry </label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="industry" name="industry" value="{{ (Input::old('industry')) ? Input::old('industry') : $client_details[0]->industry }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('industry') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                       <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Headquarters </label>
                                                <div class="col-sm-8"> 
                                                   <input class="form-control" type="text" id="headquarters" name="headquarters" value="{{ (Input::old('headquarters')) ? Input::old('headquarters') : $client_details[0]->headquarters }}" id="example-text-input">
                                                   <span class="text-danger">{{ $errors->first('headquarters') }}</span>
                                                </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Company’s Specialties </label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="company_specialties" name="company_specialties" value="{{ (Input::old('company_specialties')) ? Input::old('company_specialties') : $client_details[0]->company_specialties }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('company_specialties') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Office Address </label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="office_address" name="office_address" value="{{ (Input::old('office_address')) ? Input::old('office_address') : $client_details[0]->office_address }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('office_address') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                       
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Selling Point</label>
                                        <div class="col-sm-10">
                                           <textarea name="selling_point" type="text" id="selling_point" required class="form-control" rows="50" cols="30">{{ (Input::old('selling_point')) ? Input::old('selling_point') : $client_details[0]->selling_point }}</textarea>
                                           <span class="text-danger">{{ $errors->first('selling_point') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="md-input-wrapper">
                                   <a href="{{ URL::to('listclient') }}" class="btn btn-default waves-effect">Cancel
                                    </a> 
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                                    </button>
                                </div> 
                                </form>
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>

<script type="text/javascript">
    $('#selling_point').summernote({ height: 400 });
</script>

<script>
$(document).ready(function() {
    $("#company_size").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("#phone").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});
</script>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script>
    $(function() {
        $("form[name='client']").validate({
            ignore: [],
            rules: {
              comp_name: "required",
              company_url: "required",
              email: "required",
              contact_name: "required",
              designation: "required",

              phone: "required",
              timings: "required",
              days_working: "required",
              industry: "required",
              company_size: "required",

              headquarters: "required",
              office_address: "required",
              selling_point: "required",
              company_specialties: "required",
            },
            messages: {
              comp_name: "Please enter company name.",
              company_url: "Please enter company url.",
              email: "Please enter company email.",
              contact_name: "Please enter contact name.",
              designation: "Please enter designation.",

              phone: "Please enter phone number.",
              company_size: "Please enter company size.",
              timings: "Please enter timimngs.",
              days_working: "Please enter days of working.",
              industry: "Please enter industry.",

              headquarters: "Please enter headquarters.",
              office_address: "Please enter office address.",
              selling_point: "Please enter selling point.",
              company_specialties: "Please enter company specialties.",
            },
            submitHandler: function(form) {
              form.submit();
            }
        });
    });
</script>
@endsection

