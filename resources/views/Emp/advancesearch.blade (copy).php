@extends('Emp.layouts.master')

@section('content')



        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <h4>Add New Client</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="">Clients</a>
                                </li>
                                <li class="breadcrumb-item"><a href="">Add New Client</a>
                                </li>
                            </ol>
                </div>
                
                
                
            </div>
           
           
           
          <div class="row">
                <div class="col-xl-3 col-lg-4">
                    <div class="card faq-left">
                        
                        <div class="left-menu-dro">
                          
                          <div id='cssmenu'>
<ul>
  <!--  <li><a href='#'>Home</a></li> -->
   <li class='active has-sub'><a href='#'>All Keywords</a>
      <ul>
         <li class='has-sub'><a href='#'>Product 1</a>
            <ul>
               <li><a href='#'>Sub Product</a></li>
               <li><a href='#'>Sub Product</a></li>
            </ul>
         </li>
         <li class='has-sub'><a href='#'>Product 2</a>
            <ul>
               <li><a href='#'>Sub Product</a></li>
               <li><a href='#'>Sub Product</a></li>
            </ul>
         </li>
      </ul>
   </li>
   <li><a href='#'>About</a></li>
   <li><a href='#'>Contact</a></li>
</ul>
</div>
                            
                            <div class="faq-profile-btn">
                                <button type="button" class="btn btn-primary waves-effect waves-light">Search
                                </button>
                               
                            </div>

                        </div>
                    </div>
                  
                </div>
                
                <div class="col-xl-9 col-lg-8">
              
                   <div class="card">
                            <div class="card-header">
                            
                           <div class="top-menu-ji">
                              
                               <ul>
                                
                                  <li><a href="">  <label class="input-checkbox checkbox-primary">
                            <input type="checkbox" id="checkbox11">
                            <span class="checkbox"></span>
                        </label>  Add To <i class=" icofont icofont-simple-down"></i></a>
                                    
                                    <ul class="sub-drop">
                                      <li><a href="">jdJODj</a></li>
                                       <li><a href="">jdJODj</a></li>
                                    </ul>
                                  
                                  </li>
                                   <li><a href=""><i class="icofont icofont-envelope coiy-en"></i>Email <i class=" icofont icofont-simple-down"></i></a></li>
                                 
                               
                               </ul>
                           
                           </div>
                                
                            </div>
                            
                            <!-- end of modal -->
                            
                        </div>
                        
                         <!-------------------------- Start --------------------------------> 
                        @if(!empty($getpostion))
                        @foreach($getpostion as $key => $list)

                        <div class="card">
                        
                        <div class="to-do-list widget-to-do-list" style=" padding-top:15px;">
                    <div class="rkmd-checkbox checkbox-rotate">
                        <label class="input-checkbox checkbox-primary">
                            <input type="checkbox" id="checkbox11">
                            <span class="checkbox"></span>
                        </label>
                        <label>{{ $list->candidate_name }}</label>
                        </div>
                    </div>
                            
                            
                            <!-- end of modal -->
                            <div class="card-block">
                            
                            <div class="col-md-8">
                            
                              <div class="left-link">
                               
                                 <ul>
                                   
                                    <li><i class="icofont icofont-bag"></i>{{ $list->total_exp }}</li>
                                     <li><i class="icon-wallet"></i> <i class="icofont icofont-cur-rupee"></i>{{ $list->current_ctc }}</li>
                                      <li><i class="icon-location-pin"></i>Chennai</li>
                                   
                                 </ul>
                                 
                                 <div class="pro-dcre">
                                    
                                    <ul>
                                      <li><span>Current Organization</span> {{ $list->current_org }}</li></br>
                                       <li><span>Education </span> {{ $list->highest_qulification }}</li></br>
                                        <li><span>Key Skills </span> {{ $list->primary_skill }}</li></br>
                                        <li><span>Notice Period </span> {{ $list->np }}</li></br>
                                         <!-- <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                          <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                           <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li> -->
                                    </ul>
                                    
                                 </div>
                                 
                                 <h5>Similar Resumes</h5>
                              
                              
                              </div>
                            
                            </div>
                            
                             <div class="col-md-4">
                              <div class="right-link">
                              
                             <!--    <div class="top-oiu">
                                
                                <img src="assets/images/avatar-4.png" alt="Logo">
                                
                                </div> -->
                                
                                 <div class="dat-down">
                                  <p><strong>{{ $list->primary_skill }}</strong> with {{ $list->highest_qulification }} currently living in Chennai</p>

<div class="form-group row">
                                            
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-btn" id="btn-addon1"><button type="submit" class="btn btn-success shadow-none addon-btn waves-effect waves-light"><i class="icofont icofont-ui-call"></i></button></span>
                                                    <input type="text" id="btnaddon1" class="form-control" aria-describedby="btn-addon1" value="{{ $list->candidate_mob }}" readonly="">
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                        
                                        <ul>
                                              <li><div class="to-do-list widget-to-do-list">
                    <div class="rkmd-checkbox checkbox-rotate">
                        <label class="input-checkbox checkbox-primary">
                            <input type="checkbox" id="checkbox11">
                            <span class="checkbox"></span>
                        </label>
                        <label style="font-size:12px;">Verified Phone</label>
                        </div>
                    </div></li>
                    
                    
                                            </ul>
                                 </div>
                              
                              
                              </div>
                                
                            
                            </div>
                            
                            
                                 
                            </div>
                        </div>

                        @endforeach
                        @endif
                       <!-------------------------- End --------------------------------> 

                    
                   
                </div>
            </div>

         
            
          
        </div>
      
    <script>
    (function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);

    </script>

@endsection