@extends('Emp.layouts.master1')

@section('content')

<style type="text/css">
  #search_filter_filter{float:left!important;}
</style>


<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View CV List</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Client Call CV List</a>
                </li>
                <li class="breadcrumb-item"><a href="">Allocated CVs List Details</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
                @endif
                  <div class="card-block">
                            <div class="row">
                               <div class="col-lg-6">
                                    <div class="form-group row">
                                     
                                        
                                             <form action="{{ route('allocatedexceldownload')  }}"  method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                         <input type="hidden" name="assignfrom" value="{{$assignfrom}}">
                                         <input type="hidden" name="assignto" value="{{$assignto}}">
                                         <input type="hidden" name="posid" value="{{$posid}}">
                                                <button type="submit" class="btn  btn-success  waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel">Download Excel
                                                    <span class="badge"><i class="icofont icofont-file-excel"></i></span>
                                                </button>
                                                </form>
                                     
                                        </div>
                                    </div>
                               


                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <form action="{{ route('allocatedexcelup')  }}"  method="post" enctype="multipart/form-data">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Upload Excel</label>
                                         
                                               <!-- <label for="file" class="custom-file">
                                                    <input type="file" id="file" name="upfile"> 
                                                    <span class="custom-file-control"></span>
                                                </label> -->
                                                   
                                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                         <input type="hidden" name="assignfrom" value="{{$assignfrom}}">
                                         <input type="hidden" name="assignto" value="{{$assignto}}">
                                         <input type="hidden" name="posid" value="{{$posid}}">
                                         
                                                   <input type="file" id="file" name="upfile">
                                                <span id="lblError" style="color: red;"></span>
                                            </div>   <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>
                                    </div>
                                </div>
                           


                           
                        </form>
                    </div>
           
            
                
                   <form action="{{ URL::to('allcvdetupdate') }}" method="post"> 
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                           <div id="search_filter_filter" class="dataTables_filter"><label><input type="submit" name="submit" value="change status"></label>
                            @php 
if(isset($_GET['changestatus'])&& $_GET['changestatus']!='')
{
$_GET['changestatus']=$_GET['changestatus'];
}
else
{
$_GET['changestatus']='';
}

                  @endphp

<select name="changestatus"  onchange="changestatusalloc()" id='select_id'>
       <option value='-1' >Search Status</option>
        <option value="Pending"<?php if($_GET['changestatus']=='Pending') {?> selected <?php  } ?>>Pending</option>
         <option value="Not Going" <?php if($_GET['changestatus']=='Not Going') {?> selected <?php  } ?>>Not Going</option>
        <option value="Not Reachable" <?php if($_GET['changestatus']=='Not Reachable') {?> selected <?php  } ?>>Not Reachable</option>
       <option value="Not Responding" <?php if($_GET['changestatus']=='Not Responding') {?> selected <?php  } ?>>Not Responding</option>
         <option value="Confirm"<?php if($_GET['changestatus']=='Confirm') {?> selected <?php  } ?>>Confirm</option>
           <option value="Dicey"<?php if($_GET['changestatus']=='Dicey') {?> selected <?php  } ?>>Dicey</option>
</select>
<input type="hidden" name="assignfrom" value="{{$assignfrom}}" id='assignfrom'>
<input type="hidden" name="assignto" value="{{$assignto}}" id='assignto'>
<input type="hidden" name="posid" value="{{$posid}}" id='posid'>
                           
                           </div>
                    
                          <div class="table-responsive table_green">
    

                            <table class="table table-bordered" id="">
                                <thead>
                                <tr>
                                   <th></th>
                                    
                                    <th>CandidateName</th>
                                   
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>FollowUp Status</th>
                                    <th>Action</th> 
                                </tr>
                                </thead>
                                <tbody id="suggesstion-box">
                                @php
                                $i=1;
                                @endphp
                                @foreach($cvdetails as $key => $p)
                                <tr  >
                                  <td><input type="checkbox" name="changestatus[]" value="{{$p->tca_id}}"></td>

                                    <td>{{ !empty($p->candidate_name) ? $p->candidate_name : 'N/A' }}</td>
                                  
                                    <td>{{ !empty($p->candidate_email) ? $p->candidate_email : 'N/A' }}</td>
                                    <td>{{ !empty($p->candidate_mob) ? $p->candidate_mob : 'N/A' }}</td>
                                    <td>{{ $p->tca_followupstatus }}</td>
                                    <td> <!-- <?php if($p->tca_followupstatus=='Confirm') { echo ''; } else if($p->tca_followupstatus=='Not Going'){ echo ''; } else { ?>
                                        <select id="status" onchange="changestatus({{$p->tca_id}})">
                                            <option>Change Status</option>
                                            <option value="Confirm">Confirm</option>
                                            <option value="Not Going">Not Going</option>
                                            <option value="Not Reachable">Not Reachable</option>
                                            <option value="Not Responding">Not Responding</option>
                                        </select>  <?php  } ?> -->
                                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="tcaid[]" value="{{$p->tca_id}}">
      <input type="hidden" name="asignfrom" value="{{$assignfrom}}">
        <input type="hidden" name="asignto" value="{{$assignto}}">
           <input type="hidden" name="posid" value="{{$posid}}">

                                               <select name="changestatusval[]" onchange="changestatusdata({{$p->tca_id}})"id="select_id{{$p->tca_id}}">
                                            <option>Change Status</option>
                                            <option value="Confirm"<?php if($p->tca_followupstatus=='Confirm') {?> selected <?php  } ?>>Confirm</option>
                                            <option value="Not Going" <?php if($p->tca_followupstatus=='Not Going') {?> selected <?php  } ?>>Not Going</option>
                                            <option value="Not Reachable" <?php if($p->tca_followupstatus=='Not Reachable') {?> selected <?php  } ?>>Not Reachable</option>
                                            <option value="Not Responding" <?php if($p->tca_followupstatus=='Not Responding') {?> selected <?php  } ?>>Not Responding</option>
                                             <option value="Dicey" <?php if($p->tca_followupstatus=='Dicey') {?> selected <?php  } ?>>Dicey</option>
                                        </select> <div id="place{{$p->tca_id}}"></div>
                                    </td>

                </tr>
                @endforeach
                </tbody>
                </table>
            </div>   </div>
        </div>
    </div></form>
</div>
</div>
</div>
</div>

<script type="text/javascript">
        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
</script>

<!-- <script type="text/javascript">
    function abc(){
        alert('hi');
    }
</script> -->
<script>
    
function changestatusdata(ids)
{
    d = document.getElementById("select_id"+ids).value;
   //alert(d);
  // if(d=='Offered' || d=='Offered' )
 //alert(value);
    document.getElementById("place"+ids).innerHTML="<textarea name='salery[]' placeholder='Enter Details' value=' '></textarea><input type='hidden' name='count[]' value='1'>"
}

    function changefirststatus(ids)
    {
        var status=$('#status').val();
        //alert(ids);
        $.ajax({
            url: '{{URL::route('changecvstatus')}}',
            type: 'post',
            data:{
                '_token': "{{ csrf_token() }}",
                'key':ids,
                'status':status,
              },
            success: function(data)
            {
                if(data==1){
                    location.reload();
                }
            }
        });
    }

function changestatusalloc()
{
    dstatus = document.getElementById("select_id").value;
      assignfrom = document.getElementById("assignfrom").value;
        assignto = document.getElementById("assignto").value;
          posid = document.getElementById("posid").value;
//alert(posid);
 $.ajax({
            url: '{{URL::route('myalocatedcvdetailsajax')}}',
            type: 'post',
            data:{
                '_token': "{{ csrf_token() }}",
                'status':dstatus,
                  'assignfrom':assignfrom,
                    'assignto':assignto,
                      'posid':posid,
              },
            success: function(data)
            {
                 $('#suggesstion-box').html('');
                 $('#suggesstion-box').html(data);
            }
        });

}
</script>

@endsection

