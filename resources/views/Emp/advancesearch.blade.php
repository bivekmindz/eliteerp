@extends('Emp.layouts.master')

@section('content')
<!-- Container-fluid starts -->
<!-- Main content starts -->

<style>
    
    
.hvr-pulse-grow {
  display: inline-block;
  vertical-align: middle;
  -webkit-transform: perspective(1px) translateZ(0) infinite;
  transform: perspective(1px) translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);
    background: -webkit-linear-gradient(#2a643d, #e2524f);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
   -webkit-animation-name: hvr-pulse-grow;
  animation-name: hvr-pulse-grow;
  -webkit-animation-duration: 0.3s;
  animation-duration: 0.3s;
  -webkit-animation-timing-function: linear;
  animation-timing-function: linear;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
  -webkit-animation-direction: alternate;
  animation-direction: alternate;
}
.hvr-pulse-grow:hover, .hvr-pulse-grow:focus, .hvr-pulse-grow:active {

}
@-webkit-keyframes hvr-pulse-grow {
  to {
    -webkit-transform: scale(1.3);
    transform: scale(1.3);
  }
}
@keyframes hvr-pulse-grow {
  to {
    -webkit-transform: scale(1.3);
    transform: scale(1.3);
  }
}



    
</style>
<div class="container-fluid">
  <div class="row">
    <div class="main-header">
      <h4>Advance Search</h4>
      <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
        <li class="breadcrumb-item"><a href="javascript:void(0)"><i class="icofont icofont-home"></i></a>
        </li>
        <li class="breadcrumb-item"><a href="/cvparsing">Advance Search</a>
        </li>
        <li class="breadcrumb-item"><a href="javascript:void(0)">{{ $_GET['all_keywords'] }}</a>
        </li>


      </ol> 
    </div>



  </div>



  <div class="row">
    <div class="col-xl-3 col-lg-4">
      <div class="card faq-left">

        <div class="left-menu-dro">
          <form action="{{ URL::to('adsearch') }}" method="get"> 
            <div id='cssmenu'>

              <ul>
               <input type="hidden" name="_token" value="{{ csrf_token() }}">  
               <li class='has-sub'><a href='#'>Keywords</a>
                <ul>
                  <input type="text" class="form-control" placeholder="Search Keywords" name="all_keywords" value="{{
                      app('request')->input('all_keywords') }}">      
                  <li class='has-sub'><a href='#'>Exclude Keywords</a>
                    <ul>
                     <input type="text" class="form-control" name="exc_keywords" placeholder="Search Keywords" value="{{
                      app('request')->input('exc_keywords') }}">   
                   </ul>
                 </li>
               </ul>
             </li>

             <li class='has-sub'><a href='#'>Company</a>
              <ul>
                <input type="text" class="form-control" placeholder="Search Company" name="company" value="{{
                      app('request')->input('company') }}">      
                <li class='has-sub'><a href='#'>Exclude Company</a>
                  <ul>
                   <input type="text" class="form-control" placeholder="Search Company" name="exc_company" value="{{
                      app('request')->input('exc_company') }}">   
                 </ul>
               </li>
             </ul>
           </li>

           <li class='has-sub'><a href='#'>Designation</a>
            <ul>
              <input type="text" class="form-control" placeholder="Search Designation" name="designation" value="{{
                      app('request')->input('designation') }}">      

            </ul>
          </li>

          <li class='has-sub'><a href='#'>Experience ( in year )</a>
            <ul>
             <select class="form-control" name="exp_min">
              <option value="">Min</option>
              <?php for ($y=0; $y < 21; $y++) { ?>
                <option <?php if(!empty($_GET['exp_min'])) { if($_GET['exp_min']==$y) echo 'selected'; }?> value="<?php echo $y; ?>.0" ><?php echo $y.' Year'; ?></option>
              <?php } ?>
              </select>
              To
              <select class="form-control" name="exp_max">
                <option value="">Max</option>
                <?php for ($m=0; $m < 12; $m++) { ?>
                <option <?php if(!empty($_GET['exp_max'])) { if($_GET['exp_max']==$m) echo 'selected'; }?> value="<?php echo $m; ?>.0" ><?php echo $m.' Year'; ?></option>
                <?php } ?>
                </select>  

              </ul>
            </li>

            <li class='has-sub'><a href='#'>CTC (INR in Lac)</a>
              <ul>
                From 
                <select class="form-control" name="sal_min">
                  <option value="">Lac</option>
                  <?php for ($l=0; $l < 21; $l++) { ?>
                    <option <?php if(!empty($_GET['sal_min'])) { if($_GET['sal_min']==$l) echo 'selected'; }?> value="<?php echo $l; ?>" ><?php echo $l.' Lac'; ?></option>
                  <?php } ?>
                  </select>
                  <select class="form-control" name="sal_min1">
                    <option value="">Thousand</option>
                    <?php $t=0;
                    for ($t=0; $t < 100; $t++) { ?>
                      <option <?php if(!empty($_GET['sal_min1'])) { if($_GET['sal_min1']==$t) echo 'selected'; }?> value="<?php echo $t; ?>" ><?php echo $t.' Thousand'; ?></option>
                      <?php $t=$t+9;} ?>
                    </select> 
                    To
                    <select class="form-control" name="sal_max">
                      <option value="">Lac</option>
                      <?php for ($l1=0; $l1 < 21; $l1++) { ?>
                        <option <?php if(!empty($_GET['sal_max'])) { if($_GET['sal_max']==$l1) echo 'selected'; }?> value="<?php echo $l1; ?>" ><?php echo $l1.' Lac'; ?></option>
                      <?php } ?>
                      </select>
                      <select class="form-control" name="sal_max1">
                        <option value="">Thousand</option>
                        <?php $t1=0;
                        for ($t1=0; $t1 < 100; $t1++) { ?>
                          <option <?php if(!empty($_GET['sal_max1'])) { if($_GET['sal_min1']==$t1) echo 'selected'; }?> value="<?php echo $t1; ?>" ><?php echo $t1.' Thousand'; ?></option> 
                          <?php $t1=$t1+9;} ?>
                        </select> 


                      </ul>
                    </li>

                  </ul>

                </div>

                <div class="faq-profile-btn">
                  <button type="submit" class="btn btn-primary waves-effect waves-light space01">Search
                  </button>

                </div>
              </form>




            </div>

          </div>
           <button type="button" onclick="location.href = '{{url('cvparsing')}}';" class="btn btn-primary waves-effect waves-light space01">Back
                  </button>

        </div>

        <div class="col-xl-9 col-lg-8">

         <div class="card">
          <div class="card-header">

           <div class="top-menu-ji">

             <div class="check-box-i">

               <div class="rkmd-checkbox checkbox-rotate">
                <label class="input-checkbox checkbox-primary">
                  <input type="checkbox" id="checkbox11" class="myCheckbox" onclick="myCheckbox()">
                  <span class="checkbox"></span>
                </label>

              </div>
            </div>

            <ul>


              <li> <a href="javascript:void(0)" class="left_space_10">                                
               <i class="icofont icofont-copy-alt coiy-en no_icon_space"></i> Add To <i class=" icofont icofont-simple-down"></i></a>


               <ul class="sub-drop">  
                <li><a href="">Folder</a></li>
              </ul>

            </li>
            <li><a href="javascript:void(0)"><i class="icofont icofont-envelope coiy-en no_icon_space"></i> Email<i class=" icofont icofont-simple-down"></i></a>
             <ul class="sub-drop">
              <li><a href="javascript:void(0)" onclick="sendtomail()">Send To</a></li>
            </ul>
          </li>
          <li> <span id="sendmail"></span></li>

        </ul>

      </div>
      
      
            <div class="top-menu-ji-w">
      <div class="form-radio">
      <strong>Filter</strong> 
            <div class="radio radiofill radio-inline">
              <label>
                <input type="radio" name="radio" <?php if(@$_GET['filter']=='Both') { echo 'checked'; } ?> value="both"><i class="helper"></i>Both
              </label>
            </div>
            <div class="radio radiofill radio-inline">
              <label>
                <input type="radio" name="radio" <?php if(empty(@$_GET['filter']) || @$_GET['filter']=='Database') { echo 'checked'; } ?> value="database"><i class="helper"></i>Database
              </label>
            </div>
                                  
                                  <div class="radio radiofill radio-inline">
              <label>
                <input type="radio" name="radio" value="monster"><i class="helper"></i>Monster
              </label>
            </div>
            
        </div> 
      </div>
      

    </div>

    <!-- end of modal -->

  </div>

<?php
  class pagination
  {

    /**
     * Properties array
     * @var array   
     * @access private 
     */
    private $_properties = array();

    /**
     * Default configurations
     * @var array  
     * @access public 
     */
    public $_defaults = array(
      'page' => 1,
      'perPage' => 10 
    );

    /**
     * Constructor
     * 
     * @param array $array   Array of results to be paginated
     * @param int   $curPage The current page interger that should used
     * @param int   $perPage The amount of items that should be show per page
     * @return void    
     * @access public  
     */
    public function __construct($array, $curPage = null, $perPage = null)
    {
      $this->array   = $array;
      $this->curPage = ($curPage == null ? $this->defaults['page']    : $curPage);
      $this->perPage = ($perPage == null ? $this->defaults['perPage'] : $perPage);
    }

    /**
     * Global setter
     * 
     * Utilises the properties array
     * 
     * @param string $name  The name of the property to set
     * @param string $value The value that the property is assigned
     * @return void    
     * @access public  
     */
    public function __set($name, $value) 
    { 
      $this->_properties[$name] = $value;
    } 

    /**
     * Global getter
     * 
     * Takes a param from the properties array if it exists
     * 
     * @param string $name The name of the property to get
     * @return mixed Either the property from the internal
     * properties array or false if isn't set
     * @access public  
     */
    public function __get($name)
    {
      if (array_key_exists($name, $this->_properties)) {
        return $this->_properties[$name];
      }
      return false;
    }

    /**
     * Set the show first and last configuration
     * 
     * This will enable the "<< first" and "last >>" style
     * links
     * 
     * @param boolean $showFirstAndLast True to show, false to hide.
     * @return void    
     * @access public  
     */
    public function setShowFirstAndLast($showFirstAndLast)
    {
        $this->_showFirstAndLast = $showFirstAndLast;
    }

    /**
     * Set the main seperator character
     * 
     * By default this will implode an empty string
     * 
     * @param string $mainSeperator The seperator between the page numbers
     * @return void    
     * @access public  
     */
    public function setMainSeperator($mainSeperator)
    {
      $this->mainSeperator = $mainSeperator;
    }

    /**
     * Get the result portion from the provided array 
     * 
     * @return array Reduced array with correct calculated offset 
     * @access public 
     */
    public function getResults()
    {
      // Assign the page variable
      if (empty($this->curPage) !== false) {
        $this->page = $this->curPage; // using the get method
      } else {
        $this->page = 1; // if we don't have a page number then assume we are on the first page
      }
      
      // Take the length of the array
      $this->length = count($this->array);
      
      // Get the number of pages
      $this->pages = ceil($this->length / $this->perPage);
      
      // Calculate the starting point 
      $this->start = ceil(($this->page - 1) * $this->perPage);
      
      // return the portion of results
      return array_slice($this->array, $this->start, $this->perPage);
    }
    
    /**
     * Get the html links for the generated page offset
     * 
     * @param array $params A list of parameters (probably get/post) to
     * pass around with each request
     * @return mixed  Return description (if any) ...
     * @access public 
     */
    public function getLinks($params = array())
    {
      // Initiate the links array
      $plinks = array();
      $links = array();
      $slinks = array();
      
      // Concatenate the get variables to add to the page numbering string
      $queryUrl = '';
      if (!empty($params) === true) {
        unset($params['page']);
        $queryUrl = '&amp;'.http_build_query($params);
      }
      
      // If we have more then one pages
      if (($this->pages) > 1) {
        // Assign the 'previous page' link into the array if we are not on the first page
        if ($this->page != 1) {
          if ($this->_showFirstAndLast) {
            $plinks[] = ' <a href="?page=1'.$queryUrl.'">&laquo;&laquo; First </a> ';
          }
          $plinks[] = ' <a href="?page='.($this->page - 1).$queryUrl.'">&laquo; Prev</a> ';
        }
        
        // Assign all the page numbers & links to the array
        for ($j = 1; $j < ($this->pages + 1); $j++) {
          if ($this->page == $j) {
            $links[] = ' <a class="selected">'.$j.'</a> '; // If we are on the same page as the current item
          } else {
            $links[] = ' <a href="?page='.$j.$queryUrl.'">'.$j.'</a> '; // add the link to the array
          }
        }

        // Assign the 'next page' if we are not on the last page
        if ($this->page < $this->pages) {
          $slinks[] = ' <a href="?page='.($this->page + 1).$queryUrl.'"> Next &raquo; </a> ';
          if ($this->_showFirstAndLast) {
            $slinks[] = ' <a href="?page='.($this->pages).$queryUrl.'"> Last &raquo;&raquo; </a> ';
          }
        }
        
        // Push the array into a string using any some glue
        return implode(' ', $plinks).implode($this->mainSeperator, $links).implode(' ', $slinks);
      }
      return;
    }
  }
 $array = (array) $getpostion;
          // some example data
           echo '<div id="database" class="both">';
        foreach ($array as $value) {

       
     //   print_r($array['items']);
        // If we have an array with items
        if (count($value)) {
          // Create the pagination object
          $pagination = new pagination($value, (isset($_GET['page']) ? $_GET['page'] : 1), 5);
          // Decide if the first and last links should show
          $pagination->setShowFirstAndLast(false);
          // You can overwrite the default seperator
          $pagination->setMainSeperator(' | ');
          // Parse through the pagination class
          $productPages = $pagination->getResults();
          // If we have items 
          if (count($productPages) != 0) {
            // Create the page numbers
            $pageNumbers = '<div class="numbers">'.$pagination->getLinks($_GET).'</div>';
            // Loop through all the items in the array
            foreach ($productPages as $productArray) {
              // Show the information about the item
             //print_r($productArray);
              $str = $productArray->created_at;
              $str = explode(" ", $str);
              $aa =explode("-", $str[0]);
              //print_r($aa); //die;
              

              $url='/storage/app/uploadcv/'.$aa[0]."/".$aa[1]."/".$aa[2]."/".$productArray->cv_name;
            //  print_r($url);die;
//alert(data1[i]["cv_status"]);
              ?>

  <div class="card">

    <div class="to-do-list widget-to-do-list widget_list1" style=" padding-top:15px;">
      <div class="rkmd-checkbox checkbox-rotate">
        <label class="input-checkbox checkbox-primary">
          <input type="checkbox" id="checkbox11" name="myCheckboxInd" class="myCheckboxInd" value="{{ $productArray->id }}">
          <span class="checkbox"></span>
        </label>
        <label>{{ $productArray->candidate_name }} </label> ({{ $productArray->candidate_email }})
      </div>
    </div>


    <!-- end of modal -->
    <div class="card-block">

      <div class="col-md-8">

        <div class="left-link left_link">

         <ul>

          <li><i class="icofont icofont-bag"></i>{{ $productArray->total_exp }}</li>
          <li><i class="icon-wallet"></i> <i class="icofont icofont-cur-rupee no_icon_space"></i>{{ $productArray->current_ctc }} Lac</li>
          <!--  <li><i class="icon-location-pin"></i>Chennai</li> -->

        </ul>

        <div class="pro-dcre">

          <ul>
            <li><span>Current Organization</span> {{ $productArray->current_org }}</li></br>
            <li><span>Education </span> {{ $productArray->highest_qulification }}</li></br>
            <li><span>Key Skills </span> {{ $productArray->primary_skill }}</li></br>
            <li><span>Notice Period </span> {{ $productArray->np }}</li></br>
                                         <!-- <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                          <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                          <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li> -->
                                        </ul>

                                      </div>

                                      <h5>Similar Resumes</h5>


                                    </div>

                                  </div>

                                  <div class="col-md-4">
                                    <div class="right-link">

                             <!--    <div class="top-oiu">
                                
                                <img src="assets/images/avatar-4.png" alt="Logo">
                                
                              </div> -->

                              <div class="dat-down">
                                <p><strong>{{ $productArray->primary_skill }}</strong> with {{ $productArray->highest_qulification }} <!-- currently living in Chennai --></p>

                                <div class="form-group row">

                                  <div class="col-md-12">
                                    <div class="input-group">
                                      <span class="input-group-btn" id="btn-addon1"><button type="submit" class="btn btn-success shadow-none addon-btn waves-effect waves-light"><i class="icofont icofont-ui-call"></i></button></span>
                                      <input type="text" id="btnaddon1" class="form-control" aria-describedby="btn-addon1" value="{{ $productArray->candidate_mob }}" readonly="">
                                    </div>
                                  </div>


                                </div>

                                <ul>
                                  

                                </ul>

                                <ul>
                                  <li><div class="to-do-list widget-to-do-list">
                                    <div class="rkmd-checkbox checkbox-rotate">
                                      <label class="input-checkbox checkbox-primary">
                                        <span class="checkbox"></span>
                                      </label>
                                      <label style="font-size:12px;"><a href="{{URL::to($url)}}" class="btn btn-primary waves-effect waves-light" download="">Download CV</a></label>
                                    </div>
                                  </div></li>


                                </ul>
                              </div>
                              
                              
                            </div>

                            
                          </div>



                        </div>
                      </div>
              <?php
            }
            echo '</div><div id="monster" class="both">';
            ?>
            
 

  <!-- -----------------------------Monster-------------------------->
  <?php if(@$_GET['filter']=='Both')
  {
  /*print_r("<pre>");
    print_r($getmonsterkeys['catid']);
    print_r($getmonster);exit;*/
    if(empty($getmonster))
    {
        echo '<div class="card">
          <div class="to-do-list widget-to-do-list widget_list1" style="padding-top:15px;">
            <div class="rkmd-checkbox checkbox-rotate">
              <label class="input-checkbox checkbox-primary">
                <span class="checkbox"></span>
              </label> 
              <label><b>Monster!</b> You have generated too many requests to the server. please wait....</label>
            </div>
          </div>
        </div>';
    }
    else
    {
 /* $catid='53616c7465645f5f8b3e69ccab261e17ba6b11f8b87e88ea';
  $vendor_key='ac1f1eb3439ec67a591ce1dcd5fef15c';*/
  $catid=$getmonsterkeys['catid'];
  $vendor_key=$getmonsterkeys['vendorkey'];
  $key1='@attributes';
  $i=0;
  foreach ($getmonster as $key => $value) 
  { $i++;
    $SID=$value[$key1]['SID'];
    /*print_r("<pre>");
    print_r($value);
*/
    $cvurl="https://recruiter.monsterindia.com/v2/resumedatabase/resume.html?ResumeSID=".$SID."&ver=1.2&cat=".$catid."&vendor_key=".$vendor_key;
    $resp=file_get_contents($cvurl);

    $arra= simplexml_load_string($resp);

    $jsonr  = json_encode($arra);
    $configDatar = json_decode($jsonr, true);
    if($configDatar['MonsterHeader']['MessageData'][$key1]['Status']=='Success')
    {

    $cvdata=$configDatar['JobSeekers'];

    $result = simplexml_load_file($cvurl, 'SimpleXMLElement', LIBXML_NOCDATA);
   

    $skill=(string)($result->JobSeekers->JobSeeker->Profile->KeySkills);
    $curemp=(string)($result->JobSeekers->JobSeeker->Profile->CurrentEmployer);
    $sal=(string)($result->JobSeekers->JobSeeker->Profile->CurrentSalary);
    $edu=(string)($result->JobSeekers->JobSeeker->Profile->Educations->HighestEducationDegree->Level);

    $name= $arra->JobSeekers->JobSeeker->PersonalData->Contact->StructuredName->GivenName.' '.$arra->JobSeekers->JobSeeker->PersonalData->Contact->StructuredName->FamilyName;
    $email=$cvdata['JobSeeker']['PersonalData']['Contact']['E-mail'];
    $phone=$cvdata['JobSeeker']['PersonalData']['Contact']['Phones'][0]['Phone'];
    $exp=$cvdata['JobSeeker']['Profile']['TotalYearsWorkExperience']['@attributes']['Year'].'.'.$cvdata['JobSeeker']['Profile']['TotalYearsWorkExperience']['@attributes']['Month'];
 
    
    $cvformat=base64_decode($configDatar['JobSeekers']['JobSeeker']['Resume']['DetailedResume']);
    $cvkey=($configDatar['JobSeekers']['JobSeeker']['Resume']['DetailedResume']);

   #print_r($value);
     #exit;
   
    ?>

 <div class="card">

    <div class="to-do-list widget-to-do-list widget_list1" style=" padding-top:15px;">
      <div class="rkmd-checkbox checkbox-rotate">
        <label class="input-checkbox checkbox-primary">
         <!--  <input type="checkbox" id="checkbox11" name="myCheckboxInd" class="myCheckboxInd" value="{{ $productArray->id }}"> -->
          <span class="checkbox"></span>
        </label> 
        <label>{{ $name }} </label> ({{ $email }})
      </div>
    </div>


    <div class="card-block">

      <div class="col-md-8">

        <div class="left-link left_link">

         <ul>

          <li><i class="icofont icofont-bag"></i>{{ $exp }}yrs</li>
          <li><i class="icon-wallet"></i> <i class="icofont icofont-cur-rupee no_icon_space"></i>{{ $sal }} Lac</li>
         

        </ul>

        <div class="pro-dcre">

          <ul>
            <li><span>Current Organization</span>{{ $curemp }}</li></br>
            <li><span>Education </span> {{ $edu }}</li></br>
            <li><span>Key Skills </span>{{ $skill }}</li></br>
            <li><span>Notice Period </span> N/A</li></br>
        </ul>
      </div>

                                      <h5>Similar Resumes</h5>


                                    </div>

                                  </div>

                                  <div class="col-md-4">
                                    <div class="right-link">

                          

                              <div class="dat-down">
                                <p class="hvr-pulse-grow"><strong>Monster</strong></p>

                                <div class="form-group row">

                                  <div class="col-md-12">
                                    <div class="input-group">
                                      <span class="input-group-btn" id="btn-addon1"><button type="submit" class="btn btn-success shadow-none addon-btn waves-effect waves-light"><i class="icofont icofont-ui-call"></i></button></span>
                                      <input type="text" id="btnaddon1" class="form-control" aria-describedby="btn-addon1" value="{{ $phone }}" readonly="">
                                    </div>
                                  </div>


                                </div>

                                <ul>
                                  

                                </ul>

                                <ul>
                                  <li><div class="to-do-list widget-to-do-list">
                                    <div class="rkmd-checkbox checkbox-rotate">
                                      <label class="input-checkbox checkbox-primary">
                                        <span class="checkbox"></span>
                                      </label>
                                      <label style="font-size:12px;"> 
                                     <form method="post" name="position" action="{{ URL::to('downloadcv') }}" novalidate="novalidate" target="_blank">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="key" value="<?php echo $cvkey; ?>">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Download CV</button>
                                     </form>
                                     <!--<a href="javascript:void(0)" class="btn btn-primary waves-effect waves-light" id="downloadpdfff<?php echo $i; ?>">Download CV</a>-->
                                    </label>
                                    </div>
                                  </div></li>


                                </ul>
                              </div>
                              
                              
                            </div>

                            
                          </div>



                        </div>
                      </div> 
                      
   
<?php }
else
{
  echo ' <div class="card">

    <div class="to-do-list widget-to-do-list widget_list1" style=" padding-top:15px;">
      <div class="rkmd-checkbox checkbox-rotate">
        <label class="input-checkbox checkbox-primary">
          
          <span class="checkbox"></span>
        </label> 
        <label><b>Monster!</b> You have generated too many requests to the server. please wait....
      </div>
    </div></div>';
    break;
} 
}
        
 }
}
echo '</div>';
?>
  <!-- -----------------------------Monster-------------------------->

            
            <?php
            // print out the page numbers beneath the results
            echo $pageNumbers;
          }
        }
      } //die;
  ?>

                      <div></div>

                    </div>
                  </div>

                         


                </div>

                <script>
                  function myCheckbox()
                  { 
                    if($('.myCheckbox').prop('checked')) 
                    {
                      $('.myCheckboxInd').prop('checked', true);
                    } 
                    else 
                    {
                      $('.myCheckboxInd').prop('checked', false);
                    }
                  }


                  function sendtomail()
                  {
                    var selectedLanguage = new Array();
                    $('input[name="myCheckboxInd"]:checked').each(function() {
                      selectedLanguage.push(this.value);
                    });
//alert("Number of selected Languages: "+selectedLanguage.length+"\n"+"And, they are: "+selectedLanguage);
if(selectedLanguage.length > 0)
{ 
  //alert(selectedLanguage);
  $('#sendmail').css('color','green').html('Please wait...');
  $.ajax({
   url: '{{URL::route('advancesearchmail')}}',
   type: 'POST',
   data: {'mailid':selectedLanguage, '_token': "{{ csrf_token() }}",},
     success:function(data){ //alert(data);return false;
      if(data==1)
      {
        $('#sendmail').css('color','green').html('Successfully send mail!');return false;
      } 
      else
      {
        $('#sendmail').css('color','red').html('Not send mail! please try again!');return false;
      }
    }
  });
}
else
{
  $('#sendmail').css('color','red').html('Please select atleast one profile!');
}


}





(function($){
  $(document).ready(function(){

    $('#cssmenu li.active').addClass('open').children('ul').show();
    $('#cssmenu li.has-sub>a').on('click', function(){
      $(this).removeAttr('href');
      var element = $(this).parent('li');
      if (element.hasClass('open')) {
       element.removeClass('open');
       element.find('li').removeClass('open');
       element.find('ul').slideUp(200);
     }
     else {
       element.addClass('open');
       element.children('ul').slideDown(200);
       element.siblings('li').children('ul').slideUp(200);
       element.siblings('li').removeClass('open');
       element.siblings('li').find('li').removeClass('open');
       element.siblings('li').find('ul').slideUp(200);
     }
   });

  });
})(jQuery);

<?php 
$queryString=$_SERVER['QUERY_STRING'];
$queryUrl = '';
if(!empty($queryString) === true) 
{
  parse_str($queryString, $params);
  unset($params['filter']);
  unset($params['page']);
  $queryUrl = 'adsearch?'.http_build_query($params);
}  

?>

$(document).ready(function() {
$('input[type=radio]').change(function() {
  if (this.value == 'both') 
  {
    var url='<?php echo $queryUrl; ?>'+'&filter=Both';
    window.location.href=url;
  }
  else if (this.value == 'database') 
  {
    var url='<?php echo $queryUrl; ?>'+'&filter=Database';
    window.location.href=url;
  }
  else if (this.value == 'monster')
  {
    var url='<?php echo $queryUrl; ?>'+'&filter=Monster';
    window.location.href=url;
  }
});
});
</script>



@endsection