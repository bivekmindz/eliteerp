<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>First Submission report</title>
    <style>
        .main{

            max-width: 6750px;
            width: 100%;
            overflow: hidden;
            border: solid 1px #CCCCCC;
            border-right: solid 1px #CCCCCC;
            font-family: Rubik, sans-serif;
            font-size: 13px;
            margin: 0 auto;background-color: #f5f5f5;}
        .main p{    line-height: 25px;}
        .main .logo{ float:right;}
        .main .head-f{ color:#333;     font-weight: 600;}
        .main .logo img{     width: 108px;
            padding: 10px 13px;}
        .pad_le{ padding:15px 20px;}
    </style>
</head>

<body>
<div class="main">

    <div class="pad_le">

        <div class="logo"> <img src="public/images/logo.png" /></div>

        <p class="head-f">Dear Team,</p>

       <table border="1">
             <thead>
             <tr>
        <th> Emp Code</th>
            <th> Emp Name</th>
      
             <th> Punchin Time</th>
              <th>Position Start time</th>
              <th> Pre Time</th>
             <th> Post Time</th>    
          
                 <th> Total Break Time</th>   
             
               
          
            
             
             </tr>
             </thead>
            <tbody>

            @if(!empty($mail_data))
            @foreach($mail_data as $value)
              
                 @php
$numpre=0;

          

               @endphp
                <tr>
                <td>{{ $value['empcode'] }}</td>
                 <td>{{ $value['empname'] }}</td>
         
              @php
              $punchrecord=explode(",",$value['punchrecord']);

              $startpunch = $punchrecord[0];
                     $to = \Carbon\Carbon::createFromFormat('H:s:i', $value['clientstart_starttime']);
                  
                 
                      $to_time = strtotime( $value['clientstart_starttime'] );


                     if($startpunch!='')
                     {
 $from_time = strtotime($startpunch);


 $diff_in_minutes  =round(((($to_time - $from_time) / 60)/30)). "";

}
else
{
 $diff_in_minutes ="late comers";
  $numpre +=0 ;
}

//dd($diff_in_minutes);
      
              @endphp
                  <td>{{ $startpunch }}</td>
            
                    <td>{{ $value['clientstart_starttime'] }}</td>
                  <td>
<?php $diffres=0;
if(count($value['clientstartend'])>0) {
 foreach($value['clientstartend'] as $keyus => $ks){

if($keyus=='0')
{
  $ks->clientstart_stoptimen=$ks->clientstart_starttime;
$ks->clientstart_starttime= $startpunch;

}
else
{
 // print_r($resumes[$keyus-1]->clientstart_stoptime);
  if($value['clientstartend'][$keyus-1]->clientstart_stoptime!='')
  {$ks->clientstart_starttime=$value['clientstartend'][$keyus-1]->clientstart_stoptime;}
else {$ks->clientstart_starttime='09:30:00';}

  $ks->clientstart_stoptimen=$ks->clientstart_stoptime;
}


    if( $ks->clientstart_stoptime!='') {
                   $stoprecords = strtotime( $ks->clientstart_stoptimen);
                     $stoprecordns = ( $ks->clientstart_stoptimen);
    }
    else
    {
        $stoprecords = strtotime( '19:00:00');
         $stoprecordns =( '19:00:00');
    }

      $startrecords = strtotime($ks->clientstart_starttime);


 $startrecordnews =strtotime(date('H:s:i', $startrecords));
$stoprecordnews =strtotime(date('H:s:i', $stoprecords));

//  $diff_in_minutess =round(((($stoprecords - $startrecords) )));
$diff_in_minutess  =round(((($stoprecords - $startrecords) / 60)/30));
$diffres +=$diff_in_minutess;
//$numpre +=$diff_in_minutess;
 }}
?>

                    <?php 
if($startpunch!='') {
                    if($diffres>0  ) {
if($diff_in_minutes!="Not On Time"  ) { 
                     ?>

<?php  $numpre +=$diffres ; ?>
                    {{  ($diffres) }}
                  

<?php } } } else { echo "Not On Time";} ?>
<!-- 
<?php for ($i = 0; $i < ($diff_in_minutes); $i++) {
  $t1=30*$i;
  $t2=30*($i+1);
$time = strtotime($startpunch);
$startTime = date("H:i:s", strtotime($t1.'minutes', $time));
$endTime = date("H:i:s", strtotime($t2.' minutes', $time));
echo $startTime.'--'.$endTime .'<br>';
}
 ?> -->

                  </td>
                    <th> 
<?php
$numstend=0;
if(count($value['resumes'])>0) {
$dff=0;
  foreach($value['resumes'] as $keyus => $k){
$ketcount=($keyus-1);
 if( $keyus>0)
 {
 
                 $startrecord = strtotime($value['resumes'][$ketcount]->cv_upload_time);
                   $stoprecord = strtotime( $k->cv_upload_time);

 $startrecordnew =strtotime(date('H:s:i', $startrecord));
$stoprecordnew =strtotime(date('H:s:i', $stoprecord));

                     if($stoprecordnew!='')
                     {

  $diff_in_minutesn  =round(((($stoprecordnew - $startrecordnew) / 60)/30)). "";
// echo $diff_in_minutesn;
if( $diff_in_minutesn>=0) { 
  $dff +=$diff_in_minutesn;
    
//$numstend +=$diff_in_minutesn;
}
                     }

}


 }
?>

<?php
$numstend +=$dff;
 echo $dff; ?> <?php
}
 ?></th>   
 
 <th><?php echo ($numpre+$numstend); ?></th>
</tr>

                @endforeach
            @endif
            </tbody>
         </table>



        <p> Please get in touch for any clarifications.</p>


        <p class="head-f">Regards,</p>

        <p>Elite Software</p>
    </div>
</div>

</body>
</html>

