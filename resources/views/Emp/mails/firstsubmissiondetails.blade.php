


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>First Submission report</title>
    <style>
        .main{

            max-width: 6750px;
            width: 100%;
            overflow: hidden;
            border: solid 1px #CCCCCC;
            border-right: solid 1px #CCCCCC;
            font-family: Rubik, sans-serif;
            font-size: 13px;
            margin: 0 auto;background-color: #f5f5f5;}
        .main p{    line-height: 25px;}
        .main .logo{ float:right;}
        .main .head-f{ color:#333;     font-weight: 600;}
        .main .logo img{     width: 108px;
            padding: 10px 13px;}
        .pad_le{ padding:15px 20px;}
    </style>
</head>

<body>
<div class="main">

    <div class="pad_le">

        <div class="logo"> <img src="public/images/logo.png" /></div>

        <p class="head-f">Dear Team,</p>

       <table border="1">
             <thead>
             <tr>
          <th> Emp Code</th>
            <th> Emp Name</th>
             <th> Position Name</th>
                  <th> Client Name</th>
             <th> Punchin Time</th>
              <th>Position Start time/First Submission time</th>
              <th> Time Difference between punchin and First submission</th>
               
          
            
             
             </tr>
             </thead>
            <tbody>

            @if(!empty($mail_data))
            @foreach($mail_data as $value)
               @php
                $compwithpos = explode("#",$value->position_work);

               @endphp
                <tr class="table-active">
                <td>{{ $value->empcode }}</td>
                 <td>{{ $value->empname }}</td>
                 <td>{{ $compwithpos[0] }}</td>
                    <td>{{ $compwithpos[1] }}</td>
              @php
              $startpunch = $value->punchrecord;
                     $to = \Carbon\Carbon::createFromFormat('H:s:i', $value->clientstart_starttime);
                  
                 
                      $to_time = strtotime( $value->clientstart_starttime );


                     if($startpunch!='')
                     {
 $from_time = strtotime($startpunch);


// $diff_in_minutes  =round(($to_time - $from_time) / 60,2). " minute";
$diff_in_minutess  =round(($to_time - $from_time) );
$diff_in_minutes  = gmdate("H:i:s", $diff_in_minutess);
}
else
{
 $diff_in_minutes ="Not On Time";
}

//dd($diff_in_minutes);
     
              @endphp
                  <td>{{ $startpunch }}</td>
            
                    <td>{{ $value->clientstart_starttime }}</td>
                  <td>{{  $diff_in_minutes }}</td>
                   
                </tr>

                @endforeach
            @endif
            </tbody>
         </table>



        <p> Please get in touch for any clarifications.</p>


        <p class="head-f">Regards,</p>

        <p>Elite Software</p>
    </div>
</div>

</body>
</html>

