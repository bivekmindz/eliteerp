


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Daily Submission report</title>
    <style>
        .main{

            max-width: 850px;
            width: 100%;
            overflow: hidden;
            border: solid 1px #CCCCCC;
            border-right: solid 1px #CCCCCC;
            font-family: Rubik, sans-serif;
            font-size: 13px;
            margin: 0 auto;background-color: #f5f5f5;}
        .main p{    line-height: 25px;}
        .main .logo{ float:right;}
        .main .head-f{ color:#333;     font-weight: 600;}
        .main .logo img{     width: 108px;
            padding: 10px 13px;}
        .pad_le{ padding:15px 20px;}
        .table-mail-data{ width: 100%; float:left; padding:15px 0px;}
    </style>
</head>

<body>
<div class="main">

    <div class="pad_le">

        <div class="logo"> <img src="assets/images/logo.png" /></div>
         <p class="head-f">Submission Report </p>
        
      
  @if(!empty($mail_data))
            @foreach($mail_data as $value)

   <?php 
   $subm=0;
   if(!empty($value['userreport'])) { ?>
<?php  foreach($value['userreport'] as $keyus => $k){
  if(!empty($value['userreport2'][$keyus])) {  $subm +=$value['userreport2'][$keyus]->sendclient; } else { $subm +=0; } 

?>


<?php } } ?>
        <?php if(!empty($value['userreport'])) { ?>
<table width="100%" border="1" cellspacing="0" cellpadding="0">

<tr>
    <td width="10%" style="border:none;" height="30" align="center"><strong style="color:#1f2ccc;">{{ $value['username']}},</strong></td>
    <td colspan="3" style="border:none;"><p>&nbsp;</p>
    <p>&nbsp;</p></td>
    <td width="24%" style="border:none;" align="center"><strong>Total Submissions: <span style="color:#1f2ccc;">{{$subm}}</span></strong></td>
  </tr>
  
  <tr>
    <th height="40" scope="col">Client</th>
    <th width="19%" scope="col">Position Name</th>
    <th width="29%" scope="col">Number of uploaded cv</th>
    <th width="18%" scope="col">Sent to Client</th>
    <th scope="col">Submissions</th>
  </tr>

  
<?php //echo  $c = count($value['userreport']);?>

<?php if(!empty($value['userreport'])) { ?>
<?php  foreach($value['userreport'] as $keyus => $k){?>
  <tr>
     <td><?php if(!empty($value['client'][$keyus])) { echo $value['client'][$keyus]->comp_name; }  ?></td>
      
        <td><?php if(!empty($value['position'][$keyus])) { echo $value['position'][$keyus]->clientjob_title; }  ?></td>
      
     <td><?php echo($k->trtr);  ?></td>
     <td><?php if(!empty($value['userreport2'][$keyus])) { echo $value['userreport2'][$keyus]->sendclient; } else { echo "0"; } ?></td>
        <td><?php if(!empty($value['userreport2'][$keyus])) { echo $value['userreport2'][$keyus]->sendclient; } else { echo "0"; } ?></td>
       
   </tr>
<?php } } ?>
    




</table>

<?php } ?>
    @endforeach
            @endif
            
        <p> Please get in touch for any clarifications.</p>


        <p class="head-f">Regards,</p>

        <p>Elite Software</p>
    </div>
</div>

</body>
</html>

