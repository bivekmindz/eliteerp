


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Daily Submission report</title>
    <style>
        .main{

            max-width: 850px;
            width: 100%;
            overflow: hidden;
            border: solid 1px #CCCCCC;
            border-right: solid 1px #CCCCCC;
            font-family: Rubik, sans-serif;
            font-size: 13px;
            margin: 0 auto;background-color: #f5f5f5;}
        .main p{    line-height: 25px;}
        .main .logo{ float:right;}
        .main .head-f{ color:#333;     font-weight: 600;}
        .main .logo img{     width: 108px;
            padding: 10px 13px;}
        .pad_le{ padding:15px 20px;}
        .table-mail-data{ width: 100%; float:left; padding:15px 0px;}
    </style>
</head>

<body>
<div class="main">

    <div class="pad_le">

        <div class="logo"> <img src="assets/images/logo.png" /></div>
         <p class="head-f">Follow Up Status </p>
        
      
  @if(!empty($mail_data))
           
<p><?php echo $mail_data['username']; ?></p>
<table width="100%" border="1" cellspacing="0" cellpadding="0">

<tr>
  <td> Candidate Name</td>  <td> Candidate Phone No</td>  <td> Candidate Email</td>
    <td> Candidate Status</td>    <td>Followed By </td><td>Position Name </td>
</tr>
  @foreach($mail_data['rcv'] as $rcv)
<tr>
  <td>{{$rcv->candidate_name}}</td>  <td>{{$rcv->candidate_mob}}</td>  <td>{{$rcv->candidate_email}}</td>
    <td>{{$rcv->tca_followupstatus}}</td>  <td>{{$rcv->assignid}}</td><td>{{$rcv->positionname}}</td>
</tr>


  @endforeach
    </table>
            @endif
            
        <p> Please get in touch for any clarifications.</p>


        <p class="head-f">Regards,</p>

        <p>Elite Software</p>
    </div>
</div>

</body>
</html>

