<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style>
.main{ 

    max-width: 650px;
    width: 100%;
    overflow: hidden;
    border: solid 1px #CCCCCC;
    border-right: solid 1px #CCCCCC;
    font-family: Rubik, sans-serif;
    font-size: 13px;
    margin: 0 auto;background-color: #f5f5f5;}
.main p{    line-height: 25px;}
.main .logo{ float:right;}
.main .head-f{ color:#333;     font-weight: 600;}
.main .logo img{     width: 108px;
    padding: 10px 13px;}
    .pad_le{ padding:15px 20px;}
</style>
</head>

<body>
<div class="main">

<div class="pad_le">

<div class="logo"> <img src="assets/images/logo.png" /></div>

<p class="head-f"> Dear ALL,</p>
 
<p> {{$name}} has uploaded profiles for following position {{$positionname}} of this Client {{$clientname}}. Please have a look at profiles. </p>

 <p>CVs
  <ul>
  @foreach($getcv as $kk => $getcvnew)
@php
   $da = $getcvnew->cv_upload_time;
    $da1 = substr($da,0,10);
    $a1 = explode('-',$da1);
 $candidateCv = 'uploadcv';
$dirStorage = DIRECTORY_SEPARATOR . $candidateCv . DIRECTORY_SEPARATOR . $a1[0] . DIRECTORY_SEPARATOR
                                                . $a1[1] . DIRECTORY_SEPARATOR . $a1[2];
  $con = config('constants.APP_URLS'); 
@endphp


<li><a href="{{ 'http://115.124.98.243/~elitehr/storage/app'.$dirStorage.'/'.$getcvnew->cv_name }}">{{$getcvnew->candidate_name}}</a></li>
    @endforeach
 </ul>
 </p>
<p class="head-f">Regards,</p>
 
<p>Elite hr practices software</p>
</div>
</div>

</body>
</html>
