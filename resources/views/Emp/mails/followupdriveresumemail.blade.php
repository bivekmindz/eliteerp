


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style>
        .main{

            max-width: 900px;
            width: 100%;
            overflow: hidden;
            border: solid 1px #CCCCCC;
            border-right: solid 1px #CCCCCC;
            font-family: Rubik, sans-serif;
            font-size: 13px;
            margin: 0 auto;background-color: #f5f5f5;}
        .main p{    line-height: 25px;}
        .main .logo{ float:right;}
        .main .head-f{ color:#333;     font-weight: 600;}
        .main .logo img{     width: 108px;
            padding: 10px 13px;}
        .pad_le{ padding:15px 20px;}
    </style>
</head>

<body>
<div class="main">

    <div class="pad_le">

        <div class="logo"> <img src="assets/images/logo.png" /></div>

        <p class="head-f">Dear Team,</p>

        <table border="1">
             <thead>
             <tr>
                  <th>Company Name</th>
      <th>Position Name</th>
        <th>Drive Date</th>
     <th>No of Line UP</th>
       <th>Pending</th>
         <th>Confirm</th>
           <th>Not Going</th>
             <th>Not Reachable</th>
                 <th>Not Responding</th>
             <th>Dicey</th>
               
               
              
             </tr>
             </thead>
            <tbody>

            @if(!empty($mail_data))
            @foreach($mail_data as $p)

                <tr class="table-active">
            <td>{{$p->comp_name}}</td>
      <td>{{ !empty($p->clientjob_title) ? $p->clientjob_title : 'N/A' }}</td>
       <td>{{$p->drive_time}}</td>
<td>{{ !empty($p->total_rec_cv) ? $p->total_rec_cv : 'N/A' }}</td>
      <td> {{$p->pending}} </td>
     <td>{{$p->Confirm}}     
         </td>
       <td> {{$p->NotGoing}}
           </td>
         <td>{{$p->NotReachable}}
             </td>
           <td>
        {{$p->NotResponding}}       </td>
     <td>{{$p->Dicey}}       </td>
     
    
    
    
          
                </tr>

                @endforeach
            @endif
            </tbody>




         </table>


        <p> Please get in touch for any clarifications.</p>


        <p class="head-f">Regards,</p>

        <p>Elite Software</p>
    </div>
</div>

</body>
</html>

