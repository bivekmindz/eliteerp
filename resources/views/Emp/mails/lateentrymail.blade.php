


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Daily Cv uploaded report</title>
    <style>
        .main{

            max-width: 650px;
            width: 100%;
            overflow: hidden;
            border: solid 1px #CCCCCC;
            border-right: solid 1px #CCCCCC;
            font-family: Rubik, sans-serif;
            font-size: 13px;
            margin: 0 auto;background-color: #f5f5f5;}
        .main p{    line-height: 25px;}
        .main .logo{ float:right;}
        .main .head-f{ color:#333;     font-weight: 600;}
        .main .logo img{     width: 108px;
            padding: 10px 13px;}
        .pad_le{ padding:15px 20px;}
    </style>
</head>

<body>
<div class="main">

    <div class="pad_le">

        <div class="logo"> <img src="public/images/logo.png" /></div>

        <p class="head-f">Dear Team,</p>
<p>LATE COMERS REPORT</p>
       <table border="1">
             <thead>
             <tr>
            <th> Emp Code</th>
            <th> Emp Name</th>
         
             <th> Punchin Time</th>
             <th> Late By</th>
             </tr>
             </thead>
            <tbody>

            @if(!empty($mail_data))
            @foreach($mail_data as $value)
     @php
              $punchrecord=explode(",",$value->punchrecord);
               $startpunch = $punchrecord[0];
               
$stoppunch=$value->lastpunch;
  $to_time1 = strtotime( $value->lastpunch );
                    $from_time1 = strtotime($startpunch); 
 $diff_in_minutesnew  =round(((($from_time1 - $to_time1) / 60))); 
 if($diff_in_minutesnew>0)
 {
   $startpunch = $value->lastpunch;
 }
 else
 {
   $startpunch = $punchrecord[0];
 }
 
 
            
                    $to_time = strtotime( "09:46:00" );
                    $from_time = strtotime($startpunch); 
      $diff_in_minutes  =round(((($from_time - $to_time))));
      if($diff_in_minutes >0) {
       $diff_in_minutesne  =round(((($from_time - $to_time) / 60))); 
$seconds = $diff_in_minutes;
$ee= gmdate('H:i:s', $seconds);
              @endphp
                <tr class="table-active">
               <td>{{ $value->empcode }}</td>
                 <td>{{ $value->empname }}</td>
         
             
                  <td>{{ $startpunch  }}</td>
              <td>{{ $ee  }}</td>
        

                </tr>
@php } @endphp
                @endforeach
            @endif
            </tbody>
         </table>



        <p> Please get in touch for any clarifications.</p>


        <p class="head-f">Regards,</p>

        <p>Elite Software</p>
    </div>
</div>

</body>
</html>

