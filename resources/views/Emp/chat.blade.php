@extends('Emp.layouts.master')

@section('content')

<?php //echo "<pre>"; print_r($list);?>


        <!-- Container-fluid starts -->
        <div id="main-chat" class="container-fluid">
            <!-- Main content starts -->

            <!-- Row Starts -->
            <div class="row">
                <div class="col-sm-12 p-0">
                    <div class="main-header" style="margin-top: 0px;">
                        <h4>Chat</h4>
                        <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                            </li>
                            <li class="breadcrumb-item"><a href="#!">Apps</a>
                            </li>
                            <li class="breadcrumb-item"><a href="chat.html">Chat</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <!-- Row end -->

            <div class="row">
                <div class="chat-box">

<!-- Coding Start For User Chat System -->
<?php $url_segment = \Request::segment(2);
      if(!empty($url_segment)){
?>
                    <ul class="text-right boxs" id="chatbox">
                      <li class="chat-single-box card-shadow bg-white active" data-id="1">
                        <div class="had-container">
                        <!--head-->
                          <div class="chat-header p-10 bg-gray">
                            <div class="user-info d-inline-block f-left">
                              <div class="box-live-status bg-danger  d-inline-block m-r-10"></div>
                              <a><?php $name = DB::select(DB::raw('select name from users where id='.$url_segment)); print_r(ucfirst($name[0]->name));?>
                              </a>
                            </div>
                            <div class="box-tools d-inline-block">
                          <!--     <a href="#" class="mini">
                                <i class="icofont icofont-minus f-20 m-r-10"></i>
                              </a> -->
                              <a class="close" href="#">
                                <i class="icofont icofont-close f-20"></i>
                              </a>
                            </div>
                          </div>
                        <!--End Head-->

                       
                          <div class="chat-body p-10">
                          
                            <div class="message-scrooler">

                            <!--Ajax Auto Refrresh Code -->
                              <div class="messages" id="responce" style="position: relative;">
                              @php $logid = $loginId = Auth::user()->id @endphp
                              @foreach($msg as $sms)
                              @if($sms->from == $logid)
                              <div class="message out no-avatar  media m-b-20">
                                    <div class="body media-body text-right p-l-50">
                                        <p style="background-color: #2a653d!important;margin-bottom: 2px!important;border-radius: 0px 10px 0px!important;" class="content msg-reply p-5 f-12 bg-primary d-inline-block">{{$sms->msg}}
                                        </p>
                                        <div class="seen">
                                            <i class="icon-clock f-12 m-r-5 txt-muted d-inline-block"></i>
                                                <span><p  class="d-inline-block"><small>{{date('M d H:i',strtotime($sms->created_at))}}</small></p></span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="message out no-avatar  media m-b-20" >
                                    <div class="body media-body text-right p-r-50">
                                        <p style="margin-bottom: 2px!important;border-radius: 0px 10px 0px!important;" class="content msg-reply p-5 f-12 bg-primary d-inline-block">{{$sms->msg}}
                                        </p>
                                        <div class="seen">
                                            <i class="icon-clock f-12 m-r-5 txt-muted d-inline-block"></i>
                                                <span><p class="d-inline-block"><small>{{date('M d H:i',strtotime($sms->created_at))}}</small></p></span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                              @endforeach
                              </div>
                            <!--End Auto Refresh --> 


                            </div>
                          </div>
                          
                        <form method="post" id="form"> 
                         <!-- Msg input section--> 
                          <div class="chat-footer b-t-muted">
                            <div class="input-group write-msg">
                              <input type="text" id="chat" value="" class="form-control input-value" placeholder="Type a Message">
                              <input type="hidden" id="to" value="<?php echo $url_segment = \Request::segment(2);?>" class="form-control input-value" >
                                <span class="input-group-btn">
                                  <button type="  " id="paper-btn" class="btn btn-secondary ajax">
                                    <!-- <i class="fa fa-paper-plane"> -->Send<!-- </i> -->
                                  </button>
                                </span>
                            </div>

                          </div>
                          <!-- Msg end Input -->
                        </form>
                        </div>
                      </li>   
                    </ul>

<?php }?>





                    <div id="sidebar" class="p-fixed users p-chat-user">
                        <div class="had-container">
                            <div class="card card_main p-fixed users-main ">
                                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 608px;"><div class="card-block user-box" style="overflow: scroll; width: auto; height: 608px;">
                                    <div class="md-group-add-on p-relative md-float-material">
                                    <span class="md-add-on">
                                     <i class="icofont icofont-search-alt-2"></i>
                                    </span>
                                        <div class="md-input-wrapper">
                                            <input type="text" class="md-form-control search-text">
                                            <label>Search..</label>

                                            <span class="md-line"></span><span class="md-line"></span></div>
                                    </div>

                            @foreach($list as $li)
                                    <div class="media userlist-box" data-id="1" data-status="online" data-username="" data-toggle="tooltip" data-placement="left" title="" data-original-title="">
                                     <!--<a class="mr-3" href="chat/{{$li->id}}">
                                            <img class="media-object rounded-circle" src="../files/images/avatar-1.png" alt="Generic placeholder image">
                                            <div class="live-status bg-success"></div>
                                        </a>-->
                                        <div class="media-body">
                                           <a href="{{URL::to('chat')}}/<?php echo $li->id;?>">
                                           <div class="f-13 chat-header"><?=$li->name; $uid = $li->id;?>
                                           <span style="font-size: 12px;padding: 2px;border-radius: 50px;color: green";> &nbsp;
                                           <?php 
                                           $loginId = Auth::user()->id;
                                           $count = DB::select(DB::raw("select count(read_status) as c from tbl_chats where `from`=$uid and `to` = $loginId  and read_status=1"));   $ctr = $count[0]->c; if($ctr!='0'){ echo $ctr;}else{ echo '';}?>
                                           </span>
                                           </div></a>
                                        </div>
                                    </div>
                            @endforeach()


                                </div><div class="slimScrollBar" style="background: rgb(27, 139, 249); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 314.34px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    
<script>

    setInterval(function(){
      $(document).ready(function(){
      var data = '<?php echo $url_segment = \Request::segment(2);?>';

      $.ajax({
         type: "POST",
         url:  '{{URL::route('autorefresh')}}',
         data:{
                '_token': "{{ csrf_token() }}",
                'to':data,
            },
         
         success: function(data){
             $("#responce").show();
             $("#responce").html(data); 
         }
      });   
    });  
    },50);
</script>

<script>

$(document).ready(function(){
    $(".ajax").click(function(){
       var xxx = $("#chat").val();
       var to = $("#to").val();
       document.getElementById("chat").focus();
        document.getElementById("chat").value='';
          $.ajax({
                      type: 'POST',
                      url: '{{URL::route('insertchat')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'chat':xxx,
                        'to':to,
                      },
                      success: function(data){
                      }
                    });
    });
});



// Enter Chat Records Using Submit Cammand 
$(document).ready(function(){
    $("form").submit(function(e){
       event.preventDefault();
       var xxx = $("#chat").val();
       var to = $("#to").val();
       document.getElementById("chat").focus();
       document.getElementById("chat").value='';
          $.ajax({
                      type: 'POST',
                      url: '{{URL::route('insertchat')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'chat':xxx,
                        'to':to,
                      },
                      success: function(data){
                      }
                    });
    });
});

$(document).ready(function(){
    $(".close").click(function(){
        $("#chatbox").hide();
    })
})

$(document).ready(function(){
    $('#messagewindow').animate({
        scrollTop: $('#messagewindow')[0].scrollHeight}, 2000);
});
</script>
<script type="text/javascript">
  var height = 0;
$('.chat-body message').each(function(i, value){
    height += parseInt($(this).height());
});

height += '';

$('.chat-body').animate({scrollTop: height});
</script>
@endsection