<a href="javascript:void(0)" class="btn btn-primary waves-effect waves-light" id="cmd">Download CV</a>

<div id="content" style="width: 60%;padding-left: 9%;padding-right: 10px;"><?php echo base64_decode($cvformat); ?></div> 
<div id="editor"></div>

<script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
<script src="{{ URL::asset('js/jspdf.debug.js') }}"></script>
<!--<script src="{{ URL::asset('js/html2canvas.min.js') }}"></script>-->
<!--<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>-->

<script type="text/javascript">
    
    var doc = new jsPDF();
var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};

$('#cmd').click(function () {   
    doc.fromHTML($('#content').html(), 15, 15, {
        'width': 270,
            'elementHandlers': specialElementHandlers
    });
    doc.save('MonsterCV.pdf');
});

    </script>
    
<script>
function forprint()
{
	if (!window.print)
	{
		return
	}
	window.print()
}
		
window.onload=function()
{
 //forprint();
 //PrintWindow();
}

  function PrintWindow() {                    
       window.print();            
       CheckWindowState();
    }

    function CheckWindowState()    {           
        if(document.readyState=="complete") {
            window.close(); 
        } else {           
            setTimeout("CheckWindowState()", 2000)
        }
    }
    
	</script>
