@extends('Emp.layouts.master')

@section('content')

<style type="text/css">
  #search_filter_filter{float:left!important;}
</style>


<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View CV List</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Client Call CV List</a>
                </li>
                <li class="breadcrumb-item"><a href="">Allocated CVs List</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
                @endif
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                        <div id="search_filter_filter" class="dataTables_filter"><label>Search: <input type="search" id="filter_area" style="border: 1px solid #968f8f;" class="" placeholder="" aria-controls="search_filter" autofocus></label></div>
                           <div class="table-responsive table_green siro">
    

                            <table class="table table-bordered" id="">
                                <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Company Name</th>
                                    <th>Position Name</th>
                                       <th>Assigned From</th>
                                    <th>Position Drive Date</th>
                                  <th>No of Line UP</th>
                                    <!--<th>FollowUp Status</th>-->
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="suggesstion-box">
                                @php
                                $i=1;
                                @endphp
                                @foreach($assigneddetails as $key => $p)
                                <tr >
                                    <td>{{ $i++ }}</td>
                                    <td>{{ !empty($p->comp_name) ? $p->comp_name : 'N/A' }}</td>
                                    <td>{{ !empty($p->clientjob_title) ? $p->clientjob_title : 'N/A' }}</td>
                                    <td>{{ !empty($p->name) ? $p->name : 'N/A' }}</td>
                                     <td>{{ !empty($p->drive_time) ? $p->drive_time : '' }}
                                     {{ !empty($p->lineup_from) ? $p->lineup_to : '' }}
                                     </td>
                                    <td>{{ !empty($p->total_rec_cv) ? $p->total_rec_cv : 'N/A' }}</td>
                                    <!--<td>{{ $p->tca_followupstatus }}</td>-->
                                    <td>
<?php 
$date1 = $p->drive_time;
//$date1 = date('Y-m-d');
$date2 = date('Y-m-d');

$diff = (strtotime($date1) - strtotime($date2));

//$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
  $days = floor(($diff)/ (60*60*24));
//printf("%d", $days);
?>
<?php if( $days>'0') { ?>
  <a href="{{ url('viewmyallocatedfirstcvdetail', [$p->tca_assignfrom, $p->tca_assignto,$p->tca_positionid]) }}"><button type="button" class="btn btn-info btn-sm">View  Details</button></a> 

          
<?php } if( $days=='0') { ?>

                                     <!--    <a href="{{ url('viewmyallocatedcvdetail', [$p->tca_assignfrom, $p->tca_assignto,$p->tca_positionid]) }}"><button type="button" class="btn btn-info btn-sm">View Details</button></a>   -->
                       <?php } ?>      
                       
                       <?php 
 $date1 = $p->lineup_to;
//$date1 = date('Y-m-d');
$date2 = date('Y-m-d');

$diff = (strtotime($date1) - strtotime($date2));

//$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
  $days = floor(($diff)/ (60*60*24));
//printf("%d", $days);
?>
<?php if( $days>'0') { ?>
  <a href="{{ url('viewmyallocatedfirstcvdetail', [$p->tca_assignfrom, $p->tca_assignto,$p->tca_positionid]) }}"><button type="button" class="btn btn-info btn-sm">View  Details</button></a> 

          
<?php } if( $days=='0') { ?>

                       <?php } ?> 

    <?php 
 $date1 = $p->shortlist_time;
//$date1 = date('Y-m-d');
$date2 = date('Y-m-d');

$diff = (strtotime($date1) - strtotime($date2));

//$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
  $days = floor(($diff)/ (60*60*24));
//printf("%d", $days);
?>
<?php if( $days>'0') { ?>
  <a href="{{ url('viewmyallocatedfirstcvdetail', [$p->tca_assignfrom, $p->tca_assignto,$p->tca_positionid]) }}"><button type="button" class="btn btn-info btn-sm">View  Details</button></a> 

          
<?php } if( $days=='0') { ?>

                       <?php } ?> 


                                    </td>

                </tr>
                @endforeach
                </tbody>
                </table>
            </div> </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
</script>
@endsection

