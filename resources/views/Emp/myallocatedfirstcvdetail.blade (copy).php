@extends('Emp.layouts.master1')

@section('content')

<style type="text/css">
  #search_filter_filter{float:left!important;}
</style>


<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View CV List</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Client Call CV List</a>
                </li>
                <li class="breadcrumb-item"><a href="">Allocated CVs List Details</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
                @endif
                   <form action="{{ URL::to('allcvdetupdate') }}" method="post"> 
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                           <div id="search_filter_filter" class="dataTables_filter"><label><!-- <input type="submit" name="submit" value="change status"> -->
                               <button type="button"  id="buttonClass" class="btn btn-primary waves-effect waves-light">Change Status</button>


                           </label></div>
                    
                            <table class="table" id="">
                                <thead>
                                <tr>
                                   <th></th>
                                  
                                    <th>CandidateName</th>
                                    <th>CV Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>FollowUp Status</th>
                                    <th>Action</th> 
                                </tr>
                                </thead>
                                <tbody id="suggesstion-box">
                                @php
                                $i=1;
                                @endphp
                                @foreach($cvdetails as $key => $p)
                                <tr>
                                  <td><input type="checkbox" name="checkboxid" value="{{$p->tca_id}}"></td>

                                
                                  
                                    <td>{{ !empty($p->candidate_name) ? $p->candidate_name : 'N/A' }}</td>
                                    <td>{{ !empty($p->cv_name) ? $p->cv_name : 'N/A' }}</td>
                                    <td>{{ !empty($p->candidate_email) ? $p->candidate_email : 'N/A' }}</td>
                                    <td>{{ !empty($p->candidate_mob) ? $p->candidate_mob : 'N/A' }}</td>
                                    <td>{{ $p->tca_followupstatus }}</td>
                                    <td> <!-- <?php if($p->tca_followupstatus=='Confirm') { echo ''; } else if($p->tca_followupstatus=='Not Going'){ echo ''; } else { ?>
                                        <select id="status" onchange="changestatus({{$p->tca_id}})">
                                            <option>Change Status</option>
                                            <option value="Confirm">Confirm</option>
                                            <option value="Not Going">Not Going</option>
                                            <option value="Not Reachable">Not Reachable</option>
                                            <option value="Not Responding">Not Responding</option>
                                        </select>  <?php  } ?> -->
                                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="tcaid[]" value="{{$p->tca_id}}">
      <input type="hidden" name="asignfrom" value="{{$assignfrom}}">
        <input type="hidden" name="asignto" value="{{$assignto}}">
           <input type="hidden" name="posid" value="{{$posid}}">

     <button type='button'  class='det_css' data-toggle='modal' data-target='#myModal' onclick='getposiionvalue(<?php echo $p->tca_positionid; ?>),getupdatespocknn(<?php echo $p->tca_id; ?>,<?php echo $p->tca_assignto; ?>,"<?php echo $p->tca_followupstatus; ?>",<?php echo $p->tca_positionid; ?>)(this.value),getpositionspock(<?php echo $p->id; ?>,<?php echo $p->recruiter_id; ?>,<?php echo $p->cv_status; ?>)' ><span class='icofont icofont-eye-alt'></span></button>

                                        </div>






                                    </td>

                </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div></form>
</div>
</div>
</div>
</div>
<div class="container">
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Status</h4>
        </div>

           <form method="post" action="{{ URL::to('updaterecallclientstatuspopup') }}">
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="form-group">
                          <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Select Status:

                          </label>
                          <select class="form-control col-xs-6" required id="recruiterspockup" name="recruiter[]"  onchange="changestatuspopupdata(this.value)" >
                             <option value="">Select Status</option>                   
                          </select>
                            <input type="text" name="clientreq_posid" id="placepopupval" >
                          <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           <input type="hidden" name="spoc_id" value="{{$assignfrom}}" id="spoc_id">
                      <input type="hidden" name="recuiter_id"  value="{{$assignto}}" id="recuiter_id">
                       <input type="hidden" name="pos_id" value="{{$posid}}" id="pos_id">
                        </div>
                        <div id="placepopup"></div>
                     
                    </div>
                </div>
            </div>
           
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="button" class="btn btn-default" value="Submit" id="buttonClassstpop">
            </div>
        </form>



        <div id=""></div>
      
      </div>      
    </div>
  </div> 

  </div>
    
<div class="container">
  <div class="modal hide fade" id="myModalcvstattus" role="dialog">
      <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select  Resume Status</h4>
        </div>

           <form method="post" action="{{ URL::to('updateallocatedpopupresume') }}">
            <div class="modal-body">
              <table width="100%" border="1">
               
<tr><td id="recruiterspockupcvstattus"></td></tr>
              </table>
                
                        <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                     <input type="hidden" name="spoc_id" value="{{$assignfrom}}">
                      <input type="hidden" name="recuiter_id"  value="{{$assignto}}">
                       <input type="hidden" name="pos_id" value="{{$posid}}">
                
                </div>
                 <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-default" value="Submit">
            </div>
            </div>
           
        </form>
      
      </div>      
    </div>
</div> 
</div>

<!-- <script type="text/javascript">
    function abc(){
        alert('hi');
    }
</script> -->
<script>
  
function getposiionvalue(id)
{

 var ids = id;
 //alert(ids);

// alert(document.location.host);
    $.ajax({

       url: '{{URL::route('getdriveassignposition')}}',
            type: 'post',
            data:{
                '_token': "{{ csrf_token() }}",
                'id':ids,
                
              },
        success: function(data){
        // alert(data);
        document.getElementById("placepopupval").value = data;

        },
        error: function(data){

        }
    });

}


function getupdatespocknn(eid,recruiter_id,cv_status,posid)
{

 var idds = eid;
 var recruiterid = recruiter_id;
 var cvstatus = cv_status;
 var posid = posid;
 //alert(idds); 
    $.ajax({
        type: 'POST',
                  url: '{{URL::route('getallocatedassignstatusallcv')}}',
                  data:{
                    'idds': idds,
                    'recruiterid': recruiterid,
                       'cvstatus': cvstatus,
                          'posid': posid,
                    '_token': "{{ csrf_token() }}",
                },


     
        success: function(data){
        // alert(data);
            $("#recruiterspockup").empty();
            $("#recruiterspockup").append(data);
          // $('#recruiterspockup').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}

        $("#buttonClassstpop").click(function() {

var sel = document.getElementById('recruiterspockup').value;
var expectedsaleryval = document.getElementById('expectedsalery').value;
var Doj = document.getElementById('Doj').value;
var expDoj = document.getElementById('expDoj').value;
var count = document.getElementById('count').value;
var countval = document.getElementById('countval').value;
var placepopupval = document.getElementById('placepopupval').value;
var recuiter_id = document.getElementById('recuiter_id').value;
var spoc_id = document.getElementById('spoc_id').value;
var pos_id = document.getElementById('pos_id').value;



//return false;
   $.ajax({
                      type: 'POST',
                      url: '{{URL::route('updatemysingleallocatedresume')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'sel':sel,
                        'expectedsalery':expectedsaleryval,
                        'Doj':Doj,
                        'expDoj':expDoj,
                        'count':count,
                          'countval':countval,
                        'placepopupval':placepopupval,
                         'recuiter_id':recuiter_id,
                          'spoc_id':spoc_id,
                        'pos_id':pos_id,
                   
                      },
                      success: function(data){
                      // alert(data);
                       $('#myModal').modal('hide');
                        $("#suggesstion-box").show();
                         $("#suggesstion-box").html(data);
                    }
                    });


 

           });
    

</script>
<script>
    
function changestatuspopupdata(id)
{
document.getElementById("placepopup").innerHTML="<input type='text' name='expectedsalery[]' class='form-control'  placeholder='Enter Description'  id='expectedsalery'><input type='hidden' name='Doj[]'   class='form-control'  placeholder='Date Of Joining' id='Doj' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'    id='expDoj' ><input type='hidden' name='count[]' value="+id+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
 }


function changestatusdata(id)
{
 document.getElementById("place")r="<input type='text' name='expectedsalery' placeholder='Expected Salary' class='form-control'  ><input type='date' name='Doj'   class='form-control'   placeholder='Date Of Joining' ><input type='hidden' name='expDoj'  placeholder='Expected Date Of Joining'    ><input type='hidden' name='count[]' value="+id+"><input type='hidden' name='countval[]' value='1'>"
  
 }


/*
function changestatusdata(ids)
{
  
    document.getElementById("place"+ids).innerHTML="<textarea name='salery[]' placeholder='Enter Details' value=' '></textarea><input type='hidden' name='count[]' value='1'>"
}
*/
    function changefirststatus(ids)
    {
        var status=$('#status').val();
        //alert(ids);
        $.ajax({
            url: '{{URL::route('changecvstatus')}}',
            type: 'post',
            data:{
                '_token': "{{ csrf_token() }}",
                'key':ids,
                'status':status,
              },
            success: function(data)
            {
                if(data==1){
                    location.reload();
                }
            }
        });
    }

</script>

<script type="text/javascript">

    $(document).ready(function() {
        $("#buttonClass").click(function() {
            var favorite = [];
            $.each($("input[name='checkboxid']:checked"), function(){            
                favorite.push($(this).val());
            });
            var valuejoin=favorite.join(",");
         
        //  alert( valuejoin); exit;
          
  $.ajax({

      type: 'POST',
                      url: '{{URL::route('getallocatedresumespopupnew')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'valuejoin':valuejoin,
                      
                       
                     
                      },

        success: function(data){
           $('#myModalcvstattus').modal('show');
     //    alert(data);
            $("#recruiterspockupcvstattus").empty();
            $("#recruiterspockupcvstattus").append(data);
          // $('#recruiterspockup').multiselect('rebuild');
        },
        error: function(data){

        }
    });

        });
    });
</script>
@endsection

