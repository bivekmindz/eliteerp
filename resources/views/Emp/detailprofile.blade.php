
    <!-- Side-Nav-->
@extends('Emp.layouts.master')

@section('content')


        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <h4>Profile Details</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="">Profiles</a>
                                </li>
                                <!-- <li class="breadcrumb-item"><a href="">Menu Details</a>
                                </li> -->
                            </ol>
                </div>
                
                
                
            </div>
           
           
      
           
          <div class="row">
                      <!-- Textual inputs starts -->
                    <div class="col-lg-12">
                        <div class="card">
                        
                        <div class="to-do-list widget-to-do-list" style=" padding-top:15px;">
                    <div class="rkmd-checkbox checkbox-rotate">
                        <label class="input-checkbox checkbox-primary">
                            <input type="checkbox" id="checkbox11">
                            <span class="checkbox"></span>
                        </label>
                        <label>S Sagar</label>
                        </div>
                    </div>
                            
                            
                            <!-- end of modal -->
                            <div class="card-block">
                            
                            <div class="col-md-8">
                            
                              <div class="left-link">
                               
                                 <ul>
                                   
                                    <li><i class="icofont icofont-bag"></i>3yr 8m</li>
                                     <li><i class="icon-wallet"></i> <i class="icofont icofont-cur-rupee"></i>4.60 Lacs</li>
                                      <li><i class="icon-location-pin"></i>Chennai</li>
                                   
                                 </ul>
                                 
                                 <div class="pro-dcre">
                                    
                                    <ul>
                                      <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                       <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                        <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                         <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                          <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                           <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                    </ul>
                                    
                                 </div>
                                 
                                 <h5>Similar Resumes</h5>
                              
                              
                              </div>
                            
                            </div>
                            
                             <div class="col-md-4">
                              <div class="right-link">
                              
                                <div class="top-oiu">
                                
                                <img src="assets/images/avatar-4.png" alt="Logo">
                                
                                </div>
                                
                                 <div class="dat-down">
                                  <p><strong>MS SQL SERVER</strong> Database
Administrator with B.Tech/B.E.in computer currently living in Chennai</p>

<div class="form-group row">
                                            
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-btn" id="btn-addon1"><button type="submit" class="btn btn-success shadow-none addon-btn waves-effect waves-light"><i class="icofont icofont-ui-call"></i></button></span>
                                                    <input type="text" id="btnaddon1" class="form-control" placeholder="Show Phone Number" aria-describedby="btn-addon1">
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                        
                                        <ul>
                                              <li><div class="to-do-list widget-to-do-list">
                    <div class="rkmd-checkbox checkbox-rotate">
                        <label class="input-checkbox checkbox-primary">
                            <input type="checkbox" id="checkbox11">
                            <span class="checkbox"></span>
                        </label>
                        <label style="font-size:12px;">Verified Phone</label>
                        </div>
                    </div></li>
                    
                    <li><div class="to-do-list widget-to-do-list">
                    <div class="rkmd-checkbox checkbox-rotate">
                        <label class="input-checkbox checkbox-primary">
                            <input type="checkbox" id="checkbox11">
                            <span class="checkbox"></span>
                        </label>
                        <label style="font-size:12px;">Verified Email</label>
                        </div>
                    </div></li>
                                            </ul>
                                 </div>
                              
                              
                              </div>
                                
                            
                            </div>
                            
                            
                                 
                            </div>
                        </div>
                    </div>
                    <!-- Textual inputs ends -->
             </div>
             
             
             <div class="row">
                      <!-- Textual inputs starts -->
                    <div class="col-lg-12">
                        <div class="card">
                        
                        <div class="to-do-list widget-to-do-list" style=" padding-top:15px;">
                    <div class="rkmd-checkbox checkbox-rotate">
                        <label class="input-checkbox checkbox-primary">
                            <input type="checkbox" id="checkbox11">
                            <span class="checkbox"></span>
                        </label>
                        <label>S Sagar</label>
                        </div>
                    </div>
                            
                            
                            <!-- end of modal -->
                            <div class="card-block">
                            
                            <div class="col-md-8">
                            
                              <div class="left-link">
                               
                                 <ul>
                                   
                                    <li><i class="icofont icofont-bag"></i>3yr 8m</li>
                                     <li><i class="icon-wallet"></i> <i class="icofont icofont-cur-rupee"></i>4.60 Lacs</li>
                                      <li><i class="icon-location-pin"></i>Chennai</li>
                                   
                                 </ul>
                                 
                                 <div class="pro-dcre">
                                    
                                    <ul>
                                      <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                       <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                        <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                         <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                          <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                           <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                    </ul>
                                    
                                 </div>
                                 
                                 <h5>Similar Resumes</h5>
                              
                              
                              </div>
                            
                            </div>
                            
                             <div class="col-md-4">
                              <div class="right-link">
                              
                                <div class="top-oiu">
                                
                                <img src="assets/images/avatar-4.png" alt="Logo">
                                
                                </div>
                                
                                 <div class="dat-down">
                                  <p><strong>MS SQL SERVER</strong> Database
Administrator with B.Tech/B.E.in computer currently living in Chennai</p>

<div class="form-group row">
                                            
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-btn" id="btn-addon1"><button type="submit" class="btn btn-success shadow-none addon-btn waves-effect waves-light"><i class="icofont icofont-ui-call"></i></button></span>
                                                    <input type="text" id="btnaddon1" class="form-control" placeholder="Show Phone Number" aria-describedby="btn-addon1">
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                        
                                        <ul>
                                              <li><div class="to-do-list widget-to-do-list">
                    <div class="rkmd-checkbox checkbox-rotate">
                        <label class="input-checkbox checkbox-primary">
                            <input type="checkbox" id="checkbox11">
                            <span class="checkbox"></span>
                        </label>
                        <label style="font-size:12px;">Verified Phone</label>
                        </div>
                    </div></li>
                    
                    <li><div class="to-do-list widget-to-do-list">
                    <div class="rkmd-checkbox checkbox-rotate">
                        <label class="input-checkbox checkbox-primary">
                            <input type="checkbox" id="checkbox11">
                            <span class="checkbox"></span>
                        </label>
                        <label style="font-size:12px;">Verified Email</label>
                        </div>
                    </div></li>
                                            </ul>
                                 </div>
                              
                              
                              </div>
                                
                            
                            </div>
                            
                            
                                 
                            </div>
                        </div>
                    </div>
                    <!-- Textual inputs ends -->
             </div>

         
            
          
        </div>
      
</div>


<script src="assets/js/jquery-3.1.1.min.js"></script>
<script src="assets/js/jquery-ui.min.js"></script>
<script src="assets/js/tether.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/waves.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-clockpicker.min.js"></script>

<script type="text/javascript" src="assets/js/main.js"></script>
<script src="assets/js/menu.js"></script>

<script src="assets/js/sel.js"></script>

<script type="text/javascript">
$(function() {
    $('.multiselect-ui').multiselect({
        includeSelectAllOption: true
    });
});
</script>

<script>
$(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls form:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="icofont icofont-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
		$(this).parents('.entry:first').remove();

		e.preventDefault();
		return false;
	});
});

</script>


<script type="text/javascript">
$('.clockpicker').clockpicker()
	.find('input').change(function(){
		console.log(this.value);
	});
var input = $('#single-input').clockpicker({
	placement: 'bottom',
	align: 'left',
	autoclose: true,
	'default': 'now'
});

$('.clockpicker-with-callbacks').clockpicker({
		donetext: 'Done',
		init: function() { 
			console.log("colorpicker initiated");
		},
		beforeShow: function() {
			console.log("before show");
		},
		afterShow: function() {
			console.log("after show");
		},
		beforeHide: function() {
			console.log("before hide");
		},
		afterHide: function() {
			console.log("after hide");
		},
		beforeHourSelect: function() {
			console.log("before hour selected");
		},
		afterHourSelect: function() {
			console.log("after hour selected");
		},
		beforeDone: function() {
			console.log("before done");
		},
		afterDone: function() {
			console.log("after done");
		}
	})
	.find('input').change(function(){
		console.log(this.value);
	});

// Manually toggle to the minutes view
$('#check-minutes').click(function(e){
	// Have to stop propagation here
	e.stopPropagation();
	input.clockpicker('show')
			.clockpicker('toggleView', 'minutes');
});
if (/mobile/i.test(navigator.userAgent)) {
	$('input').prop('readOnly', true);
}
</script>



@endsection