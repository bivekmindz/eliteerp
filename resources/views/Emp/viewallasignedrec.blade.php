@extends('Emp.layouts.master')

@section('content')


<script>

$(document).ready(function(){
    $("#filter_area").keyup(function(){
        var xxx = $(this).val();
   //    alert(xxx);
          $.ajax({
                      type: 'POST',
                      url: '{{URL::route('ajaxviewallassignedrecuiterposition')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'key':xxx,
                      },
                      
                      success: function(data){
                      //alert(data);
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                         
                      }
                    });

    });
});
</script>





<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View All Position Allocation To Recruiter</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Positions</a>
                </li>
                <li class="breadcrumb-item"><a href="">Position assigned to Recuriter</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">View All Position Assigned To Recruiter</h5>
                    <form action="{{ URL::to('excellistassignrecruiterdownload') }}"  method="post" enctype="multipart/form-data">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="submit" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel" value="Download Excel" name="downexcel">
                      </input>
                    </form>
                </div>
                @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
                @endif
                
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 ">
                        
                        <div class="view-allas">
                         <div class="vaor">

                            <table id="search_filter">
                                <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Position Name</th>
                                    <th>Company Name</th>
                                    <th>Drive</th>
                                    <th>Drive Date</th>
                                    <th>Lineup From</th>
                                    <th>Lineup To</th>
                                    <th>No Of Positions</th>
                                    <th>Assigned Recuriter Name</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="suggesstion-box">
                                @php
                                $i=1;
                                @endphp
                                @foreach($pos as $key => $p)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ !empty($p->clientjob_title) ? $p->clientjob_title : 'N/A' }}</td>
                                    <td>{{ !empty($p->comp_name) ? $p->comp_name : 'N/A' }}</td>
                                    <td>{{ !empty($p->drive_shortlist) ? $p->drive_shortlist :'N/A' }}</td>
                                    <td>{{ !empty($p->drive_time) ? $p->drive_time :'N/A' }}</td>
                                    <td>{{ !empty($p->lineup_from) ? $p->lineup_from :'N/A' }}</td>
                                    <td>{{ !empty($p->lineup_to) ? $p->lineup_to :'N/A' }}</td>
                                    <td>{{ !empty($p->clientjob_noofposition) ? $p->clientjob_noofposition : 'N/A' }}</td>
                                    <td>{{ !empty($p->name) ? $p->name : 'N/A' }}</td>
                                    <td>     
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal" onclick="getrecruiterlist(<?php echo $p->fk_jdid;?>);getclientreq_id(<?php echo $p->fk_jdid;?>);">Assign</button> | 
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModalUnassigned" onclick="getrecruiterunassignedlist(<?php echo $p->fk_jdid;?>);">UnAssign</button>
                                    </td>

                                </tr>
                @endforeach
                </tbody>
                </table>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
</script>

<div class="container">
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Assigned Recruiter</h4>
        </div>
        <form method="post" action="">
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="form-group">
                        <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Select Recruiter:</label>
                          <select class="multiselect-ui form-control col-xs-6 recruiter" multiple="multiple" required id="recruiter" name="recruiter[]">
                             <option value="">Select Recruiters</option>                   
                          </select>
                          <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="button" class="btn btn-default" id="recsubmit" value="Submit">
            </div>
        </form>
      </div>      
    </div>
  </div> 
</div>

<script>
   $(document).ready(function() {
        $("#recsubmit").click(function() {

var clientreq_id = $('#clientreq_id').val() // document.getElementById('clientreq_id').value;
var recruiter = $('.recruiter').val();

var test = [];
$.each(recruiter, function(index, value){
  test.push(value);
})

// alert(clientreq_id);
// console.log(test);
// alert(test);

var dataToSend = {
  _token: "{{ csrf_token() }}",
  clientreq_id:clientreq_id,
  recruiter:test,
}


$("#recsubmit").hide();
   $.ajax({
            type: 'POST',
            url: '{{URL::route('updateassrecruiters')}}',
            data: dataToSend,
            datatype :'JSON',
            success: function(data){
                    //   alert(data);
               var response = JSON.parse(data);
            //   alert(response);
               if(response.length > 0){

                var html = "";
               $.each(response, function(index, value){
                  for(let i=0; i<value.length-1;i++){
                    let val = value[i];
                    // console.log(value);
                    html += '<p><input type="hidden" class="recid" id="recid" name="recid" value="'+val.fk_empid+'">'+val.uname+'<br><input type="checkbox" class="pid" id="pid" name="pid" value="'+val.fk_jdid+'_'+val.fk_empid+'"   style="margin-right:8px;">'+val.clientjob_title+'</p>';
                  }
               });
               html += '<input type="hidden" id="newpos" value="'+response[0][2]+'">';
               // alert(res);

               $("#udta").html(html);
              $('#myModal').modal('hide');
              $('#myModals').modal('show');

               }
               else{
                  $('#myModal').modal('hide');
                  document.location.reload() ;

              // $("#myModals").html(data);
              }

            }
          });

        });
    });

</script>

<div class="container" >
   <div class="modal fade" id="myModalUnassigned" role="dialog" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Assigned Recruiter</h4>
        </div>
        <form method="post" action="{{ URL::to('delassrecruiters') }}">
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="form-group">
                          <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Select Recruiter:</label>
                          <select class="multiselect-ui form-control col-xs-6" multiple="multiple" required id="unassignedrecruiter" name="recruiter[]">
                             <option value="">Select Recruiters</option>                   
                          </select>
                          <input type="hidden" name="fk_jdid" id="fk_jdid" value="">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-default" value="Submit">
            </div>
        </form>
      </div>      
    </div>
  </div> 
</div>

<div class="container">
   <div class="modal fade" id="myModals" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Already occupied with 2 position</h4>
        </div>
        <form method="post" action="">
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div id="udta" class="form-group">
                           
                          
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
              <input type="button" class="btn btn-default" id="update" value="Submit">
            </div>
        </form>
      </div>      
    </div>
  </div> 
</div>

<script>
   
        $("#update").click(function() {
            var selected = [];
            var positionid = [];
            var recid = [];
            $('#pid:checked').each(function(){
              selected.push($(this).val());
            });
            console.log(selected);
            var newpos = $('#newpos').val();
            var arr = selected.join();
            console.log(arr);
            var res = arr.split(',');
            var arry=[];
            for(var i=0;i<res.length;i++){
              var posid = res[i].split(',');
               console.log(posid);
              var arrr = posid.join();
              for(var j=0;j<posid.length;j++){
                var pid = posid[j].split('_');
        
            console.log(pid);
             positionid.push(pid[0]);
            
             recid.push(pid[1]);
            
              }

            }
            // alert(newpos);
            //  alert(positionid);
            //  alert(recid);
            var dataToSend = {
             _token: "{{ csrf_token() }}",
             pid:positionid,
             newpos:newpos,
             recid:recid,
            }
            $('#update').hide();
            $.ajax({
            type: 'POST',
            url: '{{URL::route('delposition')}}',
            data: dataToSend,
            datatype :'JSON',
            success: function(data){
                    //  alert(data);
               $('#myModals').modal('hide');
               document.location.reload() ;
                  }
               });
               
          });


        

</script>





<script type="text/javascript">
function position(id) {

  var pid1 = $(id).val();
 console.log(pid1);

  $(id).next("input[name=clientreq_id]").val(pid1);
}

</script>


<script>
function getrecruiterlist(fk_jdid)
{
    var position_id = fk_jdid;
    $.ajax({
        type:"GET",
        url:"getrecruiters/" + position_id,
        data:'',
        success: function(data){
            $("#recruiter").empty();
            $("#recruiter").append(data);
            $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}

function getrecruiterunassignedlist(fk_jdid)
{
    var position_id = fk_jdid;
    $("#fk_jdid").val(fk_jdid);
    $.ajax({
        type:"GET",
        url:"getunassignedrecruiters/" + position_id,
        data:'',
        success: function(data){
            $("#unassignedrecruiter").empty();
            $("#unassignedrecruiter").append(data);
            $('#unassignedrecruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}

function getclientreq_id(clientreq_id)
{
    $("#clientreq_id").val(clientreq_id);
}
</script>


<!-- <script type="text/javascript">
        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
</script> -->
@endsection
