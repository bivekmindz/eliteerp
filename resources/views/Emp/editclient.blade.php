@extends('Emp.layouts.master')

@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">
    <style>
      .error{ 
        color:red;
      }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Edit Client</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Clients</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Edit New Client</a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Edit Client</h5>
                    </div>
                    @if(Session::has('success_msg'))
                        <div class="alert alert-success">
                            {{ Session::get('success_msg') }}
                        </div>
                    @endif
                    <div class="card-block">
                                <form method="post" name="client" action="{{ URL::to('editclient/'.Crypt::encrypt($client_details[0]->client_id)) }}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Company Name<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="comp_name" name="comp_name" value="{{ (Input::old('comp_name')) ? Input::old('comp_name') : $client_details[0]->comp_name }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('comp_name') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Company URL<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                               <input class="form-control" type="text" id="company_url" name="company_url" value="{{ (Input::old('company_url')) ? Input::old('company_url') : $client_details[0]->company_url }}" id="example-text-input">
                                               <span class="text-danger">{{ $errors->first('company_url') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Contact Person Name</label>
                                            <div class="col-sm-8">
                                               
                                           <input class="form-control" type="text" id="contact_name" name="contact_name" value="{{ (Input::old('contact_name')) ? Input::old('contact_name') : $client_details[0]->contact_name }}" id="example-text-input">
                                           <span class="text-danger">{{ $errors->first('contact_name') }}</span>
                                        
                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Designation<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="designation" name="designation" value="{{ (Input::old('designation')) ? Input::old('designation') : $client_details[0]->designation }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('designation') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Phone<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                              <input class="form-control" type="text" id="phone" maxlength="20" name="phone" value="{{ (Input::old('phone')) ? Input::old('phone') : $client_details[0]->phone }}" id="example-text-input">
                                              <span class="text-danger">{{ $errors->first('phone') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Email<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="email" name="email" value="{{ (Input::old('email')) ? Input::old('email') : $client_details[0]->email }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Contact Person Name1</label>
                                            <div class="col-sm-8">
                                               
                                           <input class="form-control" type="text" id="contact_name" name="contact_name1" value="{{ (Input::old('contact_name1')) ? Input::old('contact_name1') : $client_details[0]->contact_name1 }}" id="example-text-input">
                                           <span class="text-danger">{{ $errors->first('contact_name1') }}</span>
                                        
                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Designation1<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="designation" name="designation1" value="{{ (Input::old('designation1')) ? Input::old('designation1') : $client_details[0]->designation1 }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('designation1') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Phone1<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                              <input class="form-control" type="text" id="phone" maxlength="20" name="phone1" value="{{ (Input::old('phone1')) ? Input::old('phone1') : $client_details[0]->phone1 }}" id="example-text-input">
                                              <span class="text-danger">{{ $errors->first('phone1') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Email1<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="email1" name="email1" value="{{ (Input::old('email1')) ? Input::old('email1') : $client_details[0]->email1 }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('email1') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Contact Person Name2</label>
                                            <div class="col-sm-8">
                                               
                                           <input class="form-control" type="text" id="contact_name2" name="contact_name2" value="{{ (Input::old('contact_name2')) ? Input::old('contact_name2') : $client_details[0]->contact_name2 }}" id="example-text-input">
                                           <span class="text-danger">{{ $errors->first('contact_name2') }}</span>
                                        
                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Designation2<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="designation2" name="designation2" value="{{ (Input::old('designation2')) ? Input::old('designation2') : $client_details[0]->designation2 }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('designation2') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Phone2<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                              <input class="form-control" type="text" id="phone2" maxlength="20" name="phone2" value="{{ (Input::old('phone2')) ? Input::old('phone2') : $client_details[0]->phone2 }}" id="example-text-input">
                                              <span class="text-danger">{{ $errors->first('phone2') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Email2<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="email2" name="email2" value="{{ (Input::old('email2')) ? Input::old('email2') : $client_details[0]->email2 }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('emai2l') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Company Size<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                               
                                           <input class="form-control" type="text" id="company_size" name="company_size" value="{{ (Input::old('company_size')) ? Input::old('company_size') : $client_details[0]->company_size }}" id="example-text-input">
                                            <span class="text-danger">{{ $errors->first('company_size') }}</span>
                                    
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                         <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">From Timings<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="from_timings" name="from_timings" value="{{ (Input::old('from_time')) ? Input::old('from_time') : $client_details[0]->from_time }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('from_timings') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">To Timings<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="to_timings" name="to_timings" value="{{ (Input::old('to_timings')) ? Input::old('to_timings') : $client_details[0]->to_time }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('to_timings') }}</span>
                                            </div>
                                        </div>
                                    </div>

                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Days Working<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                               
                                           <input class="form-control" type="text" id="days_working" name="days_working" value="{{ (Input::old('days_working')) ? Input::old('days_working') : $client_details[0]->days_working }}" id="example-text-input">
                                            <span class="text-danger">{{ $errors->first('days_working') }}</span>
                                
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Industry<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="industry" name="industry" value="{{ (Input::old('industry')) ? Input::old('industry') : $client_details[0]->industry }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('industry') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                       <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Headquarters<span class="error"> * </span></label>
                                                <div class="col-sm-8"> 
                                                   <input class="form-control" type="text" id="headquarters" name="headquarters" value="{{ (Input::old('headquarters')) ? Input::old('headquarters') : $client_details[0]->headquarters }}" id="example-text-input">
                                                   <span class="text-danger">{{ $errors->first('headquarters') }}</span>
                                                </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Company’s Specialties<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="company_specialties" name="company_specialties" value="{{ (Input::old('company_specialties')) ? Input::old('company_specialties') : $client_details[0]->company_specialties }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('company_specialties') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Office Address<span class="error"> * </span></label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="office_address" name="office_address" value="{{ (Input::old('office_address')) ? Input::old('office_address') : $client_details[0]->office_address }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('office_address') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="col-lg-6">
                                       <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Alternate Phone </label>
                                            <div class="col-sm-8">
                                                <input class="form-control" type="text" id="alternate_phone" maxlength="20" name="alternate_phone" value="{{ (Input::old('alternate_phone')) ? Input::old('alternate_phone') : $client_details[0]->alternate_phone }}" id="example-text-input">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Selling Point<span class="error"> * </span></label>
                                        <div class="col-sm-10">
                                           <textarea name="selling_point" type="text" id="selling_point"  class="form-control" rows="50" cols="30">{{ (Input::old('selling_point')) ? Input::old('selling_point') : $client_details[0]->selling_point }}</textarea>
                                           <span class="text-danger">{{ $errors->first('selling_point') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Summary About The Client<span class="error"> * </span></label>
                                        <div class="col-sm-10">
                                           <textarea name="summary" type="text" id="summary" required class="form-control" rows="50" cols="30">{{ (Input::old('summary')) ? Input::old('summary') : $client_details[0]->summary }}</textarea>
                                           <span class="text-danger">{{ $errors->first('summary') }}</span>
                                        </div>
                                    </div>
                                </div>

                                 <div class="row">
                                 <div class="col-lg-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Choose Logo<span class="error"> * </span></label>
                                        <div class="col-md-3">
                                   <input data-preview="#preview" name="input_img" type="file" id="imageInput">
                                <img class="col-sm-6" id="preview"  src="{{URL::to('images/'.$client_details[0]->logo) }}" ></img>
                                </div>
                                    </div>
                                    </div>
                                    <div class="col-lg-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Contractual SLA<span class="error"> * </span></label>
                                        <div class="col-sm-8">
                                                <input class="form-control" type="text" id="contractual_sla" maxlength="20" name="contractual_sla" value="{{ (Input::old('contractual_sla')) ? Input::old('contractual_sla') : $client_details[0]->contractual_sla }}" id="example-text-input">
                                                <span class="text-danger">{{ $errors->first('contractual_sla') }}</span>
                                            </div>
                                    </div>
                                    </div>
                                </div>
<div class="col-sm-12 table-responsive timeset" id="viewpage2">

                                <div class="controls">

                                    <table id="cvupload_table" class="table dt">
                                        <thead>
                                            <tr>
                                                <th>Location</th>
                                                <th>Billing Address</th>
                                                <th>GST</th>
                                               <th>Pancard</th>
                                               <th>State</th>
                                            </tr>
                                        </thead>
                                        <tbody id="upload-box" >

<?php foreach($clientadddetails as $cladd) {?>


 <tr class="table-active entry" id="combotr" >

                                                <td>
                                                    <div class="form-group required">
                                                        <input type="text"  required="" name="locationup[]" class="form-control" placeholder="Enter location" title="location" value="<?php echo $cladd->clientsdetails_location; ?>" >
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group required">

                                                        <textarea name="billingaddressup[]" required="" id="billingaddup<?php echo $cladd->clientsdetails_id; ?>" placeholder="Enter Your Billing Address" class="summernote form-control"><?php echo $cladd->clientsdetails_billingaddress; ?></textarea>
                                                       
                                                    </div>
                   <input type="hidden" name="clid[]" value="<?php echo $cladd->clientsdetails_id; ?>">                           
<!-- <script type="text/javascript">
    
      $('#billingaddup<?php echo $cladd->clientsdetails_id; ?>').summernote({ height: 40 });
</script>  --> </td>
<td>
                                                    <div class="form-group required">
                                                        <input type="text"  name="gstup[]" class="form-control" placeholder="Enter GST" title="gstup" value="<?php echo $cladd->clientsdetails_gst; ?>" >
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group required">
                                                        <input type="text"   name="pannoup[]" class="form-control" placeholder="Enter panno" title="location" value="<?php echo $cladd->clientsdetails_panno; ?>" >
                                                    </div>
                                                </td>



                                                <td>
                                                    <div class="form-group required">
                                                           <select name="cityidup[]"  class="form-control" >
                                
                                            <?php foreach ($statedata as $key => $cou){ ?>
                                          
                                     <option value="<?php echo($cou->state_id); ?>" <?php if($cladd->clientsdetails_cityid==$cou->state_id) { ?> selected <?php } ?>><?php echo($cou->state_name); ?></option>
<?php } ?>
                                            </select>

                                                    </div>
                                                </td>
                                                

                                            <td>
                                       <a href="deleteclient/<?php echo $cladd->clientsdetails_id; ?>/{{$clientidntd}}">         
                                               delete </a>
                                            </td>
                                        </tr>

<?php } ?>



                                            <tr class="table-active entry" id="combotr" >

                                                <td>
                                                    <div class="form-group required">
                                                        <input type="text"   name="location[]" class="form-control" placeholder="Enter location" title="location" >
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group required">
                                                        <textarea name="billingaddress[]"  placeholder="Enter Your Billing Address" class="summernote form-control" id="billingadd"></textarea>
                                                       
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="form-group required">
                                                        <input type="text" name="gst[]" class="form-control" placeholder="Enter Your GST Number" >
                                                    </div>
                                                </td>
                                                 <td>
                                                    <div class="form-group required">
                                                        <input type="text" name="panno[]" class="form-control" placeholder="Enter Your Pan Number" >
                                                    </div>
                                                </td>
<td>
                                                    <div class="form-group required">
                                                    
                                             <select name="cityid[]"  class="form-control" >
                                
                                            <?php foreach ($statedata as $key => $cou){ ?>
                                          
                                     <option value="<?php echo($cou->state_id); ?>"><?php echo($cou->state_name); ?></option>
<?php } ?>
                                            </select>

                                                       
                                                    </div>
                                                </td> 
                                            <td><span class="add_field">
                                                <button class="btn btn-success btn-add" type="button">
                                                    <span class="icofont icofont-plus"></span>
                                                </button>
                                            </span></td>
                                        </tr>
<!--                                         <div id="upload-box"></div>
 -->                                    </tbody>
                                </table>
                               
                            </div>
                   <!--      <script type="text/javascript">
    
      $('#billingadd').summernote({ height: 40 });
</script>  -->
                        </div>
                                <div class="md-input-wrapper">
                                    <a href="{{ URL::to('listclient') }}" class="btn btn-default waves-effect">Cancel
                                    </a>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                                    </button>
                                </div> 
                                </form>
                            </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>

<script type="text/javascript">
    $('#selling_point').summernote({ height: 400 });
</script>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script>
    $(function() {
        $("form[name='client']").validate({
            ignore: [],
            rules: {
              comp_name: "required",
             
              email: "required",
              contact_name: "required",
              designation: "required",

              phone: "required",
            
            },
            messages: {
              comp_name: "Please enter company name.",
            
              email: "Please enter company email.",
              contact_name: "Please enter contact name.",
              designation: "Please enter designation.",

              phone: "Please enter phone number.",
          
            },
            submitHandler: function(form) {
              form.submit();
            }
        });
    });
</script>
@endsection

