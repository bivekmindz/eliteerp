@extends('Emp.layouts.master1')

@section('content')

{!! Form::open([ 'action'=>'Emp\CVController@edituploadcv', 'method'=>'post', 'files'=>true   ]) !!}

    <div class="col-sm-12 table-responsive timeset">


        <div class="controls">

            <table class="table dt">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Phone No</th>
                    <th>Email</th>
                    <th>Upload Resume</th>
                    <th>Highest Qualificatione</th>
                    <th>Total Exp</th>
                    <th>Domain Exp</th>
                    <th>Primary Skill</th>
                    <th>Secondary Skill</th>
                    <th>Current ORG</th>
                    <th>Current CTC</th>
                    <th>Expected CTC</th>
                    <th>Reason For Change</th>
                    <th>Communication Skills Rating</th>
                    <th>NP</th>
                     <th>Designation</th>
                    <th>Location</th>
                    <th>Relocation</th>
                    <th>Expected DOJ</th>
                    <th>Remarks</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr class="table-active entry">

                    <td>
                        <div class="form-group required">


                            <input type="text" pattern="[A-Za-z\s]+" required="" name="candidate_name[]" class="form-control" placeholder="Enter First Name" title="First Name should only contain  letters. e.g. John" maxlength="60" value="{{ (Input::old('candidate_name')) ? Input::old('candidate_name') : $candidate_details[0]->candidate_name }}">
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="tel" name="candidate_mob[]" pattern="^\d{10}$" required=""  class="form-control" value="{{ (Input::old('candidate_mob')) ? Input::old('candidate_mob') : $candidate_details[0]->candidate_mob }}">
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="text" required="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50" name="candidate_email[]" class="form-control" value="{{ (Input::old('candidate_email')) ? Input::old('candidate_email') : $candidate_details[0]->candidate_email }}" >
                        </div>
                    </td>
                    <td>
                        <label class="custom-file ih">
                            <input onchange="gettimeval(this)" type="file" id="file" class="ihi" name="cv_name[]" value=""  width="150px">
                            <span >{{ (Input::old('cv_name')) ? Input::old('cv_name') : $candidate_details[0]->cv_name }}</span>

                            <input type="hidden"  class="filetime"  name="filetime[]" value="{{ (Input::old('cv_uploadtime')) ? Input::old('cv_uploadtime') : $candidate_details[0]->cv_uploadtime }}">
                        </label>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="text" name="highest_qualificatione[]" required="" class="form-control" value="{{ (Input::old('highest_qulification')) ? Input::old('highest_qulification') : $candidate_details[0]->highest_qulification }}">
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="tel" name="experience[]" required="" class="form-control"  maxlength="10" value="{{ (Input::old('total_exp')) ? Input::old('total_exp') : $candidate_details[0]->total_exp }}">
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="text" name="domaine_exp[]" required="" class="form-control" value="{{ (Input::old('domain_exp')) ? Input::old('domain_exp') : $candidate_details[0]->domain_exp }}" maxlength="50">
                        </div>
                    </td>
                    <td>
                        <label class="custom-file ih">
                   
                             <input type="text"  name="primary_skill[]" required="" class="form-control" value="{{ (Input::old('primary_skill')) ? Input::old('primary_skill') : $candidate_details[0]->primary_skill }}" maxlength="50">
                        </label>
                    </td>
                    <td>
                        <label class="custom-file ih">
                             <input type="text" name="secondary_skill[]" required="" class="form-control" value="{{ (Input::old('secondary_skill')) ? Input::old('secondary_skill') : $candidate_details[0]->secondary_skill }}" maxlength="50">
                            
                        </label>
                    </td>

                    <td>
                        <div class="form-group required">
                            <input type="text"required=""  name="current_org[]" class="form-control" value="{{ (Input::old('current_org')) ? Input::old('current_org') : $candidate_details[0]->current_org }}" maxlength="60">
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="text" required="" name="current_ctc[]" class="form-control" value="{{ (Input::old('current_ctc')) ? Input::old('current_ctc') : $candidate_details[0]->current_ctc }}">
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="text" required="" name="expected_ctc[]" class="form-control" value="{{ (Input::old('expected_ctc')) ? Input::old('expected_ctc') : $candidate_details[0]->expected_ctc }}">
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="text" required="" name="reason_for_change[]" class="form-control" value="{{ (Input::old('reason_for_change')) ? Input::old('reason_for_change') : $candidate_details[0]->reason_for_change }}">
                        </div>
                    </td>
                     <td>
                        <div class="form-group required">
                            <select name="communication_skills_rating[]" class="form-control" >
                                <option value="1" @if($candidate_details[0]->communication_skills_rating == '1') Selected @endif> 1 </option>
                                <option value="2" @if($candidate_details[0]->communication_skills_rating == '2') Selected @endif> 2 </option>
                                <option value="3" @if($candidate_details[0]->communication_skills_rating == '3') Selected @endif> 3 </option>
                                <option value="4" @if($candidate_details[0]->communication_skills_rating == '4') Selected @endif> 4 </option>
                                <option value="5" @if($candidate_details[0]->communication_skills_rating == '5') Selected @endif> 5 </option>
                            </select>
                        </div>
                    </td>
                    <td>
                        <label class="custom-file ih">
                        <input type="text" name="np[]" required="" class="form-control" value="{{ (Input::old('np')) ? Input::old('np') : $candidate_details[0]->np }}">
                        </label>
                    </td>
                    
                    
                    <td>
                        <label class="custom-file ih">
                        <input type="text" name="desng[]" required="" class="form-control" value="{{ (Input::old('desng')) ? Input::old('desng') : $candidate_details[0]->desng }}">
                        </label>
                    </td>
                    <td>
                        <label class="custom-file ih">
                        <input type="text" name="location[]" required="" class="form-control" value="{{ (Input::old('location')) ? Input::old('location') : $candidate_details[0]->location }}">
                        </label>
                    </td>
                    <td>
                        <div class="form-group required">
                            <select name="relocation[]" class="form-control" >
                                
                                <option value="Yes" @if($candidate_details[0]->relocation == 'Yes') Selected @endif> Yes</option>
                                <option value="No" @if($candidate_details[0]->relocation == 'No') Selected @endif> No </option>
                                <option value="Other">Other  </option>
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="date" name="doj[]" id="" class="form-control" value="{{ (Input::old('doj')) ? Input::old('doj') : $candidate_details[0]->doj }}">
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <textarea name="remark[]" class="form-control" >
                                {{ (Input::old('remark')) ? Input::old('remark') : $candidate_details[0]->remark }}
                            </textarea>
                        </div>
                    </td>
        
                    <td><span class="add_field">
                    <button class="btn btn-success btn-add" type="button">
                    <span class="icofont icofont-plus"></span>
                    </button>
                    </span></td>
                </tr>
                </tbody>
            </table>
            <div class="md-input-wrapper dt">
                <button type="submit" class="btn btn-default waves-effect">Cancel
                </button>
             
              <input type="hidden" name="EncryptId" value="{{ $fid  }}">
            <input type="hidden" name="id" value="{{ !empty($candidate_details[0]->id) ? $candidate_details[0]->id : '0' }}">

            <input type="hidden" name="posid" value="{{ !empty($candidate_details[0]->position_id) ? $candidate_details[0]->position_id : '0' }}">
        
            <input type="hidden" name="rmapid" value="{{ !empty($candidate_details[0]->r_map_id) ? $candidate_details[0]->r_map_id : '0' }}">

                <input type="submit" class="btn btn-primary waves-effect waves-light" value="submit">

            </div>
        </div>
    </div>

     {!! Form::close() !!}


</div>




@endsection

<style>
.form-control{width:150px!important}
textarea{width:200px!important; height:75px!important}
</style>