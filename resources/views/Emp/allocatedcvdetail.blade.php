@extends('Emp.layouts.master1')

@section('content')

<style type="text/css">
  #search_filter_filter{float:left!important;}
</style>


<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View CV List</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Client Call CV List</a>
                </li>
                <li class="breadcrumb-item"><a href="">Allocated CVs List Details</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
                @endif
                @php 
if(isset($_GET['changestatus'])&& $_GET['changestatus']!='')
{
$_GET['changestatus']=$_GET['changestatus'];
}
else
{
$_GET['changestatus']='';
}

                  @endphp
                <div class="card-block">
                    <div class="row">
                          <div id="search_filter_filter" class="dataTables_filter"><label>
<select name="changestatus"  onchange="changestatus()" id='select_id'>
       <option value='-1' >Search Status</option>
        <option value="Pending"<?php if($_GET['changestatus']=='Pending') {?> selected <?php  } ?>>Pending</option>
         <option value="Not Going" <?php if($_GET['changestatus']=='Not Going') {?> selected <?php  } ?>>Not Going</option>
        <option value="Not Reachable" <?php if($_GET['changestatus']=='Not Reachable') {?> selected <?php  } ?>>Not Reachable</option>
       <option value="Not Responding" <?php if($_GET['changestatus']=='Not Responding') {?> selected <?php  } ?>>Not Responding</option>
         <option value="Confirm"<?php if($_GET['changestatus']=='Confirm') {?> selected <?php  } ?>>Confirm</option>
</select>
<input type="hidden" name="assignfrom" value="{{$assignfrom}}" id='assignfrom'>
<input type="hidden" name="assignto" value="{{$assignto}}" id='assignto'>
<input type="hidden" name="posid" value="{{$posid}}" id='posid'>
                                                   </label></div>
                        <div class="col-sm-12 table-responsive">
                      
                           

<div id="suggesstion-box"> 
 <?php 
                            $Pending = 0;
                             $NotGoing = 0;
                              $NotReachable = 0;
                               $NotResponding = 0;
                                $Confirm = 0;
                              if(!empty($cvdetails)){
                                
                                foreach($cvdetails as $value)
                                {
                                  $Pending = ( $value->Pending);
                                $NotGoing = ( $value->NotGoing);
                                $NotReachable = ( $value->NotReachable);
                                $NotResponding = ( $value->NotResponding);
                                $Confirm = ( $value->Confirm);
                                }
                              
                              }
                            ?>

                             <div class="col-lg-2">
                                 <div class="form-group row">
                                         <div class="col-sm-12">
                                         <label for="example-text-input" class="col-form-label form-control-label">Total Pending </label>
                                     </div>
                                        <div class="col-sm-12">
                                             <input class="form-control" type="text" value="<?php echo $Pending; ?>" id="" name="" class="document" readonly/>
                                    </div>
                                    </div>
                                    
                                  
                                 
                                    </div>
                                     <div class="col-lg-2">
                                 <div class="form-group row">
                                         <div class="col-sm-12">
                                         <label for="example-text-input" class="col-form-label form-control-label">Total NotGoing </label>
                                     </div>
                                        <div class="col-sm-12">
                                             <input class="form-control" type="text" value="<?php echo $NotGoing; ?>" id="" name="" class="document" readonly/>
                                    </div>
                                    </div>
                                    
                                  
                                 
                                    </div>
                                     <div class="col-lg-3">
                                 <div class="form-group row">
                                         <div class="col-sm-12">
                                         <label for="example-text-input" class="col-form-label form-control-label">Total NotReachable </label>
                                     </div>
                                        <div class="col-sm-12">
                                             <input class="form-control" type="text" value="<?php echo $NotReachable; ?>" id="" name="" class="document" readonly/>
                                    </div>
                                    </div>
                                    
                                  
                                 
                                    </div>
                                     <div class="col-lg-3">
                                 <div class="form-group row">
                                         <div class="col-sm-12">
                                         <label for="example-text-input" class="col-form-label form-control-label">Total NotResponding </label>
                                     </div>
                                        <div class="col-sm-12">
                                             <input class="form-control" type="text" value="<?php echo $NotResponding; ?>" id="" name="" class="document" readonly/>
                                    </div>
                                    </div>
                                    
                                  
                                 
                                    </div>

                                     <div class="col-lg-2">
                                 <div class="form-group row">
                                         <div class="col-sm-12">
                                         <label for="example-text-input" class="col-form-label form-control-label">Total Confirm </label>
                                     </div>
                                        <div class="col-sm-12">
                                             <input class="form-control" type="text" value="<?php echo $Confirm; ?>" id="" name="" class="document" readonly/>
                                    </div>
                                    </div>
                                    
                                  
                                 
                                    </div>

                            <div class="table-responsive table_green">
    

                            <table class="table table-bordered" id="">
                                <thead>
                                <tr>
                                    <th>CandidateName</th>
                                    
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>FollowUp Status</th>
                                 
                                </tr>
                                </thead>
                                <tbody >
                                @php
                                $i=1;
                                @endphp
                                @foreach($cvdetails as $key => $p)
                                    <tr>
                                    <td>{{ !empty($p->candidate_name) ? $p->candidate_name : 'N/A' }}</td>
                                  
                                    <td>{{ !empty($p->candidate_email) ? $p->candidate_email : 'N/A' }}</td>
                                    <td>{{ !empty($p->candidate_mob) ? $p->candidate_mob : 'N/A' }}</td>
                                    <td>{{$p->tca_followupstatus }}</td>
                                      <!-- <td>{{ $p->tca_afterfollowupstatus }}</td> -->
                                    <!-- <td>
                                        <a href="{{ url('viewallocatedcvdetail', [$p->tca_assignfrom, $p->tca_assignto]) }}"><button type="button" class="btn btn-info btn-sm">View Details</button></a>  
                                    </td> -->

                </tr>
                @endforeach
                </tbody>
                </table>
            </div></div>
        </div></div>
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">

function changestatus()
{
    dstatus = document.getElementById("select_id").value;
      assignfrom = document.getElementById("assignfrom").value;
        assignto = document.getElementById("assignto").value;
          posid = document.getElementById("posid").value;
//alert(d);
 $.ajax({
            url: '{{URL::route('alocatedcvdetailsajax')}}',
            type: 'post',
            data:{
                '_token': "{{ csrf_token() }}",
                'status':dstatus,
                  'assignfrom':assignfrom,
                    'assignto':assignto,
                      'posid':posid,
              },
            success: function(data)
            {
                 $('#suggesstion-box').html('');
                 $('#suggesstion-box').html(data);
            }
        });

}



        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
</script>
@endsection

