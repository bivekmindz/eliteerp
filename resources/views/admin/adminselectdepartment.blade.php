@extends('admin.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Client List</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Department</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">View All Department</a>
                    </li>
                </ol>
            </div>
        </div>
    <div class="row">

    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Department  Listing</h5>

            </div>
            <div class="card-block">
                <div class="row">

                <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                        <div class="pull-right;">{{ $departments->links() }}</div>              
                        </div>
                    </div>
                    <div class="col-sm-12 table-responsive"><div class="table-responsive table_green siro">       
                            <table class="table table-bordered" id="">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Department Name</th>



                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                                    $temp = $departments ; 
                            @endphp
                            @foreach($departments as $kk => $departments)
                            <tr>


                                <td> {{  $i++   }}</td>

                                <td>{{  $departments->dept_name }}</td>

                               </tr>

                            @endforeach

                            </tbody>
                        </table>

                       <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                        <div class="pull-right;">{{ $temp->links() }}</div>              
                        </div>
                    </div>
                    </div>
                </div>   </div>
            </div>
        </div>
    </div>

</div>
    @endsection