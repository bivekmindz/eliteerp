@extends('admin.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Client List</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Clients</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">View All Clients</a>
                    </li>
                </ol>
            </div>
        </div>
    <div class="row">

    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Client Listing</h5>

            </div>
            <div class="card-block">

                <div class="row">

                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                          <div class="form-iu">
                    
                 
                      <form action="" method="get">
                      <div class="col-md-4">

                      <select name="user" class="form-control document" >
                         
                            @foreach($users as $kk => $c)
                           <option value="{{  $c->id }}">{{  $c->name }}</option>
@endforeach
                       </select>
                      
 </div>                       <div class="col-md-4">
    @php

    //dd($_GET);
    if(isset($_GET['page']))
    {
     $paged=$_GET['page'];


    }
    else
    {
     $paged=1; 
     $user=0;  
    }
    
        if(isset($_GET['user']))
    {
$user=$_GET['user'];
 }
    else
    {
      $user=0;  
    }
    
    
    @endphp
    <input type="hidden" name="page" value="{{$paged}}">
                        <input type="submit" class="sui" name="formsubmit">
                        </div>
                        </form>
                   
                    <div class="col-md-4">
                    
                    <a href="excelclientdownload?user={{$user}}&page={{$paged}}">
                                                <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel">Download Excel
                                                    <span class="badge"><i class="icofont icofont-file-excel"></i></span>
                                                </button>
                                            </a>
                    </div>
                          
                      
                        </div>  </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                             <div class="pull-right;"></div>
                        </div>
                    </div>

                    <div class="col-sm-12 table-responsive">
                         <div class="table-responsive table_green siro">       
                            <table class="table table-bordered" id="">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Category</th>
                                <th>Company Name</th>
                                <th>Contact Person Name</th>
                                <th>Designation</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Created by</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php
                                $i=1;
                                $temp = $clients ;
                            @endphp
                            @foreach($clients as $kk => $c)
                            <tr >


                                <td> {{  $i++   }}</td>
                                <td> @if($c->client_catid==1)
                                         {{ "IT" }}
                                         @else
                                          {{ "NON IT"  }}
                                         @endif


                                </td>
                                <td>{{  $c->comp_name }}</td>
                                <td>{{  $c->contact_name }}</td>
                                <td>{{ $c->designation }}</td>
                                <td>{{  $c->phone }}</td>
                                <td>{{ $c->emailclient }}</td>
                                <td>{{ $c->name }}</td>
                               </tr>

                            @endforeach

                            </tbody>
                        </table>
<div class="row">
                        <div class="col-sm-4 col-md-4">

                           <div>{{ $clients->links() }}</div>
                        </div>  </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                            <div class="pull-right;"></div>
                        </div>
                    </div>                    

                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
    @endsection