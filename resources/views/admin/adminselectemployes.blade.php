@extends('admin.layouts.master')


@section('content')
<script>
$(document).ready(function(){
    $("body").load(function(){

        document.getElementById("filter_area").focus();
})
    })
$(document).ready(function(){
    $("#filter_area").keyup(function(){
        var xxx = $(this).val();
        //document.getElementById("filter_area").focus();
      //alert(xxx);
          $.ajax({
                      type: 'POST',
                      url: '{{URL::route('admin.ajaxviewadminemployes')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'key':xxx,
                      },
                      success: function(data){
                      //alert(data);
                      $("#suggesstion-box").show();
                      $("#suggesstion-box").html(data);
                      }
                    });

    });
});

</script>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Employee Listing</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Employee</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">View All Employees</a>
                    </li>
                </ol>
            </div>
        </div>
    <div class="row">
    <div class="col-md-12">
    <style>
/*        .pagination li{
            display: inline-block;
        }*/
        .serach-box{float: right;
    position: relative;}
       .serach-box input  {    display: block;
  
    padding: .5rem .75rem;
    font-size: 1rem;
    line-height: 1.25;
    color: #55595c;
    background-color: #fff;
    background-image: none;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
    border: 1px solid rgba(0, 0, 0, .15);
    border-radius: .25rem;}
     .serach-box button{ border: none; background-color: transparent; position: absolute; top: 8px;
    right: 3px; }
     .serach-box button i{}
    </style>

        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Employees Listing</h5>

                 <!-- <div class="serach-box">
                     
                     <input type="text" class="form-controller" id="search" name="search">
                    <button><i class="icofont icofont-search" ></i></button>

                 </div> -->

            </div>
            <div class="card-block">
               

                <div class="row">
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                        <div class="pull-right;">{{ $employees->links() }}</div>
                        </div>
                    </div>

                    <div class="col-sm-12 table-responsive">
                        <span class="text-danger">
                    @if(session('message'))
                                {{session('message')}}
                            @endif
                        </span>
                        <div id="search_filter_filter" style="float:left" class="dataTables_filter"><label>Search: <input type="search" autofocus id="filter_area" style="border: 1px solid #968f8f;" class="" placeholder="" aria-controls="search_filter"></label></div>
                      <div class="table-responsive table_green siro">       
                            <table class="table table-bordered" id="">
                            <thead>
                            <tr>
                               
                                <th> Name</th>
                                <th> Contact No</th>
                                <th> Email</th>
                                <th> Date Of Joining</th>
                                <th>Update</th>
                                <th>Status</th>
                                <th>Offer</th>

                            </tr>
                            </thead>
                            <tbody id= "suggesstion-box">
                            @php
                                $i=1;
                                $temp = $employees ;  
                            @endphp
                            @foreach($employees as $kk => $employees)
                            <tr >


                               
                                <td> {{  $employees->name }}</td>
                                <td>{{  $employees->emp_contactno }}</td>
                                <td>{{  $employees->email }}</td>
                                <td>{{  $employees->emp_doj }}</td>
                                <td>
                                   <a href="{{ route('admin.editadminemployes',$employees->id) }}" class="btn btn-primary">Edit </a>
                                </td>
                                <td>
                                   @if($employees->emp_status==0)
                                   <a title=" Activate Now" href="{{ route('admin.reactive',$employees->id) }}" class="btn btn-primary">Inactive </a>
                                   @elseif($employees->emp_status==1)
                                   <a title="Click For Inactive" href="{{ route('admin.deleteadminemployes',$employees->id) }}" class="btn btn-primary">Active </a>
                                   @endif
                                </td>
                                 <td>
                                   <a href="{{ route('admin.offeradminemployes',$employees->id) }}" class="btn btn-primary" target="_blank">Offer Letter </a>
                                </td>
                               </tr>

                            @endforeach

                            </tbody>
                        </table>
                    </div></div>
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                            <div class="pull-right;">{{ $temp->links() }}</div>
                        </div>
                    </div>                    

                </div>
            </div>
        </div>
    </div>

</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="http://infishop.mindztechnology.com/assets/admin/js/jquery.dataTables.min.js"></script>
<script src="http://infishop.mindztechnology.com/assets/admin/js/dataTables.bootstrap.min.js"></script> -->
<script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );


// function searching_frm(){
//     var error=0;
//     if($('#custm_nm').val()==''){
//       $('#custm_nm').css('border-color','red');
//       error++;
//     }else{
//       $('#custm_nm').css('border-color','');
//     }
//     if($('#fromdate').val()==''){
//       $('#fromdate').css('border-color','red');
//       error++;
//     }else{
//       $('#fromdate').css('border-color','');
//     }
//     if($('#todate').val()==''){
//       $('#todate').css('border-color','red');
//       error++;
//     }else{
//       $('#todate').css('border-color','');
//     }
    
//      //alert(error);
//     if(error==0){
//      return true;
//     }else{
//       return false;
//     }
//   }
</script> 

<!-- <script type="text/javascript">
 $(function() {
        $('#datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: 'AdinController/adminemployes'
        });
    });
 </script> -->

    @endsection