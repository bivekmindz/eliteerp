@extends('admin.layouts.master')
@section('content')


<style type="text/css">
    
    #rectarget {
    padding: 50px;
}
#spoctarget {
    padding: 50px;
}
.list-unstyle{width: 100%;
    float: left;
    height: 150px;
    overflow: auto;}
.tab-sh{ width:100%; float:left;}
.tab-sh .tab-content{width: 100%;
    float: left;
    padding: 14px 12px;}
.he-d{    width: 100%;
    float: left;
    padding: 15px 0px 24px;}

</style>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>TEAM</h4>
                 @if(Session::has('msg'))
                    <div class="alert alert-success">
                        {{ Session::get('msg') }}
                    </div>
                @endif
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Target</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Add Target</a>
                    </li>
                </ol>
            </div>
        </div>

<script type="text/javascript">
  function reccheck(){
    var username = $("#userid").val();
    // alert(username);

        $.ajax({
          type: 'POST',
          url: '{{URL::route('checkrec')}}',
          data:{
            '_token': "{{ csrf_token() }}",
            'param1':username,
            
          },
          success: function(data){
             // alert(data);
             if(data>=1){
              alert("Target already added for this month");
              $("#offer").hide();
              $("#join").hide();

             }
          }
        });

  }
  function removecss(){
    $("#offer").show();
    $("#join").show();

  }
</script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#teamleader').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });

            $('#teammember').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });
        });
    </script>
    <script> 
// $(document).ready(function(){
//     $("#rec").click(function(){
//         $("#rectarget").show();
//     });
// });
</script>
<script>
$(document).ready(function(){
    $("#rec").click(function(){
    var param1 = $("#user").val();
    var param2 = $("#userid").val();
    var param3 = $("#offer").val();
    var param4 = $("#join").val();

    // alert(param4);
    if(param3==''){
      param3=0;
      // alert(param4);
    }
    
    if(param4==''){
      param4=0;
      // alert(param4);
    }
   

    $.ajax({
          type: 'POST',
          url: '{{URL::route('insert')}}',
          data:{
            '_token': "{{ csrf_token() }}",
            'param1':param1,
            'param2':param2,
            'param3':param3,
            'param4':param4,
          },
          success: function(data){
          	// alert(data);
          	//location.reload();
            $("#spoctarget").hide();
          	$("#rectarget").show();
            $("#rectableshow").html(data);
            $("#msg").html(" Recruiter target added successfully!!!! ");
           //   $('#countryList').html(data);  
          }
        }); 

    });
});
</script>
<script>
$(document).ready(function(){
    $("#spocsubmit").click(function(){
    var param1 = $("#spoc").val();
    var param2 = $("#spocid").val();
    var param3 = $("#client").val();
    var param4 = $("#spocoffer").val();
    var param5 = $("#spocjoin").val();
    var param6 = $("#clientid").val();

    if(param4==''){
      param4=0;
      // alert(param4);
    }
    
    if(param5==''){
      param5=0;
      // alert(param4);
    }

// alert(param1);
// alert(param2);
// alert(param3);
// alert(param4);
// alert(param5);
// alert(param6);

    $.ajax({
          type: 'POST',
          url: '{{URL::route('insertspoc')}}',
          data:{
            '_token': "{{ csrf_token() }}",
            'param1':param1,
            'param2':param2,
            'param3':param3,
            'param4':param4,
            'param5':param5,
            'param6':param6,
          },
          success: function(data){
             // alert(data);
            //location.reload();
            $("#rectarget").hide();
            $("#spoctarget").show();
            $("#spoctableshow").html(data);
            $("#msg").html(" Spoc target added successfully!!!! ");
           //   $('#countryList').html(data);  
          }
        }); 

    });
});
</script>
<script>  
 $(document).ready(function(){  
      $('#user').keyup(function(){

   //   alert("biii");
                 var query = $(this).val();  
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('searchrec')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                       // alert(data);
                         $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                      }
                    });
                    
           }  
      });  
      $(document).on('click', 'ul.list-unstyle li', function(){  
     //   alert('hiii');
    // alert($(this).text());
           // alert($(this).attr('id'));
            // alert($(this).id());
           $("#userid").val($(this).attr('id'));
           $('#user').val($(this).text());  
           $('#countryList').fadeOut();  
      });  
 });  
 </script> 
 <script>  
 $(document).ready(function(){  
      $('#client').keyup(function(){

   //   alert("biii");
                 var query = $(this).val();  
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('client')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                       // alert(data);
                         $('#countryListingg').fadeIn();  
                          $('#countryListingg').html(data);  
                      }
                    });
                    
           }  
      });  
      $(document).on('click', 'ul.list-unstyleed li', function(){  
     //   alert('hiii');
    // alert($(this).text());
           // alert($(this).attr('id'));
            // alert($(this).id());
           $("#clientid").val($(this).attr('id'));
           $('#client').val($(this).text());  
           $('#countryListingg').fadeOut();  
      });  
 });  
 </script> 

 <script>  
 $(document).ready(function(){  
      $('#spoc').keyup(function(){

   //   alert("biii");
                 var query = $(this).val();  
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('spocsearch')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                       // alert(data);
                         $('#countryListing').fadeIn();  
                          $('#countryListing').html(data);  
                      }
                    });
                    
           }  
      });  
      $(document).on('click', 'ul.list-unstylee li', function(){  
     //   alert('hiii');
    // alert($(this).text());
           // alert($(this).attr('id'));
            // alert($(this).id());
           $("#spocid").val($(this).attr('id'));
           $('#spoc').val($(this).text());  
           $('#countryListing').fadeOut();  
      });  
 });  
 </script> 

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                
                         <div class="alert alert-default" id="msg" style="color: #819f81">
                           
                         </div>
                         

                <!-- end of modal -->
                <div class="card-block">

                
                <div class="tab-sh">
         

                <ul class="nav nav-tabs  tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link " data-toggle="tab" href="#home1" role="tab">Recruiter</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#profile1" role="tab">Spoc</a>
                    </li>
                  

                </ul>
                <!-- Tab panes -->
                <div class="tab-content tabs">

                    <div class="tab-pane " id="home1" role="tabpanel">
                       
                    <h3 class="card-header-text he-d">Add Target For This Month</h3>
              
                       <form action="" onsubmit="return validateForm();" id="recruiter" method="post">
                        <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
                        <div class="form-group row">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Recruiter Name<span class="error"> * </span></label>
                            <!-- <div class="col-sm-5">
                                <input class="form-control" type="text" name="recname" onchange="onChangeTeam()" value="" id="recname" required="required">
                            </div> -->
                            <div class="col-sm-5">
      
            <input type="text" name="user" id="user" class="form-control" placeholder="Enter user Name"  autocomplete="off" onchange="removecss();"  />  
            <input type="hidden" name="userid" id="userid"/>


                <div id="countryList" class="autogen"></div> 
          
          
          </div>
                            
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Offer Target<span class="error">  </span></label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="offer" onchange="onChangeTeam()" onclick ="reccheck()"  value="" id="offer" >
                            </div>
                            <span class="error" style="display:none;" id="team_msg">Please enter offer target.</span>

                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Join Target<span class="error"> </span></label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="join" onchange="onChangeTeam()" value="" id="join" >
                            </div>
                            <span class="error" style="display:none;" id="team_msg">Please enter join target.</span>

                        </div>

                        <div class="md-input-wrapper">
                            <button type="button" id="rec" class="btn btn-primary waves-effect waves-light">Recruiter</button>
                        </div>
                    </form>

                        <div class="row" id="rectarget" >
        <div class="col-lg-12" >
            <div class="card">
                <div class="card-header">
                    <h3 class="card-header-text">Target Listing for Recruiter</h3>
                </div>

                <!-- end of modal -->
                <div class="card-block">
                    <div id="rectableshow">
                     <div class="table-responsive table_green siro">       
 <table class="table table-bordered" id="search_filter">
                <tr>
                    <th>Recruiter name</th>
                    <th>Offer Target</th>
                    <th>Join Target</th>
                    <th>Action</th>
                </tr>  
                <?php if(!empty($data)){ 
                    foreach($data as $keyus => $k){ ?>
                        <tr>
                            <td><?php echo $k->name ?></td>
                            <td><input type="text" name="rec_offeredtarget" id="recofferedtarget_<?php echo $k->rec_tid; ?>" value="<?php echo $k->rec_offeredtarget ?>" readonly style="border: none"></td>
                            <td><input type="text" name="rec_joinedtarget" id="recjoinedtarget_<?php echo $k->rec_tid; ?>"value="<?php echo $k->rec_joinedtarget ?>" readonly style="border: none"></td>
                            <td>
                                <div class="btn-group btn-group-sm" style="float: none;">
                                    <button type="button" onclick="return editrow('<?php echo $k->rec_tid; ?>');" id="edit<?php echo $k->rec_tid; ?>" class="tabledit-edit-button btn btn-primary waves-effect waves-light" style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span>
                                    </button>
                                    <button type="button" onclick="return updaterow('<?php echo $k->rec_tid; ?>');" id="update<?php echo $k->rec_tid; ?>" class="tabledit-edit-button btn btn-primary waves-effect waves-light" style="float: none;margin: 5px;display: none;" >Submit
                                    </button>
                                </div>
                            </td>
                        </tr>
                       <?php  } 
                         } ?>
               </table>
  </div>
                    </div>


                </div>
</div>
</div>
       
    </div>






                    </div>
                    <div class="tab-pane" id="profile1" role="tabpanel">
                        
                            <h3 class="card-header-text">Add Target For This Month</h3>
                         
                        <form action="" onsubmit="return validateForm();" id="teamm" method="post">
                        <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->

                         <div class="form-group row">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Spoc Name<span class="error"> * </span></label>
                            
                            <div class="col-sm-5">
      
            <input type="text" name="spoc" id="spoc" class="form-control" placeholder="Enter user Name"  autocomplete="off"  onchange="removecssspoc()" />  
            <input type="hidden" name="spocid" id="spocid"/>


                <div id="countryListing" class="autogen"></div> 
          
          
          </div>
                            <!-- <span class="error" style="display:none;" id="team_msg">Please enter recruiter name.</span> -->
                        </div>

                    
                        <div class="form-group row">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Client Name<span class="error"> * </span></label>
                            
                            <div class="col-sm-5">
      
            <input type="text" name="client" id="client" class="form-control" placeholder="Enter client Name"  autocomplete="off"  />  
            <input type="hidden" name="clientid" id="clientid"/>


                <div id="countryListingg" class="autogen"></div> 
          
          
          </div>
                            <!-- <span class="error" style="display:none;" id="team_msg">Please enter recruiter name.</span> -->
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Offer Target<span class="error">  </span></label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="spocoffer" onchange="onChangeTeam()" onclick="spoccheck()"  value="" id="spocoffer" >
                            </div>
                            <span class="error" style="display:none;" id="team_msg">Please enter offer target.</span>

                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Join Target<span class="error">  </span></label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" name="spocjoin" onchange="onChangeTeam()" value="" id="spocjoin" 
                            </div>
                            <span class="error" style="display:none;" id="team_msg">Please enter join target.</span>

                        </div>
                        <div class="md-input-wrapper">                  
                        <button type="button" id="spocsubmit" class="btn btn-primary waves-effect waves-light">Spoc</button>
                        </div>
                        </form>

                        <div class="row" id="spoctarget" >
        <div class="col-lg-12" >
            <div class="card">
                <div class="card-header">
                    <h3 class="card-header-text">Target Listing for Spoc</h3>
                </div>

                <!-- end of modal -->
                <div class="card-block">
                    <div id="spoctableshow">
                       <div class="table-responsive table_green siro">       
 <table class="table table-bordered" id="search_filter">
                <tr>
                    <th>Spoc name</th>
                    <th>Client name</th>
                    <th>Offer Target</th>
                    <th>Join Target</th>
                    <th>Action</th>
                </tr>  
                <?php if(!empty($dataa)){ 
                    foreach($dataa as $keyus => $k){ ?>
                        <tr>
                            <td><?php echo $k->name ?></td>
                            <td><?php echo $k->comp_name ?></td>
                            <td><input type="text" name="spocofferedtarget" id="spocofferedtarget_<?php echo $k->spoc_tid; ?>" value="<?php echo $k->spoc_offeredtarget ?>" readonly style="border: none"></td>
                            <td><input type="text" name="spocjoinedtarget" id="spocjoinedtarget_<?php echo $k->spoc_tid; ?>" value="<?php echo $k->spoc_joinedtarget ?>" readonly style="border: none"></td>
                            <td>
                                <div class="btn-group btn-group-sm" style="float: none;">
                                    <button type="button" onclick="return editrowspoc('<?php echo $k->spoc_tid; ?>');" id="editspoc<?php echo $k->spoc_tid; ?>" class="tabledit-edit-button btn btn-primary waves-effect waves-light" style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span>
                                    </button>
                                    <button type="button" onclick="return updaterowspoc('<?php echo $k->spoc_tid; ?>');" id="updatespoc<?php echo $k->spoc_tid; ?>" class="tabledit-edit-button btn btn-primary waves-effect waves-light" style="float: none;margin: 5px;display: none;" >Submit
                                    </button>
                                </div>
                            </td>
                   
                        </tr>
                       <?php  } 
                         } ?>
               </table>

                    </div>

 </div>
                </div>
</div>
</div>
       
    </div>
                    </div>
                    
                    
                </div>
                                            
                </div>
        
        
    </div>
</div>
</div>
       
    </div>
<script type="text/javascript">
    function editrow(a){
       $("#recofferedtarget_"+a).removeAttr( "readonly");
       $("#recofferedtarget_"+a).removeAttr( "style");
        $("#recjoinedtarget_"+a).removeAttr( "readonly");
       $("#recjoinedtarget_"+a).removeAttr( "style");
       $("#edit"+a).hide();
       $("#update"+a).show();
   
    }

    function updaterow(id){

    var param1 = $("#recofferedtarget_"+id).val();
    var param2 = $("#recjoinedtarget_"+id).val();
    // alert(param1);
    // alert(param2);
    // alert(id);

    $.ajax({
          type: 'POST',
          url: '{{URL::route('update')}}',
          data:{
            '_token': "{{ csrf_token() }}",
            'param1':param1,
            'param2':param2,
            'param3':id,

          },
          success: function(data){
            // alert(data);
            // location.reload();

            $("#spoctarget").hide();
            $("#rectarget").show();
            $("#rectableshow").html(data);
            $("#msg").html(" Recruiter target added successfully!!!! ");




           //   $('#countryList').html(data);  
          }
        }); 





        $("#recofferedtarget_"+id).css( "readonly");
        $("#recofferedtarget_"+id).css( "border", "none");
        $("#recjoinedtarget_"+id).css( "readonly");
        $("#recjoinedtarget_"+id).css( "border", "none");
        $("#edit"+id).show();
        $("#update"+id).hide();


    }
</script>
<script type="text/javascript">
  function spoccheck(){
    var username = $("#spocid").val();
    var client = $("#clientid").val();
    // alert(username);

        $.ajax({
          type: 'POST',
          url: '{{URL::route('checkspoc')}}',
          data:{
            '_token': "{{ csrf_token() }}",
            'param1':username,
            'param2':client,
            
          },
          success: function(data){
             // alert(data);
             if(data>=1){
              alert("Target already added for this month");
              $("#spocoffer").hide();
              $("#spocjoin").hide();

             }
          }
        });

  }
  function removecssspoc(){
    $("#spocoffer").show();
    $("#spocjoin").show();

  }
</script>
<script type="text/javascript">
    function editrowspoc(id){
       $("#spocofferedtarget_"+id).removeAttr( "readonly");
       $("#spocofferedtarget_"+id).removeAttr( "style");
        $("#spocjoinedtarget_"+id).removeAttr( "readonly");
       $("#spocjoinedtarget_"+id).removeAttr( "style");
       $("#editspoc"+id).hide();
       $("#updatespoc"+id).show();
   
    }

    function updaterowspoc(sid){

    var param1 = $("#spocofferedtarget_"+sid).val();
    var param2 = $("#spocjoinedtarget_"+sid).val();
    // alert(param1);
    // alert(param2);
    // alert(sid);

    $.ajax({
          type: 'POST',
          url: '{{URL::route('updatespoc')}}',
          data:{
            '_token': "{{ csrf_token() }}",
            'param1':param1,
            'param2':param2,
            'param3':sid,

          },
          success: function(data){
            // alert(data);
            // location.reload();

            $("#rectarget").hide();
            $("#spoctarget").show();
            $("#spoctableshow").html(data);
            $("#msg").html(" Spoc target added successfully!!!! ");




           //   $('#countryList').html(data);  
          }
        }); 





        $("#spocofferedtarget_"+sid).css( "readonly");
        $("#spocofferedtarget_"+sid).css( "border", "none");
        $("#spocjoinedtarget_"+sid).css( "readonly");
        $("#spocjoinedtarget_"+sid).css( "border", "none");
        $("#editspoc"+sid).show();
        $("#updatespoc"+sid).hide();


    }
</script>


@endsection


<style>
    .error{
        color:red;
    }
</style>

