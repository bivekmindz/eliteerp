@extends('Emp.layouts.master')
@section('content')
    <div class="container-fluid">
        
        <style>
            .autogen{    width: 100%;
    float: left;
    max-height: 137px;
    overflow-y: auto;
    z-index: 999;
    background-color: #fff;}
    .autogen ul {padding: 0px;
    line-height: 35px; border: 1px solid  #e5e5e5;}
     .autogen ul li {padding: 0px 8px; border-bottom: 1px solid #e5e5e5;}
     .autogen ul li:last-child{ border-bottom:none;}
        </style>
           
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
        

        <div class="row">
            <div class="main-header">
                <h4>Recuiter Joining And Pipeline  Report</h4>
             
            </div>
        </div>



       
            <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Recuiter Joining And Pipeline Report</h5>
                           
                        </div>
                        <div class="card-block">


                              <div class="card-block">
                                 <form action=""  method="get" enctype="multipart/form-data"  autocomplete="off">
                                <div class="row">
<?php if($maxempcode!='3') { ?>
     <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Recruiter Name </label>
                                        <div class="col-sm-8">
      
            <input type="text" name="user" id="country" class="form-control" placeholder="Enter user Name"  autocomplete="off" value="{{
                      app('request')->input('user') }}" />  



                <div id="countryList" class="autogen"></div> 
          
          
          </div>
                                    </div>
                                </div>

<?php } ?>
 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Start Date </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" value="{{
                      app('request')->input('doj') }}" id="dojnew" name="doj" class="document"  /> </div>
                                    </div>
                                </div>

 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">End Date </label>
                                        <div class="col-sm-8">
                                             <input class="form-control" type="text" value="{{
                      app('request')->input('doend') }}" id="doend" name="doend" class="document" />
                                    </div>
                                </div>
                              </div>
                     <?php if($maxempcode!='3') { ?>           <div class="col-lg-6">
                                <div class="form-group row">
                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Team Leader<span class="error">  </span></label>
                            <div class="col-sm-8">
                                <div class="">
                                    <select id="teamleader" name="teamleader" class="form-control" >
                                      <option value="">Select</option>
                                      @foreach($teamlead as $kk => $u)
                                            <option 

                                            <?php if(isset($_GET['teamleader']))

                                            {

                                              if($_GET['teamleader']==$u->id) echo 'selected';
                                            
                                          }?>

                                               value="{{ $u->id  }}">{{ $u->name }}</option>
                                        @endforeach
                                        <!--  -->
                                    </select>
                                </div>
                            </div>
                        </div>
</div><?php } ?>
                                 <!-- <div class="col-lg-6">
                                 <div class="form-group row">

                                        <div class="col-sm-12">
                                        
                                        </div>
                                        
                                          <div class="col-sm-12">
                                       
                                        </div>
                                    </div>
                                    
                                  
                                 
                                    </div> -->
                                 </div>

                                </div>



  <div class="md-input-wrapper">
                                <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>
                                
                                <a href="{{URL::to('recuiterjoiningpipelinereports')}}"  class="btn btn-primary waves-effect waves-light">Cancel</a>
                            </div>




                                 



                                </form>
                            </div>

                     
                            <?php 
                            $pipelined = 0;
                              if(!empty($users)){
                                
                                foreach($users as $value)
                                {
                                  $pipelined = ($pipelined + $value->piplined);
                                }
                                // print_r($pipelined);
                              }
                            ?>

                             <div class="col-lg-2">
                                 <div class="form-group row">
                                         <div class="col-sm-12">
                                         <label for="example-text-input" class="col-form-label form-control-label">Total Pipeline </label>
                                     </div>
                                        <div class="col-sm-12">
                                             <input class="form-control" type="text" value="<?php echo $pipelined; ?>" id="" name="" class="document" readonly/>
                                    </div>
                                    </div>
                                    
                                  
                                 
                                    </div>

                                    <?php 
                                    $hold = 0;
                              if(!empty($users)){
                                
                                foreach($users as $value)
                                {
                                  $hold = ($hold + $value->holdstatus);
                                }
                                // print_r($offered);
                              }
                            ?>
                                    <div class="col-lg-2">
                                 <div class="form-group row">
                                         <div class="col-sm-12">
                                         <label for="example-text-input" class="col-form-label form-control-label">Total Hold </label>
                                     </div>
                                        <div class="col-sm-12">
                                             <input class="form-control" type="text" value="<?php echo $hold; ?>" id="" name="" class="document" readonly/>
                                    </div>
                                    </div>
                                    
                                  
                                 
                                    </div>

                                    <?php 
                                    $offered = 0;
                              if(!empty($users)){
                                
                                foreach($users as $value)
                                {
                                     $arrayoffadd = explode(',', $value->Offered);
                                     $Attachmentsofferadd = count($arrayoffadd);
                                  $offered = ($offered + $Attachmentsofferadd);
                                }
                                // print_r($offered);
                              }
                            ?>
                           
                                    <div class="col-lg-2">
                                 <div class="form-group row">
                                          <div class="col-sm-12">
                                         <label for="example-text-input" class="col-form-label form-control-label">Total Offered </label>
                                     </div>
                                        <div class="col-sm-12">
                                             <input class="form-control" type="text" value="<?php echo $offered; ?>" id="" name="" class="document" readonly />
                                    </div>
                                    </div>
                                    
                                  
                                 
                                    </div>

                                    <?php 
                                    $joined = 0;
                              if(!empty($users)){
                                
                                foreach($users as $value)
                                {
                                  $joined = ($joined + $value->joined);
                                }
                                // print_r($offered);
                              }
                            ?>
                                    <div class="col-lg-2">
                                 <div class="form-group row">
                                          <div class="col-sm-12">
                                         <label for="example-text-input" class="col-form-label form-control-label">Total Join </label>
                                     </div>
                                        <div class="col-sm-12">
                                             <input class="form-control" type="text" value="<?php echo $joined; ?>" id="" name="" class="document" readonly/>
                                    </div>
                                    </div>
                                    
                                  
                                 
                                    </div>

                                <?php 
                            $yetjoined = 0;
                              if(!empty($users)){
                                
                                foreach($users as $value)
                                {
                                  $yetjoined = ($yetjoined + $value->yetOffered);
                                }
                                // print_r($pipelined);
                              }
                            ?>

                             <div class="col-lg-2">
                                 <div class="form-group row">
                                            <div class="col-sm-12">
                                         <label for="example-text-input" class="col-form-label form-control-label">Total Yet Join </label>
                                     </div>
                                        <div class="col-sm-12">
                                             <input class="form-control" type="text" value="<?php echo $yetjoined; ?>" id="" name="" class="document" readonly/>
                                    </div>
                                    </div>
                                    
                                  
                                 
                                    </div>   
                                    
                                    
                                       <?php 
                            $dropstatus = 0;
                              if(!empty($users)){
                                
                                foreach($users as $value)
                                {
                                  $dropstatus = ($dropstatus + $value->dropstatus);
                                }
                                // print_r($pipelined);
                              }
                            ?>

                            <div class="col-lg-2">
                                 <div class="form-group row">
                                            <div class="col-sm-12">
                                         <label for="example-text-input" class="col-form-label form-control-label">Total Drop </label>
                                     </div>
                                        <div class="col-sm-12">
                                             <input class="form-control" type="text" value="<?php echo $dropstatus; ?>" id="" name="" class="document" readonly/>
                                    </div>
                                    </div>
                                    
                                  
                                 
                                    </div>  

                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                  




<div class="table-responsive table_green siro">       
 <table class="table table-bordered" id="search_filter">

  <tr>
    <th height="29" width="20%"  scope="col" rowspan="2">User Name</th>
  
    <th width="10%" scope="col" rowspan="2">Pipe Line</th>
      <th width="10%" scope="col" rowspan="2">Hold</th>
    <th width="13%" scope="col" colspan="2" style='text-align: center'>Offered<br>
   </th>
      <th width="10%" scope="col" colspan="2" style='text-align: center'>Joined<br>
    </th>
    <th width="10%" scope="col" rowspan="2" style='text-align: center'>Yet To Join<br>
    </th>
    <th width="16%" scope="col" rowspan="2">Drop</th>
    
    </th>
   
  </tr>
  <tr>
   
    <th width="10%" scope="col">Target
   </th>
    <th width="10%" scope="col">Acheived
   </th>
    <!-- <th width="13%" scope="col">Yet To Offered
   </th>-->
    <th width="10%" scope="col">Target
   </th>
      <th width="10%" scope="col">Acheived
    </th>
  
  <!--   <th width="13%" scope="col">Yet To Joined
   </th>-->
   
  </tr>


 @if(!empty($users))
            @foreach($users as $value)
            <?php $arrayoff = explode(',', $value->Offered);
$Attachmentsoffer = count($arrayoff); ?>
            
            <tr>
  <td>{{$value->name}}</td>
   <td>
       <?php if($value->piplined>0) { ?>
<a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnnpiped" onclick="getresumelistreportpiped(<?php echo $value->id; ?>,<?php echo '5'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$value->piplined}}</a> 
<?php } ?>
</td>

  <td>
       <?php if($value->holdstatus>0) { ?>
<a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value->id; ?>,<?php echo '15'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$value->holdstatus}}</a> 
<?php } ?>
</td>
 <td>{{$value->Offeredtarget}}</td>
   <td>
         <?php if($value->Offered>0) { ?>
    <a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value->id; ?>,<?php echo '4'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$Attachmentsoffer}}</a> 
  <?php } ?>
    </td>

  <td>  
{{$value->joinedtarget}}</td>
   <td>
     <?php if($value->joined>0) { ?>   
       <a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value->id; ?>,<?php echo '6'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$value->joined}}</a>
<?php } ?>
</td>
   <td>
      <?php if( ($value->yetOffered)>0) { ?>  
       <a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnnyet" onclick="getresumelistreportyet(<?php echo $value->id; ?>,<?php echo '6'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" ><?php echo ($value->yetOffered); ?></a>
<?php } ?>
</td>
<td>
     <?php if($value->dropstatus>0) { ?>   
       <a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value->id; ?>,<?php echo '16'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$value->dropstatus}}</a></td>
       <?php } ?>

</tr>
 @endforeach
            @endif
            
              
</div>
<script>  
 $(document).ready(function(){  
      $('#country').keyup(function(){

   //   alert("biii");
                 var query = $(this).val();  
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('searchusers')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                      // alert(data);
                         $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                      }
                    });
                    
                    
              /*  $.ajax({  
                     method:"POST",  
                     url:"searchusers",  
                    
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                     }  
                });  */
           }  
      });  
      $(document).on('click', 'ul.list-unstyled li', function(){  
     //   alert('hiii');
    // alert($(this).text());
           $('#country').val($(this).text());  
           $('#countryList').fadeOut();  
      });  
 });  
 </script>  
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">

    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>


<script>
     $(document).ready(function () {  
          $('#doend').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>
<script>
     $(document).ready(function () {  
          $('#dojnew').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>
    <div class="container">
   <div class="modal fade" id="myModalnnn" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Resumes</h4>
        </div>
           <div class="modal-body" >
               <div id="recruitermm"></div>
    
        </div>   
      </div>      
    </div>
  </div> 
</div>


<div class="container">
   <div class="modal fade" id="myModalnnnyet" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Resumes</h4>
        </div>
           <div class="modal-body" >
               <div id="recruitermmyet"></div>
    
        </div>   
      </div>      
    </div>
  </div> 
</div>



<div class="container">
   <div class="modal fade" id="myModalnnnpiped" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Resumes</h4>
        </div>
           <div class="modal-body" >
               <div id="recruitermmpiped"></div>
    
        </div>   
      </div>      
    </div>
  </div> 
</div>
 <script>
     
function getresumelistreport(fk_id,fk_from,fk_fromdate,fk_todate)
{
    var useridd = fk_id;
  
    var fromid = fk_from;
      var fromdate = fk_fromdate;  
      var todate = fk_todate;

    $.ajax({
        type:"GET",
        url:"getresumesadmindatareport/" + useridd + "/" + fromid + "/" + fromdate+ "/" + todate ,
        data:'',
        success: function(data){
         // alert(data);
            $("#recruitermm").empty();
            $("#recruitermm").append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}   

function getresumelistreportpiped(fk_id,fk_from,fk_fromdate,fk_todate)
{
    var useridd = fk_id;
  
    var fromid = fk_from;
      var fromdate = fk_fromdate;  
      var todate = fk_todate;

    $.ajax({
        type:"GET",
        url:"getresumesadmindatapipereport/" + useridd + "/" + fromid + "/" + fromdate+ "/" + todate ,
        data:'',
        success: function(data){
         // alert(data);
            $("#recruitermmpiped").empty();
            $("#recruitermmpiped").append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}   


function getresumelistreportyet(fk_id,fk_from,fk_fromdate,fk_todate)
{
    var useridd = fk_id;
  
    var fromid = fk_from;
      var fromdate = fk_fromdate;  
      var todate = fk_todate;

    $.ajax({
        type:"GET",
        url:"getresumesadmindatareportyet/" + useridd + "/" + fromid + "/" + fromdate+ "/" + todate ,
        data:'',
        success: function(data){
         // alert(data);
            $("#recruitermmyet").empty();
            $("#recruitermmyet").append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}   


</script>

@endsection



