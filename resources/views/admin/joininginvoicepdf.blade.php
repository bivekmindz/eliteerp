
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Invoice</title>
</head>

<body>



  <!-- CSS for the things we want to print (print view) -->
<script language="javascript">
  function PrintDiv() {    
           var divToPrint = document.getElementById('divToPrint');
           var popupWin = window.open('', '_blank', 'width=766,height=300');
           popupWin.document.open();
           popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
            popupWin.document.close();
                }
</script>
</body>

       
<button style="color: #ffff;border: none;padding: 5px 10px 5px 10px;background: #929292;"  onClick="PrintDiv();"> Print </button>
<div  id="divToPrint">
<table width="900" height="100%" style="margin:auto; border:solid 1px #999999; border-spacing: 0px;" cellpadding="0" cellspacing="0" >

    <tbody>
        <tr>
            <td>
                <table style="width:100%; border-spacing: 0px;">
                    <tr>
                        <td style="text-align:center; padding:40px 0; font-weight:bold;">TAX INVOICE</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td>
                <table style="width:100%; border-spacing: 0px;">
                    <tr>
                        <td style="text-align:left; padding-left:15px;">Invoice No: <?php echo $invpdf->recuiter_invno; ?></td>
                        <td style="text-align:right; padding-right:15px;">Place of Supply: <?php echo $invpdf->clientsdetails_location; ?></td>
                    </tr>

                    <tr>
                        <td style="text-align:left; padding-left:15px;">Invoice Date: <?php 
                        $originalDate =date('Y-m-d');
echo $newDate = date("d-m-Y", strtotime($originalDate));

                        ?></td>
                        <td style="text-align:right; padding-right:15px;">SAC/HSN Code: 998512</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td style="padding-top:40px;">
                <table style="width:100%; border-spacing: 0px;">
                    <tr>
                        <td style="text-align:left; width:50%; font-weight:bold; border-top:solid 1px #999999; border-right:solid 1px #999999; padding:5px 15px 5px;">Client/Vendor Details</td>
                        <td style="text-align:left; font-weight:bold; border-top:solid 1px #999999; padding:5px 15px 5px;">Recipient’s Details</td>
                    </tr>

                    <tr>
                        <td style="text-align:left; border-top:solid 1px #999999; border-right:solid 1px #999999; padding:5px 15px 5px;">Name: Elite HR Practices Pvt. Ltd.</td>
                        <td style="text-align:left; border-top:solid 1px #999999; padding-left:15px;">Name:  <?php echo $invpdf->clientname; ?></td>
                    </tr>
                    <tr>
                        <td valign="top" style="text-align:left; border-top:solid 1px #999999; border-right:solid 1px #999999; padding:5px 15px 5px;">Address: WZ-34/4 and 34/5 2nd Floor<br>
Asalatpur, A-3 Block, Janakpuri,<br>
New Delhi 110058</td>
                        <td valign="top" style="text-align:left; border-top:solid 1px #999999; padding-left:15px;"> <?php echo $invpdf->clientsdetails_billingaddress; ?></td>
                    </tr>

 <tr>
     <td style="text-align:left; border-top:solid 1px #999999; border-right:solid 1px #999999; padding:5px 15px 5px;">GSTIN: 07AACCE2449NIZE</td>
    <td style="text-align:left; border-top:solid 1px #999999; padding-left:15px;">GSTIN:  <?php echo $invpdf->clientsdetails_gst; ?></td>
 </tr>


  <tr>
     <td style="text-align:left; border-top:solid 1px #999999; border-right:solid 1px #999999; padding:5px 15px 5px;">PAN NO: AACCE2449</td>
    <td style="text-align:left; border-top:solid 1px #999999; padding-left:15px;">PAN NO:  <?php echo $invpdf->clientsdetails_panno; ?></td>
 </tr>


  <tr>
     <td style="text-align:left; border-top:solid 1px #999999; border-right:solid 1px #999999; border-bottom:solid 1px #999999; padding:5px 15px 5px;">State: New Delhi</td>
    <td style="text-align:left; border-top:solid 1px #999999; border-bottom:solid 1px #999999; padding-left:15px;">State: <?php echo $invpdf->state_name; ?></td>
 </tr>

                </table>
            </td>
        </tr>

 <tr>
       <td style="padding-top:35px;">
        <table style="width:100%; border-spacing: 0px;">
            <tr>
               <?php if($invpdf->recuiter_empid!='') { ?>
                <td style="border-bottom:solid 1px #999999; border-top:solid 1px #999999; border-right:solid 1px #999999; padding:5px 5px 5px;">Emp ID</td>
                  <?php } ?>
                <td style="border-bottom:solid 1px #999999; border-top:solid 1px #999999; border-right:solid 1px #999999; padding:5px 5px 5px;">Name</td>
                <td style="border-bottom:solid 1px #999999; border-top:solid 1px #999999; border-right:solid 1px #999999; padding:5px 5px 5px;">DOJ</td>
                <td style="border-bottom:solid 1px #999999; border-top:solid 1px #999999; border-right:solid 1px #999999; padding:5px 5px 5px;">Department/Designation</td>
                <td style="border-bottom:solid 1px #999999; border-top:solid 1px #999999; border-right:solid 1px #999999; padding:5px 5px 5px;">Annual CTC</td>
                <td style="border-bottom:solid 1px #999999; border-top:solid 1px #999999; border-right:solid 1px #999999; padding:5px 5px 5px;">Payout %</td>
                <td style="border-bottom:solid 1px #999999; border-top:solid 1px #999999; padding:5px 5px 5px;">Emp Location</td>
            </tr>


            <tr>
                <?php if($invpdf->recuiter_empid!='') { ?>
                <td style="border-bottom:solid 1px #999999; border-right:solid 1px #999999; padding:5px 5px 5px;"> <?php echo $invpdf->recuiter_empid; ?></td>
                <?php } ?>
                <td style="border-bottom:solid 1px #999999; border-right:solid 1px #999999; padding:5px 5px 5px;"> <?php echo $invpdf->candidate_name; ?></td>
                <td style="border-bottom:solid 1px #999999; border-right:solid 1px #999999; padding:5px 5px 5px;"><?php 
 echo gmdate('F j, Y', strtotime($invpdf->dateoj));?></td>
                <td style="border-bottom:solid 1px #999999; border-right:solid 1px #999999; padding:5px 5px 5px;"> <?php echo $invpdf->positionname; ?></td>
                <td style="border-bottom:solid 1px #999999; border-right:solid 1px #999999; padding:5px 5px 5px;">Rs. <?php echo $invpdf->recuiter_invoiceamount; ?></td>
                <td style="border-bottom:solid 1px #999999; border-right:solid 1px #999999; padding:5px 5px 5px;"><?php echo $invpdf->recuiter_invoicepercentage; ?>%</td>
                <td style="border-bottom:solid 1px #999999; padding:5px 15px 5px;"><?php echo $invpdf->clientsdetails_location; ?></td>
            </tr>

        </table>
    </td>
 </tr>


 <tr>
     <td style="padding-top:35px;">
        <table style="width:100%; border-spacing: 0px;">
            <tr>
                <tr>
                <td style="border-bottom:solid 1px #999999; width:50%; border-top:solid 1px #999999; border-right:solid 1px #999999; padding:5px 15px 5px; font-weight:bold;">Total Invoice Amount In Words</td>
                <td valign="top" style="border-bottom:solid 1px #999999; border-top:solid 1px #999999; padding:5px 5px 5px; font-weight:bold;">Total Amount before Tax: Rs. <?php echo $totamt=$invpdf->recuiter_invoicetotamount; ?></td>

            </tr>

<?php if($invpdf->recuiter_sezstatus=='1') {  $cgst=($invpdf->state_cgst*$invpdf->recuiter_invoicetotamount)/100; } else { $cgst=0;} ?>
<?php if($invpdf->recuiter_sezstatus=='1') {  $sgst=($invpdf->state_sgst*$invpdf->recuiter_invoicetotamount)/100; } else { $sgst=0;} ?>
<?php if($invpdf->recuiter_sezstatus=='1') {  $igst=($invpdf->state_igst*$invpdf->recuiter_invoicetotamount)/100; } else { $igst=0;} ?>
             <tr>
                <td style="border-bottom:solid 1px #999999; border-right:solid 1px #999999; padding:5px 15px 5px;">
  
  <?php
  $tt=$totamt+$cgst+$sgst+$igst;
$number = $tt;
   $no = round($number);
   $point = round($number - $no, 2) * 100;
   $hundred = null;
   $digits_1 = strlen($no);
   $i = 0;
   $str = array();
   $words = array('0' => '', '1' => 'one', '2' => 'two',
    '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six',
    '7' => 'seven', '8' => 'eight', '9' => 'nine',
    '10' => 'ten', '11' => 'eleven', '12' => 'twelve',
    '13' => 'thirteen', '14' => 'fourteen',
    '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen',
    '18' => 'eighteen', '19' =>'nineteen', '20' => 'twenty',
    '30' => 'thirty', '40' => 'forty', '50' => 'fifty',
    '60' => 'sixty', '70' => 'seventy',
    '80' => 'eighty', '90' => 'ninety');
   $digits = array('', 'hundred', 'thousand', 'lakh', 'crore');
   while ($i < $digits_1) {
     $divider = ($i == 2) ? 10 : 100;
     $number = floor($no % $divider);
     $no = floor($no / $divider);
     $i += ($divider == 10) ? 1 : 2;
     if ($number) {
        $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
        $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
        $str [] = ($number < 21) ? $words[$number] .
            " " . $digits[$counter] . $plural . " " . $hundred
            :
            $words[floor($number / 10) * 10]
            . " " . $words[$number % 10] . " "
            . $digits[$counter] . $plural . " " . $hundred;
     } else $str[] = null;
  }
  $str = array_reverse($str);
  $result = implode('', $str);
  $points = ($point) ?
    "." . $words[$point / 10] . " " . 
          $words[$point = $point % 10] : '';
  echo $result . "Rupees  " . $points . "Only ";
 ?> 

  <script type="text/javascript">
    var translator = new T2W("EN_US");
    var num =  document.getElementById("totamount").getAttribute("data-value"); 
    //alert(num);
    var word = translator.toWords(parseInt(num));
    document.getElementById('words').textContent = word;
  </script>
                </td>
                <td valign="top" style="border-bottom:solid 1px #999999; padding:5px 5px 5px;">Add: CGST @ <?php if($invpdf->recuiter_sezstatus=='1') { echo $invpdf->state_cgst."%"; } else {} ?>: Rs. <?php if($invpdf->recuiter_sezstatus=='1') { echo $cgst=($invpdf->state_cgst*$invpdf->recuiter_invoicetotamount)/100; } else { $cgst=0;} ?></td>
            </tr>


             <tr>
                <td style="border-bottom:solid 1px #999999; border-right:solid 1px #999999; padding:5px 15px 5px;"></td>
                <td valign="top" style="border-bottom:solid 1px #999999; padding:5px 5px 5px;">Add: SGST @<?php if($invpdf->recuiter_sezstatus=='1') { echo $invpdf->state_sgst."%"; } else {} ?>: Rs. <?php if($invpdf->recuiter_sezstatus=='1') { echo $sgst=($invpdf->state_sgst*$invpdf->recuiter_invoicetotamount)/100; } else { $sgst=0;} ?></td>
            </tr>

             <tr>
                <td style="border-bottom:solid 1px #999999; border-right:solid 1px #999999; padding:5px 15px 5px;"></td>
                <td valign="top" style="border-bottom:solid 1px #999999; padding:5px 5px 5px;">Add: IGST @<?php if($invpdf->recuiter_sezstatus=='1') { echo $invpdf->state_igst."%"; } else {} ?>: Rs. <?php if($invpdf->recuiter_sezstatus=='1') { echo $igst=($invpdf->state_igst*$invpdf->recuiter_invoicetotamount)/100; } else { $igst=0;} ?></td>

            </tr>

             <tr>
                <td style="border-bottom:solid 1px #999999; border-right:solid 1px #999999; padding:5px 15px 5px;"></td>
                <td valign="top" style="border-bottom:solid 1px #999999; padding:5px 5px 5px; font-weight:bold;">Total Amount After Tax: Rs. <?php echo ($totamt+$cgst+$sgst+$igst); ?></td>
            </tr>

        </table>
    </td>
 </tr>


  <tr>
     <td style="padding-top:35px;">
        <table style="width:100%; border-spacing: 0px;">
            <tr>
                <tr>
                <td style="border-bottom:solid 1px #999999; width:50%; border-top:solid 1px #999999; border-right:solid 1px #999999; padding:5px 15px 5px; font-weight:bold;">Bank Details</td>
                <td valign="top" style="border-bottom:solid 1px #999999; border-top:solid 1px #999999; padding:5px 5px 5px; font-weight:bold;">For Elite HR Practices Pvt. Ltd.</td>

            </tr>


             <tr>
                <td valign="top" style="border-right:solid 1px #999999; padding:5px 15px 5px;">Bank Name: HDFC Bank Ltd<br>
Branch: A3/11, Janakpuri<br>
               New Delhi – 110058<br>
Bank A/c No: 15582320000086<br>
Bank IFSC: HDFC0001558</td>
                <td style="padding:5px 5px 5px;"></td>
            </tr>

        </table>
    </td>
 </tr>




    </tbody>
</table>



</div>
</body>
</html>



 <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>

 <script type="text/javascript">
 let doc = new jsPDF();
 doc.addHTML(document.body,function() {
     doc.save('invoice.pdf');
 });
</script>  
