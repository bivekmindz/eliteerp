@extends('admin.layouts.master')
@section('content')

           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
    <div class="container-fluid">
        
        <style>
            .autogen{    width: 100%;
    float: left;
    max-height: 137px;
    overflow-y: auto;
    z-index: 999;
    background-color: #fff;}
    .autogen ul {padding: 0px;
    line-height: 35px; border: 1px solid  #e5e5e5;}
     .autogen ul li {padding: 0px 8px; border-bottom: 1px solid #e5e5e5;}
     .autogen ul li:last-child{ border-bottom:none;}
        </style>
           <style>

button
{

  background-color:#fff;
  border:none;
  cursor:pointer;
}

button:hover
{

  background-color:#fff;
  border:none;
  cursor:pointer;
  color:green;
}


</style>

        

        <div class="row">
            <div class="main-header">
                <h4>Spoc Joining And Pipeline  Report</h4>
             
            </div>
        </div>
        

       
            <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Spoc Joining And Pipeline Report</h5>
                           
                        </div>
                        <div class="card-block">


                              <div class="card-block">
                                 <form action=""  method="get" enctype="multipart/form-data"  autocomplete="off">
                                <div class="row">

     <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">User </label>
                                        <div class="col-sm-8">
      
            <input type="text" name="user" id="country" class="form-control" placeholder="Enter user Name"  autocomplete="off"  />  



                <div id="countryList" class="autogen"></div> 
          
          
          </div>
                                    </div>
                                </div>


 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Start Date </label>
                                        <div class="col-sm-8">
                                            <input class="form-control document" type="text" value="" id="dojnew" name="doj"/> </div>
                                    </div>
                                </div>

 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">End Date </label>
                                        <div class="col-sm-8">
                                             <input class="form-control document" type="text" value="" id="doend" name="doend" />
                                    </div>
                                </div>
                                
                                 </div>



<div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Clients</label>
                                        <div class="col-sm-8">
                                                <input type="text" name="clients" id="clients" class="form-control" placeholder="Enter Client Name"  autocomplete="off" />  



                <div id="ClientList" class="autogen"></div> 
                                    </div>
                                </div>
                                
                                 </div>

                                </div>

  <div class="md-input-wrapper">
                                 <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>  
                                
                                <a href="joiningpipelinereports"  class="btn btn-primary waves-effect waves-light">Cancel</a>
 <span href="#" class="btn btn-primary waves-effect waves-light" id="downloadpdfff">Download  PDF</span>

                            </div>




                                 



                                </form>


<!-- Script -->
 
                            </div>

     
</div>

                            <div class="row">
                                <div class="col-sm-12 table-responsive" >



  <div id="topdf" style="width: 100%;overflow: hidden;">
        <table width="100%" id="totable"  border="1">
           <tr><td >


<!-- Content Area -->

          


  @if(!empty($data))
            @foreach($data as $value)

   <?php 
   $subm=0;
 if(count($value['client'])>0) {
?>

<table width="100%" border="1" cellspacing="1" cellpadding="1"  >

<tr <?php if($value['state']==0){ echo 'style="background-color:red;font-weight:bold"';}?>>
 <td width="10%" style="border:none;" height="30" align="left"><strong <?php if($value['state']==0){ echo 'style="color:#fff;font-weight:bold"';}else{?> style="color:#1f2ccc;"<?php }?>>{{ $value['username']}},</strong></td>
    <td colspan="6" style="border:none;"><p>&nbsp;</p>
    <p>&nbsp;</p></td>
   
  </tr>
  
  <tr>
    <th height="39" width="30%"  scope="col" rowspan="2">Client</th>
  
    <th width="16%" scope="col" rowspan="2">Pipe Line</th>
    <th width="13%" scope="col" colspan="2" style='text-align: center'>Offered<br>
   </th>
      <th width="30%" scope="col" colspan="2" style='text-align: center'>Joined<br>
    </th>
  <th width="16%" scope="col" rowspan="2">Drop</th>
   
  </tr>
  <tr>
   
    <th width="13%" scope="col">Target
   </th>
    <th width="13%" scope="col">Actual
   </th>
    <th width="13%" scope="col">Target
   </th>
      <th width="30%" scope="col">Actual
    </th>
  
   
  </tr>


<?php  foreach($value['client'] as $keyus => $k){?>
<tr>
  <td><?php echo $k->comp_name; ?></td>
  <td>
   <a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value['empcode']; ?>,<?php echo $k->client_id; ?>,<?php echo '5'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$k->piplined}}</a>    
      </td>
  <td><?php echo  $k->Offeredtarget; ?></td>
   <td>
    <a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value['empcode']; ?>,<?php echo $k->client_id; ?>,<?php echo '4'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$k->Offered}}</a>    
      </td>
    <td><?php echo  $k->joinedtarget; ?></td>
   <td>
       <a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value['empcode']; ?>,<?php echo $k->client_id; ?>,<?php echo '6'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$k->joined}}</a>    
     </td>
  <td>
   <a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value['empcode']; ?>,<?php echo $k->client_id; ?>,<?php echo '16'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$k->dropstatus}}</a>    
      </td>
</tr>

<?php } ?>


  <!-- 
<?php //echo  $c = count($value['userreport']);?>

<?php if(!empty($value['userreport'])) { ?>
<?php  foreach($value['userreport'] as $keyus => $k){?>
  <tr>
     <td><?php if(!empty($value['client'][$keyus])) { echo $value['client'][$keyus]->comp_name; }  ?>
     
     
     
    
     </td>
      
      
        <td><?php if(!empty($value['position'][$keyus])) { ?><a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value['empcode'];?>,<?php echo $value['position'][$keyus]->clientjob_id;?>,'<?php echo $from; ?>','<?php echo $to; ?>')" > <?php echo $value['position'][$keyus]->clientjob_title; }  ?></a></td>
      
     <td><?php echo($k->trtr);  ?></td>
     <td><?php if(!empty($value['userreport2'][$keyus])) { echo $value['userreport2'][$keyus]->sendclient; } else { echo "0"; } ?></td>
        <td><?php if(!empty($value['userreport2'][$keyus])) { echo $value['userreport2'][$keyus]->sendclient; } else { echo "0"; } ?></td>
        
       
   </tr>
   
   

   
<?php } } ?> -->
    




</table>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<?php } ?>
    @endforeach
            @endif
       </td></tr>
       </table>
    </div>     
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
             </div>
             
             
</div>
             
             
             
            
            </div>
            
       <div class="container">
   <div class="modal fade" id="myModalnnn" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Resumes</h4>
        </div>
           <div class="modal-body" >
       <table width="90%" border="1" cellspacing="0" cellpadding="0">
           <thead>
           <tr>
           <th> Name</th><th>Mobile Id</th>   <th>Email Id</th> <th>Resume Name</th> <th>Created On</th><th>Recuiter Name</th>
            </tr>
            </thead>
            <tbody id="recruiter">
          
            </tbody>
             
            
        </table>
        </div>   
      </div>      
    </div>
  </div> 
</div>
       
<script src="{{ URL::asset('js/jspdf.debug.js') }}"></script>
<script src="{{ URL::asset('js/html2canvas.min.js') }}"></script>

        <script>
            function upload_position_jd(clientjob_id) {
                $.ajax({
                    url: 'adminpositionjd',
                    data: { id:clientjob_id },
                    success: function(result){
                        $("#positionJd").html(result);
                    }
                });
                $('#myModal').modal('show');
            }
    </script>
     <script>
    </script>

        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">

    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>
 <script>
     
function getresumelistreport(fk_id,fk_posid,fk_from,fk_fromdate,fk_todate)
{
    var useridd = fk_id;
    var position_id = fk_posid;
    var fromid = fk_from;
      var fromdate = fk_fromdate;  
      var todate = fk_todate;

    $.ajax({
        type:"GET",
        url:"getresumesclientdatareport/" + useridd + "/"  + position_id + "/" + fromid + "/" + fromdate+ "/" + todate ,
        data:'',
        success: function(data){
        //  alert(data);
            $("#recruiter").empty();
            $("#recruiter").append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}    </script>
<script>  
 $(document).ready(function(){  
      $('#clients').keyup(function(){  
           var query = $(this).val();  
          // alert("hiii");
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('searchclients')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                      // alert(data);
                         $('#ClientList').fadeIn();  
                          $('#ClientList').html(data);  
                      }
                    });
                    
                    
              /*  $.ajax({  
                     method:"POST",  
                     url:"searchusers",  
                    
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                     }  
                });  */
           }  
      });  
      $(document).on('click', 'ul.list-unstyledclients li', function(){  
          $('#clients').val($(this).text());  
           $('#ClientList').fadeOut();  
      });  
 });  
 </script>  
 <script src="{{ URL::asset('js/jspdf.debug.js') }}"></script>
 <script src="{{ URL::asset('js/html2canvas.min.js') }}"></script>
   
   
    <script type="text/javascript">
        var quotes = document.getElementById('totable');
        //! MAKE YOUR PDF
        var pdf = new jsPDF('p', 'pt', 'letter');
        html2canvas(quotes, {
            onrendered: function (canvas) {



                for (var i = 0; i <= quotes.clientHeight / 980; i++) {
                    //! This is all just html2canvas stuff
                    var srcImg = canvas;
                    var sX = 0;
                    var sY = 1500 * i; // start 1100 pixels down for every new page
                    var sWidth = 1500;
                    var sHeight = 1500;
                    var dX = 0;
                    var dY = 0;
                    var dWidth = 1500;
                    var dHeight = 1100;

                    window.onePageCanvas = document.createElement("canvas");
                    onePageCanvas.setAttribute('width', 1200);
                    onePageCanvas.setAttribute('height', 1100);
                    var ctx = onePageCanvas.getContext('2d');
                    // details on this usage of this function: 
                    // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#Slicing
                    ctx.drawImage(srcImg, sX, sY, sWidth, sHeight, dX, dY, dWidth, dHeight);

                    // document.body.appendChild(canvas);
                    var canvasDataURL = onePageCanvas.toDataURL("image/png", 1.0);

                    var width = onePageCanvas.width;
                    var height = onePageCanvas.clientHeight;

                    //! If we're on anything other than the first page,
                    // add another page
                    if (i > 0) {
                        pdf.addPage(612, 791); //8.5" x 11" in pts (in*72)
                    }
                    //! now we declare that we're working on that page
                    pdf.setPage(i + 1);
                    //! now we add content to that page!
                    pdf.addImage(canvasDataURL, 'PNG', 20, 40, (width * .32), (height * .32));

                }
            }
        })

        $('#downloadpdfff').click(function () {
            pdf.save('joiningpipelinereports.pdf');
        });
    </script>

<script>  
 $(document).ready(function(){  
      $('#country').keyup(function(){

   //   alert("biii");
                 var query = $(this).val();  
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('searchusers')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                      // alert(data);
                         $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                      }
                    });
                    
                    
              /*  $.ajax({  
                     method:"POST",  
                     url:"searchusers",  
                    
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                     }  
                });  */
           }  
      });  
      $(document).on('click', 'ul.list-unstyled li', function(){  
     //   alert('hiii');
    // alert($(this).text());
           $('#country').val($(this).text());  
           $('#countryList').fadeOut();  
      });  
 });  
 </script>  
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">

    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>


<script>
     $(document).ready(function () {  
          $('#doend').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>
<script>
     $(document).ready(function () {  
          $('#dojnew').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>


@endsection



