@extends('admin.layouts.master')
@section('content')

          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
    <div class="container-fluid">
        
        <style>
            .autogen{    width: 100%;
    float: left;
    max-height: 137px;
    overflow-y: auto;
    z-index: 999;
    background-color: #fff;}
    .autogen ul {padding: 0px;
    line-height: 35px; border: 1px solid  #e5e5e5;}
     .autogen ul li {padding: 0px 8px; border-bottom: 1px solid #e5e5e5;}
     .autogen ul li:last-child{ border-bottom:none;}
        </style>
           <style>

button
{

  background-color:#fff;
  border:none;
  cursor:pointer;
}

button:hover
{

  background-color:#fff;
  border:none;
  cursor:pointer;
  color:green;
}


</style>

        

        <div class="row">
            <div class="main-header">
                <h4>LATE COMERS REPORT</h4>
             
            </div>
        </div>
        

       
            <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">LATE COMERS REPORT</h5>
                           
                        </div>
                     
                            <div class="row">
                               <div class="card-block">
                                 <form action=""  method="get" enctype="multipart/form-data"  autocomplete="off">
                                <div class="row">

     <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">User </label>
                                        <div class="col-sm-8">
     
         
            <input type="text" name="user" id="country" class="form-control" placeholder="Enter user Name"  autocomplete="off" value="" />  
                <div id="countryList" class="autogen"></div> 
          
          
          </div>
                                    </div>
                                </div>


 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Start Date </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" value="" id="dojnew" name="doj" class="document" /> </div>
                                    </div>
                                </div>

 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">End Date </label>
                                        <div class="col-sm-8">
                                             <input class="form-control" type="text" value="" id="doend" name="doend" class="document" />
                                    </div>
                                </div>
                                
                                

                                 <div class="col-lg-6">
                                 <div class="form-group row">

                                        <div class="col-sm-12">
                                        
                                        </div>
                                        
                                          <div class="col-sm-12">
                                       
                                        </div>
                                    </div>
                                    
                                  
                                 
                                    </div>
                                 </div>

                                </div>



  <div class="md-input-wrapper">
                                <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>
                                
                                <a href="lateentryreports"  class="btn btn-primary waves-effect waves-light">Cancel</a>
                            </div>




                                 



                                </form>
                            </div>
                                <div class="col-sm-12 table-responsive" >
 <div class="table-responsive table_green siro">       
                            <table class="table table-bordered" id="search_filter">
             <thead>
             <tr>
          <th> Emp Code</th>
            <th> Emp Name</th>
         
             <th> Punchin Time</th>
             <th> Late By</th>
              
             
             </tr>
             </thead>
            <tbody>

            @if(!empty($data))
            @foreach($data as $value)
               @php
              $punchrecord=explode(",",$value->punchrecord);
               $startpunch = $punchrecord[0];
               
$stoppunch=$value->lastpunch;
  $to_time1 = strtotime( $value->lastpunch );
                    $from_time1 = strtotime($startpunch); 
 $diff_in_minutesnew  =round(((($from_time1 - $to_time1) / 60))); 
 if($diff_in_minutesnew>0)
 {
   $startpunch = $value->lastpunch;
 }
 else
 {
   $startpunch = $punchrecord[0];
 }
 
 
            
                    $to_time = strtotime( "09:46:00" );
                    $from_time = strtotime($startpunch); 
      $diff_in_minutes  =round(((($from_time - $to_time))));
      if($diff_in_minutes >0) {
       $diff_in_minutesne  =round(((($from_time - $to_time) / 60))); 
$seconds = $diff_in_minutes;
$ee= gmdate('H:i:s', $seconds);
              @endphp
                <tr>
                <td>{{ $value->empcode }}</td>
                 <td>{{ $value->empname }}</td>
         
             
                  <td>{{ $startpunch  }}</td>
              <td>{{ $ee  }}</td>
                   
               
                </tr>
@php } @endphp
                @endforeach
            @endif
            </tbody>
         </table>

  
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
             </div>
             
             
</div>
             
            </div>
             
             
            
            </div>
            
            

<div class="container">
   <div class="modal fade" id="myModalnnn" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Pre Time</h4>
        </div>

           <form method="post" action="{{ URL::to('updatespockdata') }}">
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="form-group">
                        <div id="recruiter"></div>
                        </div>
                       
                     
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            
            </div>
        </form>



        <div id=""></div>
      
      </div>      
    </div>
  </div> 

  </div>

<div class="container">
   <div class="modal fade" id="myModalresnnn" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Resumes</h4>
        </div>

          
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="form-group">
                        <div id="recruiterres"></div>
                        </div>
                       
                     
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            
            </div>
        


        <div id=""></div>
      
      </div>      
    </div>
  </div> 

  </div>

  
        <script>

function getresumestartlistreport(diffmin,fk_id,clreqid,frtdate,todate,stpunch)
{

 var diffmin = diffmin;
  var fk_id = fk_id;
   var clreqid = clreqid;
    var frtdate = frtdate;
     var todate = todate;
      var stpunch = stpunch;


    $.ajax({
        type:"POST",
        url:"getresumesprepostnewreports",
      data: {'diffmin':diffmin,'fk_id':fk_id,'clreqid':clreqid,'frtdate':frtdate,'todate':todate,'stpunch':stpunch, '_token': "{{ csrf_token() }}",},
        success: function(data){
        //  alert(data);
            $("#recruiter").empty();
            $("#recruiter").append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}  

function getresumelistreport(fk_id,frtdate,todate)
{

  var fk_id = fk_id;
 
    var frtdate = frtdate;
     var todate = todate;
  
    $.ajax({
        type:"GET",
        url:"getresumesprepostdatareports/" + fk_id + "/" + frtdate +"/" + todate ,
        data:'',
        success: function(data){
          //  alert(data);
            $("#recruiterres").empty();
            $("#recruiterres").append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}  






          function getposiionvalue(id)
{

 var ids = id;
   document.getElementById("placepopupval").value = id;   

}


            function upload_position_jd(clientjob_id) {
                $.ajax({
                    url: 'adminpositionjd',
                    data: { id:clientjob_id },
                    success: function(result){
                        $("#positionJd").html(result);
                    }
                });
                $('#myModal').modal('show');
            }
    </script>
     <script>
    </script>

        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">

    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>
<script>  
 $(document).ready(function(){  
      $('#clients').keyup(function(){  
           var query = $(this).val();  
          // alert("hiii");
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('searchclients')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                      // alert(data);
                         $('#ClientList').fadeIn();  
                          $('#ClientList').html(data);  
                      }
                    });
                    
                    
            
           }  
      });  
      $(document).on('click', 'ul.list-unstyledclients li', function(){  
          $('#clients').val($(this).text());  
           $('#ClientList').fadeOut();  
      });  
 });  
 </script>  


<script>  
 $(document).ready(function(){  
      $('#country').keyup(function(){

   //   alert("biii");
                 var query = $(this).val();  
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('searchattusers')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                      // alert(data);
                         $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                      }
                    });
                    
             
           }  
      });  
      $(document).on('click', 'ul.list-unstyled li', function(){  
     //   alert('hiii');
    // alert($(this).text());
           $('#country').val($(this).text());  
           $('#countryList').fadeOut();  
      });  
 });  
 </script>  
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">

    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>


<script>
     $(document).ready(function () {  
          $('#doend').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>
<script>
     $(document).ready(function () {  
          $('#dojnew').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>
 

@endsection



