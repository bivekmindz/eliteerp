<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

@extends('Emp.layouts.master')
@section('content')
    <div class="container-fluid">
        
        <style>
            .autogen{    width: 100%;
    float: left;
    max-height: 137px;
    overflow-y: auto;
    z-index: 999;
    background-color: #fff;}
    .autogen ul {padding: 0px;
    line-height: 35px; border: 1px solid  #e5e5e5;}
     .autogen ul li {padding: 0px 8px; border-bottom: 1px solid #e5e5e5;}
     .autogen ul li:last-child{ border-bottom:none;}
        </style>
           
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
        

        <div class="row">
            <div class="main-header">
                <h4>Recuiter Joining And Pipeline  Report</h4>
             
            </div>
        </div>



       
            <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Recuiter Joining And Pipeline Report</h5>
                           
                        </div>
                        <div class="card-block">


                              <div class="card-block">
                                 <form action=""  method="get" enctype="multipart/form-data"  autocomplete="off">
                                <div class="row">

     <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Recruiter Name </label>
                                        <div class="col-sm-8">
      
            <input type="text" name="user" id="country" class="form-control" placeholder="Enter user Name"  autocomplete="off" value="{{
                      app('request')->input('user') }}" />  



                <div id="countryList" class="autogen"></div> 
          
          
          </div>
                                    </div>
                                </div>


 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Start Date </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" value="{{
                      app('request')->input('doj') }}" id="dojnew" name="doj" class="document"  /> </div>
                                    </div>
                                </div>

 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">End Date </label>
                                        <div class="col-sm-8">
                                             <input class="form-control" type="text" value="{{
                      app('request')->input('doend') }}" id="doend" name="doend" class="document" />
                                    </div>
                                </div>
</div>
<div class="col-lg-6">
                              
                            <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Spoc Name </label>
                                        <div class="col-sm-8">
      
            <input type="text" name="spoc" id="countryspoc" class="form-control" placeholder="Enter Spoc Name"  autocomplete="off" value="{{
                      app('request')->input('spoc') }}" />  



                <div id="countryListspoc" class="autogen"></div> 
          
          
          </div>
                                    </div>
</div>
<div class="col-lg-6">
                              
                            <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Client Name </label>
                                        <div class="col-sm-8">
      
            <input type="text" name="client"  class="form-control" placeholder="Enter Client Name"  autocomplete="off" value="{{
                      app('request')->input('client') }}" />  



                
          
          
          </div>
                                    </div>
</div>
<div class="col-lg-6">
                              
                            <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Position Name </label>
                                        <div class="col-sm-8">
      
            <input type="text" name="position"  class="form-control" placeholder="Enter Position Name"  autocomplete="off" value="{{
                      app('request')->input('position') }}" />            
          
          </div>
                                    </div>
</div>
                              
                                 <!-- <div class="col-lg-6">
                                 <div class="form-group row">

                                        <div class="col-sm-12">
                                        
                                        </div>
                                        
                                          <div class="col-sm-12">
                                       
                                        </div>
                                    </div>
                                    
                                  
                                 
                                    </div> -->
                                 </div>

                                </div>



  <div class="md-input-wrapper">
                                <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>
                                
                                <a href="{{URL::to('joinedcandidate')}}"  class="btn btn-primary waves-effect waves-light">Cancel</a>
                            </div>

                                
                      
                        
                                </form>
                                <br>

 <form action="excellistjoinedcandidatedownload"  method="post" enctype="multipart/form-data">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      
                        <input type="hidden" name="user" value="{{  app('request')->input('user') }}">
                          <input type="hidden" name="doj" value="{{ app('request')->input('doj') }}">
                            <input type="hidden" name="doend" value="{{app('request')->input('doend') }}">
                             <input type="hidden" name="spoc" value="{{app('request')->input('spoc') }}">
                            
                              <input type="hidden" name="_token" value="{{ csrf_token() }}">
                      <input type="submit" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel" value="Download Excel" name="downexcel">
                      </input>
                    </form>
                            </div>

                              <div class="col-lg-2">
                                 <div class="form-group row">
                                         <div class="col-sm-12">
                                         <label for="example-text-input" class="col-form-label form-control-label">Total joined </label>
                                     </div>
                                        <div class="col-sm-12">
<input class="form-control" type="text" value=" <?php echo count($users);?>" id="" name="" class="document" readonly/>
                                    </div>
                                    </div>
                                    
                                  
                                 
                                    </div>
                                
                           
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                  




<table id="example" class="display" width="100%" border="1"  >
<thead style="border: none;color: #fff;background: #43a060;">

  <tr>
      <th height="39" width="5%"  scope="col" >S.No.</th>
    <th height="39" width="15%"  scope="col" >Client Name</th>
    <th height="39" width="15%"  scope="col" >Position Name</th>
    <th height="39" width="15%"  scope="col" >Recruiter Name</th>
    <th height="39" width="15%"  scope="col" >Spoc Name</th>
    <th height="39" width="15%"  scope="col" >Candidate Name</th>
    <th height="39" width="15%"  scope="col" >Join Date</th>
       <th height="39" width="15%"  scope="col" >Offered Salary</th>
        <th height="39" width="10%"  scope="col" >Invoice</th>
    
  </tr>
  
  <!--   <th width="13%" scope="col">Yet To Joined
   </th>-->
   
 </thead>

	  <tbody>

<?php $i=1; ?>
 @if(!empty($users))
            @foreach($users as $value)
            <tr>
                <td>{{$i}}</td>

  <td>{{$value->comp_name}}</td>
  <td>{{$value->clientjob_title}}</td>
  <td>{{$value->recruitername}}</td>
  <td>{{$value->spocname}}</td>
  <td>{{$value->candidate_name}}</td>
  <td>{{$value->dateoj}}</td>
<td>{{$value->expectedsalery}}</td>
   <td>
   <?php if($value->invoicedet>0) { ?>
    <a style="color: #43a061;" data-toggle="modal" data-target="#myModalnn" onclick="getjoinlistupreport(<?php echo $value->client_id; ?>,<?php echo $value->id; ?>)" > Update Invoice  </a> 
    <a style="color: #FF6347;"  href="generateinvoicepdf/<?php echo $value->client_id; ?>_<?php echo $value->id; ?>" target='_blank'>
    Generate Pdf</a>
<?php } else {?> 
<a style="color: #0000FF;" data-toggle="modal" data-target="#myModal" onclick="getjoinlistreport(<?php echo $value->client_id; ?>,<?php echo $value->id; ?>)" > Raise Invoice  </a> 


<?php } ?>

</tr>
<?php $i++; ?>

 @endforeach
            @endif
            
              </tbody>
         </table>  
<div class="container">
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Insert Invoice</h4>
        </div>
           <div class="modal-body" >
            <form action="invoiceadd" method="post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
               <div id="recruiter"></div>
    </form>
        </div>   
      </div>      
    </div>
  </div> 
</div>


 <div class="container">
   <div class="modal fade" id="myModalnn" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Update Invoice</h4>
        </div>
           <div class="modal-body" >
            <form action="invoicupdate" method="post">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
               <div id="recruiternn"></div>
    </form>
        </div>   
      </div>      
    </div>
  </div> 
</div>

<script>  
 $(document).ready(function(){  
      $('#country').keyup(function(){

   //   alert("biii");
                 var query = $(this).val();  
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('searchusers')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                      // alert(data);
                         $('#countryList').fadeIn();  
                          $('#countryList').html(data);
                          
  
                      }
                    });
                    
                    
              /*  $.ajax({  
                     method:"POST",  
                     url:"searchusers",  
                    
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                     }  
                });  */
           }  
      });  
      $(document).on('click', 'ul.list-unstyled li', function(){  
     //   alert('hiii');
    // alert($(this).text());
           $('#country').val($(this).text());  
           $('#countryList').fadeOut();  
      });  
 });  
 </script>  
 <script>  
 
function getjoinlistupreport(fk_id,id)
{
    var useridd = fk_id;
    var idd = id;
      $.ajax({
        type:"GET",
        url:"getjoinuplistreport/" + useridd + "/" + idd ,
        data:'',
        success: function(data){
         // alert(data);
            $("#recruiternn").empty();
            $("#recruiternn").append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
} 
function sum() {
            var txtFirstNumberValue = document.getElementById('txt1').value;
            var txtSecondNumberValue = document.getElementById('txt2').value;
            var f1=((txtFirstNumberValue*txtSecondNumberValue)/100);
            var result =parseInt(((txtFirstNumberValue*txtSecondNumberValue)/100));
            if (!isNaN(result)) {
                document.getElementById('txt3').value = result;
            }
        }
function getjoinlistreport(fk_id,id)
{
    var useridd = fk_id;
    var idd = id;
      $.ajax({
        type:"GET",
        url:"getjoinlistreport/" + useridd + "/" + idd ,
        data:'',
        success: function(data){
         // alert(data);
            $("#recruiter").empty();
            $("#recruiter").append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}  

 $(document).ready(function(){  
      $('#countryspoc').keyup(function(){

   //   alert("biii");
                 var query = $(this).val();  
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('searchusersspoc')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                      // alert(data);
                         $('#countryListspoc').fadeIn();  
                         
                          $('#countryListspoc').html(data);  
  
                      }
                    });
           }  
      });  
      $(document).on('click', 'ul.list-unstyl li', function(){  
     //   alert('hiii');
    // alert($(this).text());
           $('#countryspoc').val($(this).text());  
           $('#countryListspoc').fadeOut();  
      });  
 });  
 </script>  

        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">

    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>


<script>
     $(document).ready(function () {  
          $('#doend').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>
<script>
     $(document).ready(function () {  
          $('#dojnew').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>

 <script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable({
      "pageLength": 50
    });
  } );
</script>
    
 <script>
     
function getresumelistreport(fk_id,fk_from,fk_fromdate,fk_todate)
{
    var useridd = fk_id;
  
    var fromid = fk_from;
      var fromdate = fk_fromdate;  
      var todate = fk_todate;

    $.ajax({
        type:"GET",
        url:"getresumesadmindatareport/" + useridd + "/" + fromid + "/" + fromdate+ "/" + todate ,
        data:'',
        success: function(data){
         // alert(data);
            $("#recruitermm").empty();
            $("#recruitermm").append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}   



   


</script>

@endsection




