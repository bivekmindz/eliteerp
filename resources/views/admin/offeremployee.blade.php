<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Invoice</title>
</head>

<body>
 <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <script type="text/javascript" src="js/numbers2words.min.js"></script>
  <!-- CSS for the things we want to print (print view) -->
<script language="javascript">
  function PrintDiv() {    
           var divToPrint = document.getElementById('divToPrint');
           var popupWin = window.open('', '_blank', 'width=766,height=300');
           popupWin.document.open();
           popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
            popupWin.document.close();
                }
</script>
</body>
 <?php 
if($users->emp_employement=='1' && $users->emp_payroll=='1') {
  $employement="Employment with Elite  HR PRACTICES";
 $employementcomp="Elite  HR PRACTICES";
   $payrolldata="";}
elseif($users->emp_employement=='1' && $users->emp_payroll=='2') {
  $employement="Employment with  Mindz Technology"; $payrolldata="You Will Be Working On Payroll Of Elite HR PRACTICES";$employementcomp="Mindz Technology";}
elseif($users->emp_employement=='2' && $users->emp_payroll=='1') {
  $employement="Employment with  Elite  HR PRACTICES"; $payrolldata="You Will Be Working On Payroll Of  Mindz Technology";$employementcomp="Elite  HR PRACTICES";}
  elseif($users->emp_employement=='2' && $users->emp_payroll=='2') {
  $employement="Employment with  Mindz Technology";$employementcomp="Mindz Technology"; $payrolldata="";}

else {$employement="Mindz Technology";}
  ?>
       
<button style="color: #ffff;border: none;padding: 5px 10px 5px 10px;background: #929292;"  onClick="PrintDiv();"> Print </button>
<div  id="divToPrint">

<table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: 'Bai Jamjuree', sans-serif;">
  <tr>
    <td>
    
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;     background-color: #f3f3f3;
    padding: 21px;">

  <tr>
    <td height="69" colspan="2" align="center"><p><strong>EMPLOYMENT  LETTER</strong></p></td>
    </tr>
  <tr>
    <td width="74%" height="26"><strong>PRIVATE AND CONFIDENTIAL </strong></td>
    <td width="26%">&nbsp;</td>
  </tr>
  <tr>
    <td rowspan="4" style="font-size:14px;"><p> <?php echo $users->name; ?><br />
       <?php echo $users->emp_address; ?>  </p></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><strong>Emp Code</strong>:  <?php echo $users->emp_empid; ?></td>
    <td align="right"><strong>Date: </strong> <?php echo date('d-m-Y'); ?></td>
  </tr>
  <tr>
    <td colspan="2"><p><strong>Re: <?php echo $employement;  ?>.</strong></p></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2"><p>Dear  <?php echo $users->name; ?>,</p></td>
    </tr>
  <tr>
    <td colspan="2"><p>In  accordance with our recent discussions, this letter will confirm that the  following shall be the terms and conditions of your employment with <strong><?php echo $employementcomp;  ?>.</strong> (Hereinafter referred  as the &ldquo;Company&rdquo;).<strong></strong></p></td>
    </tr>
  
  <tr>
    <td colspan="2">
      
      
     <h1 style=" font-size:14px;">1 . POSITION</h1>
      <ol>
      <li>You are appointed as<strong> <?php echo $users->emp_designation; ?> </strong>with effect from <strong><?php echo 
date("d-m-Y", strtotime( $users->emp_doj));?>. </strong>  <?php echo $payrolldata; ?></li>
      </ol>
      
       <h1 style=" font-size:14px;">2 . PROBATION</h1>
      <ol>
      <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">You will be on probation for a period of 6 months from the date of joining.  Period of probation is subject to extension at the discretion of the management of the Company, by another period of 2 months. Confirmation will not be construed to have taken place unless you are informed in writing by the Company to that effect.</li>
      <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">During the period of probation, your services may be terminated without any notice and/or without assigning any reason thereof.  Similarly, during the period of probation, you can leave the job by giving 15days notice to the Company.  However, the formalities of submitting proper letter of resignation and obtaining No-dues certificate from all concerned shall have to be complied with. </li>
       <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">After confirmtion notice period will be minimum 30 days and can be extended as per work.</li>
      </ol>
      
      
       <h1 style=" font-size:14px;">3 . ROLES ANDRESPONSIBILITIES</h1>
      <ol>
      <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">Subject to your qualifications and background, the Company reserves the right to assign any suitable duties (apart from the duties enumerated in Offer letter) which are in the interest of the business of the Company, prior to or during the course of your employment.  It is hereby understood and agreed by you that you will not refuse the above mentioned assignment on any ground and that you will not be entitled to any additional compensation for effectively carrying out the duties, which in the opinion of the management, of the Company is equivalent to the position you have been assigned.  </li>
      <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">In the course of performing your responsibilities you shall regularly report to the officer appointed by the Company providing them with such information and assistance as may be required by them from time to time.</li>
      </ol>
      
      <h1 style=" font-size:14px;">4 . PLACE OF WORK</h1>
      
      <ol>
       <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">You will initially be based at WZ-34/3, Asalatpur, A-3 Janakpuri, New Delhi, 110058.  However, the Company reserves the right to relocate you with prior notice of at least one week to any other place in India and/or from one department to another or from one establishment to another and/or to any other concern in which the Company may be having any interest whether existing or which may be set up in future at the sole discretion of the Company, by ensuring that your conditions of service and compensation are not adversely affected by such relocation. </li>
       
       <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">It is understood and acknowledged that as aforesaid, you may, from time to time, due to exigencies of business, be required to travel to any place within or outside the country as directed by the Company and undertakes to faithfully and diligently and in a manner consistent with sound business practice perform your duties at such place.</li>
     
      </ol>
      
      <h1 style=" font-size:14px;">5 . WORKING HOURS</h1>
      <ol>
        <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">Standard working hours in a week are 48 hours from Monday to Saturday .Any variations on working patterns are at the sole discretion of the Company.  If required you may need to extend your working hours to meet the requirement of your job without any additional payment.  In case of continuous delay or habitual absence in reporting for duty on time, the Company is entitled to take disciplinary action against you including but not limited to termination of your service.</li>
      </ol>
    
    
    <h1 style=" font-size:14px;">6 . COMPENSATION</h1>
    <ol>
      <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">Your total CTC will be Rs.  <?php echo $users->emp_salary; ?> per month, payable by 7th working day of the next English calendar month for the previous month.</li>
      <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">All payments made to you will be subject to deductions required by law including deductions in respect of tax and such other deductions and/or contributions as shall be required to be made pursuant to the applicable laws and policies and procedures of the Company.</li>
      <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">Any future increments or promotion or any other salary increase shall be based on merit considering your periodic and consistent overall performance, business conditions and other parameters fixed from time to time. Any increase in the salary will be at the sole discretion of the management and shall not be considered merely as a matter of right</li>
      <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">You unequivocally agree not to disclose or discuss your CTC including terms of employment and compensation package with any other employee, vendor, customer or business associate of the Company except as may be required under the laws or as may be required by the company in the course of your employment as it is confidential.  You fully understand that any deviation from maintaining such confidentiality on the part of the Employee would be deemed a material breach under the terms of this Agreement.</li>
    </ol>
    
    <h1 style=" font-size:14px;">7 . TRAVEL</h1>
    
    <ol>
      <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">As stipulated in Clause 4.2 you may be required to travel within or outside India with regard to the Company’s business requirements. The mode and class of travel and any associated expenses including accommodation must be approved by the Company before the travel is undertaken by you as per the Company’s policies and procedures.</li>
    </ol>
     <h1 style=" font-size:14px;">8 . REIMBURSEMENT OF EXPENSES</h1>
     
     <ol>
       <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">You shall be reimbursed for all reasonable expenses incurred in the performance of your duties by the Company, provided you submit all the appropriate and adequate supporting documents of such expenses to the Company including the vouchers for the expenses incurred, and the same has been approved as per the Company’s procedures and policies. The Company always reserves the right to ask you for any additional information or documents and to fill and sign any form or document, with regard to the reimbursements.</li>
     
     </ol>
      <h1 style=" font-size:14px;">9 . LEAVE</h1>
      
      <ol>
       <li style="line-height: 27px; margin-bottom: 15px; font-size:14px;">You will be entitled to Twelve (12) days of leave during your continuous service of twelve months. This will be given to you as per the priorities of the company. Please inform your Director 14 days in advance if you plan to take more than 1 leave.</li>
       <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">In case you are in need of extending the leave beyond the sanctioned period you must submit the application to your Director 72 hours in advance, in writing and the manager (upon receipt of such letter) shall inform in writing whether the extension of leave has been approved.</li>
       <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">On termination of your employment, the Company may at its sole discretion allow you to take during your notice period any leave entitlement which will have accrued by the date of the termination of your employment but which has not been availed.</li>
       
      </ol>
      
       <h1 style=" font-size:14px;">10 . DUTIES AND OBLIGATIONS OF THE EMPLOYEE</h1>
       
       <ol>
         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">In addition to the duties and obligations specified in herein you shall:
           
           <ul>
             <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">abide by all the Company’s rules, regulations, policies and procedures framed by the Company from time to time and applicable to the Position, which may include without limitation matters of attendance, conduct, behavior, discipline, working hours, holidays and other duties and obligations;</li>
             <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">efficiently, honestly and diligently discharge and perform all your duties and functions pertaining to your employment as also such other duties as the Employee may be required to perform from time to time by the Company, or by any duly authorized officer of the Company, which are consistent with the Employee's employment;</li>
             <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">obey and comply with all lawful orders, instructions and directions given to the Employee by the Company or by any person duly authorized by the Company in that behalf and faithfully obey all the norms and arrangements of the Company for the time being in force and applicable to the Employee for the management of the Company's property or for the control and good conduct of the Company’s employees;</li>
             <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">Immediately upon your knowledge, inform the Company of any act of dishonesty and/or any action prejudicial to the interests of the Company on the part of any other employee of the Company.</li>
           </ul>
         
         </li>
         
         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">It is understood that this employment is being offered to you on the basis of the particulars submitted by you.  If at any time during your employment with the Company, if it emerge that the particulars furnished by you are false/incorrect or if any material or relevant information has been suppressed or concealed, this appointment shall be considered ineffective and irregular and would be liable to be terminated by the Company forthwith without notice, without prejudice to the Company’s right to take disciplinary action against you for the same. </li>
         
         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">Further, it is agreed that while working as an employee if you enter into any business transaction with any party on behalf of the Company within your permissible limits, it shall be your responsibility to ensure recovery of outstanding. If any outstanding remains at the time of leaving the services of the Company, it shall be your responsibility to recover for remittance to the Company before you proceed to settle your legal dues in full and final settlement of your account</li>
       </ol>
       
       
       <h1 style=" font-size:14px;">11 . ASSIGNMENT AND PLEDGE OF BENEFITS</h1>
       
        <ol>
         
          <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;"> You shall neither assign nor pledge to any third party, for any reason whatsoever, any financial or other benefits to which you are entitled under this employment.</li>
        
        </ol>
       
       <h1 style=" font-size:14px;">12 . ACCEPTANCE OF GIFTS</h1>
       
        <ol>
         
          <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">During the term of your employment, you shall not, without the prior written permission of the management of the Company, accept or undertake to accept, either directly or indirectly, any gifts, commission or other favor of any kind whatsoever in connection with your employment with the Company.</li>
        
        </ol>
       
       <h1 style=" font-size:14px;">13 . CONFIDENTIALITY</h1>
       
       
         <ol>
         
          <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">You shall keep confidential at any time during or after your employment, any Information (including proprietary or confidential information) about the business and affairs of, or belonging to the Company or any subsidiary of the Company or their respective customers or suppliers, including information which, though technically not trade secrets, the dissemination or knowledge whereof might prove prejudicial to any of them.</li>
          
          <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">For he said purposes "Confidential Information" means any and all confidential information provided by the Company to the Employee or to which the Employee has access owing to your relationship with the Company, including but not limited to (i) Intellectual Property information; (ii) trade secrets; (iii) proprietary information related to the current, future, and proposed products and services of the Company including, without limitation, ideas, samples, media, techniques, sketches, drawings, works of authorship, models, inventions, know-how, processes, apparatuses, equipment, algorithms, software programs, software source documents and formulae, its information concerning research, experimental work, development, design details and specifications, engineering, financials, procurement requirements, purchasing, customer lists, investors, employees, business and contractual relationships, business forecasts, sales and merchandising, marketing plans, and any such information the Employee has access to regarding third parties; (iv) information relating to salary structures, perquisites and/or other terms and conditions of employment; and (iv) such other information which by its nature or the circumstances of its disclosure is confidential.</li>
          
          <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">As an employee to the company you had access to confidential and proprietary information of the company, you are counseled that after resignation as per the law you are automatically prohibited an ex-employee from disclosing or using “trade secrets” or “equivalent high level confidential information”, regardless of whether this is specifically addressed in the employment agreement itself. 
           Under applicable law and under the terms of your confidentiality agreement with the company, you are required to keep all such information confidential and not to use it to the detriment of the company. In particular you may not use or disclose any confidential information related to the company. To any new employer or competitor of the company.or interfere with the company’s existing clients or contracts directly or in directly. Any unauthorized disclosures can and may lead to litigations against you and the future employer.
</li>
        
        </ol>
       
       <h1 style=" font-size:14px;">14 . DATA PROTECTION</h1>
       
       <ol>
         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">By accepting this letter, you acknowledge and agree that the Company is permitted to hold personal information about you as part of its personnel and other business records and may use such information in the course of the Company's business. You agree that the Company may disclose such information to third parties in the event that such disclosure is in the Company's view required for the proper conduct of the Company's business or that of any associated company. This Clause applies to information held, used or disclosed in any medium.</li>
       </ol>
       <h1 style=" font-size:14px;">15 . COMPANY AND CLIENT PROPERTY</h1>
       
       <ol>
        <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">All equipment (including computer equipment), notes, memoranda, records, literature, publication, type set, lists of customers, suppliers and employees, correspondence, computer and other discs or tapes, data listings, codes, keys and passwords, designs, drawings, and other documents or material whatsoever (whether made or created by you or otherwise and in whatever medium or format) relating to the business of the Company or a group company or any of its or their clients (and any copies of the same) shall be and remain the property of the Company or the relevant client and be handed over by you to the Company on demand and in any event on the termination of your employment failing which the Company will deduct money for missing or damaged things from your full and final settlement of dues.</li>
       </ol>
       
       <h1 style=" font-size:14px;">16 . INTELLECTUAL PROPERTY</h1>
       
       <ol>
         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">The Company will own and be entitled to the benefit of all Intellectual Property in all material made/ discovered/ enhanced by either company or you in pursuance of the terms of your employment.</li>
         
         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">For the said purposes "Intellectual Property" means (i) all inventions (whether patentable or un-patentable and whether or not reduced to practice), all improvements thereto, and all patents, patent applications, and patent disclosures, together with all re-issuances, continuations, continuations-in-part, revisions, extensions and re-examinations thereof; (ii) all trademarks, service marks, logos, trade names and corporate names, together with all translations, adaptations, derivations and combinations thereof, including all goodwill associated therewith and all applications, registrations and renewals in connection therewith; (iii) all copyrightable works, all copyrights and all applications, registrations and renewals in connection therewith; (iv) all computer software (including data and related documentation), code, machine code, source code, related documentation, graphics, images, designs, logos, programs, layouts and specifications; (v) all other proprietary rights of whatsoever description whether or not protected and whether or not capable of protection, and (vi) all copies and tangible embodiments thereof regardless of form and medium.</li>
       </ol>
    
       
       <h1 style=" font-size:14px;">17 . NON COMPETE, NON SOLICIT AND EXCLUSIVITY</h1>
       
       <ol>
         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">You categorically agree that during the subsistence of your employment and for a period of 6 months after termination thereof and/or during your notice period, you shall not, for any reason whatsoever participate or render services, either directly or indirectly, in any company engaged in similar to or competes with the business of the Company. May it also be clear that under no circumstances, should any employee communicate directly or indirectly, with any of the Company’s clientele? If there is a situation where is arises then legal action will be taken.</li>
         
         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">You shall not for the duration your employment, and for the period thereafter, solicit, directly or indirectly, the customers, employees, consultants and contractors of the Company for your own interests.</li>
         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">You shall render your services on an exclusive basis and shall not, during the subsistence of this Agreement engage in any other business, trade or profession on a part-time or whole time basis without the prior specific written consent of the Company.</li>
       </ol>
       
       
       <h1 style=" font-size:14px;">18 . ADHERENCE TO POLICY </h1>
       <ol>
        <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">You shall, at all times, adhere to the provisions of the Staff Regulations and other policies and regulations, including any amendments made thereto from time to time.</li>
       </ol>
       
        <h1 style=" font-size:14px;">19 . ADHERENCE TO POLICY </h1>
        <p>Your employment shall terminate upon the occurrence of any of the following events:</p>
        <ol>
          <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">The Company may terminate your services at any time without Cause (as defined hereunder) from the position in which you are employed hereunder upon not less than 15 days prior written notice to the employee or compensation in lieu thereof. However, the Company may with Cause immediately terminate your services if you commit any material breach of any of the terms of employment letter.  In such termination cases where you are found to be in material breach of any of the terms of this letter, no compensation will be paid in lieu. Such termination shall not affect the rights and remedies that the Company may have under any other law for the time being in force. </li>
          
          <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">Upon any removal described in Clause 20.1 (i), after the effective date of such termination, no further payments shall be due, except that you shall be entitled to any amounts earned, accrued or owing but not yet paid under for services previously rendered and any benefits due in accordance with the terms of any applicable benefit plans and programs of the Company.  It is agreed that the Company shall have no liability for severance payments, damages or similar payments resulting from the termination of Employee's employment for any reason whatsoever.</li>
          
          <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">You may voluntarily terminate his employment for any reason upon providing prior written notice to the Company, the period of which shall be one month prior written notice or salary in lieu of notice.  Provided, if you are in the middle of an assignment, the Company may require you to complete all operative parts of the assignment, as determined by the Company before agreeing to relieve you from the services. <br />


However, in extenuating circumstances, the Company may in its sole discretion require you to serve a shorter notice period or waive your obligation to serve the notice period as provided herein.  In such event, after the effective date of such termination, no further payments shall be due under this Agreement, except that you shall be entitled to any amounts earned, accrued or owing but not yet paid for services previously rendered and any benefits due in accordance with the terms of any applicable benefit plans and programs of the Company.
</li>

   <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">For the purposes of the said clause "Cause" means:
    <ul>
     
     <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">The Employee’s material breach of this Agreement; </li>
     <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">The Employee’s negligence in the performance of the Employee's duties hereunder, non-performance or mis-performance of such duties, or refusal to abide by or comply with lawful directives, the Employee’s superior officers, or the Company's policies and procedures; </li>
     
     <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">The Employee’s dishonesty, fraud, or misconduct with respect to the business or affairs of the Company; </li>
     
     <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">Employee's conviction of any crime involving moral turpitude; or </li>
     <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">Employee's abuse of alcohol or drugs (legal or illegal) that, in the Company's reasonable judgment, materially impairs the Employee's ability to perform the Employee's duties hereunder.</li>
     
     
    </ul>
   </li>
        </ol>
        
        
        <h1 style=" font-size:14px;">20 . CONSEQUENCES UPON TERMINATION </h1>
        
        <ol>
          <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">Upon termination of your employment with the Company for any reason whatsoever, you shall not later than the effective date of termination:
            
            <ul>
              <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">hand over charge to such person or persons as may be nominated by the Company in that behalf, and</li>
              <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">surrender to the management of the Company or any person nominated/authorized by it, all original and copies of business documents, manuals, blueprints, reproductions or any data, tables, calculations, diaries, notes or books and correspondence either addressed to you by the Company or received by you for and on behalf of the Company without making any copies thereof and/or extracts there from and all property (i.e. residential premises, keys, software, laptop/computer, vehicle, mobile phone and simcard, documents, marketing materials, video camera, etc.) belonging to the Company which are in your possession  or custody or connected with the business of the Company or any subsidiary, associate or affiliate of the Company. </li>
            </ul>
          </li>
          
          <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">Without prejudice to the Company's other rights and remedies, the Company shall be entitled to deduct from the Employee's emoluments, the amount of any claims, if any, which the Company may have against the Employee. The full and final, if any, would only be processed after 30 days from date of separation. </li>
        </ol>
      
      
       <h1 style=" font-size:14px;">21 . RETIREMENT</h1>
       <ol>
         
         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">It is understood and agreed that the age of retirement fixed by the Company is 60 years.  Consequently, unless otherwise terminated in accordance with Clause 20, your employment will terminate upon your attaining the age of 60 years.</li>
       
       </ol>
       
         <h1 style=" font-size:14px;">22 . SERVICE OF NOTICE</h1>
       <ol>
         
         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">All notices shall be in writing and shall be served by sending the same by registered post acknowledgement due and/or by courier and/or by e-mail 
           
           <ul>
             <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">In the case of the Company at its registered office and</li>
             <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">In your case to and at</li>
           </ul>
           
           <p></p>
           
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="36%"><strong>Name :</strong></td>
    <td width="64%"><?php echo $users->name; ?></td>
  </tr>
  <tr>
    <td><strong>Address :</strong></td>
    <td><p><?php echo $users->emp_address; ?></p></td>
  </tr>
  <tr>
    <td><strong>Number :</strong></td>
    <td><?php echo $users->emp_contactno; ?></td>
  </tr>
  <tr>
    <td><strong>Email ID  :</strong></td>
    <td><?php echo $users->emp_personalemailid; ?></td>
  </tr>
           </table>
           

         
         </li>
         
         <p></p>
         <ul> 
             <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">All notices or other communications shall be deemed to have been validly given on (a) the expiry of 7 (seven) days after posting if transmitted by registered post, or (b) the date of receipt if transmitted by courier, or (c) the business date immediately after the date of transmission with confirmed answer back if transmitted by e-mail whichever shall first occur.<br />

The Company and you may, from time to time, change their address or representative for receipt of notices or other communications provided herein by giving to the other not less than 30 days prior written notice to
that effect. Any communication sent to you by the Company on the last known address communicated in writing by you shall be deemed to have been duly served notwithstanding the fact that you changed your address subsequently.
</li>
             
           </ul>

         
         
       
         
       
       
       </ol>
         <h1 style=" font-size:14px;">23 . ADDITIONAL RULES & REGULATIONS</h1>
         <ol>
           <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">In addition to the terms and conditions of employment hereinabove mentioned, you shall also observe and comply with and shall be bound by any rules, regulations, policies and procedures which the Company may from time to time separately frame for observance and compliance by its employees and which are communicated by the Company to you in writing.</li>
         </ol>
         
         
           <h1 style=" font-size:14px;">24 . MISCELLANEOUS</h1>
         <ol>
           <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;">You will submit to the Company true copies of all the certificates / mark sheets, including relieving letter & salary certificate from your present employer and other documents, at the time of joining for verification or maximum within one week of your joining. At the time of joining or at any time during your tenure with the Company, if any discrepancy or incorrect information is found in your certificates or any other documents/application furnished by you, your appointment with the Company will stand cancelled.</li>
           
           <ul>
            <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;"><strong>Reservation of Rights</strong><br />

No forbearance, indulgence, relaxation or inaction by the Company at any time, to require performance of any of the provisions hereunder shall, in any way, affect, diminish or prejudice its right to require performance of that provision and any waiver or acquiescence of any breach of any of the provisions herein shall not be construed as a waiver or acquiescence of any continuing or succeeding breach of such provisions or a recognition of rights and/or positions other than as expressly stipulated herein.
</li>

         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;"><strong>Partial Invalidity</strong><br />

If any provision provided herein is held to be invalid or unenforceable to any extent, the remaining provisions shall not be affected and shall continue to be valid and enforceable to the fullest extent permitted by law.  Any invalid or unenforceable provision of shall be replaced with a provision that is valid and enforceable and most nearly reflects the original intent of the unenforceable provision.
</li>
         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;"><strong>Relationship</strong><br />

None of the provisions herein shall be deemed to constitute a partnership between the parties hereto and no party shall have any authority to bind the other party otherwise.
</li>
         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;"><strong>Conflict</strong><br />

In the event of there being any inconsistency or repugnancy between the provisions contained in the Company's Staff Regulations and this letter, the provisions contained in this letter shall prevail.
</li>
         <li style=" line-height: 27px; margin-bottom: 15px; font-size:14px;"><strong>Governing Law and Jurisdiction</strong><br />

Any dispute shall be governed and construed in accordance with the laws of India and shall be subject to the jurisdiction of the courts in New Delhi.
<strong>IN WITNESS WHEREOF THE PARTIES HERETO HAVE PUT THEIR RESPECTIVE SEAL AND/OR HANDS THE DAY AND YEAR FIRST HEREINABOVE WRITTEN</strong>.
</li>
           </ul>
         </ol>
         
         
         <p>Signed and delivered on behalf of the within named Company</p>
       
       
    </td>
    </tr>
    
    <tr>
    <td><p><strong>Neha Arora</strong><br />
      <strong>HR-Manager</strong></p>   </td>
    <td align="right">&nbsp;</td>
  </tr>
    <tr>
      <td>&nbsp;</td>
      <td align="right"><p align="right"><strong>(Acceptance)</strong><br />
        <strong><?php echo $users->name; ?></strong></p></td>
    </tr>

    </table>

    
    </td>
   
  </tr>

</table>

</body>
</html>

</div>