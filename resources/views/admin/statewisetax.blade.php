@extends('Emp.layouts.master')

@section('content')
  

    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
    
                <h4>State Wise Tax</h4>
                
            </div>
        </div>



        <div class="row">

            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <!-- <h5 class="card-header-text">Client Listing</h5> -->

                        <a href="addstatetax" class="btn btn-success waves-effect waves-light" >Add </a>


                  
                        @if(Session::has('message'))
                            <div class="alert alert-success">
                                {{ Session::get('message') }}
                            </div>
                        @endif

                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 ">
                            
                             <div class="tablle-all">
                             <div class="tab-spok">
                             
                                <table class="table" id="">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>State Name</th>
                                        <th>CGST</th>
                                        <th>SGST</th>
                                        <th>IGST</th>
                                        <th>Status</th>
                                        <th>Edit </th>
                                    </tr>
                                    </thead>
                                        <tbody id="suggesstion-box">
                                        @php
                                        $i=1;
                                        @endphp
                                        @if(!empty($dashbd))
                                        @foreach($dashbd as $kk => $c)
                                        <tr>
                                        <td>{{  $i++   }}</td>
                                        
                                        <td>{{  $c->state_name }}</td>
                                        <td>{{  $c->state_cgst }}</td>
                                        <td>{{ $c->state_sgst }}</td>
                                        <td>{{  $c->state_igst }}</td>
                                       <td>  @if($c->state_status==0)
                                   <a title=" Activate Now" href="{{ route('admin.reactivestate',$c->state_id) }}" class="btn btn-primary">Inactive </a>
                                   @elseif($c->state_status==1)
                                   <a title="Click For Inactive" href="{{ route('admin.deletestate',$c->state_id) }}" class="btn btn-primary">Active </a>
                                   @endif</td>
                                        <td> <a title="Click For Edit" href="{{ route('admin.editstate',$c->state_id) }}" class="btn btn-primary">Edit </a> &nbsp;&nbsp;<a title="Click For Edit" href="{{ route('admin.deletestateval',$c->state_id) }}" class="btn btn-primary">Delete </a></td>
                                      
                                        </tr>
                                    @endforeach
                                    @endif
                                    </tbody>
                                    </table>
                               </div>
                            </div>
                        </div>


    </div>
    </div>
    </div>
    </div>
    
       
    </div>


@endsection