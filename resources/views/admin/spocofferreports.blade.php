@extends('admin.layouts.master')
@section('content')

          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
    <div class="container-fluid">
        
        <style>
            .autogen{    width: 100%;
    float: left;
    max-height: 137px;
    overflow-y: auto;
    z-index: 999;
    background-color: #fff;}
    .autogen ul {padding: 0px;
    line-height: 35px; border: 1px solid  #e5e5e5;}
     .autogen ul li {padding: 0px 8px; border-bottom: 1px solid #e5e5e5;}
     .autogen ul li:last-child{ border-bottom:none;}
        </style>
           <style>

button
{

  background-color:#fff;
  border:none;
  cursor:pointer;
}

button:hover
{

  background-color:#fff;
  border:none;
  cursor:pointer;
  color:green;
}


</style>

        

        <div class="row">
            <div class="main-header">
                <h4>Spoc Offer  Report</h4>
             
            </div>
        </div>
        

       
            <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Spoc Offer Report</h5>
                           
                        </div>
                        <div class="card-block">


                              <div class="card-block">
                                 <form action=""  method="get" enctype="multipart/form-data"  autocomplete="off">
                                <div class="row">
<?php if(($usersval[0]->emp_role)!='4') { } else { ?>
     <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">User </label>
                                        <div class="col-sm-8">
      
            <input type="text" name="user" id="country" class="form-control" placeholder="Enter user Name"  autocomplete="off"  />  



                <div id="countryList" class="autogen"></div> 
          
          
          </div>
                                    </div>
                                </div>

<?php } ?>
 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Start Date </label>
                                        <div class="col-sm-8">
                                            <input class="form-control document" type="text" value="" id="dojnew" name="doj"/> </div>
                                    </div>
                                </div>

 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">End Date </label>
                                        <div class="col-sm-8">
                                             <input class="form-control document" type="text" value="" id="doend" name="doend" />
                                    </div>
                                </div>
                                
                                 </div>


<!-- 
<div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Clients</label>
                                        <div class="col-sm-8">
                                                <input type="text" name="clients" id="clients" class="form-control" placeholder="Enter Client Name"  autocomplete="off" />  



                <div id="ClientList" class="autogen"></div> 
                                    </div>
                                </div>
                                
                                 </div> -->

                                </div>

  <div class="md-input-wrapper">
                                 <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>  
                                
                                <a href="Spocofferreports"  class="btn btn-primary waves-effect waves-light">Cancel</a>
 
                            </div>




                                 



                                </form>


<!-- Script -->
 
                            </div>

     
</div>

                            <div class="row">
                                <div class="col-sm-12 table-responsive" >



  <div id="topdf" style="width: 100%;overflow: hidden;">
        <table width="100%" id="totable"  border="1">
           <tr><td >


<!-- Content Area -->

          


  @if(!empty($data))
            @foreach($data as $value)

   <?php 

   $subm=0;
 if(count($value['positions'])>0) {
?>

<table width="100%" border="1" cellspacing="1" cellpadding="1"  >

<tr>
    <td width="10%" style="border:none;" height="30" align="left"><strong style="color:#1f2ccc;">{{ $value['username']}},</strong></td>
    <td colspan="9" style="border:none;"><p>&nbsp;</p>
    <p>&nbsp;</p></td>
   
  </tr>
  
  <tr>
     <th scope="col" >Position</th>
    <th height="39"   scope="col">Client</th>
    
    <th  scope="col"  style='text-align: center'>Candidate Name
   </th>
      <th  scope="col" style='text-align: center'> Candidate Email
    </th>
  </th>
      <th  scope="col" style='text-align: center'>Candidate Phone No
    </th>
   </th>
      <th  scope="col" style='text-align: center'>Total Experience
    </th>
       </th>
      <th  scope="col" style='text-align: center'>Salary
    </th>
      <th  scope="col" style='text-align: center'>Offer Date
    </th>
     <th  scope="col" style='text-align: center'>Status
    </th>
          <th  scope="col" style='text-align: center'>Action
    </th>
  </tr>
 


<?php foreach($value['resumes'] as $keyus => $k){?>
<?php if($value['resumes'][$keyus]->cv_status ==4){
                      $val = 'Offered';
                        $style = '';
                    }
                    else if($value['resumes'][$keyus]->cv_status ==6){
                      $val = 'Joined';
                        $style = 'border: 0px solid green; padding: 6px; color: green; font-weight: bold';
                    }?>
<tr  style="<?php echo $style;?>">
 <td>{{$value['positions'][$keyus]->clientjob_title }} </td>
  <td>{{$value['clients'][$keyus]->comp_name }} </td>
   <td>{{$value['resumes'][$keyus]->candidate_name }} </td>
    <td>{{$value['resumes'][$keyus]->candidate_email }} </td>
     <td>{{$value['resumes'][$keyus]->candidate_mob }} </td>
      <td>{{$value['resumes'][$keyus]->total_exp }} </td>
         <td>{{$value['resumes'][$keyus]->expectedsalery }} </td>
       <td>{{$value['resumes'][$keyus]->dateoj }} </td>
       <td><span>{{$val}} </span></td>

         <td>
          <?php if($value['resumes'][$keyus]->cv_status ==4){ ?>
                      <button type='button'  class='det_css' data-toggle='modal' data-target='#myModal' onclick='getposiionvalue(<?php echo $value['resumes'][$keyus]->id; ?>)' >Update</button>  
                    
                    
                      
                    <?php }?>
<!--
   <button type='button'  class='det_css' data-toggle='modal' data-target='#myModalview' onclick='getresumelistreport(<?php echo $value['resumes'][$keyus]->id; ?>)' >View All Updates</button>     



   <button type='button'  class='det_css' data-toggle='modal' data-target='#myModalview' onclick='getresumelistreport(<?php echo $value['resumes'][$keyus]->id; ?>)' >Follow</button>  -->   

         </td>

</tr>

<?php } ?>


</table>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<?php } ?>
    @endforeach
            @endif
       </td></tr>
       </table>
    </div>     
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
             </div>
             
             
</div>
             
             
             
            
            </div>
            
            

<div class="container">
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Status</h4>
        </div>

           <form method="post" action="{{ URL::to('updatespockdata') }}">
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="form-group">
                          <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">
Add Status
                          </label>
                     <select name="status" class="form-control">
                            <option value="6">Join</option>
                          </select><br>
                     <input type="date" name="doj" class="form-control" required=""> 
                            <input type="hidden" name="clientreq_posid" id="placepopupval" >
                          <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                       
                     
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-default" value="Submit" >
            </div>
        </form>



        <div id=""></div>
      
      </div>      
    </div>
  </div> 

  </div>

     <div class="container">
   <div class="modal fade" id="myModalview" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Display All Updates</h4>
        </div>
           <div class="modal-body" >
        <form method="post" action="{{ URL::to('updatespocfollowup') }}">
           <div class="modal-body" >
       <table width="90%" border="1" cellspacing="0" cellpadding="0">
           <thead>
       
            </thead>
            <tbody id="recruiter">
          
            </tbody>
             
            
        </table>
          <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-default" value="Submit" >
        </div>   
               </form>
        </div>   
      </div>      
    </div>
  </div> 
</div>





        <script>

function getresumelistreport(fk_id)
{
    var useridd = fk_id;
 

    $.ajax({
        type:"GET",
        url:"getresumesrecuiterofferreport/" + useridd ,
        data:'',
        success: function(data){
          //  alert(data);
            $("#recruiter").empty();
            $("#recruiter").append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}  


          function getposiionvalue(id)
{

 var ids = id;
   document.getElementById("placepopupval").value = id;   

}


            function upload_position_jd(clientjob_id) {
                $.ajax({
                    url: 'adminpositionjd',
                    data: { id:clientjob_id },
                    success: function(result){
                        $("#positionJd").html(result);
                    }
                });
                $('#myModal').modal('show');
            }
    </script>
     <script>
    </script>

        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">

    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>
<script>  
 $(document).ready(function(){  
      $('#clients').keyup(function(){  
           var query = $(this).val();  
          // alert("hiii");
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('searchclients')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                      // alert(data);
                         $('#ClientList').fadeIn();  
                          $('#ClientList').html(data);  
                      }
                    });
                    
                    
              /*  $.ajax({  
                     method:"POST",  
                     url:"searchusers",  
                    
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                     }  
                });  */
           }  
      });  
      $(document).on('click', 'ul.list-unstyledclients li', function(){  
          $('#clients').val($(this).text());  
           $('#ClientList').fadeOut();  
      });  
 });  
 </script>  


<script>  
 $(document).ready(function(){  
      $('#country').keyup(function(){

   //   alert("biii");
                 var query = $(this).val();  
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('searchusers')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                      // alert(data);
                         $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                      }
                    });
                    
                    
              /*  $.ajax({  
                     method:"POST",  
                     url:"searchusers",  
                    
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                     }  
                });  */
           }  
      });  
      $(document).on('click', 'ul.list-unstyled li', function(){  
     //   alert('hiii');
    // alert($(this).text());
           $('#country').val($(this).text());  
           $('#countryList').fadeOut();  
      });  
 });  
 </script>  
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">

    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>


<script>
     $(document).ready(function () {  
          $('#doend').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>
<script>
     $(document).ready(function () {  
          $('#dojnew').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>


@endsection



