@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <!-- Textual inputs starts -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">First Work Employee Data</h5>
                    </div>
                    @if(Session::has('success_msg'))
                        <div class="alert alert-success">
                            {{ Session::get('success_msg') }}
                        </div>
                    @endif
                    @if(Session::has('error_msg'))
                        <div class="alert alert-danger">
                            {{ Session::get('error_msg') }}
                        </div>
                    @endif
                   
                </div>
            </div>

            <!-- Textual inputs ends -->
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                          <a href="excelattendancedownload">
                                                <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel">Download Excel
                                                    <span class="badge"><i class="icofont icofont-file-excel"></i></span>
                                                </button>
                                            </a>
                        <form action="" method="get">
                        <input class="form-control" type="text" value="" id="dojnew" name="doj" class="document"/>

                        <input type="submit" name="formsubmit">
                        <h5 class="card-header-text"></h5>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 table-responsive">
                                   <div class="table-responsive table_green siro">       
                            <table class="table table-bordered" id="search_filter">
                           
                                    <thead>
                                    <tr>
                                        <th>Emp id</th>
                                         <th>Emp Code</th>
                                        <th>Name</th>
                                          <th>Punching  time</th>
                                        <th>Login  time</th>
                                        <th>Work Start Time</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $i = 0;
                                    @endphp
                                    @foreach($users as $key => $val)
                                    <tr >
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $val->empcode }}</td>
                                        <td>{{ $val->empname }}({{ $val->usersloginname }})</td>
                                        
                                        <td>@php
                                            $vpunchrec=explode(',', $val->punchrecord)
                                            @endphp

                                            {{ $vpunchrec[0] }}</td>
                                            <td>{{ $val->userslogintime }}</td>
                                             <td>{{ $val->clientstart_starttime }}</td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div> </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>




    </div>


   <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>
<script>
     $(document).ready(function () {  
          $('#dojnew').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>

<script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script>
@endsection