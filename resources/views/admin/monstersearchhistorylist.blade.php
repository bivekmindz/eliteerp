@extends('admin.layouts.master')

@section('content')
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#boot-multiselect-demo').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });
        });
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Monster Search History Details</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Monster Search History</a>
                    </li>
                   
                </ol>
            </div>
        </div>
        <div class="row">
             <div class="col-md-12">
                <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Monster Search History</h5>
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                  <div class="table-responsive table_green siro">       
 <table class="table table-bordered" id="suggesstion-box">
                                        <thead>
                                            <tr>
                                                <th>S. No.</th>
                                                <th>Employee Name</th>
                                                <th>Employee Email</th>
                                                <th>CAT ID</th>
                                                <th>Search Date</th>  
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach($result as $key => $val)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{  $val->name }}</td>
                                            <td>{{  $val->email }}</td>
                                            <td>{{  $val->md_catid }}</td>
                                            <td>{{  $val->md_createdat }}</td>
                                        </tr>
                                        @endforeach
                                       </tbody>
                                    </table>
                                </div>
                            </div>       </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection