@extends('admin.layouts.master')

@section('content')
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#boot-multiselect-demo').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });
        });
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Monster Today Search Employee Details</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Monster Search History</a>
                    </li>
                   
                </ol>
            </div>
        </div>
        <div class="row">
             <div class="col-md-12">
                <div class="card">
                    <form method="post" name="position" action="monsterfreekeybyadmin" novalidate="novalidate">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     
                        <div class="card-header">
                           <!--  <h5 class="card-header-text">Monster Search History</h5> -->
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Free Monster Keys</button>
                        </div>
                         @if(Session::has('success_msg'))
                        <div class="alert alert-success">
                            {{ Session::get('success_msg') }}
                        </div>
                    @endif
                    @if(Session::has('error_msg'))
                        <div class="alert alert-danger" style="color:#fff">
                            {{ Session::get('error_msg') }}
                        </div>
                    @endif
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                   <div class="table-responsive table_green siro">       
 <table class="table table-bordered" id="suggesstion-box">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <div class="check-box-i">
                                                    <div class="rkmd-checkbox checkbox-rotate">
                                                    <label class="input-checkbox checkbox-primary">
                                                      <input type="checkbox" id="checkAll">
                                                      <span class="checkbox"></span>
                                                    </label>
                                                    </div>
                                                    </div>
                                                </th>
                                                <th>S. No.</th>
                                                <th>Employee Name</th>
                                                <th>Employee Email</th>
                                                <th>CAT ID</th>
                                                <th>Search Date</th> 
                                                <th>Status</th>   
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach($result as $key => $val)
                                        <tr>
                                            <td><?php if($val->ma_status=='Active'){ ?>
                                            <div class="check-box-i">
                                            <div class="rkmd-checkbox checkbox-rotate">
                                            <label class="input-checkbox checkbox-primary">
                                              <input type="checkbox" name="indmon[]" value="{{  $val->ma_id }}">
                                              <span class="checkbox"></span>
                                            </label>
                                            </div>
                                            </div>
                                            <?php } ?></td>
                                            <td>{{ $i++ }}</td>
                                            <td>{{  $val->name }}</td>
                                            <td>{{  $val->email }}</td>
                                            <td>{{  $val->ma_catid }}</td>
                                            <td>{{  $val->ma_createdat }}</td>
                                            <td>{{  $val->ma_status }}</td>
                                        </tr>
                                        @endforeach
                                       </tbody>
                                    </table>
                                </div>  </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

<script type="text/javascript">
   $("#checkAll").click(function () {
     $('input:checkbox').not(this).prop('checked', this.checked);
 }); 

  
</script>
@endsection
