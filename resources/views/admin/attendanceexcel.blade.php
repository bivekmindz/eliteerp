@extends('admin.layouts.master')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <!-- Textual inputs starts -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Employee Attendance Excel Upload</h5>
                    </div>
                    @if(Session::has('success_msg'))
                        <div class="alert alert-success">
                            {{ Session::get('success_msg') }}
                        </div>
                    @endif
                    @if(Session::has('error_msg'))
                        <div class="alert alert-danger">
                            {{ Session::get('error_msg') }}
                        </div>
                    @endif
                    <!-- end of modal -->
                    <div class="card-block">
                            <div class="row">
                           


                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <form action="{{ route('excelattendanceup')  }}"  method="post" enctype="multipart/form-data">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Upload Excel</label>
                                            <div class="col-sm-8">
                                               <!-- <label for="file" class="custom-file">
                                                    <input type="file" id="file" name="upfile"> 
                                                    <span class="custom-file-control"></span>
                                                </label> -->
                                                   <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                   <input type="file" id="file" name="upattfile">
                                                <span id="lblError" style="color: red;"></span>
                                            </div>
                                    </div>
                                </div>
                            </div>


                            <div class="md-input-wrapper">
                                <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Textual inputs ends -->
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Employee Attendance  List</h5>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 table-responsive">
                                <div class="table-responsive table_green siro">       
                            <table class="table table-bordered" id="search_filter">
                                    <thead>
                                    <tr>
                                        <th>Emp id</th>
                                         <th>Emp Code</th>
                                        <th>Name</th>
                                        <th>Insert time</th>
                                        <th>Out Time</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $i = 0;
                                    @endphp
                                    @foreach($users as $key => $val)
                                    <tr>
                                        <td>{{ ++$i }}</td>
                                        <td>{{ $val->empcode }}</td>
                                        <td>{{ $val->empname }}</td>
                                        
                                        <td>@php
                                            $vpunchrec=explode(',', $val->punchrecord)
                                            @endphp

                                            {{ $vpunchrec[0] }}</td>
                                            <td>{{ $val->lastpunch }}</td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 </div>
        </div>




    </div>


    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>
<script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script> 

@endsection