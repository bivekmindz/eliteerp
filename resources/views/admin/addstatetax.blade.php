@extends('Emp.layouts.master')

@section('content')
<head>
    <meta charset="UTF-8">
    <title>bootstrap4</title>
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
    <style type="text/css">
        .parsley-errors-list li {color:#f00;}
        .red-star {
            color: red;
        }
        .poc_text {
    font-weight: normal;
    color: #409a5d;
    font-size: 15px;
    margin-bottom: 7px;
}
    </style>
    
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
</head>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
            
                <h4>Add Statewise Tax</h4>
              
            </div>
        </div>
        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h5 class="card-header-text">Add Statewise Tax</h5>

                    </div>
                    @if(Session::has('success_msg'))
                        <div class="alert alert-success">
                            {{ Session::get('success_msg') }}
                        </div>
                @endif

                <div class="card-block">
                    {!! Form::open([ 'action'=>'Admin\InvoiceController@savestatetax', 'data-parsley-validate','method'=>'post', 'files'=>true ]) !!}
                            @if(count($errors))
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.
                                    <br/>
                                    <ul>
                                        @foreach($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                           

                            <div class="form-group row {{ $errors->has('cname') ? 'has-error' : '' }}">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Country Name<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <select  name="cname" data-parsley-required-message ="Please Select Country Name" placeholder="Please Select Country Name" required class="form-control">

                                      <?php foreach ($country as $key => $cou): ?>
                                          
                                     <option value="<?php echo($cou->cid); ?>"><?php echo($cou->countryname); ?></option>
                                    <?php endforeach ?>
                                    </select>
                                   
                                    <span class="text-danger">{{ $errors->first('cname') }}</span>
                                </div>
                            </div>
                            
                            <div class="form-group row {{ $errors->has('statename') ? 'has-error' : '' }}">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">State Name<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="{{old('statename')}}" id="example-text-input" data-parsley-required-message ="Please Enter State Name" placeholder="Enter State Name" name="statename" required>
                                    <span class="text-danger">{{ $errors->first('statename') }}</span>

                                </div>
                            </div>



                            <div class="form-group row {{ $errors->has('cgst') ? 'has-error' : '' }}">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">CGST<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="{{old('cgst')}}" id="example-text-input" data-parsley-required-message ="Please Enter CGST" placeholder="Enter CGST" name="cgst" required>
                                    <span class="text-danger">{{ $errors->first('cgst') }}</span>

                                </div>
                            </div>

                             <div class="form-group row {{ $errors->has('sgst') ? 'has-error' : '' }}">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">SGST<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="{{old('sgst')}}" id="example-text-input" data-parsley-required-message ="Please Enter SGST" placeholder="Enter SGST" name="sgst" required>
                                    <span class="text-danger">{{ $errors->first('sgst') }}</span>

                                </div>
                            </div>



                            <div class="form-group row   {{ $errors->has('igst') ? 'has-error' : '' }}">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">IGST<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" data-parsley-required-message ="Please Enter IGST" placeholder="Enter IGST" value="{{old('igst')}}" id="example-text-input" name="igst" required>
                                    <span class="text-danger">{{ $errors->first('igst') }}</span>
                                </div>
                            </div>

                             

 

                            <div class="md-input-wrapper">
                                <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit">
                                </input>
                            </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> -->
<script src="http://parsleyjs.org/dist/parsley.js"></script>
<!--   <script type="text/javascript">
    
      $('#billingadd').summernote({ height: 40 });
</script>  -->
@endsection



