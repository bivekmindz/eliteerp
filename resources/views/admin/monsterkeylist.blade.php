@extends('admin.layouts.master')

@section('content')
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#boot-multiselect-demo').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });
        });
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Monster Keys Details</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Monster Details</a>
                    </li>
                   
                </ol>
            </div>
        </div>
        <div class="row">
             <div class="col-md-12">
                <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Monster Keys</h5>
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <div class="table-responsive table_green siro">       
 <table class="table table-bordered" id="suggesstion-box">
                                        <thead>
                                            <tr>
                                                <th>S. No.</th>
                                                <th>XCode</th>
                                                <th>Login ID</th>
                                                <th>CAT ID</th>
                                                <th>Expiry Date</th>
                                                <th>Vendor Kye</th>
                                                <th>Status</th>    
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach($keys as $key => $val)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{  $val->mk_xcode }}</td>
                                            <td>{{  $val->mk_login }}</td>
                                            <td>{{  $val->mk_catid }}</td>
                                            <td>{{  $val->mk_expiry }}</td>
                                            <td>{{  $val->mk_vendor_key }}</td>
                                            <td>{{  $val->mk_status }}</td>
                                        </tr>
                                        @endforeach
                                       </tbody>
                                    </table>
                                </div>
                            </div> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection