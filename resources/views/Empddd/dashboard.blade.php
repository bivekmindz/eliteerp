@extends('Emp.layouts.master')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>Dashboard</h4>
        </div>
    </div>
<?php $role = explode(',', Auth::user()->emp_role);
    //dd(Auth::user()->emp_role);
  
    ?>
    <!-- {{--@if($role == 1)--}}
        {{--<li>Insert Lists Here</li>--}}
    {{--@else if($role == 2)--}}
    {{--<li>Insert Lists Here</li>--}}
    {{--@else if($role == 3)--}}
    {{--<li>Insert Lists Here</li>--}}
    {{--@endif--}} -->

    <!-- 4-blocks row start -->
    <div class="row m-b-30 dashboard-header">
    @if(in_array(1, $role))
        <div class="col-lg-4 col-sm-6">
            <div class="dashboard-primary bg-primary">
                <div class="sales-primary">
                    
                    <div class="top-icon">
                      
                    <i class="icon-bubbles"></i>
                    <h1>{{ $tot_client[0]->clientcount }}</h1>
                    
                    <p>TOTAL CLIENTS</p>
                    </div>
                   
                </div>

               
                <div class="bg-dark-primary">
                 
                    <p><a href="{{ URL::to(''.'clientlists') }}">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">

                <div class="top-icon">

                 <i class="icon-basket-loaded"></i>
                 <h1>{{ $tot_assignclient[0]->assignclientcount }}</h1>
                 <p>TOTAL ASSIGN SPOC TO CLIENTS</p>


                </div>
                    
                    
                </div>
                <div class="bg-dark-warning">
                    
                    <p><a href="{{ URL::to(''.'clientspoc') }}">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                 <div class="top-icon">
                      
                   <!-- <i class="icon-clock"></i> -->
                    <h1></h1>
                    
                    <p>Client Listing</p>
                    </div>
                    <div class="dast-bot-ti">
                        <table>
                            <thead>
                            <tr>
                                
                                <th>Company Name</th>
                                <th>Created on</th>
                                 
                            </tr>
                            </thead>
                            <tbody>

                            @php
                            $i=1;
                            $temp = $adminclientdata ;
                            @endphp 
                            @foreach($adminclientdata as $kk => $c)

                            <tr>

                                
                                <td>{{ $c->comp_name }}</td>
                                <td>{{ $c->created_at }}</td>
                               </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                                        
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <!-- <p ><a href="{{ URL::to(''.'listallposition') }}">VIEW DETAILS</a></p> -->
                </div>
            </div>
        </div>
    @endif
    @if(in_array(2, $role))
        <div class="col-lg-4 col-sm-6">
            <div class="bg-success dashboard-success">
                <div class="sales-success">



                   <div class="top-icon">
                      
                    <i class="icon-speedometer"></i>
                    <h1>{{ $tot_spoc_assignclient[0]->spocclientcount }}</h1>
                    
                    <p>TOTAL ASSIGNED CLIENTS</p>
                    </div>


                   
                   
                </div>
                <div class="bg-dark-success">
                    
                    <p><a href="{{ URL::to(''.'assignclient') }}">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">


                   <div class="top-icon">
                      
                    <i class="icon-clock"></i>
                    <h1>{{ $tot_position[0]->spoctotpos }}</h1>
                    
                    <p>TOTAL ADDED POSITIONS</p>
                    </div>

                    
                   
                </div>
                <div class="bg-dark-facebook">
                   
                    <p><a href="{{ URL::to(''.'positionlist') }}">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">
                 <div class="top-icon">
                      
                   <i class="icon-basket-loaded"></i>
                    <h1>{{ $tot_assignpostion[0]->spoctotassignpos }}</h1>
                    
                    <p>TOTAL POSITION ALLOCATE TO RECRUITER</p>
                    </div>
                   
                   
                </div>
                <div class="bg-dark-warning">
                   
                    <p><a href="{{ URL::to(''.'viewallassignedrecuiterposition') }}">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
    @endif
    @if(in_array(3, $role))
        <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">

                  <div class="top-icon">
                      
                      <i class="icon-basket-loaded"></i>
                    <h1>{{ $tot_assignpostion_rec[0]->totassignpos_rec }}</h1>
                    
                    <p>TOTAL POSITION ASSIGNED TO RECRUITER BY SPOC</p>
                    </div>
                 
                  
                </div>
                <div class="bg-dark-warning">
                  
                    <p><a href="#">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-success dashboard-success">
                <div class="sales-success">
 <div class="top-icon">
                      
                    <i class="icon-speedometer"></i>
                    <h1>{{ $tot_recuit_cv[0]->recuit_cv }}</h1>
                    
                    <p>TOTAL CV</p>
                    </div>


                  
                </div>
                <div class="bg-dark-success">
                   
                    <p><a href="#">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                  <div class="top-icon">
                      
                   <i class="icon-clock"></i>
                    <h1>{{ $tot_recuit_cv_submission[0]->recuit_cv_submission }}</h1>
                    
                    <p>TOTAL SUBMISSION</p>
                    </div>
                    
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <p><a href="#">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
    @endif
    @if(in_array(4, $role))
    <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">
                  

                   <div class="top-icon">
                      
                   <i class="icon-basket-loaded"></i>
                    <h1>{{ $admin_tot_client[0]->adminclient }}</h1>
                    
                    <p>TOTAL CLIENTS</p>
                    </div>

                    
                  
                </div>
                <div class="bg-dark-warning">
                    
                    <p ><a href="{{ URL::to(''.'admin/adminclient') }}">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-success dashboard-success">
                <div class="sales-success">

                        <div class="top-icon">
                      
                   <i class="icon-speedometer"></i>
                    <h1>{{ $admin_tot_emp[0]->adminemp }}</h1>
                    
                    <p>TOTAL EMPLOYEES</p>
                    </div>

                    
                    
                </div>
                <div class="bg-dark-success">
                  
                    <p ><a href="{{ URL::to(''.'admin/viewadminemployes') }}">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">


                 <div class="top-icon">
                      
                 <i class="icon-clock"></i>
                    <h1>{{ $admin_tot_dept[0]->admindept }}</h1>
                    
                    <p>TOTAL DEPARTMENTS</p>
                    </div>



                    
                   
                </div>
                <div class="bg-dark-facebook">
                    
                    <p><a href="{{ URL::to(''.'admin/admindepartment') }}">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="dashboard-primary bg-primary">
                <div class="sales-primary">


                 <div class="top-icon">
                      
                   <i class="icon-bubbles"></i>
                    <h1>{{ $admin_tot_team[0]->adminteam }}</h1>
                    
                    <p>TOTAL TEAMS</p>
                    </div>
                    
                    
                </div>
                <div class="bg-dark-primary">
                    
                    <p><a href="{{ URL::to(''.'admin/team') }}">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                 <div class="top-icon">
                      
                   <i class="icon-clock"></i>
                    <h1>{{ $admin_tot_position[0]->adminposition }}</h1>
                    
                    <p>TOTAL POSITIONS</p>
                    </div>

                   
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <p ><a href="{{ URL::to(''.'listallposition') }}">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">


                  <div class="top-icon">
                      
                       <i class="icon-basket-loaded"></i>
                    <h1>{{ $admin_tot_recuit_cv[0]->admin_recuit_cv }}</h1>
                    
                    <p>TOTAL SHORTLISTED CV</p>
                    </div>

                 
                   
                </div>
                <div class="bg-dark-warning">
                   
                    <p><a href="{{ URL::to(''.'shortlistcv') }}">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>  

        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                 <div class="top-icon">
                      
                   <!-- <i class="icon-clock"></i> -->
                    <h1></h1>
                    
                    <p>Client Listing</p>
                    </div>
                    <div class="dast-bot-ti">
                        <table>
                            <thead>
                            <tr>
                                
                                <th>Company Name</th>
                                <th>Created on</th>
                                 
                            </tr>
                            </thead>
                            <tbody>

                            @php
                            $i=1;
                            $temp = $adminclientdata ;
                            @endphp 
                            @foreach($adminclientdata as $kk => $c)

                            <tr>

                                
                                <td>{{  $c->comp_name }}</td>
                                <td>{{ $c->created_at }}</td>
                               </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                                        
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <!-- <p ><a href="{{ URL::to(''.'listallposition') }}">VIEW DETAILS</a></p> -->
                </div>
            </div>
        </div>

    @endif    
    </div>
  
    
   <!--  <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Client Listing</h5>
                           
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 ">
                                
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>S.N</th>
                                            <th>Category</th>
                                            <th>Company Name</th>
                                            <th>Contact Person Name</th>
                                            <th>Designation</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                             <th>Created by</th>
                                             
                                        </tr>
                                        </thead>
                                        <tbody>

                                           @php
                                $i=1;
                                $temp = $adminclientdata ;
                            @endphp 
                                            @foreach($adminclientdata as $kk => $c)
                                        <tr>


                                <td> {{  $i++   }}</td>
                                <td> @if($c->client_catid==1)
                                         {{ "IT" }}
                                         @else
                                          {{ "NON IT"  }}
                                         @endif


                                </td>
                                <td>{{  $c->comp_name }}</td>
                                <td>{{  $c->contact_name }}</td>
                                <td>{{ $c->designation }}</td>
                                <td>{{  $c->phone }}</td>
                                <td>{{ $c->email }}</td>
                                <td>{{ $c->name }}</td>
                               </tr>
                                        @endforeach
                                        
                                        

                                        </tbody>
                                    </table>
                                </div>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
             </div>
             
             
             
            
            </div> -->

</div>



</div>



    @endsection
    
    
    