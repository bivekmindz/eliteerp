@extends('Emp.layouts.master')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View All Position Allocation To Recruiter</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Positions</a>
                </li>
                <li class="breadcrumb-item"><a href="">Position assigned to Recuriter</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">View All Position Assigned To Recruiter</h5>
                    <form action="{{ URL::to('excellistassignrecruiterdownload') }}"  method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel" value="Download Excel" name="downexcel">
                                             
                                            </input>
                                        </form>
                </div>
                @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
                @endif
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Position Name</th>
                                    <th>Company Name</th>
                                    <th>Drive</th>
                                    <th>No Of Positions</th>
                                    <th>Assigned Recuriter Name</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                $i=1;
                                @endphp
                                @foreach($pos as $key => $p)
                                <tr class="table-active">
                                    <td>{{ $i++ }}</td>
                                    <td>{{ !empty($p->clientjob_title) ? $p->clientjob_title : 'N/A' }}</td>
                                    <td>{{ !empty($p->comp_name) ? $p->comp_name : 'N/A' }}</td>
                                    <td>N/A</td>
                                    <td>{{ !empty($p->clientjob_noofposition) ? $p->clientjob_noofposition : 'N/A' }}</td>
                                    <td>{{ !empty($p->name) ? $p->name : 'N/A' }}</td>
                                    <td>
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal" onclick="getrecruiterlist(<?php echo $p->fk_empid;?>);">Edit</button>
                                    </td>

                </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
@endsection
<div class="container">
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Assigned Recruiter</h4>
        </div>
        <div class="modal-body">
           <form method="post" action="">
                <div class="form-group">
                  <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Select Recruiter:</label>
                  <select class="multiselect-ui form-control" id="recruiter" id="recruiter[]">
                                        
                  </select>
                </div>
           </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>      
    </div>
  </div> 
</div>

<script>
function getrecruiterlist(empid)
{
    var recruiter_id = empid;
    $.ajax({
        type:"GET",
        url:"getrecruiters/" + recruiter_id,
        data:'',
        success: function(data){
            $("#recruiter").empty();
            $("#recruiter").append(data);
        },
        error: function(data){

        }
    });
}
</script>