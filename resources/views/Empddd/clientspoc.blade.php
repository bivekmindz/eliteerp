@extends('Emp.layouts.master')

@section('content')


    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
              @inject('position', 'App\Component\CommonComponent')
    @php 
            $breadcrumb = $position->breadcrumbs();
            //print_r($breadcrumb); exit;
            //print_r($breadcrumb[0]->menuname); exit;
        @endphp
                <h4>Client Assigned to SPOC</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">{{$breadcrumb[0]->parentmenu}}</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">{{$breadcrumb[0]->menuname}}</a>
                    </li>
                </ol>
            </div>
        </div>



        <div class="row">

            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                    
                        <h5 class="card-header-text">Client With Assigned spoc</h5>
                        <form action="{{ URL::to('excellistclientspocdownload') }}"  method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel" value="Download Excel" name="downexcel">
                                             
                                            </input>
                                        </form>

                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12">
                            
                            <div class="tab-spok">
                                <table>
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <!-- <th>Category</th> -->
                                        <th>Company Name</th>
                                        <!-- <th>Contact Person Name</th> -->

                                        <th>Assigned SPOC</th>
                                        <th>Edit SPOC</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    @php
                                        $i=1;
                                    @endphp
                                    @foreach($clientemp as $kk => $clients)
                                        <tr >
                                            <td>{{  $i++   }}</td>
                                            <td>{{  $clients->comp_name }}</td>
                                            <td>{{ $clients->name }}</td>
                                            <td><div class="btn-group btn-group-sm" style="float: none;">
    <button onclick="return test('{{$clients->clientrole_id}}','{{$clients->comp_name}}'),getspocclient('<?php echo $clients->clientrole_id ?>');" type="button" href="#success" data-toggle="modal" class="tabledit-edit-button sl_refresh btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;"><span  class="icofont icofont-ui-edit"></span></button>
    </div>
    </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>




        </div> 
        <?php foreach($clientemp as $a=> $c){//echo "<pre>"; print_r($c->clientrole_id);}?>


         @if(!empty($c))
       

    <div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header modal-header-success">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h1><i class="glyphicon glyphicon-thumbs-up"></i>Assign SPOC</h1>
    </div>
    <div class="modal-body">
    <form action="{{ route('updateassignspoc') }} " method="POST"  name="assign" >
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group row">
    <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Company Name</label>
    <div class="col-sm-9">
    <input class="form-control" type="text"  id="pop_company_name" name="cname" id="cname" value=" " >
    <input type="hidden"  id="client_id" name="client_id"  value="" >
    </div>
    </div>


 <div class="form-group row">
    <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Employee List</label>
      <div class="col-sm-9">
       <div class="form-group">
         <select id="empid"   name="empid[]" required>
 </select>
    <span class="text-danger">{{ $errors->first('empid') }}</span>



    </div>

    </div>

    </div>

    </div>

    <div class="modal-footer">

    <div class="md-input-wrapper">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <input type="submit" class="btn btn-primary waves-effect waves-light" value="submit" onclick="checkdata('{{$c->clientrole_id}}')">
    </input>

    </div>
    </div>

    </form>
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @endif
<?php }?>


        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>


<script type="text/javascript">
    

            function test(id,c_name){
                // alert("c_name");
                $.ajax({
                    url: "{{route('clientspoc')}}",
                    type: "GET",
                    data: 'data='+id,
                    beforeSend: function() {
                        //alert('vandana1');

                    },
                    success: function(response) {

                        $('#pop_company_name').val(c_name);
                        $('#client_id').val(id);
                        ('.fetched-data').html(response);
                    },
                    error: function(xhr) {
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    }
                });



            }

            function getspocclient(id)
            { 

                $.ajax({
                    url: 'getspoctoclient/'+id,
                    type:'GET',
                    error: function(xhr) {
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    },
                    success: function(data)
                    {
                        // alert(data);
//                    debugger;
                        $('#empid').html(data);

                        $('#empid').multiselect('rebuild');

                        //alert(data);
                    }
                });
            }

</script>




        <script>


            




            $(function() {
                $("form[name='assign']").validate({
                    rules: {
                        empid: {
                            required: true,
                        },
                    },
                    messages: {
                        empid: "Please Select Spoc",
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }
                })
            });





            

function getemployee(cid){
                var cid = cid;
                $.ajax({
                    url: 'getemployeebyclientid/'+cid,
                    type:'GET',
                    error: function(xhr) {
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    },
                    success: function(data)
                    {

                        $('#empid').html(data);
                        $('#empid').multiselect('rebuild');

                    }
                });
            }

function checkdata(clientid)
            {

                var eid=$('#empid').val();


                if(eid!='')
                {
                    $.ajax({
                        url: 'updateassignspoc',
                        type: 'POST',
                        data:  $('assign').serialize(),
                        success: function(data)
                        {



                        }
                    });

                }
                else
                {


                }


            }

            
            

        @endsection