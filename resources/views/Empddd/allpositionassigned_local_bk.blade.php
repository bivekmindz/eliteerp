@extends('Emp.layouts.master')

@section('content')

    @if(!empty($getlists->clientreq_id))

        <style>
            .ih{background-color: #fff;
                border: 1px solid #bdbdbd;
                /* padding: 15px 0px; */
                height: 34px;}
            .ihi{margin: 5px 0px 27px 13px;}
            endTime{
                display: none;
            }
        </style>
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <h4>Postion Resume Upload</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Postion </a>
                        </li>
                        <li class="breadcrumb-item"><a href="">Postion List</a>
                        </li>
                    </ol>
                </div>
            </div>
            <div class="row">
            <!-- {{$sessionstart}} -->
                @if(Session::has('success_msg'))
                    <div class="alert alert-success">
                        {{ Session::get('success_msg') }}
                    </div>
                @endif




                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text full_widt">

                                {{--<div  id="showtimer"> Uploading Time will be over in  <span id="time">{{$sessionstart}}</span> minutes!</div>--}}

                                <a href="#" class="start_align">
                                    <div class="">
                                        <input class="btn btn-danger col-md-12 col-sm-12 col-xs-12 endTime" id="endTime" name="endTime" type="button" value="stop" onclick="stop();">
                                    </div>
                                </a>

                            <!-- {{$sessionstart}} -->

                                <?php
                                if($datecustid==$getlists->clientreq_id) {

                                if($sessionstart>=0) { ?>
                            <!-- <a href="#">
<button type="button" id="showtimer"  class="star_tim">
<i class="icofont icofont-clock-time"></i>
<span id="time"></span>
</button>
</a> -->



                                <div class="form-group timer align_right">
                                    <span id="time"></span>
                                </div>
                                <?php } else {  ?>

                                <a href="#" class="start_align">
                                    <div class="">
                                        <input class="btn btn-success col-md-12 col-sm-12 col-xs-12" id="startTime" name="startTime" type="button" value="start" onclick="start();">
                                    </div>
                                </a>

                                <div class="form-group timer align_right">
                                    <span id="time"></span>
                                </div>
                                <?php } } else {   ?>

                                <a href="#" class="start_align" data-toggle="modal" data-target="#myModal">
                                    <div class="">
                                        <input class="btn btn-success col-md-12 col-sm-12 col-xs-12" id="startTime" name="startTime" type="button" value="start"/>


                                    </div>
                                </a>

                                <?php } ?>
                            </h5>
                        </div>
                        <div class="container">
                          <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">
                              <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Reason For Delay</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="pwd">Reason</label>
                                            <input type="text" class="form-control" id="reasonForDelay" name="reasonForDelay">
                                        </div>
                                    </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal" onclick="start();">Submit</button>
                                </div>
                              </div>
                            </div>
                          </div> 
                        </div>
                       <div>

                            <table class="table dt">
                                <thead>
                                <tr>
                                    <th>Candidate Name</th>
                                    <th>Candidate Email</th>
                                    <th>Candidate Mobile</th>
                                    <th>Resume</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($getuploadCv))
                                    @foreach($getuploadCv as $key => $uploads)
                                        <tr class="table-active entry">
                                            <td>
                                                <div class="form-group required">
                                                    {{ $uploads->candidate_name }}
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group required">
                                                    {{ $uploads->candidate_email }}
                                                </div>
                                            </td>
                                            <td>
                                                <label class="form-group">
                                                    {{$uploads->candidate_mob}}
                                                </label>
                                            </td>
                                            <td>
                                                <div class="form-group required">
                                                    {{ $uploads->cv_name }}
                                                </div>
                                            </td>

                                        </tr>

                                    @endforeach
                                @endif

                                </tbody>
                            </table>


                        </div>

                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Company Name </th>
                                            <td>{{ !empty($getlists->comp_name) ? $getlists->comp_name : 'N/A'  }}</td>
                                            <th>Position Name</th>
                                            <td style="border-bottom: 2px solid #eceeef;">
                                                <span class="posname">{{ !empty($getlists->clientjob_title) ? $getlists->clientjob_title : 'N/A' }}</span>
                                            </td>
                                        </tr>
                                        <td colspan="2">
                                            <div class="arro_css">
                                                {{ !empty($getlists->upload_position_jd) ?  htmlspecialchars_decode($getlists->upload_position_jd)  : 'N/A'  }}
                                            </div>
                                        </td>
                                        </thead>

                                    </table>

                                    <!-- <div class="md-input-wrapper flo op timeset" style="display: none">

                                    <button type="submit" class="btn btn-primary waves-effect waves-light">
                                    <span class="icofont icofont-eye-alt"></span> Upload CV
                                    </button>
                                    </div> -->
                                </div>

                            </div>
                            <?php if($sessionstart>=0) { ?>
                            <div class="col-sm-12 table-responsive timeset">

                                {!! Form::open([ 'action'=>'Emp\RecController@uploadcv', 'method'=>'post', 'files'=>true   ]) !!}



                                <div class="controls">

                                    <table class="table dt">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Phone No</th>
                                            <th>Email</th>
                                            <th>Upload Resume</th>
                                            <th>Highest Qualificatione</th>
                                            <th>Total Exp</th>
                                            <th>Domain Exp</th>
                                            <th>Primary Skill</th>
                                            <th>Secondary Skill</th>
                                            <th>Current ORG</th>
                                            <th>Current CTC</th>
                                            <th>Expected CTC</th>
                                            <th>NP</th>
                                            <th>Relocation</th>
                                            <th>Expected DOJ</th>
                                            <th>Remarks</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="table-active entry">

                                            <td>
                                                <div class="form-group required">
                                                    <input type="text" pattern="[A-Za-z\s]+" required="" name="candidate_name[]" class="form-control" placeholder="Enter First Name" title="First Name should only contain  letters. e.g. John" maxlength="60">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group required">
                                                    <input type="tel" name="candidate_mob[]" pattern="^\d{10}$" required=""  class="form-control" placeholder="Enter Your Mobile">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group required">
                                                    <input type="text" required="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50" name="candidate_email[]" class="form-control" placeholder="Enter Your Email" >
                                                </div>
                                            </td>
                                            <td>
                                                <label class="custom-file ih">
                                                    <input type="file" id="file" class="ihi" name="cv_name[]" required width="150px">
                                                </label>
                                            </td>
                                            <td>
                                                <div class="form-group required">
                                                    <input type="text" name="highest_qualificatione[]" required="" class="form-control" placeholder="Enter Highest Qualificatione">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group required">
                                                    <input type="tel" name="experience[]" required="" class="form-control"  maxlength="10" placeholder="Enter Total experience">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group required">
                                                    <input type="text" name="domaine_exp[]" required="" class="form-control" placeholder="Enter Domain Experience" maxlength="50">
                                                </div>
                                            </td>
                                            <td>
                                                <label class="custom-file ih">
                                                    <select class="form-control" required="" name="primary_skill[]">
                                                        <option value="php">PHP</option>
                                                    </select>
                                                </label>
                                            </td>
                                            <td>
                                                <label class="custom-file ih">
                                                    <select class="form-control" required="" name="secondary_skill[]">
                                                        <option value="NET">.NET</option>
                                                    </select>
                                                </label>
                                            </td>
                        
                                            <td>
                                                <div class="form-group required">
                                                    <input type="text"required=""  name="current_org[]" class="form-control" placeholder="Enter Current ORG" maxlength="60">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group required">
                                                    <input type="text" required="" name="current_ctc[]" class="form-control" placeholder="Enter Current CTC" >
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group required">
                                                    <input type="text" required="" name="expected_ctc[]" class="form-control" placeholder="Expected CTC">
                                                </div>
                                            </td>
                                            <td>
                                                <label class="custom-file ih">
                                                <input type="text" name="np[]" required="" class="form-control" placeholder="Enter NP">
                                                </label>
                                            </td>
                                            <td>
                                                <div class="form-group required">
                                                    <select name="relocation[]" class="form-control" >
                                                        
                                                        <option value="Yes"> Yes</option>
                                                        <option value="No"> No </option>
                                                        <option value="Other">Other  </option>
                                                    </select>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group required">
                                                    <input type="text" name="doj[]" id="doj" class="form-control" placeholder="Date Of Joining">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <textarea name="remark[]" class="form-control" placeholder="Remark" required="" maxlength="">
                                                    </textarea>
                                                </div>
                                            </td>
                                
                                            <td><span class="add_field">
                                            <button class="btn btn-success btn-add" type="button">
                                            <span class="icofont icofont-plus"></span>
                                            </button>
                                            </span></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div class="md-input-wrapper dt">
                                        <button type="submit" class="btn btn-default waves-effect">Cancel
                                        </button>
                                        <input type="hidden" name="fkjdid" value="{{ !empty($getlists->fk_jdid) ? $getlists->fk_jdid : '0'  }}">
                                        <input type="hidden" name="clientreqid" value="{{ !empty($getlists->clientreq_id) ? $getlists->clientreq_id : '0' }}">
                                    
                                        @php

                                            $clientEncryptId = Crypt::encrypt($getlists->clientreq_id);
                                            
                                        @endphp
                                        <input type="hidden" name="clientreqEncrypt" value="{{ !empty($positionid) ? $positionid : '0' }}">

                                        <input type="submit" class="btn btn-primary waves-effect waves-light" value="submit">

                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>


                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card" >
                        <div class="card-header">
                            <h5 class="card-header-text">Client Preview</h5>
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th scope="row" style="width:23%;">Company Name</th>
                                            <td>{{ !empty($getlists->comp_name) ? $getlists->comp_name : 'N/A ' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Company URL</th>
                                            <td>{{ !empty($getlists->company_url) ? $getlists->company_url : 'N/A' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row" >Company Size </th>
                                            <td>+{{ !empty($getlists->company_size) ? $getlists->company_size : 'N/A' }}</td>
                                        </tr>

                                        <tr>

                                            <td colspan="2">

                                                <div class="md-input-wrapper flo">

                                                    <button type="submit" class="btn btn-primary waves-effect waves-light"><span class="icofont icofont-eye-alt"></span> View More
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="table hid" style="">
                                        <tr>
                                            <th scope="row" style="width:23%;">Timings</th>
                                            <td>{{ !empty($getlists->from_time) ? $getlists->from_time : 'N/A' }} To  {{ !empty($getlists->to_time) ? $getlists->to_time : 'N/A' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Days Working</th>
                                            <td>{{ !empty($getlists->days_working) ? $getlists->days_working : 'N/A' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Industry </th>
                                            <td>{{ !empty($getlists->industry) ? $getlists->industry : 'N/A' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Company’s Specialties</th>
                                            <td>{!! !empty($getlists->selling_point) ? $getlists->selling_point : 'N/A' !!}</td>
                                        </tr>

                                        <tr>
                                            <th scope="row">Headquarters</th>
                                            <td>{{ !empty($getlists->headquarters) ? $getlists->headquarters : 'N/A' }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Office Address</th>
                                            <td>{{ !empty($getlists->office_address) ? $getlists->office_address : 'N/A' }}</td>
                                        </tr>
                                    </table>


                                </div>


                            </div>


                        </div>
                    </div>
                </div>

            </div>

        </div>


        <script>
            function stopTime()
            {
                $('#showtimer').hide();
                $('#stopTime').hide();
            }
        </script>


        <script type="text/javascript">
            $(function(){

                $('#uploadcv').on('submit',function(e){
//alert('sjkdsjkdbsjkbd');
                    $('.endTime').css('display','block');
                    $('.timeset').css('display','block');
                    $.ajaxSetup({
                        header:$('meta[name="_token"]').attr('content')
                    })
                    e.preventDefault(e);

                    $.ajax({

                        type:"POST",
                        url:'/uploadcv',
                        data:$(this).serialize(),
                        dataType: 'json',
                        success: function(data){
// console.log(data);
// $("#suggestionbox").html(data);

                        },
                        error: function(data){

                        }
                    })
                });

            });
        </script>
        @if($datecustid==$getlists->clientreq_id) {

        <script type="text/javascript">
            //var sstartAt = {{$sessionstart}};
            var sstartAt = 0;
        </script>
        @else
            <script type="text/javascript">
                var sstartAt = 0;
            </script>
        @endif
        <script type="text/javascript">
            //alert(sstartAt);
            //var sstartAt = 20;
            //alert(sstartAt);
            var clsStopwatch = function () {

                var startAt = sstartAt;
                var lapTime = 0;


                var now = function () {
                    return (new Date()).getTime();
//alert((new Date()).getTime());
                };

                this.start = function () {
                    // alert(now());
                    startAt = startAt ? startAt : {{$datedataact}};
                };

                this.stop = function () {
                    lapTime = startAt ? lapTime + now() - startAt : lapTime;
                    startAt = sstartAt;
                };

                this.time = function () {
                    return lapTime + (startAt ? now() - startAt : 0);

                };
            };

            var x = new clsStopwatch();
            //alert(x.time);
            var $time;
            var clocktimer;
            //alert($time);
            function pad(num, size) {

                var s = "0000" + num;
                return s.substr(s.length - size);
            }

            function formatTime(time) {
                var h =  s = ms = 0;
                var m=sstartAt;
                var newTime = '';
//alert(time);
//time=1520673834;
                h = Math.floor(time / (3600 * 1000));
                time = time % (3600 * 1000);
                m = Math.floor(time / (60 * 1000));
                time = time % (60 * 1000);
                s = Math.floor(time / 1000);
                ms = time % 1000;


//alert(m);
                newTime = pad(h, 2) + ':' + pad(m, 2) + ':' + pad(s, 2);
//newTime = pad('07', 2) + ':' + pad('08', 2) + ':' + pad('09', 2);
//newTime = pad(h, 2) + ':' + pad(m, 2) + ':' + pad(s, 2) + ':' + pad(ms, 2);
                return newTime;
//alert(newTime);
            }
            //alert(newTime);
            function show() {
                //  alert(document.getElementById('time'));
                $time = document.getElementById('time');
                update();
            }

            function update() {
                // alert(formatTime(x.time()));
                $time.innerHTML = formatTime(x.time());
            }


            function start() {
                var reason = $("#reasonForDelay").val();
                if(!reason){
                    alert('Please enter reason for delay.');
                    return false;
                }
                
                clocktimer = setInterval("update()", 1);
//alert(clocktimer); exit;
                x.start();

//alert(x.start()); exit;
                $('.timeset').css('display','block');
                $('.posNavName').removeAttr("href");
                alert('start uploading form');
                $('#startTime').hide();
                $('#endTime').show();
                $('.align_right').show();
                mail_after_one();
//alert(clocktimer); exit;
//alert(x.time()); exit;
                $.ajax({
                    type:"POST",
//  url:'http://127.0.0.1:8000/gettime/1',
                    url:'gettime/{{ $getlists->clientreq_id }}',
// http://127.0.0.1:8000/get-assigned-postion/gettime/1
                    data:{'reason':reason,
                        '_token': "{{ csrf_token() }}",
                    },
// dataType: 'json',
                    success: function(data){

                       location.reload(true);
                    },
                    error: function(data){

                    location.reload(true);
                    }

                });


            }

            function millisecondsToHours(amountMS) {
                return amountMS / 3600000;
            }

            function stop() {
                x.stop();
                alert('end uploading form');
                $('#endTime').hide();
                $.ajax({

                    type:"GET",
//  url:'http://127.0.0.1:8000/endtime/1',
                    url:'endtime/{{ $getlists->clientreq_id }}',
                    data:'',
//  dataType: 'json',
                    success: function(data){

                        location.reload(true);
                    },
                    error: function(data){
                        location.reload(true);
                    }
                });
                document.getElementById('counter').value = millisecondsToHours(x.time());
                clearInterval(clocktimer);
                location.reload(true);

            }

            //Make timer element clickable so user can manually change timer
            $('span#time').bind('dblclick', function() {
                $(this).attr('contentEditable', true);
            }).blur(
                function() {
                    $(this).attr('contentEditable', false);
//Need to add ability here when user clicks off time to convert to millisecond
                });


            //plugin to make any element text editable
            $.fn.extend({
                editable: function () {
                    $(this).each(function () {
                        var $el = $(this),
                            $edittextbox = $('<input type="text"></input>').css('min-width', $el.width()),
                            submitChanges = function () {
                                if ($edittextbox.val() !== '') {
                                    $el.html($edittextbox.val());
                                    $el.show();
                                    $el.trigger('editsubmit', [$el.html()]);
                                    $(document).unbind('click', submitChanges);
                                    $edittextbox.detach();
                                }
                            },
                            tempVal;
                        $edittextbox.click(function (event) {
                            event.stopPropagation();
                        });

                        $el.dblclick(function (e) {
                            tempVal = $el.html();
                            $edittextbox.val(tempVal).insertBefore(this)
                                .bind('keypress', function (e) {
                                    var code = (e.keyCode ? e.keyCode : e.which);
                                    if (code == 13) {
                                        submitChanges();
                                    }
                                }).select();
                            $el.hide();
                            $(document).click(submitChanges);
                        });
                    });
                    return this;
                }
            });
            //implement editable plugin
            /*$('span#time').editable().on('editsubmit', function (event, val) {
            });*/

        </script>
        <script>
            function mail_after_one()
            {
                        var path =  "{{ $getlists->clientreq_id }}";
                        $.ajax({
                            type:"GET",
                            url:"sendAutomaticMail/"+path,
                            data:'',

                            success: function(data){
                                console.log(data);
                               // alert(data);
//location.reload(true);
                            },
                            error: function(data){

                            }
                        });
            }

            setInterval(function()
            {mail_after_one()},60*60*1000);
        </script>

        <script>
            localStorage.setItem('clocktime',({{$sessionstart}}));
            var storageTime = localStorage.getItem('clocktime');
            //alert(storageTime);
        </script>

        @if($datecustid==$getlists->clientreq_id) {

        <script>


            clocktimer = setInterval("update()", 1);
            //alert(clocktimer); exit;
            x.start();
            //alert(x.start());
            $('.timeset').css('display','block');
            $('.posNavName').removeAttr("href");
            //alert('start uploading form');
            $('#startTime').hide();
            $('#endTime').show();
            $('.align_right').show();
            //alert(x.start);
            //alert(clocktimer);
            //alert(x.time()); exit;


        </script>

  <script>
  $( function() {
    $( "#doj" ).datepicker();
  } );
  </script>
        @endif



    @else
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <h4>Postion Resume Upload</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Postion </a>
                        </li>
                        <li class="breadcrumb-item"><a href="">Postion List</a>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        No ID Found
                    </div>
                </div>
            </div>
        </div>


    @endif
@endsection

<style>
.form-control{width:150px!important}
textarea{width:200px!important; height:75px!important}
</style>





