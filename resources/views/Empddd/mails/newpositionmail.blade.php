

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>New Position</title>
    <style>
        .main{

            max-width: 650px;
            width: 100%;
            overflow: hidden;
            border: solid 1px #CCCCCC;
            border-right: solid 1px #CCCCCC;
            font-family: Rubik, sans-serif;
            font-size: 13px;
            margin: 0 auto;background-color: #f5f5f5;}
        .main p{    line-height: 25px;}
        .main .logo{ float:right;}
        .main .head-f{ color:#333;     font-weight: 600;}
        .main .logo img{     width: 108px;
            padding: 10px 13px;}
        .pad_le{ padding:15px 20px;}
    </style>
</head>

<body>
<div class="main">

    <div class="pad_le">

        <div class="logo"> <img src="assets/images/logo.png" /></div>

        <p class="head-f">Dear Team,</p>

        <p> Following position has been added for client--{{ $mail_data['company_name']->comp_name }}
        </p>
        <p>
            Please login to your system and assign position to recruiter

        </p>
        <p>
            Position Details
            <table>
            <tr><td>Position Department</td><td>{{$mail_data['department']->dept_name}}</td></tr>
            <tr><td>Position Title</td><td>{{$mail_data['details']->jobtitle}}</td></tr>
            <tr><td>Number Of Positions</td><td>{{$mail_data['details']->noofpos}}</td></tr>
            </table>


        </p>

        <p> Please get in touch for any clarifications.</p>


        <p class="head-f">Regards,</p>

        <p>Elite Software</p>
    </div>
</div>

</body>
</html>




