@extends('Emp.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
           
       
                <h4> Listing</h4>
               
            </div>
        </div>



       
            <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text"> POSITION ASSIGNED TO RECRUITER BY SPOC </h5>
                           
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Name</th>
                                           
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                           $i=1;
                                         @endphp
                                          @foreach($positionassignedreqbyspoc as $key=>$value)
                                        <tr class="table-active">
                                           
                                             <td>{{  $i++   }}</td>
                                            <td><a class="waves-effect waves-dark posNavName" href="{{('get-assigned-postion/'.Crypt::encrypt($value->clientjob_id) )}}">{{ $value->clientjob_title }}</a></td>
                                          
                                        
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
             </div>
             
             
             
            
            </div>
      

        
@endsection



