@extends('admin.layouts.master')
@section('content')

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>

           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
    <div class="container-fluid">
        
        <style>
            .autogen{    width: 100%;
    float: left;
    max-height: 137px;
    overflow-y: auto;
    z-index: 999;
    background-color: #fff;}
    .autogen ul {padding: 0px;
    line-height: 35px; border: 1px solid  #e5e5e5;}
     .autogen ul li {padding: 0px 8px; border-bottom: 1px solid #e5e5e5;}
     .autogen ul li:last-child{ border-bottom:none;}
        </style>
           <style>

button
{

  background-color:#fff;
  border:none;
  cursor:pointer;
}

button:hover
{

  background-color:#fff;
  border:none;
  cursor:pointer;
  color:green;
}


</style>

        

        <div class="row">
            <div class="main-header">
                <h4>Spoc Joining And Pipeline  Report</h4>
             
            </div>
        </div>
        

       
            <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Spoc Joining And Pipeline Report</h5>
                           
                        </div>
                        <div class="card-block">


                              <div class="card-block">
                                 <form action=""  method="get" enctype="multipart/form-data"  autocomplete="off">
                                <div class="row">

     <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">User </label>
                                        <div class="col-sm-8">
      
            <input type="text" name="user" id="country" class="form-control" placeholder="Enter user Name"  autocomplete="off"  />  



                <div id="countryList" class="autogen"></div> 
          
          
          </div>
                                    </div>
                                </div>


 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Start Date </label>
                                        <div class="col-sm-8">
                                            <input class="form-control document" type="text" value="" id="dojnew" name="doj"/> </div>
                                    </div>
                                </div>

 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">End Date </label>
                                        <div class="col-sm-8">
                                             <input class="form-control document" type="text" value="" id="doend" name="doend" />
                                    </div>
                                </div>
                                
                                 </div>



<div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Clients</label>
                                        <div class="col-sm-8">
                                                <input type="text" name="clients" id="clients" class="form-control" placeholder="Enter Client Name"  autocomplete="off" />  



                <div id="ClientList" class="autogen"></div> 
                                    </div>
                                </div>
                                
                                 </div>

                                </div>

  <div class="md-input-wrapper">
                                 <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>  
                                
                                <a href="joiningpipelinereports"  class="btn btn-primary waves-effect waves-light">Cancel</a>
 <a href="#" class="btn btn-primary waves-effect waves-light">Download  PDF</a>

                            </div>




                                 



                                </form>


<!-- Script -->
 
                            </div>

     
</div>

                            <div class="row">
                                <div class="col-sm-12 table-responsive" >



  <div id="topdf" style="width: 100%;overflow: hidden;">
        <table width="100%" id="totable"  border="1">
           <tr><td >


<!-- Content Area -->

          


  @if(!empty($data))
            @foreach($data as $value)

   <?php 
   $subm=0;
 if(count($value['client'])>0) {
?>

<table width="100%" border="1" cellspacing="1" cellpadding="1"  >

<tr>
    <td width="10%" style="border:none;" height="30" align="left"><strong style="color:#1f2ccc;">{{ $value['username']}},</strong></td>
    <td colspan="5" style="border:none;"><p>&nbsp;</p>
    <p>&nbsp;</p></td>
   
  </tr>
  
  <tr>
    <th height="39" width="30%"  scope="col" rowspan="2">Client</th>
  
    <th width="16%" scope="col" rowspan="2">Pipe Lined</th>
    <th width="13%" scope="col" colspan="2" style='text-align: center'>Offered<br>
   </th>
      <th width="30%" scope="col" colspan="2" style='text-align: center'>Joined<br>
    </th>
  
   
  </tr>
  <tr>
   
    <th width="13%" scope="col">Target
   </th>
    <th width="13%" scope="col">Actual
   </th>
    <th width="13%" scope="col">Target
   </th>
      <th width="30%" scope="col">Actual
    </th>
  
   
  </tr>


<?php  foreach($value['client'] as $keyus => $k){?>
<tr>
  <td><?php echo $k->comp_name; ?></td>
  <td><?php echo  $k->piplined; ?></td>
  <td><?php echo  $k->Offeredtarget; ?></td>
   <td><?php echo  $k->Offered; ?></td>
    <td><?php echo  $k->joinedtarget; ?></td>
   <td><?php echo  $k->joined; ?></td>
</tr>

<?php } ?>


  <!-- 
<?php //echo  $c = count($value['userreport']);?>

<?php if(!empty($value['userreport'])) { ?>
<?php  foreach($value['userreport'] as $keyus => $k){?>
  <tr>
     <td><?php if(!empty($value['client'][$keyus])) { echo $value['client'][$keyus]->comp_name; }  ?>
     
     
     
    
     </td>
      
      
        <td><?php if(!empty($value['position'][$keyus])) { ?><a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value['empcode'];?>,<?php echo $value['position'][$keyus]->clientjob_id;?>,'<?php echo $from; ?>','<?php echo $to; ?>')" > <?php echo $value['position'][$keyus]->clientjob_title; }  ?></a></td>
      
     <td><?php echo($k->trtr);  ?></td>
     <td><?php if(!empty($value['userreport2'][$keyus])) { echo $value['userreport2'][$keyus]->sendclient; } else { echo "0"; } ?></td>
        <td><?php if(!empty($value['userreport2'][$keyus])) { echo $value['userreport2'][$keyus]->sendclient; } else { echo "0"; } ?></td>
        
       
   </tr>
   
   
   
   
   
<?php } } ?> -->
    




</table>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<?php } ?>
    @endforeach
            @endif
       </td></tr>
       </table>
    </div>     
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
             </div>
             
             
</div>
             
             
             
            
            </div>
            
            


        <script>
            function upload_position_jd(clientjob_id) {
                $.ajax({
                    url: 'adminpositionjd',
                    data: { id:clientjob_id },
                    success: function(result){
                        $("#positionJd").html(result);
                    }
                });
                $('#myModal').modal('show');
            }
    </script>
     <script>
    </script>

        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">

    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>
<script>  
 $(document).ready(function(){  
      $('#clients').keyup(function(){  
           var query = $(this).val();  
          // alert("hiii");
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('searchclients')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                      // alert(data);
                         $('#ClientList').fadeIn();  
                          $('#ClientList').html(data);  
                      }
                    });
                    
                    
              /*  $.ajax({  
                     method:"POST",  
                     url:"searchusers",  
                    
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                     }  
                });  */
           }  
      });  
      $(document).on('click', 'ul.list-unstyledclients li', function(){  
          $('#clients').val($(this).text());  
           $('#ClientList').fadeOut();  
      });  
 });  
 </script>  


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.1/jspdf.plugin.autotable.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
   
    <script type="text/javascript">
        var quotes = document.getElementById('totable');
        //! MAKE YOUR PDF
        var pdf = new jsPDF('p', 'pt', 'letter');
        html2canvas(quotes, {
            onrendered: function (canvas) {



                for (var i = 0; i <= quotes.clientHeight / 980; i++) {
                    //! This is all just html2canvas stuff
                    var srcImg = canvas;
                    var sX = 0;
                    var sY = 1100 * i; // start 1100 pixels down for every new page
                    var sWidth = 900;
                    var sHeight = 1100;
                    var dX = 0;
                    var dY = 0;
                    var dWidth = 900;
                    var dHeight = 1100;

                    window.onePageCanvas = document.createElement("canvas");
                    onePageCanvas.setAttribute('width', 900);
                    onePageCanvas.setAttribute('height', 1100);
                    var ctx = onePageCanvas.getContext('2d');
                    // details on this usage of this function: 
                    // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#Slicing
                    ctx.drawImage(srcImg, sX, sY, sWidth, sHeight, dX, dY, dWidth, dHeight);

                    // document.body.appendChild(canvas);
                    var canvasDataURL = onePageCanvas.toDataURL("image/png", 1.0);

                    var width = onePageCanvas.width;
                    var height = onePageCanvas.clientHeight;

                    //! If we're on anything other than the first page,
                    // add another page
                    if (i > 0) {
                        pdf.addPage(612, 791); //8.5" x 11" in pts (in*72)
                    }
                    //! now we declare that we're working on that page
                    pdf.setPage(i + 1);
                    //! now we add content to that page!
                    pdf.addImage(canvasDataURL, 'PNG', 20, 40, (width * .62), (height * .62));

                }
            }
        })

        $('a').click(function () {
            pdf.save('joiningpipelinereports.pdf');
        });
    </script>

<script>  
 $(document).ready(function(){  
      $('#country').keyup(function(){

   //   alert("biii");
                 var query = $(this).val();  
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: '{{URL::route('searchusers')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                      // alert(data);
                         $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                      }
                    });
                    
                    
              /*  $.ajax({  
                     method:"POST",  
                     url:"searchusers",  
                    
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                     }  
                });  */
           }  
      });  
      $(document).on('click', 'ul.list-unstyled li', function(){  
     //   alert('hiii');
    // alert($(this).text());
           $('#country').val($(this).text());  
           $('#countryList').fadeOut();  
      });  
 });  
 </script>  
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">

    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>


<script>
     $(document).ready(function () {  
          $('#doend').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>
<script>
     $(document).ready(function () {  
          $('#dojnew').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>


@endsection



