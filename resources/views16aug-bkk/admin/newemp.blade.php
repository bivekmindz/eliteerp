@extends('admin.layouts.master')
@section('content')
<style>
  .error{
    color:red;
  }
</style>
   <div class="container-fluid">
      <div class="row">
         <div class="main-header">
            <h4>New Employee Add</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
               <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
               </li>
               <li class="breadcrumb-item"><a href="javascript:void(0)">Employees</a>
               </li>
               <li class="breadcrumb-item"><a href="">Add Employees</a>
               </li>
            </ol>
         </div>
      </div>
<div class="row">
   <!-- Textual inputs starts -->
   <div class="col-lg-12">
      <div class="card">
         <div class="card-header">
            <h5 class="card-header-text">Employee Creation Form</h5>
         </div>
         <!-- end of modal -->
         <div class="card-block">
            <form  action="{{ route('admin.usersave') }}" name='emp' method="POST" >
              @if(count($errors))
                <div class="alert alert-danger">
                  <strong>Whoops!</strong> There were some problems with your input.
                  <br/>
                  <ul>
                      @foreach($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
                </div>
              @endif
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
               <div class="row">

                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Name<span class="error"> * </span></label>
                        <div class="col-sm-8">
                           <input class="form-control" type="text" value="" id="name" name="name"/>
                           <span class="text-danger">{{ $errors->first('name') }}</span>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Contact Number<span class="error"> * </span></label>
                        <div class="col-sm-8">
                           <input class="form-control" type="text" maxlength="20" value="" id="phone" name="phone"/>
                           <span class="text-danger">{{ $errors->first('phone') }}</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Email<span class="error"> * </span></label>
                        <div class="col-sm-8">
                           <input class="form-control" type="text" value="" id="email" name="email"/>
                           <span class="text-danger">{{ $errors->first('email') }}</span>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Password<span class="error"> * </span></label>
                        <div class="col-sm-8">
                           <input class="form-control" type="password" value="" id="password" name="password"/>
                           <span class="text-danger">{{ $errors->first('password') }}</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Confirm Password<span class="error"> * </span></label>
                        <div class="col-sm-8">
                           <input class="form-control" type="password" value="" id="cpassword" name="cpassword"/>
                           <span class="text-danger">{{ $errors->first('cpassword') }}</span>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Data of Joining<span class="error"> * </span></label>
                        <div class="col-sm-8">
                           <input class="form-control" type="text" value="" id="dojnew" name="doj" class="document"/>
                           <span class="text-danger">{{ $errors->first('doj') }}</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Department </label>
                        <div class="col-sm-8">
                           <div class="form-group">
                              <select class="form-control" id="dept" name="dept"/>
                                 <option value="">Please Select Department</option>
                               @foreach($departments as $kk => $val)
                                 <option value="{{$val->dept_id}}">{{$val->dept_name}}</option>
                               @endforeach
                              </select>
                              <span class="text-danger">{{ $errors->first('dept') }}</span>
                           </div>
                        </div>
                     </div>
                  </div>
  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Emp Id<span class="error"> * </span></label>
                        <div class="col-sm-8">
                           <input class="form-control" type="text" value="" id="empid" name="empid"/>
                           <span class="text-danger">{{ $errors->first('empid') }}</span>
                        </div>
                     </div>
                  </div>
                  
                  <div class="col-lg-6">
                     <div class="form-group row">
                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Role<span class="error"> * </span></label>
                        <div class="col-sm-8">
                           <div class="form-group">
                              <select id="role" class="multiselect-ui form-control" multiple name="role[]"/>
                                 @foreach($roles as $kk => $value)
                                   <option value="{{ $value->role_id  }}">{{ $value->role_name }}</option>
                                 @endforeach
                              </select>
                              <span class="text-danger">{{ $errors->first('role') }}</span>                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="md-input-wrapper">
                  <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit">
                  </input>
               </div>
            </form>
         </div>
      </div>
   </div>
   <!-- Textual inputs ends -->
</div>
</div>
</div>


   <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
     <!--  <script src = "https://code.jquery.com/jquery-1.10.2.js"></script> -->
      <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script>
    $(function() {
        $("form[name='emp']").validate({
            rules: {
              name: "required",
              phone: "required",
              email: {
                  required: true,
                  email: true,
                  remote: {
                    url: "checkemail",
                    type: "post",
                    data: {
                      _token: function() {
                        return "{{csrf_token()}}"
                      }
                    }
                  }
               },
              doj: "required",
              password: {
                  required: true,
                  minlength: 5,
                  maxlength: 20,
              },
              cpassword: {
                  required: true,
                  minlength: 5,
                  maxlength: 20,
                  equalTo: "#password"
              },
              'role[]': "required",
            },
            messages: {
              name: "Please enter name.",
              phone: "Please enter contact number.",
              email: {
                required:"Please enter email.",
                remote:"Please enter unique email.",
              },
              doj: "Please enter date of joining.",
              password: "Please enter minimum 5 digits password.",
              cpassword: "Enter confirm password same as password.",
              'role[]': "Please enter role.",
            },
            submitHandler: function(form) {
              form.submit();
            }
        });
    });

    $(document).ready(function() {
      $("#phone").keydown(function (e) {
          if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
              (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
              (e.keyCode >= 35 && e.keyCode <= 40)) {
                 return;
          }
          if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
              e.preventDefault();
          }
      });
    });
</script>
<script>
     $(document).ready(function () {  
          $('#dojnew').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>
@endsection
