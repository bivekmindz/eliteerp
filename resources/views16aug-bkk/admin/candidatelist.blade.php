@extends('admin.layouts.master')

@section('content')
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#boot-multiselect-demo').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });
        });
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Candidate Details</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Candidate Details</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Candidate Status</a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
             <div class="col-md-12">
                <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Candidate Status</h5>
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Candidate Name</th>
                                                <th>Mobile No</th>
                                                <th>CV Download</th>
                                                <th>Position</th>
                                                <th>Company Name</th>
                                                <th>Recruiter Name</th>
                                                <th>Status</th>    
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach($candidate as $key => $val)
                                        <tr class="table-active">
                                            <td>{{ $i++ }}</td>
                                            <td>{{  $val->candidate_name }}</td>
                                            <td>{{  $val->candidate_mob }}</td>
                                            <td>Download</td>
                                            <td>{{  $val->clientjob_title }}</td>
                                            <td>{{  $val->comp_name }}</td>
                                            <td>{{  $val->name }}</td>
                                            <td> 
                                                <select class="form-control form-control-sm" id="exampleSelect3">
                                                    <option>---</option>
                                                    <option>Telephonic</option>
                                                    <option>Skype</option>
                                                    <option>Round 3</option>
                                                    <option>Final Round</option>
                                                    <option>Closure </option>
                                                    <option>Document Share</option>
                                                    <option>On Hold</option>
                                                    <option>Joined</option>
                                                    <option>Screen Reject</option>
                                                    <option>Drop</option>  
                                                </select>
                                            </td>
                                        </tr>
                                        @endforeach
                                       </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection