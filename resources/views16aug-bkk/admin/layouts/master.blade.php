
<!DOCTYPE html>
<html lang="en">

<head>
 @include('admin.layouts.head')
</head>
<body class="sidebar-mini fixed">
@include('admin.layouts.header')
            <!-- Sidebar Menu-->
@include('admin.layouts.nav')
    <!-- Sidebar chat end-->

<div class="content-wrapper">
        @yield('content')
</div>
@include('admin.layouts.footer')

</body>


</html>
