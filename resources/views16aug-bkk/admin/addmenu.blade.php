@extends('admin.layouts.master')

@section('content')
<style>
  .error{
    color:red;
  }
</style>
    <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <h4>Add New Menu</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="">Menu</a>
                        </li>
                        <li class="breadcrumb-item"><a href="">Add New Menu</a>
                        </li>
                    </ol>
                </div>
           </div>

            <div class="row">
                <!-- Textual inputs starts -->
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header"><h5 class="card-header-text">Add New Menu</h5>
                            @if(Session::has('success_msg'))
                                <div class="alert alert-success">
                                    {{ Session::get('success_msg') }}
                                </div>
                            @endif

                        </div>
                        <!-- end of modal -->
                        <div class="card-block">
                            <div class="controls">
                        <form method="post" action="{{ route('savemenu') }}" name="menu">
                          <!--  {!!  Form::open(array('route' => "savemenu", 'method' => 'post')); !!} -->
                                @if(count($errors))
                                    <div class="alert alert-danger">
                                        <strong>Whoops!</strong> There were some problems with your input.
                                        <br/>
                                        <ul>
                                            @foreach($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            <div class="form-group row{{ $errors->has('parentmenu')? 'has-error' : '' }}">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Parent Menu<span class="error"> * </span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="" id="example-text-input" name="parentmenu">
                                </div>
                            </div>

                            <div class="entry input-group dat-add ">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Child Menu<span class="error"> * </span></label>
                                <div class="col-sm-5 {{ $errors->has('childmenu')? 'has-error' : '' }}" >
                                    <input class="form-control add_button" name="childmenu[]" type="text" placeholder="Enter child menu" />
                                    <span class="add_field">
                            <button class="btn btn-success btn-add" type="button">
                                <span class="icofont icofont-plus"></span>
                            </button>
                              </span>

                                </div>
                            </div>

                            </div>


                            <div class="md-input-wrapper">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            {!! Form::close(); !!}
                        </div>
                    </div>
                </div>
                <!-- Textual inputs ends -->
            </div>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
<script>
    $(function() {
        $("form[name='menu']").validate({
            rules: {
              'parentmenu': "required",
              'childmenu[]': "required",
            },
            messages: {
              'parentmenu': "Please enter parent menu.",
              'childmenu[]': "Please enter child menu.",
            },
            submitHandler: function(form) {
              form.submit();
            }
        });
    });
</script>
@endsection

