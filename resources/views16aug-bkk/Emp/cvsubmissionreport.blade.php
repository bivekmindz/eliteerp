@extends('Emp.layouts.master')

@section('content')

<style>
.menu .accordion-heading {  position: relative; }
.menu .accordion-heading .edit {
    position: absolute;
    top: 8px;
    right: 30px; 
}
.menu .area { border-left: 4px solid #f38787; }
.menu .equipamento { border-left: 4px solid #65c465; }
.menu .ponto { border-left: 4px solid #98b3fa; }
.menu .collapse.in { overflow: visible; }   
</style>
        <div class="row">

            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <!-- <h5 class="card-header-text">Client Listing</h5> -->
               <form action="{{ URL::to('clientlistexceldown') }}"  method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel" value="Download Excel" name="downexcel">
                                             
                                            </input>
                                        </form>
                        @if(Session::has('success_msg'))
                            <div class="alert alert-success">
                                {{ Session::get('success_msg') }}
                            </div>
                        @endif

                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 ">
                            
                         <div class="at-to-ty">
                         
                         <div class="accordion">
                    <!-- Áreas -->
                    <div class="accordion-group">
                        <!-- Área -->

                        
                        <div class="accordion-heading area">
                            <a class="accordion-toggle" data-toggle="collapse" href=
                            "#area1">Área #1</a>
            
                            <div class="dropdown edit">
                               
            
                                <ul class="dropdown-menu">
                                    <!-- Adicionar equipamento -->
            
                                    <li>
                                        <a href=""> Adicionar equipamento</a>
                                    </li>
            
                                    <li class="divider"></li><!-- Editar área -->
            
                                    <li>
                                        <a href=""> Editar área</a>
                                    </li>
            
                                    <li class="divider"></li><!-- Remover área -->
            
                                    <li>
                                        <a class="danger" href="#remove"><i class=
                                        "icon-remove"></i> Remover área</a>
                                    </li>
                                </ul>
                            </div>
                        </div><!-- /Área -->
            
                        <div class="accordion-body collapse" id="area1">
                            <div class="accordion-inner">
                                <div class="accordion" id="equipamento1">
                                    <!-- Equipamentos -->
            
                                    <div class="accordion-group">
                                        <div class="accordion-heading equipamento">
                                            <a class="accordion-toggle" data-parent=
                                            "#equipamento1-1" data-toggle="collapse" href=
                                            "#ponto1-1">Equipamento #1-1</a>
            
                                            <div class="dropdown edit">
                                               
            
                                                <ul class="dropdown-menu">
                                                    <!-- Adicionar ponto -->
            
                                                    <li>
                                                        <a href=
                                                        ""> Adicionar ponto</a>
                                                    </li>
            
                                                    <li class="divider"></li>
                                                    <!-- Editar equipamento -->
            
                                                    <li>
                                                        <a href=
                                                        ""> Editar
                                                        equipamento</a>
                                                    </li>
            
                                                    <li class="divider"></li>
                                                    <!-- Remover equipamento -->
            
                                                    <li>
                                                        <a class="danger" href=
                                                        "#remove"><i class=
                                                        "icon-remove"></i> Remover
                                                        equipamento</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div><!-- Pontos -->
            
                                        <div class="accordion-body collapse" id="ponto1-1">
                                            <div class="accordion-inner">
                                                <div class="accordion" id="servico1">
                                                    <div class="accordion-group">
                                                        <div class=
                                                        "accordion-heading ponto">
                                                            <a class="accordion-toggle"
                                                            data-parent="#servico1-1-1"
                                                            data-toggle="collapse" href=
                                                            "#servico1-1-1">Ponto
                                                            #1-1-1</a>
            
                                                            <div class="dropdown edit">
                                                              
            
                                                                <ul class="dropdown-menu">
                                                                    <!-- Adicionar servico -->
            
                                                                    <li>
                                                                        <a href=
                                                                        "../servico/add.php">
                                                                        <i class=
                                                                        "icon-plus"></i>
                                                                        Adicionar
                                                                        servico</a>
                                                                    </li>
            
                                                                    <li class="divider">
                                                                    </li><!-- Editar ponto -->
            
                                                                    <li>
                                                                        <a href=
                                                                        "../ponto/edit.php">
                                                                        <i class=
                                                                        "icon-pencil"></i>
                                                                        Editar ponto</a>
                                                                    </li>
            
                                                                    <li class="divider">
                                                                    </li><!-- Remover ponto -->
            
                                                                    <li>
                                                                        <a class="danger"
                                                                        href=
                                                                        "#remove"><i class=
                                                                        "icon-remove"></i>
                                                                        Remover ponto</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div><!-- Serviços -->
            
                                                        <div class=
                                                        "accordion-body collapse" id=
                                                        "servico1-1-1">
                                                            <div class="accordion-inner">
                                                                <ul class="nav nav-list">
                                                                    <li>
                                                                        <a href=
                                                                        "#"><i class=
                                                                        "icon-chevron-right">
                                                                        </i> Serviço
                                                                        #1-1-1-1</a>
                                                                    </li>
            
                                                                    <li>
                                                                        <a href=
                                                                        "#"><i class=
                                                                        "icon-chevron-right">
                                                                        </i> Serviço
                                                                        #1-1-1-2</a>
                                                                    </li>
            
                                                                    <li>
                                                                        <a href=
                                                                        "#"><i class=
                                                                        "icon-chevron-right">
                                                                        </i> Serviço
                                                                        #1-1-1-3</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div><!-- /Serviços -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- /Pontos -->
                                    </div><!-- /Equipamentos -->
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                </div>
                         
                         
                         
                         
                         
                          <div id="search_filter_filter" style="float:left" class="dataTables_filter"><label>Search:<input type="search" id="filter_area" style="border: 1px solid #968f8f;" class="" placeholder="" aria-controls="search_filter"></label></div>
                                
                                <table class="table" id="">
                                    <thead>
                                    <tr>
                                        <th>Company Name</th>
                                        <th>Position Name</th>
                                        <th>Recruiter Name</th>
                                        <th>No. of Uploaded CV</th>
                                        <th>No. of CV Submission</th>
                                        
                                      

    </tr>
    </thead>
    <tbody id="suggesstion-box">

    @php
    $i=1;
    @endphp
    @if(!empty($data))
    @foreach($data as $kk => $c)
    <tr>
     <td colspan="5"><strong>{{$c['comp_name'] }}</strong></td>
   @foreach($c['position'] as $v =>$val)
        <tr>
            <td></td>
      <td colspan="3"> {{$val->clientjob_title }}</td>
       @foreach($c['position'] as $v =>$val)
        <tr>
            <td></td>
            <td></td>
       <td>{{$val->name }}</td>
       <td>{{$val->upload_cv }}</td>
       <td>{{$val->sendtoclient_cv }}</td>
       </tr>
    @endforeach

    </tr>
    @endforeach
</tr>
    
  
    @endforeach
    @endif


    </tbody>
    </table>

                         </div>
                            

                               
  
    </div>

    </div>
    </div>
    </div>
    </div>
      </div>
      
      @endsection