@extends('Emp.layouts.master')

@section('content')

<script>

$(document).ready(function(){
    $("#filter_area").keyup(function(){
        var xxx = $(this).val();
        document.getElementById("filter_area").focus();
      //  alert(xxx);
          $.ajax({
                      type: 'POST',
                      url: '{{URL::route('ajaxvacancy')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'key':xxx,
                      },
                      success: function(data){
                      alert(data);
                      $("#suggesstion-box").show();
                      $("#suggesstion-box").html(data);
                      }
                    });

    });
});

</script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#boot-multiselect-demo').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });
        });
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Vacancy List</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Vacancy</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Vacancy List</a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">All Vacancies</h5>
                           
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12">
                                
                                <div class="vacancy-lisi">
                                    <table id="search_filter">
                                        <thead>
                                        <tr>
                                            <th>S.N</th>
                                            <th>Position Name</th>
                                            <th>Company Name</th>
                                            <th>Total Positions</th>
                                            <th>Total Uploaded Profile</th>
                                            <th>Status</th>
                                            <th>Total Short Listing</th>
                                             <th>Closure</th>
                                        </tr>
                                        </thead>
                                        <tbody id="suggesstion-box">
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach($vacancy as $key => $val)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{  $val->clientjob_title }}</td>
                                            <td>{{  $val->comp_name }}</td>
                                            <td>{{  $val->clientjob_noofposition }}</td>
                                            <td>{{  $val->totalCount }}</td>
                                            <td>{{  $val->clientjob_status }}</td>
                                            <td>Ducky</td>
                                            <td>
                                                <div class="btn-group btn-group-sm" style="float: none;">
                                                    <button type="button"  class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span></button>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                       </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script> 
@endsection