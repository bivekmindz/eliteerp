@extends('Emp.layouts.master')

@section('content')

<script type="text/javascript">
	$(document).ready(function(){
    $(".searchcategory").keypress(function(event){
    
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
         alert('You pressed a "enter" key in textbox'); 
         var xxx = $(this).val();
        // alert(xxx);
        var yyy = $("#mmm").val();
        

          $.ajax({
                      type: 'POST',
                      url: '{{URL::route("searchcandidateprofile")}}',                                               
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'key':xxx,
                        'act_mode':yyy,
                       
                      },
                      success: function(data){
                       // alert(data);
                         $("#suggesstion-box").show();
                         $("#suggesstion-box").html(data);
  
                      }
                    });
 
    }
    event.stopPropagation();
   });
       });
</script>


<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			 <div class="card">
                <div class="card-header">
                  <h4>Search Candidate Profile</h4>
				<select id="searchcategory" name="checkbox_data[]" onchange="searchcategory()">
				    <option value=""> Search By</option>
				    <option value="1">Candidate Name </option>
				    <option value="2">Candidate Email </option>
				    <option value="3">Candidate Phone </option>

				</select>
			    <input class="searchcategory" id="searchname" name="searchname" type="text" value="" placeholder="Enter Name" style="display: none">
			    <input class="searchcategory" id="searchemail" name="searchemail" type="text" value="" placeholder="Enter Email" style="display: none">
			    <input class="searchcategory" id="searchphone" name="searchphone" type="text" value="" placeholder="Enter Phone" style="display: none">
			  <input class="" id="mmm" name="" type="hidden" value="" placeholder="Enter Phone" >

</div>
</div>
</div>
</div>
</div>

<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
       <div class="card">
                <div >


    <table class="table dt" id="suggesstion-box">

    </table>

</div>
</div>
</div>
</div>
</div>

<div class="container">
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Status</h4>
        </div>

           <form method="post" action="{{ URL::to('updateclientstatuspopup') }}">
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="form-group">
                          <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Select Status:

                          </label>
                          <select class="form-control col-xs-6" required id="recruiterspockup" name="recruiter[]"  onchange="changestatuspopupdata(this.value)" >
                             <option value="">Select Status</option>                   
                          </select>
                            <input type="text" name="clientreq_posid" id="placepopupval" >
                          <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                        <div id="placepopup"></div>
                     
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="button" class="btn btn-default" value="Submit" id="buttonClassstpop">
            </div>
        </form>



        <div id=""></div>
      
      </div>      
    </div>
  </div> 

<script type="text/javascript">

function changestatuspopupdata(id)
{
var str = id;
var res = str.split("/");
//alert(str);

//Then read the values from the array where 0 is the first
var myvar = res[0] + ":" + res[1] + ":" + res[2];
  // d = document.getElementById("select_id").value;
  if(res[2]=='4')
  {
 document.getElementById("placepopup").innerHTML="<input type='text' name='expectedsalery[]'  placeholder='Expected Salary'  class='form-control' id='expectedsalery' ><input type='date' name='Doj[]' class='form-control'  placeholder='Date Of Joining' id='Doj'><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining' id='expDoj' class='form-control'  ><input type='hidden' name='count[]' class='form-control col-xs-6'  value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  else

{


   if(res[2]=='5')
  {
 document.getElementById("placepopup").innerHTML="<input type='hidden' name='expectedsalery[]' class='form-control'  placeholder='Expected Salary' id='expectedsalery'><input type='hidden'class='form-control'  name='Doj[]'  placeholder='Date Of Joining'   id='Doj'><input type='date'  class='form-control' name='expDoj[]'  placeholder='Expected Date Of Joining'  id='expDoj'  ><input type='hidden' name='count[]' value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  else
  {


   if(res[2]=='6')
  {
    document.getElementById("placepopup").innerHTML="<input type='text' name='expectedsalery[]' class='form-control'  placeholder='Expected Salary'  id='expectedsalery'><input type='date' name='Doj[]'   class='form-control'  placeholder='Date Of Joining' id='Doj' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'    id='expDoj' ><input type='hidden' name='count[]' value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  else
  {
    document.getElementById("placepopup").innerHTML="<input type='hidden' name='expectedsalery[]' class='form-control col-xs-6' placeholder='Expected Salary' id='expectedsalery' ><input type='hidden' name='Doj[]'  placeholder='Date Of Joining'  id='Doj' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'  id='expDoj' ><input type='hidden' name='count[]' value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  }
}
 }


    function searchcategory() {
        var ids = $('#searchcategory').val();
        $('#mmm').val(ids);
        //alert(vvv);

        // alert(ids);

        if(ids==1){
            // alert(1);

            $('#searchname').show();
            $('#searchemail').hide();
            $('#searchphone').hide();
        }
        else if(ids==2){
            // alert(2);

            $('#searchname').hide();
            $('#searchemail').show();
            $('#searchphone').hide();
        }
        else if(ids=3){

            $('#searchname').hide();
            $('#searchemail').hide();
            $('#searchphone').show();
        }
        // body...
    }
    function getposiionvalue(id)
{

 var ids = id;
 //alert(ids);
    $.ajax({
        type:"GET",
        url:"getspockposition/"+ids,
        data:'',
        success: function(data){
        // alert(data);
        document.getElementById("placepopupval").value = data;

        },
        error: function(data){

        }
    });



      

}

function getupdatespock(id,recruiter_id,cv_status,posid)
{

 var ids = id;
 var recruiterid = recruiter_id;
 var cvstatus = cv_status;
 var posid = posid;
    $.ajax({
        type:"GET",
        url:"getspockstatus/"+ids+"/"+recruiterid+"/"+cvstatus+"/"+posid+"/",
        data:'',
        success: function(data){
        // alert(data);
            $("#recruiterspockup").empty();
            $("#recruiterspockup").append(data);
          // $('#recruiterspockup').multiselect('rebuild');
        },
        error: function(data){

        }
    });



      

}
</script>

<script>
   $(document).ready(function() {
        $("#buttonClassstpop").click(function() {

var sel = document.getElementById('recruiterspockup').value;
var expectedsaleryval = document.getElementById('expectedsalery').value;
var Doj = document.getElementById('Doj').value;
var expDoj = document.getElementById('expDoj').value;
var count = document.getElementById('count').value;
var countval = document.getElementById('countval').value;
var placepopupval = document.getElementById('placepopupval').value;
var keyval = document.getElementById('keyval').value;
var actmode = document.getElementById('actmode').value;
alert(sel);

   $.ajax({
                      type: 'POST',
                      url: '{{URL::route('updaterecresume')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'sel':sel,
                        'expectedsalery':expectedsaleryval,
                        'Doj':Doj,
                        'expDoj':expDoj,
                        'count':count,
                          'countval':countval,
                        'placepopupval':placepopupval,
                         'keyval':keyval,
                          'actmode':actmode,
                      },
                      success: function(data){
                      $("#suggesstion-box").show();
                         $("#suggesstion-box").html(data);
                      // $('#myModal').modal('hide');
                     
               
      //  location.reload();

  //$('#candilist'+placepopupval).html(data); 





                         
                      }
                    });


 

           });
    });

</script>

@endsection