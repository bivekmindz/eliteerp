<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Elite Software</title>
    <style>
        .main{

            max-width: 650px;
            width: 100%;
            overflow: hidden;
            border: solid 1px #CCCCCC;
            border-right: solid 1px #CCCCCC;
            font-family: Rubik, sans-serif;
            font-size: 13px;
            margin: 0 auto;background-color: #f5f5f5;}
        .main p{    line-height: 25px;}
        .main .logo{ float:right;}
        .main .head-f{ color:#333;     font-weight: 600;}
        .main .logo img{     width: 108px;
            padding: 10px 13px;}
        .pad_le{ padding:15px 20px;}
    </style>
</head>

<body>
<div class="main">

    <div class="pad_le">

        <div class="logo"> <img src="{{$urldata}}assets/images/logo.png" /></div>

        <p class="head-f">Dear {{ $spocname }},</p>

        <p>Position {{ $clientjob_title }} has been assigned to</p>
        <ul>
        @foreach($rec as $key=>$value)
            <li>{{ $value->name }}</li>
        @endforeach
        </ul>

        <p>Happy recruiting.</p>
        <p class="head-f">Regards,</p>

        <p>Recruitment software</p>
    </div>
</div>

</body>
</html>
