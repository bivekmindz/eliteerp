


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Daily Cv uploaded report</title>
    <style>
        .main{

            max-width: 650px;
            width: 100%;
            overflow: hidden;
            border: solid 1px #CCCCCC;
            border-right: solid 1px #CCCCCC;
            font-family: Rubik, sans-serif;
            font-size: 13px;
            margin: 0 auto;background-color: #f5f5f5;}
        .main p{    line-height: 25px;}
        .main .logo{ float:right;}
        .main .head-f{ color:#333;     font-weight: 600;}
        .main .logo img{     width: 108px;
            padding: 10px 13px;}
        .pad_le{ padding:15px 20px;}
    </style>
</head>

<body>
<div class="main">

    <div class="pad_le">

        <div class="logo"> <img src="public/images/logo.png" /></div>

        <p class="head-f">Dear Team,</p>

       <table border="1">
             <thead>
             <tr>
             <th>Company Name</th>
              <th>Position Name</th>
               <th>Spoc Name</th>
                 <th>Recuriter Name</th>
                
                
                 <th>Number of uploaded cv</th>
                 <th>Sent to Client</th>
                 <th>Submissions</th>
                 <th>Total Submission Post</th>
             </tr>
             </thead>
            <tbody>

            @if(!empty($mail_data['report']))
            @foreach($mail_data['report'] as $value)

                <tr class="table-active">
                <td>{{ $value->comp_name }}</td>
                 <td>{{ $value->clientjob_title }}</td>
                <td>{{ $value->spoc }}</td>
               
             <td><table >
@if ($value->recruiter_name != "") @foreach(explode(',', $value->recruiter_name) as $info) 
   <tr><td>{{$info}}</td></tr>@endforeach
@endif
</table> </td>
             
              <td><table >
@if ($value->upload_cv != "") @foreach(explode(',', $value->upload_cv) as $infocv) 
   <tr><td>{{$infocv}}</td></tr>@endforeach
@endif
</table> </td>

<td><table >
@if ($value->sent_to_client != "") @foreach(explode(',', $value->sent_to_client) as $infoclient) 
   <tr><td>{{$infoclient}}</td></tr>@endforeach
@endif
</table> </td>
<td><table >
@if ($value->sent_to_client != "") @foreach(explode(',', $value->sent_to_client) as $infoclient) 
   <tr><td>{{$infoclient}}</td></tr>@endforeach
@endif
</table> </td>

                        <td>{{ $value->totalupload_cv }}</td>

                </tr>

                @endforeach
            @endif
            </tbody>
         </table>



        <p> Please get in touch for any clarifications.</p>


        <p class="head-f">Regards,</p>

        <p>Elite Software</p>
    </div>
</div>

</body>
</html>

