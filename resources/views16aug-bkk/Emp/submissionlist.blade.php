@extends('Emp.layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
          
                <h4>Submissions Listing</h4>
              
            </div>
        </div>



       
            <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">All Submissions</h5>
                           
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Candidate Name</th>
                                            <th> Mobile</th>
                                            <th>Email</th>
                                             <th>Cv Upload Time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                           $i=1;
                                         @endphp
                                          @foreach($submissionlist as $key=>$value)
                                        <tr class="table-active">
                                           
                                             <td>{{  $i++   }}</td>
                                         <td>{{ $value->candidate_name }}</td>
                                            <td>{{ $value->candidate_mob }}</td>
                                               <td>{{ $value->candidate_email }}</td>
                                                
                                                     <td>{{ $value->created_at }}</td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
             </div>
             
             
             
            
    </div>
      
@endsection



