@extends('Emp.layouts.master')

@section('content')


<!-- Container-fluid starts -->
<!-- Main content starts -->
<div class="container-fluid">
  <div class="row">
    <div class="main-header">
      <h4>Shortlist Profile</h4>
      <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
        <li class="breadcrumb-item"><a href=""><i class="icofont icofont-home"></i></a>
        </li>
        <li class="breadcrumb-item"><a href="">Positions</a>
        </li>
        <li class="breadcrumb-item"><a href="">Shortlisting of CV</a>
        </li>
      </ol>
    </div>

  </div>


 

  <div class="row">

    <div class="col-md-12">

      <!-- <div class="card hide_css"> -->
      <div class="card">
        <div class="card-header">
          <h5 class="card-header-text">All Candidate List</h5>
        </div>
            @if(Session::has('success_msg'))
           <div class="alert alert-success">
             {{ Session::get('success_msg') }}
           </div>
           @endif
         {!! Form::open() !!} 
        <div class="card-block">
          <div class="row" >
            <div class="col-sm-12 table-responsive">
              <table class="table"  >
                <thead>
                  <tr>
                    <th>S.N</th>
                    <th>Candidate's Name</th>
                    <th>Mobile Number</th>
                    <th>Email</th>
                    <th>Qulification</th>
                    <th>Total Experience</th>
                    <th>Domain Experience </th>
                    <th>Primary Skill</th>
                    <th>Secondary Skill</th>
                    <th>Current ORG</th>
                    <th>Current CTC </th>
                    <th>Expected CTC</th>
                    <th>NP</th>
                    <th>Reason For Change</th>
                    <th>Skills For Rating</th>
                    <th>Relocation</th>
                    <th>DOJ</th>
                    <th>Remark</th>
                    <th>Cv Upload</th>
                    <!-- <th>Status</th> -->


                  </tr>
                </thead>
                <tbody id="candilist">
                    @php
                  $i=1;
                  @endphp
                  @foreach($pos as $key=>$value)
                  <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $value->candidate_name }}</td>
                    <td>{{ $value->candidate_mob }}</td>
                    <td>{{ $value->candidate_email }}</td>
                    <td>{{ $value->highest_qulification }}</td>
                    <td>{{ $value->total_exp }}</td>
                    <td>{{ $value->domain_exp }}</td>
                    <td>{{ $value->primary_skill }}</td>
                    <td>{{ $value->secondary_skill }}</td>
                    <td>{{ $value->current_org }}</td>
                    <td>{{ $value->current_ctc }}</td>
                    <td>{{ $value->expected_ctc }}</td>
                    <td>{{ $value->np }}</td>
                    <td>{{ $value->reason_for_change }}</td>
                    <td>{{ $value->communication_skills_rating }}</td>
                    <td>{{ $value->relocation }}</td>
                    <td>{{ $value->doj }}</td>
                    <td>{{ $value->remark }}</td>
                    <td>
                        <button type="button" class="btn btn-info btn-sm" onClick="cvupload('<?php echo $value->id;?>');" data-toggle="modal" data-target="#myModal">Upload</button>
                    </td>
                    </tr>
                    @endforeach
                </tbody>
                <input type="hidden" value="" id="poid" name="poid">
              </table>

            </div>
          </div>

          </div>
          <!-- <div class="md-input-wrapper">
            <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit">
          </div> -->
      </div>
      {!! Form::close() !!} 
    </div>

  </div>

</div>

<div class="container">
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
       <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload CV</h4>
        </div>
        <div class="modal-body">
           <form method="post" action="{{ URL::to('cvupload') }}" enctype="multipart/form-data">
              <input type="hidden" name="recruiter_cv_id" id="recruiter_cv_id" value="">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <input type="file" name="cv_name" id="cv_name" required/></br>
              <input type="submit" class="btn btn-primary" name="submit" value="submit"/>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- </div>



</div> -->
<!-- <script>

 function getposition(id){
  var ids  = id;
  $('#poid').val(ids);

}



$(document).ready(function(){
  $("button").click(function(){
    var x = $(this).val();
    alert(1);
    $.ajax({url: "demo_test.txt", success: function(result){
      $("#div1").html(result);
    }});

  })
})
</script -->
<script type="text/javascript">
  $("input:checkbox:not(:checked)").each(function () {
    alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
  });
  
  function cvupload(id){
    $("#recruiter_cv_id").val(id);
  }
</script>

@endsection