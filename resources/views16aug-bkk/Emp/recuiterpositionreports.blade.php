@extends('Emp.layouts.master')
@section('content')
    <div class="container-fluid">
        
        <style>
            .autogen{    width: 100%;
    float: left;
    max-height: 137px;
    overflow-y: auto;
    z-index: 999;
    background-color: #fff;}
    .autogen ul {padding: 0px;
    line-height: 35px; border: 1px solid  #e5e5e5;}
     .autogen ul li {padding: 0px 8px; border-bottom: 1px solid #e5e5e5;}
     .autogen ul li:last-child{ border-bottom:none;}
        </style>
           
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
        

        <div class="row">
            <div class="main-header">
                <h4>Recuiter Status Report</h4>
             
            </div>
        </div>
@php
if(isset($_GET['user'] ) && $_GET['user']!='')
{
$_GET['user']=$_GET['user'];
}
else
{
  $_GET['user']='';
}
if(isset($_GET['doj'] ) && $_GET['doj']!='')
{
$_GET['doj']=$_GET['doj'];
}
else
{
  $_GET['doj']='';
}
if(isset($_GET['doend'] ) && $_GET['doend']!='')
{
$_GET['doend']=$_GET['doend'];
}
else
{
  $_GET['doend']='';
}


@endphp


       
            <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Recuiter  Report</h5>
                           
                        </div>
                        <div class="card-block">


                              <div class="card-block">
                                 <form action=""  method="get" enctype="multipart/form-data"  autocomplete="off">
                                <div class="row">

     <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Position </label>
                                        <div class="col-sm-8">
      
            <input type="text" name="user" id="country" class="form-control" placeholder="Enter Position Name" value="<?php echo $_GET['user']; ?>"  autocomplete="off" />  



                <div id="countryList" class="autogen"></div> 
          
          
          </div>
                                    </div>
                                </div>


 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Start Date </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" value="<?php echo $_GET['doj']; ?>"  id="dojnew" name="doj" class="document" /> </div>
                                    </div>
                                </div>

 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">End Date </label>
                                        <div class="col-sm-8">
                                             <input class="form-control" type="text" value="<?php echo $_GET['doend']; ?>"  id="doend" name="doend" class="document" />
                                    </div>
                                </div>
                                
                                

                                 <div class="col-lg-6">
                                 <div class="form-group row">

                                        <div class="col-sm-12">
                                        
                                        </div>
                                        
                                          <div class="col-sm-12">
                                       
                                        </div>
                                    </div>
                                    
                                  
                                 
                                    </div>
                                 </div>

                                </div>



  <div class="md-input-wrapper">
                                <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>
                                
                                <a href="recuiterpositionreports"  class="btn btn-primary waves-effect waves-light">Cancel</a>
                            </div>




                                 



                                </form>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                  




<table width="100%" border="1" cellspacing="0" cellpadding="0">

  <tr>
    <th height="39" width="30%"  scope="col">Position Name</th>
  
    <th width="16%" scope="col">Pipe Lined</th>
    <th width="13%" scope="col"  style='text-align: center'>Offered<br>
   </th>
      <th width="30%" scope="col" style='text-align: center'>Joined<br>
    </th>
   <th width="30%" scope="col" style='text-align: center'>Not Seen<br>
    </th>
     
      <th width="30%" scope="col"  style='text-align: center'>Submission<br>
    </th> 
    <th width="30%" scope="col"  style='text-align: center'>Rejected<br>
    </th>
  </tr>

  
   
  </tr>


 @if(!empty($getPositions))
            @foreach($getPositions as $value)
            <tr>
  <td>{{$value->clti}}</td>
   <td>
<a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value->fk_empid; ?>,<?php echo $value->cltidd; ?>,<?php echo '5'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$value->Pipeline}}</a> 
</td>
    <td><a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value->fk_empid; ?>,<?php echo $value->cltidd; ?>,<?php echo '4'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$value->Offer}}</a> 
    </td>
    <td><a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value->fk_empid; ?>,<?php echo $value->cltidd; ?>,<?php echo '6'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$value->Joining}}</a> 
   </td>

     <td>
<a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value->fk_empid; ?>,<?php echo $value->cltidd; ?>,<?php echo '1'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$value->notseen}}</a>  </td>
    <td>

<a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value->fk_empid; ?>,<?php echo $value->cltidd; ?>,<?php echo '2'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$value->sendtoclient}}</a> 
     </td>
    <td>
<a style="color: #43a061;" data-toggle="modal" data-target="#myModalnnn" onclick="getresumelistreport(<?php echo $value->fk_empid; ?>,<?php echo $value->cltidd; ?>,<?php echo '3'; ?>,'<?php echo $from; ?>','<?php echo $to; ?>')" >{{$value->rejected}}</a>
</td>
</tr>
 @endforeach
            @endif
         </table>

           <div class="container">
   <div class="modal fade" id="myModalnnn" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Resumes</h4>
        </div>
           <div class="modal-body" >
       <table width="90%" border="1" cellspacing="0" cellpadding="0">
           <thead>
           <tr>
           <th> Name</th><th>Mobile Id</th>   <th>Email Id</th> <th>Resume Name</th> <th>Created On</th>
            </tr>
            </thead>
            <tbody id="recruiter">
          
            </tbody>
             
            
        </table>
        </div>   
      </div>      
    </div>
  </div> 
</div>



   
       <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
       <script>
     
function getresumelistreport(fk_id,fk_posid,fk_from,fk_fromdate,fk_todate)
{
    var useridd = fk_id;
    var position_id = fk_posid;
    var fromid = fk_from;
      var fromdate = fk_fromdate;  
      var todate = fk_todate;

    $.ajax({
        type:"GET",
        url:"getresumesdatareport/" + useridd + "/"  + position_id + "/" + fromid + "/" + fromdate+ "/" + todate ,
        data:'',
        success: function(data){
          //  alert(data);
            $("#recruiter").empty();
            $("#recruiter").append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}    </script>
<script>
     $(document).ready(function () {  
          $('#doend').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>
<script>
     $(document).ready(function () {  
          $('#dojnew').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>
  


<script>  
 $(document).ready(function(){  
      $('#country').keyup(function(){

   //   alert("biii");
                 var query = $(this).val();  
           if(query != '')  
           {  
                 $.ajax({
                      type: 'POST',
                      url: 'searchposition',
                      data:{
                         '_token': "{{ csrf_token() }}",
                        'query':query,
                      },
                      success: function(data){
                      // alert(data);
                         $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                      }
                    });
                    
                    
              /*  $.ajax({  
                     method:"POST",  
                     url:"searchusers",  
                    
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                     }  
                });  */
           }  
      });  
      $(document).on('click', 'li', function(){  
     //   alert('hiii');
    // alert($(this).text());
           $('#country').val($(this).text());  
           $('#countryList').fadeOut();  
      });  
 });  
 </script>  
@endsection



