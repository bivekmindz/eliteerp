<label>Recruiter</label>

    <li class="treeview">
        <a class="waves-effect waves-dark" href="">
        <i class="icon-speedometer"></i><span> My Positions</span><i class="icon-arrow-down"></i>
    </a>
    @inject('position', 'App\Component\CommonComponent')

    <ul class="treeview-menu">
        @php 
            $positions = $position->getPositions();
        @endphp
<!-- <?PHP ECHO "<PRE>"; PRINT_R( $positions);?>
 -->

        @if(!empty($positions))
        @foreach($positions as $key => $p)
        <li class="active" id="unclick">
            <a class="waves-effect waves-dark posNavName" href="{{url::to('get-assigned-postion/'.Crypt::encrypt($p->clientjob_id) )}}">
            <i class="icon-arrow-right"></i>
            <span>{{$p->clientjob_title}} </span> 
            </a>
        </li>
        @endforeach
        @endif
    </ul>
</li>
