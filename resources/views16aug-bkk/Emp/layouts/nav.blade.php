
<ul class="sidebar-menu">
    <li class="nav-level"><a href="{{ URL::to(''.'dashboard') }}">Dashboard</a></li>


    @inject('userRole','App\Component\CommonComponent')
    @inject('position', 'App\Component\CommonComponent')
    <?php
    $roles = $userRole->getRole();
  //print_r($roles);
    $arr=(array) $roles->emp_role;
    $d=''
;    foreach($arr as $k){
        $d .= $k;
    }

    if (strpos($d, '3') !== false) {
    // echo 'true';
    ?>
<li class="treeview">
        <a class="waves-effect waves-dark" href="">
        <i class="icon-speedometer"></i><span> My Positions</span><i class="icon-arrow-down"></i>
    </a>
        <ul class="treeview-menu">
            @php 
                $positions = $position->getPositions();
            @endphp
    

            @if(!empty($positions))
            @foreach($positions as $key => $p)
            <li class="active" id="unclick">
                <a class="waves-effect waves-dark posNavName" href="{{url::to('get-assigned-postion/'.Crypt::encrypt($p->clientjob_id) )}}">
                <i class="icon-arrow-right"></i>
                <span>{{$p->clientjob_title}} </span> 
                </a>
            </li>
            @endforeach
            @endif
        </ul>
    </li>
 



   
    <?php } ?>
    
    <?php
        $nav = $userRole->navigation();
        foreach($nav as $key=>$value)
        { ?>
            <li class="treeview">
            <?php if($value->menuparentid==0)
            { ?>
                <a class="waves-effect waves-dark" href="{{ URL::to(''.$value->url) }}">
                    <i class="icon-speedometer"></i><span>{{ $value->menuname }}</span><i class="icon-arrow-down"></i>
                </a>
            <?php }
                foreach($nav as $k=>$v)
                { 
                    if($v->menuparentid==$value->id)
                    { ?>
                      <ul class="treeview-menu">
                        <li class="active"><a class="waves-effect waves-dark" href="{{ URL::to(''.$v->url) }}"><i class="icon-arrow-right"></i><span>{{ $v->menuname }} </span></a></li>

                        </ul>  
                    <?php }
                    
                } ?>
                    
            </li> 
        <?php }
       
        ?>

 <!-- <li class="nav-level"><a href="{{ URL::to(''.'dashboard') }}">Hi</a></li> -->


</ul>
</section>
</aside>
<!-- Sidebar chat start -->

<div class="showChat_inner">
    <div class="media chat-inner-header">
        <a class="back_chatBox">
            <i class="icofont icofont-rounded-left"></i> Josephin Doe
        </a>
    </div>
    <div class="media chat-messages">
        <a class="media-left photo-table" href="#!">
            <img class="media-object img-circle m-t-5" src="assets/images/avatar-1.png" alt="Generic placeholder image">
            <div class="live-status bg-success"></div>
        </a>
        <div class="media-body chat-menu-content">
            <div class="">
                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                <p class="chat-time">8:20 a.m.</p>
            </div>
        </div>
    </div>
    <div class="media chat-messages">
        <div class="media-body chat-menu-reply">
            <div class="">
                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                <p class="chat-time">8:20 a.m.</p>
            </div>
        </div>
        <div class="media-right photo-table">
            <a href="#!">
                <img class="media-object img-circle m-t-5" src="assets/images/avatar-2.png" alt="Generic placeholder image">
                <div class="live-status bg-success"></div>
            </a>
        </div>
    </div>
    <div class="media chat-reply-box">
        <div class="md-input-wrapper">
            <input type="text" class="md-form-control" id="inputEmail" name="inputEmail" >
            <label>Share your thoughts</label>
            <span class="highlight"></span>
            <span class="bar"></span>  <button type="button" class="chat-send waves-effect waves-light">
                <i class="icofont icofont-location-arrow f-20 "></i>
            </button>

            <button type="button" class="chat-send waves-effect waves-light">
                <i class="icofont icofont-location-arrow f-20 "></i>
            </button>
        </div>

    </div>
</div>
<!-- Sidebar chat end-->






























