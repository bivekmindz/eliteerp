@extends('Emp.layouts.master')

@section('content')

<style type="text/css">
  #search_filter_filter{float:left!important;}
</style>


<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View CV List</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Client Call CV List</a>
                </li>
                <li class="breadcrumb-item"><a href="">Allocated CVs List Details</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
                @endif
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                   
                            <table class="table" id="">
                                <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>CandidateName</th>
                                    <th>CV Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>FollowUp Status</th>
                                    <th>Action</th> 
                                </tr>
                                </thead>
                                <tbody id="suggesstion-box">
                                @php
                                $i=1;
                                @endphp
                                @foreach($cvdetails as $key => $p)
                                <tr class="table-active" >
                                    <td>{{ $i++ }}</td>
                                    <td>{{ !empty($p->candidate_name) ? $p->candidate_name : 'N/A' }}</td>
                                    <td>{{ !empty($p->cv_name) ? $p->cv_name : 'N/A' }}</td>
                                    <td>{{ !empty($p->candidate_email) ? $p->candidate_email : 'N/A' }}</td>
                                    <td>{{ !empty($p->candidate_mob) ? $p->candidate_mob : 'N/A' }}</td>
                                    <td>{{ $p->tca_afterfollowupstatus }}</td>
                                    <td>  <select id="status" onchange="changestatusafterfollowup({{$p->tca_id}})">
                                            <option>Change Status</option>
                                              <option value="Confirm">Confirm</option>
                                            <option value="Going">Going</option>
                                             <option value="Not Going">Not Going</option>
                                               <option value="Not Responding">Not Responding</option>
                                            <option value="Rejected">Rejected</option>
                                            <option value="Offered">Offered</option>
                                           <option value="Joined">Joined</option>

                                        </select>
                                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </td>

                </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
</script>

<!-- <script type="text/javascript">
    function abc(){
        alert('hi');
    }
</script> -->
<script>
    
    function changestatusafterfollowup(ids)
    {
        var status=$('#status').val();
        //alert(ids);
        $.ajax({
            url: '{{URL::route('changecvafterfollstatus')}}',
            type: 'post',
            data:{
                '_token': "{{ csrf_token() }}",
                'key':ids,
                'status':status,
              },
            success: function(data)
            {
                if(data==1){
                    location.reload();
                }
            }
        });
    }

</script>

@endsection

