@extends('Emp.layouts.master')

@section('content')
   <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#boot-multiselect-demo').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });
        });

    </script>

<style type="text/css">
  #search_filter_filter{float:left!important;}
</style>
<script>

$(document).ready(function(){
    $("#filter_area").keyup(function(){
        var xxx = $(this).val();
        document.getElementById("filter_area").focus();
       // alert(xxx);
          $.ajax({
                      type: 'POST',
                      url: '{{URL::route('ajaxlistclient')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'key':xxx,
                      },
                      success: function(data){
                      // alert(data);
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                      }
                    });

    });
});

</script>

    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
             @inject('position', 'App\Component\CommonComponent')
    @php 
            $breadcrumb = $position->breadcrumbs();
            //print_r($breadcrumb); exit;
            //print_r($breadcrumb[0]->menuname); exit;
        @endphp
 
                <h4>Assign Client To Spoc</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">{{$breadcrumb[0]->parentmenu}}</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">{{$breadcrumb[0]->menuname}}</a>
                    </li>
                </ol>
            </div>
        </div>



        <div class="row">

            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <!-- <h5 class="card-header-text">Client Listing</h5> -->

                          <form action="{{ URL::to('excellistclientdownload') }}"  method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="submit" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel" value="Download Excel" name="downexcel">
                                             
                                            </input>
                                        </form>


                  
                        @if(Session::has('success_msg'))
                            <div class="alert alert-success">
                                {{ Session::get('success_msg') }}
                            </div>
                        @endif

                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 ">
                            
                             <div class="tablle-all">
                             <div class="inner-ty">
                             <div id="search_filter_filter" style="float:left" class="dataTables_filter"><label>Search:<input type="search" autofocus id="filter_area" style="border: 1px solid #968f8f;" class="" placeholder="" aria-controls="search_filter"></label></div>
                                <table class="table" id="">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Category</th>
                                        <th>Company Name</th>
                                        <th>Contact Person Name</th>
                                        <th>Designation</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>POC1_Name</th>
                                        <th>POC1_Designation</th>
                                        <th>POC1_Phone</th>
                                        <th>POC1_Email</th>
                                        <th>POC2_Name</th>
                                        <th>POC2_Designation</th>
                                        <th>POC2_Phone</th>
                                        <th>POC2_Email</th>
                                        <th>Summary</th>
                                        <th>Constractual_sla</th>
                                        <th>Assign SPOC</th>
                                        <th>Edit Client</th>
                                    </tr>
                                    </thead>
                                        <tbody id="suggesstion-box">
                                        @php
                                        $i=1;
                                        @endphp
                                        @if(!empty($clients))
                                        @foreach($clients as $kk => $c)
                                        <tr>
                                        <td>{{  $i++   }}</td>
                                        <td> @if($c->client_catid==1)
                                        {{ "IT" }}
                                        @else
                                        {{ "NON IT"  }}
                                        @endif
                                        </td>
                                        <td>{{  $c->comp_name }}</td>
                                        <td>{{  $c->contact_name }}</td>
                                        <td>{{ $c->designation }}</td>
                                        <td>{{  $c->phone }}</td>
                                        <td>{{ $c->email }}</td>
                                        <td>{{  $c->contact_name1 }}</td>
                                        <td>{{ $c->designation1 }}</td>
                                        <td>{{  $c->phone1 }}</td>
                                        <td>{{ $c->email1 }}</td>
                                         <td>{{  $c->contact_name2 }}</td>
                                        <td>{{ $c->designation2 }}</td>
                                        <td>{{  $c->phone2 }}</td>
                                        <td>{{ $c->email2 }}</td>
                                        <td>{{  strip_tags($c->summary) }}</td>
                                        <td>{{  $c->contractual_sla }}</td>
                                        <td><div class="btn-group btn-group-sm" style="float: none;">
                                        <button type="button" onclick="test('{{$c->client_id}}','{{$c->comp_name}}'),getspocclient('<?php echo $c->client_id ?>')" href="#success" data-toggle="modal" class="tabledit-edit-button sl_refresh btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span></button>
                                        </div>
                                        </td>
                                        <td>
                                        <a href="{{ URL::to('editclient/'.Crypt::encrypt($c->client_id)) }}">
                                        <div class="btn-group btn-group-sm" style="float:none;"> 
                                        <button type="button"  class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;">
                                        <span class="icofont icofont-ui-edit"></span>
                                        </button>
                                        </div>
                                        </a>
                                        </td>
                                        </tr>
                                    @endforeach
                                    @endif
                                    </tbody>
                                    </table>
                               </div>
                            </div>
                        </div>


    </div>
    </div>
    </div>
    </div>
    
        <script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script> 
    </div>

    @if(!empty($c))
    <div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header modal-header-success">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h1><i class="glyphicon glyphicon-thumbs-up"></i>Assign SPOC</h1>
    </div>
    <div class="modal-body">
    <form action="{{ route('assignspoc') }} " method="POST"  name="assign" >
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group row">
    <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Company Name</label>
    <div class="col-sm-9">
    <input class="form-control" type="text"  id="pop_company_name" name="cname" id="cname" value=" " >
     <!-- {{$c->client_id}} -->
    <input type="hidden"  id="client_id" name="client_id"  value="" >
    </div>
    </div>


 <div class="form-group row">
    <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Employee List</label>
      <div class="col-sm-9">
       <div class="form-group">
         <select id="empid" multiple="multiple"  name="empid[]" class="multiselect-ui form-control " required>
 </select>
    <span class="text-danger">{{ $errors->first('empid') }}</span>



    </div>

    </div>

    </div>

    </div>

    <div class="modal-footer">

    <div class="md-input-wrapper">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <input type="submit" class="btn btn-primary waves-effect waves-light" value="submit" onclick="checkdata('{{$c->client_id}}')">
    </input>

    </div>
    </div>

    </form>
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    @endif
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
        <script>

            $(function() {
                $("form[name='assign']").validate({
                    rules: {
                        empid: {
                            required: true,
                        },
                    },
                    messages: {
                        empid: "Please Select Spoc",
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
            });



            function test(id,c_name){

                $.ajax({
                    url: "{{route('listclient')}}",
                    type: "GET",
                    data: 'data='+id,
                    beforeSend: function() {
                        //alert('vandana1');

                    },
                    success: function(response) {
                        $('#pop_company_name').val(c_name);
                        $('#client_id').val(id);
                        ('.fetched-data').html(response);
                    },
                    error: function(xhr) {
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    }
                });

            }


            function getspocclient(id)
            {

                $.ajax({
                    url: 'getspoctoclient/'+id,
                    type:'GET',
                    error: function(xhr) {
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    },
                    success: function(data)
                    {
                        // alert(data);
//                    debugger;
                        $('#empid').html(data);

                        $('#empid').multiselect('rebuild');

                        //alert(data);
                    }
                });


            }

            function getemployee(cid){
                var cid = cid;
                $.ajax({
                    url: 'getemployeebyclientid/'+cid,
                    type:'GET',
                    error: function(xhr) {
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    },
                    success: function(data)
                    {

                        $('#empid').html(data);
                        $('#empid').multiselect('rebuild');

                    }
                });
            }


            function checkdata(clientid)
            {

                var eid=$('#empid').val();


                if(eid!='')
                {
                    $.ajax({
                        url: 'assignspoc',
                        type: 'POST',
                        data:  $('assign').serialize(),
                        success: function(data)
                        {



                        }
                    });

                }
                else
                {


                }


            }


        </script>

@endsection