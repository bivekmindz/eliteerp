@extends('Emp.layouts.master')

@section('content')

<style type="text/css">
  #search_filter_filter{float:left!important;}
</style>


<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View CV List</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Client Call CV List</a>
                </li>
                <li class="breadcrumb-item"><a href="">Allocated CVs List Details</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
                @endif
                <div class="card-block">
                    <form action="{{ URL::to('allcvdetupdate') }}" method="post"> 
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                        <div id="search_filter_filter" class="dataTables_filter"><label><input type="submit" name="submit" value="change status"></label></div>
                            <table class="table" id="">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>S.No.</th>
                                    <th>CandidateName</th>
                                    <th>CV Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>FollowUp Status</th>
                                    <th>Action</th> 
                                </tr>
                                </thead>
                                <tbody id="suggesstion-box">
                                @php
                                $i=1;
                                @endphp
                                @foreach($cvdetails as $key => $p)
                               
                                <tr >
                                    <td><input type="checkbox" name="changestatus[]" value="{{$p->tca_id}}"></td>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ !empty($p->candidate_name) ? $p->candidate_name : 'N/A' }}</td>
                                    <td>{{ !empty($p->cv_name) ? $p->cv_name : 'N/A' }}</td>
                                    <td>{{ !empty($p->candidate_email) ? $p->candidate_email : 'N/A' }}</td>
                                    <td>{{ !empty($p->candidate_mob) ? $p->candidate_mob : 'N/A' }}</td>
                                    <td>{{ $p->tca_followupstatus }}</td>
                                    <td><!-- 
   <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="tcaid[]" value="{{$p->tca_id}}">
      <input type="hidden" name="asignfrom" value="{{$assignfrom}}">
        <input type="hidden" name="asignto" value="{{$assignto}}">
                                          <select name="changestatusval[]" onchange="changestatusdata({{$p->tca_id}})"id="select_id{{$p->tca_id}}">
                                            <option>Change Status</option>
                                            <option value="on the way" <?php if($p->tca_followupstatus=='on the way' ) { ?> selected <?php } ?> >on the way</option>
                                            <option value="Interviewed" <?php if($p->tca_followupstatus=='Interviewed' ) { ?> selected <?php } ?> >Interviewed</option>
                                            <option value="Not Going" <?php if($p->tca_followupstatus=='Not Going' ) { ?> selected <?php } ?> >Not Going</option>
                                            <option value="Offered" <?php if($p->tca_followupstatus=='Offered' ) { ?> selected <?php } ?> >Offered</option>
                                            <option value="Joined" <?php if($p->tca_followupstatus=='Joined' ) { ?> selected <?php } ?> >Joined</option>
                                            <option value="Declined" <?php if($p->tca_followupstatus=='Declined' ) { ?> selected <?php } ?> >Declined</option>
                                        </select>

<div id="place{{$p->tca_id}}"></div> -->
{{ !empty($p->clientstattus) ? $p->clientstattus : 'N/A' }}
                                    </td>

                                    <td class='title'><button type='button'  class='det_css' data-toggle='modal' data-target='#myModal' onclick='getposiionvalue(<?php echo $p->position_id; ?>),getupdatespock(<?php echo $p->id; ?>,<?php echo $p->recruiter_id; ?>,<?php echo $p->cv_status; ?>,<?php echo $p->position_id; ?>)(this.value),getpositionspock(<?php echo $p->id; ?>,<?php echo $p->recruiter_id; ?>,<?php echo $p->cv_status; ?>)' ><span class='icofont icofont-eye-alt'></span></button> </td>







                </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
</div>
</div>


<div class="container">
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Status</h4>
        </div>

           <form method="post" action="{{ URL::to('updateclientstatuspopup') }}">
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="form-group">
                          <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Select Status:

                          </label>
                          <select class="form-control col-xs-6" required id="recruiterspockup" name="recruiter[]"  onchange="changestatuspopupdata(this.value)" >
                             <option value="">Select Status</option>                   
                          </select>
                            <input type="text" name="clientreq_posid" id="placepopupval" >
                          <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                        <div id="placepopup"></div>
                     
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="button" class="btn btn-default" value="Submit" id="buttonClassstpop">
            </div>
        </form>



        <div id=""></div>
      
      </div>      
    </div>
  </div> 

  </div>

<script>
  
        $("#buttonClassstpop").click(function() {

var sel = document.getElementById('recruiterspockup').value;
var expectedsaleryval = document.getElementById('expectedsalery').value;
var Doj = document.getElementById('Doj').value;
var expDoj = document.getElementById('expDoj').value;
var count = document.getElementById('count').value;
var countval = document.getElementById('countval').value;
var placepopupval = document.getElementById('placepopupval').value;
//alert(placepopupval);

   $.ajax({
                      type: 'POST',
                      url: '{{URL::route('updatespockresume')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'sel':sel,
                        'expectedsalery':expectedsaleryval,
                        'Doj':Doj,
                        'expDoj':expDoj,
                        'count':count,
                          'countval':countval,
                        'placepopupval':placepopupval,
                      },
                      success: function(data){
                      // alert(data);
                       $('#myModal').modal('hide');
                    }
                    });


 

           });
    

</script>
  <script>

function changestatuspopupdata(id)
{
var str = id;
var res = str.split("/");
//alert(str);

//Then read the values from the array where 0 is the first
var myvar = res[0] + ":" + res[1] + ":" + res[2];
  // d = document.getElementById("select_id").value;
  if(res[2]=='4')
  {
 document.getElementById("placepopup").innerHTML="<input type='text' name='expectedsalery[]'  placeholder='Expected Salary'  class='form-control' id='expectedsalery' ><input type='date' name='Doj[]' class='form-control'  placeholder='Date Of Joining' id='Doj'><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining' id='expDoj' class='form-control'  ><input type='hidden' name='count[]' class='form-control col-xs-6'  value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  else

{


   if(res[2]=='5')
  {
 document.getElementById("placepopup").innerHTML="<input type='hidden' name='expectedsalery[]' class='form-control'  placeholder='Expected Salary' id='expectedsalery'><input type='hidden'class='form-control'  name='Doj[]'  placeholder='Date Of Joining'   id='Doj'><input type='date'  class='form-control' name='expDoj[]'  placeholder='Expected Date Of Joining'  id='expDoj'  ><input type='hidden' name='count[]' value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  else
  {


   if(res[2]=='6')
  {
    document.getElementById("placepopup").innerHTML="<input type='text' name='expectedsalery[]' class='form-control'  placeholder='Expected Salary'  id='expectedsalery'><input type='date' name='Doj[]'   class='form-control'  placeholder='Date Of Joining' id='Doj' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'    id='expDoj' ><input type='hidden' name='count[]' value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  else
  {
    document.getElementById("placepopup").innerHTML="<input type='hidden' name='expectedsalery[]' class='form-control col-xs-6' placeholder='Expected Salary' id='expectedsalery' ><input type='hidden' name='Doj[]'  placeholder='Date Of Joining'  id='Doj' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'  id='expDoj' ><input type='hidden' name='count[]' value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  }
}
 }

function getposiionvalue(id)
{

 var ids = id;

// alert(document.location.host);
    $.ajax({
        type:"GET",
         url:"getspockposition/"+ids,

        data:'',
        success: function(data){
        // alert(data);
        document.getElementById("placepopupval").value = data;

        },
        error: function(data){

        }
    });



      

}
/*

function getupdatespock(id,recruiter_id,cv_status,posid)
{

 var ids = id;
 var recruiterid = recruiter_id;
 var cvstatus = cv_status;
 var posid = posid;
 alert(cvstatus);
    $.ajax({
        type:"GET",
          url: "{{route('listclient')}}/"+ids+"/"+recruiterid+"/"+cvstatus+"/"+posid+"/",
     //   url:"getspockstatus/"+ids+"/"+recruiterid+"/"+cvstatus+"/"+posid+"/",
        data:'',
        success: function(data){
        // alert(data);
            $("#recruiterspockup").empty();
            $("#recruiterspockup").append(data);
          // $('#recruiterspockup').multiselect('rebuild');
        },
        error: function(data){

        }
    });

}*/


function getupdatespock(eid,recruiter_id,cv_status,posid)
{

 var idds = eid;
 var recruiterid = recruiter_id;
 var cvstatus = cv_status;
 var posid = posid;
 //alert(idds);
    $.ajax({
        type:"GET",
        url:"getspockstatusallcv/"+idds+"/"+recruiterid+"/"+cvstatus+"/"+posid+"/",
        data:'',
        success: function(data){
        // alert(data);
            $("#recruiterspockup").empty();
            $("#recruiterspockup").append(data);
          // $('#recruiterspockup').multiselect('rebuild');
        },
        error: function(data){

        }
    });



      

}
function add(ids)
{
   
    document.getElementById("place"+ids).innerHTML="<input type='text' value=''>"
}
</script>
<script type="text/javascript">
        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
</script>

<!-- <script type="text/javascript">
    function abc(){
        alert('hi');
    }
</script> -->
<script>
    
function changestatusdata(ids)
{
    d = document.getElementById("select_id"+ids).value;
   alert(d);
   if(d=='Offered' || d=='Offered' )
 //alert(value);
    document.getElementById("place"+ids).innerHTML="<textarea name='salery[]' placeholder='Enter data' value=' '></textarea><input type='hidden' name='count[]' value='1'>"
}


    function changestatus(ids)
    {
        var status=$('#status').val();
        //alert(ids);
        $.ajax({
            url: '{{URL::route('changecvstatus')}}',
            type: 'post',
            data:{
                '_token': "{{ csrf_token() }}",
                'key':ids,
                'status':status,
              },
            success: function(data)
            {
                if(data==1){
                    location.reload();
                }
            }
        });
    }

</script>

@endsection

