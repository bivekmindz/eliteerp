@extends('Emp.layouts.master')

@section('content')


<style type="text/css">
  .green-o{    color: #03561e;
    font-weight: 600; }
   .red-o{     color: #9a0f0f;
    font-weight: 600;}
    .dateclass {
  width: 100%;
}

.dateclass.placeholderclass::before {
  width: 100%;
  content: attr(placeholder);
}

.dateclass.placeholderclass:hover::before {
  width: 0%;
  content: "";
}
</style>
<script>

$(document).ready(function(){
    $("#filter_area").keyup(function(){
        var xxx = $(this).val();

       // alert(xxx);

          $.ajax({
                      type: 'POST',
                      url: '{{URL::route('cvshortlisting')}}',
                      data:{
                        '_token': "{{ csrf_token() }}",
                        'key':xxx,
                      },
                      success: function(data){
                     //  alert(data);
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);

                         
                      }
                    });



    
    });
});



// function selectCountry(val) {
// $("#search-box").val(val);
// $("#suggesstion-box").hide();
// }
</script><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- Container-fluid starts -->
<!-- Main content starts -->
<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>Shortlist Profile</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href=""><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="">Positions</a>
                </li>
                <li class="breadcrumb-item"><a href="">Shortlisting of CV</a>
                </li>
            </ol>
        </div>



    </div>

    <div class="row">

        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">SPOC Shortlisting</h5>

                </div>
                @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
                @endif
                <div class="card-block">
  <!-- <input type="text" class="" autofocus placeholder="" id="filter_area" >  -->

  <input id="myInput" type="text" placeholder="Search..">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">

         
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Position Name</th>
                                         <th>Company Name</th>
                                        <th>No. Of Positions</th>
                                         <th>Spoc Name</th>
                                        <th>Total Uploaded Profile</th>
                                        <th>View</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody id="myTable">

                                    @php
                                    $i=1;
                                    @endphp
                                    @foreach($pos as $key=>$value)
                                    <tr >
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $value->clientjob_title }}
                                            <input type="hidden" value="{{$value->clientjob_title}}"  id="pos">
                                        </td>
                                         <td>{{ $value->comp_name }}</td>
                                        <td>{{ $value->clientjob_noofposition }}</td>
                                          <td>{{ $value->name }}</td>

                                        <td>{{ $value->total_upload_profile }}</td>

                                        <td>
<button type="button" value="{{ $value->position_id  }}" class="det_css one-click{{ $value->position_id  }}" onclick="getcandidatedetails{{ $value->position_id  }}(this.value),getposition('<?php echo $value->clientjob_title ?>')" ><span class="icofont icofont-eye-alt"></span></button> 


                                         </td>



                                    </tr>

<!-- </tbody> -->

               <tr><td  colspan="6">
<style>
 .hide_css{{ $value->position_id  }}{ width:100%; float:left; display:none;}
 
 </style>
 {!! Form::open([ 'action'=>'Emp\CVController@savecandidate', 'method'=>'post', 'files'=>true ]) !!}

<table   class="table card hide_css{{ $value->position_id  }}"  >
                              
                                    <tr>
                                        <th>S.No</th>
                                          <th>Recuiter Name</th>
                                        <th>Candidate's Name</th>
                                        <th>CV Uploaded Time</th>
                                        <th>Mobile Number</th>
                                        <th>Download CV</th>
                                        <th>Qulification</th>
                                        <th>Total Experience</th>
                                        <th>Domain Experience </th>
                                        <th>Primary Skill</th>
                                        <th>Secondary Skill</th>
                                        <th>Current ORG</th>
                                        <th>Current CTC </th>
                                        <th>Expected CTC</th>
                                        <th>NP</th>
                                        <th>Designation</th>
                                        <th>Location</th>
                                        <th>Reason For Change</th>
                                        <th>Skills For Rating</th>
                                        <th>Relocation</th>
                                        <th>DOJ</th>
                                        <th>Remark</th>
                                        <th>Status</th>


                                    </tr>
                            
                                <tbody id="candilist{{ $value->position_id  }}">

                                </tbody>
                                <tr><td>
                                <input type="hidden" value="" id="poid" name="poid">

                        <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit">
                    </input></td></tr>
               
                            </table>
  

  {!! Form::close() !!}
          </td></tr>   

                               
<script>

   function getposition(id){
    var ids  = id;
   // alert(ids);
    $('#poid').val(ids);

}

function getcandidatedetails{{ $value->position_id  }}(val)
{


 var  id=val;

 $.ajax({
    url: 'getcandidatedetails/'+id,
    type:'GET',
    error: function(xhr) {
        alert("An error occured: " + xhr.status + " " + xhr.statusText);
    },
    success: function(data)
    {
     var data1= JSON.parse(data);

     var trHTML = '';
        var j=1;


                  //  $.each(data, function (i, data) {
                    for (i = 0; i < data1.length; i++) {
                       var str = data1[i]["created_at"];
                       var str = str.split(" ");
                       var aa =str[0].split("-");

                       var con="{{ config('constants.APP_URLS') }}";

                       var url=con+'/storage/app/uploadcv/'+aa[0]+"/"+aa[1]+"/"+aa[2]+"/"+data1[i]["cv_name"];
//alert(data1[i]["cv_status"]);
if(data1[i]["cv_status"]=='3')
{
data1[i]["cv_nameaa"]="<span class='red-o'>Rejected</span>";
}
if(data1[i]["cv_status"]=='4')
{
data1[i]["cv_nameaa"]="<span class='red-o'>Offered</span>";
}if(data1[i]["cv_status"]=='5')
{
data1[i]["cv_nameaa"]="<span class='red-o'>Piplined</span>";
}
if(data1[i]["cv_status"]=='2')
{
  data1[i]["s1"]=1;
  data1[i]["s2"]=2;
  data1[i]["s3"]=3;
  data1[i]["s4"]=4;
   data1[i]["s5"]=5;
    data1[i]["s6"]=6;
data1[i]["cv_nameaa"]='<select name= "checkbox_data[]" onchange="changestatusdatasuccess(this.value)"  id="foo"><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s2"]+' selected>Send To Client</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s4"]+'>Offered</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s5"]+'>Pipelined</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s6"]+'>Joined</option></select>';
}
if(data1[i]["cv_status"]=='1')
{
  data1[i]["s1"]=1;
  data1[i]["s2"]=2;
  data1[i]["s3"]=3;
  data1[i]["s4"]=4;
   data1[i]["s5"]=5;
    data1[i]["s6"]=6;
data1[i]["cv_nameaa"]='<select name= "checkbox_data[]" onchange="changestatusdata(this.value)"  id="foo"><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s1"]+'>Not Seen</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s2"]+'>Send To Client</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s3"]+'>Rejected</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s4"]+'>Offered</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s5"]+'>Pipelined</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s6"]+'>Joined</option></select>';
}
                       trHTML += "<tr>" +
                       "<td>"+j+++"</td>" +
                        "<td>"+data1[i]["username"]+"</td>" +
                       "<td>"+data1[i]["candidate_name"]+"</td>" +
                       "<td>"+data1[i]["cv_uploadtime"]+"</td>" +
                       "<td>"+data1[i]["candidate_mob"]+"</td>" +
                       '<td><a href='+url+' download>   ' +'Download </a></td>' +
                       "<td>"+data1[i]["highest_qulification"]+"</td>" +
                       "<td>"+data1[i]["total_exp"]+"</td>" +
                       "<td>"+data1[i]["domain_exp"]+"</td>" +
                       "<td>"+data1[i]["primary_skill"]+"</td>" +
                       "<td>"+data1[i]["secondary_skill"]+"</td>" +
                       "<td>"+data1[i]["current_org"]+"</td>" +
                       "<td>"+data1[i]["current_ctc"]+"</td>" +
                       "<td>"+data1[i]["expected_ctc"]+"</td>" +
                       "<td>"+data1[i]["np"]+"</td>" +
                       "<td>"+data1[i]["desng"]+"</td>" +
                       "<td>"+data1[i]["location"]+"</td>" +
                       "<td>"+data1[i]["reason_for_change"]+"</td>" +
                       "<td>"+data1[i]["communication_skills_rating"]+"</td>" +
                       "<td>"+data1[i]["relocation"]+"</td>" +
                       "<td>"+data1[i]["doj"]+"</td>" +
                       "<td>"+data1[i]["remark"]+"</td>" +
                       "<td>"+data1[i]["cv_nameaa"]+"<div id='place"+data1[i]["id"]+""+data1[i]["recruiter_id"]+"'></div></td>" +
                      
                       '</tr>';
                   }
             //      });
                   // console.log(data.length);
             $('#candilist'+id).html(trHTML);
 
 /*$(".hide_css{{ $value->position_id }}").slideToggle(700);*/


         }




     });
$('#foo').on("change",function(){
    var dataid = $("#foo option:selected").attr('data-id');
    alert(dataid)
});
}$(".one-click{{ $value->position_id }}").click(function(){

//  alert("dddddddddddd");
        $(".hide_css{{ $value->position_id }}").slideToggle(700);
    });

function changestatusdata(id)
{
var str = id;
var res = str.split("/");
alert(str);

//Then read the values from the array where 0 is the first
var myvar = res[0] + ":" + res[1] + ":" + res[2];
  // d = document.getElementById("select_id").value;
  if(res[2]=='4')
  {
 document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='text' name='expectedsalery[]'  placeholder='Expected Salery' ><input type='date' name='Doj[]'    placeholder='Date Of Joining' class='dateclass placeholderclass' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'   ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else

{


   if(res[2]=='5')
  {
 document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='hidden' name='expectedsalery[]'  placeholder='Expected Salery' ><input type='hidden' name='Doj[]'  placeholder='Date Of Joining'  ><input type='date' class='dateclass placeholderclass' name='expDoj[]'  placeholder='Expected Date Of Joining'  ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else
  {


   if(res[2]=='6')
  {
    document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='text' name='expectedsalery[]'  placeholder='Expected Salery' ><input type='date' name='Doj[]'   class='dateclass placeholderclass' placeholder='Date Of Joining' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'   ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else
  {
    document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='hidden' name='expectedsalery[]'  placeholder='Expected Salery' ><input type='hidden' name='Doj[]'  placeholder='Date Of Joining'   ><input type='hidden' name='expDoj[]'   placeholder='Expected Date Of Joining' ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  }
}
 }


 function changestatusdatasuccess(id)
{
var str = id;
var res = str.split("/");
alert(str);

//Then read the values from the array where 0 is the first
var myvar = res[0] + ":" + res[1] + ":" + res[2];
  // d = document.getElementById("select_id").value;
  if(res[2]=='4')
  {
 document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='text' name='expectedsalery[]'  placeholder='Expected Salery' ><input type='date' name='Doj[]'  class='dateclass placeholderclass'  placeholder='Date Of Joining' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'   ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else

{


   if(res[2]=='5')
  {
 document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='hidden' name='expectedsalery[]'  placeholder='Expected Salery' ><input type='date' class='dateclass placeholderclass' name='Doj[]'  placeholder='Date Of Joining'  ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'  ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else
  {
     if(res[2]=='6')
  {
    document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='text' name='expectedsalery[]'  placeholder='Expected Salery' ><input type='date' class='dateclass placeholderclass' name='Doj[]'    placeholder='Date Of Joining' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'   ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else
  {
    document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='hidden' name='expectedsalery[]'  placeholder='Expected Salery' ><input type='hidden' name='Doj[]'  placeholder='Date Of Joining'   ><input type='hidden' name='expDoj[]'   placeholder='Expected Date Of Joining' ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  }
}
 }
</script>

                                    @endforeach

                               
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>






</div><script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script>

<script type="text/javascript">

    $("input:checkbox:not(:checked)").each(function () {
        alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
    });

</script>




@endsection