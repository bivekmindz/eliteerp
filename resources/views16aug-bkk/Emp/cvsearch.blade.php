@extends('Emp.layouts.master')

@section('content')
    

        <style>
            .ih{background-color: #fff;
                border: 1px solid #bdbdbd;
                /* padding: 15px 0px; */
                height: 34px;}
            .ihi{margin: 5px 0px 27px 13px;}
            endTime{
                display: none;
            }
        </style>
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <h4>Postion Resume Upload</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                        </li>
                        <li class="breadcrumb-item"><a href="#">Postion </a>
                        </li>
                        <li class="breadcrumb-item"><a href="">Postion List</a>
                        </li>
                    </ol>
                </div>
            </div>
           <div class="row">
                      <!-- Textual inputs starts -->
                    <div class="col-lg-12">
                        <div class="card">
                        
                        
                            <div class="card-header"><h5 class="card-header-text">Requirement you are hiring for</h5>
                                
                            </div>
                            
                            
                            
                            <!-- end of modal -->
                            <div class="card-block">
                            
                            
    
                           
                                
                                <div class="row">
                                
                  
                                 <div class="col-lg-12">
                                 <div class="form-group row">
                                       <div id="single_search">
                                         <form action="{{ URL::to('cvparsing/profiles') }}" method="get">
                                        <div class="col-sm-12">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                                           <textarea class="form-control" name="search_data" id="search_data" rows="1"></textarea>
                                        </div>
                                         <div class="col-sm-12">
                                          <div class="md-input-wrapper">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light" >Submit
                                    </button><p style="float: right;"><a href="javascript:void(0)" onclick="solr_search()">Skip and Continue</a></p>
                                </div>
                                 </div>
                                    </form>
                                    </div>

                                        <div id="advance_search" class="advanc-pp" style="display: none;">
                            <form action="{{ URL::to('adsearch') }}" method="get">  
                            
                                  <div class="form-group row">
                                         <div class="col-sm-3">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                           All Keywords
                                        </div>
                                        <div class="col-sm-9">
                                           <textarea class="form-control" name="all_keywords" id="all_keywords" rows="1"></textarea>
                                        </div>
                                      
                                    </div>
                                <!--    <div class="form-group row">
                                         <div class="col-sm-3">
                                             
                                           Any Keywords
                                        </div>
                                        <div class="col-sm-9">
                                           <textarea class="form-control" name="any_keywords" id="any_keywords" rows="1"></textarea>
                                        </div>
                                      
                                    </div> -->
                                
                                   <div class="form-group row">
                                         <div class="col-sm-3">
                                            
                                           Excluding Keywords
                                        </div>
                                        <div class="col-sm-9">
                                           <textarea class="form-control" name="exc_keywords" id="exc_keywords" rows="1"></textarea>
                                        </div>
                                      
                                    </div>
                                    <div class="form-group row">
                                         <div class="col-sm-3">
                                           
                                          Total Experience
                                        </div>
                                        <div class="col-sm-3">
                                            <select class="form-control" name="exp_min">
                                            <option value="">Min</option>
                                            <?php for ($y=0; $y < 21; $y++) { 
                                                echo '<option value="'.$y.'.0">'.$y.' Year</option>'; } ?>
                                            </select>
                                          
                                        </div> <div class="col-sm-1">
                                           To
                                        </div>
                                         <div class="col-sm-3">
                                           <select class="form-control" name="exp_max">
                                            <option value="">Max</option>
                                            <?php for ($m=0; $m < 12; $m++) { 
                                                echo '<option value="'.$m.'.0">'.$m.' Year</option>'; } ?>
                                            </select>
                                        </div> <div class="col-sm-2">
                                           In Year
                                        </div>
                                      
                                    </div>
                                    <div class="form-group row">
                                         <div class="col-sm-2">
                                            
                                           Annual Salary
                                        </div>
                                         <div class="col-sm-1">
                                           Rs
                                        </div>
                                        <div class="col-sm-2">
                                           <select class="form-control" name="sal_min">
                                            <option value="">Lac</option>
                                            <?php for ($l=0; $l < 21; $l++) { 
                                                echo '<option value="'.$l.'">'.$l.' Lac</option>'; } ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                           <select class="form-control" name="sal_min1">
                                            <option value="">Thousand</option>
                                            <?php $t=0;
                                                for ($t=0; $t < 100; $t++) {
                                                echo '<option value="'.$t.'">'.$t.' Thousand</option>'; 
                                            $t=$t+9;} ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-1">
                                           To
                                        </div>
                                        <div class="col-sm-2">
                                          <select class="form-control" name="sal_max">
                                            <option value="">Lac</option>
                                            <?php for ($l1=0; $l1 < 21; $l1++) { 
                                                echo '<option value="'.$l1.'">'.$l1.' Lac</option>'; } ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                          <select class="form-control" name="sal_max1">
                                            <option value="">Thousand</option>
                                            <?php $t1=0;
                                                for ($t1=0; $t1 < 100; $t1++) {
                                                echo '<option value="'.$t1.'">'.$t1.' Thousand</option>'; 
                                            $t1=$t1+9;} ?>
                                            </select>
                                        </div>

                                      
                                    </div>
                                 <div class="form-group row">
                                         <div class="col-sm-3">
                                            
                                           Current Location
                                        </div>
                                        <div class="col-sm-9">
                                           <textarea class="form-control" name="current_loc" id="current_loc" rows="1"></textarea>
                                        </div>
                                      
                                    </div>
                                    <div class="col-sm-12">
                                      <div class="md-input-wrapper">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light" >Submit
                                    </button>
                                   
                                    <p style="float: right;"><a href="javascript:void(0)" onclick="solr_searchCan()">Cancel</a></p>
                                    </div> </div>
                             
                                     </form>

                                </div>









                                 </div>
                                  </div>
                                 
                                 
                                
                                </div>
                              
                               
                                
                               
                                
                                
                                 
                            </div>
                        </div>
                    </div>
                    <!-- Textual inputs ends -->
             </div>

        </div>
        <script type="text/javascript">

            function solr_search()
            {
                $('#single_search').css('display','none');
                $('#advance_search').css('display','block');
            }
            function solr_searchCan()
            {
                $('#single_search').css('display','block');
                $('#advance_search').css('display','none');
            }
            
          
        </script>
        <!-- <script type="text/javascript">
       
function search(name){
  var name=$('#search_data').val();
   alert(name);

                $.ajax({
                    url: "cvparsing/profiles",
                    type: "get",
                    data: 'data='+name,
                    beforeSend: function() {
                        //alert('vandana1');

                    },
                    success: function(response) {
                      //alert(response);
                        // $('#pop_company_name').val(c_name);
                        // $('#client_id').val(id);
                        // ('.fetched-data').html(response);
                    },
                    error: function(xhr) {
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    }
                });

            }
</script> -->


@endsection







