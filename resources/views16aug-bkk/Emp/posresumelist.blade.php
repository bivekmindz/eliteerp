@extends('Emp.layouts.master')

@section('content')

<style type="text/css">
  #search_filter_filter{float:left!important;}
</style>
<script>
$(document).ready(function(){

        document.getElementById("filter_area").focus();
    });
$(document).ready(function(){
    $("#filter_area").keyup(function(){
        var xxx = $(this).val();

    //   alert(xxx);
          $.ajax({
                      type: 'POST',
                      url: '{{URL::route('ajaxpositiondrive')}}',
                      data:{
                        '_token': "ujUuPBYTGWx8smDtoEX1rlR3u8GlIVJdi4HBmHKw",
                        'key':xxx,
                      },
                      success: function(data){
                  //    alert(data);
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                      }
                    });
    });
});
</script>


<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View CV List</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Client Call CV List</a>
                </li>
                <li class="breadcrumb-item"><a href="">CV List</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               @if(Session::has('success_msg'))
                <div class="alert alert-success">
                    {{ Session::get('success_msg') }}
                </div>
                @endif
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                        <div id="search_filter_filter" class="dataTables_filter"><label>Search: <input type="search" id="filter_area" style="border: 1px solid #968f8f;" class="" placeholder="" aria-controls="search_filter" autofocus></label></div>
                            <table class="table" id="">
                                <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Company Name</th>
                                    <th>Position Name</th>
                                    <th>No of Line UP</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="suggesstion-box">
                                @php
                                $i=1;
                                @endphp
                                @foreach($client_pos_resumes as $key => $p)
                                <tr class="table-active" >
                                    <td>{{ $i++ }}</td>
                                    <td>{{ !empty($p->comp_name) ? $p->comp_name : 'N/A' }}</td>
                                    <td>{{ !empty($p->clientjob_title) ? $p->clientjob_title : 'N/A' }}</td>
                                    <td>{{ !empty($p->total_rec_cv) ? $p->total_rec_cv : 'N/A' }}</td>
                                    <td>
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal" onclick="getclientreq_id(<?php echo $p->clientjob_compid;?>,<?php echo $p->clientjob_id;?>);">Assign</button>    
                                    </td>

                </tr>
                @endforeach
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
</script>
@endsection
<div class="container">
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Assigned To</h4>
        </div>
        <form method="post" action="{{ URL::to('assigncvcall') }}">
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="form-group">
                          <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Select Employee:</label>
                          <select class="multiselect-ui form-control col-xs-6" multiple="multiple" required id="recruiter" name="recruiter[]">
                            @foreach($emp_list as $keyr => $empval)
                            <option value="{{ $empval->id }}">{{ $empval->name }}</option>
                            @endforeach
                          </select>
                          <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
                          <input type="hidden" name="positionreq_id" id="positionreq_id" value="">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-default" value="Submit">
            </div>
        </form>
      </div>      
    </div>
  </div> 
</div>

<script>
function getclientreq_id(clientreq_id,positionreq_id)
{
    $("#clientreq_id").val(clientreq_id);
    $("#positionreq_id").val(positionreq_id);
}
</script>