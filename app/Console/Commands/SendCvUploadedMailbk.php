<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use Carbon\Carbon;

class SendCvUploadedMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendCv:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is used to send cv uploaded mail at 2 and 6';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dt=Carbon::now();


        // $report= DB::table('tbl_recruiter_cv as rr')
        //     ->join('users as u','u.id','=','rr.recruiter_id')
        //     ->join('tbl_clientjd_master as cjm','rr.position_id','=','cjm.clientjob_id')
        //     ->join('users as us','cjm.clientjob_empid','=','us.id')
        //     ->select('u.name as recruiter_name','u.id','cjm.clientjob_title as pos','cjm.clientjob_id',DB::raw('count(rr.position_id) as upload_cv'))
        //     ->whereRaw('Date(rr.created_at) = CURDATE()')
        //     ->groupBy('rr.position_id','rr.recruiter_id','u.name','cjm.clientjob_title','us.name','u.id','cjm.clientjob_id')

        //     ->get();
            
            
            
             $report= DB::table('tbl_recruiter_cv as rr')
            ->join('users as u','u.id','=','rr.recruiter_id')
            ->join('tbl_clientjd_master as cjm','rr.position_id','=','cjm.clientjob_id')
           ->join('users as us','cjm.clientjob_empid','=','us.id')
            ->select('u.name as recruiter_name','u.id','cjm.clientjob_title as pos','cjm.clientjob_id',DB::raw('count(rr.position_id) as upload_cv'),'us.name as spoc',DB::raw("(SELECT count(*) FROM tbl_recruiter_cv as rrr
                          WHERE rrr.recruiter_id = u.id and rrr.position_id=cjm.clientjob_id and rrr.cv_status=2
                        ) as sent_to_client"))
           ->whereRaw('Date(rr.created_at) = CURDATE()')
           ->groupBy('rr.position_id','rr.recruiter_id','u.name','cjm.clientjob_title','us.name','u.id','cjm.clientjob_id')

           ->get();
       // echo $report;die;

//        $to=DB::table('users as u')
//                ->where('emp_role',4)
//                ->select('email')
//                ->get();
//        echo $to;die;


        $reports=[ 'report'=> $report,
        ];


        // return view('Emp.mails.dailyreport',['mail_data'=>$reports]);

        $sendmail= $this->sendMail($reports);

    }

    public  function sendMail($reports)
    {
         $tomail= DB::table('users')->select('email','name')->first();
        // dd($tomail);
         $to=$tomail->email;
        // dd($to);
         $toname=$tomail->name;

       // $to='shalini@mindztechnology.com';
       // $toname="shalini";
        $from='bivek@mindztechnology.com';


        Mail::send('Emp.mails.dailyreport',  ['mail_data' => $reports], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Daily Submission Report');

        });
    }
}
