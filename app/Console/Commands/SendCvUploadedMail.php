<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use Carbon\Carbon;

class SendCvUploadedMail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendCv:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is used to send cv uploaded mail at 2 and 6';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dt=Carbon::now();


        // $report= DB::table('tbl_recruiter_cv as rr')
        //     ->join('users as u','u.id','=','rr.recruiter_id')
        //     ->join('tbl_clientjd_master as cjm','rr.position_id','=','cjm.clientjob_id')
        //     ->join('users as us','cjm.clientjob_empid','=','us.id')
        //     ->select('u.name as recruiter_name','u.id','cjm.clientjob_title as pos','cjm.clientjob_id',DB::raw('count(rr.position_id) as upload_cv'))
        //     ->whereRaw('Date(rr.created_at) = CURDATE()')
        //     ->groupBy('rr.position_id','rr.recruiter_id','u.name','cjm.clientjob_title','us.name','u.id','cjm.clientjob_id')

        //     ->get();
            
            
            
         /*    $report= DB::table('tbl_recruiter_cv as rr')
            ->join('users as u','u.id','=','rr.recruiter_id')
            ->join('tbl_clientjd_master as cjm','rr.position_id','=','cjm.clientjob_id')
           ->join('users as us','cjm.clientjob_empid','=','us.id')
            ->select('u.name as recruiter_name','u.id','cjm.clientjob_title as pos','cjm.clientjob_id',DB::raw('count(rr.position_id) as upload_cv'),DB::raw("(SELECT count(rrr.position_id) FROM tbl_recruiter_cv as rrr
                          WHERE  rrr.position_id=cjm.clientjob_id  and Date(rrr.created_at) = CURDATE()
                        ) as totalupload_cv"),'us.name as spoc',DB::raw("(SELECT count(*) FROM tbl_recruiter_cv as rrr
                          WHERE rrr.recruiter_id = u.id and rrr.position_id=cjm.clientjob_id and rrr.cv_status=2
                        ) as sent_to_client"))
          ->whereRaw('Date(rr.created_at) = CURDATE()')
           ->groupBy('rr.position_id','rr.recruiter_id','u.name','cjm.clientjob_title','us.name','u.id','cjm.clientjob_id')

           ->get();*/

           $report= DB::table('tbl_clientjd_master as cjm')
 ->select('cjm.clientjob_id','uss.name as spoc','cjm.clientjob_title','clients.comp_name','cjm.clientjob_empid',DB::raw("(SELECT group_concat(us.name) FROM users as us join tbl_recruiter_cv as reqmap
                 WHERE  reqmap.recruiter_id=us.id and cjm.clientjob_id=reqmap.position_id and Date(reqmap.created_at) = CURDATE()
         ) as recruiter_name"),DB::raw("(SELECT GROUP_CONCAT(cnt) cnt FROM ( SELECT COUNT(*) cnt
    FROM tbl_recruiter_cv as cv join tbl_clientjd_master as cj where cv.position_id=cj.clientjob_id and Date(cv.created_at) = CURDATE() GROUP BY recruiter_id) q ) as upload_cv"),DB::raw("(SELECT GROUP_CONCAT(cnt) cnt FROM ( SELECT COUNT(*) cnt
    FROM tbl_recruiter_cv as rrr join tbl_clientjd_master as cj where rrr.position_id=cj.clientjob_id and Date(rrr.created_at) = CURDATE() and rrr.cv_status=2 GROUP BY recruiter_id) q ) as sent_to_client"),DB::raw("(SELECT count(rrr.position_id) FROM tbl_recruiter_cv as rrr
                          WHERE  rrr.position_id=cjm.clientjob_id   and Date(rrr.created_at) = CURDATE()
                        ) as totalupload_cv"))
 ->join('tbl_clients as clients','clients.client_id','=','cjm.clientjob_compid')
 ->join('tbl_recruiter_cv as rr','rr.position_id','=','cjm.clientjob_id')
  ->join('users as uss','cjm.clientjob_empid','=','uss.id')
    ->whereRaw('Date(rr.created_at) = CURDATE()')
  ->groupBy('rr.position_id','cjm.clientjob_id','cjm.clientjob_title','clients.comp_name','cjm.clientjob_id','cjm.clientjob_empid','uss.name')
 //->where('assignement_id',1)
 //->groupBy('rr.position_id','rr.recruiter_id','cjm.clientjob_title','cjm.clientjob_id')
->get();



       // echo $report;die;

//        $to=DB::table('users as u')
//                ->where('emp_role',4)
//                ->select('email')
//                ->get();
//        echo $to;die;


        $reports=[ 'report'=> $report,
        ];


        // return view('Emp.mails.dailyreport',['mail_data'=>$reports]);

        $sendmail= $this->sendMail($reports);

    }

    public  function sendMail($reports)
    {
         $tomail= DB::table('users')->select('email','name')->first();
        // dd($tomail);
         // $to="bivek11@gmail.com";
         $to=$tomail->email;

        

 /*$report12= DB::table('tbl_clientjd_master as cjm')
 ->select('cjm.clientjob_id','uss.name as spoc','cjm.clientjob_title','clients.comp_name','cjm.clientjob_empid',DB::raw("(SELECT group_concat(us.name) FROM users as us join tbl_clientjdrecruitermap as reqmap
                 WHERE  reqmap.fk_empid=us.id and cjm.clientjob_id=reqmap.fk_jdid
         ) as recruiter_name"),DB::raw("(SELECT GROUP_CONCAT(cnt) cnt FROM ( SELECT COUNT(*) cnt
    FROM tbl_recruiter_cv as cv join tbl_clientjd_master as cj where cv.position_id=cj.clientjob_id GROUP BY recruiter_id) q ) as upload_cv"),DB::raw("(SELECT GROUP_CONCAT(cnt) cnt FROM ( SELECT COUNT(*) cnt
    FROM tbl_recruiter_cv as rrr join tbl_clientjd_master as cj where rrr.position_id=cj.clientjob_id and rrr.cv_status=2 GROUP BY recruiter_id) q ) as sent_to_client"),DB::raw("(SELECT count(rrr.position_id) FROM tbl_recruiter_cv as rrr
                          WHERE  rrr.position_id=cjm.clientjob_id  
                        ) as totalupload_cv"))
 ->join('tbl_clients as clients','clients.client_id','=','cjm.clientjob_compid')
 ->join('tbl_recruiter_cv as rr','rr.position_id','=','cjm.clientjob_id')
  ->join('users as uss','cjm.clientjob_empid','=','uss.id')
  ->groupBy('rr.position_id','cjm.clientjob_id','cjm.clientjob_title','clients.comp_name','cjm.clientjob_id','cjm.clientjob_empid','uss.name')
 //->where('assignement_id',1)
 //->groupBy('rr.position_id','rr.recruiter_id','cjm.clientjob_title','cjm.clientjob_id')
->get();
// WHERE  cjm.clientjob_empid=us.id
           dd($report12);*/
       //  dd($to);
         $toname=$tomail->name;

       // $to='shalini@mindztechnology.com';
       // $toname="shalini";
        $from='bivek@mindztechnology.com';


        Mail::send('Emp.mails.dailyreport',  ['mail_data' => $reports], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Daily Submission Report');

        });
    }
}
