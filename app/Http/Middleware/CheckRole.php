<?php
/**
 * Created by PhpStorm.
 * User: mindz
 * Date: 22/2/18
 * Time: 11:19 AM
 */
namespace  App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\Adminmodel\Role;
use App\Model\User;
use Illuminate\Support\Facades\URL;

use Flash;
use closure;
use Illuminate\Support\Facades\Session;



class CheckRole
{
    public function handle($request, Closure $next)
    {
        $route= $request->route()->uri;
       $url = URL::previous();
      // dd($url);


        if($request->route()->methods[0]=='POST')
        {
            return $next($request);
        }

        $group = $request->route()->parameters();
        //        dd($group);

        if(!empty($group))
        {
            return $next($request);
        }
        else
        {
            if(empty(Auth::user()->id))
            {
                return redirect()->route('login');
            }
            if(Auth::user()->emp_role==4)
            {
                return $next($request);
            }

            $menid=DB::table('tbl_menu as m')->select('id')->where('url',$route)->get();
            // dd($menid);
            $roleid= DB::table('tbl_menu_role_assigned as mr')
                ->select('mr.roleid')
                ->where('mr.menuid',$menid[0]->id)
                ->get();

            if($roleid->isEmpty())
            {
                return redirect()->route('unauthorized');
            }
            else
            {
                //  dd($roleid);
                foreach ($roleid as $key => $value)
                {
                    $menu_roleid[] = $value->roleid;
                }

                // dd($menu_roleid);

                $id = Auth::user()->id;
                $current_role = DB::table('users')
                    ->select('emp_role')
                    ->where('id', $id)
                    ->first();
                //dd($current_role);

                if (strpos($current_role->emp_role, ',') !== false)
                {
                    $emp_role = explode(',', $current_role->emp_role);

                    foreach ($emp_role as $key => $value)
                    {

                        if (in_array($value, $menu_roleid))
                        {
                            return $next($request);
                        } else
                        {
                            continue;
                        }

                    }


                } else
                {

                    $emp_role = explode(" ", $current_role->emp_role);
                    if (!empty(array_intersect($emp_role, $menu_roleid)))
                    {
                        return $next($request);
                    }

                }
            }


            return redirect()->route('unauthorized');
        }
    }




    public function hasRole($role)
    {

        $id =Auth::user()->id;
        dd($id);
        $current_role = DB::table('users')
            ->select('emp_role')
            ->where('id', $id)
            ->first();


        if (strpos($current_role->emp_role,',') !== false)
        {
            $emp_role = explode(',', $current_role->emp_role);
            if(in_array($role,$emp_role))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        else
        {
            if($current_role==$role)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }



    public function checkrole($request, Closure $next)
    {
        $route= $request->route()->uri;
        if(empty(Auth::user()->id))
        {
            return redirect()->route('login');
        }
        $roleid= DB::table('tbl_menu_role_assigned as mr')
            ->join('tbl_menu as m','m.id','=','mr.menuid')
            ->select('roleid')
            ->where('m.url',$route)
            ->get();

        $menu_roleid=$roleid->toArray();

        $id =Auth::user()->id;
        $current_role = DB::table('users')
            ->select('emp_role')
            ->where('id', $id)
            ->first();

        if (strpos($current_role->emp_role,',') !== false)
        {
            $emp_role = explode(',', $current_role->emp_role);

            foreach($emp_role as $key=>$value)
            {

                if(in_array($value,$menu_roleid))
                {
                    return $next($request);
                }
                else
                {
                    continue;
                }

            }




        }
        else
        {
            $emp_role=explode(" ",$current_role->emp_role);
            if(in_array($emp_role,$menu_roleid))
            {
                return $next($request);
            }

            return redirect()->route('unauthorized');

        }


    }

}