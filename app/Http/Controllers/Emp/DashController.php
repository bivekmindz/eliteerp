<?php

namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Notifications\PushNotif;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Model\Client as Client;
use App\Model\User as User;
use App\Model\Adminmodel\Department as Department;
use Carbon\Carbon;
use Session;
use Mail;

class DashController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile()
    {
        return view('Emp.profile'); 
    }

    public function reset()
    {
        $id = Auth::user()->id;
        $currentuser = User::find($id);
        $email = Auth::user()->email;
        
        $from = $email;
        $to = $email;
        Mail::send('Emp.mails.resetpassword',['mail_data' => 'Abc Singh'], function ($message) use ($from, $to) {
            $message->from($from);
            $message->to($to);
            $message->subject('Reset Password');
        });
    }

    public function passwordreset()
    {
        $id = Auth::user()->id;
        $currentuser = User::find($id);
        $email = Auth::user()->email;
        
        $from = $email;
        $to = $email;
        Mail::send('Emp.mails.resetpassword',['mail_data' => 'Abc Singh'], function ($message) use ($from, $to) {
            $message->from($from);
            $message->to($to);
            $message->subject('Reset Password');
        });
    }

    public function getthirtymindelay()
    {
        /*$picks=  DB::table('tbl_clientrec_start_endtime as st')
        
        ->select('st.*','us.*', DB::raw('(select (count(a.id)) from tbl_recruiter_cv as a where a.r_map_id= st.clientreq_id and Date(a.created_at)= "'.date('Y-m-d').'") as countuser'))
           ->join('users as us', 'us.id', '=', 'st.clientstart_recuiterid')
           ->whereRaw('Date(st.created_at) between "'.date('Y-m-d').'" and "'.date('Y-m-d').'"')
     //   ->where(['st.usersloginid'=>$id])
      //  ->whereRaw('Date(st.created_at)', '=', date('Y-m-d'))
        ->orderby('st.clientreq_id','desc')
        ->get();*/
      //  dd($picks);
$livetime = date('Y-m-d H:i:s ', time() - 1800);
            $t=date('Y-m-d H:i:s ',time());
//echo $livetime.'--'.$t; exit;
        $picks=  DB::table('tbl_clientrec_start_endtime as st')
        
        ->select('st.*','us.*', DB::raw('(select (count(a.id)) from tbl_recruiter_cv as a where a.r_map_id= st.clientreq_id and (a.created_at) between "'.$livetime.'" and "'.$t.'")   as countuser'))
           ->join('users as us', 'us.id', '=', 'st.clientstart_recuiterid')
         //   ->whereRaw('Date(st.created_at)', '=', date('Y-m-d'))
           ->whereRaw('st.clientstart_stoptime IS NULL')
           ->whereRaw('Date(st.created_at) between "'. date('Y-m-d').'" and "'. date('Y-m-d').'"')
     //   ->where(['st.usersloginid'=>$id])
      
        ->orderby('st.clientreq_id','desc')
        ->get();
        $html='';
        $html .='<marquee behavior="scroll" direction="left" style="
    background: #fff;
">';
        if(count($picks )>0) {
                 foreach($picks as $key => $val){
                        $to = \Carbon\Carbon::createFromFormat('H:i:s', $val->clientstart_starttime);
    $from = \Carbon\Carbon::createFromFormat('H:i:s', date('H:i:s'));
    $diff_in_seconds = $to->diffInSeconds($from);
    //print_r($diff_in_seconds); // Output: 325
    $diff_in_minutes = $to->diffInMinutes($from);
 //  print_r($diff_in_minutes); // Output: 5
    $diff_in_hours = $to->diffInHours($from);
    
    
          if($val->countuser==0) {
              if($diff_in_minutes>=30) {
 $html .=''.$val->name.' Facing issue to upload profile'."&nbsp;, &nbsp;";
}}}}
         $html .='</marquee>'; 

echo $html;
   // print_r($picks);
    }
    public function dashboardval(Request $request)
    {
     
        
        $id = Auth::user()->id;
        $currentuser = User::find($id);
        $email = Auth::user()->emp_empid;
     //  dd( $id);


        $picks=  DB::table('userslogin')
        
        ->select(DB::raw('count(usersloginid) as countuser'))
        ->where(['usersloginid'=>$id])
        ->where('createdon', '=', date('Y-m-d'))
        ->orderby('usersloginid','desc')
        ->first();

//dd($picks->countuser);
        if($picks->countuser==0)

        {
           $id1=DB::table('userslogin')->insert(
            [
            'usersloginname' => Auth::user()->name ,
            'usersloginid' => Auth::user()->id,
            'usersloginempid' => Auth::user()->emp_empid ,
            'userslogintime' => date('H:i:s'),   
            'createdon' => date("Y-m-d") ,
            'updatedon' =>  date("Y-m-d") ,
            
            ]
            );
       }           

       return redirect()->route('dashboard');
       //return view('Emp.dashboard');

   }



   public function index(Request $request)
   {
    
// Check User DOB & DOJ for Existing user

$assigneeid=Auth::user()->id;

# Dashboard Popup#

      $id = Auth::user()->id;
      $ser = "select count(id) as BirthDayGirl from users where emp_dob like '%".date('d-m')."%' and id='".$id."'";
      $c = DB::select(DB::raw($ser));
      $dashbd['hbd'] = $c[0]->BirthDayGirl;

  //

      $date  = date('d-m-Y');
      $date2  = date('d-m', strtotime($date));

      $sqli = "select count(id) as CC from users where emp_doj like '%".$date2."%' and emp_doj < '".$date."' and id='".$id."'";
      $querys = DB::select(DB::raw($sqli));
      $dashbd['han'] = $querys[0]->CC;

  //
$dashbd['singnature'] = DB::table('users')->select( DB::raw('COUNT(id) as count'))->where('id', '=', $assigneeid)->Where('verified','=','2')->get();

if ($request->isMethod('post')) {
   $dob = $request['dob'];
   $doj = $request['doj'];

   $query = DB::table('users')->where('id', $assigneeid)->update(['emp_dob'=>$dob, 'emp_doj'=> $doj, 'verified'=>1]);
    return redirect('/dashboard');
}

//dd($dashbdas['singnature']);


// End by Ashish



    $start = (date('D') != 'Mon') ? date('Y-m-d', strtotime('last Monday')) : date('Y-m-d');
    $finish = (date('D') != 'Sat') ? date('Y-m-d', strtotime('next Saturday')) : date('Y-m-d');
//echo $start."-----".$finish; 

    $id = Auth::user()->id;
    $currentuser = User::find($id);
    $email = Auth::user()->emp_empid;
    $emprole = Auth::user()->emp_role;

    if($emprole!='4') {
        
       $dashbd['adminclientdata']=  DB::table('tbl_clients as c')
       ->join('users as d','c.clientsuid','=','d.id')
       ->select('*','c.*')
       ->where('c.clientsuid','=',$id)
       ->where('c.client_status','!=',3)
       ->whereBetween('c.created_at', [$start, $finish])
       ->orderby('c.client_id','desc')
       ->get();
// echo "<pre>";
//             print_r($dashbd['adminclientdata']);die;


    $dashbd['tot_positionlist']=DB::table('tbl_clientjd_master as m')//clientjob_id
    ->join('tbl_clients as c','m.clientjob_compid','=','c.client_id')
    ->select(DB::raw('*','m.*'))
    ->where('m.clientjob_empid','=',$id)
    ->whereBetween('m.created_at', [$start, $finish])
    ->orderby('m.clientjob_id','desc')
    ->get();
    
}

else if($emprole=='7') 
{
   
  $dashbd['adminclientdata']=  DB::table('tbl_clients as c')
  ->join('users as d','c.clientsuid','=','d.id')
  ->select('*','c.*')
  ->where('c.client_status','!=',3)
  ->whereBetween('c.created_at', [$start, $finish])
  ->orderby('c.client_id','desc')
  ->get(); 
            // echo "<pre>";
            //  print_r($dashbd['adminclientdata']);die;  

  $admin_tot_positionlist= DB::table('tbl_clientjd_master as m')
  ->join('tbl_clients as c','m.clientjob_compid','=','c.client_id')
  ->select(DB::raw('m.*'))
  // ->where('m.clientjob_status',1)
  ->whereBetween('m.created_at', [$start, $finish])
  ->orderby('m.clientjob_id','desc')
  ->get();
}

else 
{
   
  $dashbd['adminclientdata']=  DB::table('tbl_clients as c')
  ->join('users as d','c.clientsuid','=','d.id')
  ->select('*','c.*')
  ->where('c.client_status','!=',3)
  ->whereBetween('c.created_at', [$start, $finish])
  ->orderby('c.client_id','desc')
  ->get(); 
            // echo "<pre>";
            //  print_r($dashbd['adminclientdata']);die;  

  $admin_tot_positionlist= DB::table('tbl_clientjd_master as m')
  ->join('tbl_clients as c','m.clientjob_compid','=','c.client_id')
  ->select(DB::raw('m.*'))
  // ->where('m.clientjob_status',1)
  ->whereBetween('m.created_at', [$start, $finish])
  ->orderby('m.clientjob_id','desc')
  ->get();
}

if(!empty($admin_tot_positionlist)){
     $dashbd['admin_tot_positionlist'] = $admin_tot_positionlist;
}else{
  $dashbd['admin_tot_positionlist'] = '';
}

$assigneeid=Auth::user()->id;

$dashbd['tot_client']=  DB::table('tbl_clients as c')
->join('tbl_department as d','c.client_catid','=','d.dept_id')
->select(DB::raw('count(c.client_id) as clientcount'))
->where(['c.clientsuid'=>$assigneeid])
->where('c.client_status', '!=', 3)
->orderby('client_id','desc')
->get();

$dashbd['tot_assignclient']=  DB::table('tbl_clients as c')
->join('tbl_department as d','c.client_catid','=','d.dept_id')
->select(DB::raw('count(c.client_id) as assignclientcount'))
->where(['c.clientsuid'=>$assigneeid])
->where('c.client_status', '=', 2)
->orderby('client_id','desc')
->get();

$dashbd['tot_spoc_assignclient']=DB::table('tbl_clientspockmap as m')
->join('users as u','m.assignee_emp_id','=','u.id')
->join('tbl_clients as c','c.client_id','=','m.fk_clientid')
->leftjoin('users as us','us.id','=','m.fk_empid')
->select(DB::raw('count(c.client_id) as spocclientcount'))
->where(['fk_empid'=>$assigneeid])
->get();        

        $dashbd['tot_position']=DB::table('tbl_clientjd_master as m')//clientjob_id
        ->join('tbl_clients as c','m.clientjob_compid','=','c.client_id')
        ->select(DB::raw('count(m.clientjob_id) as spoctotpos'))
        ->where(['clientjob_empid'=>$assigneeid])
        ->get();

        

        $dashbd['tot_assignpostion']= DB::table('tbl_clientjd_master as c')
        ->join('tbl_clientjdrecruitermap as m','c.clientjob_id','=','m.fk_jdid')
        ->join('users as u','u.id','=','m.fk_empid')
        ->join('tbl_clients as cm','cm.client_id','=','c.clientjob_compid')
        ->select(DB::raw('count(c.clientjob_id) as spoctotassignpos'))
        ->where(['c.clientjob_empid'=>$assigneeid])
        ->get();


// dd($dashbd['tot_assignpostion']);


        $dashbd['tot_assignpostion_rec']= DB::table('tbl_clientjd_master as c')
        ->join('tbl_clientjdrecruitermap as m','c.clientjob_id','=','m.fk_jdid')
        ->join('users as u','u.id','=','m.fk_empid')
        ->join('tbl_clients as cm','cm.client_id','=','c.clientjob_compid')
        ->select(DB::raw('count(c.clientjob_id) as totassignpos_rec'))
         ->where(['m.clientrequpdate_status'=>'1'])
        ->where(['m.fk_empid'=>$assigneeid])
        ->get();    



        $dashbd['tot_recuit_cv']= DB::table('tbl_recruiter_cv')
        ->select(DB::raw('count(id) as recuit_cv'))
        ->where(['recruiter_id'=>$assigneeid])
        ->get();   

        $dashbd['tot_recuit_cv_submission']= DB::table('tbl_recruiter_cv')
        ->select(DB::raw('count(id) as recuit_cv_submission'))
        ->where(['recruiter_id'=>$assigneeid,'cv_status'=>2])
        ->get();

        $dashbd['admin_tot_client']=  DB::table('tbl_clients as c')
        ->join('tbl_department as d','c.client_catid','=','d.dept_id')
        ->select(DB::raw('count(c.client_id) as adminclient'))
        ->where('c.client_status', '!=', 3)
        ->orderby('client_id','desc')
        ->get();          

        $dashbd['admin_tot_emp']= DB::table('users')
        ->select(DB::raw('count(id) as adminemp'))
        ->where('emp_status',1)
        ->where('emp_role','!=',4)
        ->get();

        $dashbd['admin_tot_dept']= DB::table('tbl_department')
        ->select(DB::raw('count(dept_id) as admindept'))
        ->where('dept_status',1)
        ->get();  

        $dashbd['admin_tot_team']= DB::table('tbl_team')
        ->select(DB::raw('count(id) as adminteam'))
        ->where('team_status',1)
        ->get();

        $dashbd['admin_tot_position']= DB::table('tbl_clientjd_master as m')
        ->join('tbl_clients as c','m.clientjob_compid','=','c.client_id')
        ->select(DB::raw('count(m.clientjob_id) as adminposition'))
        // ->where('m.clientjob_status',1)
        ->get();

        

            // echo "<pre>";
            // print_r($dashbd['admin_tot_positionlist']);die;  



        $dashbd['admin_tot_recuit_cv']= DB::table('tbl_recruiter_cv')
        ->select(DB::raw('count(id) as admin_recuit_cv'))
        ->get();              
        
        // dd($dashbd['admin_tot_client']);    

        return view('Emp.dashboard', $dashbd);

    }
    public function newclient()
    {
       $dept=Department::all();
$statedata=  DB::table('tbl_state as c')->select('*')->whereRaw('c.state_status!=2')->orderby('state_id','desc')->get();
        return view('Emp.newclient',compact('dept','statedata'));

    }

    public function saveclient(Request $request)
    {
//dd($request);
        $this->validate($request,[
            'cname' => 'required',
            'cpn' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required|numeric',
            'deptid' => 'required',
            'desig' => 'required',
            
            'input_img' =>'required'
            ],[
            'cname.required' => 'Company name field is required.',
            'cpn.required' => 'Contact person name is required',
            'email.required' => 'Email field is required',
            'phone.required' => 'Phone number field is required.',
            'desig.required' => ' Designation field is required',
            
            
            'deptid.required' => ' Select Category',
            ]);

        $assigneeid=Auth::user()->id;
        $postData = $request->all();
        //dd($postData);
        $client_obj= new Client;
        $client_obj->client_catid=$request['deptid'];
        $client_obj->comp_name = $request['cname'];
        $client_obj->phone=$request['phone'];
        $client_obj->contact_name=$request['cpn'];
        $client_obj->email=$request['email'];
        $client_obj->designation=$request['desig'];
        

        $client_obj->contact_name1= $request['cpn1'];
        $client_obj->email1=$request['email1'];
        $client_obj->phone1=$request['phone1'];
        $client_obj->designation1=$request['desig1'];
        $client_obj->contact_name2=$request['cpn2'];
        $client_obj->email2=$request['email2'];
        $client_obj->phone2=$request['phone2'];
        $client_obj->designation2=$request['desig2'];
        

        $client_obj->company_size=$request['company_size'];
        $client_obj->from_time=$request['from_time'];
        $client_obj->to_time=$request['to_time'];
        $client_obj->days_working=$request['days_working'];
        $client_obj->industry=$request['industry'];
        $client_obj->headquarters=$request['headquarters'];
        $client_obj->company_specialties=$request['company_specialties'];
        $client_obj->office_address=$request['office_address'];
        $client_obj->alternate_phone=$request['alternate_phone'];
        $client_obj->selling_point=$request['selling_point'];
        
        $client_obj->summary=$request['summary'];
        

        $image = $request->file('input_img');
        // echo "<pre>"; print_r($image );
        $finename = $image->getClientOriginalName();
        $input['logo'] =  $finename;
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['logo']);
     //   $this->postImage->add($input);
        // return back()->with('success','Image Upload successful');

        $client_obj->logo=$finename;
        $client_obj->contractual_sla=$request['contractual_sla'];

        $client_obj->clientsuid=$assigneeid;
        $client_obj->save();
        //$user=User::find(1);
       // $user->notify(new PushNotif());
$insertedId = $client_obj->id;
if(isset($_POST['location'])) {
for ($x = 0; $x < (count($_POST['location'])); $x++) {
         
DB::table('tbl_clientsdetails')->insert([
                    'clientsdetails_location'=> $_POST['location'][$x],
                    'clientsdetails_billingaddress'=> $_POST['billingaddress'][$x],
                    'clientsdetails_gst'  => $_POST['gst'][$x],
                     'clientsdetails_panno'  => $_POST['panno'][$x],
                      'clientsdetails_cityid'  => $_POST['cityid'][$x],
                    'clientsdetails_clientid' => $insertedId,
                     'clientsdetails_created'=> Carbon::now(),
                    'clientsdetails_update'=> Carbon::now()
                ]);
}


         // echo $_POST['location'][$x];  panno  cityid
        }


        Session::flash('success_msg', 'Client Added Successfully!');

        return redirect()->route('newclient');

    }


    public function clientlists(Request $request)
    {
       $assigneeid=Auth::user()->id;

       $clients=  DB::table('tbl_clients as c')
       ->select('c.*')
       ->where('c.client_status','!=',3)
       ->where('c.clientsuid','=',$assigneeid)
       ->orderby('c.client_id','desc')
       ->paginate(15);
         //  dd($clients);
       // $user=User::all();

       return view('Emp.clientlist',compact('clients','user'))
       ->with('i', (request()->input('page', 1) - 1) * 5);

   }

   public function listclient(Request $request)
   {
       $assigneeid=Auth::user()->id;


       $clients=  DB::table('tbl_clients as c')
       ->join('tbl_department as d','c.client_catid','=','d.dept_id')
       ->select('d.dept_name','c.*')
       ->where(['c.clientsuid'=>$assigneeid,'c.client_status'=>1])
       ->orderby('client_id','desc')
       ->get();
            //echo "<pre>";
            //print_r($clients);
       // $user=User::all();

       return view('Emp.listclient',compact('clients','user'))
       ->with('i', (request()->input('page', 1) - 1) * 5);

   }

   public function assignspoc(Request $request)
   {
    

    $this->validate($request,[
        'empid' => 'required',

        ],[
        'empid.required' => 'Select  Employees from the list.',
        ]);
    
    
    $clientid = $request['client_id'];

    
       // dd($clientid);
    $assigneeid=Auth::user()->id;
    for($k=0;$k <count($request['empid']);$k++)
    {
        //echo $request['empid'][$k]."<br>";
      $abc= explode('/',$request['empid'][$k]);
      $emprole= explode(',',$abc[1]);
    // dd($emprole);
      if(in_array("2", $emprole)==false)
      {

        array_push($emprole,2);
        $role=implode(',',$emprole);
            //dd($role);
        DB::table('users')
        ->where('id',$abc[0])
        ->update(['emp_role'=>$role]);

    }



    DB::table('tbl_clientspockmap')->insert([
        'fk_clientid'=>$clientid,
        'fk_empid'=>$abc[0],
        'assignee_emp_id'=>$assigneeid
        ]);

    DB::table('tbl_clients')
    ->where('client_id',$clientid)
    ->update(['client_status'=>2]);





}




Session::flash('success_msg', 'Spoc Allocated Successfully!');

return redirect()->route('listclient');



}


public function updateassignspoc(Request $request)
{
   

   $this->validate($request,[
       'empid' => 'required',

       ],[
       'empid.required' => 'Select  Employees from the list.',
       ]);
   
   
   $clientid = $request['client_id'];

   
       // dd($clientid);
   $assigneeid=Auth::user()->id;
   for($k=0;$k <count($request['empid']);$k++)
   {
        //echo $request['empid'][$k]."<br>";
      $abc= explode('/',$request['empid'][$k]);
      $emprole= explode(',',$abc[1]);
    // dd($emprole);
      if(in_array("2", $emprole)==false)
      {

        array_push($emprole,2);
        $role=implode(',',$emprole);
            //dd($role);
        DB::table('users')
        ->where('id',$abc[0])
        ->update(['emp_role'=>$role]);

    }



    DB::table('tbl_clientspockmap')->insert([
        'fk_clientid'=>$clientid,
        'fk_empid'=>$abc[0],
        'assignee_emp_id'=>$assigneeid
        ]);

          DB::table('tbl_clientjd_master')->where('clientjob_compid',$clientid)->update([
        'clientjob_empid'=>$abc[0]
        ]);


 //        DB::table('tbl_clientspockmap')
 // ->where('clientrole_id',$client_id)
 //        ->update([
 //            'fk_empid'=>$abc[0],
 //            'assignee_emp_id'=>$assigneeid
 //        ]);

        //  DB::table('tbl_clientspockmap')->insert([
        //     'fk_clientid'=>$clientid,
        //     'fk_empid'=>$abc[0],
        //     'assignee_emp_id'=>$assigneeid
        // ]);


    

 //        // DB::table('tbl_clients')
 //        //     ->where('client_id',$clientid)
 //        //     ->update(['client_status'=>2]);





}




Session::flash('success_msg', 'Spoc Update Successfully!');

return redirect()->route('clientspoc');



}

public function clientspoc()
{
    $assigneeid=Auth::user()->id;

        // $clientemp   =DB::table('tbl_clientspockmap as cs')
        //     ->join('tbl_clients as c','cs.fk_clientid','=','c.client_id')
        //     ->join('users as u','cs.fk_empid','=','u.id')
        //     ->select('cs.*','c.client_id','c.comp_name','u.name')
        //     ->where('cs.assignee_emp_id','=',$assigneeid)
        //     ->groupby('c.comp_name','u.name')

        //     ->get();
        //   dd($clientemp);


    $clientemp   =DB::table('tbl_clientspockmap as cs')
    ->join('tbl_clients as c','cs.fk_clientid','=','c.client_id')
    ->join('users as u','cs.fk_empid','=','u.id')
    ->select('cs.*','c.client_id','c.comp_name',DB::raw('group_concat(u.name) as name'))
    ->where('cs.assignee_emp_id','=',$assigneeid)
    ->groupBy('cs.fk_clientid')
    ->get();
         // dd($clientemp); 

          //  ->toSQL();
          // dd($clientemp);
    return view('Emp.clientspoc',compact('clients','clientemp'));
}



public function updateunassignspoc(Request $request)
{

   
   $assigneeid=Auth::user()->id;


   $this->validate($request,[
       'empid' => 'required',

       ],[
       'empid.required' => 'Select  Employees from the list.',
       ]);
   
   
   $clientid = $request->client_id;
   $fk_empid = $request->empid;
  // echo $clientid.'</br>';
  // print_r($fk_empid);
  //       die;
       // dd($clientid);
   $assigneeid=Auth::user()->id;
   for($k=0;$k <count($request['empid']);$k++)
   {
        //echo $request['empid'][$k]."<br>";
      $abc= explode('/',$request['empid'][$k]);
      $emprole= explode(',',$abc[1]);
    // dd($emprole);
      if(in_array("2", $emprole)==false)
      {

        array_push($emprole,2);
        $role=implode(',',$emprole);
            //dd($role);
        DB::table('users')
        ->where('id',$abc[0])
        ->update(['emp_role'=>$role]);

    }
    

    for($i=0;$i<sizeof($fk_empid);$i++){
        DB::table('tbl_clientspockmap')
        ->where('fk_clientid',$clientid)
        ->where('fk_empid',$fk_empid[$i])
            // ->groupBy('cs.fk_clientid')
        ->delete();
    }

    DB::table('tbl_clients')
    ->where('client_id',$clientid)
    ->update(['client_status'=>1]);
         // dd($clientemp); 

          // ->toSQL();
          // dd($clientemp);
}
Session::flash('success_msg','Recruiters Deleted Successfully!');
return redirect()->route('clientspoc');
}



public function clientexcel()
{
    return view('Emp.clientexcel');
}



public function updateClient($client_id,Request $request)
{
    $this->validate($request,[
        'comp_name' => 'required',
        'company_url' => 'required',
        'email' => 'required|email|unique:tbl_clients',
        'contact_name' => 'required',
        'designation' => 'required',
        'phone' => 'required',
        'company_size' => 'required',
        'timings' => 'required',
        'days_working' => 'required',
        'company_size' => 'required',
        'industry' => 'required',
        'headquarters' => 'required',
        'company_specialties' => 'required',
        'office_address' => 'required',
        'selling_point' => 'required',
        ],[
        'comp_name.required' => 'Company name field is required.',
        'company_url.required' => 'Company Url is required.',
        'email.required' => 'Email field is required.',
        'contact_name.required' => 'Contact number field is required.',
        'designation.required' => 'Designation field is required.',
        'phone.required' => 'Phone number field is required.',
        'phone.max'=>'Phone number field must be of 10 digit only.',
        'company_size.required' => 'Company Size is required.',
        'timings.required' => 'Timings is required.',
        'days_working.required' => 'Days of working field is required',
        'industry.required' => 'Industry field is required',
        'headquarters.required' => 'Headquarters field is required',
        'company_specialties.required' => 'Company Specialties field is required',
        'office_address.required' => 'Office address field is required',
        'selling_point.required' => 'Selling Point field is required',
        ]);


    $assigneeid = Auth::user()->id;
    
    $clientid = Crypt::decrypt($client_id);

    $client_obj = new Client();
    
    DB::table('tbl_clients')
    ->where('client_id',$clientid)
    ->update(['comp_name' => $request['comp_name'],
        'company_url' => $request['company_url'],
        'contact_name' => $request['contact_name'],
        'designation' => $request['designation'],
        'phone' => $request['phone'],
        'alternate_phone' => $request['alternate_phone'],
        'email' => $request['email'],
        'company_size' => $request['company_size'],
        'timings' => $request['timings'],
        'days_working' => $request['days_working'],
        'industry' => $request['industry'],
        'headquarters' => $request['headquarters'],
        'company_specialties' => $request['company_specialties'],
        'office_address' => $request['office_address'],
        'selling_point' => $request['selling_point']]);
    
    Session::flash('success_msg', 'Client Updated Successfully!');
    return redirect()->route('listclient');
}

public function editclient($client_id,Request $request)
{
    $clientid = Crypt::decrypt($client_id);
    $client_details = DB::table('tbl_clients')
    ->select('*')
    ->where('client_id',$clientid)
    ->get();
    
    return view('Emp.editclient',compact('client_details'));
}

public function getspoctoclient($id)
{

    $assign_spoc_id=DB::table('users')
    ->select('id','name','emp_role')
    ->where('emp_role','!=','4')
    ->where('emp_status','=','1')
    ->whereNotIn('id',function ($query) use ($id){
        $query->select('fk_empid')->from('tbl_clientspockmap')
        ->Where('fk_clientid','=',$id);

    })
            //  ->whereRaw(['FIND_IN_SET(?,emp_role)',[2]])
    ->get();


    if($assign_spoc_id->isEmpty())
    {

        $assign_spoc_id=DB::table('users as u')
        ->select('u.id','u.name','u.emp_role')
        ->where(['u.emp_status','=',1])
        ->get();

    }

    $str='<option value="">'.'Select'.'</option>';

    foreach ($assign_spoc_id as $key=>$value)
    {
        $str .= '<option value="'.$value->id.'/'.$value->emp_role.'">'.$value->name.'</option>';
    }

    echo $str;

}

public function getassignspoctoclient($id)
{

    $assign_spoc_id=DB::table('users')
    ->select('id','name','emp_role')
    ->where('emp_role','!=','4')
    ->whereIn('id',function ($query) use ($id){
        $query->select('fk_empid')->from('tbl_clientspockmap')
        ->Where('fk_clientid','=',$id);

    })
            //  ->whereRaw(['FIND_IN_SET(?,emp_role)',[2]])
    ->get();

//dd($assign_spoc_id);
    if($assign_spoc_id->isEmpty())
    {

        $assign_spoc_id=DB::table('users as u')
        ->select('u.id','u.name','u.emp_role')
        ->where(['u.emp_status','=',1])
        ->get();

    }

    $str='<option value="">'.'Select'.'</option>';

    foreach ($assign_spoc_id as $key=>$value)
    {
        $str .= '<option value="'.$value->id.'/'.$value->emp_role.'">'.$value->name.'</option>';
    }

    echo $str;

}

public function getemployeebyclientid($cid){
    $clientuser=  DB::table('tbl_clientspockmap as c')
    ->where(['c.fk_clientid'=>$cid])
    ->get();

    $user=  DB::table('users as c')
    ->whereRaw('FIND_IN_SET(?,emp_role)',[2])
    ->get();

    $list =array();
    foreach($clientuser as $value){
        $list[] = $value->fk_empid;
    }

    $getvalue =array();
    foreach($user as $value){
        $getvalue ='';
        if(in_array($value->id,$list,TRUE)){
            $getvalue = 0;
        }else{
            $getvalue[] = $value->id.','.$value->name;
        }
    }

    $varList = '';
    foreach($getvalue as $value){
        $getlist = explode(',', $value);

        $varList .=  '<option value='.$getlist[0].'>'.$getlist[1].'</option>';
    }
    echo $varList;
}

public function excellistclientdownload()
{

 $assigneeid=Auth::user()->id;


 
 $users= DB::table('tbl_clients as c')
 ->join('tbl_department as d','c.client_catid','=','d.dept_id')
 ->select('d.dept_name','c.*')
 ->where(['c.clientsuid'=>$assigneeid,'c.client_status'=>1])
 ->orderby('client_id','desc')
 ->get();
      // DB::table('userslogin as c')
      // ->join('attendanceuser as d','c.usersloginempid','=','d.empcode')
      // ->join('tbl_clientrec_start_endtime as e','e.clientstart_recuiterid','=','c.usersloginid')
      // ->select(DB::raw('*'))
      // ->where('c.createdon',$dobj)
      // ->where('d.created_at',$dobj)
      //      // ->where('e.created_at', 'like', '28%')
 
      // ->where('e.created_at', 'LIKE', '%' .$dobj. '%')
      //      //  ->where('e.created_at',date('Y-m-d').'00:00:00')
      // ->orderby('c.usersloginempid','desc')
      // ->get();
      //   $users = DB::table('userslogin')->where('createdon','=',date('Y-m-d'))->get();
        //-----------Va Define-----------------------//
 $allArr               = array();
 $templevel            = 0;  
 $newkey               = 0;
 $grouparr[$templevel] = "";
  //-----------------------------------------------//
 /* $role=['BD','SPOC','REC','HR'];*/

 $response['array_test']  = array(
     '1'=>'BD',
     '2'=>'SPOC',
     '3'=>'Recruiter',
     );
 foreach($response['array_test'] as $key => $value){
     $fuelID[] = $key;
     $fuelName[] = $value;
    //p($fuelName); exit;
 }
 $fuelProID = implode(',',$fuelID);
 $fuelProName = implode(',',$fuelName);

 $featurevaluee1[]    = 'test';
 
 $excelHeadArrqq1 = array_merge($featurevaluee1);
 $excelHeadArr1 = array_unique($excelHeadArrqq1);
 
 $oldfinalAttrData['test']       = $fuelProName;
 $oldfinalAttrData2['test']      = $fuelName;

 $finalAttrData  = array_merge($oldfinalAttrData);
 $finalAttrData2 = $oldfinalAttrData2;
 $attCount = array();
 foreach($finalAttrData as $k=>$v){
     $valueCount   = count(explode(",", $v));
     $attCount[$k] = $valueCount+1;

 } 
 $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
 $objWorkSheet  = $objPHPExcel->createSheet();
 $objPHPExcel->setActiveSheetIndex(0);

 $finalExcelArr1 = array_merge($excelHeadArr1);

 $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
 $j      = 2;
 
 for($i=0;$i<count($finalExcelArr1);$i++){
     $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
     foreach ($finalAttrData2 as $key => $value) {
        foreach ($value as $k => $v) {

          if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
        }
    }
}  
}
$arrPart2 = array('Category','Company Name','Contact Person Name','Designation','Phone','Email','POC1_Name','POC1_Designation','POC1_Phone','POC1_Email','POC2_Name','POC2_Designation','POC2_Phone','POC2_Email','Summary','Constractual_sla');
if(!empty($excelHeadArr)){
   $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
}else{
    $finalExcelArr = array_merge($arrPart2);
}
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');

$cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
$j=2;
 //Set border style for active worksheet
for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
    $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

    foreach ($users as $key => $value) {
        //Set height for all rows.
      $vpunchrec=explode(',', $value->punchrecord);
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      $newvar = $j+$key;
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      if($value->client_catid==1)
        { $valcat="IT";}
    else
        { $valcat="Non IT";}
    

    $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $valcat);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->comp_name);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->contact_name);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->designation);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->phone);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->email);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->contact_name1);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->designation1);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->phone1);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->email1);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->contact_name2);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->designation2);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $value->phone2);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->email2);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[14].$newvar, strip_tags($value->summary));
    $objPHPExcel->getActiveSheet()->setCellValue($cols[15].$newvar, $value->contractual_sla);

    
}
for($k=2;$k <1000;$k++)
{
   $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
   $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
   
   $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
   $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
   $objValidation24->setAllowBlank(false);
   $objValidation24->setShowInputMessage(true);
   $objValidation24->setShowErrorMessage(true);
   $objValidation24->setShowDropDown(true);
   $objValidation24->setErrorTitle('Input error');
   $objValidation24->setError('Value is not in list.');
   $objValidation24->setPromptTitle('Pick from list');
   $objValidation24->setPrompt('Please pick a value from the drop-down list.');
   $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
   $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
   
  }//secfor


}

$filename  = "EmployeeExcel.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
        die("a");
        exit;
        echo "fffffffffffffffff";
    }



    public function excellistclientspocdownload()
    {

     $assigneeid=Auth::user()->id;

     $clientemp   =DB::table('tbl_clientspockmap as cs')
     ->join('tbl_clients as c','cs.fk_clientid','=','c.client_id')
     ->join('users as u','cs.fk_empid','=','u.id')
     ->select('c.comp_name','u.name')
     ->where('cs.assignee_emp_id','=',$assigneeid)
     ->groupby('c.comp_name','u.name')

     ->get();
     
        //-----------Va Define-----------------------//
     $allArr               = array();
     $templevel            = 0;  
     $newkey               = 0;
     $grouparr[$templevel] = "";
  //-----------------------------------------------//
     /* $role=['BD','SPOC','REC','HR'];*/

     $response['array_test']  = array(
         '1'=>'BD',
         '2'=>'SPOC',
         '3'=>'Recruiter',
         );
     foreach($response['array_test'] as $key => $value){
         $fuelID[] = $key;
         $fuelName[] = $value;
    //p($fuelName); exit;
     }
     $fuelProID = implode(',',$fuelID);
     $fuelProName = implode(',',$fuelName);

     $featurevaluee1[]    = 'test';
     
     $excelHeadArrqq1 = array_merge($featurevaluee1);
     $excelHeadArr1 = array_unique($excelHeadArrqq1);
     
     $oldfinalAttrData['test']       = $fuelProName;
     $oldfinalAttrData2['test']      = $fuelName;

     $finalAttrData  = array_merge($oldfinalAttrData);
     $finalAttrData2 = $oldfinalAttrData2;
     $attCount = array();
     foreach($finalAttrData as $k=>$v){
         $valueCount   = count(explode(",", $v));
         $attCount[$k] = $valueCount+1;

     } 
     $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
     $objWorkSheet  = $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(0);

     $finalExcelArr1 = array_merge($excelHeadArr1);

     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
     $j      = 2;
     
     for($i=0;$i<count($finalExcelArr1);$i++){
         $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
         foreach ($finalAttrData2 as $key => $value) {
            foreach ($value as $k => $v) {

              if($key == $finalExcelArr1[$i]){
                $newvar = $j+$k;
                $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
            }
        }
    }  
}
$arrPart2 = array('Company Name','Assigned SPOC');
if(!empty($excelHeadArr)){
   $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
}else{
    $finalExcelArr = array_merge($arrPart2);
}
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');

$cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
$j=2;
 //Set border style for active worksheet
for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
    $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

    foreach ($users as $key => $value) {
        //Set height for all rows.
      $vpunchrec=explode(',', $value->punchrecord);
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      $newvar = $j+$key;
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      if($value->client_catid==1)
        { $valcat="IT";}
    else
        { $valcat="Non IT";}
    

    $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->comp_name);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->name);
    
}
for($k=2;$k <1000;$k++)
{
   $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
   $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
   
   $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
   $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
   $objValidation24->setAllowBlank(false);
   $objValidation24->setShowInputMessage(true);
   $objValidation24->setShowErrorMessage(true);
   $objValidation24->setShowDropDown(true);
   $objValidation24->setErrorTitle('Input error');
   $objValidation24->setError('Value is not in list.');
   $objValidation24->setPromptTitle('Pick from list');
   $objValidation24->setPrompt('Please pick a value from the drop-down list.');
   $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
   $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
   
  }//secfor


}

$filename  = "EmployeeExcel.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
        die("a");
        exit;
        echo "fffffffffffffffff";
    }



   public function ajaxlistclient(Request $request)
   {
       $assigneeid=Auth::user()->id;
       $key = $request['key'];

       $sqlQuery = "
       select  c.clientsuid, d.dept_name, c.* from tbl_clients as c inner join tbl_department as d on c.client_catid = d.dept_id 
        where c.clientsuid = '".$assigneeid."' 
        and c.client_status = '1' 
        and c.comp_name like '%".$key."%' 
        and (
         c.contact_name like '%".$key."%' 
        or c.designation like '%".$key."%' 
        or c.phone like '%".$key."%' 
        or c.email like '%".$key."%' 
        or c.contact_name1 like '%".$key."%' 
        or c.designation1 like '%".$key."%' 
        or c.phone1 like '%".$key."%' 
        or c.email1 like '%".$key."%' 
        or c.contact_name2 like '%".$key."%' 
        or c.designation2 like '%".$key."%' 
        or c.phone2 like '%".$key."%' 
        or c.email2 like '%".$key."%' 
        or c.summary like '%".$key."%' 
        or c.contractual_sla like '%".$key."%' )
        order by c.client_id desc, client_id DESC";
       $result = DB::select(DB::raw($sqlQuery));
       $i=1;
      // echo "<pre>";
     //  print_r($result);
      
  foreach($result as $kk => $c){

      if($c->client_catid==1)
      {
        $int = "IT";
      }else{
        $int = "NON IT";
      }

      echo '<tr>
            <td>'.$i.'</td>
            <td>'.$int.'</td>
            <td>'.$c->comp_name.'</td>
            <td>'.$c->contact_name.'</td>
            <td>'.$c->designation.'</td>
            <td>'.$c->phone.'</td>
            <td>'.$c->email.'</td>
            <td>'.$c->contact_name1.'</td>
            <td>'.$c->designation1.'</td>
            <td>'.$c->phone1.'</td>
            <td>'.$c->email1.'</td>
             <td>'.$c->contact_name2.'</td>
            <td>'.$c->designation2.'</td>
            <td>'.$c->phone2.'</td>
            <td>'.$c->email2.'</td>
            <td>'.strip_tags($c->summary).'</td>
            <td>'.$c->contractual_sla.'</td>
            <td><div class="btn-group btn-group-sm" style="float: none;">
            <button type="button" onclick="test('.$c->client_id.','.$c->comp_name.'),getspocclient('.$c->client_id.')" href="#success" data-toggle="modal" class="tabledit-edit-button sl_refresh btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span></button>
            </div>
            </td>
            <td>
            <a href="editclient/'.Crypt::encrypt($c->client_id).'">
            <div class="btn-group btn-group-sm" style="float:none;"> 
            <button type="button"  class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;">
            <span class="icofont icofont-ui-edit"></span>
            </button>
            </div>
            </a>
            </td>
            </tr>';

            $i++; }

   }


       public function ajaxclientlists(Request $request)
    {
       $assigneeid=Auth::user()->id;
       $key = $request['key'];

       $sqlQuery = "
       select c.* from tbl_clients as c 
        where c.client_status != 3 and c.clientsuid = '".$assigneeid."'  
        and (
        c.comp_name like '%".$key."%' 
        or c.contact_name like '%".$key."%' 
        or c.designation like '%".$key."%' 
        or c.phone like '%".$key."%' 
        or c.contact_name1 like '%".$key."%' 
        or c.designation1 like '%".$key."%' 
        or c.phone1 like '%".$key."%' 
        or c.email1 like '%".$key."%' 
        or c.contact_name2 like '%".$key."%' 
        or c.designation2 like '%".$key."%' 
        or c.phone2 like '%".$key."%' 
        or c.email2 like '%".$key."%' 
        or c.summary like '%".$key."%' 
        or c.contractual_sla like '%".$key."%' )
        order by c.client_id desc, client_id DESC";
       $result = DB::select(DB::raw($sqlQuery));
       $i=1;

  foreach($result as $kk => $c){

      if($c->client_catid==1)
      {
        $int = "IT";
      }else{
        $int = "NON IT";
      }

      echo '<tr>
             <td>'.$int.'</td>
            <td>'.$c->comp_name.'</td>
            <td>'.$c->contact_name.'</td>
            <td>'.$c->designation.'</td>
            <td>'.$c->phone.'</td>
            <td>'.$c->email.'</td>
            <td>'.$c->contact_name1.'</td>
            <td>'.$c->designation1.'</td>
            <td>'.$c->phone1.'</td>
            <td>'.$c->email1.'</td>
             <td>'.$c->contact_name2.'</td>
            <td>'.$c->designation2.'</td>
            <td>'.$c->phone2.'</td>
            <td>'.$c->email2.'</td>
            <td>'.strip_tags($c->summary).'</td>
            <td>'.$c->contractual_sla.'</td>
              <td><a href="editclient/'.Crypt::encrypt($c->client_id).'">
                                        <div class="btn-group btn-group-sm" style="float:none;"> 
                                        <button type="button"  class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;">
                                        <span class="icofont icofont-ui-edit"></span>
                                        </button>
                                        </div>
                                        </a></td>
          </tr>';
          $i++;}

   }



   public function ajaxclientspoc(Request $request)
{


      $assigneeid=Auth::user()->id;
      $key = $request['key'];

$sqlQuery = "select cs.*, c.client_id, c.comp_name, group_concat(u.name) as name from tbl_clientspockmap as cs inner join tbl_clients as c on cs.fk_clientid = c.client_id inner join users as u on cs.fk_empid = u.id where cs.assignee_emp_id = '".$assigneeid."' and 
     (
      c.comp_name like '%".$key."%' 
      or u.name like '%".$key."%' 
    ) group by cs.fk_clientid";
   $result = DB::select(DB::raw($sqlQuery));
   $i=1;
 //  echo "<pre>";
 // print_r($result);


foreach($result as $kk => $clients)
{

  $cname = "'".$clients->comp_name."'";
echo '<tr >
    <td>'.$i.'</td>
    <td>'.$clients->comp_name.'</td>
    <td>'.$clients->name.'</td>
    <td>
        <div class="btn-group btn-group-sm" style="float: none;">
<button onclick="return test('.$clients->fk_clientid.','.$cname.'),getspocclient('.$clients->fk_clientid.');" type="button" href="#success" data-toggle="modal" class="tabledit-edit-button sl_refresh btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;"><span  class=""></span>Add</button></div>


<div class="btn-group btn-group-sm">
<button onclick="return testrmv('.$clients->fk_clientid.','.$cname.'),getassignspocclient('.$clients->fk_clientid.');" type="button" href="#unassign" data-toggle="modal" class="tabledit-edit-button sl_refresh btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;"><span  class=""></span>Remove</button>
</div>
</td>
</tr>';
$i++;
}
}








}
