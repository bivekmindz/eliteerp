<?php
namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Mail;
use Storage;
use Carbon\Carbon;
use Session;


class SearchController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function advancesearch()
    {
        $all_keywords=$_GET['all_keywords'];
        $exc_keywords=$_GET['exc_keywords'];
        $company=@$_GET['company'];
        $exc_company=@$_GET['exc_company'];
        $designation=@$_GET['designation'];
        $loc=explode('_', @$_GET['current_loc']);
        $loc_id=@$loc[0];
        $current_loc=@$loc[1];
        $exp_min=$_GET['exp_min'];
        $exp_max=$_GET['exp_max'];
        $page=@$_GET['page'];

        if($_GET['sal_min']!='')
        {
            $sal_min=$_GET['sal_min'].'.'.$_GET['sal_min1'];
        }
        else
        {
            $sal_min=0;
        }
        
        if($_GET['sal_max']!='')
        {
            $sal_max=$_GET['sal_max'].'.'.$_GET['sal_max1'];
            $sal_maxm=$_GET['sal_max'].'.'.$_GET['sal_max1'];
        }
        else
        {
            $sal_max=1000.0;
            $sal_maxm=0;
        }
        
        if($_GET['exp_min']!='')
        {
            $exp_min=$_GET['exp_min'];
        }
        else
        {
            $exp_min=0;
        }
        
        if($_GET['exp_max']!='')
        {
            $exp_max=$_GET['exp_max'];
            $exp_maxm=$_GET['exp_max'];
        }
        else
        {
            $exp_max=100.0;
            $exp_maxm=0;
        }
        
        if(empty($page))
        {
            $pages=1;
        }
        else
        {
            $pages=$page;
        }

        $filter=@$_GET['filter']; #Both,Database,Monster
         
        /*----------------------Monster API----------------------------*/
        if($filter=='Monster') #if Monster
        {
            $loginid = Auth::user()->id;

            $getmonsterprovidekey = DB::table('tbl_monsteractivities as ma')
                        ->select('ma.*')
                        ->where('ma.ma_status', '=','Active')
                        ->where('ma.ma_userid', '=',$loginid)
                        ->orderBy('ma_id','desc')
                        #->limit(1)
                        ->first();

            if(empty($getmonsterprovidekey))
            {
                $query="select mk_id,mk_catid,mk_vendor_key from tbl_monsterkeys where mk_id not in (select ma_mapid from tbl_monsteractivities where ma_status ='Active') and mk_status='Active' limit 1"; 
                $result = DB::select(DB::raw($query));
                
                if(empty($result))
                {
                    $monsterresponse=0;
                    $catid=0;
                    $vendorkey=0;
                }
                else
                {
                    $monsterresponse=1;
                    $catid=$result[0]->mk_catid;
                    $vendorkey=$result[0]->mk_vendor_key;

                    $lastid=    DB::table('tbl_monsteractivities')
                                ->insertGetId(['ma_userid' => $loginid,'ma_mapid' => $result[0]->mk_id,
                                'ma_catid' => $catid,'ma_vendorkey' => $vendorkey]);
                
                    DB::table('tbl_monsteractivitiesdays')->insert([
                    'md_userid'=>$loginid,
                    'md_ma_id'=>$lastid,
                    'md_mapid'=>$result[0]->mk_id,
                    'md_catid'=>$catid,
                    'md_vendorkey'=>$vendorkey
                    ]);
                }
            }
            else
            {    
                $monsterresponse=1;
                $catid=$getmonsterprovidekey->ma_catid;
                $vendorkey=$getmonsterprovidekey->ma_vendorkey;

                DB::table('tbl_monsteractivitiesdays')
                ->where('md_userid', '=', $loginid)
                ->update([
                    'md_status'=>'Inactive'
                ]);

                DB::table('tbl_monsteractivitiesdays')->insert([
                'md_userid'=>$loginid,
                'md_ma_id'=>$getmonsterprovidekey->ma_id,
                'md_mapid'=>$getmonsterprovidekey->ma_mapid,
                'md_catid'=>$catid,
                'md_vendorkey'=>$vendorkey
                ]);
            }

            if(empty($monsterresponse))
            {
                $getmonster='';
                $getmonsterkeys='NotFoundData';
            }
            else
            {
                $getmonsterkeys=array('catid'=>$catid,'vendorkey'=>$vendorkey);
                $catid=$catid;
                $vendor_key=$vendorkey;
                $keyword=$all_keywords;
                $location='&rlid='.$loc_id.'&twlid=0'; # Location &rlid=4&twlid=2 = rlid (Residence Location or Current Location),twlid (Preferred Job Location)
                $experience='&mne='.$exp_min.'&mxe='.$exp_maxm; # Experience &mne=5&mxe=10
                #$indcatrole='&indus=0&tcc=0&rol=0'; # Industry, Category, Role &indus=11,48&tcc=17&rol=591,596
                $salary='&tsalmin='.$sal_min.'&tsalmax='.$sal_maxm; # Salary &tsalmin=5.6&tsalmax=8.7
                $mexc_keywords='&qnot='.$exc_keywords; #qnot (Keyword Operator for Excluding keywords) A logical operator (either AND or OR ) can also inserted between keywords.
                $mcompany='&qrcn='.str_replace(' ','+',$company); #qrcn (Company name)
                # passyr (Year of Passing)  &passyr =2004-2008
                # page (Page number)    &page=1
                # pagesize (Size of result pages) &pagesize=1

                $variable=$keyword.$location.$experience.$salary.$mexc_keywords.$mcompany;

                $monurl="https://recruiter.monsterindia.com/v2/resumedatabase/searchresult.html?ver=1.2&cat=".$catid."&vendor_key=".$vendor_key."&q=".$variable."&page=".$pages."&pagesize=10";
               
                $monres=file_get_contents($monurl);
                $arr=simplexml_load_string($monres);
                $json  = json_encode($arr);
                $configData = json_decode($json, true);
                $getmonster=$configData['Monster']['Resumes']['Resume'];
                $getmonstertotalcount=60;

                 return view('Emp.advancesearchmon', [
                'getmonster'=>$getmonster,
                'getmonstertotalcount'=>$getmonstertotalcount,
                'getmonsterkeys'=>$getmonsterkeys,
                ]);
          
            }
        } #if Monster
        
        /*----------------------Monster API End----------------------------*/

        /*----------------------Solr Database API--------------------------*/
        if(empty($filter) || $filter=='Database')  #if Database
        {

            $url='http://115.124.98.243/~logistiks/solrApi/advancesearch.php';
            $data = str_replace(' ', '+', $all_keywords);

            $exckeywords = str_replace(' ', '+', $exc_keywords);
            $company = str_replace(' ', '+', $company);
            $excp = str_replace(' ', '+', $exc_company);
            $designation = str_replace(' ', '+', $designation);
            $current_loc = str_replace(' ', '+', $current_loc);
            $exp_min = str_replace(' ', '+', $exp_min);
             $exp_max = str_replace(' ', '+', $exp_max);
            $sal_min = str_replace(' ', '+', $sal_min);
            $sal_max = str_replace(' ', '+', $sal_max);
            //print_r($exckeywords); exit;

            $post = [
            'search' =>$data, 
            'merchantname' => 'techproducts',
            'all' => $data,
            'minsalary' =>$sal_min,
            'maxsalary' => $sal_max,
            'location' => $current_loc,
            'designation' => $designation,
            'company' => $company,
            'minexperience' => $exp_min,
            'maxexperience' => $exp_max,
            'salary' => '',
            'experience' => '',
            'exdata'=>$exckeywords,
            'excp'=>$excp,
            ];
            //print_r($post); exit;

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
            $server_output = curl_exec ($ch);
         
            $response = curl_exec($ch);
            curl_close ($ch);

            $apidata=json_decode($response);
            $startpagination=$apidata->result->start;
            $endpagination=$apidata->result->numFound;
     
            if($apidata->response[0]->error_status=='Success')
            { 
                $id_array = array();
                foreach ($apidata->result->docs as $key => $value) 
                {
                    array_push($id_array, $value->id);
                }
                $ids= implode(',', $id_array);

                $getpostion = DB::table('tbl_recruiter_cv as st')
                            ->whereRaw("FIND_IN_SET(st.id, '$ids')")
                            ->select('st.*')
                            ->get();
            }
            else
            {
                $getpostion='';
            }

            $getmonster='';
            $getmonsterkeys='NotFoundData';

            return view('Emp.advancesearch', [
            'getpostion'=>$getpostion,
            'getmonster'=>$getmonster,
            'getmonsterkeys'=>$getmonsterkeys,
            'startpagination'=>$startpagination,
            'endpagination'=>$endpagination
            ]);
        } #if Database
        
  
       /*----------------------Solr Database API End------------------------*/


        /*----------------------- Database And Monster ---------------------*/
        if($filter=='Both') # if Both
        { 

        /*----------------------Monster API----------------------------*/
            $loginid = Auth::user()->id;

            $getmonsterprovidekey = DB::table('tbl_monsteractivities as ma')
                    ->select('ma.*')
                    ->where('ma.ma_status', '=','Active')
                    ->where('ma.ma_userid', '=',$loginid)
                    ->orderBy('ma_id','desc')
                    #->limit(1)
                    ->first();

            if(empty($getmonsterprovidekey))
            {
                $query="select mk_id,mk_catid,mk_vendor_key from tbl_monsterkeys where mk_id not in (select ma_mapid from tbl_monsteractivities where ma_status ='Active') and mk_status='Active' limit 1"; 
                $result = DB::select(DB::raw($query));
            
                if(empty($result))
                {
                    $monsterresponse=0;
                    $catid=0;
                    $vendorkey=0;
                }
                else
                {
                    $monsterresponse=1;
                    $catid=$result[0]->mk_catid;
                    $vendorkey=$result[0]->mk_vendor_key;

                    $lastid=    DB::table('tbl_monsteractivities')
                                ->insertGetId(['ma_userid' => $loginid,'ma_mapid' => $result[0]->mk_id,
                                'ma_catid' => $catid,'ma_vendorkey' => $vendorkey]);
                
                    DB::table('tbl_monsteractivitiesdays')->insert([
                    'md_userid'=>$loginid,
                    'md_ma_id'=>$lastid,
                    'md_mapid'=>$result[0]->mk_id,
                    'md_catid'=>$catid,
                    'md_vendorkey'=>$vendorkey
                    ]);
                }
            }
            else
            {    
                $monsterresponse=1;
                $catid=$getmonsterprovidekey->ma_catid;
                $vendorkey=$getmonsterprovidekey->ma_vendorkey;

                DB::table('tbl_monsteractivitiesdays')
                ->where('md_userid', '=', $loginid)
                ->update([
                    'md_status'=>'Inactive'
                ]);

                DB::table('tbl_monsteractivitiesdays')->insert([
                'md_userid'=>$loginid,
                'md_ma_id'=>$getmonsterprovidekey->ma_id,
                'md_mapid'=>$getmonsterprovidekey->ma_mapid,
                'md_catid'=>$catid,
                'md_vendorkey'=>$vendorkey
                ]);
            }

            if(empty($monsterresponse))
            {
                $getmonster='';
                $getmonsterkeys='NotFoundData';
            }
            else
            {
                $getmonsterkeys=array('catid'=>$catid,'vendorkey'=>$vendorkey);
                $catid=$catid;
                $vendor_key=$vendorkey;
                $keyword=$all_keywords;
                $location='&rlid='.$loc_id.'&twlid=0'; # Location &rlid=4&twlid=2 = rlid (Residence Location or Current Location),twlid (Preferred Job Location)
                $experience='&mne='.$exp_min.'&mxe='.$exp_maxm; # Experience &mne=5&mxe=10
                #$indcatrole='&indus=0&tcc=0&rol=0'; # Industry, Category, Role &indus=11,48&tcc=17&rol=591,596
                $salary='&tsalmin='.$sal_min.'&tsalmax='.$sal_maxm; # Salary &tsalmin=5.6&tsalmax=8.7
                $mexc_keywords='&qnot='.$exc_keywords; #qnot (Keyword Operator for Excluding keywords) A logical operator (either AND or OR ) can also inserted between keywords.
                $mcompany='&qrcn='.str_replace(' ','+',$company); #qrcn (Company name)
                # passyr (Year of Passing)  &passyr =2004-2008
                # page (Page number)    &page=1
                # pagesize (Size of result pages) &pagesize=1

                $variable=$keyword.$location.$experience.$salary.$mexc_keywords.$mcompany;

                $monurl="https://recruiter.monsterindia.com/v2/resumedatabase/searchresult.html?ver=1.2&cat=".$catid."&vendor_key=".$vendor_key."&q=".$variable."&page=".$pages."&pagesize=10";
               
                $monres=file_get_contents($monurl);
                $arr=simplexml_load_string($monres);
                $json  = json_encode($arr);
                $configData = json_decode($json, true);
                $getmonster=$configData['Monster']['Resumes']['Resume'];
            }
        
        /*----------------------Monster API End----------------------------*/

        /*----------------------Solr Database API--------------------------*/

            $url='http://115.124.98.243/~logistiks/solrApi/advancesearch.php';
            $data = str_replace(' ', '+', $all_keywords);

            $exckeywords = str_replace(' ', '+', $exc_keywords);
            $company = str_replace(' ', '+', $company);
            $excp = str_replace(' ', '+', $exc_company);
            $designation = str_replace(' ', '+', $designation);
            $current_loc = str_replace(' ', '+', $current_loc);
            $exp_min = str_replace(' ', '+', $exp_min);
             $exp_max = str_replace(' ', '+', $exp_max);
            $sal_min = str_replace(' ', '+', $sal_min);
            $sal_max = str_replace(' ', '+', $sal_max);
            //print_r($exckeywords); exit;

            $post = [
            'search' =>$data, 
            'merchantname' => 'techproducts',
            'all' => $data,
            'minsalary' =>$sal_min,
            'maxsalary' => $sal_max,
            'location' => $current_loc,
            'designation' => $designation,
            'company' => $company,
            'minexperience' => $exp_min,
            'maxexperience' => $exp_max,
            'salary' => '',
            'experience' => '',
            'exdata'=>$exckeywords,
            'excp'=>$excp,
            ];
            //print_r($post); exit;

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
            $server_output = curl_exec ($ch);
         
            $response = curl_exec($ch);
            curl_close ($ch);

            $apidata=json_decode($response);
            $startpagination=$apidata->result->start;
            $endpagination=$apidata->result->numFound;
     
            if($apidata->response[0]->error_status=='Success')
            { 
                $id_array = array();
                foreach ($apidata->result->docs as $key => $value) 
                {
                    array_push($id_array, $value->id);
                }
                $ids= implode(',', $id_array);

                $getpostion = DB::table('tbl_recruiter_cv as st')
                            ->whereRaw("FIND_IN_SET(st.id, '$ids')")
                            ->select('st.*')
                            ->get();
            }
            else
            {
                $getpostion='';
            }
  
       /*----------------------Solr Database API End------------------------*/

            return view('Emp.advancesearch', [
                'getpostion'=>$getpostion,
                'getmonster'=>$getmonster,
                'getmonsterkeys'=>$getmonsterkeys,
                'startpagination'=>$startpagination,
                'endpagination'=>$endpagination
            ]);

        } # if Both

        /*---------------------- Database And Monster End-------------------*/
    
   }
    
 
    public function advancemail()
    {
        foreach ($_POST['mailid'] as $key => $value) 
        {
            #echo $value;
            $getpostion = DB::table('tbl_recruiter_cv as st')
            ->whereRaw("st.id='$value'")
            ->select('st.*')
            ->get();
            #print_r($getpostion[0]);  
            $emailid=trim($getpostion[0]->candidate_email); 
            $ename=trim($getpostion[0]->candidate_name); 
            #$content='Hi '.$getpostion[0]->candidate_name.', This is the content of mail body ';
            $data=(array)$getpostion[0];
            #print_r($data);
            $subject='very very urgent requirement of '.$getpostion[0]->primary_skill.' professionals with '.$getpostion[0]->current_org;
            #Mail::raw($content, function ($message) use ($emailid,$ename) {
            Mail::send('Emp.mails.advancesearchmailer',  $data, function ($message) use ($emailid, $ename,$subject) {
            $message->from('elitehr@gmail.com', 'EliteHR Team');
            $message->to($emailid,$ename);
            $message->subject($subject);
            });
        }
        echo 1;
    }

    
    public function downloadcv(Request $request)
    {
        $key = $request['key'];

        return view('Emp.monstercv', [
                'cvformat'=>$key
            ]);
    }
    

} #class