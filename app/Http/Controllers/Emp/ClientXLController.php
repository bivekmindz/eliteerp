<?php

namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use PHPExcel_IOFactory;
//use PHPExcel_Settings;
use App\Exceptions\ApplicationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use App\Model\User;
use App\Model\Role;
use App\Model\Client;
use App\Model\Adminmodel\Department as Dept;
use Illuminate\Support\Facades\Crypt;
use Session;
use DB;
use File;
//require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Cell\DataValidation;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column;
use PhpOffice\PhpSpreadsheet\Worksheet\AutoFilter\Column\Rule;

use PhpOffice\PhpSpreadsheet\NamedRange;
use Carbon\Carbon;


use PHPExcel_Cell;
use PHPExcel_Cell_DataType;
use PHPExcel_Cell_IValueBinder;
use PHPExcel_Cell_DefaultValueBinder;

use PHPExcel; 
use PHPExcel_IOFactory;

class ClientXLController extends Controller
{
    public function index()
    {

        $dept = Dept::all();
        return view('Emp.empXl',compact('dept'));
    }

    //client excel download   excellistclientdownload

public function excellistclientdownload()
    {

       $assigneeid=Auth::user()->id;


   
       $users= DB::table('tbl_clients as c')
            ->join('tbl_department as d','c.client_catid','=','d.dept_id')
            ->select('d.dept_name','c.*')
            ->where(['c.clientsuid'=>$assigneeid,'c.client_status'=>1])
            ->orderby('client_id','desc')
            ->get();
      // DB::table('userslogin as c')
      // ->join('attendanceuser as d','c.usersloginempid','=','d.empcode')
      // ->join('tbl_clientrec_start_endtime as e','e.clientstart_recuiterid','=','c.usersloginid')
      // ->select(DB::raw('*'))
      // ->where('c.createdon',$dobj)
      // ->where('d.created_at',$dobj)
      //      // ->where('e.created_at', 'like', '28%')
      
      // ->where('e.created_at', 'LIKE', '%' .$dobj. '%')
      //      //  ->where('e.created_at',date('Y-m-d').'00:00:00')
      // ->orderby('c.usersloginempid','desc')
      // ->get();
      //   $users = DB::table('userslogin')->where('createdon','=',date('Y-m-d'))->get();
        //-----------Va Define-----------------------//
      $allArr               = array();
      $templevel            = 0;  
      $newkey               = 0;
      $grouparr[$templevel] = "";
  //-----------------------------------------------//
      /* $role=['BD','SPOC','REC','HR'];*/

      $response['array_test']  = array(
       '1'=>'BD',
       '2'=>'SPOC',
       '3'=>'Recruiter',
       );
      foreach($response['array_test'] as $key => $value){
       $fuelID[] = $key;
       $fuelName[] = $value;
    //p($fuelName); exit;
     }
     $fuelProID = implode(',',$fuelID);
     $fuelProName = implode(',',$fuelName);

     $featurevaluee1[]    = 'test';
     
     $excelHeadArrqq1 = array_merge($featurevaluee1);
     $excelHeadArr1 = array_unique($excelHeadArrqq1);
     
     $oldfinalAttrData['test']       = $fuelProName;
     $oldfinalAttrData2['test']      = $fuelName;

     $finalAttrData  = array_merge($oldfinalAttrData);
     $finalAttrData2 = $oldfinalAttrData2;
     $attCount = array();
     foreach($finalAttrData as $k=>$v){
       $valueCount   = count(explode(",", $v));
       $attCount[$k] = $valueCount+1;

     } 
     $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
     $objWorkSheet  = $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(0);

     $finalExcelArr1 = array_merge($excelHeadArr1);

     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
     $j      = 2;
     
     for($i=0;$i<count($finalExcelArr1);$i++){
       $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
       foreach ($finalAttrData2 as $key => $value) {
        foreach ($value as $k => $v) {

          if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
          }
        }
      }  
    }
    $arrPart2 = array('Category','Company Name','Contact Person Name','Designation','Phone','Email','POC1_Name','POC1_Designation','POC1_Phone','POC1_Email','POC2_Name','POC2_Designation','POC2_Phone','POC2_Email','Summary','Constractual_sla');
    if(!empty($excelHeadArr)){
     $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
   }else{
    $finalExcelArr = array_merge($arrPart2);
  }
  $objPHPExcel->setActiveSheetIndex(1);
  $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');
  
  $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
  $j=2;
 //Set border style for active worksheet
  for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
    $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

    foreach ($users as $key => $value) {
        //Set height for all rows.
     // $vpunchrec=explode(',', $value->punchrecord);
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      $newvar = $j+$key;
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      if($value->client_catid==1)
{ $valcat="IT";}
    else
    { $valcat="Non IT";}
    

      $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $valcat);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->comp_name);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->contact_name);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->designation);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->phone);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->email);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->contact_name1);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->designation1);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->phone1);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->email1);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->contact_name2);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->designation2);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $value->phone2);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->email2);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[14].$newvar, strip_tags($value->summary));
      $objPHPExcel->getActiveSheet()->setCellValue($cols[15].$newvar, $value->contractual_sla);

      
    }
    for($k=2;$k <1000;$k++)
    {
     $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
     $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
     
     $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
     $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
     $objValidation24->setAllowBlank(false);
     $objValidation24->setShowInputMessage(true);
     $objValidation24->setShowErrorMessage(true);
     $objValidation24->setShowDropDown(true);
     $objValidation24->setErrorTitle('Input error');
     $objValidation24->setError('Value is not in list.');
     $objValidation24->setPromptTitle('Pick from list');
     $objValidation24->setPrompt('Please pick a value from the drop-down list.');
     $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
     $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
     
  }//secfor


}

$filename  = "Assignspock.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
        die("a");
        exit;
        echo "fffffffffffffffff";
      }

public function clientlistexceldown()
    {

       $assigneeid=Auth::user()->id;


   
       $users= DB::table('tbl_clients as c')
         ->select('c.*')
            ->where('c.client_status','!=',3)
            ->where('c.clientsuid','=',$assigneeid)
            ->orderby('c.client_id','desc')
           
            ->get();
      // DB::table('userslogin as c')
      // ->join('attendanceuser as d','c.usersloginempid','=','d.empcode')
      // ->join('tbl_clientrec_start_endtime as e','e.clientstart_recuiterid','=','c.usersloginid')
      // ->select(DB::raw('*'))
      // ->where('c.createdon',$dobj)
      // ->where('d.created_at',$dobj)
      //      // ->where('e.created_at', 'like', '28%')
      
      // ->where('e.created_at', 'LIKE', '%' .$dobj. '%')
      //      //  ->where('e.created_at',date('Y-m-d').'00:00:00')
      // ->orderby('c.usersloginempid','desc')
      // ->get();
      //   $users = DB::table('userslogin')->where('createdon','=',date('Y-m-d'))->get();
        //-----------Va Define-----------------------//
      $allArr               = array();
      $templevel            = 0;  
      $newkey               = 0;
      $grouparr[$templevel] = "";
  //-----------------------------------------------//
      /* $role=['BD','SPOC','REC','HR'];*/

      $response['array_test']  = array(
       '1'=>'BD',
       '2'=>'SPOC',
       '3'=>'Recruiter',
       );
      foreach($response['array_test'] as $key => $value){
       $fuelID[] = $key;
       $fuelName[] = $value;
    //p($fuelName); exit;
     }
     $fuelProID = implode(',',$fuelID);
     $fuelProName = implode(',',$fuelName);

     $featurevaluee1[]    = 'test';
     
     $excelHeadArrqq1 = array_merge($featurevaluee1);
     $excelHeadArr1 = array_unique($excelHeadArrqq1);
     
     $oldfinalAttrData['test']       = $fuelProName;
     $oldfinalAttrData2['test']      = $fuelName;

     $finalAttrData  = array_merge($oldfinalAttrData);
     $finalAttrData2 = $oldfinalAttrData2;
     $attCount = array();
     foreach($finalAttrData as $k=>$v){
       $valueCount   = count(explode(",", $v));
       $attCount[$k] = $valueCount+1;

     } 
     $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
     $objWorkSheet  = $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(0);

     $finalExcelArr1 = array_merge($excelHeadArr1);

     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
     $j      = 2;
     
     for($i=0;$i<count($finalExcelArr1);$i++){
       $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
       foreach ($finalAttrData2 as $key => $value) {
        foreach ($value as $k => $v) {

          if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
          }
        }
      }  
    }
    $arrPart2 = array('Category','Company Name','Contact Person Name','Designation','Phone','Email','POC1_Name','POC1_Designation','POC1_Phone','POC1_Email','POC2_Name','POC2_Designation','POC2_Phone','POC2_Email','Summary','Constractual_sla');
    if(!empty($excelHeadArr)){
     $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
   }else{
    $finalExcelArr = array_merge($arrPart2);
  }
  $objPHPExcel->setActiveSheetIndex(1);
  $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');
  
  $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
  $j=2;
 //Set border style for active worksheet
  for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
    $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

    foreach ($users as $key => $value) {
        //Set height for all rows.
     // $vpunchrec=explode(',', $value->punchrecord);
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      $newvar = $j+$key;
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      if($value->client_catid==1)
{ $valcat="IT";}
    else
    { $valcat="Non IT";}
    

      $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $valcat);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->comp_name);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->contact_name);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->designation);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->phone);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->email);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->contact_name1);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->designation1);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->phone1);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->email1);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->contact_name2);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->designation2);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[12].$newvar, $value->phone2);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[13].$newvar, $value->email2);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[14].$newvar, $value->summary);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[15].$newvar, $value->contractual_sla);

      
    }
    for($k=2;$k <1000;$k++)
    {
     $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
     $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
     
     $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
     $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
     $objValidation24->setAllowBlank(false);
     $objValidation24->setShowInputMessage(true);
     $objValidation24->setShowErrorMessage(true);
     $objValidation24->setShowDropDown(true);
     $objValidation24->setErrorTitle('Input error');
     $objValidation24->setError('Value is not in list.');
     $objValidation24->setPromptTitle('Pick from list');
     $objValidation24->setPrompt('Please pick a value from the drop-down list.');
     $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
     $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
     
  }//secfor


}

$filename  = "ClientList.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
        die("a");
        exit;
        echo "fffffffffffffffff";
      }

public function excellistclientspocdownload()
    {

       $assigneeid=Auth::user()->id;

        $users   =DB::table('tbl_clientspockmap as cs')
            ->join('tbl_clients as c','cs.fk_clientid','=','c.client_id')
            ->join('users as u','cs.fk_empid','=','u.id')
            ->select('c.comp_name','u.name')
            ->where('cs.assignee_emp_id','=',$assigneeid)
            ->groupby('c.comp_name','u.name')

            ->get();
      
        //-----------Va Define-----------------------//
      $allArr               = array();
      $templevel            = 0;  
      $newkey               = 0;
      $grouparr[$templevel] = "";
  //-----------------------------------------------//
      /* $role=['BD','SPOC','REC','HR'];*/

      $response['array_test']  = array(
       '1'=>'BD',
       '2'=>'SPOC',
       '3'=>'Recruiter',
       );
      foreach($response['array_test'] as $key => $value){
       $fuelID[] = $key;
       $fuelName[] = $value;
    //p($fuelName); exit;
     }
     $fuelProID = implode(',',$fuelID);
     $fuelProName = implode(',',$fuelName);

     $featurevaluee1[]    = 'test';
     
     $excelHeadArrqq1 = array_merge($featurevaluee1);
     $excelHeadArr1 = array_unique($excelHeadArrqq1);
     
     $oldfinalAttrData['test']       = $fuelProName;
     $oldfinalAttrData2['test']      = $fuelName;

     $finalAttrData  = array_merge($oldfinalAttrData);
     $finalAttrData2 = $oldfinalAttrData2;
     $attCount = array();
     foreach($finalAttrData as $k=>$v){
       $valueCount   = count(explode(",", $v));
       $attCount[$k] = $valueCount+1;

     } 
     $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
     $objWorkSheet  = $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(0);

     $finalExcelArr1 = array_merge($excelHeadArr1);

     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
     $j      = 2;
     
     for($i=0;$i<count($finalExcelArr1);$i++){
       $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
       foreach ($finalAttrData2 as $key => $value) {
        foreach ($value as $k => $v) {

          if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
          }
        }
      }  
    }
    $arrPart2 = array('Company Name','Assigned SPOC');
    if(!empty($excelHeadArr)){
     $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
   }else{
    $finalExcelArr = array_merge($arrPart2);
  }
  $objPHPExcel->setActiveSheetIndex(1);
  $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');
  
  $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
  $j=2;
 //Set border style for active worksheet
  for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
    $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

    foreach ($users as $key => $value) {
        //Set height for all rows.
    //  $vpunchrec=explode(',', $value->punchrecord);
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      $newvar = $j+$key;
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
   
      $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->comp_name);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->name);
      
    }
    for($k=2;$k <1000;$k++)
    {
     $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
     $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
     
     $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
     $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
     $objValidation24->setAllowBlank(false);
     $objValidation24->setShowInputMessage(true);
     $objValidation24->setShowErrorMessage(true);
     $objValidation24->setShowDropDown(true);
     $objValidation24->setErrorTitle('Input error');
     $objValidation24->setError('Value is not in list.');
     $objValidation24->setPromptTitle('Pick from list');
     $objValidation24->setPrompt('Please pick a value from the drop-down list.');
     $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
     $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
     
  }//secfor


}

$filename  = "Client-Assigned-to-SPOC.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
        die("a");
        exit;
        echo "fffffffffffffffff";
      }


    //
    public function exceldown()
    {


        
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('Maarten Balliauw')
            ->setLastModifiedBy('Maarten Balliauw')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Company Name')
            ->setCellValue('B1', 'Contact Person')
            ->setCellValue('C1', 'Contact Number')
            ->setCellValue('D1', 'Email')
            ->setCellValue('E1', 'Designation')
            ->setCellValue('F1', 'Company URL')
            ->setCellValue('G1', 'Company Size')
            ->setCellValue('H1', 'Timings From')
            ->setCellValue('I1', 'Timings To')
            ->setCellValue('J1', 'Days Working')
            ->setCellValue('K1', 'Industry')
            ->setCellValue('L1', 'Company’s Specialties')
            ->setCellValue('M1', 'Headquarters')
            ->setCellValue('N1', 'Office Address')
              ->setCellValue('O1', 'Alternate Phone')
                ->setCellValue('P1', 'Selling Points')
                 ->setCellValue('Q1', 'Summary About The Client')
                  ->setCellValue('R1', 'Contractual SLA')
                   ->setCellValue('S1', 'POC1 Contact Person Name')
                    ->setCellValue('T1', 'POC1 Designation')
                     ->setCellValue('U1', 'POC1 Mobile')
                      ->setCellValue('V1', 'POC1 Email')
                   ->setCellValue('W1', 'POC2 Contact Person Name')
                    ->setCellValue('X1', 'POC2 Designation')
                     ->setCellValue('Y1', 'POC2 Mobile')
                      ->setCellValue('Z1', 'POC2 Email');
            
             for($k=2;$k <1000;$k++) {
            $spreadsheet->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
     /*       $validation = $spreadsheet->getActiveSheet()->getCell('J2')->getDataValidation();
            $validation->setType(DataValidation::TYPE_WHOLE);
            $validation->setErrorStyle(DataValidation::STYLE_STOP);
            $validation->setAllowBlank(true);
            $validation->setShowInputMessage(true);
            $validation->setShowErrorMessage(true);
            $validation->setErrorTitle('Input error');
            $validation->setError('Only numbers between 1 and 7 are allowed!');
            $validation->setPromptTitle('Allowed input');
            $validation->setPrompt('Only numbers between 1 and 7 are allowed.');
            $validation->setFormula1(1);
            $validation->setFormula2(7);
            $spreadsheet->getActiveSheet()->getCell('J'.$k)->setDataValidation($validation);*/
        }




        $spreadsheet->getActiveSheet()->setTitle('Client Excel');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="ClientExcel.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');
        exit;


    }



    public function excelup(Request $request)
    {
       
        if(!isset($request['upfile']))
        {
            Session::flash('error_msg', 'Please select excel file to upload.');
            return redirect()->route('client-excel');
        }
        
        if(isset($request['upfile']))
        {
           $extension = File::extension($request['upfile']->getClientOriginalName());
           if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
              //'Your file is a valid xls or csv file'
           }else {
              Session::flash('error_msg', 'Please select excel file (xlsx,xls,csv) to upload.');
              return redirect()->route('client-excel');
           }
        }

        $assigneeid = Auth::user()->id;
        $inputFileName = $request['upfile'];
        //dd( $inputFileName);
        $helper = new Sample();
        //dd($helper);
       // $helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
        $spreadsheet = IOFactory::load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $excel_row = array_shift($sheetData);


        foreach ($sheetData as $key => $value)
        {
            //dd($value);
            $clients = DB::table('tbl_clients')
                        ->select('client_id')
                        ->where('email',$value['D'])
                        ->get();

            if($clients->isEmpty()){
                DB::table('tbl_clients')
                    ->insert([
                        'comp_name' => $value['A'],
                        'contact_name' => $value['B'],
                        'phone' => $value['C'],
                        'email' => $value['D'],
                        'designation' => $value['E'],
                        'company_url' => $value['F'],
                        'company_size' => $value['G'],
                        'from_time' => $value['H'],
                        'to_time' => $value['I'],
                        'days_working' => $value['J'],
                        'industry' => $value['K'],
                        'company_specialties' => $value['L'],
                        'headquarters' => $value['M'],
                        'office_address' => $value['N'],
                        'clientsuid' => $assigneeid,
                        'client_catid' => $request['catid'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'client_catid' => $request['catid'],
                         'alternate_phone' => $value['O'],
                          'selling_point' => $value['P'],
                           'summary' => $value['Q'],
                            'contractual_sla' => $value['R'],
                             'contact_name1' => $value['S'],
                              'designation1' => $value['T'],
                               'phone1' => $value['U'],
                                'email1' => $value['V'],
                                 'contact_name2' => $value['W'],
                                  'designation2' => $value['X'],
                                   'phone2' => $value['Y'],
                                    'email2' => $value['Z'],
                                  
                                      
                ]);
            }
        }
        // $client=new Client();

        // foreach ($sheetData as $key=>$value)
        // {

        //     $client->comp_name=$value['A'];
        //     $client->contact_name=$value['B'];
        //     $client->phone=$value['C'];
        //     $client->email=$value['D'];
        //     $client->designation=$value['E'];
        //     $client->save();

        // }

        Session::flash('success_msg', 'Excel Uploaded  Successfully!');
        return redirect()->route('client-excel');
    }


    public function editclient($client_id)
    {
//
         $clientidntd = ($client_id);
        $clientid = Crypt::decrypt($client_id);
        $client_details = DB::table('tbl_clients')
            ->select('*')
            ->where('client_id',$clientid)
            ->get();
  $clientadddetails = DB::table('tbl_clientsdetails')
    ->select('*')
    ->where('clientsdetails_clientid',$clientid)
    ->get();
  $statedata=  DB::table('tbl_state as c')->select('*')->whereRaw('c.state_status!=2')->orderby('state_id','desc')->get();
        return view('Emp.editclient',compact('client_details','clientadddetails','clientidntd','statedata'));
    }


    public function updateClient($client_id,Request $request)
    {
       // dd($request);
        $this->validate($request,[
            'comp_name' => 'required',
           /* 'company_url' => 'required',*/
            'email' => 'required|email|unique:users',
            'contact_name' => 'required',
            'designation' => 'required',
            'phone' => 'required',
            'email' => 'required',
           /* 'company_size' => 'required',
            'from_timings' => 'required',
            'to_timings'=>'required',
            'days_working' => 'required',
            'company_size' => 'required',
            'industry' => 'required',
            'headquarters' => 'required',
            'company_specialties' => 'required',
            'office_address' => 'required',
            'selling_point' => 'required',*/
        ],[
            'comp_name.required' => 'Company name field is required.',
           /* 'company_url.required' => 'Company Url is required.',*/
            'email.required' => 'Email field is required.',
            'contact_name.required' => 'Contact number field is required.',
            'designation.required' => 'Designation field is required.',
            'phone.required' => 'Phone number field is required.',
            'phone.max'=>'Phone number field must be of 10 digit only.',
          /*  'company_size.required' => 'Company Size is required.',
            'from_timings.required' => 'From Timings is required.',
            'to_timings.required' => 'To Timings is required.',
            'days_working.required' => 'Days of working field is required',
            'industry.required' => 'Industry field is required',
            'headquarters.required' => 'Headquarters field is required',
            'company_specialties.required' => 'Company Specialties field is required',
            'office_address.required' => 'Office address field is required',
            'selling_point.required' => 'Selling Point field is required',*/
        ]);


        $assigneeid = Auth::user()->id;

        $clientid = Crypt::decrypt($client_id);

        $client_obj = new Client();

    if($request->file('input_img')!=''){
        $image = $request->file('input_img');
        echo "<pre>"; print_r($image );
        $finename = $image->getClientOriginalName();
        $input['logo'] =  $finename;
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['logo']);

        DB::table('tbl_clients')
            ->where('client_id',$clientid)
            ->update(['comp_name' => $request['comp_name'],
                'company_url' => $request['company_url'],
                'contact_name' => $request['contact_name'],
                'designation' => $request['designation'],
                'phone' => $request['phone'],
                'alternate_phone' => $request['alternate_phone'],
                'email' => $request['email'],
                'contact_name1' => $request['contact_name1'],
                'designation1' => $request['designation1'],
                'phone1' => $request['phone1'],
                'email1' => $request['email1'],
                 'contact_name2' => $request['contact_name2'],
                'designation2' => $request['designation2'],
                'phone2' => $request['phone2'],
                'email2' => $request['email2'],
                'company_size' => $request['company_size'],
                'from_time' => $request['from_timings'],
                'to_time' => $request['to_timings'],
                'days_working' => $request['days_working'],
                'industry' => $request['industry'],
                'headquarters' => $request['headquarters'],
                'company_specialties' => $request['company_specialties'],
                'office_address' => $request['office_address'],
                'selling_point' => $request['selling_point'],
                'summary' => $request['summary'],
                'logo' => $finename,
                'contractual_sla' => $request['contractual_sla']]);
        }else{
                    DB::table('tbl_clients')
            ->where('client_id',$clientid)
            ->update(['comp_name' => $request['comp_name'],
                'company_url' => $request['company_url'],
                'contact_name' => $request['contact_name'],
                'designation' => $request['designation'],
                'phone' => $request['phone'],
                'alternate_phone' => $request['alternate_phone'],
                'email' => $request['email'],
                'contact_name1' => $request['contact_name1'],
                'designation1' => $request['designation1'],
                'phone1' => $request['phone1'],
                'email1' => $request['email1'],
                 'contact_name2' => $request['contact_name2'],
                'designation2' => $request['designation2'],
                'phone2' => $request['phone2'],
                'email2' => $request['email2'],
                'company_size' => $request['company_size'],
                'from_time' => $request['from_timings'],
                'to_time' => $request['to_timings'],
                'days_working' => $request['days_working'],
                'industry' => $request['industry'],
                'headquarters' => $request['headquarters'],
                'company_specialties' => $request['company_specialties'],
                'office_address' => $request['office_address'],
                'selling_point' => $request['selling_point'],
                'summary' => $request['summary'],
                'contractual_sla' => $request['contractual_sla']]);
        }
if(isset($_POST['location'])) {    
for ($x = 0; $x < (count($_POST['location'])); $x++) {
     if($_POST['location'][$x]!='') {    
DB::table('tbl_clientsdetails')->insert([
                    'clientsdetails_location'=> $_POST['location'][$x],
                    'clientsdetails_billingaddress'=> $_POST['billingaddress'][$x],
                    'clientsdetails_gst'  => $_POST['gst'][$x],
                     'clientsdetails_panno'  => $_POST['panno'][$x],
                      'clientsdetails_cityid'  => $_POST['cityid'][$x],
                    'clientsdetails_clientid' => $clientid,
                     'clientsdetails_created'=> Carbon::now(),
                    'clientsdetails_update'=> Carbon::now()
                ]);
}}}
   if(isset($_POST['locationup'])) {  
   for ($x = 0; $x < (count($_POST['locationup'])); $x++) {
     if($_POST['locationup'][$x]!='') { 

  DB::table('tbl_clientsdetails')
            ->where('clientsdetails_id',$_POST['clid'][$x])
            ->update(['clientsdetails_location' => $_POST['locationup'][$x],
                'clientsdetails_billingaddress' =>  $_POST['billingaddressup'][$x],
                'clientsdetails_gst' =>$_POST['gstup'][$x],
                  'clientsdetails_panno'  => $_POST['pannoup'][$x],
                      'clientsdetails_cityid'  => $_POST['cityidup'][$x],
'clientsdetails_update'=> Carbon::now()]);

}}

}
                
        Session::flash('success_msg', 'Client Updated Successfully!');
        return redirect()->route('listclient');
    }



public function excellistassignrecruiterdownload()
    {

       $assigneeid=Auth::user()->id;
        $users=DB::table('tbl_clientjd_master as c')
                ->join('tbl_clientjdrecruitermap as m','c.clientjob_id','=','m.fk_jdid')
                ->join('users as u','u.id','=','m.fk_empid')
                ->join('tbl_clients as cm','cm.client_id','=','c.clientjob_compid')
                ->select('m.*','c.*','u.name','cm.comp_name')
                ->get();
      
        //-----------Va Define-----------------------//
      $allArr               = array();
      $templevel            = 0;  
      $newkey               = 0;
      $grouparr[$templevel] = "";
  //-----------------------------------------------//
      /* $role=['BD','SPOC','REC','HR'];*/

      $response['array_test']  = array(
       '1'=>'BD',
       '2'=>'SPOC',
       '3'=>'Recruiter',
       );
      foreach($response['array_test'] as $key => $value){
       $fuelID[] = $key;
       $fuelName[] = $value;
    //p($fuelName); exit;
     }
     $fuelProID = implode(',',$fuelID);
     $fuelProName = implode(',',$fuelName);

     $featurevaluee1[]    = 'test';
     
     $excelHeadArrqq1 = array_merge($featurevaluee1);
     $excelHeadArr1 = array_unique($excelHeadArrqq1);
     
     $oldfinalAttrData['test']       = $fuelProName;
     $oldfinalAttrData2['test']      = $fuelName;

     $finalAttrData  = array_merge($oldfinalAttrData);
     $finalAttrData2 = $oldfinalAttrData2;
     $attCount = array();
     foreach($finalAttrData as $k=>$v){
       $valueCount   = count(explode(",", $v));
       $attCount[$k] = $valueCount+1;

     } 
     $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
     $objWorkSheet  = $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(0);

     $finalExcelArr1 = array_merge($excelHeadArr1);

     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
     $j      = 2;
     
     for($i=0;$i<count($finalExcelArr1);$i++){
       $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
       foreach ($finalAttrData2 as $key => $value) {
        foreach ($value as $k => $v) {

          if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
          }
        }
      }  
    }
    $arrPart2 = array('Position Name ','Company Name','Drive','No. of Position','Assigned Recuriter Name');
    if(!empty($excelHeadArr)){
     $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
   }else{
    $finalExcelArr = array_merge($arrPart2);
  }
  $objPHPExcel->setActiveSheetIndex(1);
  $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');
  
  $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
  $j=2;
 //Set border style for active worksheet
  for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
    $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

    foreach ($users as $key => $value) {
        //Set height for all rows.
     // $vpunchrec=explode(',', $value->punchrecord);
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      $newvar = $j+$key;
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      
    

      $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->clientjob_title);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->comp_name);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->drive_shortlist);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->clientjob_noofposition);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->name);
      
      
    }
    for($k=2;$k <1000;$k++)
    {
     $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
     $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
     
     $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
     $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
     $objValidation24->setAllowBlank(false);
     $objValidation24->setShowInputMessage(true);
     $objValidation24->setShowErrorMessage(true);
     $objValidation24->setShowDropDown(true);
     $objValidation24->setErrorTitle('Input error');
     $objValidation24->setError('Value is not in list.');
     $objValidation24->setPromptTitle('Pick from list');
     $objValidation24->setPrompt('Please pick a value from the drop-down list.');
     $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
     $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
     
  }//secfor


}

$filename  = "Positionassgnrecruiter.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
        die("a");
        exit;
        echo "fffffffffffffffff";
      }



   

public function exceldrivereportsdownload()
    {

       $assigneeid=Auth::user()->id;
       
        if(isset($_POST['user']) && $_POST['user']!='')
        {
      $quser="where cm.clientjob_title='".$_POST['user']."'";      
        }
        else
        {
            $quser="where cvall.tca_status='Active'"; 
        }
        if(isset($_POST['doend']) && $_POST['doend']!='')
        {
               $start = $_POST['doj'];
  $finish =  $_POST['doend'];
        }
        else
        {
              $start = date('Y-m-01',strtotime(date('Y-m-d')));
  $finish =  date('Y-m-t',strtotime(date('Y-m-d'))); 
        }
        
        
  
  $query = 'select rc.id, cvall.tca_positionid,cm.*,cl.comp_name ,(select count(tca_id) from tbl_cv_allocated where  tca_positionid=rc.position_id ) as total_rec_cv 
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Pending" and tca_positionid=rc.position_id and (Date(cvall.tca_createdon) between "'.$start.'" and "'.$finish.'" )) as pending
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Confirm" and tca_positionid=rc.position_id and (Date(cvall.tca_createdon) between "'.$start.'" and "'.$finish.'" )) as Confirm
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Not Going" and tca_positionid=rc.position_id and (Date(cvall.tca_createdon) between "'.$start.'" and "'.$finish.'" )) as NotGoing
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Not Reachable" and tca_positionid=rc.position_id and (Date(cvall.tca_createdon) between "'.$start.'" and "'.$finish.'" )) as NotReachable
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Not Responding" and tca_positionid=rc.position_id and (Date(cvall.tca_createdon) between "'.$start.'" and "'.$finish.'" )) as NotResponding
   ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Dicey" and tca_positionid=rc.position_id and (Date(cvall.tca_createdon) between "'.$start.'" and "'.$finish.'" )) as Dicey
  from tbl_clientjd_master as cm join tbl_recruiter_cv as rc on rc.position_id=cm.clientjob_id
  join tbl_cv_allocated as cvall on cvall.tca_cvid=rc.id join tbl_clients as cl on cl.client_id=cm.clientjob_compid and (Date(cm.drive_time) between "'.$start.'" and "'.$finish.'" )
'.$quser.'    group by rc.position_id   ';
   $users = DB::select(DB::raw($query)); 
         
         
        //-----------Va Define-----------------------//
      $allArr               = array();
      $templevel            = 0;  
      $newkey               = 0;
      $grouparr[$templevel] = "";
  //-----------------------------------------------//
      /* $role=['BD','SPOC','REC','HR'];*/

      $response['array_test']  = array(
       '1'=>'BD',
       '2'=>'SPOC',
       '3'=>'Recruiter',
       );
      foreach($response['array_test'] as $key => $value){
       $fuelID[] = $key;
       $fuelName[] = $value;
    //p($fuelName); exit;
     }
     $fuelProID = implode(',',$fuelID);
     $fuelProName = implode(',',$fuelName);

     $featurevaluee1[]    = 'test';
     
     $excelHeadArrqq1 = array_merge($featurevaluee1);
     $excelHeadArr1 = array_unique($excelHeadArrqq1);
     
     $oldfinalAttrData['test']       = $fuelProName;
     $oldfinalAttrData2['test']      = $fuelName;

     $finalAttrData  = array_merge($oldfinalAttrData);
     $finalAttrData2 = $oldfinalAttrData2;
     $attCount = array();
     foreach($finalAttrData as $k=>$v){
       $valueCount   = count(explode(",", $v));
       $attCount[$k] = $valueCount+1;

     } 
     $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
     $objWorkSheet  = $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(0);

     $finalExcelArr1 = array_merge($excelHeadArr1);

     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
     $j      = 2;
     
     for($i=0;$i<count($finalExcelArr1);$i++){
       $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
       foreach ($finalAttrData2 as $key => $value) {
        foreach ($value as $k => $v) {

          if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
          }
        }
      }  
    }
    $arrPart2 = array('Company Name','Position Name','Drive Date','No of Line UP','Pending','Confirm','Not Going','Not Reachable','Not Responding','Dicey');
    if(!empty($excelHeadArr)){
     $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
   }else{
    $finalExcelArr = array_merge($arrPart2);
  }
  $objPHPExcel->setActiveSheetIndex(1);
  $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');
  
  $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
  $j=2;
 //Set border style for active worksheet
  for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
    $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

    foreach ($users as $key => $value) {
        //Set height for all rows.
     // $vpunchrec=explode(',', $value->punchrecord);
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      $newvar = $j+$key;
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      
    

      $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->comp_name);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->clientjob_title);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->drive_time);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->total_rec_cv);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->pending);
       $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->Confirm);
        $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->NotGoing);
         $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->NotReachable);
          $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->NotResponding);
           $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->Dicey);
      
      
    }
    for($k=2;$k <1000;$k++)
    {
     $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
     $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
     
     $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
     $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
     $objValidation24->setAllowBlank(false);
     $objValidation24->setShowInputMessage(true);
     $objValidation24->setShowErrorMessage(true);
     $objValidation24->setShowDropDown(true);
     $objValidation24->setErrorTitle('Input error');
     $objValidation24->setError('Value is not in list.');
     $objValidation24->setPromptTitle('Pick from list');
     $objValidation24->setPrompt('Please pick a value from the drop-down list.');
     $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
     $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
     
  }//secfor


}

$filename  = "drivereports.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
        die("a");
        exit;
        echo "fffffffffffffffff";
      }



   


public function exceldrivereportsdatadownload()
    {

       $assigneeid=Auth::user()->id;
       
      if(isset($_POST['user']) && $_POST['user']!='')
        {
      $quser="where cm.clientjob_title='".$_POST['user']."'";      
        }
        else
        {
            $quser="where cvall.tca_status='Active'"; 
        }


        if(isset($_POST['doend']) && $_POST['doend']!='')
        {
               $start = $_POST['doj'];
  $finish =  $_POST['doend'];
        }
        else
        {
              $start = date('Y-m-01',strtotime(date('Y-m-d')));
  $finish =  date('Y-m-t',strtotime(date('Y-m-d'))); 
        }
        
  
$query = 'select rc.id, cvall.tca_positionid,cm.*,cl.comp_name
,(select count(tca_id) from tbl_cv_allocated where  tca_positionid=rc.position_id ) as total_rec_cv 
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="7" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as OnTheWay
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="8" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as Interviewed
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="9" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as NotGoing
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="17" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as NotResponding
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="18" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as Confirmed
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="22" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as shortlistclient
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="23" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as Dicey
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="24" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as Reached
 

  from tbl_clientjd_master as cm join tbl_recruiter_cv as rc on rc.position_id=cm.clientjob_id
  join tbl_cv_allocated as cvall on cvall.tca_cvid=rc.id join tbl_clients as cl on cl.client_id=cm.clientjob_compid and (Date(cm.drive_time) between "'.$start.'" and "'.$finish.'" )
   '.$quser.'  group by rc.position_id ';
  
   $users = DB::select(DB::raw($query)); 
         
         
        //-----------Va Define-----------------------//
      $allArr               = array();
      $templevel            = 0;  
      $newkey               = 0;
      $grouparr[$templevel] = "";
  //-----------------------------------------------//
      /* $role=['BD','SPOC','REC','HR'];*/

      $response['array_test']  = array(
       '1'=>'BD',
       '2'=>'SPOC',
       '3'=>'Recruiter',
       );
      foreach($response['array_test'] as $key => $value){
       $fuelID[] = $key;
       $fuelName[] = $value;
    //p($fuelName); exit;
     }
     $fuelProID = implode(',',$fuelID);
     $fuelProName = implode(',',$fuelName);

     $featurevaluee1[]    = 'test';
     
     $excelHeadArrqq1 = array_merge($featurevaluee1);
     $excelHeadArr1 = array_unique($excelHeadArrqq1);
     
     $oldfinalAttrData['test']       = $fuelProName;
     $oldfinalAttrData2['test']      = $fuelName;

     $finalAttrData  = array_merge($oldfinalAttrData);
     $finalAttrData2 = $oldfinalAttrData2;
     $attCount = array();
     foreach($finalAttrData as $k=>$v){
       $valueCount   = count(explode(",", $v));
       $attCount[$k] = $valueCount+1;

     } 
     $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
     $objWorkSheet  = $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(0);

     $finalExcelArr1 = array_merge($excelHeadArr1);

     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
     $j      = 2;
     
     for($i=0;$i<count($finalExcelArr1);$i++){
       $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
       foreach ($finalAttrData2 as $key => $value) {
        foreach ($value as $k => $v) {

          if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
          }
        }
      }  
    }
    $arrPart2 = array('Company Name','Position Name','Drive Date','No of Line UP','On The Way','Interviewed','Not Going','Not Responding','Confirmed','Shortlist Client','Dicey','Reached');
    if(!empty($excelHeadArr)){
     $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
   }else{
    $finalExcelArr = array_merge($arrPart2);
  }
  $objPHPExcel->setActiveSheetIndex(1);
  $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');
  
  $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
  $j=2;
 //Set border style for active worksheet
  for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
    $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

    foreach ($users as $key => $value) {
        //Set height for all rows.
     // $vpunchrec=explode(',', $value->punchrecord);
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      $newvar = $j+$key;
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      
    

      $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->comp_name);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->clientjob_title);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->drive_time);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->total_rec_cv);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->OnTheWay);
       $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->Interviewed);
        $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->NotGoing);
         $objPHPExcel->getActiveSheet()->setCellValue($cols[7].$newvar, $value->NotResponding);
          $objPHPExcel->getActiveSheet()->setCellValue($cols[8].$newvar, $value->Confirmed);
           $objPHPExcel->getActiveSheet()->setCellValue($cols[9].$newvar, $value->shortlistclient);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[10].$newvar, $value->Dicey);
             $objPHPExcel->getActiveSheet()->setCellValue($cols[11].$newvar, $value->Reached);
      
      
    }
    for($k=2;$k <1000;$k++)
    {
     $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
     $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
     
     $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
     $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
     $objValidation24->setAllowBlank(false);
     $objValidation24->setShowInputMessage(true);
     $objValidation24->setShowErrorMessage(true);
     $objValidation24->setShowDropDown(true);
     $objValidation24->setErrorTitle('Input error');
     $objValidation24->setError('Value is not in list.');
     $objValidation24->setPromptTitle('Pick from list');
     $objValidation24->setPrompt('Please pick a value from the drop-down list.');
     $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
     $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
     
  }//secfor


}

$filename  = "DriveStatusReport.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
        die("a");
        exit;
        echo "fffffffffffffffff";
      }



   
public function excellistjoinedcandidatedownload(Request $request)
    {
//print_r($_POST); exit;
       $assigneeid=Auth::user()->id;
       
       
    $usersval= DB::select("select * from users where emp_role like '%3%'");      

     //dd($_GET['user']);
    //echo date('Y-m-01',strtotime(date('Y-m-d')));

        
        if(isset($_POST['user']) && $_POST['user']!=''){
          $name=$_POST['user'];
        }else{
          $name=-1;
        }
        if(isset($_POST['doj']) && $_POST['doj']!=''){
          $from=$_POST['doj'];
        }else{
          $from=date('Y-m-01',strtotime(date('Y-m-d')));
        }
        if(isset($_POST['doend']) && $_POST['doend']!=''){
          $to=$_POST['doend'];
        }else{
          $to=date('Y-m-t',strtotime(date('Y-m-d')));;
        }
        if(isset($_POST['spoc']) && $_POST['spoc']!=''){
          $spoc=$_POST['spoc'];
        }else{
          $spoc=-1;
        }
        if(isset($_POST['client']) && $_POST['client']!=''){
          $client=$_POST['client'];
        }else{
          $client=-1;
        }
        if(isset($_POST['position']) && $_POST['position']!=''){
          $position=$_POST['position'];
        }else{
          $position=-1;
        }
        
       $query = "select r.expectedsalery,r.dateoj,c.comp_name,jd.clientjob_title,u.name as recruitername,uspoc.name as spocname,rcv.candidate_name from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id join tbl_clients as c on rcv.clientsid=c.client_id join tbl_clientjd_master as jd on rcv.position_id=jd.clientjob_id join users as u on rcv.recruiter_id=u.id join users as uspoc on rcv.spockid=uspoc.id where r.cv_status=6 and r.cvupdatestatus=1 and date(r.dateoj) BETWEEN '".$from."' and  '".$to."' and (u.name like concat('%','".$name."','%') or '".$name."'=-1) and (uspoc.name like concat('%','".$spoc."','%') or '".$spoc."'=-1) and (c.comp_name like concat('%','".$client."','%') or '".$client."'=-1) and (jd.clientjob_title like concat('%','".$position."','%') or '".$position."'=-1) order by r.dateoj asc
       ";
      $users = DB::select(DB::raw($query));
      
      
      
        //-----------Va Define-----------------------//
      $allArr               = array();
      $templevel            = 0;  
      $newkey               = 0;
      $grouparr[$templevel] = "";
  //-----------------------------------------------//
      /* $role=['BD','SPOC','REC','HR'];*/

      $response['array_test']  = array(
       '1'=>'BD',
       '2'=>'SPOC',
       '3'=>'Recruiter',
       );
      foreach($response['array_test'] as $key => $value){
       $fuelID[] = $key;
       $fuelName[] = $value;
    //p($fuelName); exit;
     }
     $fuelProID = implode(',',$fuelID);
     $fuelProName = implode(',',$fuelName);

     $featurevaluee1[]    = 'test';
     
     $excelHeadArrqq1 = array_merge($featurevaluee1);
     $excelHeadArr1 = array_unique($excelHeadArrqq1);
     
     $oldfinalAttrData['test']       = $fuelProName;
     $oldfinalAttrData2['test']      = $fuelName;

     $finalAttrData  = array_merge($oldfinalAttrData);
     $finalAttrData2 = $oldfinalAttrData2;
     $attCount = array();
     foreach($finalAttrData as $k=>$v){
       $valueCount   = count(explode(",", $v));
       $attCount[$k] = $valueCount+1;

     } 
     $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
     $objWorkSheet  = $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(0);

     $finalExcelArr1 = array_merge($excelHeadArr1);

     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
     $j      = 2;
     
     for($i=0;$i<count($finalExcelArr1);$i++){
       $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
       foreach ($finalAttrData2 as $key => $value) {
        foreach ($value as $k => $v) {

          if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
          }
        }
      }  
    }
    $arrPart2 = array('Client Name ','Position Name','Recruiter Name','Spoc Name','Candidate Name','Join Date','Offered Salery');
    if(!empty($excelHeadArr)){
     $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
   }else{
    $finalExcelArr = array_merge($arrPart2);
  }
  $objPHPExcel->setActiveSheetIndex(1);
  $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');
  
  $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
  $j=2;
 //Set border style for active worksheet
  for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
    $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

    foreach ($users as $key => $value) {
        //Set height for all rows.
     // $vpunchrec=explode(',', $value->punchrecord);
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      $newvar = $j+$key;
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      
    

      $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->comp_name);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->clientjob_title);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->recruitername);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->spocname);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->candidate_name);
         $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->dateoj);
         $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $value->expectedsalery);
    }
    for($k=2;$k <1000;$k++)
    {
     $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
     $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
     
     $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
     $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
     $objValidation24->setAllowBlank(false);
     $objValidation24->setShowInputMessage(true);
     $objValidation24->setShowErrorMessage(true);
     $objValidation24->setShowDropDown(true);
     $objValidation24->setErrorTitle('Input error');
     $objValidation24->setError('Value is not in list.');
     $objValidation24->setPromptTitle('Pick from list');
     $objValidation24->setPrompt('Please pick a value from the drop-down list.');
     $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
     $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
     
  }//secfor


}

$filename  = "joinedcandidate.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
        die("a");
        exit;
        echo "fffffffffffffffff";
      }




 public function deleteclient($id,$client_id)
    {
 DB::table('tbl_clientsdetails')->where('clientsdetails_id', '=', $id)->delete();
  return redirect('editclient/'.$client_id);

    }



}
