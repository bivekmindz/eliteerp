<?php

namespace App\Http\Controllers\Emp;
use App\Events\RecStartUploadEvent;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Model\Adminmodel;
use App\Model\Adminmodel\Employees;
use Illuminate\Support\Facades\Input;
use App\Model\User;
use App\Model\Admin;
use Carbon\Carbon;
use Session;
use Storage;
use DB;
use Mail;

class ChatController extends Controller
{
    
    public function index($id=null){
    	//dd($id);
    	$loginId = Auth::user()->id;
    	$list = DB::table('users')->where('emp_status','=',1)->where('emp_role','!=','4')
        ->orderBy('name', 'asc')->get();
    	if($id!=''){

    // Update Read Status Of Text Sms //
        $update = DB::table('tbl_chats')->where('from','=',$id)->where('to','=',$loginId)->update(['read_status' => 0]);
    //End Update Read status //

    	$sql = "SELECT * FROM `tbl_chats` where (`from`=$id and `to`=$loginId) or (`from`=$loginId and `to`=$id) order by id asc";
    	$msg = DB::select(DB::raw($sql));
    	}

    	return view('Emp.chat', compact('list', 'msg'));
    }


    public function insertchat(Request $request){
       if ($request->isMethod('post')){
       //print_r($request->all());

            $to = $request['to'];
            $from = Auth::user()->id;
            $msg = $request['chat'];            
            if(!empty($to)){
 
            $insert = DB::table('tbl_chats')->insert(['from'=> $from, 'to'=>$to, 'msg'=>$msg]);
            }
        }        
    }

// Insert Chat On Submit 
    //     public function insertchatenter(Request $request){

    //         if ($request->isMethod('post')){
    //         $to = $request['to'];
    //         $from = Auth::user()->id;
    //         $msg = $request['chat'];            
    //         if(!empty($to)){
 
    //         $insert = DB::table('tbl_chats')->insert(['from'=> $from, 'to'=>$to, 'msg'=>$msg]);
    //         }
    //     }        
    // }


        public function autorefresh(Request $request){

        $id = $request['to'];
        $loginId = Auth::user()->id;
        if($id!=''){
        echo $sql = "SELECT * FROM `tbl_chats` where (`from`=$id and `to`=$loginId) or (`from`=$loginId and `to`=$id) order by id asc";
        $msg = DB::select(DB::raw($sql));
        
        foreach($msg as $sms){


                        echo '<div class="messages" style="position: relative;"  id="responce" >';

                              if($sms->from == $loginId){
                              echo '<div class="message out no-avatar  media m-b-20">
                                    <div class="body media-body text-right p-l-50">
                                        <div style="background-color: #2a653d!important;margin-bottom: 2px!important;border-radius: 0px 10px 0px!important;" class="content msg-reply p-5 f-12 bg-primary d-inline-block">'.$sms->msg.'
                                        </div>
                                        <div class="seen">
                                            <i class="icon-clock f-12 m-r-5 txt-muted d-inline-block"></i>
                                                <span><p  class="d-inline-block"><small>'.date('M d H:i',strtotime($sms->created_at)).'</small></p></span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>';
                            }else{

                            echo    '<div class="message out no-avatar  media m-b-20" >
                                    <div class="body media-body text-right p-r-50">
                                        <div style="margin-bottom: 2px!important;border-radius: 0px 10px 0px!important;" class="content msg-reply p-5 f-12 bg-primary d-inline-block">'.$sms->msg.'
                                        </div>
                                        <div class="seen">
                                            <i class="icon-clock f-12 m-r-5 txt-muted d-inline-block"></i>
                                                <span><p class="d-inline-block"><small>'.date('M d H:i',strtotime($sms->created_at)).'</small></p></span>
                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                </div>
  
                              </div>';
                        }
                }
        }

    }
    
}
