<?php
namespace App\Http\Controllers\Emp;
use App\Events\RecStartUploadEvent;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Model\Adminmodel;
use App\Model\Adminmodel\Employees;
use Illuminate\Support\Facades\Input;
use App\Model\User;
use App\Model\Admin;
use Carbon\Carbon;
use Session;
use Storage;
use DB;
use Mail;


class StartmailController extends Controller
{

public function gettime()
        {
           // $reason = $_GET['reason'];

            $mytime = Carbon::now();
            $currentTime = $mytime->toTimeString();
            $currentDate = Date('Y-m-d',strtotime($mytime));
            // print_r($currentDate);
            $recruiterId = Auth::user()->id;

            $sql = "SELECT count(*) FROM `tbl_clientrec_start_endtime` WHERE ismail=0 and date(created_at)='".$currentDate."'";

            $checkTime = DB::select(DB::raw($sql));
            $sql = "SELECT * FROM `tbl_clientrec_start_endtime` WHERE ismail=0 and date(created_at)='".$currentDate."'";

            $checkdata = DB::select(DB::raw($sql));

               // print_r($checkdata);die;
            foreach ($checkdata as $key => $value) {
            	# code...
            

			$posid = $value->clientreq_id;
         
            $clientstartid = $value->clientstartid;
            $recid = $value->clientstart_recuiterid;

            // print_r($posid);
            // print_r($recid);
            
            $sql = "SELECT jd.clientjob_id,jd.clientjob_title from tbl_clientjd_master as jd JOIN tbl_clientjdrecruitermap as jdr on jd.clientjob_id=jdr.fk_jdid inner join tbl_clientrec_start_endtime as strt on jdr.clientreq_id=$posid order by strt.clientstart_starttime DESC limit 0,1"; 


            $getpostion = DB::select(DB::raw($sql));
            $positionid = $getpostion[0]->clientjob_id;
          // print_r($getpostion);die;




            // $getpostion = DB::table('tbl_clientjd_master as st')
            //     ->where('st.clientjob_id','=',$id)

            //     ->select('st.*')
            //     ->orderBy('st.clientjob_id', 'desc')
            //     ->first();
             $arr = [];
           
                $dataemp = Employees::select("*")->where('id','=',"{$recid}")->get();
                $dataadmin = User::select("*")->where('id','=',"1")->get();

                $emailuser= $dataemp[0]['email'];
                $emailadmin= $dataadmin[0]['email'];

                $spoc=DB::table('tbl_clientjd_master as c')
                    ->join('users as u','c.clientjob_empid','=','u.id')
                    ->where('c.clientjob_id',$positionid)
                    ->select('u.email','u.name')
                    ->get();
                     
                $spocemail=$spoc[0]->email;
                $spocname=$spoc[0]->name;
                $adminemail=$dataadmin[0]['email'];
                $adminname=$dataadmin[0]['name'];
                $to=[['name'=>$spocname,'email'=>$spocemail],['name'=>$adminname,'email'=>$adminemail]];

                $data = [
                    'name' => $dataemp[0]['name'],
                    'email' =>$dataemp[0]['email'],
                    'adminname' =>$adminname,
                    'positionname' =>$getpostion[0]->clientjob_title,
                    'emailadmin' =>$adminemail,
                ];


//echo($data['email']."--".$data['name']);


                Mail::send('Emp.mails.starttimemail', $data, function ($message) use ($data,$to) {

                    $message->from($data['email'], $data['name']);
             //  $message->from('mindzbug@gmail.com', $data['name']);
                    foreach ($to as $key=>$value)
                    {
                        //$message->to($value['email'], $value['name']);
                    	$message->to('erp@elitehrpractices.com');
                    }
                    $message->subject('Started work on position');
                });


array_push($arr,$clientstartid);
// print_r($arr);
$id = implode(',', $arr);
// print_r($id);

 $sql ="update tbl_clientrec_start_endtime set ismail=1  WHERE clientstartid IN ($id)";

 $update = DB::update($sql);

    // Session::put('time.starttimevalue', $currentTime);
                // return 'yes';
            }


         } 

        




public function sendMail()
        {
        	// echo 111;
             // $recid=Auth::user()->id;

            $livetime = date('Y-m-d H:i:s ', time() - 3600);
            $t=date('Y-m-d H:i:s ',time());

            // $getpostion = DB::table('tbl_clientjd_master as st')
            //     ->where('st.clientjob_id','=',$id)

            //     ->select('st.*')
            //     ->orderBy('st.clientjob_id', 'desc')
            //     ->first();
// echo $id;
// dd($getpostion);
            $getjst = DB::table('tbl_clientrec_start_endtime as st')
                // ->where('st.clientstart_recuiterid','=',$recid)
                // ->where('st.clientreq_id','=',$id)
                ->where('st.ismail','=',0)
                // ->select('st.clientstartid','st.clientreq_id','st.clientstart_starttime','st.clientstart_recuiterid')
                ->select('st.*')
                ->orderBy('st.clientstartid', 'desc')
                 ->limit(5)
                ->get();
                // print_r($getjst);
          $arr = [];      
        foreach ($getjst as $key => $value) {
            	# code...
        	// echo '<pre>';
              // print_r($getjst);
             // die('/');
            	if($value->clientstartid>0){
            	
            	

            	$posid = $value->clientreq_id;
                $recid = $value->clientstart_recuiterid;
                $clientstartid = $value->clientstartid;

                //  print_r($recid);

                $sql = "SELECT jd.clientjob_id,jd.clientjob_title from tbl_clientjd_master as jd JOIN tbl_clientjdrecruitermap as jdr on jd.clientjob_id=jdr.fk_jdid inner join tbl_clientrec_start_endtime as strt on jdr.clientreq_id=$posid order by strt.clientstart_starttime DESC limit 0,1"; 


                $getpostion = DB::select(DB::raw($sql));

                   // print_r($getpostion);
                $positionid = $getpostion[0]->clientjob_id;

            $dataemp = Employees::select("*")->where('id','=',"{$recid}")->get();
            $dataadmin = User::select("*")->where('id','=',"1")->get();
            $emailuser= $dataemp[0]['email'];
            $emailadmin= $dataadmin[0]['email'];



    //dd($getjst->clientstart_starttime);

            if(!empty($value->clientstart_starttime)) {
                //  echo "dddddddddddd";die;
                if(empty($value->clientstart_stoptime)) {


                    $getcvupload = DB::table('tbl_recruiter_cv as rc')
                        ->select(DB::raw('COUNT(rc.id) as cvcount'))
                        ->whereBetween('rc.created_at', [$livetime, $t])
                        ->where('rc.recruiter_id', '=', $recid)
                        ->where('rc.position_id', '=', $positionid)
                        ->get();
// echo '<pre>';
// print_r($getcvupload[0]->cvcount);
// die('/');
                    if ($getcvupload[0]->cvcount  == 0) {


                        $data= [
                            'name' => $dataemp[0]['name'],
                            'email' =>$dataemp[0]['email'],
                            'adminname' =>$dataadmin[0]['name'],
                            'positionname' =>$getpostion[0]->clientjob_title,
                            'emailadmin' =>$emailadmin

                        ];
     // print_r($data);
    					$query = "select u.email,u.name from tbl_clientjd_master as c inner join users as u on c.clientjob_empid=u.id where c.clientjob_id=$positionid";

    					$spoc = DB::select(DB::raw($query));

                        // $spoc=DB::table('tbl_clientjd_master as c')
                        //     ->join('users as u','c.clientjob_empid','=','u.id')
                        //     ->where('c.clientjob_id',$posid)
                        //     ->select('u.email','u.name')
                        //     ->get();

                         // print_r($spoc);

                        $spocemail=$spoc[0]->email;
                        $spocname=$spoc[0]->name;
                        $adminemail=$dataadmin[0]['email'];
                        $adminname=$dataadmin[0]['name'];

                         $to=[['name'=>$spocname,'email'=>$spocemail],['name'=>$adminname,'email'=>$adminemail]];
   // print_r($to);die('//');
                        Mail::send('Emp.mails.sendonehoursautomail',$data, function($message) use ($data,$to)
                        {
                            $message->from($data['email'],$data['name']);
                            // $message->to('bivek@mindztechnology.com', 'Shalini');
                            foreach ($to as $key=>$value)
                            {
                                  // $message->to($value['email'], $value['name']);
                                $message->to('erp@elitehrpractices.com');

                            }
                            $message->subject('Recruiter Facing Issue on Cv Upload');
                        });
                      
				         /*     Mail::send('Emp.sendonehoursautomail', $data, function ($message) use ($data) {
                                $message->from('gurpreetsingh@mindztechnology.com', 'Gurpreet Singh');
                                $message->to('bivek@mindztechnology.com', 'Shalini');
                                $message->subject('User has not submit any Resume');
                            });
            */
                        array_push($arr,$clientstartid);
                    }

                }
            }

                // else
                // {
                //     echo "bvek";die;
                // }

            }
            
            
            }
//print_r($arr);
$id = implode(',', $arr);
//print_r($id);

 $sql ="update tbl_clientrec_start_endtime set ismail=1  WHERE clientstartid IN ($id)";

 $update = DB::update($sql);
        
        
            // else
            // {
            //     echo "hgghgh";die;

            // }
        }/*end method*/
    }
    /*end class*/