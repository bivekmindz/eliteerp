<?php

namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Model\User as User;
use App\Model\Role as Role;
use Session;

class EmployeeController extends Controller
{
    //
    public function employee(){
    	$roles=Role::orderBy('role_name','asc');
    	//print_r($roles);die;
    	return view('Emp.newemp',compact('roles'));
    }

    public function save(Request $request)
    {
    	//echo 'hi'; die;

        $this->validate($request,[
            'name' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'doj' => 'required',
            'dept' => 'required',
            'role' => 'required'
        ],[
            'name.required' => 'Name field is required.',
            'phone.required' => 'Phone number field is required.',
            'email.required' => 'Email field is required',
            'password.required' => 'Password field is required',
//            'phone.max'=>'Phone number field must be of 10 digit only',
            'doj.required' => ' Date of joining field is required',
            'dept.required' => ' Department is required',
            'role.required' => ' Role is required'
        ]);



        $postData = $request->all();
        $role = implode(',', $request['role']);
        $user_obj= new User;
        $user_obj->name=$request['name'];
        $user_obj->emp_contactno  = $request['phone'];
        $user_obj->email=$request['email'];
        $user_obj->password=$request['password'];
        $user_obj->emp_doj=$request['doj'];
        $user_obj->emp_dept =$request['dept'];
        $user_obj->emp_role=$role;
        //print_r($user_obj); //die;
        $user_obj->save();

       Session::flash('success_msg', 'Employee Added Successfully!');

        return redirect()->route('employee');

    }
    
    public function empexcel()
    {
        $users = DB::table('users')->where('emp_role','!=',4)->get();
      //  dd($users);
        return view('admin/empexcel',compact('users'));
    }
    
    
     public function hrattupanddown()
    {
        // where('empcode','=',date('Y-m-d')) whereBetween('reservation_from', [$from, $to])
    if(isset($_GET['user']))
     {
         if(isset($_GET['doj']))
     {
         $from=$_GET['doj'];
          $to=$_GET['doend'];
     }
     else
     {
        $from=date('Y-m-d');
          $to=date('Y-m-d');  
     }
     if($_GET['user']!='0') {
        $users = DB::table('hrattendance')->whereBetween('created_at', [$from, $to])->where('empcode','=',$_GET['user'])->get();
     }
     else
     {
           $users = DB::table('hrattendance')->whereBetween('created_at', [$from, $to])->get();
     }
     }
    else{
    
     if(isset($_GET['doj']))
     {
         $from=$_GET['doj'];
          $to=$_GET['doend'];
     }
     else
     {
        $from=date('Y-m-d');
          $to=date('Y-m-d');  
     }
        $users = DB::table('hrattendance')->whereBetween('created_at', [$from, $to])->get();
    }
        
      //  dd($users);
        return view('admin/hrattupanddown',compact('users'));
    }




    public function excelup(Request $request)
    {
        if(!isset($request['upfile'])){
            Session::flash('error_msg', 'Please select excel file to upload.');
            return redirect()->route('empexcel');
        }
        
        if(isset($request['upfile']))
        {
           $extension = File::extension($request['upfile']->getClientOriginalName());
           if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
              //'Your file is a valid xls or csv file'
           }else {
              Session::flash('error_msg', 'Please select excel file (xlsx,xls,csv) to upload.');
              return redirect()->route('empexcel');
           }
        }

        $inputFileName = $request['upfile'];
        $helper = new Sample();
       // $helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
        $spreadsheet = IOFactory::load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $excel_row = array_shift($sheetData);
        $m=0;
       // dd($sheetData);
        foreach ($sheetData as $key=>$value)
        {
             $roleId = DB::table('tbl_role as tr')
                         ->select('tr.role_id')
                        ->where('role_name',$value['A'])
                         ->first();
              //  dd($roleId);      
                 $password = bcrypt($value['D']);
                $id1=DB::table('users')->insert(
                [
                'emp_role' => $roleId->role_id,
                'name' => $value['B'] ? $value['B'] : '' ,
                'email' => $value['C'] ? $value['C'] : "" ,
                'password' => $password ? $password : "" ,
                'emp_contactno' => $value['E'] ? $value['E'] : "",   
                'emp_doj' => $value['F'] ? $value['F'] : "" ,
                'emp_dob' => $value['G'] ? $value['G'] : "",
  'emp_empid' => $value['J'] ? $value['J'] : "",
                 ]
            );
            
               $id2 = DB::getPdo()->lastInsertId();
               
               
               if($value['H']==0)
               {

                DB::table('tbl_team')->insert([
                     'team_lead_id'=> $id2 ? $id2 : "0" ,
                     'team_name'=> $value['I']

                ]);

               }
               else
               {
                DB::table('tbl_team')->insert(
               [
               'team_members_id'=> $id2 ? $id2 : "0" ,
               'team_name'=>$value['I']
                  ]);

               }

     $m++;  
        }
        Session::flash('success_msg', 'Excel Uploaded  Successfully!');
        return redirect()->route('empexcel');
    }


   
        public function attendanceexcelup(Request $request)
    {
        if(!isset($request['upfile'])){
            Session::flash('error_msg', 'Please select excel file to upload.');
            return redirect()->route('empexcel');
        }
        
        if(isset($request['upfile']))
        {
           $extension = File::extension($request['upfile']->getClientOriginalName());
           if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
              //'Your file is a valid xls or csv file'
           }else {
              Session::flash('error_msg', 'Please select excel file (xlsx,xls,csv) to upload.');
              return redirect()->route('empexcel');
           }
        }


 $users= DB::table('hrattendance') ->select(DB::raw('count(attendid) as attendids'))->where('created_at','=',date('Y-m-d'))->get();

if($users[0]->attendids!='0')
{
  DB::table('hrattendance')->where('created_at', '=', date('Y-m-d'))->delete();
}




        $inputFileName = $request['upfile'];
        $helper = new Sample();
       // $helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
        $spreadsheet = IOFactory::load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $excel_row = array_shift($sheetData);
        $m=0;
        //dd($sheetData);
        foreach ($sheetData as $key=>$value)
        {
             /*$roleId = DB::table('tbl_leavetype as tr')
                         ->select('tr.role_id')
                        ->where('role_name',$value['A'])
                         ->first();
            //   dd($roleId);      */
               
                $id1=DB::table('hrattendance')->insert(
                [
                'leavestatus' =>$value['A'] ? $value['A'] : '' ,
                'leaverecord' => $value['B'] ? $value['B'] : '' ,
                'empcode' => $value['C'] ? $value['C'] : "" ,
                'empname' => $value['D'] ? $value['D'] : "",   
                 'created_at' => date("Y-m-d"),   
              
                 ]
            );
            
               $id2 = DB::getPdo()->lastInsertId();
               
               
             

     $m++;  
        }
        Session::flash('success_msg', 'Excel Uploaded  Successfully!');
        return redirect()->route('hrattupanddown');
    }

    


 public function exceldownload()
 {
    //-----------Va Define-----------------------//
     $allArr               = array();
     $templevel            = 0;  
     $newkey               = 0;
     $grouparr[$templevel] = "";
  //-----------------------------------------------//
/* $role=['BD','SPOC','REC','HR'];*/

    $response['array_test']  = array(
       '1'=>'BD',
       '2'=>'SPOC',
       '3'=>'Recruiter',
      );
    foreach($response['array_test'] as $key => $value){
     $fuelID[] = $key;
     $fuelName[] = $value;
    //p($fuelName); exit;
   }
      $fuelProID = implode(',',$fuelID);
       $fuelProName = implode(',',$fuelName);

       $featurevaluee1[]    = 'test';
 
      $excelHeadArrqq1 = array_merge($featurevaluee1);
      $excelHeadArr1 = array_unique($excelHeadArrqq1);
      
      $oldfinalAttrData['test']       = $fuelProName;
      $oldfinalAttrData2['test']      = $fuelName;

       $finalAttrData  = array_merge($oldfinalAttrData);
       $finalAttrData2 = $oldfinalAttrData2;
       $attCount = array();
       foreach($finalAttrData as $k=>$v){
       $valueCount   = count(explode(",", $v));
       $attCount[$k] = $valueCount+1;

      } 
             $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
    $objWorkSheet  = $objPHPExcel->createSheet();
    $objPHPExcel->setActiveSheetIndex(0);

    $finalExcelArr1 = array_merge($excelHeadArr1);
//p($finalAttrData2);exit;
     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
        $j      = 2;
       
        for($i=0;$i<count($finalExcelArr1);$i++){
         $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
         foreach ($finalAttrData2 as $key => $value) {
          foreach ($value as $k => $v) {

            if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
            }
        }
      }  
    }
           $arrPart2 = array('Role','Name','Email','Password','Contact No','DOJ','DOB','Team Lead','Team Name','Emp Id');
          if(!empty($excelHeadArr)){
             $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
         }else{
            $finalExcelArr = array_merge($arrPart2);
         }
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');
      
    $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
 
 //Set border style for active worksheet
for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

          for($k=2;$k <1000;$k++)
          {
 $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
    $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
 
    $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
    $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
    $objValidation24->setAllowBlank(false);
    $objValidation24->setShowInputMessage(true);
    $objValidation24->setShowErrorMessage(true);
    $objValidation24->setShowDropDown(true);
    $objValidation24->setErrorTitle('Input error');
    $objValidation24->setError('Value is not in list.');
    $objValidation24->setPromptTitle('Pick from list');
    $objValidation24->setPrompt('Please pick a value from the drop-down list.');
    $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
    $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
  
  }//secfor


}

        $filename  = "EmployeeExcel.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
die("a");
        exit;
          
}





 public function attendanceexceldownload()
 {
    //-----------Va Define-----------------------//
     $allArr               = array();
     $templevel            = 0;  
     $newkey               = 0;
     $grouparr[$templevel] = "";
  //-----------------------------------------------//
/* $role=['BD','SPOC','REC','HR'];*/

    $response['array_test']  = array(
       '1'=>'Short Leave',
       '2'=>'Halfday Leave',
       '3'=>'Full Day Leave',
      );
    foreach($response['array_test'] as $key => $value){
     $fuelID[] = $key;
     $fuelName[] = $value;
    //p($fuelName); exit;
   }
      $fuelProID = implode(',',$fuelID);
       $fuelProName = implode(',',$fuelName);

       $featurevaluee1[]    = 'test';
 
      $excelHeadArrqq1 = array_merge($featurevaluee1);
      $excelHeadArr1 = array_unique($excelHeadArrqq1);
      
      $oldfinalAttrData['test']       = $fuelProName;
      $oldfinalAttrData2['test']      = $fuelName;

       $finalAttrData  = array_merge($oldfinalAttrData);
       $finalAttrData2 = $oldfinalAttrData2;
       $attCount = array();
       foreach($finalAttrData as $k=>$v){
       $valueCount   = count(explode(",", $v));
       $attCount[$k] = $valueCount+1;

      } 
             $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
    $objWorkSheet  = $objPHPExcel->createSheet();
    $objPHPExcel->setActiveSheetIndex(0);

    $finalExcelArr1 = array_merge($excelHeadArr1);
//p($finalAttrData2);exit;
     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
        $j      = 2;
       
        for($i=0;$i<count($finalExcelArr1);$i++){
         $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
         foreach ($finalAttrData2 as $key => $value) {
          foreach ($value as $k => $v) {

            if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
            }
        }
      }  
    }
           $arrPart2 = array('Leave Type','Leave Reason','Emp Id','Emp Name');
          if(!empty($excelHeadArr)){
             $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
         }else{
            $finalExcelArr = array_merge($arrPart2);
         }
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');
      
    $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
 
 //Set border style for active worksheet
for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

          for($k=2;$k <1000;$k++)
          {
 $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
    $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
 
    $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
    $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
    $objValidation24->setAllowBlank(false);
    $objValidation24->setShowInputMessage(true);
    $objValidation24->setShowErrorMessage(true);
    $objValidation24->setShowDropDown(true);
    $objValidation24->setErrorTitle('Input error');
    $objValidation24->setError('Value is not in list.');
    $objValidation24->setPromptTitle('Pick from list');
    $objValidation24->setPrompt('Please pick a value from the drop-down list.');
    $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
    $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
  
  }//secfor


}

        $filename  = "AttendanceEmployeeExcel.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
die("a");
        exit;
          
}


  public function attendanceexcel()
    {
        $users = DB::table('attendanceuser')->where('created_at','=',date('Y-m-d'))->get();
     //   dd($users);
        return view('admin/attendanceexcel',compact('users'));
    }

    public function excelattendanceup(Request $request)
    {
        if(!isset($request['upattfile'])){
            Session::flash('error_msg', 'Please select excel file to upload.');
            return redirect()->route('attendanceexcel');
        }

 if(isset($request['upattfile']))
        {
           $extension = File::extension($request['upattfile']->getClientOriginalName());
           if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
              //'Your file is a valid xls or csv file'
           }else {
              Session::flash('error_msg', 'Please select excel file (xlsx,xls,csv) to upload.');
              return redirect()->route('attendanceexcel');
           }
        }

 $inputFileName = $request['upattfile'];
        $helper = new Sample();
      //  dd($inputFileName);
       // $helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
        $spreadsheet = IOFactory::load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $excel_row = array_shift($sheetData);
        $m=0;
        //dd($sheetData);
        foreach ($sheetData as $key=>$value)
        {
            
                $id1=DB::table('attendanceuser')->insert(
                [
                'empcode' => $value['A'] ? $value['A'] : '' ,
                'empname' => $value['B'] ? $value['B'] : "" ,
                'lastpunch' => $value['C'] ? $value['C'] : "" ,
                'punchrecord' => $value['D'] ? $value['D'] : "",   
                'created_at' => date("Y-m-d") ,
                'updated_at' =>  date("Y-m-d") ,
 
                 ]
            );
            
               $id2 = DB::getPdo()->lastInsertId();
               
          
        }

  Session::flash('success_msg', 'Excel Uploaded  Successfully!');
        return redirect()->route('attendanceexcel');

        
      }
  public function fworkemployee()
    {

  if(isset($_GET['doj'])!='')
  {
$dobj=$_GET['doj'];
  }
  else
  {
$dobj=date('Y-m-d');
  }
$users=  DB::table('userslogin as c')
            ->join('attendanceuser as d','c.usersloginempid','=','d.empcode')
            ->join('tbl_clientrec_start_endtime as e','e.clientstart_recuiterid','=','c.usersloginid')
            ->select(DB::raw('*'))
            ->where('c.createdon',$dobj)
           ->where('d.created_at',$dobj)
           // ->where('e.created_at', 'like', '28%')
           
           ->where('e.created_at', 'LIKE', '%' .$dobj. '%')
           //  ->where('e.created_at',date('Y-m-d').'00:00:00')
            ->orderby('c.usersloginempid','desc')
            ->get();
      //   $users = DB::table('userslogin')->where('createdon','=',date('Y-m-d'))->get();
       
        return view('admin/fworkemploye',compact('users'));
    }
  public function excelattendancedownload()
    {


  if(isset($_GET['doj'])!='')
  {
$dobj=$_GET['doj'];
  }
  else
  {
$dobj=date('Y-m-d');
  }
$users=  DB::table('userslogin as c')
            ->join('attendanceuser as d','c.usersloginempid','=','d.empcode')
            ->join('tbl_clientrec_start_endtime as e','e.clientstart_recuiterid','=','c.usersloginid')
            ->select(DB::raw('*'))
            ->where('c.createdon',$dobj)
           ->where('d.created_at',$dobj)
           // ->where('e.created_at', 'like', '28%')
           
           ->where('e.created_at', 'LIKE', '%' .$dobj. '%')
           //  ->where('e.created_at',date('Y-m-d').'00:00:00')
            ->orderby('c.usersloginempid','desc')
            ->get();
      //   $users = DB::table('userslogin')->where('createdon','=',date('Y-m-d'))->get();
        //-----------Va Define-----------------------//
     $allArr               = array();
     $templevel            = 0;  
     $newkey               = 0;
     $grouparr[$templevel] = "";
  //-----------------------------------------------//
/* $role=['BD','SPOC','REC','HR'];*/

    $response['array_test']  = array(
       '1'=>'BD',
       '2'=>'SPOC',
       '3'=>'Recruiter',
      );
    foreach($response['array_test'] as $key => $value){
     $fuelID[] = $key;
     $fuelName[] = $value;
    //p($fuelName); exit;
   }
      $fuelProID = implode(',',$fuelID);
       $fuelProName = implode(',',$fuelName);

       $featurevaluee1[]    = 'test';
 
      $excelHeadArrqq1 = array_merge($featurevaluee1);
      $excelHeadArr1 = array_unique($excelHeadArrqq1);
      
      $oldfinalAttrData['test']       = $fuelProName;
      $oldfinalAttrData2['test']      = $fuelName;

       $finalAttrData  = array_merge($oldfinalAttrData);
       $finalAttrData2 = $oldfinalAttrData2;
       $attCount = array();
       foreach($finalAttrData as $k=>$v){
       $valueCount   = count(explode(",", $v));
       $attCount[$k] = $valueCount+1;

      } 
             $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
    $objWorkSheet  = $objPHPExcel->createSheet();
    $objPHPExcel->setActiveSheetIndex(0);

    $finalExcelArr1 = array_merge($excelHeadArr1);

     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
        $j      = 2;
       
        for($i=0;$i<count($finalExcelArr1);$i++){
         $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
         foreach ($finalAttrData2 as $key => $value) {
          foreach ($value as $k => $v) {

            if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
            }
        }
      }  
    }
           $arrPart2 = array('Emp Code','Name','Punching time','Login time','Work Start Time');
          if(!empty($excelHeadArr)){
             $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
         }else{
            $finalExcelArr = array_merge($arrPart2);
         }
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');
      
    $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
   $j=2;
 //Set border style for active worksheet
for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

  foreach ($users as $key => $value) {
        //Set height for all rows.
        $vpunchrec=explode(',', $value->punchrecord);
            $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
             $newvar = $j+$key;
    $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
            
           $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->empcode);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->empname);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $vpunchrec[0]);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->userslogintime);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->clientstart_starttime);

      
  }
          for($k=2;$k <1000;$k++)
          {
 $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
    $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
 
    $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
    $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
    $objValidation24->setAllowBlank(false);
    $objValidation24->setShowInputMessage(true);
    $objValidation24->setShowErrorMessage(true);
    $objValidation24->setShowDropDown(true);
    $objValidation24->setErrorTitle('Input error');
    $objValidation24->setError('Value is not in list.');
    $objValidation24->setPromptTitle('Pick from list');
    $objValidation24->setPrompt('Please pick a value from the drop-down list.');
    $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
    $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
  
  }//secfor


}

        $filename  = "EmployeeExcel.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
die("a");
        exit;
      echo "fffffffffffffffff";
    }
    
    
    

        public function excelclientdownload()
    {
if($_GET['user']=='0')
    {
     $paged=$_GET['user'];

       $users=  DB::table('tbl_clients as c')
       ->join('users as d','c.clientsuid','=','d.id')
         ->select('*')
            ->where('c.client_status','!=',3)
              ->where('c.clientsuid','=', $paged)
            ->orderby('c.client_id','desc')
           ->get();

 }
 else
 {
     $users=  DB::table('tbl_clients as c')
       ->join('users as d','c.clientsuid','=','d.id')
         ->select('*')
            ->where('c.client_status','!=',3)
            ->orderby('c.client_id','desc')
           ->get();
 }
      



        $users=  DB::table('tbl_clients as c')
       ->join('users as d','c.clientsuid','=','d.id')
         ->select('*')
            ->where('c.client_status','!=',3)
            ->orderby('c.client_id','desc')
           ->get();

      //   $users = DB::table('userslogin')->where('createdon','=',date('Y-m-d'))->get();
        //-----------Va Define-----------------------//
      $allArr               = array();
      $templevel            = 0;  
      $newkey               = 0;
      $grouparr[$templevel] = "";
  //-----------------------------------------------//
      /* $role=['BD','SPOC','REC','HR'];*/

      $response['array_test']  = array(
       '1'=>'BD',
       '2'=>'SPOC',
       '3'=>'Recruiter',
       );
      foreach($response['array_test'] as $key => $value){
       $fuelID[] = $key;
       $fuelName[] = $value;
    //p($fuelName); exit;
     }
     $fuelProID = implode(',',$fuelID);
     $fuelProName = implode(',',$fuelName);

     $featurevaluee1[]    = 'test';
     
     $excelHeadArrqq1 = array_merge($featurevaluee1);
     $excelHeadArr1 = array_unique($excelHeadArrqq1);
     
     $oldfinalAttrData['test']       = $fuelProName;
     $oldfinalAttrData2['test']      = $fuelName;

     $finalAttrData  = array_merge($oldfinalAttrData);
     $finalAttrData2 = $oldfinalAttrData2;
     $attCount = array();
     foreach($finalAttrData as $k=>$v){
       $valueCount   = count(explode(",", $v));
       $attCount[$k] = $valueCount+1;

     } 
     $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
     $objWorkSheet  = $objPHPExcel->createSheet();
     $objPHPExcel->setActiveSheetIndex(0);

     $finalExcelArr1 = array_merge($excelHeadArr1);

     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
     $j      = 2;
     
     for($i=0;$i<count($finalExcelArr1);$i++){
       $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
       foreach ($finalAttrData2 as $key => $value) {
        foreach ($value as $k => $v) {

          if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
          }
        }
      }  
    }
    $arrPart2 = array('Category','Company Name','Contact Person Name','Designation','Phone','Email','Created by');
    if(!empty($excelHeadArr)){
     $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
   }else{
    $finalExcelArr = array_merge($arrPart2);
  }
  $objPHPExcel->setActiveSheetIndex(1);
  $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');
  
  $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
  $j=2;
 //Set border style for active worksheet
  for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
    $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);

    foreach ($users as $key => $value) {
        //Set height for all rows.
     // $vpunchrec=explode(',', $value->punchrecord);
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      $newvar = $j+$key;
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      
      $objPHPExcel->getActiveSheet()->setCellValue($cols[0].$newvar, $value->comp_name);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->contact_name);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->designation);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->phone);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->email);
   $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->name);
      
    }
    for($k=2;$k <1000;$k++)
    {
     $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
     $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
     
     $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
     $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
     $objValidation24->setAllowBlank(false);
     $objValidation24->setShowInputMessage(true);
     $objValidation24->setShowErrorMessage(true);
     $objValidation24->setShowDropDown(true);
     $objValidation24->setErrorTitle('Input error');
     $objValidation24->setError('Value is not in list.');
     $objValidation24->setPromptTitle('Pick from list');
     $objValidation24->setPrompt('Please pick a value from the drop-down list.');
     $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
     $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
     
  }//secfor


}

$filename  = "EmployeeExcel.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
        die("a");
        exit;
        echo "fffffffffffffffff";
      }
    
    
 public function myallocatedexceldownload()
 {
//print_r($_POST); exit;
        
     $assigneeid=Auth::user()->id;
       $users=DB::table('tbl_recruiter_cv as trc')
                ->select('trc.cv_name','trc.id', 'trc.candidate_name', 'trc.candidate_email', 'trc.candidate_mob', 'trc.cv_status','tc.tca_followupstatus','tc.tca_id','trc.position_id','trc.recruiter_id')
                  ->join('tbl_cv_allocated as tc', 'trc.id', '=', 'tc.tca_cvid')
                ->where('trc.recruiter_id',$_POST['recruiterId'])
               //  ->where('tc.tca_assignto',$recruiterId)
                ->where('trc.position_id', '=', $_POST['clientid'])
                //  ->where('trc.cv_status', '=', '22')
                  ->where('tc.tca_followupstatus', '!=', 'Not Going')
                ->get();
            
            
            
          //  dd($users);


    //-----------Va Define-----------------------//
     $allArr               = array();
     $templevel            = 0;  
     $newkey               = 0;
     $grouparr[$templevel] = "";
  //-----------------------------------------------//
/* $role=['BD','SPOC','REC','HR'];*/

    $response['array_test']  = array(
       '7'=>'On The Way',
       '8'=>'Interviewed',
        '9'=>'Not Going',
         '17'=>'Not Responding',
          '18'=>'Confirmed',
           '23'=>'Dicey',
       '24'=>'Reached',
      
      );
    foreach($response['array_test'] as $key => $value){
     $fuelID[] = $key;
     $fuelName[] = $value;
    //p($fuelName); exit;
   }
      $fuelProID = implode(',',$fuelID);
       $fuelProName = implode(',',$fuelName);

       $featurevaluee1[]    = 'test';
 
      $excelHeadArrqq1 = array_merge($featurevaluee1);
      $excelHeadArr1 = array_unique($excelHeadArrqq1);
      
      $oldfinalAttrData['test']       = $fuelProName;
      $oldfinalAttrData2['test']      = $fuelName;

       $finalAttrData  = array_merge($oldfinalAttrData);
       $finalAttrData2 = $oldfinalAttrData2;
       $attCount = array();
       foreach($finalAttrData as $k=>$v){
       $valueCount   = count(explode(",", $v));
       $attCount[$k] = $valueCount+1;

      } 
             $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
    $objWorkSheet  = $objPHPExcel->createSheet();
    $objPHPExcel->setActiveSheetIndex(0);

    $finalExcelArr1 = array_merge($excelHeadArr1);
//p($finalAttrData2);exit;
     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
        $j      = 2;
       
        for($i=0;$i<count($finalExcelArr1);$i++){
         $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
         foreach ($finalAttrData2 as $key => $value) {
          foreach ($value as $k => $v) {

            if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
            }
        }
      }  
    }
           $arrPart2 = array('Action','Id','CandidateName','Email','Mobile','FollowUp Status','Recuiterid');
          if(!empty($excelHeadArr)){
             $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
         }else{
            $finalExcelArr = array_merge($arrPart2);
         }
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');
      
    $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
 
 //Set border style for active worksheet
for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);
   foreach ($users as $key => $value) {
             $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      $newvar = $j+$key;
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);

      $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->id);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->candidate_name);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->candidate_email);
        $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->candidate_mob);
        $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->tca_followupstatus);
          $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $assigneeid);

          for($k=2;$k <1000;$k++)
          {
 $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
    $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
 
    $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
    $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
    $objValidation24->setAllowBlank(false);
    $objValidation24->setShowInputMessage(true);
    $objValidation24->setShowErrorMessage(true);
    $objValidation24->setShowDropDown(true);
    $objValidation24->setErrorTitle('Input error');
    $objValidation24->setError('Value is not in list.');
    $objValidation24->setPromptTitle('Pick from list');
    $objValidation24->setPrompt('Please pick a value from the drop-down list.');
    $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
    $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
  
  }//secfor


}



}

$filename  = "viewmyallocatedcvdetail.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
        die("a");
        exit;
        echo "fffffffffffffffff";
}


   public function myallocatedexcelup(Request $request)
    {
       
      //  print_r($_POST); exit;
        if(!isset($request['upfile'])){
            Session::flash('error_msg', 'Please select excel file to upload.');
            return redirect()->route('empexcel');
        }
        
        if(isset($request['upfile']))
        {
           $extension = File::extension($request['upfile']->getClientOriginalName());
           if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
              //'Your file is a valid xls or csv file'
           }else {
              Session::flash('error_msg', 'Please select excel file (xlsx,xls,csv) to upload.');
              return redirect('viewmyallocatedcvdetail/'.$_POST['nidd']);
           }
        }


        $inputFileName = $request['upfile'];
        $helper = new Sample();
       // $helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
        $spreadsheet = IOFactory::load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $excel_row = array_shift($sheetData);
        $m=0;
     //   dd($sheetData);
        foreach ($sheetData as $key=>$value)
        {
         
if($value['A']=='On The Way') {$statusval=7;}elseif($value['A']=='Interviewed') {$statusval=8;}
elseif($value['A']=='Not Going') {$statusval=9;}elseif($value['A']=='Not Responding') {$statusval=17;}
elseif($value['A']=='Confirmed') {$statusval=18;}elseif($value['A']=='Dicey') {$statusval=23;}
elseif($value['A']=='Reached') {$statusval=24;}

            
            
       // echo $value['A'];
        DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$value['B']])
                        ->update(['c.cv_status'=>$statusval]);
                        
                           DB::table('tbl_recruiter_cvchangestatus as c')
                        ->where(['c.cvid'=>$value['B']])
                        ->update(['c.cvupdatestatus'=>0]);
$id1=DB::table('tbl_recruiter_cvchangestatus')->insert(
                [
                'cv_status' =>$statusval ? $statusval : '' ,
                'cvid' => $value['B'] ? $value['B'] : '' ,
                'created_at' => date('Y-m-d H:i:s'),
                'loginid' => $value['G'] ? $value['G'] : "",   
                 'recuiterid' => $value['G'] ? $value['G'] : "",   
                   'cvupdatestatus' => '1',  
              
                 ]
            );
            
           
             /*$roleId = DB::table('tbl_leavetype as tr')
                         ->select('tr.role_id')
                        ->where('role_name',$value['A'])
                         ->first();
            //   dd($roleId);      */
               
             /*   $id1=DB::table('hrattendance')->insert(
                [
                'leavestatus' =>$value['A'] ? $value['A'] : '' ,
                'leaverecord' => $value['B'] ? $value['B'] : '' ,
                'empcode' => $value['C'] ? $value['C'] : "" ,
                'empname' => $value['D'] ? $value['D'] : "",   
                 'created_at' => date("Y-m-d"),   
              
                 ]
            );
            
               $id2 = DB::getPdo()->lastInsertId();*/
               
     /*  DB::table('tbl_cv_allocated as c')
                        ->where(['c.tca_cvid'=>$value['B']])
                        ->update(['c.tca_followupstatus'=>$value['A']]);  */      
             

     $m++;  
        }
        Session::flash('success_msg', 'Excel Uploaded  Successfully!');
         return redirect('viewmyallocatedcvdetail/'.$_POST['nidd']);
    }
    
        
      
}
