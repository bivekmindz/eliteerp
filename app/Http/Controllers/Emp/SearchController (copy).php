<?php
namespace App\Http\Controllers\Emp;
use App\Events\RecStartUploadEvent;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Model\Adminmodel;
use App\Model\Adminmodel\Employees;
use Illuminate\Support\Facades\Input;
use App\Model\User;
use App\Model\Admin;
use Carbon\Carbon;
use Session;
use Storage;
use DB;
use Mail;


class SearchController extends Controller
{
    public function advancesearch()
    {
    
        
        $all_keywords=$_GET['all_keywords'];
        $exc_keywords=$_GET['exc_keywords'];
        $company=@$_GET['company'];
        $exc_company=@$_GET['exc_company'];
        $designation=@$_GET['designation'];
        $current_loc=@$_GET['current_loc'];
        $exp_min=$_GET['exp_min'];
        $exp_max=$_GET['exp_max'];
        if($_GET['sal_min']!='')
        {
        $sal_min=$_GET['sal_min'].'.'.$_GET['sal_min1'];
        }
        else
        {
           $sal_min=0;
        
        }
        
         if($_GET['sal_max']!='')
        {
          $sal_max=$_GET['sal_max'].'.'.$_GET['sal_max1'];
        }
        else
        {
           $sal_max=0;
        
        }
      

      /*  if(!empty($all_keywords))
        { */

            $url='http://115.124.98.243/~logistiks/solrApi/advancesearch.php';
            $data = str_replace(' ', '+', $all_keywords);

            $exckeywords = str_replace(' ', '+', $exc_keywords);
            $company = str_replace(' ', '+', $company);
            $exc_company = str_replace(' ', '+', $exc_company);
            $designation = str_replace(' ', '+', $designation);
            $current_loc = str_replace(' ', '+', $current_loc);
            $exp_min = str_replace(' ', '+', $exp_min);
             $exp_max = str_replace(' ', '+', $exp_max);
            $sal_min = str_replace(' ', '+', $sal_min);
            $sal_max = str_replace(' ', '+', $sal_max);

            $post = [
              'search' =>$data, 
              'merchantname' => 'techproducts',
                'all' => $data,
                'minsalary' =>$sal_min,
                'maxsalary' => $sal_max,
                'location' => $current_loc,
                'designation' => $designation,
                'company' => $company,
                'minexperience' => $exp_min,
                 'maxexperience' => $exp_max,
                  'salary' => '',
                   'experience' => '',
            ];
//print_r($post); exit;

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            
            $server_output = curl_exec ($ch);
       //    echo "<pre>";
        //  print_r(($server_output));exit;
$response = curl_exec($ch);
            curl_close ($ch);

         
            $apidata=json_decode($response);
            #$res=$apidata->response;
        // print_r($response); exit;
            // print_r($response[0]->error_status);
            if($apidata->response[0]->error_status=='Success')
            {  
                // dd($apidata->response[0]->error_status);
            $id_array = array();
            foreach ($apidata->result as $key => $value) {
                // print_r($value);
                array_push($id_array, $value->id);
                # code...
            }
            //print_r($id_array);
            $ids= implode(',', $id_array);
            //echo $ids;

            //die();
             //$getpostion='200';
            $getpostion = DB::table('tbl_recruiter_cv as st')
                            ->whereRaw("FIND_IN_SET(st.id, '$ids')")
                            ->select('st.*')
                            ->get();

                               // dd($getpostion);
            //echo 2222;
            }
            else
            {
                $getpostion='';
            }
                         
     /*   }
        else
        {
            $getpostion='';
        }*/
        // dd($getpostion);
       

        return view('Emp.advancesearch', [
            'getpostion'=>$getpostion
        ]);
        #return view('Emp.advancesearch');
   #return view('Emp.advancesearch');
    }

public function advancemail()
{
    foreach ($_POST['mailid'] as $key => $value) 
    {
        #echo $value;
        $getpostion = DB::table('tbl_recruiter_cv as st')
        ->whereRaw("st.id='$value'")
        ->select('st.*')
        ->get();
        #print_r($getpostion[0]);  
        $emailid=trim($getpostion[0]->candidate_email); 
        $ename=trim($getpostion[0]->candidate_name); 
        #$content='Hi '.$getpostion[0]->candidate_name.', This is the content of mail body ';
        $data=(array)$getpostion[0];
        #print_r($data);
        $subject='very very urgent requirement of '.$getpostion[0]->primary_skill.' professionals with '.$getpostion[0]->current_org;
        #Mail::raw($content, function ($message) use ($emailid,$ename) {
        Mail::send('Emp.mails.advancesearchmailer',  $data, function ($message) use ($emailid, $ename,$subject) {
        $message->from('elitehr@gmail.com', 'EliteHR Team');
        $message->to($emailid,$ename);
        $message->subject($subject);
        });
    }
    echo 1;
}

    


} #class