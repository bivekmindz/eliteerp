<?php

namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Model\Adminmodel\Position;
use App\Model\Adminmodel\Department;
use App\Model\Client as Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Session;
use Mail;
use Log;
use Storage;

class PositionController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
        //$assigneeid=Auth::user()->id;
    }

    public function position()
    {
        $assigneeid=Auth::user()->id;
        $departments=Department::all();

         if(Auth::user()->id==1)
        {
            $client=DB::table('tbl_clients  as c')
                ->select('c.client_id as fk_clientid','c.comp_name')
                ->where('c.client_status','!=',3)
                ->get();
        }
        else {
            $client = DB::table('tbl_clientspockmap as c')
                ->join('tbl_clients as t', 'c.fk_clientid', '=', 't.client_id')
                ->select('c.fk_clientid', 't.comp_name')
                ->where(['c.fk_empid' => $assigneeid])
                ->distinct('c.fk_clientid')
                ->get();
        }
        
//       $client=Client::all();
      //  dd($client);

        $emp=DB::table('users')
            ->select('name','id')
            ->whereRaw('FIND_IN_SET(3,emp_role)','emp_role')
            ->get();

        $target=DB::table('tbl_clientjdrecruitermap as c')
            ->join('users as u','u.id','=','c.fk_empid')
            ->select('u.name')
            ->where(['c.fk_assigneeid'=>$assigneeid])
            ->distinct('c.fk_assigneeid')
            ->get();
       //dd($target);
       // dd($emp);
        return view('Emp.position',compact('departments','client','emp','target'));
    }
    
    public function alluserswithbreak()
    {
        $data['users'] = DB::table('tbl_clientrec_break_backtime as tcbr')
                                ->join('users as u','tcbr.clientstart_recuiterid','=','u.id')
                                ->select('tcbr.*','u.name')
                                ->whereDate('tcbr.created_at', '=', Carbon::today())
                                ->get();
        
        foreach($data['users'] as $key => $val){
            $differenceTime = ($val->clientstart_backtime - $val->clientstart_breaktime);
            $diffTimeMinutes = ($differenceTime/60);

            $data['users'][$key]->clientstart_breaktime = date('Y-m-d H:i:s',$val->clientstart_breaktime);
            $data['users'][$key]->clientstart_backtime = date('Y-m-d H:i:s',$val->clientstart_backtime);
            $data['users'][$key]->breakdelay = $diffTimeMinutes.' minutes';
        }
        
        $to = 'ankit@mindztechnology.com';
        Mail::send('Emp.mails.userswithbreak', $data, function($message) use ($data,$to)
        {
            $message->to('ankit@mindztechnology.com');
            $message->from('bivek@mindztechnology.com');
            $message->subject('Break Time Report');
        });
        
         // check for failures
        if (count(Mail::failures()) > 0) {
            echo "There was one or more failures. They were: <br />";
        }else{
            echo "No errors, all sent successfully!";
        } 
      
                               
    }
    
    public function addposition(Request $request)
    {

        $clientJd = $request->input('upload_position_jd');
    /*    $dom = new \DomDocument();
        $dom->loadHtml($clientJd, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
        $clientJd = $dom->saveHTML();*/
        //echo "<pre>";
        //print_r($request->all()); 


        // $this->validate($request,[
        //     'dept' => 'required',
        //     'compname'=>'required',
        //     'jobtitle' => 'required',
        //     'upload_position_jd' => 'required',
        //     'noofpos' => 'required|numeric',
            
        // ], 
        // [
        //     'dept.required' => 'Department field is required.',
        //     'compname.required'=>'company name field is required list',
        //     'jobtitle.required' => 'Jobtitle field is required.',
        //     'upload_position_jd' => 'Please Upload Job Decription',
        //     'noofpos.required' => 'Number of Positions field is required'
        // ]);
        $assigneeid=Auth::user()->id;


    $user=  DB::table('tbl_clientjd_master')->insertGetId([
            'clientjob_deptid'=>$request['dept'],
            'clientjob_compid'=>$request['compname'],
            'clientjob_title'  => str_replace("'"," ",$request['jobtitle']),
            'upload_position_jd' => strip_tags($clientJd),
            'clientjob_noofposition'=>$request['noofpos'],
            'drive'=>1,
            'clientjob_empid'=>$assigneeid,
            'clientjob_otherinfo'=>$request['otherinfo'],
            'experience'=>$request['experience'],
            'domain_experience'=>$request['domain_experience'],
            'primary_skills'=>$request['primary_skill'],
            'secondary_skills'=>$request['secondary_skill'],
            'rpn'=>$request['rpn'],
            'rpd'=>$request['rpd'],
            'rpp'=>$request['rpp'],
            'rpe'=>$request['rpe'],
            'pocn'=>$request['pocn'],
            'pocd'=>$request['pocd'],
            'pocp'=>$request['pocp'],
            'poce'=>$request['poce'],
            'hmn'=>$request['hmn'],
            'hmd'=>$request['hmd'],
            'hme'=>$request['hme'],
            'hmp'=>$request['hmp'],
            'drive_shortlist'=>$request['report'],
            'drive_time'=>$request['drivedate'],
            'lineup_from'=>$request['DaliyLineUpFrom'],
            'lineup_to'=>$request['DaliyLineUpTo'],
            'severity'=>$request['severity'],
              'shortlist_time'=>$request['Shortlistdate'],
            'clientjob_jobdescription'=>str_replace( "'", ' ', $request['upload_position_jd']),
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()
     ]);


DB::table('tbl_clientjdrecruitermap')->insert([
                    'fk_jdid'=> $user,
                    'fk_empid'=> 19,
                    'fk_assigneeid'  => 19,
                    'clientreq_dailytarget' => '',
                    'clientreq_starttime'=> '',
                    'clientreq_stoptime'=> '',
                    'clientreq_status'=> 1,
                    'clientreq_createdon'=> Carbon::now(),
                    'clientreq_updatedon'=> Carbon::now()
                ]);


        $comp=DB::table('tbl_clients as c')
            ->select('c.comp_name')
            ->where('c.client_id',$request['compname'])
            ->first();
        //dd($comp);

        $dept=DB::table('tbl_department as d')
            ->select('d.dept_name')
            ->where('d.dept_id',$request['dept'])->first();


        $from_email = DB::table('users  as u')
            ->select('u.email')
            ->where(['u.id' => $assigneeid])
            ->get();
      
        $from = $from_email[0]->email;
        //$to=DB::table('admins')->select('email','name')->first();

       // $to_email=DB::table('users as u')->select('email','name')->where('emp_role',4)->get();
   //   $to=$to_email[0];
        

      //  $to = $positiondetails['to']->email;
      // echo $to;
     //   $toname = $positiondetails['to']->name;
       // dd($toname);
     //   $from=$positiondetails['from'];
        
        // $positiondetails=[ 'company_name'=> $comp,
        //                   'department'=>$dept,
        //                   'details'=>$request,
        //                   'from'=>$from,
        //                   'to'=>$to
        //           ];

      
    //   dd($positiondetails);

      //  return view('Emp.mails.newpositionmail',['mail_data'=>$positiondetails]);

      // $sendmail= $this->sendMail($positiondetails);

        Session::flash('success_msg', 'Position Added Successfully!');

        return redirect()->route('positionlist');

    }

     public function allposition()
    {
        $pos=DB::table('tbl_clientjd_master as m')
            ->join('tbl_clients as c','m.clientjob_compid','=','c.client_id')
            ->select('m.*','c.comp_name')
           // ->where('m.clientjob_status',1)
            ->get();
            
                  $list =  DB::table('users')->where('emp_role', 3)->orwhere('emp_role', 'like', '%3%')->get();
                  $sqls =   DB::table('users')->where('emp_role','=',4)->orwhere('emp_role','=',7)->get();
                  $mytime = Carbon::now();
        $currentTime = $mytime->toTimeString();
        $currentDate = Date('Y-m-d',strtotime($mytime));  
        return view('Emp.listposition',compact('pos','currentDate','list'));          
     //   return view('Emp.listposition',['pos'=>$pos],['lists'=>$recruiterlist],['list'=>$sqls]);
    }


    public function positionlist()
    {
        $assigneeid=Auth::user()->id;
        $pos=DB::table('tbl_clientjd_master as m')
          ->join('tbl_clients as c','m.clientjob_compid','=','c.client_id')
        ->select('m.*','c.comp_name')
        ->where(['clientjob_empid'=>$assigneeid])
        //->where(['clientjob_status'=>1])
          ->orderBy('clientjob_id', 'desc')
            ->get();
      //  dd($pos);
      $list =  DB::table('users')->where('emp_role', 3)->orwhere('emp_role', 'like', '%3%')->get();
       $mytime = Carbon::now();
        $currentTime = $mytime->toTimeString();
        $currentDate = Date('Y-m-d',strtotime($mytime));
     // dd($recruiterlist);die;
       // return view('Emp.listposition',['pos'=>$pos],['list'=>$recruiterlist]);
return view('Emp.listposition',compact('pos','currentDate','list'));
    }
    
    public function editposition($clientjob_id)
    {
       $assigneeid=Auth::user()->id;
//
        $clientid = ($clientjob_id);
        $client_details = DB::table('tbl_clientjd_master as m')
            ->join('tbl_clients as c','m.clientjob_compid','=','c.client_id')
            ->where('clientjob_id','=',$clientid)
            ->get();

             // dd($client_details[0]);

        $client = DB::table('tbl_clientspockmap as c')
        ->join('tbl_clients as t', 'c.fk_clientid', '=', 't.client_id')
        ->select('c.fk_clientid', 't.comp_name')
        ->where(['c.fk_empid' => $assigneeid])
        ->distinct('c.fk_clientid')
        ->get();
      //  $complist = DB::table('tbl_clientjd_master as m')
     //   ->join 
// $complist = DB::table('tbl_clients as m')
//   ->join('tbl_clientspockmap as c','m.client_id','=','c.fk_clientid')
// ->where('c.assignee_emp_id','=',$clientid)

  
// ->get();
// dd($complist);
        return view('Emp.editposition',compact('client_details','client'));
    }

 public function updateposition($clientjob_id,Request $request)
    {

       $assigneeid=Auth::user()->id;
      $clientjob_id = $clientjob_id;

        DB::table('tbl_clientjd_master')
        ->where('clientjob_id',$clientjob_id)
        ->update([
            'clientjob_deptid'=>$request['dept'],
            'clientjob_compid'=>$request['compname'],
            'clientjob_title'  => str_replace("'"," ",$request['jobtitle']),
            'upload_position_jd' => strip_tags('clientJd'),
            'clientjob_noofposition'=>$request['noofpos'],
            'drive'=>1,
            'clientjob_empid'=>$assigneeid,
            'clientjob_otherinfo'=>$request['otherinfo'],
            'experience'=>$request['experience'],
            'domain_experience'=>$request['domain_experience'],
            'primary_skills'=>$request['primary_skill'],
            'secondary_skills'=>$request['secondary_skill'],
            'rpn'=>$request['rpn'],
            'rpd'=>$request['rpd'],
            'rpp'=>$request['rpp'],
            'rpe'=>$request['rpe'],
            'pocn'=>$request['pocn'],
            'pocd'=>$request['pocd'],
            'pocp'=>$request['pocp'],
            'poce'=>$request['poce'],
            'hmn'=>$request['hmn'],
            'hmd'=>$request['hmd'],
            'hme'=>$request['hme'],
            'hmp'=>$request['hmp'],
              'shortlist_time'=>$request['Shortlistdate'],
            'drive_shortlist'=>$request['report'],
            'drive_time'=>$request['drivedate'],
            'lineup_from'=>$request['DaliyLineUpFrom'],
            'lineup_to'=>$request['DaliyLineUpTo'],
            'severity'=>$request['severity'],
            'clientjob_jobdescription'=> str_replace( "'", ' ', $request['upload_position_jd']),
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()]);
        Session::flash('success_msg', 'Position Updated Successfully!');
        return redirect()->route('positionlist');
      }



    //  public function assignclient()
    // {   str_replace( "'", ' ', $request['upload_position_jd'])
    //     $assigneeid=Auth::user()->id;
    //     //dd($assigneeid); 
    //     $pos=DB::table('tbl_clientspockmap as m')
    //       ->join('users as u','m.assignee_emp_id','=','u.id')
    //       ->join('tbl_clients as c','c.client_id','=','m.fk_clientid')
    //       ->leftjoin('users as us','us.id','=','m.fk_empid')
    //       ->select('c.*','m.fk_empid','us.name')
    //       ->where(['fk_empid'=>$assigneeid])
    //       //->groupBy('m.fk_clientid')
    //       ->get();
    //       //dd($pos);
    //       return view('Emp.viewassignclient',['assignlist'=>$pos]);

    // }
    
     public function assignclient()
    {
        $assigneeid=Auth::user()->id;
        //dd($assigneeid); 
        $pos=DB::table('tbl_clientspockmap as m')
          ->join('users as u','m.assignee_emp_id','=','u.id')
          ->join('tbl_clients as c','c.client_id','=','m.fk_clientid')
          ->leftjoin('users as us','us.id','=','m.fk_empid')
          ->select('c.*','m.fk_empid','us.name','u.name')
          ->where(['fk_empid'=>$assigneeid])
          //->groupBy('m.fk_clientid')
          ->get();
          // dd($pos);
          return view('Emp.viewassignclient',['assignlist'=>$pos]);

    }

    public function positionjd($id)
    {
        $assigneeid = Auth::user()->id;
        $recruiters = DB::table('users as u')
                    ->select(DB::raw('(select count(clientreq_id) from tbl_clientjdrecruitermap where fk_empid=u.id and clientrequpdate_status=1) as contrec '),'u.*')
                    ->where('emp_role','!=','4')
                     ->where('emp_status','=','1')
                    ->get();
                            
        $getRecruiters_id = DB::table('tbl_clientjdrecruitermap')
                                ->select('fk_empid')
                                ->where(['fk_jdid' => $id])
                                  ->where(['clientrequpdate_status' =>1])
                                ->where(['fk_assigneeid' => $assigneeid])
                                ->get();
           
        foreach($getRecruiters_id as $key => $val){
            $recruiters_ids[] = $val->fk_empid;
        }
      
        $html = '';          
        $selected = '';

        foreach($recruiters as $key => $val){
            if(!empty($recruiters_ids)){
                if(in_array($val->id,$recruiters_ids)){
                    $selected = 'selected';
                }else{
                    $selected = '';
                }
            }
            $html .= '<option value="'.$val->id.'" '.$selected.'>'.$val->name.'('.$val->contrec.')</option>';
        }
       
        echo $html;
        
        //   $position = DB::table('tbl_clientjd_master')
        //     ->select('upload_position_jd')
        //     ->where(['clientjob_id' => $id])
        //     ->first();


        // echo $position->upload_position_jd;
        // $position = DB::table('tbl_clientjd_master')
        //     ->select('upload_position_jd')
        //     ->where(['clientjob_id' => $request->id])
        //     ->first();
        // $pos = str_replace("\n", "", $position->upload_position_jd);
        // $finalPos = stripslashes($pos);
        //@php
      //  $tag = htmlspecialchars($position->upload_position_jd);

       // @endphp

        //dd($tag);

        //return $pos;
    }
    
    public  function sendMail($positiondetails)
    {
      // dd($positiondetails);
    
        $to =$positiondetails['to']->email;
      $toname =$positiondetails['to']->name;
    //   // dd($toname);
      $from=$positiondetails['from'];
        
        //  $to='shalini@mindztechnology.com';
        // $toname="shalini";
        // $from='shalini@mindztechnology.com';


     
    
        Mail::send('Emp.mails.newpositionmail', ['mail_data' => $positiondetails], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('New Added Position');

        });
    }
public  function positionassignedreqbyspoc()
    {
         $assigneeid=Auth::user()->id;
         $positionassignedreqbyspoc= DB::table('tbl_clientjd_master as c')
            ->join('tbl_clientjdrecruitermap as m','c.clientjob_id','=','m.fk_jdid')
            ->join('users as u','u.id','=','m.fk_empid')
            ->join('tbl_clients as cm','cm.client_id','=','c.clientjob_compid')
            ->select('*')
            ->where(['m.clientrequpdate_status'=>'1'])
         ->where(['m.fk_empid'=>$assigneeid])
            ->get();
           // dd($positionassignedreqbyspoc);
               return view('Emp.positionassignedreqbyspoc',compact('positionassignedreqbyspoc'));

            //return view('Emp.positionassignedreqbyspoc', $positionassignedreqbyspoc);
    }
public  function submissionlist()
    {
         $assigneeid=Auth::user()->id;
                $submissionlist= DB::table('tbl_recruiter_cv')
            ->select('*')
            ->where(['recruiter_id'=>$assigneeid,'cv_status'=>2])
            ->get();

            //dd($submissionlist);
               return view('Emp.submissionlist',compact('submissionlist'));

            //return view('Emp.positionassignedreqbyspoc', $positionassignedreqbyspoc);
    }
    /*
    public function positionallocate(Request $request)
    {
        
     //   print_r(count($request->req)); exit;
                
for ($x = 0; $x < count($request->req); $x++) {
    
      $usercheck = DB::table('tbl_clientjdrecruitermap')
                   ->select( DB::raw('(SELECT count(clientreq_id) from tbl_clientjdrecruitermap WHERE clientrequpdate_status = 1 and fk_empid='.$request->req[$x].') as count_attendence' ))
                    ->where('fk_empid',$request->req[$x])
                     ->where('clientrequpdate_status', 1)
                    ->first();  
                  
   if(isset($usercheck->count_attendence) )
               {
              //  $usercheck->count_attendence;
if($usercheck->count_attendence>= 2)
{ 
 Session::flash('fail_msg', 'Users Already Assigned More Then 2 Position');
}
else
{ 
   $assigneeid=Auth::user()->id;

       $sql = DB::table('tbl_clientjdrecruitermap')->insert(['fk_jdid' => $request->aid, 'fk_empid' =>$request->req[$x] ,'fk_assigneeid'=>$assigneeid]);
       $sqls = DB::table('tbl_clientjd_master')->where('clientjob_id', $request->aid)
       ->update(['clientjob_status' => 0]);
    
}
                   
                  
               }
                    else
                    {
                        
                         $assigneeid=Auth::user()->id;

       $sql = DB::table('tbl_clientjdrecruitermap')->insert(['fk_jdid' => $request->aid, 'fk_empid' =>$request->req[$x] ,'fk_assigneeid'=>$assigneeid]);
       $sqls = DB::table('tbl_clientjd_master')->where('clientjob_id', $request->aid)
       ->update(['clientjob_status' => 0]); 
                    }
   
 

} 


return redirect()->route('positionlist');
}
*/

    public function positionallocate(Request $request)
    {
       
      //  print_r($_POST); exit;
     //   print_r(count($request->req)); exit;
                
for ($x = 0; $x < count($request->req); $x++) {
//print_r($_POST);
  $usercheckposition = DB::table('tbl_clientjdrecruitermap')
                   ->select( DB::raw('(SELECT count(clientreq_id) from tbl_clientjdrecruitermap WHERE fk_empid='.$request->req[$x].' and fk_jdid= '.$request->aid.'
               ) as count_attendenceposition' ))
                    ->where('fk_empid',$request->req[$x])
                     ->where('clientrequpdate_status', 1)
                    ->get();  
                //   print_r( $usercheckposition ); exit;
    //   print_r($usercheckposition[0]->count_attendenceposition);      
   if(isset($usercheckposition[0]->count_attendenceposition) )
               {
           // echo $usercheckposition[0]->count_attendenceposition;              // print_r($usercheckposition);
if($usercheckposition[0]->count_attendenceposition>=1) 
{
   Session::flash('fail_msg', 'Users Already Assigned This Position');  
    
} else {


    
      $usercheck = DB::table('tbl_clientjdrecruitermap')
                   ->select( DB::raw('(SELECT count(clientreq_id) from tbl_clientjdrecruitermap WHERE clientrequpdate_status = 1 and fk_empid='.$request->req[$x].') as count_attendence' ))
                    ->where('fk_empid',$request->req[$x])
                     ->where('clientrequpdate_status', 1)
                    ->first();  
       // print_r($usercheck);         
   if(isset($usercheck->count_attendence) )
               {
              //  $usercheck->count_attendence;
if($usercheck->count_attendence>= 2)
{ 
 Session::flash('fail_msg', 'Users Already Assigned More Then 2 Position');
}
else
{ 
   $assigneeid=Auth::user()->id;

       $sql = DB::table('tbl_clientjdrecruitermap')->insert(['fk_jdid' => $request->aid, 'fk_empid' =>$request->req[$x] ,'fk_assigneeid'=>$assigneeid]);
       $sqls = DB::table('tbl_clientjd_master')->where('clientjob_id', $request->aid)
       ->update(['clientjob_status' => 0]);
    
}
                   
                  
               }
                    else
                    {
                        
                         $assigneeid=Auth::user()->id;

       $sql = DB::table('tbl_clientjdrecruitermap')->insert(['fk_jdid' => $request->aid, 'fk_empid' =>$request->req[$x] ,'fk_assigneeid'=>$assigneeid]);
       $sqls = DB::table('tbl_clientjd_master')->where('clientjob_id', $request->aid)
       ->update(['clientjob_status' => 0]); 
     }               }
   
 }
 else
 {
        $assigneeid=Auth::user()->id;

       $sql = DB::table('tbl_clientjdrecruitermap')->insert(['fk_jdid' => $request->aid, 'fk_empid' =>$request->req[$x] ,'fk_assigneeid'=>$assigneeid]);
       $sqls = DB::table('tbl_clientjd_master')->where('clientjob_id', $request->aid)
       ->update(['clientjob_status' => 0]); 
 }

} 


return redirect()->route('positionlist');
   }

   public function test_ajax(Request $request)
    {

        $key = $request['key'];
        $assigneeid=Auth::user()->id;

        $pos = "select m.*, c.comp_name from tbl_clientjd_master as m inner join tbl_clients as c on m.clientjob_compid = c.client_id where clientjob_empid = '".$assigneeid."' and (m.clientjob_title like '%".$key."%' or c.comp_name like '%".$key."%' or m.clientjob_noofposition like '%".$key."%' or m.experience like '%".$key."%' or m.domain_experience like '%".$key."%' or m.secondary_skills like '%".$key."%')";

        $pos = DB::select(DB::raw($pos));

                foreach ($pos as $key => $value) {
                  $string  = "'".$value->clientjob_title."'";
                  echo  '<tr class="table-active">
                        <td>1</td>
                        <td>'.$value->clientjob_title.'</td>
                        <td>'.$value->comp_name.' </td>
                        <td>'.$value->clientjob_noofposition.'</td>
                        <td>'.$value->experience.'Year</td>
                        <td>'.$value->domain_experience.' Year</td>
                        <td>'.$value->primary_skills.'</td>
                        <td>'.$value->secondary_skills.'</td>
                        <td>
                            <div class="btn-group btn-group-sm" style="float: none;">
                                 <button type="button" onclick=upload_position_jd('.$value->clientjob_id.','.$string.') class="tabledit-edit-button btn btn-primary waves-effect waves-light"
                                        style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span>
                                </button>
                            </div>
                        </td>
                        <td>
                             <a href="editposition/'.$value->clientjob_id.'">
                             <div class="btn-group btn-group-sm" style="float:none;"> 
                            <button type="button"  class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;">
                            <span class="icofont icofont-ui-edit"></span>
                            </button>
                            </div>
                            </a>
                        </td>
                        </tr>
                        ';
            }
    }

    public function ajaxassignclient(Request $request)
    {
        $key = $request['key'];
        $assigneeid=Auth::user()->id;
        $poss = "select c.*, m.fk_empid, us.name, u.name from tbl_clientspockmap as m inner join users as u on m.assignee_emp_id = u.id inner join tbl_clients as c on c.client_id = m.fk_clientid left join users as us on us.id = m.fk_empid where fk_empid = '".$assigneeid."' and ( c.comp_name like '%".$key."%' or c.contact_name like '%".$key."%' or c.email like '%".$key."%' or c.contact_name1 like '%".$key."%' or c.email1 like '%".$key."%' or c.phone1 like '%".$key."%')";
        $pos = DB::select(DB::raw($poss));

            if(!empty($pos)){

                $i=1;
            foreach ($pos as $key => $val) {
            
                                echo    '<tr class="table-active">
                                            <td>1</td>
                                            <td>'.$val->comp_name.'</td>
                                            <td>'.$val->contact_name.'</td>
                                            <td>'.$val->email.'</td>
                                            <td>'.$val->phone.'</td>
                                            <td>'.$val->contact_name1.'</td>
                                            <td>'.$val->email1.'</td>
                                            <td>'.$val->phone1.'</td>
                                            <td>'.$val->contact_name2.'</td>
                                            <td>'.$val->email2.'</td>
                                            <td>'.$val->phone2.'</td>
                                            <td>'.strip_tags($val->summary).'</td>
                                            <td>'.$val->contractual_sla.'</td>
                                            <td>'.$val->name.'</td>
                                        </tr>';
                                  $i++;  }
                            }else{
                                echo "No Result Fount";
                            }

    }


public function createposition($clientjob_id)
    {
       $assigneeid=Auth::user()->id;
//
        $clientid = ($clientjob_id);
        $client_details = DB::table('tbl_clientjd_master as m')
            ->join('tbl_clients as c','m.clientjob_compid','=','c.client_id')
            ->where('clientjob_id','=',$clientid)
            ->get();

             // dd($client_details[0]);

        $client = DB::table('tbl_clientspockmap as c')
        ->join('tbl_clients as t', 'c.fk_clientid', '=', 't.client_id')
        ->select('c.fk_clientid', 't.comp_name')
        ->where(['c.fk_empid' => $assigneeid])
        ->distinct('c.fk_clientid')
        ->get();

        $mytime = Carbon::now();
        $currentTime = $mytime->toTimeString();
        $currentDate = Date('Y-m-d',strtotime($mytime));
      //  $complist = DB::table('tbl_clientjd_master as m')
     //   ->join 
// $complist = DB::table('tbl_clients as m')
//   ->join('tbl_clientspockmap as c','m.client_id','=','c.fk_clientid')
// ->where('c.assignee_emp_id','=',$clientid)

  
// ->get();
// dd($complist);
        return view('Emp.createposition',compact('client_details','client','currentDate'));
    }
 public function updatecreateposition($clientjob_id,Request $request)
    {

       $assigneeid=Auth::user()->id;
       $clientjob_id = $clientjob_id;

        $user = DB::table('tbl_clientjd_master')->insertGetId([
            'clientjob_deptid'=> $request['dept'],
            'clientjob_compid'=> $request['compname'],
            'clientjob_title'  => str_replace("'","",$request['jobtitle']),
            'upload_position_jd' => strip_tags('clientJd'),
            'clientjob_noofposition'=>$request['noofpos'],
            'drive'=>1,
            'clientjob_empid'=>$assigneeid,
            'clientjob_otherinfo'=>$request['otherinfo'],
            'experience'=>$request['experience'],
            'domain_experience'=>$request['domain_experience'],
            'primary_skills'=>$request['primary_skill'],
            'secondary_skills'=>$request['secondary_skill'],
            'rpn'=>$request['rpn'],
            'rpd'=>$request['rpd'],
            'rpp'=>$request['rpp'],
            'rpe'=>$request['rpe'],
            'pocn'=>$request['pocn'],
            'pocd'=>$request['pocd'],
            'pocp'=>$request['pocp'],
            'poce'=>$request['poce'],
            'hmn'=>$request['hmn'],
            'hmd'=>$request['hmd'],
            'hme'=>$request['hme'],
            'hmp'=>$request['hmp'],
            'drive_shortlist'=>$request['report'],
            'drive_time'=>$request['drivedate'],
           'shortlist_time'=>$request['Shortlistdate'],
            'lineup_from'=>$request['DaliyLineUpFrom'],
            'lineup_to'=>$request['DaliyLineUpTo'],
            'severity'=>$request['severity'],
            'clientjob_jobdescription'=>str_replace( array( '\'', '"', ',' , ';', '<', '>' ), ' ', $request['upload_position_jd']),
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()]);
            
               DB::table('tbl_clientjdrecruitermap')->insert([
                    'fk_jdid'=> $user,
                    'fk_empid'=> 19,
                    'fk_assigneeid'  => 19,
                    'clientreq_dailytarget' => '',
                    'clientreq_starttime'=> '',
                    'clientreq_stoptime'=> '',
                    'clientreq_status'=> 1,
                    'clientreq_createdon'=> Carbon::now(),
                    'clientreq_updatedon'=> Carbon::now()
                ]);
        Session::flash('success_msg', 'Position added Successfully!');
        return redirect()->route('positionlist');
      }




}
