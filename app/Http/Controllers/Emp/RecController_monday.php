<?php
namespace App\Http\Controllers\Emp;
use App\Events\RecStartUploadEvent;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Model\Adminmodel;
use App\Model\Adminmodel\Employees;
use Illuminate\Support\Facades\Input;
use App\Model\User;
use App\Model\Admin;
use Carbon\Carbon;
use Session;
use Storage;
use DB;
use Mail;


    class RecController extends Controller
    {

public function searchusers(Request $request)
        {
 $assigneeid=Auth::user()->id;
       $key = $request['query'];
        $query = "SELECT * FROM users WHERE name LIKE '%".$key."%'";  
        
       
       $result = DB::select(DB::raw($query));
//print_r(count($result));
  $output = '<ul class="list-unstyled">';  
   if(count($result) > 0)  
      { 
          foreach($result as $row)
          
           {  
          $output .= '<li id='.$row->id.'>'.$row->name.'</li>';  
           }
      }
       else  
      {  
           $output .= '<li>Users Not Found</li>';  
      } 
  
  
  
    $output .= '</ul>';  
      echo $output;  
        }

  public function searchposition(Request $request)
        {
 $assigneeid=Auth::user()->id;
       $key = $request['query'];
        $query = "SELECT * FROM tbl_clientjd_master WHERE clientjob_title LIKE '%".$key."%'";  
        
       
       $result = DB::select(DB::raw($query));
//print_r(count($result));
  $output = '<ul class="list-unstyledclients">';  
   if(count($result) > 0)  
      { 
          foreach($result as $row)
          
           {  
          $output .= '<li id='.$row->clientjob_id.'>'.$row->clientjob_title.'</li>';  
           }
      }
       else  
      {  
           $output .= '<li>Position  Not Found</li>';  
      } 
  
  
  
    $output .= '</ul>';  
      echo $output;  
        }



        
          public function getresumesreportdata($uid,$posid,$fromid)
        {

             $recruiters = DB::table('tbl_recruiter_cv as u')
                    ->select('u.*')
                    ->where('u.recruiter_id','=',$uid)
                    ->where('u.position_id','=',$posid)
                     ->where('u.cv_status','=',$fromid)
                //    ->whereBetween('Date(u.created_at)', [$fromid, $toid])
              //    ->whereRaw('Date(u.created_at) between "'.$fromid.'" and "'.$toid.'"')
                    
                    ->get();  
            
      //    dd($recruiters);
            
             $html="";
           //  $html = $uid.'---'.$posid.'----'.$fromid.'----'.$toid;    
           
           //<a href="http://192.168.1.196/elitehr/storage/app/uploadcv/{{$list->cv_uploaded_date}}/{{$list->cv_name}}" margin-left: 15px;"> 
             foreach($recruiters as $key => $val){
                  if($val->cv_uploaded_date!='') {
 $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td><a href="http://115.124.98.243/~elitehr/storage/app/uploadcv/'.$val->cv_uploaded_date.'/'.$val->cv_name.'">'.$val->cv_name.'</a></td><td>'.$val->created_at.'</td>
            </tr>';
                  }
                  
                  else
                  {
                       $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td>'.$val->cv_name.'</td><td>'.$val->created_at.'</td>
            </tr>';
                      
                  }
           
             }
           /* 
            $html = '';          
           
            foreach($recruiters as $key => $val){
               $html .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }*/
            echo $html;
        }
        
        


        public function searchclients(Request $request)
        {
 $assigneeid=Auth::user()->id;
       $key = $request['query'];
        $query = "SELECT * FROM tbl_clients WHERE comp_name LIKE '%".$key."%'";  
        
       
       $result = DB::select(DB::raw($query));
//print_r(count($result));
  $output = '<ul class="list-unstyledclients">';  
   if(count($result) > 0)  
      { 
          foreach($result as $row)
          
           {  
          $output .= '<li id='.$row->client_id.'>'.$row->comp_name.'</li>';  
           }
      }
       else  
      {  
           $output .= '<li>Clients  Not Found</li>';  
      } 
  
  
  
    $output .= '</ul>';  
      echo $output;  
        }


        public function viewallassignedrecuiterposition()
        {
            $pos= DB::table('tbl_clientjd_master as c')
                ->join('tbl_clientjdrecruitermap as m','c.clientjob_id','=','m.fk_jdid')
                ->join('users as u','u.id','=','m.fk_empid')
                ->join('tbl_clients as cm','cm.client_id','=','c.clientjob_compid')
                ->select('m.*','c.*','cm.comp_name',DB::raw('group_concat(distinct(u.name)) as names'),DB::raw('group_concat(distinct(CASE WHEN m.clientrequpdate_status ="1" THEN  u.name ELSE NULL END 
                     )) as name'))
                
                ->groupBy('c.clientjob_id')
                ->get();
            return view('Emp.viewallasignedrec',['pos'=>$pos]);
        }

        public function getrecruiters($position_id)
        {

            $recruiters = DB::table('users as u')
                    ->select('u.*')
                    ->where('emp_role','!=','4')
                    ->whereNotIn('id',function ($query) use ($position_id){
                        $query->select('fk_empid')
                              ->from('tbl_clientjdrecruitermap')
                              ->Where('fk_jdid','=',$position_id)
                              ->Where('clientrequpdate_status','=',1);
                    })
                    ->get();  
            $html = '';          
           
            foreach($recruiters as $key => $val){
               $html .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }
            echo $html;
        }
        
        public function getunassignedrecruiters($position_id)
        {

            $recruiters = DB::table('users as u')
                    ->select('u.*')
                    ->where('emp_role','!=','4')
                    ->whereIn('id',function ($query) use ($position_id){
                        $query->select('fk_empid')
                              ->from('tbl_clientjdrecruitermap')
                              ->Where('fk_jdid','=',$position_id)
                              ->Where('clientrequpdate_status','=',1);
                    })
                    ->get();  
            $html = '';          
           
            foreach($recruiters as $key => $val){
               $html .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }
            echo $html;
        }

        public function delassrecruiters(Request $request)
        {
            $recruiter_id = $request->recruiter;
            $fk_jdid = $request->fk_jdid;
            
            for($i=0;$i<sizeof($recruiter_id);$i++){
                DB::table('tbl_clientjdrecruitermap')
             ->where('fk_empid',$recruiter_id[$i])
                ->where('fk_jdid',$fk_jdid)
                ->update(['clientrequpdate_status' => 0]);
             /* DB::table('tbl_clientjdrecruitermap')
                ->where('fk_empid',$recruiter_id[$i])
                ->where('fk_jdid',$fk_jdid)
                ->delete();*/
            }

            Session::flash('success_msg','Recruiters Deleted Successfully!');
            return redirect()->route('viewallassignedrecuiterposition');
        }

        public function updateassrecruiters(Request $request)
        {
            $assigneeid = Auth::user()->id;
            $recruiter_id = $request->recruiter;
            $clientreq_id = $request->clientreq_id;

            $details = DB::table('tbl_clientjdrecruitermap')
                            ->where('clientreq_id',$clientreq_id) 
                            ->get();
           
            for($i=0;$i<sizeof($recruiter_id);$i++){
                 $usercheck = DB::table('tbl_clientjdrecruitermap')
                    ->select( DB::raw('(SELECT count(clientreq_id) from tbl_clientjdrecruitermap WHERE clientrequpdate_status = 1 and fk_empid='.$recruiter_id[$i].') as count_attendence' ))
       
                    ->where('fk_empid', $recruiter_id[$i])
                     ->where('clientrequpdate_status', 1)
                    ->first();  
               

                 if(isset($usercheck->count_attendence) )
               {
              //  echo $usercheck->count_attendence;
if($usercheck->count_attendence>0)
{ 
 Session::flash('fail_msg', 'Users Already Assigned More Then 2 Position');
}
else
{ 
     $user = DB::table('tbl_clientjdrecruitermap')->where('fk_jdid', $details[0]->fk_jdid)->where('fk_empid', $recruiter_id[$i])->first();
              // print_r($user->fk_empid);
              if(isset($user->fk_empid) && ($user->fk_empid==$recruiter_id[$i]))
               {
               DB::table('tbl_clientjdrecruitermap')
          ->where('fk_jdid', $details[0]->fk_jdid)
          ->where('fk_empid', $recruiter_id[$i])
            ->update(['clientrequpdate_status' => 1]);
               }
               else
               {
               DB::table('tbl_clientjdrecruitermap')->insert([
                    'fk_jdid'=> $details[0]->fk_jdid,
                    'fk_empid'=> $recruiter_id[$i],
                    'fk_assigneeid'  => $assigneeid,
                    'clientreq_dailytarget' => $details[0]->clientreq_dailytarget,
                    'clientreq_starttime'=> $details[0]->clientreq_starttime,
                    'clientreq_stoptime'=> $details[0]->clientreq_stoptime,
                    'clientreq_status'=> $details[0]->clientreq_status,
                    'clientreq_createdon'=> Carbon::now(),
                    'clientreq_updatedon'=> Carbon::now()
                ]);
               }
}

               }
               else
               { 
                 $user = DB::table('tbl_clientjdrecruitermap')->where('fk_jdid', $details[0]->fk_jdid)->where('fk_empid', $recruiter_id[$i])->first();
              // print_r($user->fk_empid);
              if(isset($user->fk_empid) && ($user->fk_empid==$recruiter_id[$i]))
               {
               DB::table('tbl_clientjdrecruitermap')
          ->where('fk_jdid', $details[0]->fk_jdid)
          ->where('fk_empid', $recruiter_id[$i])
            ->update(['clientrequpdate_status' => 1]);
               }
               else
               {
               DB::table('tbl_clientjdrecruitermap')->insert([
                    'fk_jdid'=> $details[0]->fk_jdid,
                    'fk_empid'=> $recruiter_id[$i],
                    'fk_assigneeid'  => $assigneeid,
                    'clientreq_dailytarget' => $details[0]->clientreq_dailytarget,
                    'clientreq_starttime'=> $details[0]->clientreq_starttime,
                    'clientreq_stoptime'=> $details[0]->clientreq_stoptime,
                    'clientreq_status'=> $details[0]->clientreq_status,
                    'clientreq_createdon'=> Carbon::now(),
                    'clientreq_updatedon'=> Carbon::now()
                ]);
               }
               }
 
//print_r($recruiter_id[$i]);

//print_r($details[0]->fk_jdid);
              

               /* DB::table('tbl_clientjdrecruitermap')->insert([
                    'fk_jdid'=> $details[0]->fk_jdid,
                    'fk_empid'=> $recruiter_id[$i],
                    'fk_assigneeid'  => $assigneeid,
                    'clientreq_dailytarget' => $details[0]->clientreq_dailytarget,
                    'clientreq_starttime'=> $details[0]->clientreq_starttime,
                    'clientreq_stoptime'=> $details[0]->clientreq_stoptime,
                    'clientreq_status'=> $details[0]->clientreq_status,
                    'clientreq_createdon'=> Carbon::now(),
                    'clientreq_updatedon'=> Carbon::now()
                ]);*/
            }
            
           Session::flash('success_msg', 'Recruiters Allocated Successfully!');
           return redirect()->route('viewallassignedrecuiterposition');
        }

        public function uploadcv(Request $request)
        {

/*$a = '4/8/2018 17:52:0';

if (strpos($a, ':') !== false) {
    echo 'true';
}


die("reach");*/

//  echo "<pre>";
//  print_r($_POST);
// echo "</pre>";
//              print_r($request->all());
//$cv=$request->file('cv_name');
//print_r($cv->getClientOriginalName());
             //die;
           
        $encryptId = $request->clientreqEncrypt;
            $recruiterId = Auth::user()->id;
            
            /**** Done By Ankit ****/
            
            /**** Done By Ankit ****/
                               // print_r($request->candidate_name[0]);
                               //  print_r($request->candidate_name[1]);
    //        $file = $request->file('cv_name');
            
//echo $file;

//die();


            $positionId = $request->fkjdid;
            $recruiterMapId = $request->clientreqid;
            // echo $var =count($request->candidate_name);die;

            for($i=0; $i<count($request->candidate_name); $i++) {

                // echo ($i);
                // echo "<br>";
                $records = DB::table('tbl_recruiter_cv')
                    ->select('id','recruiter_id')
                    ->where('position_id','=',$positionId)
                    ->where('candidate_email','=',$request->candidate_email[$i])
                    ->where('candidate_mob','=',$request->candidate_mob[$i])
                    ->get();

               // if($records->isEmpty()){
                    $candidateName = $request->candidate_name[$i];
                    $candidateMob = $request->candidate_mob[$i];
                    $candidateEmail = $request->candidate_email[$i];
                    //  Ashish 
                    // $cvuploadtime = $request->filetime[$i];
    // echo "<pre>"; print_r($cvuploadtime);die;

                    $highest_qualificatione = $request->highest_qualificatione[$i];
                    $total_exp = $request->experience[$i];
                    $domain_exp = $request->domaine_exp[$i];
                    $primary_skill = $request->primary_skill[$i];
                    $secondary_skill = $request->secondary_skill[$i];
                    $current_org = $request->current_org[$i];
                    $current_ctc = $request->current_ctc[$i];
                    $expected_ctc = $request->expected_ctc[$i];
                    $np = $request->np[$i];
                    $desng = $request->desng[$i];
                    $location = $request->location[$i];
                    $reason_for_change = $request->reason_for_change[$i];
                    $communication_skills_rating = $request->communication_skills_rating[$i];
                    $relocation = $request->relocation[$i];
                    $doj = $request->doj[$i];
                    $remark = $request->remark[$i];
                    //
                    $date = new \DateTime();
                    //die;

                    $candidateCv = 'uploadcv';
                    $year = date('Y');
                    $month = date('m');
                    $day = date('d');
                    $cv_uploaded_date = $year.'/'.$month.'/'.$day;
                      
                    $cv_name = $request->file('cv_name')[$i];

                    $file = $cv_name->getClientOriginalName();

                    if(!empty($file)){
                   
                    //if((strpos($request->cv_name_hide[$i], ':') !== false) ){

                     // $cv_name=$request->file('cv_name')[$i];
                     // $file = $cv_name->getClientOriginalName();
                    	// echo 'hi'; 
                    $dirStorage = DIRECTORY_SEPARATOR . $candidateCv . DIRECTORY_SEPARATOR . $year . DIRECTORY_SEPARATOR
                        . $month . DIRECTORY_SEPARATOR . $day;

                    Storage::disk('local')->makeDirectory($dirStorage);
                    $fileName = uniqid() . "-" . str_replace(" ", "-", $file);

                    $filepath = $dirStorage . DIRECTORY_SEPARATOR . $fileName;

                  //  echo "<pre>"; print_r($filepath); 
                    Storage::disk('local')->put($filepath,file_get_contents($request->file('cv_name')[$i]->getRealPath()));
                    /**JD upload starts*/
                    
                        

                    }
                    else{
                     
//File::move($dirStorage, $dirStorage2);


Storage::disk('local')->put('uploadcv/2018/08/03/5b63dc8d7a709-java11.doc', Storage::get('uploadcv/2018/08/03/5b63dc8d7a709-java2.doc'));

                      $fileName='5b63dc8d7a709-java6.doc';

                        
                    }

                    if(!empty($request->filetime[$i])){
                        $cvuploadtime = $request->filetime[$i];

                    }

                    else{
                        $cvuploadtime = Carbon::now();

                    }





                     // $fileName= $request->cv_name_hide[0]; //dd($fileName[0]);//die;

                     $sql = DB::table('tbl_recruiter_cv')->insertGetId([
                         'recruiter_id'=>$recruiterId,
                         'r_map_id'=>$recruiterMapId,
                         'position_id'  => $positionId,
                         'cv_name' => $fileName,
                         'cv_uploadtime' =>$cvuploadtime,
                         'cv_uploaded_date' => $cv_uploaded_date,
                         'candidate_name'=>$candidateName,
                         'candidate_mob'=>$candidateMob,
                         'candidate_email'=>$candidateEmail,
                         'highest_qulification'=>$highest_qualificatione,
                         'total_exp'=>$total_exp,
                         'domain_exp'=>$domain_exp,
                         'primary_skill'=>$primary_skill,
                         'secondary_skill'=>$secondary_skill,
                         'current_org'=>$current_org,
                         'current_ctc'=>$current_ctc,
                         'expected_ctc'=>$expected_ctc,
                         'np'=>$np,
                         'desng'=>$desng,
                         'location'=>$location,
                         'reason_for_change'=>$reason_for_change,
                         'communication_skills_rating'=>$communication_skills_rating,
                         'relocation'=>$relocation,
                         'doj'=>$doj,
                         'remark'=>$remark,
                         'created_at'=> Carbon::now(),
                         'updated_at'=>Carbon::now()
                   ]);
                 
                    /*-----------------------------------------------*/
                     $url = 'http://115.124.98.243/~logistiks/solrApi/uploadfile.php';
                     $merchantname = 'techproducts';
                     $fileid = $sql;

                        // Make sure there are no upload errors
                         // if ($_FILES['cv_name']['error'][0] > 0)
                         // {
                         //     die("Error uploading file...");
                         // }
                     
                         // Prepare the cURL file to upload, including file name and MIME type
                         $post = array(
                         'file_contents' => new \CurlFile($_FILES["cv_name"]["tmp_name"][0], $_FILES["cv_name"]["type"][0],$_FILES["cv_name"]["name"][0]),
                         );
                         $fileposts=array('merchantname'=>$merchantname,'fileid'=>$fileid,'experience'=>$total_exp,'salary'=>$current_ctc,'location'=>$location,'designation'=>$desng,'company'=>$current_org,'primaryskill'=>$primary_skill,'secondaryskill'=>$secondary_skill);
                         // Include the other $_POST fields from the form?
                         $post = array_merge($post, $fileposts);
                     
                         // Prepare the cURL call to upload the external script
                         $ch = curl_init();
                         curl_setopt($ch, CURLOPT_URL, $url);
                         curl_setopt($ch, CURLOPT_HEADER, false);
                         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                         curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0");
                         curl_setopt($ch, CURLOPT_POST, true);
                         curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                         $result = curl_exec($ch);
                        curl_close($ch);
                     
                 
                  
                 }
                // print_r(json_decode($result));
                /*------------------------------------------------*/
            //}
        

            
             $uid=Auth::user()->id;
            $count= DB::table('tbl_recruiter_cv')
                
                ->where('recruiter_id', '=', $uid)
                ->where('cv_uploaded_date', '=', date("Y/m/d"))
                ->get()
                ->count();

            Session::flash('success_msg', 'Candidate resume uploaded. Successfully to start another position please close 
                this position');
            return redirect('get-assigned-postion/'.$encryptId);
        }

 public function editassignedpostion($id,$fid)
        {

return view('Emp.editassignedpostion');
        }

        public function submission(Request $request){


$encryptId =$request['clientreqEncrypt'];
// $encryptId =  Crypt::encrypt($encrypt);
            $recruiterId = Auth::user()->id;

            $sql = DB::table('tbl_recruiterperformance')->insertGetId([
                    'rp_recruiter_id'=>$recruiterId,
                    'rp_date'=>Carbon::now()
                    
                ]);
                print_r($sql);
            return redirect('get-assigned-postion/'.$encryptId);
        }

        public function getassignedposition($id)
        {
                // print_r($_GET['search_data']);
                if(!empty($_GET['search_data'])){

                   $url='http://115.124.98.243/~logistiks/solrApi/search.php';
$data = $_GET['search_data'];
$post = [
  'search' =>$data, 
  'merchantname' => 'techproducts'
];

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec ($ch);

curl_close ($ch);

#print_r($server_output);
//print_r(json_decode($server_output));

// foreach (json_decode($server_output) as $key => $value) {
//  print_r($value);
//   echo '<br>';
// }
// further processing ....
#if (!empty($server_output)) { echo '<pre>'; print_r(json_decode($server_output)); } else {echo "ni"; }
$apidata=json_decode($server_output);
// $res=$apidata->response;
// print_r($res);
// print_r($res[0]->error_status);
if($apidata->response[0]->error_status=='Success'){  
    // dd($apidata->response[0]->error_status);
$id_array = array();
foreach ($apidata->result as $key => $value) {
    // print_r($value);
    array_push($id_array, $value->id);
    # code...
}
//print_r($id_array);
$ids= implode(',', $id_array);
//echo $ids;

//die();
 //$getpostion='200';
$getpostion = DB::table('tbl_recruiter_cv as st')
                ->whereRaw("FIND_IN_SET(st.id, '$ids')")
                ->select('st.*')
                ->get();

                   // dd($getpostion);
//echo 2222;
}
else{
    //echo 111111;
    $getpostion='';
}
                 
                }
                else
                {
                    $getpostion='';
                }
                // dd($getpostion);
                 // die;
                // return view('Emp.allpositionassigned',compact('getpostion'));
                // return redirect('get-assigned-postion/',compact('getpostion'));

            $clientid = Crypt::decrypt($id); "<br>";
            $recruiterId = Auth::user()->id;


            $getList = DB::table('tbl_clientjd_master as tcm')
                ->join('tbl_clients as tc', 'tcm.clientjob_compid', '=', 'tc.client_id')
                ->join('tbl_clientjdrecruitermap as tcp', 'tcm.clientjob_id', '=', 'tcp.fk_jdid')
                ->select('tcm.clientjob_title','tcp.clientreq_id', 'tc.*', 'tcm.upload_position_jd', 'tcp.fk_jdid', 'tcp.clientreq_id')
                ->where('tcm.clientjob_id', '=', $clientid)
                ->first();
        //  dd($clientid);

            $getuploadedCv=DB::table('tbl_recruiter_cv as trc')
                ->select('trc.cv_name','trc.id', 'trc.candidate_name', 'trc.candidate_email', 'trc.candidate_mob')
                ->where('trc.recruiter_id',$recruiterId)
                ->where('trc.position_id', '=', $clientid)
                ->get();

//dd($getuploadedCv);
            $data = Session::all();

            if(isset($data['time']['starttime']))
            {

                $date1 = $data['time']['starttimevalue'];
                $date2 = date("H:i:s");
                $datedataa=round((strtotime($date2) - strtotime($date1)) /60);
                $datedataactringtotime=((strtotime(date("Y-m-d ".$date1))).'000');
                $datecustid = $data['time']['custid'];

                $datestime=$datedataa;
            }
            else
            {
                $datestime=-1;
                $datecustid =0;
                $datedataactringtotime=((strtotime(date("Y-m-d H:i:s"))).'000');
            }
            // dd($datestime);

 $uid=Auth::user()->id;
            $count= DB::table('tbl_recruiter_cv')
                
                ->where('recruiter_id', '=', $uid)
                ->where('cv_uploaded_date', '=', date("Y/m/d"))
                ->get()
                ->count();
                
            return view('Emp.allpositionassigned', [
                'getlists'=>$getList,
                'sessionstart'=>$datestime,
                'datedataact'=>$datedataactringtotime,
                'datecustid'=>$datecustid,
                'positionid'=>$id,
                'getuploadCv'=>$getuploadedCv,
                'getpostion'=>$getpostion,
                'count'=>$count
            ]);
        }

        public function reasonfordelay()
        {
            $currentDate = date('Y-m-d');
            $time = date('h:i:s');
            $recruiterId = Auth::user()->id;
            $clientreq_id = $_GET['clientreq_id'];

            $currentDateEntry = DB::table('tbl_clientrec_start_endtime as tcse')
                                    ->select('tcse.*')
                                    ->where('clientstart_stoptime', '=', NULL)
                                    ->where('clientreq_id', '=', $clientreq_id)
                                    ->where('clientstart_recuiterid', '=', $recruiterId)
                                    ->whereDate('created_at', '=', Carbon::today())
                                    ->orderBy('clientstartid','desc')
                                    ->first();
            
            $currentDateTime = strtotime($currentDateEntry->created_at);
            $finalDateTime = $currentDateTime + 1800;
             
            $currDate = date('Y-m-d H:i:s',$currentDateTime);
            $finalDate = date('Y-m-d H:i:s',$finalDateTime);
            

            $cventry = DB::table('tbl_recruiter_cv as trc')   
                                ->select('trc.*') 
                                ->where('created_at', '>', $currDate)
                                ->where('created_at', '<', $finalDate)
                                ->where('recruiter_id', '=', $recruiterId)
                                ->get();
            
            DB::table('tbl_clientrec_start_endtime')
                ->where('clientstartid', $currentDateEntry->clientstartid)
                ->update(['created_at' => $finalDate]);

            if($cventry->isEmpty()){
               echo $currentDateEntry->clientstartid;
            }else{
                echo '0';
            }
        }

        public function duplicatecvupload()
        {
            $positionid = $_POST['positionid'];
            $candidate_email = $_POST['email'];
            $candidate_mob = $_POST['candidate_mob'];

            $records = DB::table('tbl_recruiter_cv')
                                ->select('id','recruiter_id')
                                ->where('position_id','=',$positionid)
                                ->where('candidate_email','=',$candidate_email)
                                ->where('candidate_mob','=',$candidate_mob)
                                ->get();
            
            if($records->isEmpty()){
                echo json_encode(1);
            }else{
                $records = DB::table('users')
                                ->select('name')
                                ->where('id','=',$records[0]->recruiter_id)
                                ->first();
                echo json_encode($records->name);
            }
        }

        public function clientstart()
        {
            $clientstartid = $_POST['clientstartid'];
            $reason = $_POST['reason'];

            $created_at = DB::table('tbl_clientrec_start_endtime')
                                ->select('created_at')
                                ->where('clientstartid','=',$clientstartid)
                                ->first();

            // DB::table('tbl_clientrec_start_endtime')
            //     ->where('clientstartid', $clientstartid)
            //     ->update(['created_at' => date('Y-m-d H:i:s')]);

            $clientstartid = DB::table('tbl_start_endtime_reasons')
                            ->insertGetId(['clientstartid' => $clientstartid,'reason_for_delay' => $reason,
                                'created_at' => $created_at->created_at,'updated_at' => date('Y-m-d H:i:s')]);
            
            if($clientstartid){
                echo $clientstartid;
            }else{
                echo 0;
            }   
        }

        public function breaktime()
        {
            $mytime = time();
            $currentTime = $mytime;
            $recruiterId = Auth::user()->id;

            $checkTime = DB::table('tbl_clientrec_break_backtime as tcbr')
                ->select('tcbr.*')
                ->where('clientstart_recuiterid', '=', $recruiterId)
                ->where('clientstart_backtime', '=', NULL)
                ->count();
           
            if($checkTime == 0) {
                DB::table('tbl_clientrec_break_backtime')->insert([
                    'clientstart_breaktime'=>$currentTime,
                    'clientstart_recuiterid'=>$recruiterId,
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now()
                ]);
            }
        }

        public function breaktimeback()
        {
            $mytime = time();
            $currentTime = $mytime;
            $recruiterId = Auth::user()->id;

            $gettimeid = DB::table('tbl_clientrec_break_backtime')
                ->where('clientstart_recuiterid', '=',$recruiterId)
                ->select('tbl_clientrec_break_backtime.*')
                ->orderBy('clientstartbreak_id', 'desc')
                ->first();
            
            DB::table('tbl_clientrec_break_backtime')
                ->where('clientstartbreak_id', '=', $gettimeid->clientstartbreak_id)
                ->update([
                    'clientstart_backtime'=>$currentTime
                ]);
            
            $time1 = $gettimeid->clientstart_breaktime;
            $time2 = $currentTime;

            $difference = ($time2 - $time1);
            $diffinminutes = ($difference) / 60;
            
            if($diffinminutes > 35){
                echo $diffinminutes - 35;
            }else{
                echo '0';
            }
        }

        public function gettime($id)
        {
           // $reason = $_GET['reason'];

            $mytime = Carbon::now();
            $currentTime = $mytime->toTimeString();
            $recruiterId = Auth::user()->id;

            $checkTime = DB::table('tbl_clientrec_start_endtime as tcse')
                ->select('tcse.*')
                ->where('clientstart_recuiterid', '=', $recruiterId)
                ->where('clientstart_stoptime', '=', 'NULL')
                ->count();
            $getpostion = DB::table('tbl_clientjd_master as st')
                ->where('st.clientjob_id','=',$id)

                ->select('st.*')
                ->orderBy('st.clientjob_id', 'desc')
                ->first();

            if( $checkTime == 0 ) {


                DB::table('tbl_clientjdrecruitermap')
                    ->where('clientreq_id', '=', $id)
                    ->update([
                        'clientreq_starttime'=>$currentTime
                    ]);



                DB::table('tbl_clientrec_start_endtime')->insert([
                    'clientreq_id'=>$id,
                    'clientstart_starttime'=>$currentTime,
                    'clientstart_recuiterid'=>$recruiterId,
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now()

                ]);
                //  echo $currentTime;


                $datas = Session::all();

                Session::put('time.starttime', 0);
                Session::put('time.starttimevalue', $currentTime);
                Session::put('time.custid', $id);

                //dd($datas);

                $dataemp = Employees::select("*")->where('id','=',"{$recruiterId}")->get();
                $dataadmin = User::select("*")->where('id','=',"1")->get();
                $emailuser= $dataemp[0]['email'];
                $emailadmin= $dataadmin[0]['email'];

                $spoc=DB::table('tbl_clientjd_master as c')
                    ->join('users as u','c.clientjob_empid','=','u.id')
                    ->where('c.clientjob_id',$id)
                    ->select('u.email','u.name')
                    ->get();
                $spocemail=$spoc[0]->email;
                $spocname=$spoc[0]->name;
                $adminemail=$dataadmin[0]['email'];
                $adminname=$dataadmin[0]['name'];

                $to=[['name'=>$spocname,'email'=>$spocemail],['name'=>$adminname,'email'=>$adminemail]];



                $data = [
                    'name' => $dataemp[0]['name'],
                    'email' =>$dataemp[0]['email'],
                    'adminname' =>$adminname,
                    'positionname' =>$getpostion->clientjob_title,
                    'emailadmin' =>$adminemail,
                ];





                // Mail::send('Emp.mails.starttimemail', $data, function ($message) use ($data,$to) {

                //     $message->from($data['email'], $data['name']);
                //     foreach ($to as $key=>$value)
                //     {
                //         $message->to($value['email'], $value['name']);

                //     }
                //     $message->subject('Started work on position');
                // });


    //Session::put('time.starttimevalue', $currentTime);
                return 'yes';
            } else{
                return 'No';
            }

        }


        public function endtime($id)
        {

            $getcv = DB::table('tbl_recruiter_cv')
                ->select('tbl_recruiter_cv.*' )
                ->where('r_map_id', $id)->get();


            $getjobid = DB::table('tbl_clientrec_start_endtime')
                ->where('clientreq_id', '=', $id)
                ->select('tbl_clientrec_start_endtime.*')
                ->orderBy('clientstartid', 'desc')
                ->first();
            $getpostion = DB::table('tbl_clientjd_master as st')
                ->where('st.clientjob_id','=',$id)

                ->select('st.*')
                ->orderBy('st.clientjob_id', 'desc')
                ->first();
            $mytime = Carbon::now();
            $currentTime = $mytime->toTimeString();
            DB::table('tbl_clientjdrecruitermap')
                ->where('clientreq_id', '=', $id)
                ->update([
                    'clientreq_stoptime'=>$currentTime
                ]);
            $recruiterId = Auth::user()->id;

            DB::table('tbl_clientrec_start_endtime')
                ->where('clientstartid', '=', $getjobid->clientstartid)
                ->update([
                    'clientstart_stoptime'=>$currentTime
                ]);


            $dataemp = Employees::select("*")->where('id','=',"{$recruiterId}")->get();
            $dataadmin = User::select("*")->where('id','=',"1")->get();

            $emailuser= $dataemp[0]['email'];
            $emailadmin= $dataadmin[0]['email'];

            $datas = Session::all();
            Session::forget('time.starttime');

            $spoc=DB::table('tbl_clientjd_master as c')
                ->join('users as u','c.clientjob_empid','=','u.id')
                ->where('c.clientjob_id',$id)
                ->select('u.email','u.name')
                ->get();
            $spocemail=$spoc[0]->email;
            $spocname=$spoc[0]->name;
            $adminemail=$dataadmin[0]['email'];
            $adminname=$dataadmin[0]['name'];
            $spoc=DB::table('tbl_clientjd_master as c')
                ->join('users as u','c.clientjob_empid','=','u.id')
                ->where('c.clientjob_id',$id)
                ->select('u.email','u.name')
                ->get();
            $spocemail=$spoc[0]->email;
            $spocname=$spoc[0]->name;
            $adminemail=$dataadmin[0]['email'];
            $adminname=$dataadmin[0]['name'];

            $to=[['name'=>$spocname,'email'=>$spocemail],['name'=>$adminname,'email'=>$adminemail]];


            $emailuser= $dataemp[0]['email'];
            $emailadmin= $dataadmin[0]['email'];

            $datas = Session::all();

            $data = [
                'name' => $dataemp[0]['name'],
                'email' =>$dataemp[0]['email'],
                'adminname' =>$dataadmin[0]['name'],
                'getcv' =>$getcv,
                'positionname' =>$getpostion->clientjob_title,
                'urldata' =>URL('/'),
                'emailadmin' =>$emailadmin

            ];


    //mail to spoc and admin
            Mail::send('Emp.mails.endtimemail', $data, function($message) use ($data,$to)
            {
                $message->from($data['email'],$data['name']);
                foreach ($to as $key=>$value)
                {
                    $message->to($value['email'], $value['name']);

                }
                $message->subject('Endtime work on position');
            });


        }

        public function sendAutoMail($id)
        {
            $recid=Auth::user()->id;

            $livetime = date('Y-m-d H:i:s ', time() - 3600);
            $t=date('Y-m-d H:i:s ',time());

            $getpostion = DB::table('tbl_clientjd_master as st')
                ->where('st.clientjob_id','=',$id)

                ->select('st.*')
                ->orderBy('st.clientjob_id', 'desc')
                ->first();
echo $id;
dd($getpostion);
            $getjst = DB::table('tbl_clientrec_start_endtime as st')
                ->where('st.clientstart_recuiterid','=',$recid)
                ->where('st.clientreq_id','=',$id)
                ->select('st.*')
                ->orderBy('st.clientstartid', 'desc')
                ->first();
            $dataemp = Employees::select("*")->where('id','=',"{$recid}")->get();
            $dataadmin = User::select("*")->where('id','=',"1")->get();
            $emailuser= $dataemp[0]['email'];
            $emailadmin= $dataadmin[0]['email'];



    //dd($getjst->clientstart_starttime);

            if(!empty($getjst->clientstart_starttime)) {
                //  echo "dddddddddddd";die;
                if(empty($getjst->clientstart_stoptime)) {


                    $getcvupload = DB::table('tbl_recruiter_cv as rc')
                        ->select(DB::raw('COUNT(rc.id) as cvcount'))
                        ->whereBetween('rc.created_at', [$livetime, $t])
                        ->where('rc.recruiter_id', '=', $recid)
                        ->where('rc.position_id', '=', $id)
                        ->get();


                    if ($getcvupload[0]->cvcount  == 0) {


                        $data= [
                            'name' => $dataemp[0]['name'],
                            'email' =>$dataemp[0]['email'],
                            'adminname' =>$dataadmin[0]['name'],
                            'positionname' =>$getpostion->clientjob_title,
                            'emailadmin' =>$emailadmin

                        ];
    //print_r($data);die;
                        $spoc=DB::table('tbl_clientjd_master as c')
                            ->join('users as u','c.clientjob_empid','=','u.id')
                            ->where('c.clientjob_id',$id)
                            ->select('u.email','u.name')
                            ->get();
                        $spocemail=$spoc[0]->email;
                        $spocname=$spoc[0]->name;
                        $adminemail=$dataadmin[0]['email'];
                        $adminname=$dataadmin[0]['name'];

                        $to=[['name'=>$spocname,'email'=>$spocemail],['name'=>$adminname,'email'=>$adminemail]];
    //print_r($data);die;
                        Mail::send('Emp.mails.sendonehoursautomail',$data, function($message) use ($data,$to)
                        {
                            $message->from($data['email'],$data['name']);
                            // $message->to('bivek@mindztechnology.com', 'Shalini');
                            foreach ($to as $key=>$value)
                            {
                                $message->to($value['email'], $value['name']);

                            }
                            $message->subject('Recruiter Facing Issue on Cv Upload');
                        });


                        /*     Mail::send('Emp.sendonehoursautomail', $data, function ($message) use ($data) {
                                $message->from('gurpreetsingh@mindztechnology.com', 'Gurpreet Singh');
                                $message->to('bivek@mindztechnology.com', 'Shalini');
                                $message->subject('User has not submit any Resume');
                            });
            */

                    }
                }
                else
                {
                    echo "bvek";die;
                }
            }
            else
            {
                echo "hgghgh";die;

            }
        }


    public function cvparsing(Request $request)
        {

            if(!empty($_GET['search_data'])){

            $url='http://115.124.98.243/~logistiks/solrApi/search.php';
            $data = $_GET['search_data'];
            $post = [
                        'search' =>$data, 
                        'merchantname' => 'techproducts'
                    ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $server_output = curl_exec ($ch);

            curl_close ($ch);

            $apidata=json_decode($server_output);

         //   echo "<pre>"; print_r($apidata);die;

            if($apidata->response[0]->error_status=='Success'){  

            $id_array = array();
                foreach ($apidata->result as $key => $value) {
                    array_push($id_array, $value->id);
                }

                $ids= implode(',', $id_array);

                $getpostion = DB::table('tbl_recruiter_cv as st')
                                ->whereRaw("FIND_IN_SET(st.id, '$ids')")
                                ->select('st.*')
                                ->get();

             //   echo "<pre>"; print_r($getpostion);die;

            }
            else{

                $getpostion='';
            }
                 
        }
        else
        {
            $getpostion='';
        }
                return view('Emp.cvprofile', ['getpostion'=>$getpostion]); 
    }


        public function cvsearch()
                {
                    return view('Emp.cvsearch');
                }

        public function detailprofile()
                {
                    return view('Emp.detailprofile');
                }

        public function saveresume(Request $request)
        {
            //  $in=$request->all();
            $file = $request->file('cv_name');
            //  print_r($in);die;

            $recruiterId=Auth::user()->id;

            $positionId = $request->fkjdid;
            $recruiterMapId = $request->clientreqid;

            for($i=0; $i<count($request->candidate_name); $i++) {

                $candidateName = $request->candidate_name[$i];
                $candidateMob = $request->candidate_mob[$i];
                $candidateEmail = $request->candidate_email[$i];

                $date = new \DateTime();

                $candidateCv = 'uploadcv';
                $year = date('Y');
                $month = date('m');
                $day = date('d');
                $dirStorage = DIRECTORY_SEPARATOR . $candidateCv . DIRECTORY_SEPARATOR . $year . DIRECTORY_SEPARATOR
                    . $month . DIRECTORY_SEPARATOR . $day;

                Storage::disk('local')->makeDirectory($dirStorage);
                $fileName = uniqid() . "-" . str_replace(" ", "-", $request->file('cv_name')[$i]->getClientOriginalName());

                $filepath = $dirStorage . DIRECTORY_SEPARATOR . $fileName;
                Storage::disk('local')->put($filepath,file_get_contents($request->file('cv_name')[$i]->getRealPath()));
                /**JD upload starts*/

                DB::table('tbl_recruiter_cv')->insert([
                    'recruiter_id'=>$recruiterId,
                    'r_map_id'=>$recruiterMapId,
                    'position_id'  => $positionId,
                    'cv_name' => $fileName,
                    'candidate_name'=>$candidateName,
                    'candidate_mob'=>$candidateMob,
                    'candidate_email'=>$candidateEmail,
                    'created_at'=>Carbon::now(),
                    'updated_at'=>Carbon::now()
                ]);
            }
        }


public function cvsubmission(){

    $id = Auth::user()->id;

    $data=[];

    $client = DB::table('tbl_clientspockmap as spoc')
            ->join('tbl_clients as c','c.client_id','=','spoc.fk_clientid')
            ->select('spoc.*','c.*')
            ->where('spoc.fk_empid','=',$id)
            ->get();

// dd($client);
    foreach($client as  $key => $value) {

      //  print_r($key);
        $data[$key]["comp_name"]=$value->comp_name;
        $data[$key]["client_id"]=$value->client_id;

        $client1 = DB::table('tbl_clientjd_master as jd')
            ->join('tbl_clients as c','c.client_id','=','jd.clientjob_compid')
             ->join('tbl_clientjdrecruitermap as rec','rec.fk_jdid','=','jd.clientjob_id')
             ->join('users as u','u.id','=','rec.fk_empid')
            ->select('jd.clientjob_title','jd.clientjob_id','u.name','u.id',DB::raw('(SELECT count(*) FROM tbl_recruiter_cv as rrrr WHERE rrrr.recruiter_id = u.id and rrrr.position_id=jd.clientjob_id ) as upload_cv'),DB::raw('(SELECT count(*) FROM tbl_recruiter_cv as rrrr WHERE rrrr.recruiter_id = u.id and rrrr.position_id=jd.clientjob_id  and rrrr.cv_status=2 ) as sendtoclient_cv'))
            ->where('c.client_id','=',$value->client_id)
            ->get();
             // dd($client1);
$data[$key]["position"]=$client1;
    // foreach ($client1 as $keyyy => $valuee) { 
/*
 $client2 = DB::table('tbl_clientjd_master as jd')
            ->join('tbl_clients as c','c.client_id','=','jd.clientjob_compid')
            ->join('tbl_clientjdrecruitermap as rec','rec.fk_jdid','=','jd.clientjob_id')
            ->join('users as u','u.id','=','rec.fk_empid')
            ->select('u.name','u.id')
            ->where('jd.clientjob_id','=',$valuee->clientjob_id)
            ->get();
             // dd($client2);

    foreach ($client2 as $keyn => $valueee) {

          $sql = "SELECT count(rv.id) as trtr from tbl_recruiter_cv as rv inner join tbl_clientjd_master 
        as cm on cm.clientjob_id=rv.position_id where 
        rv.recruiter_id=$valueee->id";

// echo ($sql);die;
        $client3 = DB::select(DB::Raw($sql));
//print_r($client3);
 $data[$key]["positionn"][$keyyy][$keyn]["cvuploadcount"]=$client3;

//echo $key."---".$keyyy."--".$keyn."<br>";


    }
*/
 




     // }
    }
//     echo "<pre>";
// print_r($data);
// echo "</pre>";
 return view('Emp.cvsubmissionreport',compact('data'));

}

public function searchcandidate(Request $request)
    {

        $key = $request['key'];
        $act_mode = $request['act_mode'];
        $pos_id = $request['posid'];

        $assigneeid=Auth::user()->id;
        $comp =DB::select(DB::raw("Select c.client_id from tbl_clients as c inner join tbl_clientjd_master as jd on jd.clientjob_compid=c.client_id where jd.clientjob_id=$pos_id"));

        if($act_mode==1){

            $result="select rcv.*,u.name,jd.clientjob_title,jd.clientjob_id,jd.drive_time,jd.shortlist_time,jd.lineup_to,c.comp_name,c.client_id from tbl_recruiter_cv as rcv inner join users as u on u.id=rcv.recruiter_id inner join tbl_clientjd_master as jd on jd.clientjob_id=rcv.position_id inner join tbl_clients as c on c.client_id=jd.clientjob_compid where rcv.candidate_name like '%".$key."%'";

        }

        else if($act_mode==2){
            $result="select rcv.*,u.name,jd.clientjob_title,jd.clientjob_id,jd.drive_time,jd.shortlist_time,jd.lineup_to,c.comp_name,c.client_id from tbl_recruiter_cv as rcv inner join users as u on u.id=rcv.recruiter_id inner join tbl_clientjd_master as jd on jd.clientjob_id=rcv.position_id inner join tbl_clients as c on c.client_id=jd.clientjob_compid where rcv.candidate_email like '%".$key."%'";

        }
        else{
            $result="select rcv.*,u.name,jd.clientjob_title,jd.clientjob_id,jd.drive_time,jd.shortlist_time,jd.lineup_to,c.comp_name,c.client_id from tbl_recruiter_cv as rcv inner join users as u on u.id=rcv.recruiter_id inner join tbl_clientjd_master as jd on jd.clientjob_id=rcv.position_id inner join tbl_clients as c on c.client_id=jd.clientjob_compid where rcv.candidate_mob like '%".$key."%'";

        }
 

        $pos = DB::select(DB::raw($result));

// echo "<pre>";
//         print_r($comp);
     echo "<pre>"; 
      // print_r($pos);die;

     $date = Date('Y-m-d');
     // print_r($date);
                echo '<tr>
                        <th>Candidate Name</th>
                        <th>Candidate Mobile</th>
                        <th>Company Name</th>
                        <th>Position Name</th>
                        <th>Recruiter Name</th>
                        <th>Action</th>
                        
                    </tr>';

                foreach ($pos as $key => $value) {

 // echo $pos[$key]->clientjob_id.'--'.$pos_id.'--'.$pos[$key]->client_id.'--'.$comp[0]->client_id.'----'.$pos[$key]->drive_time.'----'.$date; die;

if(($pos[$key]->clientjob_id!=$pos_id) && ($pos[$key]->client_id!=$comp[0]->client_id) && (($pos[$key]->drive_time<$date) || ($pos[$key]->shortlist_time<$date) || ($pos[$key]->lineup_to<$date)))
         	{
         		$arr = '<option value="'.$pos[$key]->id.','.'2'.'">Resubmit</option>';
         	}
         	else{
         		$arr='';
         	}

                  $string  = "'".$value->candidate_name."'";
                  echo  '<tr class="table-active">
                        
                        <td>'.$value->candidate_name.'</td>
                        <td>'.$value->candidate_mob.' </td>
                        <td>'.$value->comp_name.'</td>
                        <td>'.$value->clientjob_title.'</td>
                        <td>'.$value->name.'</td>
                        <td>
                        <select id="searchcategory" name="checkbox_data[]" onchange="resubmit(this.value)" class="form-control" style="display:block">
					        <option value=""> Search By</option>
					        <option value="'.$value->id.','.'1'.'">View</option>.'.$arr.'"
					        
					        <option value="'.$value->id.','.'3'.'">Change Status </option>

					    </select>
   						</td>
                        </tr>
                        ';}

            





    }


    public function resubmission(Request $request){
 

        // $id = ($id);
        $id = $request['key'];
    // $fid =($fid);
$sql = "select * ,concat(cv_uploaded_date,'|',cv_name)as cv_nameeee FROM tbl_recruiter_cv where(id=$id)";

 //    $candidate_details = DB::table('tbl_recruiter_cv')
 //        ->select('*',concat('cv_uploaded_date','|','cv_name')  'cv_name')
 //        ->where('id',$id)
 //        ->get();
 // echo ($candidate_details);
 $candidate_details=DB::select(DB::raw($sql));
 echo (json_encode($candidate_details)); 
        //dd($candidate_details);

        

                // foreach ($candidate_details as $key => $candidate_details) {
                //   $string  = "'".$candidate_details->candidate_name."'";

//             echo '<tr class="table-active entry">
   
//                     <td>
//                         <div class="form-group required">


//                             <input type="text" pattern="[A-Za-z\s]+" required="" name="candidate_name[]" class="form-control" placeholder="Enter First Name" title="First Name should only contain  letters. e.g. John" maxlength="60" value="'.$candidate_details->candidate_name.'">
//                         </div>
//                     </td>

//                     <td>
//                         <div class="form-group required">
//                             <input type="tel" name="candidate_mob[]" pattern="^\d{10}$" required="" class="form-control" value="'.$candidate_details->candidate_mob.'">
//                         </div>
//                     </td>
//                     <td>
//                         <div class="form-group required">
//                             <input type="text" required="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50" name="candidate_email[]" class="form-control" value="'.$candidate_details->candidate_email.'">
//                         </div>
//                     </td>
//                     <td>
//                         <label class="custom-file ih">
//                             <input onchange="gettimeval(this)" type="file" id="file" class="ihi" name="cv_name[]" value="'.$candidate_details->cv_name.'" width="150px">
//                             <input type="hidden" id="" name="cv_name_hide[]" value="'.$candidate_details->cv_name.'">
//                             <span>'.$candidate_details->cv_name.'</span>

//                             <input type="hidden" class="filetime" name="filetime[]" value="'.date("d/m/Y h:i:sa").'">
//                         </label>
//                     </td>
//                     <td>
//                         <div class="form-group required">
//                             <input type="text" name="highest_qualificatione[]" required="" class="form-control" value="'.$candidate_details->highest_qulification.'">
//                         </div>
//                     </td>
//                     <td>
//                         <div class="form-group required">
//                             <input type="tel" name="experience[]" required="" class="form-control" maxlength="10" value="'.$candidate_details->total_exp.'">
//                         </div>
//                     </td>
//                     <td>
//                         <div class="form-group required">
//                             <input type="text" name="domaine_exp[]" required="" class="form-control" value="'.$candidate_details->domain_exp.'" maxlength="50">
//                         </div>
//                     </td>
//                     <td>
//                         <label class="custom-file ih">
                   
//                              <input type="text" name="primary_skill[]" required="" class="form-control" value="'.$candidate_details->primary_skill.'" maxlength="50">
//                         </label>
//                     </td>
//                     <td>
//                         <label class="custom-file ih">
//                              <input type="text" name="secondary_skill[]" required="" class="form-control" value="'.$candidate_details->secondary_skill.'" maxlength="50">
                            
//                         </label>
//                     </td>

//                     <td>
//                         <div class="form-group required">
//                             <input type="text" required="" name="current_org[]" class="form-control" value="'.$candidate_details->current_org.'" maxlength="60">
//                         </div>
//                     </td>
//                     <td>
//                         <div class="form-group required">
//                             <input type="text" required="" name="current_ctc[]" class="form-control" value="'.$candidate_details->current_ctc.'">
//                         </div>
//                     </td>
//                     <td>
//                         <div class="form-group required">
//                             <input type="text" required="" name="expected_ctc[]" class="form-control" value="'.$candidate_details->expected_ctc.'">
//                         </div>
//                     </td>
//                     <td>
//                         <div class="form-group required">
//                             <input type="text" required="" name="reason_for_change[]" class="form-control" value="'.$candidate_details->reason_for_change.'">
//                         </div>
//                     </td>
//                      <td>
//                         <div class="form-group required">
//                             <select name="communication_skills_rating[]" class="form-control">
//                                 <option value="1" selected=""> 1 </option>
//                                 <option value="2"> 2 </option>
//                                 <option value="3"> 3 </option>
//                                 <option value="4"> 4 </option>
//                                 <option value="5"> 5 </option>
//                             </select>
//                         </div>
//                     </td>
//                     <td>
//                         <label class="custom-file ih">
//                         <input type="text" name="np[]" required="" class="form-control" value="'.$candidate_details->np.'">
//                         </label>
//                     </td>
//                     <td>
//                         <label class="custom-file ih">
//                         <input type="text" name="desng[]" required="" class="form-control" value="'.$candidate_details->desng.'">
//                         </label>
//                     </td>
//                     <td>
//                         <label class="custom-file ih">
//                         <input type="text" name="location[]" required="" class="form-control" value="'.$candidate_details->location.'">
//                         </label>
//                     </td>
//                     <td>
//                         <div class="form-group required">
//                             <select name="relocation[]" class="form-control">
                                
//                                 <option value="Yes" selected=""> Yes</option>
//                                 <option value="No"> No </option>
//                                 <option value="Other">Other  </option>
//                             </select>
//                         </div>
//                     </td>
//                     <td>
//                         <div class="form-group required">
//                             <input type="date" name="doj[]" id="" class="form-control" value="'.$candidate_details->doj.'">
//                         </div>
//                     </td>
//                     <td>
//                         <div class="form-group">
//                             <textarea name="remark[]" class="form-control">                                '.$candidate_details->remark.'
//                             </textarea>
//                         </div>
//                     </td>
        
                    
//                 </tr>';

// }


    }
     public function viewupdatespocstatus(Request $request){

        // $id = ($id);
         $id = $request['key'];
         $candidate_details = DB::table('tbl_recruiter_cv')
        ->select('*')
        ->where('id',$id)
        ->first();

        $cid=$candidate_details->cv_status;
         if($cid=='1')
            {
              $sel1='selected';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';
            }
             if($cid=='2')
            {
              $sel1='';$sel2='selected';$sel3='';$sel4='';$sel5='';$sel6='';
            }
            if($cid=='3')
            {
              $sel1='';$sel2='';$sel3='selected';$sel4='';$sel5='';$sel6='';
            }
            if($cid=='4')
            {
              $sel1='';$sel2='';$sel3='';$sel4='selected';$sel5='';$sel6='';
            }
            if($cid=='5')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='selected';$sel6='';
            }
            if($cid=='6')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='selected';
            }
        $rid=$candidate_details->recruiter_id;
         $html = ''; 
           $html .= '<option value="'.$candidate_details->id.'/'.$rid.'/1" '.$sel1.'>Not Seen</option>
              <option value="'.$candidate_details->id.'/'.$rid.'/2" '.$sel2.'>Send To Client</option>
              <option value="'.$candidate_details->id.'/'.$rid.'/3" '.$sel3.'>Rejected</option>
              <option value="'.$candidate_details->id.'/'.$rid.'/4" '.$sel4.'>Offer</option>
              <option value="'.$candidate_details->id.'/'.$rid.'/5" '.$sel5.'>Pipeline</option>
              <option value="'.$candidate_details->id.'/'.$rid.'/6" '.$sel6.'>Join</option>';
//echo $id.'----'.$rid.'---'.$cid;
echo $html;
 //echo ($candidate_details->id);
    // $fid =($fid);


}

        public function view(Request $request){

        // $id = ($id);
        $id = $request['key'];
    // $fid =($fid);


    $candidate_details = DB::table('tbl_recruiter_cv')
        ->select('*')
        ->where('id',$id)
        ->get();
 echo ($candidate_details);
        //dd($candidate_details);
			echo '<tr>
                 <th>Name</th>
                <th>Phone No</th>
                <th>Email</th>
                <th>Upload Resume</th>
                <th>Highest Qualificatione</th>
                <th>Total Exp</th>
                <th>Domain Exp</th>
                <th>Primary Skill</th>
                <th>Secondary Skill</th>
                <th>Current ORG</th>
                <th>Current CTC</th>
                <th>Expected CTC</th>
                <th>Reason For Change</th>
                <th>Communication Skills Rating</th>
                <th>NP</th>
                <th>Designation</th>
                <th>Location</th>
                <th>Relocation</th>
                <th>Expected DOJ</th>
                <th>Remarks</th>
            	</tr>';


                foreach ($candidate_details as $key => $candidate_details) {
                  $string  = "'".$candidate_details->candidate_name."'";

            echo '<tr class="table-active entry">
   
                    <td>
                        <div class="form-group required">


                            <input type="text" pattern="[A-Za-z\s]+" required="" name="candidate_name[]" class="form-control" placeholder="Enter First Name" title="First Name should only contain  letters. e.g. John" maxlength="60" value="'.$candidate_details->candidate_name.'">
                        </div>
                    </td>

                    <td>
                        <div class="form-group required">
                            <input type="tel" name="candidate_mob[]" pattern="^\d{10}$" required="" class="form-control" value="'.$candidate_details->candidate_mob.'">
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="text" required="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50" name="candidate_email[]" class="form-control" value="'.$candidate_details->candidate_email.'">
                        </div>
                    </td>
                    <td>
                    	<div class="form-group required">
                            <input type="text" required="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title="The input is not a valid email address" maxlength="50" name="candidate_email[]" class="form-control" value="'.$candidate_details->cv_name.'">
                        </div>
                       
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="text" name="highest_qualificatione[]" required="" class="form-control" value="'.$candidate_details->highest_qulification.'">
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="tel" name="experience[]" required="" class="form-control" maxlength="10" value="'.$candidate_details->total_exp.'">
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="text" name="domaine_exp[]" required="" class="form-control" value="'.$candidate_details->domain_exp.'" maxlength="50">
                        </div>
                    </td>
                    <td>
                        <label class="custom-file ih">
                   
                             <input type="text" name="primary_skill[]" required="" class="form-control" value="'.$candidate_details->primary_skill.'" maxlength="50">
                        </label>
                    </td>
                    <td>
                        <label class="custom-file ih">
                             <input type="text" name="secondary_skill[]" required="" class="form-control" value="'.$candidate_details->secondary_skill.'" maxlength="50">
                            
                        </label>
                    </td>

                    <td>
                        <div class="form-group required">
                            <input type="text" required="" name="current_org[]" class="form-control" value="'.$candidate_details->current_org.'" maxlength="60">
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="text" required="" name="current_ctc[]" class="form-control" value="'.$candidate_details->current_ctc.'">
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="text" required="" name="expected_ctc[]" class="form-control" value="'.$candidate_details->expected_ctc.'">
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="text" required="" name="reason_for_change[]" class="form-control" value="'.$candidate_details->reason_for_change.'">
                        </div>
                    </td>
                     <td>
                        <div class="form-group required">
                            <select name="communication_skills_rating[]" class="form-control">
                                <option value="1" selected=""> 1 </option>
                                <option value="2"> 2 </option>
                                <option value="3"> 3 </option>
                                <option value="4"> 4 </option>
                                <option value="5"> 5 </option>
                            </select>
                        </div>
                    </td>
                    <td>
                        <label class="custom-file ih">
                        <input type="text" name="np[]" required="" class="form-control" value="'.$candidate_details->np.'">
                        </label>
                    </td>
                    <td>
                        <label class="custom-file ih">
                        <input type="text" name="desng[]" required="" class="form-control" value="'.$candidate_details->desng.'">
                        </label>
                    </td>
                    <td>
                        <label class="custom-file ih">
                        <input type="text" name="location[]" required="" class="form-control" value="'.$candidate_details->location.'">
                        </label>
                    </td>
                    <td>
                        <div class="form-group required">
                            <select name="relocation[]" class="form-control">
                                
                                <option value="Yes" selected=""> Yes</option>
                                <option value="No"> No </option>
                                <option value="Other">Other  </option>
                            </select>
                        </div>
                    </td>
                    <td>
                        <div class="form-group required">
                            <input type="date" name="doj[]" id="" class="form-control" value="'.$candidate_details->doj.'">
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <textarea name="remark[]" class="form-control">                                '.$candidate_details->remark.'
                            </textarea>
                        </div>
                    </td>
        
                    
                </tr>';

}


}








        public function ajaxviewallassignedrecuiterposition(Request $request)
        {
            $key = $request['key'];

            $pos= DB::table('tbl_clientjd_master as c')
                ->join('tbl_clientjdrecruitermap as m','c.clientjob_id','=','m.fk_jdid')
                ->join('users as u','u.id','=','m.fk_empid')
                ->join('tbl_clients as cm','cm.client_id','=','c.clientjob_compid')
                ->select('m.*','c.*','cm.comp_name',DB::raw('group_concat(u.name) as name'))
                ->where('clientrequpdate_status','=',1)
                ->Where('c.clientjob_title', 'like', '%'.$key.'%')
                ->orWhere('c.clientjob_noofposition', 'like', '%'.$key.'%')
                ->orWhere('cm.comp_name', 'like', '%'.$key.'%')
                ->orWhere('u.name', 'like', '%'.$key.'%')
                ->groupBy('c.clientjob_id')
                ->get();

           // $pos = "select "

                print_r($pos);
                $i=1;   

                            foreach($pos as $key => $p){
                            echo' <tr class="table-active" >
                                    <td>'.$i.'</td>
                                    <td>'.$p->clientjob_title.'</td>
                                    <td>'.$p->comp_name.'</td>
                                    <td>N/A</td>
                                    <td>'.$p->clientjob_noofposition.'</td>
                                    <td>'.$p->name.'</td>
                                    <td>
                                        '.$p->clientreq_id.'
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal" onclick="getrecruiterlist('.$p->fk_jdid.');getclientreq_id('.$p->clientreq_id.')">Assign</button> | 
                                          <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModalUnassigned" onclick="getrecruiterunassignedlist('.$p->fk_jdid.')"UnAssign</button>
                                     
                                    </td>

                                </tr>';
                                $i++;
                            }



        }




    // public function navigation(){
    //   // $assigneeid=Auth::user()->emp_id;
    
    // $roles= Auth::user()->emp_role;
    // // print_r($roles);
    // // $emp_role = explode(',',$roles);
    // // print_r($emp_role);die;

    // $sql = "select m.menuname,m.menuparentid,m.url,m.id from tbl_menu as m inner join tbl_menu_role_assigned as ma on m.id=ma.menuid where ma.roleid IN($roles) GROUP BY m.menuname";
    // dd($sql);

    // $menu=DB::select(DB::raw($sql));

    //      dd($menu);
    //   }
        


    }