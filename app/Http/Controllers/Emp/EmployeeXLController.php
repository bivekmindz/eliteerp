<?php

namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use PHPExcel_IOFactory;
//use PHPExcel_Settings;
use App\Exceptions\ApplicationException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Model\User;
use App\Model\Role;
use App\Model\Client;
use App\Model\Adminmodel\Department as Dept;
use Session;
use DB;
use File;
//require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;






class EmployeeXLController extends Controller
{
    public function index()
    {

        $dept = Dept::all();
        return view('Emp.empXl',compact('dept'));
    }
    //
    public function exceldown()
    {
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setCreator('Maarten Balliauw')
            ->setLastModifiedBy('Maarten Balliauw')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('Test result file');

        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Company Name')
            ->setCellValue('B1', 'Contact Person')
            ->setCellValue('C1', 'Contact Number')
            ->setCellValue('D1', 'Email')
            ->setCellValue('E1', 'Designation')
            ->setCellValue('F1', 'Company URL')
            ->setCellValue('G1', 'Company Size')
            ->setCellValue('H1', 'Timings')
            ->setCellValue('I1', 'Days Working')
            ->setCellValue('J1', 'Industry')
            ->setCellValue('K1', 'Company’s Specialties')
            ->setCellValue('L1', 'Headquarters')
            ->setCellValue('M1', 'Office Address');



        $spreadsheet->getActiveSheet()->setTitle('Client Excel');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $spreadsheet->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="ClientExcel.xls"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');
        exit;


    }
    
   public function excelup(Request $request)
    {
        if(!isset($request['upfile'])){
            Session::flash('error_msg', 'Please select excel file to upload.');
            return redirect()->route('client-excel');
        }
        
        if(isset($request['upfile']))
        {
           $extension = File::extension($request['upfile']->getClientOriginalName());
           if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
              //'Your file is a valid xls or csv file'
           }else {
              Session::flash('error_msg', 'Please select excel file (xlsx,xls,csv) to upload.');
              return redirect()->route('client-excel');
           }
        }

        $assigneeid = Auth::user()->id;
       
        $inputFileName = $request['upfile'];
       
        $helper = new Sample();
      
        $spreadsheet = IOFactory::load($inputFileName);
      
         $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
         //dd($sheetData);
        $excel_row = array_shift($sheetData);
        // dd($excel_row);

        foreach ($sheetData as $key => $value)
        {
          //  dd($value['D']);
            $clients = DB::table('tbl_clients')
                        ->select('client_id')
                        ->where(['email'=>$value['D']])
                        ->get();
                        dd($clients);

            if($clients->isEmpty())
            {
                 
                DB::table('tbl_clients')
                    ->insert([
                        'comp_name' => $value['A'],
                        'contact_name' => $value['B'],
                        'phone' => $value['C'],
                        'email' => $value['D'],
                        'designation' => $value['E'],
                        'company_url' => $value['F'],
                        'company_size' => $value['G'],
                        'timings' => $value['H'],
                        'days_working' => $value['I'],
                        'industry' => $value['J'],
                        'company_specialties' => $value['K'],
                        'headquarters' => $value['L'],
                        'office_address' => $value['M'],
                        'clientsuid' => $assigneeid,
                        'client_catid' => $request['catid'],
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now(),
                        'client_catid' => $request['catid'],
                ]);
                //dd($abc);
            }
        }
        // $client=new Client();

        // foreach ($sheetData as $key=>$value)
        // {

        //     $client->comp_name=$value['A'];
        //     $client->contact_name=$value['B'];
        //     $client->phone=$value['C'];
        //     $client->email=$value['D'];
        //     $client->designation=$value['E'];
        //     $client->save();

        // }

        Session::flash('success_msg', 'Excel Uploaded  Successfully!');
        return redirect()->route('client-excel');
    }
//
    

    public function excelup1(Request $request)
    {
        if(!isset($request['upfile'])){
            Session::flash('error_msg', 'Please select excel file to upload.');
            return redirect()->route('client-excel');
        }
        
        if(isset($request['upfile']))
        {
           $extension = File::extension($request['upfile']->getClientOriginalName());
           if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
              //'Your file is a valid xls or csv file'
           }else {
              Session::flash('error_msg', 'Please select excel file (xlsx,xls,csv) to upload.');
              return redirect()->route('client-excel');
           }
        }

        $assigneeid = Auth::user()->id;
        $inputFileName = $request['upfile'];
        //dd( $inputFileName);
        $helper = new Sample();
        //dd($helper);
        $helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
        $spreadsheet = IOFactory::load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $excel_row = array_shift($sheetData);
        // dd($sheetData);

        foreach ($sheetData as $key => $value)
        {
            $clients = DB::table('tbl_clients')
                        ->select('client_id')
                        ->where('email',$value['D'])
                        ->get();

            if($clients->isEmpty()){
                DB::table('tbl_clients')
                    ->insert([
                        'comp_name' => $value['A'],
                        'contact_name' => $value['B'],
                        'phone' => $value['C'],
                        'email' => $value['D'],
                        'designation' => $value['E'],
                        'company_url' => $value['F'],
                        'company_size' => $value['G'],
                        'timings' => $value['H'],
                        'days_working' => $value['I'],
                        'industry' => $value['J'],
                        'company_specialties' => $value['K'],
                        'headquarters' => $value['L'],
                        'office_address' => $value['M'],
                        'clientsuid' => $assigneeid,
                        'client_catid' => $request['catid'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'client_catid' => $request['catid'],
                ]);
            }
        }
        // $client=new Client();

        // foreach ($sheetData as $key=>$value)
        // {

        //     $client->comp_name=$value['A'];
        //     $client->contact_name=$value['B'];
        //     $client->phone=$value['C'];
        //     $client->email=$value['D'];
        //     $client->designation=$value['E'];
        //     $client->save();

        // }

        Session::flash('success_msg', 'Excel Uploaded  Successfully!');
        return redirect()->route('client-excel');
    }
//




    public function employeexl(){

        return view('Emp.empXl');
    }



    public function empxlupload( Request $request ){

        $master_array_key = [
            'seller_id' => null,
            'rate_card_title' => 'required',
            'valid_from' => 'required',
            'valid_to' => 'required',
            'terms_condition' => null,
            'post_type' => null,
            'overwrite' => null,
            'user_time_zone' => null
        ];


        if (!$request->hasFile('uploadFile')) {

            //   throw new ApplicationException([], ["uploadFile needs to be specified"]);
        }

        //$path = $request->file;

        $path = $request->file('file');
        //Load Excel
        //$objPHPExcel = PHPExcel_IOFactory::load($file);


        PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);

        $objPHPExcel = PHPExcel_IOFactory::load($path);

        $masterSheet = $objPHPExcel->getSheet(0);
        //    echo "<pree>";
        //print_r($masterSheet);
        //echo "</pre>";
        $topRow = $masterSheet->getHighestRow();
        $topColumn = $masterSheet->getHighestColumn();
        $master = [];
        for ($row = 1; $row <= $topRow; ++$row) {
            $masterRows = $masterSheet->rangeToArray('A' . $row . ':' . $topColumn . $row, NULL, TRUE, FALSE);

            array_push($master, $masterRows[0]);
        }


        foreach ($master as $key => $value) {

            $client = new Client();
            $client->comp_name=$value[1];
            $client->contact_name=$value[2];
//$client->phone=$value[4];
            $client->email=$value[5];
            $client->save();
        }

        return redirect("employeexl")->with("status","File Upload Successfully.");
    }


    public function userloyeexl(){
        return view('Emp.userXl');
    }

    public function userxlupload( Request $request ){

        $path = $request->file('file');
        PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
        $objPHPExcel = PHPExcel_IOFactory::load($path);
        $masterSheet = $objPHPExcel->getSheet(0);
        $topRow = $masterSheet->getHighestRow();
        $topColumn = $masterSheet->getHighestColumn();
        $master = [];
        for ($row = 1; $row <= $topRow; ++$row) {
            $masterRows = $masterSheet->rangeToArray('A' . $row . ':' . $topColumn . $row, NULL, TRUE, FALSE);

            array_push($master, $masterRows[0]);
        }
        foreach ($master as $key => $value) {
            $user = new User();
            $user->name=$value[0];
// $user->emp_contactno=$value[4];
            $user->email = $value[3];
//$user->password=$value[5];
            $user->emp_doj=date('Y-m-d',strtotime($value[2]));
//$user->emp_dept=$value[1];
            $user->emp_role=$value[5];
            $user->save();
        }
        return redirect("employeexl")->with("status","File Upload Successfully.");

    }






    public function save(Request $request)
    {
        //echo 'hi'; die;

//         $this->validate($request,[
//             'cname' => 'required',
//             'cpn' => 'required',
//             'email' => 'required|email|unique:users',
//             'phone' => 'required|numeric',
//             'deptid' => 'required',
//             'desig' => 'required'
//         ],[
//             'cname.required' => 'Company name field is required.',
//             'cpn.required' => 'Contact person name is required',
//             'email.required' => 'Email field is required',
//             'phone.required' => 'Phone number field is required.',
// //            'phone.max'=>'Phone number field must be of 10 digit only',
//             'desig.required' => ' Designation field is required',
//             'deptid.required' => ' Select Category',
//         ]);



        $postData = $request->all();
        print_r($postData); die;
        $client_obj= new Client;
        $client_obj->client_catid=$request['deptid'];
        $client_obj->comp_name = $request['cname'];
        $client_obj->phone=$request['phone'];
        $client_obj->contact_name=$request['cpn'];
        $client_obj->email=$request['email'];
        $client_obj->designation=$request['desig'];
        //  dd($client_obj);
        $client_obj->save();

        Session::flash('success_msg', 'Client Added Successfully!');

        return redirect()->route('newclient');

    }


    
    
 public function myallocatedexceldownload()
 {
//print_r($_POST); exit;
        
     $assigneeid=Auth::user()->id;
       $users=DB::table('tbl_recruiter_cv as trc')
                ->select('trc.cv_name','trc.id', 'trc.candidate_name', 'trc.candidate_email', 'trc.candidate_mob', 'trc.cv_status','tc.tca_followupstatus','tc.tca_id','trc.position_id','trc.recruiter_id')
                  ->join('tbl_cv_allocated as tc', 'trc.id', '=', 'tc.tca_cvid')
                ->where('trc.recruiter_id',$_POST['recruiterId'])
               //  ->where('tc.tca_assignto',$recruiterId)
                ->where('trc.position_id', '=', $_POST['clientid'])
                //  ->where('trc.cv_status', '=', '22')
                  ->where('tc.tca_followupstatus', '!=', 'Not Going')
                ->get();
            
            
            
          //  dd($users);


    //-----------Va Define-----------------------//
     $allArr               = array();
     $templevel            = 0;  
     $newkey               = 0;
     $grouparr[$templevel] = "";
  //-----------------------------------------------//
/* $role=['BD','SPOC','REC','HR'];*/

    $response['array_test']  = array(
       '7'=>'On The Way',
       '8'=>'Interviewed',
        '9'=>'Not Going',
         '17'=>'Not Responding',
          '18'=>'Confirmed',
           '23'=>'Dicey',
       '24'=>'Reached',
      
      );
    foreach($response['array_test'] as $key => $value){
     $fuelID[] = $key;
     $fuelName[] = $value;
    //p($fuelName); exit;
   }
      $fuelProID = implode(',',$fuelID);
       $fuelProName = implode(',',$fuelName);

       $featurevaluee1[]    = 'test';
 
      $excelHeadArrqq1 = array_merge($featurevaluee1);
      $excelHeadArr1 = array_unique($excelHeadArrqq1);
      
      $oldfinalAttrData['test']       = $fuelProName;
      $oldfinalAttrData2['test']      = $fuelName;

       $finalAttrData  = array_merge($oldfinalAttrData);
       $finalAttrData2 = $oldfinalAttrData2;
       $attCount = array();
       foreach($finalAttrData as $k=>$v){
       $valueCount   = count(explode(",", $v));
       $attCount[$k] = $valueCount+1;

      } 
             $objPHPExcel   = new PHPExcel();
    //-------------------------------- First defult Sheet -------------------------------------------------//
    $objWorkSheet  = $objPHPExcel->createSheet();
    $objPHPExcel->setActiveSheetIndex(0);

    $finalExcelArr1 = array_merge($excelHeadArr1);
//p($finalAttrData2);exit;
     $cols   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');
        $j      = 2;
       
        for($i=0;$i<count($finalExcelArr1);$i++){
         $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr1[$i]);
         foreach ($finalAttrData2 as $key => $value) {
          foreach ($value as $k => $v) {

            if($key == $finalExcelArr1[$i]){
            $newvar = $j+$k;
            $objPHPExcel->getActiveSheet()->setCellValueExplicit($cols[$i].$newvar, $v, PHPExcel_Cell_DataType::TYPE_STRING);
            }
        }
      }  
    }
           $arrPart2 = array('Action','Id','CandidateName','Email','Mobile','FollowUp Status','Recuiterid');
          if(!empty($excelHeadArr)){
             $finalExcelArr = array_merge($arrPart2,$excelHeadArr);
         }else{
            $finalExcelArr = array_merge($arrPart2);
         }
        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->setTitle('VendorProductWorksheet#');
      
    $cols  = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ', 'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ');
 
 //Set border style for active worksheet
for($i=0;$i<count($finalExcelArr);$i++){
                    //Set width for column head.
            $objPHPExcel->getActiveSheet()->getColumnDimension($cols[$i])->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->setCellValue($cols[$i].'1', $finalExcelArr[$i]);
   foreach ($users as $key => $value) {
             $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);
      $newvar = $j+$key;
      $objPHPExcel->getActiveSheet()->getRowDimension($newvar)->setRowHeight(20);

      $objPHPExcel->getActiveSheet()->setCellValue($cols[1].$newvar, $value->id);
      $objPHPExcel->getActiveSheet()->setCellValue($cols[2].$newvar, $value->candidate_name);
    $objPHPExcel->getActiveSheet()->setCellValue($cols[3].$newvar, $value->candidate_email);
        $objPHPExcel->getActiveSheet()->setCellValue($cols[4].$newvar, $value->candidate_mob);
        $objPHPExcel->getActiveSheet()->setCellValue($cols[5].$newvar, $value->tca_followupstatus);
          $objPHPExcel->getActiveSheet()->setCellValue($cols[6].$newvar, $assigneeid);

          for($k=2;$k <1000;$k++)
          {
 $objPHPExcel->getActiveSheet()->getRowDimension($k)->setRowHeight(20);
    $objValidation24 = $objPHPExcel->getActiveSheet()->getCell('A2')->getDataValidation();
 
    $objValidation24->setType(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::TYPE_LIST);
    $objValidation24->setErrorStyle(\PhpOffice\PhpSpreadsheet\Cell\DataValidation::STYLE_INFORMATION);
    $objValidation24->setAllowBlank(false);
    $objValidation24->setShowInputMessage(true);
    $objValidation24->setShowErrorMessage(true);
    $objValidation24->setShowDropDown(true);
    $objValidation24->setErrorTitle('Input error');
    $objValidation24->setError('Value is not in list.');
    $objValidation24->setPromptTitle('Pick from list');
    $objValidation24->setPrompt('Please pick a value from the drop-down list.');
    $objValidation24->setFormula1('Worksheet!$'.'A$2:$'.'A$'.($attCount['test']));
    $objPHPExcel->getActiveSheet()->getCell('A'.$k)->setDataValidation($objValidation24);
  
  }//secfor


}



}

$filename  = "viewmyallocatedcvdetail.xls";
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5'); 
      //  ob_end_clean();
      //  ob_start();
        $objWriter->save('php://output');
        
        die("a");
        exit;
        echo "fffffffffffffffff";
}


   public function myallocatedexcelup(Request $request)
    {
       
      //  print_r($_POST); exit;
        if(!isset($request['upfile'])){
            Session::flash('error_msg', 'Please select excel file to upload.');
            return redirect()->route('empexcel');
        }
        
        if(isset($request['upfile']))
        {
           $extension = File::extension($request['upfile']->getClientOriginalName());
           if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
              //'Your file is a valid xls or csv file'
           }else {
              Session::flash('error_msg', 'Please select excel file (xlsx,xls,csv) to upload.');
              return redirect('viewmyallocatedcvdetail/'.$_POST['nidd']);
           }
        }


        $inputFileName = $request['upfile'];
        $helper = new Sample();
       // $helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
        $spreadsheet = IOFactory::load($inputFileName);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $excel_row = array_shift($sheetData);
        $m=0;
     //   dd($sheetData);
        foreach ($sheetData as $key=>$value)
        {
         
if($value['A']=='On The Way') {$statusval=7;}elseif($value['A']=='Interviewed') {$statusval=8;}
elseif($value['A']=='Not Going') {$statusval=9;}elseif($value['A']=='Not Responding') {$statusval=17;}
elseif($value['A']=='Confirmed') {$statusval=18;}elseif($value['A']=='Dicey') {$statusval=23;}
elseif($value['A']=='Reached') {$statusval=24;}

            
            
       // echo $value['A'];
        DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$value['B']])
                        ->update(['c.cv_status'=>$statusval]);
                        
                           DB::table('tbl_recruiter_cvchangestatus as c')
                        ->where(['c.cvid'=>$value['B']])
                        ->update(['c.cvupdatestatus'=>0]);
$id1=DB::table('tbl_recruiter_cvchangestatus')->insert(
                [
                'cv_status' =>$statusval ? $statusval : '' ,
                'cvid' => $value['B'] ? $value['B'] : '' ,
                'created_at' => date('Y-m-d H:i:s'),
                'loginid' => $value['G'] ? $value['G'] : "",   
                 'recuiterid' => $value['G'] ? $value['G'] : "",   
                   'cvupdatestatus' => '1',  
              
                 ]
            );
            
           
             /*$roleId = DB::table('tbl_leavetype as tr')
                         ->select('tr.role_id')
                        ->where('role_name',$value['A'])
                         ->first();
            //   dd($roleId);      */
               
             /*   $id1=DB::table('hrattendance')->insert(
                [
                'leavestatus' =>$value['A'] ? $value['A'] : '' ,
                'leaverecord' => $value['B'] ? $value['B'] : '' ,
                'empcode' => $value['C'] ? $value['C'] : "" ,
                'empname' => $value['D'] ? $value['D'] : "",   
                 'created_at' => date("Y-m-d"),   
              
                 ]
            );
            
               $id2 = DB::getPdo()->lastInsertId();*/
               
     /*  DB::table('tbl_cv_allocated as c')
                        ->where(['c.tca_cvid'=>$value['B']])
                        ->update(['c.tca_followupstatus'=>$value['A']]);  */      
             

     $m++;  
        }
        Session::flash('success_msg', 'Excel Uploaded  Successfully!');
         return redirect('viewmyallocatedcvdetail/'.$_POST['nidd']);
    }
    
    


}
