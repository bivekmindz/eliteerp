<?php
/**
 * Created by PhpStorm.
 * User: mindz
 * Date: 2/2/18
 * Time: 3:39 PM
 */

namespace App\Http\Controllers\Emp;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Mail;
use Storage;
use Carbon\Carbon;
use Session;



class CVController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //$assigneeid=Auth::user()->id;
    }

    public function editassignedpostion($id,$fid)
    { 
    $id = ($id);
    $fid =($fid);


    $candidate_details = DB::table('tbl_recruiter_cv')
        ->select('*')
        ->where('id',$id)
        ->get();
            
             // dd($candidate_details);
         // DIE('/////');
            
            //die('////////');
    return view('Emp.editassignedpostion',compact('candidate_details','fid'));
     }
     
      public function edituploadcv(Request $request)
     {
    // echo 111111111;
     $Id = $request->id;
     $position_id = $request->posid;
      $recruiterId = Auth::user()->id;
      $recruiterMapId = $request->rmapid;
     $encryptId = $request->EncryptId;
    
    if($request['cv_name']!=''){

       // echo 11111; 
    $file = $request->file('cv_name');
    

    for($i=0; $i<count($request->candidate_name); $i++) {

        $candidateName = $request->candidate_name[$i];
        $candidateMob = $request->candidate_mob[$i];
        $candidateEmail = $request->candidate_email[$i];
        //  Ashish 
        $cvuploadtime = $request->filetime[$i];
// echo "<pre>"; print_r($cvuploadtime);die;

        $highest_qualificatione = $request->highest_qualificatione[$i];
        $total_exp = $request->experience[$i];
        $domain_exp = $request->domaine_exp[$i];
        $primary_skill = $request->primary_skill[$i];
        $secondary_skill = $request->secondary_skill[$i];
        $current_org = $request->current_org[$i];
        $current_ctc = $request->current_ctc[$i];
        $expected_ctc = $request->expected_ctc[$i];
        $np = $request->np[$i];
         $desng = $request->desng[$i];
        $location = $request->location[$i];
        $reason_for_change = $request->reason_for_change[$i];
        $communication_skills_rating = $request->communication_skills_rating[$i];
        $relocation = $request->relocation[$i];
        $doj = $request->doj[$i];
        $remark = $request->remark[$i];
        //
        $date = new \DateTime();
        //die;

        $candidateCv = 'uploadcv';
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $cv_uploaded_date = $year.'/'.$month.'/'.$day;

        $dirStorage = DIRECTORY_SEPARATOR . $candidateCv . DIRECTORY_SEPARATOR . $year . DIRECTORY_SEPARATOR
            . $month . DIRECTORY_SEPARATOR . $day;

        Storage::disk('local')->makeDirectory($dirStorage);
        $fileName = uniqid() . "-" . str_replace(" ", "-", $request->file('cv_name')[$i]->getClientOriginalName());
        $filepath = $dirStorage . DIRECTORY_SEPARATOR . $fileName;
        Storage::disk('local')->put($filepath,file_get_contents($request->file('cv_name')[$i]->getRealPath()));
        /**JD upload starts*/


        $sql = DB::table('tbl_recruiter_cv')
 ->where('id',$Id)
 ->update([
            'recruiter_id'=>$recruiterId,
            'r_map_id'=>$recruiterMapId,
            'position_id'  => $position_id,
            'cv_name' => $fileName,
            'cv_uploadtime' =>$cvuploadtime,
            'cv_uploaded_date' => $cv_uploaded_date,
            'candidate_name'=>$candidateName,
            'candidate_mob'=>$candidateMob,
            'candidate_email'=>$candidateEmail,
            'highest_qulification'=>$highest_qualificatione,
            'total_exp'=>$total_exp,
            'domain_exp'=>$domain_exp,
            'primary_skill'=>$primary_skill,
            'secondary_skill'=>$secondary_skill,
            'current_org'=>$current_org,
            'current_ctc'=>$current_ctc,
            'expected_ctc'=>$expected_ctc,
            'np'=>$np,
            'desng'=>$desng,
            'location'=>$location,
            'reason_for_change'=>$reason_for_change,
            'communication_skills_rating'=>$communication_skills_rating,
            'relocation'=>$relocation,
            'doj'=>$doj,
            'remark'=>$remark,
            'updated_at'=>date('Y-m-d H:i:s'),
       

    ]);
      //  dd("zzzzzzz");
      
        //dd(DB::getQueryLog());
        
     
                /*-----------------------------------------------*/
        $url = 'http://115.124.98.243/~logistiks/solrApi/uploadfile.php';
        $merchantname = 'techproducts';
        $fileid = $sql;

           // Make sure there are no upload errors
            if ($_FILES['cv_name']['error'][0] > 0)
            {
                die("Error uploading file...");
            }
         
            // Prepare the cURL file to upload, including file name and MIME type
            $post = array(
            'file_contents' => new \CurlFile($_FILES["cv_name"]["tmp_name"][0], $_FILES["cv_name"]["type"][0],$_FILES["cv_name"]["name"][0]),
            );
            //$fileposts=array('merchantname'=>$merchantname,'fileid'=>$fileid);
            $fileposts=array('merchantname'=>$merchantname,'fileid'=>$fileid,'experience'=>$total_exp,'salary'=>$current_ctc,'location'=>$location,'designation'=>$desng,'company'=>$current_org,'primaryskill'=>$primary_skill,'secondaryskill'=>$secondary_skill);

            // Include the other $_POST fields from the form?
            $post = array_merge($post, $fileposts);
         
            // Prepare the cURL call to upload the external script
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:54.0) Gecko/20100101 Firefox/54.0");
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            $result = curl_exec($ch);
            curl_close($ch);
         
            // print_r(json_decode($result));
        /*------------------------------------------------*/
    }
}
else{


    // echo "<pre>"; print_r($request->all());

   // echo 2222222222222;

        $candidateName = $request->candidate_name;
        // print_r($candidateName[0]);
        $candidateMob = $request->candidate_mob;
        $candidateEmail = $request->candidate_email;
        //  Ashish 
        $cvuploadtime = $request->filetime;
// echo "<pre>"; print_r($cvuploadtime);die;

        $highest_qualificatione = $request->highest_qualificatione;
        $total_exp = $request->experience;
        $domain_exp = $request->domaine_exp;
        $primary_skill = $request->primary_skill;
        $secondary_skill = $request->secondary_skill;
        $current_org = $request->current_org;
        $current_ctc = $request->current_ctc;
        $expected_ctc = $request->expected_ctc;
        $np = $request->np;
        $reason_for_change = $request->reason_for_change;
        $communication_skills_rating = $request->communication_skills_rating;
        $relocation = $request->relocation;
        $doj = $request->doj;
        $remark = $request->remark;
        //
        $date = new \DateTime();

$data = [
            'recruiter_id'=>$recruiterId,
            'r_map_id'=>$recruiterMapId,
            'position_id'  => $position_id,
            // 'cv_name' => $fileName,
            // 'cv_uploadtime' =>$cvuploadtime,
            // 'cv_uploaded_date' => $cv_uploaded_date,
            'candidate_name'=>$candidateName[0],
            'candidate_mob'=>$candidateMob[0],
            'candidate_email'=>$candidateEmail[0],
            'highest_qulification'=>$highest_qualificatione[0],
            'total_exp'=>$total_exp[0],
            'domain_exp'=>$domain_exp[0],
            'primary_skill'=>$primary_skill[0],
            'secondary_skill'=>$secondary_skill[0],
            'current_org'=>$current_org[0],
            'current_ctc'=>$current_ctc[0],
            'expected_ctc'=>$expected_ctc[0],
            'np'=>$np[0],
            'reason_for_change'=>$reason_for_change[0],
            'communication_skills_rating'=>$communication_skills_rating[0],
            'relocation'=>$relocation[0],
            'doj'=>$doj[0],
            'remark'=>$remark[0],
           
            'updated_at'=>date('Y-m-d H:i:s'),
       

    ];
 // echo"<pre>";print_r($data);
 // die;

    $sql = DB::table('tbl_recruiter_cv')
 ->where('id',$Id)
 ->update($data);


}

    Session::flash('success_msg', 'Candidate resume uploaded. Successfully to start another position please close 
        this position');

        return redirect('get-assigned-postion/'.$encryptId);
         }



    public function shortlistcv()
    {
        $assigneeid=Auth::user()->id;
       // dd($assigneeid);
        $role = DB::table('users as u')
                   ->select('emp_role','id')
                   ->where('u.id','=',$assigneeid)
                   ->get();
               
        if($role[0]->emp_role != 4  and   $role[0]->emp_role != 7){
         /*  $pos = DB::table('tbl_recruiter_cv as c')
                    ->join('tbl_clientjd_master as p','p.clientjob_id','=','c.position_id')
                    ->join('users as us','us.id','=','p.clientjob_empid')
                     ->join('tbl_clients as cli','cli.client_id','=','p.clientjob_compid')
                    ->select('c.position_id','p.clientjob_title','p.clientjob_noofposition','cli.comp_name','us.name',DB::raw('COUNT(c.position_id) as total_upload_profile'))
                    ->where('p.clientjob_empid','=',$assigneeid)
                    ->groupBy('c.position_id','p.clientjob_title','p.clientjob_noofposition','us.name')
                    ->get();
                    */
                    
                  $pos = DB::table('tbl_recruiter_cv as c')
                    ->join('tbl_clientjd_master as p','p.clientjob_id','=','c.position_id')
                     ->join('tbl_clientjdrecruitermap as par','par.fk_jdid','=','c.position_id')
                    ->join('users as us','us.id','=','p.clientjob_empid')
                     ->join('tbl_clients as cli','cli.client_id','=','p.clientjob_compid')
                      ->select('c.position_id','p.clientjob_title','p.clientjob_noofposition','cli.comp_name','us.name',DB::raw('(select COUNT(id) from tbl_recruiter_cv where position_id=p.clientjob_id  ) as total_upload_profile'))
                ->where('p.clientjob_empid','=',$assigneeid)
                   ->orWhere('par.fk_assigneeid', '=', $assigneeid)
                    ->groupBy('c.position_id','p.clientjob_title','p.clientjob_noofposition','us.name')
                    ->get();
        }
     
        
        else{
            $pos = DB::table('tbl_recruiter_cv as c')
                    ->join('tbl_clientjd_master as p','p.clientjob_id','=','c.position_id')
                    ->join('users as us','us.id','=','p.clientjob_empid')
                     ->join('tbl_clients as cli','cli.client_id','=','p.clientjob_compid')
                    ->select('c.position_id','p.clientjob_title','p.clientjob_noofposition','cli.comp_name','us.name',DB::raw('COUNT(c.position_id) as total_upload_profile'))
                    // ->where('p.clientjob_empid','=',$assigneeid)
                    ->groupBy('c.position_id','p.clientjob_title','p.clientjob_noofposition','us.name')
                   //->having('p.clientjob_empid',$assigneeid)
                    ->get();
        }
        
        return view('Emp.shortlistcv',['pos'=>$pos]);

    }
    public function cvshortlisting(Request $request)
    {

        $key = $request['key'];
        $assigneeid=Auth::user()->id;
      
          $role = DB::table('users as u')
                   ->select('emp_role')
                   ->where('u.id','=',$assigneeid)
                   ->get();
               
        if($role[0]->emp_role != 4){


                $pos = "select c.position_id, p.clientjob_title, p.clientjob_noofposition, cli.comp_name, us.name, (select COUNT(*) from tbl_recruiter_cv where position_id=p.clientjob_id  ) as total_upload_profile from tbl_recruiter_cv as c inner join tbl_clientjd_master as p on p.clientjob_id = c.position_id inner join tbl_clientjdrecruitermap as par on par.fk_jdid = c.position_id inner join users as us on us.id = p.clientjob_empid inner join tbl_clients as cli on cli.client_id = p.clientjob_compid where p.clientjob_empid = ".$assigneeid." or par.fk_jdid = ".$assigneeid." group by c.position_id, p.clientjob_title, p.clientjob_noofposition, us.name";

           
        }else{
              $pos = "select c.position_id, p.clientjob_title, p.clientjob_noofposition, cli.comp_name, us.name, (select COUNT(*) from tbl_recruiter_cv where position_id=p.clientjob_id  ) as total_upload_profile from tbl_recruiter_cv as c inner join tbl_clientjd_master as p on p.clientjob_id = c.position_id inner join tbl_clientjdrecruitermap as par on par.fk_jdid = c.position_id inner join users as us on us.id = p.clientjob_empid inner join tbl_clients as cli on cli.client_id = p.clientjob_compid group by c.position_id, p.clientjob_title, p.clientjob_noofposition, us.name";

        }
       // dd( $pos );


      /*  $pos = "select m.*, c.comp_name from tbl_clientjd_master as m inner join tbl_clients as c on m.clientjob_compid = c.client_id where clientjob_empid = '".$assigneeid."' and (m.clientjob_title like '%".$key."%' or c.comp_name like '%".$key."%' or m.clientjob_noofposition like '%".$key."%' or m.experience like '%".$key."%' or m.domain_experience like '%".$key."%' or m.secondary_skills like '%".$key."%')";*/

        $pos = DB::select(DB::raw($pos));



      //  print_r($pos)
/*
<button type="button" value="{{ $value->position_id  }}" class="det_css one-click{{ $value->position_id  }}" onclick="getcandidatedetails{{ $value->position_id  }}(this.value),getposition('<?php echo $value->clientjob_title ?>')" ><span class="icofont icofont-eye-alt"></span></button>
*/



                foreach ($pos as $key => $value) {
                  $string  = "'".$value->clientjob_title."'";
                //  echo "hiii";
                  echo  '<tr >
                        <td>1</td>
                        <td>'.$value->clientjob_title.'</td>
                        <td>'.$value->comp_name.' </td>
                        <td>'.$value->clientjob_noofposition.'</td>
                        <td>'.$value->name.'Year</td>
                        <td>'.$value->total_upload_profile.' Year</td>
                    
                        <td>
                              sss                          
                            <button type="button" value='.$value->position_id.'  class="det_css one-click'.$value->position_id.'"   onclick="getcandidatedetails'.$value->position_id.'(this.value),getposition('.$value->clientjob_title.')"  style="float: none;margin: 5px;">
                           <span class="icofont icofont-eye-alt"></span>
                            </button>
                           
                           
                        </td>
                        </tr>
                        ';
            }
    }

    public function getcandidatedetails($id)
    {
       // echo $id;

        $assigneeid=Auth::user()->id;
       // echo $assigneeid;
      /*  $candidate=DB::table('tbl_recruiter_cv')
            ->select('*')
            ->where(['position_id'=>$id])
            ->get();*/
             $candidate=DB::table('tbl_recruiter_cv as rcv')
            ->select('rcv.*','us.name as username')
             ->join('users as us','us.id','=','rcv.recruiter_id')
            ->where(['rcv.position_id'=>$id])
            
           ->orderBy('rcv.id', 'DESC')
            ->get();
            
         echo $candidate;

    }
    
    public function cvlist()
     {
     $assigneeid=Auth::user()->id;
       // // dd($assigneeid);
     $list= DB::table('tbl_recruiter_cv')->where('recruiter_id','=',$assigneeid)->get();
      // dd($list);
     return view('Emp.cvlist',['pos'=>$list]);
      //dd($pos);
    }

    public function cvupload(Request $request)
    {
      // echo $request->recruiter_cv_id;
      // die('/');
      $candidateCv = 'uploadcv';
      $year = date('Y');
      $month = date('m');
      $day = date('d');
      $dirStorage = DIRECTORY_SEPARATOR . $candidateCv . DIRECTORY_SEPARATOR . $year . DIRECTORY_SEPARATOR
          . $month . DIRECTORY_SEPARATOR . $day;

      Storage::disk('local')->makeDirectory($dirStorage);
      $fileName = uniqid() . "-" . str_replace(" ", "-", $request->file('cv_name')->getClientOriginalName());
      $filepath = $dirStorage . DIRECTORY_SEPARATOR . $fileName;
      Storage::disk('local')->put($filepath,file_get_contents($request->file('cv_name')->getRealPath()));

      DB::table('tbl_recruiter_cv')
                ->where('id', $request->recruiter_cv_id)
                ->update(['cv_name' => $fileName,
                          'updated_at'=> Carbon::now(),
                ]);

      Session::flash('success_msg', 'Cv has been updated successfully!');
      return redirect()->route('cvlist');
    }
    
    public function savecandidate(Request $request)
    {
        
        
      $loginid = Auth::user()->id;
      foreach($request['checkbox_data'] as $kkk=>$vvv)
            {
              $candidate_recruiter_array = explode('/',$vvv);

DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$candidate_recruiter_array[0]])
                        ->update(['c.cv_status'=>$candidate_recruiter_array[2]]);



$val=$candidate_recruiter_array[0];
 $rec_email = DB::table('tbl_recruiter_cv as rc')
                       ->join('users as u', 'rc.recruiter_id', '=', 'u.id')
                        ->select('u.email', 'u.name')
                        ->where(['rc.recruiter_id' => $candidate_recruiter_array[1]])
                        ->first();
                      //  dd( $rec_email);
 
                    $complete_shortlist = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->where('rc.id', $candidate_recruiter_array[0])
                         ->where('rc.cv_status', '2')
                        // ->where(['rc.id'=>$v])
                        ->get();
                    //    dd(count($complete_shortlist) );
                      
                       /*   $complete_unshortlist = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->where('rc.id', $candidate_recruiter_array[0])
                         ->where('rc.cv_status', '3')
                        // ->where(['rc.id'=>$v])
                        ->get();*/
                   $complete_unshortlist=0;
if((count($complete_shortlist) )>0) {
                   $sendMail = $this->sendMail($complete_shortlist,$complete_unshortlist,$request['poid'],$loginid,$rec_email );
}




  /*DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$candidate_recruiter_array[0]])
                        ->update(['c.cv_status'=>$candidate_recruiter_array[2]]);*/
          


            }

 session::flash('success','Message Sent');
                return redirect()->route('shortlistcv');



    }//end of save candidate



    public  function merge_common_keys()
    {
        $arr = func_get_args();
        $num = func_num_args();

        $keys = array();
        $i = 0;
        for ($i=0; $i<$num; ++$i)
        {
            $keys = array_merge($keys, array_keys($arr[$i]));
        }
        $keys = array_unique($keys);

        $merged = array();

        foreach ($keys as $key)
        {
            $merged[$key] = array();
            for($i=0; $i<$num; ++$i)
            {
                $merged[$key][] = isset($arr[$i][$key]) ? $arr[$i][$key] : '';
            }
        }
        return $merged;
    }


    public  function sendMail($proSort=Null,$proUnsort=Null ,$reuest,$loginid,$rec_email)
    {

        $mail_data = [
            //'position' => $position,
            'profile' => $proSort,
            'unprofile' => $proUnsort,
            'position' => $reuest


        ];


      //  dd($mail_data);


        $from_email = DB::table('users  as u')
            ->select('u.email')
            ->where(['u.id' => $loginid])
            ->get();
         

        $from = $from_email[0]->email;
        
    //    $to = trim($rec_email->email," "); //'vijitha@mindztechnology.com';
          $to = [ trim($rec_email->email," "),'bivek@mindztechnology.com'];  
    
        $toname = $rec_email->name; //vijitha
       // $to = 'shalini29081991@gmail.com';
        //$toname = 'vijitha';
        
      //  dd($to);
         // dd($mail_data);

          Mail::send('Emp.mails.shortlistcvmail', ['mail_data' => $mail_data], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite erp');
            $message->to($to, $toname);
            $message->subject('Short Listed Profiles');

        });
       
    }

}