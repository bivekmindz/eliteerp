<?php
/**
 * Created by PhpStorm.
 * User: mindz
 * Date: 17/2/18
 * Time: 10:07 AM
 */

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use Mail;


class ReportController extends Controller
{
    
    
public function monsterfreekey()
{
    $getkey = DB::table('tbl_monsteractivitiesdays as md')
    ->select('md.*')
    ->where('md.md_status', '=','Active')
    ->get();

    if(count($getkey) < 1)
    {
        echo 'Not Found Busy Key!';
    }
    else
    {
        $now = Carbon::now()->toDateTimeString();
        $startTime = Carbon::parse($now);

        foreach ($getkey as $key => $value) 
        { 
            $finishTime = Carbon::parse($value->md_createdat);
            $totalDuration = $finishTime->diffInMinutes($startTime);
            #echo '<pre>';
            #print_r($totalDuration);
            if($totalDuration > 10)
            {
                 DB::table('tbl_monsteractivitiesdays')
                ->where('md_id', '=', $value->md_id)
                ->update([
                    'md_status'=>'Inactive'
                ]);

                 DB::table('tbl_monsteractivities')
                ->where('ma_id', '=', $value->md_ma_id)
                ->update([
                    'ma_status'=>'Inactive'
                ]);

                $msg='All Keys are free!';
            }
            else
            {
                $msg='Not Found Busy Key!';
            }
        }
        echo $msg;
    }
    
    #$totalDuration = $finishTime->diffForHumans($startTime); # Description Day
    #$totalDuration = $finishTime->diffInSeconds($startTime); # Seconds
    #$totalDuration = $finishTime->diffInMinutes($startTime); # Minutes
    #dd($totalDuration);
}


 public function lateentrymail()
    {   
     
    $fromdate=date('Y-m-d');
$todate=date('Y-m-d');
      $users1= ("select ats.lastpunch,ats.punchrecord ,  ats.empname , ats.empcode  from attendanceuser  as ats     where ( date(ats.created_at) between '".$fromdate."' and '".$todate."' ) and  ats.empcode NOT IN(502,503,303,236,305,308,532)");
 $data = DB::select(DB::raw($users1));
 //print_r($data);
  if((count($data)>0))
{ 
    

$to = ['team@mindztechnology.com','team@elitehrpractices.com'];
       $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
Mail::send('Emp.mails.lateentrymail',  ['mail_data' => $data], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Today Late Comers');

        });   
}

    }
 public function todayfollowupdriveresumestatusmail()
    {
     //  $start = date('Y-m-d');
  //$finish =  date('Y-m-d'); 
    $start = date('Y-m-d');
  $finish =  date('Y-m-d'); 
  
$query = 'select rc.id, cvall.tca_positionid,cm.*,cl.comp_name
,(select count(tca_id) from tbl_cv_allocated where  tca_positionid=rc.position_id ) as total_rec_cv 
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="7" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as OnTheWay
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="8" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as Interviewed
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="9" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as NotGoing
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="17" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as NotResponding
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="18" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as Confirmed
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="22" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as shortlistclient
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="23" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as Dicey
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="24" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as Reached
 

  from tbl_clientjd_master as cm join tbl_recruiter_cv as rc on rc.position_id=cm.clientjob_id
  join tbl_cv_allocated as cvall on cvall.tca_cvid=rc.id join tbl_clients as cl on cl.client_id=cm.clientjob_compid and (Date(cm.drive_time) between "'.$start.'" and "'.$finish.'" )  group by rc.position_id ';
   $clientposresumes = DB::select(DB::raw($query));
   
  if((count($clientposresumes)>0))
{ 
  

$to = ['bivek@mindztechnology.com','erp@elitehrpractices.com'];
       $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
Mail::send('Emp.mails.todayfollowupdriveresumestatusmail',  ['mail_data' => $clientposresumes], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Client call allocation Status');

        });    
}
  
   
    }
   
public function followupdriveresumemail()
    {
        // $start =date('Y-m-d');
//  $finish = date('Y-m-d');

    $start =date("Y-m-d");
    $date=date("Y-m-d");
  $finish = date('Y-m-d', strtotime('+1 day', strtotime($date)));
  
  
        
  $query = 'select rc.id, cvall.tca_positionid,cm.*,cl.comp_name ,(select count(tca_id) from tbl_cv_allocated where  tca_positionid=rc.position_id ) as total_rec_cv 
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Pending" and tca_positionid=rc.position_id and (Date(cvall.tca_createdon) between "'.$start.'" and "'.$finish.'" )) as pending
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Confirm" and tca_positionid=rc.position_id and (Date(cvall.tca_createdon) between "'.$start.'" and "'.$finish.'" )) as Confirm
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Not Going" and tca_positionid=rc.position_id and (Date(cvall.tca_createdon) between "'.$start.'" and "'.$finish.'" )) as NotGoing
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Not Reachable" and tca_positionid=rc.position_id and (Date(cvall.tca_createdon) between "'.$start.'" and "'.$finish.'" )) as NotReachable
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Not Responding" and tca_positionid=rc.position_id and (Date(cvall.tca_createdon) between "'.$start.'" and "'.$finish.'" )) as NotResponding
   ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Dicey" and tca_positionid=rc.position_id and (Date(cvall.tca_createdon) between "'.$start.'" and "'.$finish.'" )) as Dicey
  from tbl_clientjd_master as cm join tbl_recruiter_cv as rc on rc.position_id=cm.clientjob_id
  join tbl_cv_allocated as cvall on cvall.tca_cvid=rc.id join tbl_clients as cl on cl.client_id=cm.clientjob_compid and (Date(cm.drive_time) between "'.$start.'" and "'.$finish.'" )
    group by rc.position_id   ';
   $clientposresumes = DB::select(DB::raw($query));   
  if((count($clientposresumes)>0))
{ 
  
$to = ['bivek@mindztechnology.com','erp@elitehrpractices.com'];  
       $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
Mail::send('Emp.mails.followupdriveresumemail',  ['mail_data' => $clientposresumes], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Follow Up Status');

        });    
}
   
  // dd($clientposresumes);
   
    }
  
public function todaydrivepositionallocatedresumemail()
    {
      
$userpos=DB::select("select us.id,us.name,us.email,rmap.clientreq_id,rmap.fk_jdid from users as us join tbl_clientjdrecruitermap as rmap on rmap.fk_empid=us.id join tbl_clientjd_master as jdmast on jdmast.clientjob_id=rmap.fk_jdid  and jdmast.drive_time='".date('Y-m-d')."' ");
//print_r($userpos);
if((count($userpos))>0)

{

 foreach($userpos as  $key => $value) {
    
$data[$key]["username"]=$value->name;
$data[$key]["empcode"]=$value->id;

//$users1= DB::select("select *,(select uu.name from users as uu where uu.id=alloca.tca_assignfrom) as assignid  from tbl_recruiter_cv as rcv  join tbl_cv_allocated as alloca on alloca.tca_cvid=rcv.id where rcv.position_id=".$value->fk_jdid." and rcv.recruiter_id=".$value->id."");

$users1= DB::select("select *,(select uu.name from users as uu where uu.id=alloca.tca_assignfrom) as assignid 
,(select uuu.clientjob_title from tbl_clientjd_master as uuu where uuu.clientjob_id=rcv.position_id) as positionname 
 from tbl_recruiter_cv as rcv  join tbl_cv_allocated as alloca on alloca.tca_cvid=rcv.id where rcv.position_id=".$value->fk_jdid." and rcv.recruiter_id=".$value->id."");
 
$data[$key]["rcv"]=$users1;
}

$to = ['bivek@mindztechnology.com','erp@elitehrpractices.com'];  
       $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
        echo "<pre>";
//print_r($data);
 foreach($data as  $key => $valuedata) {
if((count($valuedata['rcv'])>0))
{
 // print_r($valuedata);

Mail::send('Emp.mails.todaydrivepositionallocatedresumemail',  ['mail_data' => $valuedata], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Follow Up Status');

        });   

}}
 
  
  


 }

//echo "111111";
    }  
    
public function todaydrivepositionallocatedmail()
    {
 $userscv= DB::select("select a.tca_assignfrom,us.id,us.name,us.email,jdmast.clientjob_title,count(a.tca_id) as total_rec_cv,(select uu.name from users as uu where uu.id=a.tca_assignfrom) as assignby from tbl_cv_allocated as a join users as us on us.id=a.tca_assignto   join tbl_clientjd_master as jdmast on jdmast.clientjob_id = a.tca_positionid where   Date(a.tca_createdon)='".date('Y-m-d')."' GROUP BY tca_assignto,tca_positionid");
if((count($userscv))>0)
{
 // echo "hii";
     $to = ['bivek@mindztechnology.com','erp@elitehrpractices.com'];  
       $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
Mail::send('Emp.mails.todaydrivepositionallocate',  ['mail_data' => $userscv], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Client call allocation Status');

        });


       /* foreach($userscv as $uscv)
        {
               $data = [
                    'name' => $uscv->name,
                    'email' =>$uscv->email,
                   'positionname' =>$uscv->clientjob_title,
                    
                ];
Mail::send('Emp.mails.todaydrivepositionallocate',  ['mail_data' => $data], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Tommorrow Drive Allocated');

        });

        }*/
   

}

    }
    
    
public function todayoffermail()
    {
       // echo ("select * from tbl_recruiter_cv where  cv_status ='4' and dateoj=".date('Y-m-d')."");
       $users= DB::select("select * from tbl_recruiter_cv as a join users as us on us.id=a.recruiter_id   where  a.cv_status ='4' and a.dateoj='".date('Y-m-d')."'");
     
     if(count($users)>0) {
    foreach($users as  $key => $value) {
      //  echo $value->email; exit;
       $to = [$value->email,'bivek@mindztechnology.com','erp@elitehrpractices.com'];  
       // $to='bivek@mindztechnology.com';
        $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
        
 
Mail::send('Emp.mails.todatoffermail',  ['mail_data' => $users], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Today Offer Mail');

        });

    }}
    
    }
    
    
public function todayofferremindermail()
    {
        
     //   echo ("select * from tbl_recruiter_cv as a join users as us on us.id=a.recruiter_id   where  a.cv_status ='4' and a.dateoj>='".date('Y-m-d')."'");   
   $users= DB::select("select a.*, us.name from tbl_recruiter_cv as a join users as us on us.id=a.recruiter_id   where  a.cv_status ='4' and a.dateoj>='".date('Y-m-d')."'");   
      foreach($users as  $key => $value) {
      
$users3= DB::select("select dateoj,date(created_at) as cdate from tbl_recruiter_cvchangestatus  where cvid=".$value->id." and cv_status='4' order by rcvid desc limit 0,1");
if(count($users3)>0)
{
    $then = $users3[0]->dateoj;
    $then = strtotime($then);
 
//Get the current timestamp.
 $now = strtotime($users3[0]->cdate); 
 
//Calculate the difference.
$difference = $then - $now;
 //Convert seconds into days.
$days = floor($difference / (60*60*24) );
if(($days/7)>0)
 {
$noofdays= floor($days/7);
 //Get the current timestamp.
 $now1 = strtotime(date("Y-m-d")); 
 
//Calculate the difference.
$difference2 = $then - $now1;
 //Convert seconds into days.
$daysn = floor($difference2 / (60*60*24) );
$noofdaysn= floor($daysn/7);
$users9= DB::select("select * from tbl_recruiter_cvupdateddata  where rc_resumeid=".$value->id." order by followup_status asc ");
$countval=count($users9);
$countdata= $noofdaysn+$countval;

//echo $countdata."---".$noofdays;
if($countdata>=$noofdays)
{

}
else
{
    $usersnew= DB::select("select a.*, us.name from tbl_recruiter_cv as a join users as us on us.id=a.recruiter_id   where a.id=".$value->id." ");   
    $to = ['bivek@mindztechnology.com','erp@elitehrpractices.com'];  
//$to='bivek@mindztechnology.com';
        $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
        
Mail::send('Emp.mails.notfollowup',  ['mail_data' => $usersnew], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Miss Followup Report');

        });

}

//echo $noofdays;

}
}

}
 //  dd($users);
    // echo  "hiiii";
    }
    
    public function report()
    {
        $dt=Carbon::now();
        /*
SELECT us.id,us.name,jdmaster.clientjob_id as jobid, jdmaster.clientjob_title as pos,u.name as spoc,(SELECT count(*) FROM tbl_recruiter_cv as rrrr
                          WHERE rrrr.recruiter_id = us.id and rrrr.position_id=jdmaster.clientjob_id and Date(rrrr.created_at) = CURDATE()
) as uploadcv
,(SELECT count(*) FROM tbl_recruiter_cv as rrrr
                          WHERE rrrr.recruiter_id = us.id and rrrr.position_id=jdmaster.clientjob_id and rrrr.cv_status=2 and Date(rrrr.created_at) = CURDATE()
) as sent_to_client 


FROM tbl_clientrec_start_endtime as startend join users as us on us.id=startend.clientstart_recuiterid join tbl_clientjdrecruitermap as jdmap  on jdmap.clientreq_id=startend.clientreq_id  join tbl_clientjd_master as jdmaster
on jdmaster.clientjob_id=jdmap.fk_jdid join users as u on u.id=jdmap.fk_assigneeid   where Date(startend.created_at)=CURDATE()  and startend.clientstart_recuiterid=us.id    group by startend.clientstartid  
 order by us.id asc  limit 0,100*/

  /*   $report= DB::table('tbl_recruiter_cv as rr')
            ->join('users as u','u.id','=','rr.recruiter_id')
            
            ->join('tbl_clientjd_master as cjm','rr.position_id','=','cjm.clientjob_id')
            ->join('tbl_clients as clie','clie.client_id','=','cjm.clientjob_compid')
           ->join('users as us','cjm.clientjob_empid','=','us.id')
            ->select('u.name as recruiter_name','u.id','cjm.clientjob_title as pos','cjm.clientjob_id',DB::raw('count(rr.position_id) as upload_cv'),'us.name as spoc','rr.created_at as reqcreated','clie.comp_name as clientjob_title',DB::raw("(SELECT count(*) FROM tbl_recruiter_cv as rrr
                          WHERE rrr.recruiter_id = u.id and rrr.position_id=cjm.clientjob_id and rrr.cv_status=2 and Date(rrr.created_at) = CURDATE()
                        ) as sent_to_client"))
           ->whereRaw('Date(rr.created_at) = CURDATE()')
           ->groupBy('rr.position_id','rr.recruiter_id','u.name','cjm.clientjob_title','us.name','u.id','cjm.clientjob_id')

           ->get();*/
           
           
                   $report= DB::table('tbl_clientrec_start_endtime as startend')
                              ->select('us.id','us.name as recruiter_name','jdmaster.clientjob_id as jobid','jdmaster.clientjob_title as pos','cli.comp_name as clientjob_title','u.name as spoc',DB::raw('(SELECT count(*) FROM tbl_recruiter_cv as rrrr WHERE rrrr.recruiter_id = us.id and rrrr.position_id=jdmaster.clientjob_id and Date(rrrr.created_at) = CURDATE()) as upload_cv'),DB::raw('(SELECT count(*) FROM tbl_recruiter_cv as rrrr  WHERE rrrr.recruiter_id = us.id and rrrr.position_id=jdmaster.clientjob_id and rrrr.cv_status=2 and Date(rrrr.created_at) = CURDATE() ) as sent_to_client ')) ->join('users as us','us.id','=','startend.clientstart_recuiterid')
                 ->join('tbl_clientjdrecruitermap as jdmap','jdmap.clientreq_id','=','startend.clientreq_id')
             ->join('tbl_clientjd_master as jdmaster','jdmaster.clientjob_id','=','jdmap.fk_jdid')
             ->join('users as u','u.id','=','jdmap.fk_assigneeid')
              ->join('tbl_clients as cli','cli.client_id','=','jdmaster.clientjob_compid')
->whereRaw('Date(startend.created_at) = CURDATE()')
->whereRaw('startend.clientstart_recuiterid=us.id')
->groupBy('startend.clientstartid')
->orderBy('us.id')->get();
       //    dd($report);
           
           //,DB::raw("(SELECT group_concat(usn.name) FROM users as usn join tbl_clientjdrecruitermap as reqmap WHERE  reqmap.fk_empid=usn.id and cjm.clientjob_empid=reqmap.fk_assigneeid   and reqmap.fk_jdid =cjm.clientjob_id  ) as spock_name")
           
            /*  $report= DB::table('tbl_recruiter_cv as rr')
            ->join('users as u','u.id','=','rr.recruiter_id')
              ->join('tbl_clientjd_master as cjm','rr.position_id','=','cjm.clientjob_id')
          //     ->join('tbl_clientjdrecruitermap as cjmap','cjmap.fk_assigneeid','=','cjm.clientjob_empid')
                
          //   ->join('tbl_clientjdrecruitermap as cjmap','rr.position_id','=','cjmap.fk_empid') 
         //   ->join('tbl_clientjd_master as cjm','cjm.clientjob_empid','=','cjmap.fk_assigneeid')
            ->join('tbl_clients as clie','clie.client_id','=','cjm.clientjob_compid')
           ->join('users as us','cjm.clientjob_empid','=','us.id')
            ->select('u.name as recruiter_name','u.id','cjm.clientjob_title as pos','cjm.clientjob_id' ,DB::raw('count(rr.position_id) as upload_cv'),'us.name as spoc','rr.created_at as reqcreated','clie.comp_name as clientjob_title',DB::raw("(SELECT count(*) FROM tbl_recruiter_cv as rrr
                          WHERE rrr.recruiter_id = u.id and rrr.position_id=cjm.clientjob_id and rrr.cv_status=2 and Date(rrr.created_at) = CURDATE()
                        ) as sent_to_client"),DB::raw('(SELECT GROUP_CONCAT(up.name ) from users as up join tbl_clientjdrecruitermap as rrmap where rrmap.fk_assigneeid=up.id and rrmap.fk_jdid=cjm.clientjob_id group by cjm.clientjob_id ) as recuitername') )
           ->whereRaw('Date(rr.created_at) = CURDATE()')
           ->groupBy('rr.position_id','rr.recruiter_id','u.name','cjm.clientjob_title','us.name','u.id','cjm.clientjob_id')

           ->get();*/
           
/*  $report= DB::table('tbl_clientrec_start_endtime as startend')
  ->leftjoin('tbl_recruiter_cv as rr','startend.clientstart_recuiterid','=','rr.recruiter_id')
  
          ->whereRaw('Date(startend.created_at) = CURDATE()')
           ->whereRaw('Date(rr.created_at) = CURDATE()')
           ->get();*/
           
       //  dd($report);

       // $to= DB::table('admins')->select('email','name')->first();
$to = ['bivek@mindztechnology.com','erp@elitehrpractices.com'];  
        //  $to = ['shibbu@mindztechnology.com', 'simer@elitehrpractices.com' ,'anupreet@elitehrpractices.com' ,'priyanka@elitehrpractices.com','vikas@elitehrpractices.com','vijitha@mindztechnology.com','bhavana@elitehrpractices.com','bivek@mindztechnology.com','erp@elitehrpractices.com', 'nehaarora@elitehrpractices.com'];  
        $from='erp@elitehrpractices.com';

        $reports=[ 'report'=> $report,
            'from'=>$from,
            'to'=>$to
        ];


      //  return view('Emp.mails.submisssion',['mail_data'=>$reports]);
      $users= DB::table('attendanceuser') ->select(DB::raw('count(attendid) as attendids'))->where('created_at','=',date('Y-m-d'))->get();

if($users[0]->attendids!='0')
{
  


       $sendmail= $this->sendMail($reports);
}
       
       
            //mail sent to recruiter
     /*       Mail::send('Emp.mails.positionreqmail', $reports, function($message) use ($reports)
            {
                $message->from('vijitha@mindztechnology.com');
                $message->to('bivek@mindztechnology.com','bivek');
                $message->subject('Testtt');
            });
*/




   dd('msg sent successfully');

 
    }

    public  function sendMail($reports)
    {
         $toemail= DB::table('users')->select('email')->first();
         //$to=$toemail->email;
 //   $to = ['shibbu@mindztechnology.com', 'simer@elitehrpractices.com' ,'anupreet@elitehrpractices.com' ,'priyanka@elitehrpractices.com','vikas@elitehrpractices.com','vijitha@mindztechnology.com','bhavana@elitehrpractices.com','bivek@mindztechnology.com','erp@elitehrpractices.com', 'nehaarora@elitehrpractices.com'];  
      //   $to='bivek@mindztechnology.com';
      $to = ['bivek@mindztechnology.com','erp@elitehrpractices.com'];  
        $toname="Elite Erp";
        $from='erp@elitehrpractices.com';


        Mail::send('Emp.mails.submisssion',  ['mail_data' => $reports], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Submission Report');

        });
    }
    
    public function alluserswithbreak()
    {
        $data['users'] = DB::table('tbl_clientrec_break_backtime as tcbr')
                                ->join('users as u','tcbr.clientstart_recuiterid','=','u.id')
                                ->select('tcbr.*','u.name')
                                ->whereDate('tcbr.created_at', '=', Carbon::today())
                                ->get();
        
        foreach($data['users'] as $key => $val){
           
            if(empty($val->clientstart_backtime)){
                $data['users'][$key]->clientstart_backtime = 'still has not come.';
                $data['users'][$key]->breakdelay = '';
            }else{
                $differenceTime = ($val->clientstart_backtime - $val->clientstart_breaktime);
                $diffTimeMinutes = number_format((float)($differenceTime/60), 2, '.', '');
                $data['users'][$key]->clientstart_backtime = date('Y-m-d H:i:s',$val->clientstart_backtime);
                
                    $data['users'][$key]->breakdelay=gmdate("H:i:s", $differenceTime);
               // $data['users'][$key]->breakdelay = $diffTimeMinutes.' minutes';
            }
            $data['users'][$key]->clientstart_breaktime = date('Y-m-d H:i:s',$val->clientstart_breaktime);
            
            
        }
        
        $to = 'erp@elitehrpractices.com';
        Mail::send('Emp.mails.userswithbreak', $data, function($message) use ($data,$to)
        {
            $message->to('erp@elitehrpractices.com');
          //  $message->to('bivek@mindztechnology.com');
            $message->cc('bivek@mindztechnology.com');
            $message->from('erp@elitehrpractices.com');
            $message->subject('Break Time Report');
        });
        
         // check for failures
        if (count(Mail::failures()) > 0) {
            echo "There was one or more failures. They were: <br />";
        }else{
            echo "No errors, all sent successfully!";
        } 
    }
    
    public function resupldelayreport()
    {
        $data['records'] = DB::table('tbl_clientrec_start_endtime as tcse')
                            ->join('users as us', 'us.id', '=', 'tcse.clientstart_recuiterid')
                            ->join('tbl_start_endtime_reasons as tser', 'tser.clientstartid', '=', 'tcse.clientstartid')
                            ->join('tbl_clientjdrecruitermap as tcjrm', 'tcjrm.clientreq_id', '=', 'tcse.clientreq_id')
                            ->join('tbl_clientjd_master as tcjm', 'tcjm.clientjob_id', '=', 'tcjrm.fk_jdid')
                            ->select('tcse.*','us.name','tcjm.clientjob_title','tser.*')
                            ->whereDate('tcse.created_at', '=', Carbon::today())
                            ->get();
        
              
        $to = 'erp@elitehrpractices.com';
        Mail::send('Emp.mails.reasonfordelay', $data, function($message) use ($data,$to)
        {
            $message->to('ankit@mindztechnology.com');
            $message->from('bivek@mindztechnology.com');
            $message->subject('Reason for delay of resume upload');
        });
        
         // check for failures
        if (count(Mail::failures()) > 0) {
            echo "There was one or more failures. They were: <br />";
        }else{
            echo "No errors, all sent successfully!";
        }                       
    }

    
      public  function thirtyminmail()
    {
        $time= date("H:i:s", strtotime("-30 minutes"));
     
        
        $reports= DB::table('tbl_clientrec_start_endtime as rr')
           ->join('tbl_clientjdrecruitermap as cjmap','rr.clientreq_id','=','cjmap.clientreq_id')
         ->join('tbl_clientjd_master as cjm','cjmap.fk_jdid','=','cjm.clientjob_id')
         
         
        ->join('users as us','rr.clientstart_recuiterid','=','us.id')
         //->join('tbl_recruiter_cv as reccv','reccv.position_id','=','cjm.clientjob_id')
           ->whereRaw('Date(rr.created_at) = CURDATE()')
           ->whereRaw('rr.clientstart_stoptime =0')
            ->where('rr.clientstart_starttime','<=',$time)
         ->get();
  //dd($reports);
  /*    $report= DB::table('tbl_recruiter_cv as rr')
            ->join('users as u','u.id','=','rr.recruiter_id')
        ->join('tbl_clientjd_master as cjm','rr.position_id','=','cjm.clientjob_id')
             ->join('tbl_clientrec_start_endtime as stet','stet.clientreq_id','=','cjm.clientjob_id')
           ->join('users as us','cjm.clientjob_empid','=','us.id')
            ->select('u.name as recruiter_name','u.id','cjm.clientjob_title as pos','cjm.clientjob_id',DB::raw('count(rr.position_id) as upload_cv'),'us.name as spoc',DB::raw("(SELECT count(*) FROM tbl_recruiter_cv as rrr
                          WHERE rrr.recruiter_id = u.id and rrr.position_id=cjm.clientjob_id and rrr.cv_status=2
                        ) as sent_to_client"))
           ->whereRaw('Date(rr.created_at) = CURDATE()')
           ->groupBy('rr.position_id','rr.recruiter_id','u.name','cjm.clientjob_title','cjm.clientjob_id')

           ->get();*/
           
    // $to = ['shibbu@mindztechnology.com', 'simer@elitehrpractices.com' ,'anupreet@elitehrpractices.com','vikas@elitehrpractices.com','vijitha@mindztechnology.com','bhavana@elitehrpractices.com','bivek@mindztechnology.com','erp@elitehrpractices.com', 'nehaarora@elitehrpractices.com'];  
  $to = ['bivek@mindztechnology.com','erp@elitehrpractices.com'];     
 //      $to1='priyanka@elitehrpractices.com';

        $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
    //   echo "Today is " . date("H:i:s") . "<br>"; exit;
        if ( (date("H:i:s") >= "10:29:00" && date("H:i:s") <=  "13:00:00")  || (date("H:i:s") >=  "14:29:00"  && date("H:i:s") <=  "19:00:00")) {
            
          //  echo "dddddddddddd"; exit;
            $users= DB::table('attendanceuser') ->select(DB::raw('count(attendid) as attendids'))->where('created_at','=',date('Y-m-d'))->get();

if($users[0]->attendids!='0')
{
  

 Mail::send('Emp.mails.thirtymindelay',  ['mail_data' => $reports], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('30 Minutes Delay Report');

        });
    
 /*  Mail::send('Emp.mails.thirtymindelay',  ['mail_data' => $reports], function ($message) use ($from, $to1, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to1, $toname);
            $message->subject('30 Minutes Delay Report');

        }); */ 
        
        
    
}
         dd('Send');
// do action
}
else
{
   dd('Time is not between it 10:30 to 1pm and 2:30 to 7 pm'); 
    
}
 /* Mail::send('Emp.mails.thirtymindelay',  ['mail_data' => $reports], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('30 Minutes Delay Report');

        });*/
        
         

    }
    
    public  function dailyattance()
    {
         $toemail= DB::table('users')->select('email')->first();
         //$to=$toemail->email;
//$to='bivek@mindztechnology.com';

       $report= DB::table('attendanceuser')
         
              ->select(DB::raw('count(attendid) as clientcount'))
           ->whereRaw('Date(created_at) =CURDATE()')
       //    ->groupBy('rr.position_id','rr.recruiter_id','u.name','cjm.clientjob_title','us.name','u.id','cjm.clientjob_id')

           ->get();

       // $to= DB::table('admins')->select('email','name')->first();
if($report[0]->clientcount>0)
{
    dd('Already Insert');
}
else
{
    //$to = ['shibbu@mindztechnology.com', 'simer@elitehrpractices.com' ,'anupreet@elitehrpractices.com' ,'priyanka@elitehrpractices.com','vikas@elitehrpractices.com','vijitha@mindztechnology.com','bhavana@elitehrpractices.com','bivek@mindztechnology.com','erp@elitehrpractices.com', 'nehaarora@elitehrpractices.com'];  
      $to = ['bivek@mindztechnology.com','erp@elitehrpractices.com'];      
       // $to=//'shalini29081991@gmail.com';
        $toname="Simer";
        $from='erp@elitehrpractices.com';


        Mail::send('Emp.mails.dailyattandance',  ['mail_data' => ''], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Alert Daily Attendance');

        });
           dd('msg sent successfully');
    }
    
    }


  public  function hrdailyattance()
    {
         $toemail= DB::table('users')->select('email')->first();
         //$to=$toemail->email;
//$to='bivek@mindztechnology.com';

       $report= DB::table('hrattendance')
         
              ->select(DB::raw('count(attendid) as clientcount'))
           ->whereRaw('Date(created_at) =CURDATE()')
       //    ->groupBy('rr.position_id','rr.recruiter_id','u.name','cjm.clientjob_title','us.name','u.id','cjm.clientjob_id')

           ->get();

       // $to= DB::table('admins')->select('email','name')->first();
if($report[0]->clientcount>0)
{
    dd('Already Insert');
}
else
{
    //$to = ['shibbu@mindztechnology.com', 'simer@elitehrpractices.com' ,'anupreet@elitehrpractices.com' ,'priyanka@elitehrpractices.com','vikas@elitehrpractices.com','vijitha@mindztechnology.com','bhavana@elitehrpractices.com','bivek@mindztechnology.com','erp@elitehrpractices.com', 'nehaarora@elitehrpractices.com'];  
      $to = ['bivek@mindztechnology.com','erp@elitehrpractices.com'];      
     //  $to='bivek@mindztechnology.com';
        $toname="Elite Erp";
        $from='erp@elitehrpractices.com';


        Mail::send('Emp.mails.hrdailyattandance',  ['mail_data' => ''], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Alert Hr Leave  Attendance');

        });
           dd('msg sent successfully');
    }
    
    }


 public  function onehrsmail()
    {
        $time= date("H:i:s", strtotime("-60 minutes"));
     
        
        
        $reports= DB::table('tbl_clientrec_start_endtime as rr')
           ->join('tbl_clientjdrecruitermap as cjmap','rr.clientreq_id','=','cjmap.clientreq_id')
         ->join('tbl_clientjd_master as cjm','cjmap.fk_jdid','=','cjm.clientjob_id')
         
         
        ->join('users as us','rr.clientstart_recuiterid','=','us.id')
         //->join('tbl_recruiter_cv as reccv','reccv.position_id','=','cjm.clientjob_id')
           ->whereRaw('Date(rr.created_at) = CURDATE()')
           ->whereRaw('rr.clientstart_stoptime =0')
            ->where('rr.clientstart_starttime','<=',$time)
         ->get();
  
  /*    $report= DB::table('tbl_recruiter_cv as rr')
            ->join('users as u','u.id','=','rr.recruiter_id')
        ->join('tbl_clientjd_master as cjm','rr.position_id','=','cjm.clientjob_id')
             ->join('tbl_clientrec_start_endtime as stet','stet.clientreq_id','=','cjm.clientjob_id')
           ->join('users as us','cjm.clientjob_empid','=','us.id')
            ->select('u.name as recruiter_name','u.id','cjm.clientjob_title as pos','cjm.clientjob_id',DB::raw('count(rr.position_id) as upload_cv'),'us.name as spoc',DB::raw("(SELECT count(*) FROM tbl_recruiter_cv as rrr
                          WHERE rrr.recruiter_id = u.id and rrr.position_id=cjm.clientjob_id and rrr.cv_status=2
                        ) as sent_to_client"))
           ->whereRaw('Date(rr.created_at) = CURDATE()')
           ->groupBy('rr.position_id','rr.recruiter_id','u.name','cjm.clientjob_title','cjm.clientjob_id')

           ->get();*/
           
     //$to = ['shibbu@mindztechnology.com', 'simer@elitehrpractices.com' ,'anupreet@elitehrpractices.com' ,'priyanka@elitehrpractices.com','vikas@elitehrpractices.com','vijitha@mindztechnology.com','bhavana@elitehrpractices.com','bivek@mindztechnology.com','erp@elitehrpractices.com', 'nehaarora@elitehrpractices.com'];  
     $to = ['bivek@mindztechnology.com','erp@elitehrpractices.com'];  
       

        $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
     //   echo "Today is " . date("H:i:s") . "<br>"; exit;
         if ( (date("H:i:s") >= "10:29:00" && date("H:i:s") <=  "13:00:00")  || (date("H:i:s") >=  "14:29:00"  && date("H:i:s") <=  "19:00:00")) {
            
            $users= DB::table('attendanceuser') ->select(DB::raw('count(attendid) as attendids'))->where('created_at','=',date('Y-m-d'))->get();

if($users[0]->attendids!='0')
{
  

 Mail::send('Emp.mails.thirtymindelay',  ['mail_data' => $reports], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('One Hrs Delay Report');

        });}
         dd('Send');
// do action
}
else
{
   dd('Time is not between it 10:30 to 1pm and 2:30 to 7 pm'); 
    
}
 /* Mail::send('Emp.mails.thirtymindelay',  ['mail_data' => $reports], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('30 Minutes Delay Report');

        });*/
        
         

    }
    
    public  function firstsubmissionreport()
    {



$users= DB::select("select ats.punchrecord ,  ats.empname , ats.empcode , st.clientstart_starttime ,
(select concat(clientjob_title,'#' ,fn_clientname(clientjob_compid)) 
  from  tbl_clientjd_master where 
clientjob_id =(select fk_jdid from  tbl_clientjdrecruitermap where  clientreq_id=st.clientreq_id and 
  st.clientstart_recuiterid=us.id and   date(st.created_at)=date(now())  order by fk_jdid asc  limit 1  )) as position_work   
from
 tbl_clientrec_start_endtime as st inner join users as us on us.id=st.clientstart_recuiterid and
 date(st.created_at)=date(now())
inner join attendanceuser  as ats on ats.empcode=us.emp_empid and date(ats.created_at) =date(now())
where  date(st.created_at)=date(now()) group by ats.empcode
");



/*
$users= DB::select("select ats.lastpunch ,  ats.empname , ats.empcode , st.clientstart_starttime ,
(select concat(clientjob_title,'#' ,fn_clientname(clientjob_compid)) 
  from  tbl_clientjd_master where 
clientjob_id =(select fk_jdid from  tbl_clientjdrecruitermap where 
  fk_empid=st.clientstart_recuiterid limit 1 )) as position_work   
from
 tbl_clientrec_start_endtime as st inner join users as us on us.id=st.clientstart_recuiterid and
 date(st.created_at)=date(now())
inner join attendanceuser  as ats on ats.empcode=us.emp_empid and date(ats.created_at) =date(now())
where  date(st.created_at)=date(now()) group by ats.empcode
");
*/
//dd($users);
    //  $to = ['bhavana@elitehrpractices.com' ,'priyanka@elitehrpractices.com', 'nehaarora@elitehrpractices.com','shibbu@mindztechnology.com', 'simer@elitehrpractices.com' ,'anupreet@elitehrpractices.com','vikas@elitehrpractices.com','vijitha@mindztechnology.com','bivek@mindztechnology.com','erp@elitehrpractices.com'];  
 $to = ['bivek@mindztechnology.com','erp@elitehrpractices.com'];  
  //$to='bivek@mindztechnology.com';
        $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
       
Mail::send('Emp.mails.firstsubmissiondetails',  ['mail_data' => $users], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('FIRST SUBMISSION REPORT');

        });
        
    
        
         dd('msg sent successfully');
    }
        
    public  function sevenoclockmail()
    {
        
       /*   $reports= DB::table('tbl_clientjd_master as cjm')
 ->select('cjm.clientjob_id','uss.name as spoc','cjm.clientjob_title','clients.comp_name','cjm.clientjob_empid',DB::raw("(SELECT group_concat(us.name) FROM users as us join tbl_recruiter_cv as reqmap
                 WHERE  reqmap.recruiter_id=us.id and cjm.clientjob_id=reqmap.position_id and Date(reqmap.created_at) = CURDATE()
         ) as recruiter_name"),DB::raw("(SELECT GROUP_CONCAT(cnt) cnt FROM ( SELECT COUNT(*) cnt
    FROM tbl_recruiter_cv as cv join tbl_clientjd_master as cj where cv.position_id=cj.clientjob_id and Date(cv.created_at) = CURDATE() GROUP BY recruiter_id) q ) as upload_cv"),DB::raw("(SELECT GROUP_CONCAT(cnt) cnt FROM ( SELECT COUNT(*) cnt
    FROM tbl_recruiter_cv as rrr join tbl_clientjd_master as cj where rrr.position_id=cj.clientjob_id and Date(rrr.created_at) = CURDATE() and rrr.cv_status=2 GROUP BY recruiter_id) q ) as sent_to_client"),DB::raw("(SELECT count(rrr.position_id) FROM tbl_recruiter_cv as rrr
                          WHERE  rrr.position_id=cjm.clientjob_id   and Date(rrr.created_at) = CURDATE()
                        ) as totalupload_cv"))
 ->join('tbl_clients as clients','clients.client_id','=','cjm.clientjob_compid')
 ->join('tbl_recruiter_cv as rr','rr.position_id','=','cjm.clientjob_id')
  ->join('users as uss','cjm.clientjob_empid','=','uss.id')
    ->whereRaw('Date(rr.created_at) = CURDATE()')
  ->groupBy('rr.position_id','cjm.clientjob_id','cjm.clientjob_title','clients.comp_name','cjm.clientjob_id','cjm.clientjob_empid','uss.name')
 //->where('assignement_id',1)
 //->groupBy('rr.position_id','rr.recruiter_id','cjm.clientjob_title','cjm.clientjob_id')
->get();*/
//dd($reports);',DB::raw("(SELECT group_concat(us.name) FROM users as us join tbl_recruiter_cv as reqmap  WHERE  reqmap.recruiter_id=us.id and cjm.clientjob_id=reqmap.position_id and Date(reqmap.created_at) = CURDATE()  ) as recruiter_name")

// $to = ['shibbu@mindztechnology.com', 'simer@elitehrpractices.com' ,'anupreet@elitehrpractices.com' ,'priyanka@elitehrpractices.com','vikas@elitehrpractices.com','vijitha@mindztechnology.com','bhavana@elitehrpractices.com','bivek@mindztechnology.com','erp@elitehrpractices.com', 'nehaarora@elitehrpractices.com'];  
   /* $reports= DB::table('users as user')
    
    ->select('user.name',DB::raw("(SELECT GROUP_CONCAT(cnt) cnt FROM ( SELECT COUNT(*) cnt
    FROM tbl_recruiter_cv as cv join tbl_clientjd_master as cj join tbl_clientjdrecruitermap as rmap where cv.position_id=cj.clientjob_id  GROUP BY recruiter_id) q ) as upload_cv"),DB::raw("(SELECT group_concat(jdmast.clientjob_title) FROM tbl_clientjdrecruitermap as rmap join tbl_clientjd_master as jdmast   WHERE  rmap.fk_empid=user.id and jdmast.clientjob_id=rmap.fk_jdid ) as recruiter_name"))
   //  ->join('tbl_clientjdrecruitermap as repmap','repmap.fk_empid','=','user.id')
    ->get();*/
    /*
    $pos=DB::table('tbl_recruiter_cv as c')
            ->join('tbl_clientjd_master as p','p.clientjob_id','=','c.position_id')
         //   ->join('tbl_clientjdrecruitermap as pmap','pmap.fk_assigneeid','=','p.clientjob_empid')
            ->join('users as us','us.id','=','p.clientjob_empid')
             ->join('tbl_clients as cli','cli.client_id','=','p.clientjob_compid')
            ->select('c.position_id','p.clientjob_title','p.clientjob_noofposition','cli.comp_name','us.name',DB::raw('COUNT(c.position_id) as total_upload_profile'),DB::raw('(SELECT GROUP_CONCAT(up.name ) from users as up join tbl_clientjdrecruitermap as rrmap where rrmap.fk_empid=up.id and rrmap.fk_jdid=p.clientjob_id group by p.clientjob_id ) as recuitername') )
           // ->where('p.clientjob_empid','=',$assigneeid)
           ->groupBy('c.position_id','p.clientjob_title','p.clientjob_noofposition','us.name')
           //->having('p.clientjob_empid',$assigneeid)
            ->get();*/
            
            //CURDATE()
            
               /*  $report= DB::table('tbl_clientrec_start_endtime as startend')
               ->select('us.id','us.name as recruiter_name','jdmaster.clientjob_id as jobid','jdmaster.clientjob_title as pos','cli.comp_name as clientjob_title','u.name as spoc',DB::raw('(SELECT count(*) FROM tbl_recruiter_cv as rrrr
                          WHERE rrrr.recruiter_id = us.id and rrrr.position_id=jdmaster.clientjob_id and Date(rrrr.created_at) = date("2018-07-10")
) as upload_cv'),DB::raw('(SELECT count(*) FROM tbl_recruiter_cv as rrrr
                          WHERE rrrr.recruiter_id = us.id and rrrr.position_id=jdmaster.clientjob_id and rrrr.cv_status=2 and Date(rrrr.created_at) = date("2018-07-10")
) as sent_to_client ')

)
               ->join('users as us','us.id','=','startend.clientstart_recuiterid')
                 ->join('tbl_clientjdrecruitermap as jdmap','jdmap.clientreq_id','=','startend.clientreq_id')
             ->join('tbl_clientjd_master as jdmaster','jdmaster.clientjob_id','=','jdmap.fk_jdid')
             ->join('users as u','u.id','=','jdmap.fk_assigneeid')
              ->join('tbl_clients as cli','cli.client_id','=','jdmaster.clientjob_compid')
->whereRaw('Date(startend.created_at) = date("2018-07-10")')
->whereRaw('startend.clientstart_recuiterid=us.id')
->groupBy('startend.clientstartid')
->orderBy('us.id')
           ->get();*/
           /*
             $report= DB::table('tbl_clientrec_start_endtime as startend')
              ->select('us.id','us.name as recruiter_name',DB::raw('(SELECT GROUP_CONCAT(up.clientjob_title ) from tbl_clientjd_master  as up join tbl_clientjdrecruitermap as rrmap where rrmap.fk_jdid=up.clientjob_id and rrmap.fk_empid=us.id and rrmap.clientreq_id=startend.clientreq_id  group by us.id ) as pos'))
       ->join('users as us','us.id','=','startend.clientstart_recuiterid')
       ->whereRaw('Date(startend.created_at) = date("2018-07-10")')
->groupBy('startend.clientstartid')
//->orderBy('us.id')
           ->get();
           */

/*$pos= DB::table('users as user')
 ->select('user.id','user.name',DB::raw("(SELECT GROUP_CONCAT(distinct(up.clientjob_id ) ) from tbl_clientjd_master  as up join tbl_clientjdrecruitermap as jdrmap on jdrmap.fk_jdid=up.clientjob_id join tbl_clientrec_start_endtime as stend on stend.clientreq_id=jdrmap.clientreq_id  where  stend.clientstart_recuiterid=user.id and Date(stend.created_at) = CURDATE()  ) as posid"),DB::raw('(SELECT GROUP_CONCAT((up.clientjob_title) ) from tbl_clientjd_master  as up join tbl_clientjdrecruitermap as jdrmap on jdrmap.fk_jdid=up.clientjob_id join tbl_clientrec_start_endtime as stend on stend.clientreq_id=jdrmap.clientreq_id  where  stend.clientstart_recuiterid=user.id and Date(stend.created_at) = CURDATE() ) as pos'),DB::raw('(SELECT GROUP_CONCAT((jdmast.comp_name) ) from tbl_clients as jdmast join tbl_clientjd_master  as up on up.clientjob_compid=jdmast.client_id join tbl_clientjdrecruitermap as jdrmap on jdrmap.fk_jdid=up.clientjob_id join tbl_clientrec_start_endtime as stend on stend.clientreq_id=jdrmap.clientreq_id  where  stend.clientstart_recuiterid=user.id and Date(stend.created_at) = CURDATE() ) as clientjob_title ')
// ,DB::raw('(SELECT GROUP_CONCAT((jdmast.client_id) ) from tbl_clients as jdmast join tbl_clientjd_master  as up on up.clientjob_compid=jdmast.client_id join tbl_clientjdrecruitermap as jdrmap on jdrmap.fk_jdid=up.clientjob_id join tbl_clientrec_start_endtime as stend on stend.clientreq_id=jdrmap.clientreq_id  where  stend.clientstart_recuiterid=user.id and Date(stend.created_at) = CURDATE() ) as upload_cv')
 //,DB::raw('(SELECT GROUP_CONCAT((select count(*) from tbl_recruiter_cv  where recruiter_id=jdrmap.fk_empid and Date(created_at)=CURDATE() ) ) from tbl_clients as jdmast join tbl_clientjd_master  as up on up.clientjob_compid=jdmast.client_id join tbl_clientjdrecruitermap as jdrmap on jdrmap.fk_jdid=up.clientjob_id join tbl_clientrec_start_endtime as stend on stend.clientreq_id=jdrmap.clientreq_id  where  stend.clientstart_recuiterid=user.id and Date(stend.created_at) = CURDATE() ) as upload_cv')
//  ,DB::raw('(SELECT GROUP_CONCAT((select count(*) from tbl_recruiter_cv  where recruiter_id=jdrmap.fk_empid and r_map_id =jdrmap.clientreq_id and Date(created_at)=CURDATE() ) ) from tbl_clients as jdmast join tbl_clientjd_master  as up on up.clientjob_compid=jdmast.client_id join tbl_clientjdrecruitermap as jdrmap on jdrmap.fk_jdid=up.clientjob_id join tbl_clientrec_start_endtime as stend on stend.clientreq_id=jdrmap.clientreq_id  where  stend.clientstart_recuiterid=user.id and Date(stend.created_at) = CURDATE() ) as upload_cv')

 ,DB::raw('(SELECT GROUP_CONCAT((select count(*) from tbl_recruiter_cv  where recruiter_id=stend.clientstart_recuiterid and position_id =jdrmap.fk_jdid and Date(created_at)=CURDATE()) ) from   tbl_clientjdrecruitermap as jdrmap  join tbl_clientrec_start_endtime as stend on stend.clientreq_id=jdrmap.clientreq_id  where  stend.clientstart_recuiterid=user.id and Date(stend.created_at) = CURDATE() ) as upload_cv')
 
  ,DB::raw('(SELECT GROUP_CONCAT((select count(*) from tbl_recruiter_cv  where recruiter_id=stend.clientstart_recuiterid and position_id =jdrmap.fk_jdid and Date(created_at)=CURDATE() and cv_status=2  ) )  from   tbl_clientjdrecruitermap as jdrmap  join tbl_clientrec_start_endtime as stend on stend.clientreq_id=jdrmap.clientreq_id  where  stend.clientstart_recuiterid=user.id and Date(stend.created_at) = CURDATE() ) as sent_to_client')
  ,DB::raw('(SELECT count(*) from tbl_recruiter_cv as recci   join   tbl_clientjdrecruitermap as jdrmap  join tbl_clientrec_start_endtime as stend on stend.clientreq_id=jdrmap.clientreq_id  where  stend.clientstart_recuiterid=user.id  and Date(stend.created_at) = CURDATE() and recci.position_id =jdrmap.fk_jdid and Date(recci.created_at)=CURDATE() and recci.cv_status=2 ) as upload_cvcount')

 //,DB::raw('((select count(*) from tbl_recruiter_cv where recruiter_id=user.id  and cv_status=2   and Date(created_at)=CURDATE()) ) as upload_cvcount')
 
  )
->where('user.emp_role', 'like', '%3%')
 ->get();*/

/*$pos= DB::table('users as user')
 ->select('user.id','user.name'
  ,DB::raw('(SELECT GROUP_CONCAT(distinct(jd.clientjob_title)) from tbl_clientjd_master as jd join tbl_recruiter_cv as recci  on jd.clientjob_id=recci.position_id  where recci.recruiter_id=user.id and  Date(recci.created_at)=CURDATE()   ) as pos')
  #,DB::raw('(SELECT GROUP_CONCAT(distinct(select count(*) from tbl_recruiter_cv  where recruiter_id=user.id and position_id =jdrmap.fk_jdid and Date(created_at)=CURDATE()) ) from   tbl_clientjdrecruitermap as jdrmap where  jdrmap.fk_empid=user.id   ) as upload_cv')

 ,DB::raw('(SELECT GROUP_CONCAT(distinct(comp.comp_name)) from tbl_clients as comp  join  tbl_clientjd_master as jd on comp.client_id=jd.clientjob_compid  join tbl_recruiter_cv as recci  on jd.clientjob_id=recci.position_id  where recci.recruiter_id=user.id and  Date(recci.created_at)=CURDATE()   ) as clientjob_title')
 # ,DB::raw('(SELECT GROUP_CONCAT(distinct(select count(*) from tbl_recruiter_cv  where recruiter_id=user.id and position_id =jdrmap.fk_jdid  and cv_status=2 and Date(created_at)=CURDATE() ) ) from   tbl_clientjdrecruitermap as jdrmap  join tbl_clientrec_start_endtime as stend on stend.clientreq_id=jdrmap.clientreq_id  where  stend.clientstart_recuiterid=user.id and Date(stend.created_at) = CURDATE()  ) as sent_to_client')
,DB::raw('(SELECT count(*) from tbl_recruiter_cv as recci where recruiter_id=user.id and  Date(created_at)=CURDATE()   ) as upload_cvcount')
  ,DB::raw('(SELECT count(*) from tbl_recruiter_cv as recci where recruiter_id=user.id and  Date(created_at)=CURDATE()  and cv_status=2 ) as upload_cvcount2status')

  ,DB::raw('(SELECT GROUP_CONCAT((select count(*) from tbl_recruiter_cv as rrr   where rrr.recruiter_id=user.id and rrr.position_id =jd.clientjob_id and Date(rrr.created_at)=CURDATE() group by position_id )) from tbl_clientjd_master as jd join tbl_recruiter_cv as recci  on jd.clientjob_id=recci.position_id  where recci.recruiter_id=user.id and  Date(recci.created_at)=CURDATE()   ) as upload_cv')
  ,DB::raw('(SELECT GROUP_CONCAT((select count(*) from tbl_recruiter_cv  where recruiter_id=user.id and position_id =jd.clientjob_id and cv_status=2 and Date(created_at)=CURDATE() group by position_id )) from tbl_clientjd_master as jd join tbl_recruiter_cv as recci  on jd.clientjob_id=recci.position_id  where recci.recruiter_id=user.id and  Date(recci.created_at)=CURDATE()   ) as sent_to_client')
 ,DB::raw('(SELECT GROUP_CONCAT(distinct(jd.clientjob_id )) from tbl_clientjd_master as jd join tbl_recruiter_cv as recci  on jd.clientjob_id=recci.position_id  where recci.recruiter_id=user.id and  Date(recci.created_at)=CURDATE()   ) as upload_cvssss')
 
 )
 
 // ->join('tbl_recruiter_cv as rcv','user.id','=','rcv.recruiter_id')
->where('user.emp_role', 'like', '%3%')
//->groupBy('rcv.recruiter_id','rcv.position_id')
 ->get();*/
 
 
 
/*$pos= DB::table('users as user')
 ->select('user.id','user.name'
  //,DB::raw('(SELECT GROUP_CONCAT(distinct(jd.clientjob_title)) from tbl_clientjd_master as jd join tbl_recruiter_cv as recci  on jd.clientjob_id=recci.position_id  where recci.recruiter_id=user.id and  Date(recci.created_at)="2018-07-18"   ) as pos')
  #,DB::raw('(SELECT GROUP_CONCAT(distinct(select count(*) from tbl_recruiter_cv  where recruiter_id=user.id and position_id =jdrmap.fk_jdid and Date(created_at)="2018-07-18") ) from   tbl_clientjdrecruitermap as jdrmap where  jdrmap.fk_empid=user.id   ) as upload_cv')

// ,DB::raw('(SELECT GROUP_CONCAT(distinct(comp.comp_name)) from tbl_clients as comp  join  tbl_clientjd_master as jd on comp.client_id=jd.clientjob_compid  join tbl_recruiter_cv as recci  on jd.clientjob_id=recci.position_id  where recci.recruiter_id=user.id and  Date(recci.created_at)="2018-07-18"   ) as clientjob_title')
 # ,DB::raw('(SELECT GROUP_CONCAT(distinct(select count(*) from tbl_recruiter_cv  where recruiter_id=user.id and position_id =jdrmap.fk_jdid  and cv_status=2 and Date(created_at)="2018-07-18" ) ) from   tbl_clientjdrecruitermap as jdrmap  join tbl_clientrec_start_endtime as stend on stend.clientreq_id=jdrmap.clientreq_id  where  stend.clientstart_recuiterid=user.id and Date(stend.created_at) = "2018-07-18"  ) as sent_to_client')
//,DB::raw('(SELECT count(*) from tbl_recruiter_cv as recci where recruiter_id=user.id and  Date(created_at)="2018-07-18"   ) as upload_cvcount')
//  ,DB::raw('(SELECT count(*) from tbl_recruiter_cv as recci where recruiter_id=user.id and  Date(created_at)="2018-07-18"  and cv_status=2 ) as upload_cvcount2status')

 //,DB::raw('(SELECT GROUP_CONCAT((select count(*) from tbl_recruiter_cv as rrr   where rrr.recruiter_id=user.id and rrr.position_id =jd.clientjob_id and Date(rrr.created_at)="2018-07-18" group by position_id )) from tbl_clientjd_master as jd join tbl_recruiter_cv as recci  on jd.clientjob_id=recci.position_id  where recci.recruiter_id=user.id and  Date(recci.created_at)="2018-07-18"    ) as upload_cv')
  ,DB::raw('(SELECT GROUP_CONCAT((select count(*) from tbl_recruiter_cv  where recruiter_id=user.id and position_id =jd.clientjob_id and cv_status=2 and Date(created_at)="2018-07-18" group by position_id )) from tbl_clientjd_master as jd join tbl_recruiter_cv as recci  on jd.clientjob_id=recci.position_id  where recci.recruiter_id=user.id and  Date(recci.created_at)="2018-07-18" group by recci.recruiter_id) as sent_to_client')
 ,DB::raw('(SELECT GROUP_CONCAT(cnt) cnt FROM (SELECT count(recci.position_id) AS cnt from tbl_clientjd_master as jd join tbl_recruiter_cv as recci  on jd.clientjob_id=recci.position_id  where recci.recruiter_id=user.id and recci.cv_status=2 and  Date(recci.created_at)="2018-07-18" group by jd.clientjob_id) p) as ssssssent_to_client')
,DB::raw('(SELECT GROUP_CONCAT(PPcnt) PPcnt FROM (SELECT count(recci.position_id) AS PPcnt from tbl_clientjd_master as jd join tbl_recruiter_cv as recci  on jd.clientjob_id=recci.position_id  where recci.recruiter_id=user.id and  Date(recci.created_at)="2018-07-18" group by jd.clientjob_id) p) as upload_cv')

 /* ,DB::raw('(SELECT GROUP_CONCAT(SELECT count(recci.position_id)
from tbl_clientjd_master as jd 
join tbl_recruiter_cv as recci  on jd.clientjob_id=recci.position_id  
where recci.recruiter_id=29 and recci.cv_status=2 and  Date(recci.created_at)="2018-07-18" group by jd.clientjob_id) as sffent_to_client')

 ,DB::raw('(SELECT GROUP_CONCAT(distinct(jd.clientjob_id )) from tbl_clientjd_master as jd join tbl_recruiter_cv as recci  on jd.clientjob_id=recci.position_id  where recci.recruiter_id=user.id and  Date(recci.created_at)="2018-07-18"   ) as upload_cvssss')
 
 )
 
 // ->join('tbl_recruiter_cv as rcv','user.id','=','rcv.recruiter_id')
->where('user.emp_role', 'like', '%3%')
//->groupBy('rcv.recruiter_id','rcv.position_id')
 ->tosql();
*/

$data=[];


$users= DB::select("select * from users where emp_role like '%3%'");

  foreach($users as  $key => $value) {
    
$data[$key]["username"]=$value->name;
$data[$key]["empcode"]=$value->name;


$users1= DB::select("SELECT count(rv.id) as trtr from tbl_recruiter_cv as rv inner join tbl_clientjd_master 
as cm on cm.clientjob_id=rv.position_id where 
rv.recruiter_id=".$value->id." and Date(rv.created_at)=CURDATE() group by rv.position_id")

;

$data[$key]["userreport"]=$users1;

$users2= DB::select("SELECT count(rv.id) as sendclient, cm.clientjob_title as position  from tbl_recruiter_cv as rv inner join tbl_clientjd_master 
as cm on cm.clientjob_id=rv.position_id where 
rv.recruiter_id=".$value->id." and Date(rv.created_at)=CURDATE() and rv.cv_status=2  group by rv.position_id");

$data[$key]["userreport2"]=$users2;

$users3= DB::select("SELECT  cm.clientjob_title from tbl_recruiter_cv as rv inner join tbl_clientjd_master 
as cm on cm.clientjob_id=rv.position_id where 
rv.recruiter_id=".$value->id." and Date(rv.created_at)=CURDATE()  group by rv.position_id");

$data[$key]["position"]=$users3;

$users4= DB::select("SELECT  tblclient.comp_name from tbl_recruiter_cv as rv inner join tbl_clientjd_master 
as cm on cm.clientjob_id=rv.position_id  join tbl_clients as tblclient on tblclient.client_id=cm.clientjob_compid  where 
rv.recruiter_id=".$value->id." and Date(rv.created_at)=CURDATE()   group by rv.position_id");

$data[$key]["client"]=$users4;
    
}


$to = ['bivek@mindztechnology.com','erp@elitehrpractices.com'];  
//$to='bivek@mindztechnology.com';
        $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
$users= DB::table('attendanceuser') ->select(DB::raw('count(attendid) as attendids'))->where('created_at','=',date('Y-m-d'))->get();




if($users[0]->attendids!='0')
{
  
Mail::send('Emp.mails.sevenoclockmail',  ['mail_data' => $data], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Submission Report');

        });


    }}
    
    
     public  function onehrsattposmail()
    {
        
        $attendance=DB::table('attendanceuser as user')
  ->select('*')
  ->whereRaw('user.lastpunch ="" ')
//  ->wherein('user.lastpunch','!=','')   
   ->where('user.created_at','=', date('Y-m-d'))   
   ->groupBy('user.empname')
           ->get();
       //   dd($attendance); 
          /*  $report= DB::table('tbl_clientjd_master as jdmast')
              ->join('tbl_clients as cli','cli.client_id','=','jdmast.clientjob_compid')
                ->join('tbl_clientjdrecruitermap as reqmap','reqmap.fk_jdid','=','jdmast.clientjob_id')
            //   ->where('reqmap.created_at','=', date('Y-m-d'))   
           ->get();
            */
             /*$report= DB::table('tbl_clientrec_start_endtime as startend')
               ->select('us.id','us.name as recruiter_name','jdmaster.clientjob_id as jobid','jdmaster.clientjob_title as pos','cli.comp_name as clientjob_title','u.name as spoc',DB::raw('(SELECT count(*) FROM tbl_recruiter_cv as rrrr
                          WHERE rrrr.recruiter_id = us.id and rrrr.position_id=jdmaster.clientjob_id and Date(rrrr.created_at) = CURDATE()
) as upload_cv'),DB::raw('(SELECT count(*) FROM tbl_recruiter_cv as rrrr
                          WHERE rrrr.recruiter_id = us.id and rrrr.position_id=jdmaster.clientjob_id and rrrr.cv_status=2 and Date(rrrr.created_at) = CURDATE()
) as sent_to_client ')

)
               ->join('users as us','us.id','=','startend.clientstart_recuiterid')
                 ->join('tbl_clientjdrecruitermap as jdmap','jdmap.clientreq_id','=','startend.clientreq_id')
             ->join('tbl_clientjd_master as jdmaster','jdmaster.clientjob_id','=','jdmap.fk_jdid')
             ->join('users as u','u.id','=','jdmap.fk_assigneeid')
              ->join('tbl_clients as cli','cli.client_id','=','jdmaster.clientjob_compid')
->whereRaw('Date(startend.created_at) = CURDATE()')
->whereRaw('startend.clientstart_recuiterid=us.id')
->groupBy('startend.clientstartid')
->orderBy('us.id')
           ->get();*/

$report= DB::table('tbl_clientjd_master as jdmast')
  ->select('jdmast.clientjob_title as pos','cli.comp_name as clientjob_title','us.name as recruiter_name','u.name as spoc')
 ->join('tbl_clients as cli','cli.client_id','=','jdmast.clientjob_compid')
   ->join('tbl_clientjdrecruitermap as reqmap','reqmap.fk_jdid','=','jdmast.clientjob_id')
    ->join('users as us','us.id','=','reqmap.fk_empid')
    ->join('users as u','u.id','=','reqmap.fk_assigneeid')
  ->join('tbl_clientrec_start_endtime as startend','startend.clientstart_recuiterid','=','reqmap.fk_empid')
 ->whereRaw('Date(startend.created_at) = CURDATE()')
 ->groupBy('reqmap.fk_jdid','reqmap.fk_empid','reqmap.fk_assigneeid')
 ->get();

    // dd($report);  
    $to = ['shibbu@mindztechnology.com', 'simer@elitehrpractices.com' ,'anupreet@elitehrpractices.com' ,'priyanka@elitehrpractices.com','vikas@elitehrpractices.com','vijitha@mindztechnology.com','bhavana@elitehrpractices.com','bivek@mindztechnology.com','erp@elitehrpractices.com', 'nehaarora@elitehrpractices.com'];  

     //   $to='bivek@mindztechnology.com';
        $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
        $data = array('report' => $report,
                  'attendance' => $attendance);  
        
//dd($data);
Mail::send('Emp.mails.onehrsattposmail',  ['mail_data' => $data], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('One Hrs Attendance Emp mail');

        });
           
    }
    
     public  function birthdayanversary()
    {
    
      $date  = date('d-m-Y');
      $date2  = date('d-m', strtotime($date));

      $sql = "select * from users where emp_dob like '%".date('d-m')."%' ";
      $query = DB::select(DB::raw($sql));

      $sqls = "select * from users where emp_doj like '%".$date2."%' and emp_doj < '".$date."'";
      $querys = DB::select(DB::raw($sqls));
      
      
      $report= DB::table('users')->whereRaw("emp_dob like '%".date('d-m')."%'")->get();
 //echo $report; exit;
 
$to = ['team@elitehrpractices.com','erp@elitehrpractices.com'];  
//$to='bivek@mindztechnology.com';
        $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
    if((count($querys))>0) {
Mail::send('Emp.mails.birthdayanversary',  ['mail_data' => $report], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Birthday Mail');

        });
}

  
      
    }
    
      public  function birthdaywish()
    {
    
      $date  = date('d-m-Y');
      $date2  = date('d-m', strtotime($date));

      $sql = "select * from users where emp_dob like '%".date('d-m')."%' ";
      $query = DB::select(DB::raw($sql));

      $sqls = "select * from users where emp_doj like '%".$date2."%' and emp_doj < '".$date."'";
      $querys = DB::select(DB::raw($sqls));
      
      
      $report= DB::table('users')
 
 ->whereRaw("emp_dob like '%".date('d-m')."%'")
  ->get();
 
 
  
//$to = ['team@mindztechnology.com','team@elitehrpractices.com','erp@elitehrpractices.com'];  
$to='nehaarora@elitehrpractices.com';
        $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
    
Mail::send('Emp.mails.birthdaywish',  ['mail_data' => $report], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Happy Birthday');

        });


  
      
    }
    

public  function prepostbreakmail()
    {
      $data=[];
   $fromdate=date('Y-m-d');
$todate=date('Y-m-d');  
$query="where ( date(ats.created_at) between '".$fromdate."' and '".$todate."' ) "; 
 $users1= ("select ats.punchrecord ,  ats.empname , ats.empcode , st.clientstart_starttime  , st.clientstart_stoptime ,us.id,st.clientreq_id,us.name,us.emp_empid
from
 tbl_clientrec_start_endtime as st inner join users as us on us.id=st.clientstart_recuiterid and ( date(st.created_at) between '".$fromdate."' and '".$todate."' ) 
inner join attendanceuser  as ats on ats.empcode=us.emp_empid ".$query."  group by ats.empcode
 ");


 $pos = DB::select(DB::raw($users1));
//print_r( $pos );
foreach($pos as  $key => $value) {
    
$data[$key]["punchrecord"]=$value->punchrecord;
$data[$key]["empname"]=$value->name;
$data[$key]["empcode"]=$value->emp_empid;
$data[$key]["clientstart_starttime"]=$value->clientstart_starttime;
$data[$key]["clientstart_stoptime"]=$value->clientstart_stoptime;
$data[$key]["id"]=$value->id;
$data[$key]["clientreq_id"]=$value->clientreq_id;


$selectresume=DB::select("select rc.id, rc.cv_upload_time,rc.cv_uploadtime  from tbl_recruiter_cv as rc where rc.recruiter_id=".$value->id." and rc.r_map_id=".$value->clientreq_id." and (date(rc.created_at) between '".$fromdate."' and '".$todate."') " ) ;
$data[$key]["resumes"]=$selectresume;

$selectclients=DB::select("select rc.* from tbl_clientrec_start_endtime as rc where rc.clientstart_recuiterid=".$value->id."  and (date(rc.created_at) between '".$fromdate."' and '".$todate."') " ) ;
$data[$key]["clientstartend"]=$selectclients;

}
 
 
  
//$to = ['team@mindztechnology.com','team@elitehrpractices.com','erp@elitehrpractices.com'];  
 $to = ['bivek@mindztechnology.com','erp@elitehrpractices.com']; 
        $toname="Elite Erp";
        $from='erp@elitehrpractices.com';
    
Mail::send('Emp.mails.prepostbreakmail',  ['mail_data' => $data], function ($message) use ($from, $to, $toname) {

            $message->from($from, 'Elite ERP');
            $message->to($to, $toname);
            $message->subject('Pre Post Break');

        });


  
      
    }




}