<?php 

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Model\Client as Client;
use App\Model\Adminmodel\Role as Role;
use App\Model\Adminmodel\Department as Department;
use App\Model\Adminmodel\Employees as Employees;

use Illuminate\Support\Facades\Auth;
use App\Model\User;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use App\Exceptions\Handler;
use Exception;
use Carbon\Carbon;
// use Datatables;

 
use Illuminate\Http\Request;
 
class InvoiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

   

    public function __construct()
    {
        $this->middleware('auth');
    }
 
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     

 public function invoiceadd(Request $request)
    {   

$assigneeid=Auth::user()->id;
DB::table('tbl_recuiter_invoice')->insert(
    ['recuiter_invoiceselectadd' => $request['selectadd'],'recuiter_invno' => $request['invno'],'recuiter_invoiceamount'=>$request['amount'],'recuiter_invoicepercentage'=>$request['percentage'], 'recuiter_invoicestatus' =>'1', 'recuiter_invoicecreatedat' =>Carbon::now(), 'recuiter_invoiceupdatedat' => Carbon::now(),'recuiter_invoicetotamount'=>$request['billing'],'recuiter_resid'=>$request['rid'],'assigneeid'=>$assigneeid,'recuiter_empid' => $request['empid'],'recuiter_sezstatus' => $request['sezstatus'],'statecgst' => $request['statecgst'],'statesgst' => $request['statesgst'],'stateigst' => $request['stateigst']]
);
   return redirect('joinedcandidate');

    }

public function invoicupdate(Request $request)
    {   
//print_r($_POST); exit;
$assigneeid=Auth::user()->id;

DB::table('tbl_recuiter_invoice as c')
                        ->where(['c.recuiter_resid'=>$request['rid']])
                        ->update(['c.recuiter_invoicestatus'=>'0']);


DB::table('tbl_recuiter_invoice')->insert(
    ['recuiter_invoiceselectadd' => $request['selectadd'],'recuiter_invno' => $request['invno'],'recuiter_invoiceamount'=>$request['amount'],'recuiter_invoicepercentage'=>$request['percentage'], 'recuiter_invoicestatus' =>'1', 'recuiter_invoicecreatedat' =>Carbon::now(), 'recuiter_invoiceupdatedat' => Carbon::now(),'recuiter_invoicetotamount'=>$request['billing'],'recuiter_resid'=>$request['rid'],'recuiter_sezstatus' => $request['sezstatus'],'recuiter_empid' => $request['empid'],'assigneeid'=>$assigneeid,'statecgst' => $request['statecgst'],'statesgst' => $request['statesgst'],'stateigst' => $request['stateigst']]
);
  return redirect('joinedcandidate');

    }

public function generateinvoicepdf($nid)
    { 
      $explodeid=explode('_',$nid);

$invpdf=  DB::table('tbl_recuiter_invoice as c')
->select('*',DB::raw("(select comp_name from tbl_clients where client_id=e.clientsdetails_clientid) as clientname"),DB::raw("(select clientjob_title from tbl_clientjd_master where clientjob_id=d.position_id) as positionname"))
->join('tbl_recruiter_cv as d','c.recuiter_resid','=','d.id')
->join('tbl_clientsdetails as e','e.clientsdetails_id','=','c.recuiter_invoiceselectadd')
->join('tbl_state as f','f.state_id','=','e.clientsdetails_cityid')
->whereRaw('c.recuiter_invoicestatus=1')
->whereRaw('d.id='.$explodeid[1].'')
->orderby('d.id','desc')
->first();
//print_r($invpdf); exit;
//echo $nid; exit;
return view('admin.joininginvoicepdf',compact('invpdf'));
    }
public function statewisetax()
    { 

$dashbd=  DB::table('tbl_state as c')->select('*')->whereRaw('c.state_status!=2')->orderby('state_id','desc')->get();
return view('admin.statewisetax',compact('dashbd'));
    }

public function addstatetax()
    { 
 $country=  DB::table('tbl_country')->select('*')->get();

// dd($country);
  return view('admin.addstatetax',compact('country'));
    }

public function savestatetax(REQUEST $request)
    { 
      //  print_r($_POST);
        DB::table('tbl_state')->insert(
    ['state_name' => $request['statename'],'countid'=>$request['cname'],'state_cgst'=>$request['cgst'],'state_sgst'=>$request['sgst'], 'state_igst' =>$request['igst'], 'state_status' =>'1', 'state_createdat' => Carbon::now(),'state_updatedat'=>Carbon::now()]);

 return redirect()->route('statewisetax');
    }
public function deletestate($id)
    { 


            try {

               DB::table('tbl_state as c')
                        ->where(['c.state_id'=>$id])
                        ->update(['c.state_status'=>'0']);
                return redirect('statewisetax')->with('message', 'You have successfully Deactive');

            } catch (Exception $e) {
                return redirect()->route('admin.statewisetax')->with('message', 'Error For  Deactive');
            }  

}
public function reactivestate($id)
    { try {

               DB::table('tbl_state as c')
                        ->where(['c.state_id'=>$id])
                        ->update(['c.state_status'=>'1']);
                return redirect('statewisetax')->with('message', 'You have successfully Active');

            } catch (Exception $e) {
                return redirect()->route('admin.statewisetax')->with('message', 'Error For  Active');
            }  

}

public function editstate($id)
    { try {
           $dashbd=  DB::table('tbl_state as c')->select('*')->where(['c.state_id'=>$id])->orderby('state_id','desc')->first();
            $country=  DB::table('tbl_country')->select('*')->get();
          // dd($dashbd);
           return view('admin.statewisetaxedit',compact('dashbd','country','id'));

            } catch (Exception $e) {
                return redirect('statewisetax')->with('message', 'Error For  Edit State');
            }  

}

public function updatestatetax(REQUEST $request)
    { try {
        
//print_r($_POST); exit;
             DB::table('tbl_state as c')
                        ->where(['c.state_id'=>$request['id']])
                        ->update(['c.countid'=>$request['cname'],'c.state_name'=>$request['statename'],'c.state_cgst'=>$request['cgst'],'c.state_sgst'=>$request['sgst'],'c.state_igst'=>$request['igst']]);

          // dd($dashbd);
          return redirect('statewisetax')->with('message', '  Edit State Successfully');

            } catch (Exception $e) {
                return redirect('statewisetax')->with('message', 'Error For  Edit State');
            }  

}


public function deletestateval($id)
    { try {

               DB::table('tbl_state as c')
                        ->where(['c.state_id'=>$id])
                        ->update(['c.state_status'=>'2']);
                return redirect('statewisetax')->with('message', 'You have successfully Delete Data');

            } catch (Exception $e) {
                return redirect()->route('admin.statewisetax')->with('message', 'Error For   Delete Data');
            }  

}

}
