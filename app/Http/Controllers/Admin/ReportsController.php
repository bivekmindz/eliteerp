<?php 

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Model\Client as Client;
use App\Model\Adminmodel\Role as Role;
use App\Model\Adminmodel\Department as Department;
use App\Model\Adminmodel\Employees as Employees;

use Illuminate\Support\Facades\Auth;
use App\Model\User;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use App\Exceptions\Handler;
use Exception;
// use Datatables;

 
use Illuminate\Http\Request;
 
class ReportsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $redirectTo = '/admin/dashboard';
    protected $redirectAfterLogout = '/admin/login';

    public function __construct()
    {
        $this->middleware('auth');
    }
 
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     

 public function updaterecresume(Request $request)
    {
 $loginid = Auth::user()->id;
 //Array ( [_token] => dQjQyIviG5PswpB84u8Dmeiz7lvvXXgUjKhJ6uiG [sel] => 790/6/4 [expectedsaleryval] => 00 [Doj] => 2018-08-24 [expDoj] => [count] => 790 [countval] => 1 )


 //Array ( [_token] => ddBDm6HB0UMOwj73IRUhyqOduIwHuG59AObye67L [sel] => 1253/15/3 [expectedsalery] => [Doj] => [expDoj] => [count] => 1253 [countval] => 1 [placepopupval] => 1253 [keyval] => 0 [actmode] => 3 )
//print_r($_POST ); exit;
//Array ( [_token] => 2p09cbVnJ2AASWrDv7qI9GJ3wkiqdcKprZXuKcF8 [expectedsalery] => [Doj] => [expDoj] => [count] => 1253 [countval] => 1 [placepopupval] => 1253 [keyval] => 1111 [actmode] => 3 )
//Array ( [_token] => UdyQzMGEwwuYmsKz8ZPg8GAQzDxvqtaVH0Nf8jcx [sel] => 1337/15/5 [expectedsalery] => [Doj] => 2018-08-19 [expDoj] => 2018-08-05 [count] => 1337 [countval] => 1 [placepopupval] => 1337 [keyval] => 0 [actmode] => 3 )
 if(isset($request['countval']))
     {

    $candidate_recruiter_array = explode('/',$request['sel']);
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$request['placepopupval']])
                        ->update(['c.expectedsalery'=>$request['expectedsalery'],'c.dateoj'=>$request['Doj'],'c.expDoj'=>$request['expDoj']]);
    //echo "ddddddd";loginid
DB::table('tbl_recruiter_cvchangestatus')
                        ->where(['cvid'=>$request['count']])
                        ->update(['cvupdatestatus'=>'0']);


DB::table('tbl_recruiter_cvchangestatus')->insert(
    ['cvid' => $request['count'],'loginid'=>$loginid,'recuiterid'=>$loginid, 'dateoj' => $request['Doj'], 'expectedsalery' => $request['expectedsalery'], 'expDoj' => $request['expDoj'], 'cvupdatestatus' => '1', 'cv_status' =>$candidate_recruiter_array[2]]
);

}


if(isset($request['sel'])) {
     
              $candidate_recruiter_array = explode('/',$request['sel'] );
//print_r( $candidate_recruiter_array); exit;

DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$candidate_recruiter_array[0]])
                        ->update(['c.cv_status'=>$candidate_recruiter_array[2]]);



$val=$candidate_recruiter_array[0];
 $rec_email = DB::table('tbl_recruiter_cv as rc')
                       ->join('users as u', 'rc.recruiter_id', '=', 'u.id')
                        ->select('u.email', 'u.name')
                        ->where(['rc.recruiter_id' => $candidate_recruiter_array[1]])
                        ->first();
                      //  dd( $rec_email);
 
                    $complete_shortlist = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->where('rc.id', $candidate_recruiter_array[0])
                         ->where('rc.cv_status', '2')
                        // ->where(['rc.id'=>$v])
                        ->get();
                      //  dd($complete_shortlist );
                    $complete_unshortlist=0;

           //        $sendMail = $this->sendMail($complete_shortlist,$complete_unshortlist,$request['poid'],$loginid,$rec_email );



}

 $assigneeid=Auth::user()->id;
       // echo $assigneeid;
        /*$candidate=DB::table('tbl_recruiter_cv as rcv')
            ->select('rcv.*','us.name as username')
             ->join('users as us','us.id','=','rcv.recruiter_id')
            ->where(['rcv.id'=>$request['placepopupval'] ])
            ->orderBy('cv_status', 'ASC')
            ->get();


         echo $candidate;*/

        $key = $_POST['keyval'];
        $act_mode = $_POST['actmode'];
        // $pos_id = $request['posid'];

        $assigneeid=Auth::user()->id;
        // $comp =DB::select(DB::raw("Select c.client_id from tbl_clients as c inner join tbl_clientjd_master as jd on jd.clientjob_compid=c.client_id where jd.clientjob_id=$pos_id"));

        if($act_mode==1){

            $result="select rcv.*,u.name,jd.clientjob_title,jd.clientjob_id,jd.drive_time,jd.shortlist_time,jd.lineup_to,c.comp_name,c.client_id from tbl_recruiter_cv as rcv inner join users as u on u.id=rcv.recruiter_id inner join tbl_clientjd_master as jd on jd.clientjob_id=rcv.position_id inner join tbl_clients as c on c.client_id=jd.clientjob_compid where rcv.candidate_name like '%".$key."%'";

        }

        else if($act_mode==2){
            $result="select rcv.*,u.name,jd.clientjob_title,jd.clientjob_id,jd.drive_time,jd.shortlist_time,jd.lineup_to,c.comp_name,c.client_id from tbl_recruiter_cv as rcv inner join users as u on u.id=rcv.recruiter_id inner join tbl_clientjd_master as jd on jd.clientjob_id=rcv.position_id inner join tbl_clients as c on c.client_id=jd.clientjob_compid where rcv.candidate_email like '%".$key."%'";

        }
        else{
            $result="select rcv.*,u.name,jd.clientjob_title,jd.clientjob_id,jd.drive_time,jd.shortlist_time,jd.lineup_to,c.comp_name,c.client_id from tbl_recruiter_cv as rcv inner join users as u on u.id=rcv.recruiter_id inner join tbl_clientjd_master as jd on jd.clientjob_id=rcv.position_id inner join tbl_clients as c on c.client_id=jd.clientjob_compid where rcv.candidate_mob like '%".$key."%'";

        }
 
        
        $pos = DB::select(DB::raw($result));



     echo "<pre>"; 
       // print_r($pos);

                echo '<tr>
                        <th>Candidate Name</th>
                        <th>Candidate Mobile</th>
                        <th>Company Name</th>
                        <th>Position Name</th>
                        <th>Recruiter Name</th>
                       <th>Status</th>
                         <th>Action</th>
                    </tr>';

      
                foreach ($pos as $key => $value) {
                 //   if(($pos[$key]->cv_status!=4)){


$str = $value->created_at;
              $str = explode(" ", $str);
              $aa =explode("-", $str[0]);
              // print_r($aa); 
              

              $url='/storage/app/uploadcv/'.$aa[0]."/".$aa[1]."/".$aa[2]."/".$value->cv_name;
              // print_r($url);

if($value->cv_status=='1') { $cvstatus= "Not Seen";}elseif($value->cv_status=='2') { $cvstatus= "Send To Client";}elseif($value->cv_status=='3') { $cvstatus= "Rejected";}elseif($value->cv_status=='4') { $cvstatus= "Offer";}elseif($value->cv_status=='5') { $cvstatus= "Pipeline";}elseif($value->cv_status=='6') { $cvstatus= "Joined";}
elseif($value->cv_status=='15') { $cvstatus= "Hold";}
elseif($value->cv_status=='7') { $cvstatus= "On The Way";}
elseif($value->cv_status=='9') { $cvstatus= "Not Going";}
elseif($value->cv_status=='17') { $cvstatus= "Not Responding";}
elseif($value->cv_status=='18') { $cvstatus= "Confirmed";}
elseif($value->cv_status=='16') { $cvstatus= "Drop";}
elseif($value->cv_status=='19') { $cvstatus= "Terminate";}
elseif($value->cv_status=='20') { $cvstatus= "Postpone";}
elseif($value->cv_status=='21') { $cvstatus= "Reject By Client";}
elseif($value->cv_status=='22') { $cvstatus= "Sortlist By Client";}
else{
    
     $cvstatus= "Not Updated";
}
         
                  $string  = "'".$value->candidate_name."'";
                  echo  '<tr class="table-active">
                        
                        <td>'.$value->candidate_name.'</td>
                        <td>'.$value->candidate_mob.' </td>
                        <td>'.$value->comp_name.'</td>
                        <td>'.$value->clientjob_title.'</td>
                        <td>'.$value->name.'</td>
                          <td>'.$cvstatus.'</td>
                        <td><a href="'.$url.'">Download</a>&nbsp; <button type="button"  class="det_css one-click" data-toggle="modal" data-target="#myModal" onclick="getposiionvalue('.$value->id.'),getupdatespock('.$value->id.','.$value->recruiter_id.','.$value->cv_status.','.$value->position_id.')(this.value),getpositionspock('.$value->id.','.$value->recruiter_id.','.$value->cv_status.')" ><span class="icofont icofont-eye-alt"></span></button>
<Input type="hidden" name="keyval" value='.$key.' id="keyval">
<Input type="hidden" name="actmode" value='.$act_mode.' id="actmode">

                        </td>
   </td>
                        
                     
                        </tr>
                        ';
                          }

                   //   }
                    

            
}      

 public function updatespockresume(Request $request)
    {
 $loginid = Auth::user()->id;
 //Array ( [_token] => dQjQyIviG5PswpB84u8Dmeiz7lvvXXgUjKhJ6uiG [sel] => 790/6/4 [expectedsaleryval] => 00 [Doj] => 2018-08-24 [expDoj] => [count] => 790 [countval] => 1 )

 if(isset($request['countval']))
     {

    $candidate_recruiter_array = explode('/',$request['sel']);
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$request['count']])
                        ->update(['c.expectedsalery'=>$request['expectedsalery'],'c.dateoj'=>$request['Doj'],'c.expDoj'=>$request['expDoj']]);
    //echo "ddddddd";loginid
DB::table('tbl_recruiter_cvchangestatus')
                        ->where(['cvid'=>$request['count']])
                        ->update(['cvupdatestatus'=>'0']);


DB::table('tbl_recruiter_cvchangestatus')->insert(
    ['cvid' => $request['count'],'loginid'=>$loginid,'recuiterid'=>$loginid, 'dateoj' => $request['Doj'], 'expectedsalery' => $request['expectedsalery'], 'expDoj' => $request['expDoj'], 'cvupdatestatus' => '1', 'cv_status' =>$candidate_recruiter_array[2]]
);

}


 /*





     if(isset($request['countval']))
     {
  $candidate_recruiter_array = explode('/',$request['sel']);
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$request['count']])
                        ->update(['c.expectedsalery'=>$request['expectedsalery'],'c.dateoj'=>$request['Doj'],'c.expDoj'=>$request['expDoj']]);
    //echo "ddddddd";loginid
DB::table('tbl_recruiter_cvchangestatus')
                        ->where(['cvid'=>$request['count']])
                        ->update(['cvupdatestatus'=>'0']);

DB::table('tbl_recruiter_cvchangestatus')->insert(
    ['cvid' => $request['count'],'loginid'=>$loginid,'recuiterid'=>$loginid, 'dateoj' => $request['Doj'], 'expectedsalery' => $request['expectedsalery'], 'expDoj' => $request['expDoj'], 'cvupdatestatus' => '1', 'cv_status' =>$candidate_recruiter_array[2]]



     }
*/

if(isset($request['sel'])) {
     
              $candidate_recruiter_array = explode('/',$request['sel'] );
//print_r( $candidate_recruiter_array); exit;

DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$candidate_recruiter_array[0]])
                        ->update(['c.cv_status'=>$candidate_recruiter_array[2]]);



$val=$candidate_recruiter_array[0];
 $rec_email = DB::table('tbl_recruiter_cv as rc')
                       ->join('users as u', 'rc.recruiter_id', '=', 'u.id')
                        ->select('u.email', 'u.name')
                        ->where(['rc.recruiter_id' => $candidate_recruiter_array[1]])
                        ->first();
                      //  dd( $rec_email);
 
                    $complete_shortlist = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->where('rc.id', $candidate_recruiter_array[0])
                         ->where('rc.cv_status', '2')
                        // ->where(['rc.id'=>$v])
                        ->get();
                      //  dd($complete_shortlist );
                    $complete_unshortlist=0;

           //        $sendMail = $this->sendMail($complete_shortlist,$complete_unshortlist,$request['poid'],$loginid,$rec_email );



}



 $assigneeid=Auth::user()->id;
       // echo $assigneeid;
        $candidate=DB::table('tbl_recruiter_cv as rcv')
            ->select('rcv.*','us.name as username')
             ->join('users as us','us.id','=','rcv.recruiter_id')
            ->where(['rcv.position_id'=>$request['placepopupval'] ])
            ->orderBy('cv_status', 'ASC')
            ->get();


         echo $candidate;


//print_r($_POST);
}


        public function getresumespopup($id)
        {
            $html="";
          $sql = "select * from  tbl_recruiter_cv where id IN (".$id.") ";

      $recruiters = DB::select(DB::raw($sql)); 
       $html .= '<table width="100%"><tr><td>Name</td><td>Mobile No</td></tr>';
  foreach($recruiters as $key => $val){
$cid=$val->cv_status;
 if($cid=='1')
            {
              $sel1='selected';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16='';$sel21='';   $sel22='';   
            }
             elseif($cid=='2')
            {
              $sel1='';$sel2='selected';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16='';$sel21='';   $sel22='';   
            }
            elseif($cid=='3')
            {
              $sel1='';$sel2='';$sel3='selected';$sel4='';$sel5='';$sel6='';$sel15='';$sel16='';$sel21='';   $sel22='';   
            }
            elseif($cid=='4')
            {
              $sel1='';$sel2='';$sel3='';$sel4='selected';$sel5='';$sel6='';$sel15='';$sel16='';$sel21='';   $sel22='';   
            }
            elseif($cid=='5')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='selected';$sel6='';$sel15='';$sel16='';$sel21='';   $sel22='';   
            }
            elseif($cid=='6')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='selected';$sel15='';$sel16='';$sel21='';   $sel22='';   
            }
              elseif($cid=='15')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='selected';$sel15='';$sel16='';$sel21='';   $sel22='';   
            }
              elseif($cid=='15')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='selected';$sel16='';$sel21='';   $sel22='';   
            }
              elseif($cid=='16')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16='selected';$sel21='';   $sel22='';   
            }
                elseif($cid=='21')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16='';$sel21='selected';   $sel22='';   
            }
                elseif($cid=='22')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16='';$sel21='';   $sel22='selected';   
            }
            else
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='';   $sel22='';     
            }

  /*$html .= '  <tr>
                <td>'.$val->candidate_name.'</td>  <td>'.$val->candidate_mob.'</td> 
                <td>
<select class="form-control col-xs-6" required="" id="recruiterspockup" name="checkbox_data[]" onchange="changestatusdata(this.value)"><option value="'.$val->id.'/'.$val->position_id.'/1" '.$sel1.'>Not Seen</option>
              <option value="'.$val->id.'/'.$val->position_id.'/2"  '.$sel2.'>Send To Client</option>
              <option value="'.$val->id.'/'.$val->position_id.'/3"  '.$sel3.'>Rejected</option>
              <option value="'.$val->id.'/'.$val->position_id.'/4"   '.$sel4.'>Offer</option>
              <option value="'.$val->id.'/'.$val->position_id.'/5"  '.$sel5.'>Pipeline</option>
              <option value="'.$val->id.'/'.$val->position_id.'/6"  '.$sel6.'>Join</option></select>
                </td>
                <td><div id="place'.$val->id.''.$val->position_id.'"></div></td>
            </tr>';*/

            $html .= '  <tr>
                <td>'.$val->candidate_name.'</td>  <td>'.$val->candidate_mob.'</td> 
            <input type="hidden" name="canddata[]" value="'.$val->id.'"> 
            </tr>';


  }

    $html .= '  <tr>
                <td><select class="form-control col-xs-6" required="" id="recruiterspockup" name="checkbox_data" onchange="changestatusdata(this.value)"><option value="'.$val->id.'/'.$val->position_id.'/1" '.$sel1.'>Not Seen</option>
              <option value="'.$val->id.'/'.$val->position_id.'/2"  '.$sel2.'>Send To Client</option>
              <option value="'.$val->id.'/'.$val->position_id.'/3"  '.$sel3.'>Rejected</option>
              <option value="'.$val->id.'/'.$val->position_id.'/4"   '.$sel4.'>Offer</option>
              <option value="'.$val->id.'/'.$val->position_id.'/5"  '.$sel5.'>Pipeline</option>
              <option value="'.$val->id.'/'.$val->position_id.'/6"  '.$sel6.'>Join</option>
              <option value="'.$val->id.'/'.$val->position_id.'/15"  '.$sel15.'>Hold</option>
               <option value="'.$val->id.'/'.$val->position_id.'/16"  '.$sel15.'>Drop</option>
                <option value="'.$val->id.'/'.$val->position_id.'/21"  '.$sel21.'>Reject By Client</option>
                 <option value="'.$val->id.'/'.$val->position_id.'/22"  '.$sel22.'>Sortlist By Client</option>
              </select></td>  <td><div id="place'.$val->id.''.$val->position_id.'"></div></td> 
             
            </tr>';


    $html .= '</table>';
echo $html;
         
        }
        
        public function getstatus($id,$rid,$cid,$pid)
        {
        //   echo $id.'--'.$rid.'---'.$cid.'--'.$pid; exit;
  $query = "select rcv.*  from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where rcv.id=".$id." and r.cvupdatestatus=1"; 
      $users = DB::select(DB::raw($query));
//echo count($users); exit;
//print_r(  $users); exit;




            $html = ''; 
                       if($cid=='1')
            {
              $sel1='selected';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='';$sel22=''; 
            }
             elseif($cid=='2')
            {
              $sel1='';$sel2='selected';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='';$sel22=''; 
            }
            elseif($cid=='3')
            {
              $sel1='';$sel2='';$sel3='selected';$sel4='';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='';$sel22=''; 
            }
            elseif($cid=='4')
            {
              $sel1='';$sel2='';$sel3='';$sel4='selected';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='';$sel22=''; 
            }
            elseif($cid=='5')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='selected';$sel6='';$sel15='';$sel16=''; $sel21='';$sel22=''; 
            }
            elseif($cid=='6')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='selected';$sel15='';$sel16=''; $sel21='';$sel22=''; 
            }elseif($cid=='15')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='selected';$sel16=''; $sel21='';$sel22=''; 
            }
             elseif($cid=='16')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16='selected'; $sel21='';$sel22=''; 
            }
             elseif($cid=='21')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='selected';$sel22=''; 
            }
             elseif($cid=='22')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='';$sel22='selected'; 
            }
            
            else
            {
               $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='';$sel22=''; 
            }
              // $html .= '<option value="'.$id.'/'.$rid.'/1" '.$sel1.'>Not Seen</option>
              // <option value="'.$id.'/'.$rid.'/2" '.$sel2.'>Send To Client</option>
              // <option value="'.$id.'/'.$rid.'/3" '.$sel3.'>Rejected</option>
              $html .='
                <select class="form-control col-xs-6" required  name="recruiter[]"  onchange="changestatuspopupdata(this.value)" id="recruiter" >
                             <option value="">Select Status</option>                   
                         
              <option value="'.$id.'/'.$rid.'/4" '.$sel4.'>Offer</option>
              <option value="'.$id.'/'.$rid.'/5" '.$sel5.'>Pipeline</option>
              <option value="'.$id.'/'.$rid.'/6" '.$sel6.'>Join</option>
               <option value="'.$id.'/'.$rid.'/15" '.$sel15.'>Hold</option>
                 <option value="'.$id.'/'.$rid.'/16" '.$sel16.'>Drop</option>
                   <option value="'.$id.'/'.$rid.'/21" '.$sel21.'>Reject By Client</option>
                     <option value="'.$id.'/'.$rid.'/22" '.$sel22.'>Shortlist By Client</option>
             </select>';
             $html .='<div id="placepopup">';
          if($cid=='4')
          {
              if(count($users)>0) {
               $html .='   <input type="text" name="expectedsalery[]" placeholder="Expected Salary" class="form-control" id="expectedsalery"  value="'.$users[0]->expectedsalery.' ">
               <input type="hidden" name="expDoj[]" placeholder="Expected Salary" class="form-control" id="expDoj"  value="'.$users[0]->expDoj.' ">
             <input type="date" name="Doj[]" class="form-control" placeholder="Date Of Joining" id="Doj"   value="'.$users[0]->dateoj.'">
             <input type="hidden" name="count[]" class="form-control col-xs-6" value="'.$id.'" id="count"><input type="hidden" name="countval[]" value="1" id="countval">';
              }
          }
            elseif($cid=='6')
          {
                 if(count($users)>0) {
               $html .='   <input type="text" name="expectedsalery[]" placeholder="Expected Salary" class="form-control" id="expectedsalery"  value="'.$users[0]->expectedsalery.' " required>
             <input type="date" name="Doj[]" class="form-control" placeholder="Date Of Joining" id="Doj"   value="'.$users[0]->dateoj.'" required>
               <input type="hidden" name="expDoj[]" placeholder="Expected Salary" class="form-control" id="expDoj"  value="'.$users[0]->expDoj.' " required>
             <input type="hidden" name="count[]" class="form-control col-xs-6" value="'.$id.'"  id="count"><input type="hidden" name="countval[]" value="1" id="countval">';
                 }
          }
               elseif($cid=='5')
          {
                 if(count($users)>0) {
               $html .='   <input type="hidden" name="expectedsalery[]" placeholder="Expected Salary" class="form-control" id="expectedsalery"  value="'.$users[0]->expectedsalery.' ">
             <input type="hidden" name="Doj[]" class="form-control" placeholder="Date Of Joining" id="Doj"   value="'.$users[0]->expDoj.'">
              <input type="date" name="expDoj[]" class="form-control" placeholder="Date Of Joining" id="expDoj"   value="'.$users[0]->expDoj.'">
             <input type="hidden" name="count[]" class="form-control col-xs-6" value="'.$id.'"  id="count"><input type="hidden" name="countval[]" value="1" id="countval">';
                 }
          }
               $html .=' <input type="hidden" name="cid" value="'.$cid.'" id="cid"></div> ';
             $html .='</div> ';
           
             
            
//echo $id.'----'.$rid.'---'.$cid;
echo $html;
}


 public function updateclientstatuspopupresume(Request $request)
    {
        
        
  //   print_r($_POST); 
$loginid = Auth::user()->id;
if(isset( $request['expDoj']) and  $request['expDoj']!='') {  $request['expDoj']= $request['expDoj'];} else {  $request['expDoj']='';} 
     if(isset($request['canddata']))
     {
for($i=0;$i<count($request['canddata']);$i++)
{ $candidate_recruiter_array = explode('/',$request['checkbox_data']);
//print_r($_POST);
   // echo $request['expectedsalery'][$i].'--'.$request['Doj'][$i].'-----'.$request['expDoj'][$i];
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$request['canddata'][$i]])
                        ->update(['c.expectedsalery'=>$request['expectedsalery'],'c.dateoj'=>$request['Doj'],'c.expDoj'=>$request['expDoj']]);
    //echo "ddddddd";
DB::table('tbl_recruiter_cvchangestatus')
                        ->where(['cvid'=>$request['canddata'][$i]])
                        ->update(['cvupdatestatus'=>'0']);


DB::table('tbl_recruiter_cvchangestatus')->insert(
    ['cvid' => $request['canddata'][$i],'loginid'=>$loginid,'recuiterid'=>$loginid, 'dateoj' => $request['Doj'], 'expectedsalery' => $request['expectedsalery'], 'expDoj' => $request['expDoj'], 'cvupdatestatus' => '1', 'cv_status' =>$candidate_recruiter_array[2]]
);

}}



if(isset($request['canddata'])) {
      foreach($request['canddata'] as $kkk=>$vvv)
            {
              $candidate_recruiter_array = explode('/',$request['checkbox_data']);
//echo $vvv;
//print_r( $candidate_recruiter_array );
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$vvv])
                        ->update(['c.cv_status'=>$candidate_recruiter_array[2]]);



$val=$vvv;
 $rec_email = DB::table('tbl_recruiter_cv as rc')
                       ->join('users as u', 'rc.recruiter_id', '=', 'u.id')
                        ->select('u.email', 'u.name')
                        ->where(['rc.recruiter_id' => $candidate_recruiter_array[1]])
                        ->first();
                      //  dd( $rec_email);
 
                    $complete_shortlist = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->where('rc.id', $vvv)
                         ->where('rc.cv_status', '2')
                        // ->where(['rc.id'=>$v])
                        ->get();
                      //  dd($complete_shortlist );
                    $complete_unshortlist=0;

           //        $sendMail = $this->sendMail($complete_shortlist,$complete_unshortlist,$request['poid'],$loginid,$rec_email );



}}


     session::flash('success','Message Sent');
                return redirect('shortlistcv');
 
        
        
        
    }

        
 public function updateclientstatuspopup(Request $request)
    {
 $loginid = Auth::user()->id;


     if(isset($request['countval']))
     {
for($i=0;$i<count($request['countval']);$i++)
{ $candidate_recruiter_array = explode('/',$request['recruiter'][$i]);
   // echo $request['expectedsalery'][$i].'--'.$request['Doj'][$i].'-----'.$request['expDoj'][$i];
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$request['count'][$i]])
                        ->update(['c.expectedsalery'=>$request['expectedsalery'][$i],'c.dateoj'=>$request['Doj'][$i],'c.expDoj'=>$request['expDoj'][$i]]);
    //echo "ddddddd";
DB::table('tbl_recruiter_cvchangestatus')
                        ->where(['cvid'=>$request['count'][$i]])
                        ->update(['cvupdatestatus'=>'0']);


DB::table('tbl_recruiter_cvchangestatus')->insert(
    ['cvid' => $request['count'][$i],'loginid'=>$loginid,'recuiterid'=>$loginid, 'dateoj' => $request['Doj'][$i], 'expectedsalery' => $request['expectedsalery'][$i], 'expDoj' => $request['expDoj'][$i], 'cvupdatestatus' => '1', 'cv_status' =>$candidate_recruiter_array[2]]
);

}}


if(isset($request['recruiter'])) {
      foreach($request['recruiter'] as $kkk=>$vvv)
            {
              $candidate_recruiter_array = explode('/',$vvv);


DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$candidate_recruiter_array[0]])
                        ->update(['c.cv_status'=>$candidate_recruiter_array[2]]);



$val=$candidate_recruiter_array[0];
 $rec_email = DB::table('tbl_recruiter_cv as rc')
                       ->join('users as u', 'rc.recruiter_id', '=', 'u.id')
                        ->select('u.email', 'u.name')
                        ->where(['rc.recruiter_id' => $candidate_recruiter_array[1]])
                        ->first();
                      //  dd( $rec_email);
 
                    $complete_shortlist = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->where('rc.id', $candidate_recruiter_array[0])
                         ->where('rc.cv_status', '2')
                        // ->where(['rc.id'=>$v])
                        ->get();
                      //  dd($complete_shortlist );
                    $complete_unshortlist=0;

           //        $sendMail = $this->sendMail($complete_shortlist,$complete_unshortlist,$request['poid'],$loginid,$rec_email );



}}

     session::flash('success','Message Sent');
                return redirect()->route('shortlistcv');
    }

public function getspockposition($id)
{
  echo $id;
}
    public function getspockstatus($id,$rid,$cid)
        {
            $html = ''; 
                       if($cid=='1')
            {
              $sel1='selected';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='';$sel22=''; 
            }
             elseif($cid=='2')
            {
              $sel1='';$sel2='selected';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='';$sel22=''; 
            }
            elseif($cid=='3')
            {
              $sel1='';$sel2='';$sel3='selected';$sel4='';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='';$sel22=''; 
            }
            elseif($cid=='4')
            {
              $sel1='';$sel2='';$sel3='';$sel4='selected';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='';$sel22=''; 
            }
            elseif($cid=='5')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='selected';$sel6='';$sel15='';$sel16=''; $sel21='';$sel22=''; 
            }
            elseif($cid=='6')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='selected';$sel15='';$sel16=''; $sel21='';$sel22=''; 
            }elseif($cid=='15')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='selected';$sel16=''; $sel21='';$sel22=''; 
            }
             elseif($cid=='16')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16='selected'; $sel21='';$sel22=''; 
            }
             elseif($cid=='21')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='selected';$sel22=''; 
            }
             elseif($cid=='22')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='';$sel22='selected'; 
            }
            
            else
            {
               $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel15='';$sel16=''; $sel21='';$sel22=''; 
            }
              $html .= '<option value="'.$id.'/'.$rid.'/1" '.$sel1.'>Not Seen</option>
              <option value="'.$id.'/'.$rid.'/2" '.$sel2.'>Send To Client</option>
              <option value="'.$id.'/'.$rid.'/3" '.$sel3.'>Rejected</option>
              <option value="'.$id.'/'.$rid.'/4" '.$sel4.'>Offer</option>
              <option value="'.$id.'/'.$rid.'/5" '.$sel5.'>Pipeline</option>
              <option value="'.$id.'/'.$rid.'/6" '.$sel6.'>Join</option>
               <option value="'.$id.'/'.$rid.'/15" '.$sel15.'>Hold</option>
                 <option value="'.$id.'/'.$rid.'/16" '.$sel16.'>Drop</option>
                   <option value="'.$id.'/'.$rid.'/21" '.$sel21.'>Reject By Client</option>
                     <option value="'.$id.'/'.$rid.'/22" '.$sel22.'>Sortlist By Client</option>
              ';
//echo $id.'----'.$rid.'---'.$cid;
echo $html;
}

     public function updateclientstatusrecuiter(Request $request)
    {
 $loginid = Auth::user()->id;

// print_r($request['recruiter'][0]); exit;
//print_r($request['countval']); exit;
    
     if(isset($request['countval']))
     {
for($i=0;$i<count($request['countval']);$i++)
{
    $candidate_recruiter_array = explode('/',$request['recruiter'][$i]);
//print_r($candidate_recruiter_array[2]); exit;

//print_r($request['count'][$i]); exit;
   // echo $request['expectedsalery'][$i].'--'.$request['Doj'][$i].'-----'.$request['expDoj'][$i];
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$request['count'][$i]])
                        ->update(['c.expectedsalery'=>$request['expectedsalery'][$i],'c.dateoj'=>$request['Doj'][$i],'c.expDoj'=>$request['expDoj'][$i]]);
    //echo "ddddddd";loginid
DB::table('tbl_recruiter_cvchangestatus')
                        ->where(['cvid'=>$request['count'][$i]])
                        ->update(['cvupdatestatus'=>'0']);


DB::table('tbl_recruiter_cvchangestatus')->insert(
    ['cvid' => $request['count'][$i],'loginid'=>$loginid,'recuiterid'=>$loginid, 'dateoj' => $request['Doj'][$i], 'expectedsalery' => $request['expectedsalery'][$i], 'expDoj' => $request['expDoj'][$i], 'cvupdatestatus' => '1', 'cv_status' =>$candidate_recruiter_array[2]]
);

}}


if(isset($request['recruiter'])) {
      foreach($request['recruiter'] as $kkk=>$vvv)
            {
              $candidate_recruiter_array = explode('/',$vvv);


DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$candidate_recruiter_array[0]])
                        ->update(['c.cv_status'=>$candidate_recruiter_array[2]]);



$val=$candidate_recruiter_array[0];
 $rec_email = DB::table('tbl_recruiter_cv as rc')
                       ->join('users as u', 'rc.recruiter_id', '=', 'u.id')
                        ->select('u.email', 'u.name')
                        ->where(['rc.recruiter_id' => $candidate_recruiter_array[1]])
                        ->first();
                      //  dd( $rec_email);
 
                    $complete_shortlist = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->where('rc.id', $candidate_recruiter_array[0])
                         ->where('rc.cv_status', '2')
                        // ->where(['rc.id'=>$v])
                        ->get();
                      //  dd($complete_shortlist );
                    $complete_unshortlist=0;

           //        $sendMail = $this->sendMail($complete_shortlist,$complete_unshortlist,$request['poid'],$loginid,$rec_email );



}}

     session::flash('success','Message Sent');
                return redirect('get-assigned-postion/'.$request['clientreqEncrypt']);
    }

    public function index(Request $request)
    {
        //dd(session(['adminsess'=>'abcd']));


        return view('admin.adashboard');
    }

     public function Sevenoclockclientreports()
    {
        
    $usersval= DB::select("select * from users where emp_role like '%3%'");      
        
        if(isset($_GET['user']) && $_GET['user']!='' )
     {

  if(isset($_GET['doj'])  && $_GET['doj']!='')
     {
         $from=$_GET['doj'];
         $to=$_GET['doend'];
     }
    
     else
     {
        $from=date('Y-m-d');
        $to=date('Y-m-d');
     }
     
      if($_GET['user']!='') {
     $users= DB::select("select * from users where  name='".$_GET['user']."' and  emp_role like '%3%'");
     }
     else
     {
          $users= DB::select("select * from users where  emp_role like '%3%'");
     }
     
     
     }
     
 else
 {
      if(isset($_GET['doj']))
     {
         $from=$_GET['doj'];
         $to=$_GET['doend'];
     }
    
     else
     {
        $from=date('Y-m-d');
        $to=date('Y-m-d');
     }
     $users= DB::select("select * from users where emp_role like '%3%'");
 }
     
     
$data=[];




  foreach($users as  $key => $value) {
    
$data[$key]["username"]=$value->name;
$data[$key]["empcode"]=$value->id;
$data[$key]["state"]=$value->emp_status;

$users1= DB::select("SELECT count(rv.id) as trtr from tbl_recruiter_cv as rv inner join tbl_clientjd_master 
as cm on cm.clientjob_id=rv.position_id where   
rv.recruiter_id=".$value->id." and   ( Date(rv.created_at) BETWEEN '".$from."' AND '".$to."'  )  group by rv.position_id")

;

$data[$key]["userreport"]=$users1;

$users2= DB::select("SELECT count(rv.id) as sendclient, cm.clientjob_title as position  from tbl_recruiter_cv as rv inner join tbl_clientjd_master 
as cm on cm.clientjob_id=rv.position_id where 
rv.recruiter_id=".$value->id." and      ( Date(rv.created_at) BETWEEN '".$from."' AND '".$to."'  )  and rv.cv_status=2  group by rv.position_id");

$data[$key]["userreport2"]=$users2;

$users3= DB::select("SELECT  cm.clientjob_title ,cm.clientjob_id from tbl_recruiter_cv as rv inner join tbl_clientjd_master 
as cm on cm.clientjob_id=rv.position_id where 
rv.recruiter_id=".$value->id." and   ( Date(rv.created_at) BETWEEN '".$from."' AND '".$to."'  )   group by rv.position_id");

$data[$key]["position"]=$users3;

$users4= DB::select("SELECT  tblclient.comp_name from tbl_recruiter_cv as rv inner join tbl_clientjd_master 
as cm on cm.clientjob_id=rv.position_id  join tbl_clients as tblclient on tblclient.client_id=cm.clientjob_compid  where 
rv.recruiter_id=".$value->id." and   ( Date(rv.created_at) BETWEEN '".$from."' AND '".$to."'  )   group by rv.position_id");

$data[$key]["client"]=$users4;
 
$users5= DB::select("SELECT count(rv.id) as shortlistclients, cm.clientjob_title as position  from tbl_recruiter_cv as rv inner join tbl_clientjd_master 
as cm on cm.clientjob_id=rv.position_id where 
rv.recruiter_id=".$value->id." and      ( Date(rv.created_at) BETWEEN '".$from."' AND '".$to."'  )  and rv.cv_status=22  group by rv.position_id");

$data[$key]["shortlistclients"]=$users5;
   
}
//dd($data);

       return view('admin.Sevenoclockclientreports',compact('data','usersval','from','to'));


        //  return view('admin.adminselectrole');
    }
    //View Role
    
    
    
    
    
    public function joiningpipelinereports()
    {
        
 //  echo "Under Process"; exit;
$loginid = Auth::user()->id;
          $usersvaldata= DB::select("select * from users where id=".$loginid);    
        
        //dd( $usersvaldata[0]->emp_role);
        
       
 $str = $usersvaldata[0]->emp_role;
$a  = [];
for($i=0; $i<strlen($str); $i++){
array_push($a, $str[$i]);
}
//print_r($a);
$maxempcode= max($a);
 
        
        if($maxempcode!='4') 
        {
      if($maxempcode!='7') 
        {        
             $usersval= DB::select("select * from users where  id=".$loginid);     
if(isset($_GET['user']) && $_GET['user']!='' ) {
 $users= DB::select("select * from users where  name='".$_GET['user']."' and  id=".$loginid);
}
else
{ 
   // echo ("select * from users where   id=".$loginid); exit;
   $users= DB::select("select * from users where   id=".$loginid);
}
        }
        else
        {
       $usersval= DB::select("select * from users where emp_role like '%2%'");     
if(isset($_GET['user']) && $_GET['user']!='' ) {
 $users= DB::select("select * from users where  emp_role like '%2%' and name='".$_GET['user']."'");
}
else
{
   $users= DB::select("select * from users where  emp_role like '%2%'");
}
       
            
              
}

        }
        else
        {
          $usersval= DB::select("select * from users where emp_role like '%2%'");     
if(isset($_GET['user']) && $_GET['user']!='' ) {
 $users= DB::select("select * from users where  emp_role like '%2%' and name='".$_GET['user']."'");
}
else
{
   $users= DB::select("select * from users where  emp_role like '%2%'");
}


        }    
   
$data=[];

 


  foreach($users as  $key => $value) {



$data[$key]["username"]=$value->name;
$data[$key]["empcode"]=$value->id;
$data[$key]["state"]=$value->emp_status;
//$queryval=" where ( p.clientjob_empid = ".$value->id." or par.fk_jdid =".$value->id.")";
$queryval=" where ( us.id =".$value->id.")";

if(isset($_GET['clients']) && $_GET['clients']!='' ) {
$queryval .=" and cli.comp_name='".$_GET['clients']."' ";

}
if(isset($_GET['doj']) && $_GET['doj']!='' ) {
    $from=$_GET['doj'];
         $to=$_GET['doend'];
$queryval .="and   ( Date(c.created_at) BETWEEN '".$from."' AND '".$to."'  )  ";

}
  else
     {
           if($usersvaldata[0]->emp_role!='4') 
        {
      //    $from = date('Y-m-01',strtotime(date('Y-m-d')));
      $from = date('Y-m-d',strtotime(date('Y-m-d')));
  $to =  date('Y-m-d',strtotime(date('Y-m-d')));
        }
        else
        {
              // $from = date('Y-m-01',strtotime(date('Y-m-d')));
              $from = date('Y-m-d',strtotime(date('Y-m-d')));
  $to =  date('Y-m-d',strtotime(date('Y-m-d')));
            
        }
$queryval .="and   ( Date(c.created_at) BETWEEN '".$from."' AND '".$to."'  )  ";

  }
  $users4= DB::select("
select DISTINCT(cli.comp_name), c.position_id, p.clientjob_title, p.clientjob_noofposition,cli.client_id, us.name,
(select (sptarg.spoc_offeredtarget) from tbl_spoctarget as sptarg where sptarg.spoc_spid=".$value->id." and  sptarg.spoc_clientid=cli.client_id  and   ( Date(sptarg.created_at) BETWEEN '".$from."' AND '".$to."'  ) order by cli.client_id desc limit 0,1) as Offeredtarget,
  (select (sptarg.spoc_joinedtarget) from tbl_spoctarget as sptarg where sptarg.spoc_spid=".$value->id." and  sptarg.spoc_clientid=cli.client_id  and   ( Date(sptarg.created_at) BETWEEN '".$from."' AND '".$to."'  ) order by cli.client_id desc limit 0,1) as joinedtarget
,
(select  COUNT(r.rcvid) from  tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid =rcv.id where  r.cv_status=15 and rcv.spockid=".$value->id." and r.cvupdatestatus='1' and rcv.clientsid=cli.client_id and   ( Date(r.created_at) BETWEEN '".$from."' AND '".$to."'  ) ) as holdstatus
,(select  COUNT(r.rcvid) from  tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid =rcv.id where  r.cv_status=4 and rcv.spockid=".$value->id." and r.cvupdatestatus='1' and rcv.clientsid=cli.client_id and   ( Date(r.created_at) BETWEEN '".$from."' AND '".$to."'  ) ) as Offered
,(select  COUNT(r.rcvid) from  tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid =rcv.id where  r.cv_status=16 and rcv.spockid=".$value->id." and r.cvupdatestatus='1' and rcv.clientsid=cli.client_id and   ( Date(r.created_at) BETWEEN '".$from."' AND '".$to."'  ) ) as dropstatus


,(select  COUNT(r.rcvid) from  tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid =rcv.id where  r.cv_status=6 and rcv.spockid=".$value->id." and r.cvupdatestatus='1' and rcv.clientsid=cli.client_id and   ( Date(r.created_at) BETWEEN '".$from."' AND '".$to."'  ) ) as joined
,
(select  COUNT(r.rcvid) from  tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid =rcv.id where  r.cv_status=5 and rcv.spockid=".$value->id." and r.cvupdatestatus='1' and rcv.clientsid=cli.client_id  and   ( Date(r.created_at) BETWEEN '".$from."' AND '".$to."'  ) ) as piplined
from tbl_recruiter_cv as c
  join tbl_clientjdrecruitermap as par on  par.clientreq_id=c.r_map_id  join tbl_clientjd_master as p on   par.fk_jdid=p.clientjob_id  join users as us on us.id = p.clientjob_empid  join tbl_clients as cli on cli.client_id = p.clientjob_compid  and us.id=".$value->id." and   ( Date(c.created_at) BETWEEN '".$from."' AND '".$to."') group by cli.client_id

");
 
$data[$key]["client"]=$users4;
 }  
//dd($data);
       return view('admin.joiningpipelinereports',compact('data','from','to'));


        //  return view('admin.adminselectrole');
   

    }
    
    
    
       public function recuiterjoiningpipelinereports()
    {
        $loginid = Auth::user()->id;
          $usersvaldata= DB::select("select * from users where id=".$loginid);   
                
 $str = $usersvaldata[0]->emp_role;
$a  = [];
for($i=0; $i<strlen($str); $i++){
array_push($a, $str[$i]);
}
//print_r($a);
 $maxempcode= max($a);
 
 
       
       $sql = "SELECT id,team_lead_id,(select GROUP_CONCAT(u.name) from users as u where find_in_set(u.id,team_lead_id))as name FROM tbl_team";

       $teamlead = DB::select(DB::raw($sql));

        // print_r($teamlead);die;
if($maxempcode=='3')
{
    $usersval= DB::select("select * from users where emp_role like '%3%' and id=".$loginid."");   
    
}
else
{
     $usersval= DB::select("select * from users where emp_role like '%3%'");     
}
     //dd($_GET['user']);
    //echo date('Y-m-01',strtotime(date('Y-m-d')));

        
        if(isset($_GET['user'])){
          $name=$_GET['user'];
        }else{
          $name=-1;
        }
        if(isset($_GET['doj']) && $_GET['doj']!=''){
          $from=$_GET['doj'];
        }else{
          $from=date('Y-m-01',strtotime(date('Y-m-d')));
        }
        if(isset($_GET['doend']) && $_GET['doend']!=''){
          $to=$_GET['doend'];
        }else{
          $to=date('Y-m-t',strtotime(date('Y-m-d')));;
        }
        if(isset($_GET['teamleader']) && $_GET['teamleader']!=''){
          $team=$_GET['teamleader'];
        }else{
          $team=-1;
        }
        
        if($maxempcode=='3')
{
  
  
     $query = "SELECT us.id,us.`name`,
     (select GROUP_CONCAT(DISTINCT(rcv.id))  from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where r.cv_status=4 and (r.cvupdatestatus=1 || r.cvupdatestatus=0 ) and rcv.recruiter_id=us.`id`  and date(r.created_at) BETWEEN '".$from."' and  '".$to."' group by r.cv_status) as Offered,

(select count(r.rcvid) from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where r.cv_status=5 and r.cvupdatestatus=1 and rcv.recruiter_id=us.`id`) as piplined,
(select count(r.rcvid) from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where r.cv_status=15 and r.cvupdatestatus=1 and rcv.recruiter_id=us.`id` and date(r.created_at) BETWEEN '".$from."' and  '".$to."') as holdstatus,

(select count(r.rcvid) from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where r.cv_status=6 and r.cvupdatestatus=1 and rcv.recruiter_id=us.`id` and date(r.created_at) BETWEEN '".$from."' and  '".$to."') as joined,

(select t.rec_offeredtarget from tbl_rectarget as t where t.rec_rid=us.id and date(t.created_at) BETWEEN '".$from."' and  '".$to."'  order by rec_tid desc limit 0,1 ) as Offeredtarget,

(select count(r.rcvid) from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where r.cv_status=4 and r.cvupdatestatus=1 and rcv.recruiter_id=us.`id` and date(r.dateoj) BETWEEN '".$from."' and  '".$to."' ) as yetOffered,


(select count(r.rcvid) from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where r.cv_status=16 and r.cvupdatestatus=1 and rcv.recruiter_id=us.`id` and date(r.created_at) BETWEEN '".$from."' and  '".$to."') as dropstatus,
(select count(r.rcvid) from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where r.cv_status in (4) and r.cvupdatestatus=1 and rcv.recruiter_id=us.`id` and ( (date(r.dateoj) BETWEEN '".$from."' and  '".$to."') || (date(r.expDoj) BETWEEN '".$from."' and  '".$to."'))  ) as yetjoined,

(select t.rec_joinedtarget from tbl_rectarget as t where t.rec_rid=us.id and date(t.created_at) BETWEEN '".$from."' and  '".$to."'  order by rec_tid desc limit 0,1) as joinedtarget
FROM `users` as us where (us.name like concat('%','".$name."','%') or '".$name."'=-1) and (find_in_set(us.id,(select t.team_members_id from tbl_team as t where t.id='".$team."')) or '".$team."'=-1) and us.id=".$loginid." and us.emp_role!='4'";
    
    
}
else
{
   /* and date(r.expDoj) BETWEEN '".$from."' and  '".$to."'*/
     $query = "SELECT us.id,us.`name`,
(select GROUP_CONCAT(DISTINCT(rcv.id))   from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where r.cv_status=4 and (r.cvupdatestatus=1 || r.cvupdatestatus=0 ) and rcv.recruiter_id=us.`id` and date(r.created_at) BETWEEN '".$from."' and  '".$to."' group by r.cv_status) as Offered,

(select count(r.rcvid) from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where r.cv_status=5 and r.cvupdatestatus=1 and rcv.recruiter_id=us.`id` ) as piplined,
(select count(r.rcvid) from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where r.cv_status=15 and r.cvupdatestatus=1 and rcv.recruiter_id=us.`id` and date(r.created_at) BETWEEN '".$from."' and  '".$to."') as holdstatus,

(select count(r.rcvid) from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where r.cv_status=6 and r.cvupdatestatus=1 and rcv.recruiter_id=us.`id` and date(r.created_at) BETWEEN '".$from."' and  '".$to."') as joined,

(select t.rec_offeredtarget from tbl_rectarget as t where t.rec_rid=us.id and date(t.created_at) BETWEEN '".$from."' and  '".$to."'  order by rec_tid desc limit 0,1 ) as Offeredtarget,

(select count(r.rcvid) from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where r.cv_status=4 and r.cvupdatestatus=1 and rcv.recruiter_id=us.`id` and date(r.dateoj) BETWEEN '".$from."' and  '".$to."' ) as yetOffered,


(select count(r.rcvid) from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where r.cv_status=16 and r.cvupdatestatus=1 and rcv.recruiter_id=us.`id` and date(r.created_at) BETWEEN '".$from."' and  '".$to."') as dropstatus,
(select count(r.rcvid) from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id where r.cv_status in (4) and r.cvupdatestatus=1 and rcv.recruiter_id=us.`id` and ( (date(r.dateoj) BETWEEN '".$from."' and  '".$to."') || (date(r.expDoj) BETWEEN '".$from."' and  '".$to."'))  ) as yetjoined,

(select t.rec_joinedtarget from tbl_rectarget as t where t.rec_rid=us.id and date(t.created_at) BETWEEN '".$from."' and  '".$to."'  order by rec_tid desc limit 0,1) as joinedtarget
FROM `users` as us where (us.name like concat('%','".$name."','%') or '".$name."'=-1) and (find_in_set(us.id,(select t.team_members_id from tbl_team as t where t.id='".$team."')) or '".$team."'=-1) and us.emp_role!='4'";
      
}



    
      $users = DB::select(DB::raw($query));




     

       return view('admin.recuiterjoiningpipelinereports',compact('users','from','to','teamlead','maxempcode'));
        //  return view('admin.adminselectrole');
    }
    
    /*
       public function recuiterjoiningpipelinereports()
    {
        
        
    $usersval= DB::select("select * from users where emp_role like '%3%'");      
        
        if(isset($_GET['user']))
     {

    if(isset($_GET['doj']) && $_GET['doj']!='')
     {
         $from=$_GET['doj'];
         $to=$_GET['doend'];
     }
    
     else
     {
         $from = date('Y-m-01',strtotime(date('Y-m-d')));
  $to =  date('Y-m-t',strtotime(date('Y-m-d')));
     }
     
      if($_GET['user']!='') {
    

     $users= DB::select("select * ,(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=4 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as Offered ,
 (select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=5 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as piplined,
(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=6 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as joined 
      ,(select rec_offeredtarget from tbl_rectarget as rtar where rtar.rec_rid =us.id  and   ( Date(rtar.created_at) BETWEEN '".$from."' AND '".$to."')  ) as Offeredtarget 

 ,(select rec_joinedtarget from tbl_rectarget as rtar where rtar.rec_rid =us.id and   ( Date(rtar.created_at) BETWEEN '".$from."' AND '".$to."' ) ) as joinedtarget 

 from users as us where us.emp_role like '%3%' and   us.name='".$_GET['user']."'");



     }
     else
     { 
        $users= DB::select("select *,
(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=4 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as Offered ,
 (select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=5 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as piplined,
(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=6 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as joined
        ,(select rec_offeredtarget from tbl_rectarget as rtar where rtar.rec_rid =us.id  and   ( Date(rtar.created_at) BETWEEN '".$from."' AND '".$to."')  ) as Offeredtarget 

 ,(select rec_joinedtarget from tbl_rectarget as rtar where rtar.rec_rid =us.id and   ( Date(rtar.created_at) BETWEEN '".$from."' AND '".$to."' ) ) as joinedtarget 


          from users as us where us.emp_role like '%3%'");
     }
     
     
     }
     
 else
 {
      if(isset($_GET['doj']) && $_GET['doj']!='')
     {
         $from=$_GET['doj'];
         $to=$_GET['doend'];
     }
    
     else
     {
         $from = date('Y-m-01',strtotime(date('Y-m-d')));
  $to =  date('Y-m-t',strtotime(date('Y-m-d')));
     }

  $users= DB::select("select *

 ,(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=4 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as Offered ,
 (select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=5 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as piplined,
(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=6 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as joined ,
 (select rec_offeredtarget from tbl_rectarget as rtar where rtar.rec_rid =us.id  and   ( Date(rtar.created_at) BETWEEN '".$from."' AND '".$to."')  ) as Offeredtarget 
 ,(select rec_joinedtarget from tbl_rectarget as rtar where rtar.rec_rid =us.id and   ( Date(rtar.created_at) BETWEEN '".$from."' AND '".$to."' ) ) as joinedtarget from users as us where us.emp_role like '%3%'");
 }
  
     

       return view('admin.recuiterjoiningpipelinereports',compact('users','from','to'));
        //  return view('admin.adminselectrole');
    }
    */
    
public function lunchbreakreports()
    {

  if(isset($_GET['doj']))
     {
         $from=$_GET['doj'];
         $to=$_GET['doend'];
     }
    
     else
     {
        $from=date('Y-m-d');
        $to=date('Y-m-d');
     }


if(isset($_GET['user'])) {
    
if($_GET['user']!='') {
 $data = DB::table('tbl_clientrec_break_backtime as tcbr')
                                ->join('users as u','tcbr.clientstart_recuiterid','=','u.id')
                                ->select('tcbr.*','u.name')
                                  ->where('tcbr.created_at','>=',$from)
                                  // ->where('tcbr.created_at','<=',$to)
                                     ->where('u.name','=',$_GET['user'])
                            
                                ->get();
                            }
                            else
                            {
                                $data = DB::table('tbl_clientrec_break_backtime as tcbr')
                                ->join('users as u','tcbr.clientstart_recuiterid','=','u.id')
                                ->select('tcbr.*','u.name')
                                  ->where('tcbr.created_at','>=',$from)
                                 //  ->where('tcbr.created_at','<=',$to)
                           
                                ->get();
                            }
     }
     else
     {
 
 $data = DB::table('tbl_clientrec_break_backtime as tcbr')
                                ->join('users as u','tcbr.clientstart_recuiterid','=','u.id')
                                ->select('tcbr.*','u.name')
                                  ->where('tcbr.created_at','>=',$from)
                                 //  ->where('tcbr.created_at','=',$to)
                                   ->get();

      }
     
 return view('admin.lunchbreakreports',compact('data'));

}
    
public function recuiterpositionreports()
    {
      $id = Auth::user()->id;
 /* $getPositions = DB::table('tbl_clientjdrecruitermap as tcr')
      ->join('tbl_clientjd_master as tcm', 'tcr.fk_jdid', '=', 'tcm.clientjob_id')
      ->select(DB::raw('distinct(tcm.clientjob_title) as clti') ,'tcr.fk_empid'
        ,DB::raw('(select count(id) from tbl_recruiter_cv as rcv where rcv.cv_status=5 and position_id=tcm.clientjob_id  and rcv.recruiter_id='.$id.') as Pipeline') 
,DB::raw('(select count(id) from tbl_recruiter_cv as rcv where rcv.cv_status=5 and position_id=tcm.clientjob_id and rcv.recruiter_id='.$id.') as Joining') 
,DB::raw('(select count(id) from tbl_recruiter_cv as rcv where rcv.cv_status=4 and position_id=tcm.clientjob_id and rcv.recruiter_id='.$id.') as Offer') 
,DB::raw('(select count(id) from tbl_recruiter_cv as rcv where rcv.cv_status=1 and position_id=tcm.clientjob_id and rcv.recruiter_id='.$id.') as notseen') 
,DB::raw('(select count(id) from tbl_recruiter_cv as rcv where rcv.cv_status=2 and position_id=tcm.clientjob_id and rcv.recruiter_id='.$id.') as sendtoclient') 
,DB::raw('(select count(id) from tbl_recruiter_cv as rcv where rcv.cv_status=3 and position_id=tcm.clientjob_id and rcv.recruiter_id='.$id.') as rejected') 

)
      ->where('tcr.fk_empid', '=', $id)
                  ->groupBy('tcr.fk_empid', 'tcm.clientjob_title', 'tcm.clientjob_id')
                  ->tosql();*/
$quertd=" where tcr.fk_empid = ".$id."";

if(isset($_GET['user'] ) && $_GET['user']!='')
{
$quertd .=" and tcm.clientjob_title = '".$_GET['user']."' ";  
}
 if(isset($_GET['doj']) && $_GET['doj']!='' )
     {
         $from=$_GET['doj'];
         $to=$_GET['doend'];
     }
    
     else
     {
         $from = date('Y-m-01',strtotime(date('Y-m-d')));
  $to =  date('Y-m-t',strtotime(date('Y-m-d')));
     }


$sql = "select distinct(tcm.clientjob_title) as clti,tcm.clientjob_id as cltidd, tcr.fk_empid,
 
 (select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=5 and rcv.position_id=tcm.clientjob_id  and rcv.recruiter_id=".$id." and r.cvupdatestatus=1 and  ( Date(r.expDoj ) BETWEEN '".$from."' AND '".$to."') ) as Pipeline,
(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=6 and rcv.position_id=tcm.clientjob_id  and rcv.recruiter_id=".$id." and r.cvupdatestatus=1 and  ( Date(r.dateoj ) BETWEEN '".$from."' AND '".$to."')) as Joining,

(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=4 and rcv.position_id=tcm.clientjob_id  and rcv.recruiter_id=".$id." and r.cvupdatestatus=1 and  ( Date(r.dateoj ) BETWEEN '".$from."' AND '".$to."')) as Offer,

(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=1 and rcv.position_id=tcm.clientjob_id  and rcv.recruiter_id=".$id." and r.cvupdatestatus=1 and  ( Date(rcv.created_at ) BETWEEN '".$from."' AND '".$to."')) as notseen,


(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=2 and rcv.position_id=tcm.clientjob_id  and rcv.recruiter_id=".$id." and r.cvupdatestatus=1 and  ( Date(rcv.created_at ) BETWEEN '".$from."' AND '".$to."')) as sendtoclient,


(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=3 and rcv.position_id=tcm.clientjob_id  and rcv.recruiter_id=".$id." and r.cvupdatestatus=1 and  ( Date(rcv.created_at ) BETWEEN '".$from."' AND '".$to."')) as rejected

    from tbl_clientjdrecruitermap as tcr inner join tbl_clientjd_master as tcm on tcr.fk_jdid = tcm.clientjob_id ".$quertd." group by tcr.fk_empid, tcm.clientjob_title, tcm.clientjob_id";
      $getPositions = DB::select(DB::raw($sql));

//dd($getPositions);
  return view('Emp.recuiterpositionreports',compact('getPositions','from','to'));
}

  public function getdriveposreports($posid,$fromid,$fromdate,$todate)
        {
//echo $posid.'--'.$fromid.'--'.$fromdate.'--'.$todate; exit;
  $crdate="and  ( Date(r.created_at ) BETWEEN '".$fromdate."' AND '".$todate."'";


$sql = "select u.*,r.created_at as crdate from  tbl_recruiter_cv as u join tbl_recruiter_cvchangestatus as r on r.cvid =u.id where  r.cvupdatestatus=1 and
 u.position_id=".$posid." and r.cv_status=".$fromid." and cvupdatestatus='1' ". $crdate.") ";
    
    
      $recruiters = DB::select(DB::raw($sql));



        //  dd($recruiters);
            
             $html="";
           //  $html = $uid.'---'.$posid.'----'.$fromid.'----'.$toid;    
           
           //<a href="http://192.168.1.196/elitehr/storage/app/uploadcv/{{$list->cv_uploaded_date}}/{{$list->cv_name}}" margin-left: 15px;"> 
             foreach($recruiters as $key => $val){
                  if($val->cv_uploaded_date!='') {
 $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td><a target="_blank" href="http://115.124.98.243/~myhirepro/storage/app/uploadcv/'.$val->cv_uploaded_date.'/'.$val->cv_name.'" >Download</a></td><td>'.$val->created_at.'</td><td>'.$val->dateoj.''.$val->expDoj.'</td>
            </tr>';
                  }
                  
                  else
                  {
                       $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td>'.$val->cv_name.'</td><td>'.$val->created_at.'</td><td>'.$val->dateoj.''.$val->expDoj.'</td>
            </tr>';
                      
                  }
           
             }
           /* 
            $html = '';          
           
            foreach($recruiters as $key => $val){
               $html .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }*/
            echo $html;
        }
   public function getresumesclientdatapipedreport($uid,$posid,$fromid,$fromdate,$todate)
        {

  $sql = "select  rcv.* , (select us.name from users as us where us.id=rcv.recruiter_id ) as usname  from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id  join tbl_clientjdrecruitermap as rjdmap on rjdmap.clientreq_id=rcv.r_map_id
join tbl_clientjd_master as jdmast on rjdmap.fk_jdid=jdmast.clientjob_id  join tbl_clients as clie on clie.client_id = jdmast.clientjob_compid where r.cv_status=".$fromid." and clie.client_id=".$posid." and rjdmap.fk_assigneeid=".$uid." and   ( Date(r.expDoj) BETWEEN '".$fromdate."' AND '".$todate."'  )";

   $recruiters = DB::select(DB::raw($sql));


        //  dd($recruiters);
            
             $html="";
           //  $html = $uid.'---'.$posid.'----'.$fromid.'----'.$toid;    
           
           //<a href="http://192.168.1.196/elitehr/storage/app/uploadcv/{{$list->cv_uploaded_date}}/{{$list->cv_name}}" margin-left: 15px;"> 
             foreach($recruiters as $key => $val){
                  if($val->cv_uploaded_date!='') {
 $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td><a href="http://115.124.98.243/~elitehr/storage/app/uploadcv/'.$val->cv_uploaded_date.'/'.$val->cv_name.'" target="_blank">Download</a></td><td>'.$val->created_at.'</td><td>'.$val->usname.'</td>
            </tr>';
                  }
                  
                  else
                  {
                       $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td>Download</td><td>'.$val->created_at.'</td><td>'.$val->usname.'</td>
            </tr>';
                      
                  }
           
             }
           /* 
            $html = '';          
           
            foreach($recruiters as $key => $val){
               $html .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }*/
            echo $html;
        }

   public function getresumesclientdatareport($uid,$posid,$fromid,$fromdate,$todate)
        {

  $sql = "select  rcv.* , (select us.name from users as us where us.id=rcv.recruiter_id ) as usname  from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id  join tbl_clientjdrecruitermap as rjdmap on rjdmap.clientreq_id=rcv.r_map_id
join tbl_clientjd_master as jdmast on rjdmap.fk_jdid=jdmast.clientjob_id  join tbl_clients as clie on clie.client_id = jdmast.clientjob_compid where r.cv_status=".$fromid." and clie.client_id=".$posid." and rjdmap.fk_assigneeid=".$uid." and   ( Date(r.created_at) BETWEEN '".$fromdate."' AND '".$todate."' and r.cvupdatestatus='1'  )";

   $recruiters = DB::select(DB::raw($sql));


        //  dd($recruiters);
            
             $html="";
           //  $html = $uid.'---'.$posid.'----'.$fromid.'----'.$toid;    
           
           //<a href="http://192.168.1.196/elitehr/storage/app/uploadcv/{{$list->cv_uploaded_date}}/{{$list->cv_name}}" margin-left: 15px;"> 
             foreach($recruiters as $key => $val){
                  if($val->cv_uploaded_date!='') {
 $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td><a href="http://115.124.98.243/~elitehr/storage/app/uploadcv/'.$val->cv_uploaded_date.'/'.$val->cv_name.'" target="_blank">Download</a></td><td>'.$val->created_at.'</td><td>'.$val->usname.'</td>
            </tr>';
                  }
                  
                  else
                  {
                       $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td>Download</td><td>'.$val->created_at.'</td><td>'.$val->usname.'</td>
            </tr>';
                      
                  }
           
             }
           /* 
            $html = '';          
           
            foreach($recruiters as $key => $val){
               $html .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }*/
            echo $html;
        }
   
    public function getdrivereports($uid,$posid,$fromid,$fromdate,$todate)
        {
         //   echo $uid.'--'.$posid.'--'.$fromid.'---'.$fromdate.'--'.$todate;
             $crdate="and  ( Date(r.tca_createdon ) BETWEEN '".$fromdate."' AND '".$todate."'";
   if($fromid=='2')  {   $eeid='Pending';}
   elseif($fromid=='3')  {   $eeid='Confirm';} elseif($fromid=='4')  {   $eeid='Not Going';}
    elseif($fromid=='5')  {   $eeid='Not Reachable';} elseif($fromid=='6')  {   $eeid='Not Responding';}
    elseif($fromid=='7')  {   $eeid='Dicey';}
$sql = "select u.*,r.tca_createdon as crdate from  tbl_recruiter_cv as u join
tbl_cv_allocated as r on r.tca_cvid =u.id where u.position_id=".$posid." and r.tca_followupstatus='".$eeid."' 
 ". $crdate.") ";
        $recruiters = DB::select(DB::raw($sql));   
        $html="";
           //  $html = $uid.'---'.$posid.'----'.$fromid.'----'.$toid;    
           
           //<a href="http://192.168.1.196/elitehr/storage/app/uploadcv/{{$list->cv_uploaded_date}}/{{$list->cv_name}}" margin-left: 15px;"> 
             foreach($recruiters as $key => $val){
                  if($val->cv_uploaded_date!='') {
 $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td><a target="_blank" href="http://115.124.98.243/~myhirepro/storage/app/uploadcv/'.$val->cv_uploaded_date.'/'.$val->cv_name.'" >Download</a></td><td>'.$val->created_at.'</td><td>'.$val->dateoj.''.$val->expDoj.'</td>
            </tr>';
                  }
                  
                  else
                  {
                       $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td>Download</td><td>'.$val->created_at.'</td><td>'.$val->dateoj.''.$val->expDoj.'</td>
            </tr>';
                      
                  }
           
             }
           /* 
            $html = '';          
           
            foreach($recruiters as $key => $val){
               $html .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }*/
            echo $html;  
        }
        
          public function getresumesreportdata($uid,$posid,$fromid,$fromdate,$todate)
        {

if($fromid=='5')
{
     $crdate="and  ( Date(r.expDoj ) BETWEEN '".$fromdate."' AND '".$todate."'";
}
elseif($fromid=='4')
{
     $crdate="and  ( Date(r.dateoj ) BETWEEN '".$fromdate."' AND '".$todate."'";
}
elseif($fromid=='6')
{
    $crdate="and  ( Date(r.dateoj ) BETWEEN '".$fromdate."' AND '".$todate."'";
}
else
{
    $crdate="and  ( Date(u.created_at ) BETWEEN '".$fromdate."' AND '".$todate."'";
}


$sql = "select u.*,r.created_at as crdate from  tbl_recruiter_cv as u join tbl_recruiter_cvchangestatus as r on r.cvid =u.id where  r.cvupdatestatus=1 and  u.recruiter_id=".$uid."  and u.position_id=".$posid." and r.cv_status=".$fromid." and cvupdatestatus='1' ". $crdate.") ";
    
    
      $recruiters = DB::select(DB::raw($sql));



        //  dd($recruiters);
            
             $html="";
           //  $html = $uid.'---'.$posid.'----'.$fromid.'----'.$toid;    
           
           //<a href="http://192.168.1.196/elitehr/storage/app/uploadcv/{{$list->cv_uploaded_date}}/{{$list->cv_name}}" margin-left: 15px;"> 
             foreach($recruiters as $key => $val){
                  if($val->cv_uploaded_date!='') {
 $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td><a target="_blank" href="http://115.124.98.243/~myhirepro/storage/app/uploadcv/'.$val->cv_uploaded_date.'/'.$val->cv_name.'" >'.$val->cv_name.'</a></td><td>'.$val->created_at.'</td><td>'.$val->dateoj.''.$val->expDoj.'</td>
            </tr>';
                  }
                  
                  else
                  {
                       $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td>'.$val->cv_name.'</td><td>'.$val->created_at.'</td><td>'.$val->dateoj.''.$val->expDoj.'</td>
            </tr>';
                      
                  }
           
             }
           /* 
            $html = '';          
           
            foreach($recruiters as $key => $val){
               $html .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }*/
            echo $html;
        }
        

          public function getresumesadmindatareport($uid,$fromid,$fromdate,$todate)
        {
// $uid.'--'.$posid.'--'.$fromid.'---'.$fromdate.'--'.$todate; 
          /*   $recruiters = DB::table('tbl_recruiter_cv as u')
                    ->select('u.*')
                    ->where('u.recruiter_id','=',$uid)
                    ->where('u.position_id','=',$posid)
                     ->where('u.cv_status','=',$fromid)
                  // ->whereBetween('Date(u.created_at)', [$fromdate, $todate])
                 ->whereRaw('Date(u.created_at) between "'.$fromdate.'" and "'.$todate.'"')
                    
                    ->get();  
            */
if($fromid!='4') {
$sql = "select u.*, r.dateoj as dateofjoin from  tbl_recruiter_cv as u join tbl_recruiter_cvchangestatus as r on r.cvid =u.id where  r.cvupdatestatus=1 and  u.recruiter_id=".$uid."   and r.cv_status=".$fromid." and cvupdatestatus='1' and  ( Date(r.created_at ) BETWEEN '".$fromdate."' AND '".$todate."') ";
   

}
else
{
  $sql = "select u.*, r.dateoj as dateofjoin from  tbl_recruiter_cv as u join tbl_recruiter_cvchangestatus as r on r.cvid =u.id where  u.recruiter_id=".$uid."   and r.cv_status=".$fromid." and (r.cvupdatestatus='1' || r.cvupdatestatus='0' )  and  ( Date(r.created_at ) BETWEEN '".$fromdate."' AND '".$todate."' ) group by u.id";
  
    
}
   $recruiters = DB::select(DB::raw($sql));
        //  dd($recruiters);
            
             $html="";
           //  $html = $uid.'---'.$posid.'----'.$fromid.'----'.$toid;    
           
           //<a href="http://192.168.1.196/elitehr/storage/app/uploadcv/{{$list->cv_uploaded_date}}/{{$list->cv_name}}" margin-left: 15px;"> 
       $html .= '<table width="100%" border="1"><tr><td>Candidate Name</td><td>Candidate Mobile</td><td>Candidate Email</td><td>Candidate Cv</td><td>Created Date</td><td>Candidate DOJ/EDOJ</td></tr>';    
             foreach($recruiters as $key => $val){
                  if($val->cv_uploaded_date!='') {
 $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td><a target="_blank" href="http://115.124.98.243/~elitehr/storage/app/uploadcv/'.$val->cv_uploaded_date.'/'.$val->cv_name.'">Download</a></td><td>'.$val->created_at.'</td><td>'.$val->dateofjoin.''.$val->expDoj.'</td>
            </tr>';
                  }
                  
                  else
                  {
                       $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td>Download</td><td>'.$val->created_at.'</td><td>'.$val->dateofjoin.''.$val->expDoj.'</td>
            </tr>';
                      
                  }
           
             }
              $html .= '</table>';
           /* 
            $html = '';          
           
            foreach($recruiters as $key => $val){
               $html .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }*/
            echo $html;
        }
        
        
        
          public function getresumesadmindatapipereport($uid,$fromid,$fromdate,$todate)
        {
// $uid.'--'.$posid.'--'.$fromid.'---'.$fromdate.'--'.$todate; 
          /*   $recruiters = DB::table('tbl_recruiter_cv as u')
                    ->select('u.*')
                    ->where('u.recruiter_id','=',$uid)
                    ->where('u.position_id','=',$posid)
                     ->where('u.cv_status','=',$fromid)
                  // ->whereBetween('Date(u.created_at)', [$fromdate, $todate])
                 ->whereRaw('Date(u.created_at) between "'.$fromdate.'" and "'.$todate.'"')
                    
                    ->get();  
                    and  ( Date(r.expDoj ) BETWEEN '".$fromdate."' AND '".$todate."'
            */

$sql = "select u.candidate_name,  u.candidate_mob,  u.candidate_email,  u.cv_uploaded_date,  u.cv_name, r.dateoj as dateofjoin ,u.created_at,r.expDoj from  tbl_recruiter_cv as u join tbl_recruiter_cvchangestatus as r on r.cvid =u.id where  r.cvupdatestatus=1 and  u.recruiter_id=".$uid."   and r.cv_status=".$fromid." and cvupdatestatus='1'  ";
      $recruiters = DB::select(DB::raw($sql));



        //  dd($recruiters);
            
             $html="";
           //  $html = $uid.'---'.$posid.'----'.$fromid.'----'.$toid;    
           
           //<a href="http://192.168.1.196/elitehr/storage/app/uploadcv/{{$list->cv_uploaded_date}}/{{$list->cv_name}}" margin-left: 15px;"> 
       $html .= '<table width="100%" border="1"><tr><td>Candidate Name</td><td>Candidate Mobile</td><td>Candidate Email</td><td>Candidate Cv</td><td>Created Date</td><td>Candidate DOJ/EDOJ</td></tr>';    
             foreach($recruiters as $key => $val){
                  if($val->cv_uploaded_date!='') {
 $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td><a target="_blank" href="http://115.124.98.243/~elitehr/storage/app/uploadcv/'.$val->cv_uploaded_date.'/'.$val->cv_name.'">Download</a></td><td>'.$val->created_at.'</td><td>'.$val->dateofjoin.''.$val->expDoj.'</td>
            </tr>';
                  }
                  
                  else
                  {
                       $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td>Download</td><td>'.$val->created_at.'</td><td>'.$val->dateofjoin.''.$val->expDoj.'</td>
            </tr>';
                      
                  }
           
             }
              $html .= '</table>';
           /* 
            $html = '';          
           
            foreach($recruiters as $key => $val){
               $html .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }*/
            echo $html;
        }
        
        
        
        
        
          public function getresumesadmindatareportyet($uid,$fromid,$fromdate,$todate)
        {
// $uid.'--'.$posid.'--'.$fromid.'---'.$fromdate.'--'.$todate; 
          /*   $recruiters = DB::table('tbl_recruiter_cv as u')
                    ->select('u.*')
                    ->where('u.recruiter_id','=',$uid)
                    ->where('u.position_id','=',$posid)
                     ->where('u.cv_status','=',$fromid)
                  // ->whereBetween('Date(u.created_at)', [$fromdate, $todate])
                 ->whereRaw('Date(u.created_at) between "'.$fromdate.'" and "'.$todate.'"')
                    
                    ->get();  
            */

 $sql = "select u.*, r.dateoj as dateofjoin from  tbl_recruiter_cv as u join tbl_recruiter_cvchangestatus as r on r.cvid =u.id where  r.cvupdatestatus=1 and  u.recruiter_id=".$uid."   and r.cv_status in (4) and cvupdatestatus='1' and ( ( Date(r.dateoj ) BETWEEN '".$fromdate."' AND '".$todate."') ||   ( Date(r.expDoj ) BETWEEN '".$fromdate."' AND '".$todate."') )   ";
    
      $recruiters = DB::select(DB::raw($sql));



        //  dd($recruiters);
            
             $html="";
           //  $html = $uid.'---'.$posid.'----'.$fromid.'----'.$toid;    
           
           //<a href="http://192.168.1.196/elitehr/storage/app/uploadcv/{{$list->cv_uploaded_date}}/{{$list->cv_name}}" margin-left: 15px;"> 
       $html .= '<table width="100%" border="1"><tr><td>Candidate Name</td><td>Candidate Mobile</td><td>Candidate Email</td><td>Candidate Cv</td><td>Created Date</td><td>Candidate DOJ/EDOJ</td></tr>';    
             foreach($recruiters as $key => $val){
                  if($val->cv_uploaded_date!='') {
 $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td><a href="http://115.124.98.243/~elitehr/storage/app/uploadcv/'.$val->cv_uploaded_date.'/'.$val->cv_name.'" target="_blank">Download</a></td><td>'.$val->created_at.'</td><td>'.$val->dateofjoin.''.$val->expDoj.'</td>
            </tr>';
                  }
                  
                  else
                  {
                       $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td>Download</td><td>'.$val->created_at.'</td><td>'.$val->dateofjoin.''.$val->expDoj.'</td>
            </tr>';
                      
                  }
           
             }
              $html .= '</table>';
           /* 
            $html = '';          
           
            foreach($recruiters as $key => $val){
               $html .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }*/
            echo $html;
        }
        
        
        
   
  public function recuiterofferreports()
    {
 
 $assigneeid=Auth::user()->id;
  $usersval= DB::select("select * from users where  id=".$assigneeid.""); 
  
  
  
 $str = $usersval[0]->emp_role;
$a  = [];
for($i=0; $i<strlen($str); $i++){
array_push($a, $str[$i]);
}
//print_r($a);
$maxempcode= max($a);


if($maxempcode!='4')
{
     
if($maxempcode!='7'  )
{
    
if(isset($_GET['user']) && $_GET['user']!='' )
{
$users= DB::select("select * from users where  emp_role like '%3%' and name='".$_GET['user']."'  and id=".$assigneeid."");
}
else
{
  $users= DB::select("select * from users where  emp_role like '%3%'  and id=".$assigneeid."");
}
}
else
{
    
    
if(isset($_GET['user']) && $_GET['user']!='' )
{
$users= DB::select("select * from users where  emp_role like '%3%' and name='".$_GET['user']."'");
}
else
{
  $users= DB::select("select * from users where  emp_role like '%3%'");
}


}

}
else
{
    
   // echo  "dddddddd"; exit;
if(isset($_GET['user']) && $_GET['user']!='' )
{
$users= DB::select("select * from users where  emp_role like '%3%' and name='".$_GET['user']."'");
}
else
{
  $users= DB::select("select * from users where  emp_role like '%3%'");
}
}
   
$data=[];
if(isset($_GET['doj']) and $_GET['doj']!='')
{
$start = $_GET['doj'];
$finish = (date('D') != 'Sat') ? date('Y-m-d', strtotime('next Saturday')) : date('Y-m-d');

}
if(isset($_GET['doend']) and $_GET['doend']!='')
{
    $start = $_GET['doj'];
     $finish = $_GET['doend'];
}
else
{
  
  /*  $start = (date('D') != 'Mon') ? date('Y-m-d', strtotime('last Monday')) : date('Y-m-d');
    $finish = date('Y-m-d');*/
           $start = date('Y-m-01',strtotime(date('Y-m-d')));
  $finish =  date('Y-m-t',strtotime(date('Y-m-d')));
}


  foreach($users as  $key => $value) {



$data[$key]["username"]=$value->name;
$data[$key]["empcode"]=$value->id;
$data[$key]["state"]=$value->emp_status;

$users1= DB::select("select a.* , b.expectedsalery as expsal from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as b on a.id=b.cvid where  a.recruiter_id=".$value->id." and b.cvupdatestatus=1  and (b.cv_status='4' or b.cv_status='6') and (b.dateoj between '".$start ."' and '".$finish ."') order by a.id desc ");

   $users2= DB::select("select b.r_map_id,a.clientjob_id, b.position_id, b.dateoj,a.clientjob_title from tbl_clientjdrecruitermap as c join tbl_clientjd_master as a  on a.clientjob_id=c.fk_jdid join tbl_recruiter_cv as b on b.r_map_id=c.clientreq_id join tbl_recruiter_cvchangestatus as m on b.id=m.cvid where b.recruiter_id=".$value->id." and (m.cv_status='4' or m.cv_status='6') and m.cvupdatestatus=1 and (m.dateoj between '".$start ."' and '".$finish ."') order by id desc ");

     $users3= DB::select("select a.comp_name  from  tbl_clients as a join tbl_recruiter_cv as b on a.client_id=b.clientsid
    join tbl_recruiter_cvchangestatus as m on b.id=m.cvid where  b.recruiter_id=".$value->id." and m.cvupdatestatus=1 
    and  (m.cv_status='4' or m.cv_status='6')  and (m.dateoj between '".$start ."' and '".$finish ."') order by id desc ");
    
    $data[$key]["resumes"]=$users1;
 $data[$key]["positions"]=$users2;
 $data[$key]["clients"]=$users3;
/*$users1= DB::select("select * from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as b on a.id=b.cvid

where  a.recruiter_id=".$value->id." and b.cvupdatestatus=1  and b.cv_status='4' and (b.dateoj between '".$start ."' and '".$finish ."') order by a.id desc ");
$data[$key]["resumes"]=$users1;

$users2= DB::select("
select b.r_map_id,a.clientjob_id, b.position_id, b.dateoj,a.clientjob_title from tbl_clientjdrecruitermap as c join tbl_clientjd_master as a  on a.clientjob_id=c.fk_jdid 

join tbl_recruiter_cv as b on b.r_map_id=c.clientreq_id
join tbl_recruiter_cvchangestatus as m on b.id=m.cvid

where b.recruiter_id=".$value->id." and m.cv_status='4'   and m.cvupdatestatus=1 and (m.dateoj between '".$start ."' and '".$finish ."') order by id desc ");
$data[$key]["positions"]=$users2;

$users3= DB::select("select a.comp_name  from  tbl_clients as a join tbl_recruiter_cv as b on a.client_id=b.clientsid
join tbl_recruiter_cvchangestatus as m on b.id=m.cvid

where  b.recruiter_id=".$value->id."

 and m.cvupdatestatus=1 
and  m.cv_status='4'  and (m.dateoj between '".$start ."' and '".$finish ."') order by id desc ");
$data[$key]["clients"]=$users3;*/

}
//dd($data);

  return view('admin.recuiterofferreports',compact('data','usersval','maxempcode'));

         
        
        
        
        
        
    }     
    
      
public function updatespockdata(Request $request)
    {
    
       $assigneeid=Auth::user()->id;
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$request['clientreq_posid']])
                        ->update(['c.dateoj'=>$request['doj'],'c.cv_status'=>$request['status']]);

DB::table('tbl_recruiter_cvchangestatus')
                        ->where(['cvid'=>$request['clientreq_posid']])
                        ->update(['cvupdatestatus'=>'0']);



DB::table('tbl_recruiter_cvchangestatus')->insert(
    ['cvid' => $request['clientreq_posid'],'loginid'=>$assigneeid,'recuiterid'=>$assigneeid, 'dateoj' => $request['doj'], 'cvupdatestatus' => '1', 'cv_status' =>$request['status']]
);

   return redirect('Spocofferreports');
    }
    
    
public function updatecrecuiterdata(Request $request)
    {
              $assigneeid=Auth::user()->id;
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$request['clientreq_posid']])
                        ->update(['c.dateoj'=>$request['doj'],'c.cv_status'=>$request['status']]);

DB::table('tbl_recruiter_cvchangestatus')
                        ->where(['cvid'=>$request['clientreq_posid']])
                        ->update(['cvupdatestatus'=>'0']);



DB::table('tbl_recruiter_cvchangestatus')->insert(
    ['cvid' => $request['clientreq_posid'],'loginid'=>$assigneeid,'recuiterid'=>$assigneeid, 'dateoj' => $request['doj'], 'cvupdatestatus' => '1', 'cv_status' =>$request['status']]
);


   return redirect('recuiterofferreports');
    }
    
public function updaterecfollowup(Request $request)
    {
        $assigneeid=Auth::user()->id;

   //   print_r($_POST);
      for ($x = 0; $x < $_POST['countval']; $x++) {
       if($_POST['followupcheckbox']==$x)
       {
       DB::table('tbl_recruiter_cvupdateddata')->insert(
    ['rc_resumeid' => $request['cvid'],'loginid'=>$assigneeid,'recuiterid'=>$assigneeid, 'followup_status' =>$x]
);
       }
}

   return redirect('recuiterofferreports');
/*DB::table('tbl_recruiter_cvupdateddata')->insert(
    ['rc_resumeid' => $request['cvid'],'loginid'=>$assigneeid,'recuiterid'=>$assigneeid, 'followup_status' =>$request['status']]
);


    }
    
    
    
    
public function updatespocfollowup(Request $request)
    {
        $assigneeid=Auth::user()->id;

   //   print_r($_POST);
      for ($x = 0; $x < $_POST['countval']; $x++) {
       if($_POST['followupcheckbox']==$x)
       {
       DB::table('tbl_recruiter_cvupdateddata')->insert(
    ['rc_resumeid' => $request['cvid'],'loginid'=>$assigneeid,'recuiterid'=>$assigneeid, 'followup_status' =>$x]
);
       }
}

   return redirect('Spocofferreports');
/*DB::table('tbl_recruiter_cvupdateddata')->insert(
    ['rc_resumeid' => $request['cvid'],'loginid'=>$assigneeid,'recuiterid'=>$assigneeid, 'followup_status' =>$request['status']]
);
*/


    }
    
    
    
    
public function getresumesrecuiterofferreport($id)
    {
           $assigneeid=Auth::user()->id;

   $assigneeid=Auth::user()->id;

$users3= DB::select("select dateoj,date(created_at) as cdate from tbl_recruiter_cvchangestatus  where cvid=".$id." and cv_status='4' order by rcvid desc limit 0,1");
//$data[$key]["clients"]=$users3;
//Our "then" date.
$then = $users3[0]->dateoj;
 //$then = "2018-09-03";
//Convert it into a timestamp.
$then = strtotime($then);
 
//Get the current timestamp.
 $now = strtotime($users3[0]->cdate); 
 
//Calculate the difference.
$difference = $then - $now;
 
 
 
//Convert seconds into days.
$days = floor($difference / (60*60*24) );
 if(($days/7)>0)
 {
$noofdays= floor($days/7);
$users9= DB::select("select * from tbl_recruiter_cvupdateddata  where rc_resumeid=".$id." order by followup_status asc ");
$exp="";
//print_r($users9); exit;
foreach($users9 as $us9)
{
  //$exp +=implode(',',$us9->followup_status);
  $exp =$exp.$us9->followup_status.',';
 //  $exp = implode(",",$us9->followup_status);
//print_r($us9->followup_status);
}
#echo $exp; exit;
if(empty($exp))
{
  $explval=array();
}
else
{
  $explval=explode(",", trim($exp,','));
}
//$a=array(trim($exp,','));
//$a=array(0,1);
#print_r($explval); exit;
$html="";
/*if(count($users9)>0) {
foreach($users9 as $us9)
{*/
  $checkval=0;
for ($x = 0; $x < $noofdays; $x++) {

  //if($us9->followup_status==$x)
  if(in_array($x, $explval))
  {
$html .= '<td>followup '.($x+1).'
  &nbsp Completed</td>';
  
  }
  else
  {
    if(empty($checkval))
    {
      $checkval=$x+1;
    }
    else
    {
      $checkval=$checkval;
    }
    #echo $checkval;exit;
    
 if($checkval-1==$x)
 {
  $html .= '<td>followup '.($x+1).'<input type="radio" name="followupcheckbox" value="'.$x.'">
  &nbsp<input type="hidden" value="'.$noofdays.'" name="countval"><input type="hidden" value="'.$id.'" name="cvid"></td>';  
 }
 else
 {
  $html .= '<td>followup '.($x+1).'<input type="radio" name="followupcheckbox" value="'.$x.'" disabled>
  &nbsp<input type="hidden" value="'.$noofdays.'" name="countval"><input type="hidden" value="'.$id.'" name="cvid"></td>';
  }  
  }
 }
/*}
}
else
{$html .= '<td>followup '.($x+1).'<input type="radio" name="followupcheckbox" value="'.$x.'">
  &nbsp<input type="hidden" value="'.$noofdays.'" name="countval"><input type="hidden" value="'.$id.'" name="cvid"></td>';


}
*/
}

/*  $role = DB::table('tbl_recruiter_cvupdateddata')
                   ->select('*')
                   ->where('rc_resumeid','=',$id)
                   ->get();
*/
 /*$html="";
  foreach($role as $key => $val){
$html .= '  <tr><td>'.$val->rc_updatededdata.'</td><td>'.$val->created_at.'</td></tr>';

  }
echo $html;*/
     echo $html;
     
}


           public function joinedcandidate()
    {
       

    $usersval= DB::select("select * from users where emp_role like '%3%'");      

     //dd($_GET['user']);
    //echo date('Y-m-01',strtotime(date('Y-m-d')));

        
        if(isset($_GET['user'])){
          $name=$_GET['user'];
        }else{
          $name=-1;
        }
        if(isset($_GET['doj']) && $_GET['doj']!=''){
          $from=$_GET['doj'];
        }else{
          $from=date('Y-m-01',strtotime(date('Y-m-d')));
        }
        if(isset($_GET['doend']) && $_GET['doend']!=''){
          $to=$_GET['doend'];
        }else{
          $to=date('Y-m-t',strtotime(date('Y-m-d')));;
        }
        if(isset($_GET['spoc'])){
          $spoc=$_GET['spoc'];
        }else{
          $spoc=-1;
        }
        if(isset($_GET['client'])){
          $client=$_GET['client'];
        }else{
          $client=-1;
        }
        if(isset($_GET['position'])){
          $position=$_GET['position'];
        }else{
          $position=-1;
        }
        
    /*   $query = "select rcv.id,c.client_id,r.expectedsalery,r.dateoj,c.comp_name,jd.clientjob_title,u.name as recruitername,uspoc.name as spocname,rcv.candidate_name ,(select (count(recuiter_invoiceid)) from tbl_recuiter_invoice where recuiter_resid=rcv.id and recuiter_invoicestatus='1' ) as invoicedet from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id join tbl_clients as c on rcv.clientsid=c.client_id join tbl_clientjd_master as jd on rcv.position_id=jd.clientjob_id join users as u on rcv.recruiter_id=u.id join users as uspoc on rcv.spockid=uspoc.id where r.cv_status=6 and r.cvupdatestatus=1 and date(r.dateoj) BETWEEN '".$from."' and  '".$to."' and (u.name like concat('%','".$name."','%') or '".$name."'=-1) and (uspoc.name like concat('%','".$spoc."','%') or '".$spoc."'=-1) and (c.comp_name like concat('%','".$client."','%') or '".$client."'=-1) and (jd.clientjob_title like concat('%','".$position."','%') or '".$position."'=-1) order by r.dateoj asc
       ";*/

    $query = "select rcv.id,c.client_id,r.expectedsalery,r.dateoj,c.comp_name,jd.clientjob_title,u.name as recruitername,uspoc.name as spocname,rcv.candidate_name ,(select (count(recuiter_invoiceid)) from tbl_recuiter_invoice where recuiter_resid=rcv.id and recuiter_invoicestatus='1' ) as invoicedet from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id join tbl_clients as c on rcv.clientsid=c.client_id join tbl_clientjd_master as jd on rcv.position_id=jd.clientjob_id join users as u on rcv.recruiter_id=u.id join users as uspoc on jd.clientjob_empid=uspoc.id where r.cv_status=6 and r.cvupdatestatus=1 and date(r.dateoj) BETWEEN '".$from."' and  '".$to."' and (u.name like concat('%','".$name."','%') or '".$name."'=-1) and (uspoc.name like concat('%','".$spoc."','%') or '".$spoc."'=-1) and (c.comp_name like concat('%','".$client."','%') or '".$client."'=-1) and (jd.clientjob_title like concat('%','".$position."','%') or '".$position."'=-1) order by r.dateoj asc
       ";

      $users = DB::select(DB::raw($query));
 // dd($users);



     

        return view('admin.joinedcandidate',compact('users','from','to'));
        //  return view('admin.adminselectrole');
    }


public function Spocofferreports()
    {
//dd(Auth::user());
 $assigneeid=Auth::user()->id;
  $usersval= DB::select("select * ,MAX( emp_role) as rolen from users where  id=".$assigneeid."");
//  dd($usersval);
//$v = array($usersval[0]->emp_role);
//dd($v);
 $str = $usersval[0]->emp_role;
$a  = [];
for($i=0; $i<strlen($str); $i++){
array_push($a, $str[$i]);
}
//print_r($a);
$maxempcode= max($a);

if($maxempcode!='4'  )
{
  
  
if($maxempcode!='7'  )
{
 
if(isset($_GET['user']) && $_GET['user']!='' )
{
$users= DB::select("select * from users where  emp_role like '%2%' and name='".$_GET['user']."'  and id=".$assigneeid."");
}
else
{
  $users= DB::select("select * from users where  emp_role like '%2%'  and id=".$assigneeid."");
}
}
else
{
     
if(isset($_GET['user']) && $_GET['user']!='' )
{
$users= DB::select("select * from users where  emp_role like '%2%' and name='".$_GET['user']."'");
}
else
{
  $users= DB::select("select * from users where  emp_role like '%2%'");
}







}
}
else
{
    
    
if(isset($_GET['user']) && $_GET['user']!='' )
{
$users= DB::select("select * from users where  emp_role like '%2%' and name='".$_GET['user']."'");
}
else
{
  $users= DB::select("select * from users where  emp_role like '%2%'");
}
}
   
$data=[];
if(isset($_GET['doj']) and $_GET['doj']!='')
{
$start = $_GET['doj'];
$finish = (date('D') != 'Sat') ? date('Y-m-d', strtotime('next Saturday')) : date('Y-m-d');

}
if(isset($_GET['doend']) and $_GET['doend']!='')
{
    $start = $_GET['doj'];
     $finish = $_GET['doend'];
}
else
{
  
               $start = date('Y-m-01',strtotime(date('Y-m-d')));
  $finish =  date('Y-m-t',strtotime(date('Y-m-d')));
  
  /*
    $start = (date('D') != 'Mon') ? date('Y-m-d', strtotime('last Monday')) : date('Y-m-d');
    $finish = date('Y-m-l');*/
}








  foreach($users as  $key => $value) {



$data[$key]["username"]=$value->name;
$data[$key]["empcode"]=$value->id;

$users1= DB::select("select * from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as b on a.id=b.cvid  where  a.spockid=".$value->id."  and (b.cv_status='4' or b.cv_status='6') and b.cvupdatestatus=1 and (b.dateoj between '".$start ."' and '".$finish ."') order by id desc ");
$data[$key]["resumes"]=$users1;

$users2= DB::select("
select b.r_map_id,a.clientjob_id, b.position_id, b.dateoj,a.clientjob_title from tbl_clientjdrecruitermap as c join tbl_clientjd_master as a  on a.clientjob_id=c.fk_jdid  

join tbl_recruiter_cv as b on b.r_map_id=c.clientreq_id

join tbl_recruiter_cvchangestatus as m on b.id=m.cvid

where b.spockid=".$value->id."  and (m.cv_status='4' or m.cv_status='6')  and m.cvupdatestatus=1  and (m.dateoj between '".$start ."' and '".$finish ."') order by id desc ");
$data[$key]["positions"]=$users2;

$users3= DB::select("select a.comp_name  from  tbl_clients as a join tbl_recruiter_cv as b on a.client_id=b.clientsid 

join tbl_recruiter_cvchangestatus as m on b.id=m.cvid

where  b.spockid=".$value->id."  and (m.cv_status='4' or m.cv_status='6')   and m.cvupdatestatus=1  and (m.dateoj between '".$start ."' and '".$finish ."') order by id desc ");
$data[$key]["clients"]=$users3;

}
//dd($data);

  return view('admin.spocofferreports',compact('data','usersval'));

    }
    
    public function drivereports()
    {
         if(isset($_GET['user']) && $_GET['user']!='')
        {
      $quser="where cm.clientjob_title='".$_GET['user']."'";      
        }
        else
        {
            $quser="where cvall.tca_status='Active'"; 
        }
        if(isset($_GET['doend']) && $_GET['doend']!='')
        {
               $start = $_GET['doj'];
  $finish =  $_GET['doend'];
        }
        else
        {
              $start = date('Y-m-01',strtotime(date('Y-m-d')));
  $finish =  date('Y-m-t',strtotime(date('Y-m-d'))); 
        }
        
         
  
  
  
  $query = 'select rc.id, cvall.tca_positionid,cm.*,cl.comp_name ,(select count(tca_id) from tbl_cv_allocated where  tca_positionid=rc.position_id ) as total_rec_cv 
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Pending" and tca_positionid=rc.position_id and (Date(cvall.tca_modifiedon) between "'.$start.'" and "'.$finish.'" )) as pending
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Confirm" and tca_positionid=rc.position_id and (Date(cvall.tca_modifiedon) between "'.$start.'" and "'.$finish.'" )) as Confirm
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Not Going" and tca_positionid=rc.position_id and (Date(cvall.tca_modifiedon) between "'.$start.'" and "'.$finish.'" )) as NotGoing
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Not Reachable" and tca_positionid=rc.position_id and (Date(cvall.tca_modifiedon) between "'.$start.'" and "'.$finish.'" )) as NotReachable
  ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Not Responding" and tca_positionid=rc.position_id and (Date(cvall.tca_modifiedon) between "'.$start.'" and "'.$finish.'" )) as NotResponding
   ,(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Dicey" and tca_positionid=rc.position_id and (Date(cvall.tca_modifiedon) between "'.$start.'" and "'.$finish.'" )) as Dicey
  from tbl_clientjd_master as cm join tbl_recruiter_cv as rc on rc.position_id=cm.clientjob_id
  join tbl_cv_allocated as cvall on cvall.tca_cvid=rc.id join tbl_clients as cl on cl.client_id=cm.clientjob_compid and ((Date(cm.drive_time) between "'.$start.'" and "'.$finish.'" ) || (Date(cm.lineup_to) between "'.$start.'" and "'.$finish.'" ) || (Date(cm.shortlist_time) between "'.$start.'" and "'.$finish.'" ))
'.$quser.'    group by rc.position_id   ';
   $clientposresumes = DB::select(DB::raw($query));
 /* 
   $query = "select * from 
       ";
      $users = DB::select(DB::raw($query));*/
      
        /*  $clientposresumes   =DB::table('tbl_clientjd_master as cm')
        ->join('tbl_recruiter_cv as rc','rc.position_id','=','cm.clientjob_id')
          ->join('tbl_cv_allocated as cvall','cvall.tca_cvid','=','rc.id')
        ->join('tbl_clients as cl','cl.client_id','=','cm.clientjob_compid')
        ->select('cvall.tca_positionid','cm.*','cl.comp_name'
         ,DB::raw('(select count(tca_id) from tbl_cv_allocated where  tca_positionid=rc.position_id) as total_rec_cv')
        
        ,DB::raw('(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Pending" and tca_positionid=rc.position_id) as pending')
        ,DB::raw('(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Confirm" and tca_positionid=rc.position_id) as Confirm')
        ,DB::raw('(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Not Going" and tca_positionid=rc.position_id) as NotGoing')
        ,DB::raw('(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Not Reachable" and tca_positionid=rc.position_id) as NotReachable')
        ,DB::raw('(select count(tca_id) from tbl_cv_allocated where tca_followupstatus="Not Responding" and tca_positionid=rc.position_id) as NotResponding')
        
        
        )
       // ->where('cm.clientjob_empid','=',$assigneeid)
       ->where('cm.drive_shortlist','=','drive')
        ->where('rc.cv_status','=',22)
      // ->where('rc.assign_status','=',1)
       ->groupBy('rc.position_id')
        ->get(); */
       // dd( $clientposresumes);
     return view('admin.drivereports',compact('clientposresumes','start','finish'));
    }
    
   public function drivereportsdata()
    {
       if(isset($_GET['user']) && $_GET['user']!='')
        {
      $quser="where cm.clientjob_title='".$_GET['user']."'";      
        }
        else
        {
            $quser="where cvall.tca_status='Active'"; 
        }


        if(isset($_GET['doend']) && $_GET['doend']!='')
        {
               $start = $_GET['doj'];
  $finish =  $_GET['doend'];
        }
        else
        {
              $start = date('Y-m-01',strtotime(date('Y-m-d')));
  $finish =  date('Y-m-t',strtotime(date('Y-m-d'))); 
        }
        
  
$query = 'select rc.id, cvall.tca_positionid,cm.*,cl.comp_name
,(select count(tca_id) from tbl_cv_allocated where  tca_positionid=rc.position_id ) as total_rec_cv 
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="7" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as OnTheWay
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="8" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as Interviewed
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="9" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as NotGoing
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="17" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as NotResponding
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="18" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as Confirmed
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="22" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as shortlistclient
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="23" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as Dicey
 ,(select count(id) from tbl_recruiter_cv as a join tbl_recruiter_cvchangestatus as c on c.cvid=a.id where a.cv_status="24" and c.cvupdatestatus=1 and a.position_id=cvall.tca_positionid  and (Date(c.created_at) between "'.$start.'" and "'.$finish.'" )) as Reached
 

  from tbl_clientjd_master as cm join tbl_recruiter_cv as rc on rc.position_id=cm.clientjob_id
  join tbl_cv_allocated as cvall on cvall.tca_cvid=rc.id join tbl_clients as cl on cl.client_id=cm.clientjob_compid and (Date(cm.drive_time) between "'.$start.'" and "'.$finish.'" )
   '.$quser.'  group by rc.position_id ';
   $clientposresumes = DB::select(DB::raw($query));
   
   return view('admin.todaydatedrivereports',compact('clientposresumes','start','finish'));
    } 
    
    
 public function prepostreports()
    {

if(isset($_GET['doend']) && $_GET['doend']!='' )
{
$fromdate=$_GET['doend'];
$todate=$_GET['doend'];
  }
  else
  {
    $fromdate=date('Y-m-d');
$todate=date('Y-m-d');
  }


$data=[];
$query="where ( date(ats.created_at) between '".$fromdate."' and '".$todate."' ) ";
if(isset($_GET['user']) && $_GET['user']!='' )
{
$query="and us.name='".$_GET['user']."'";
}


      $users1= ("select ats.punchrecord ,  ats.empname , ats.empcode , st.clientstart_starttime  , st.clientstart_stoptime ,us.id,st.clientreq_id,us.name,us.emp_empid,
(select concat(clientjob_title,'#' ,fn_clientname(clientjob_compid)) 
  from  tbl_clientjd_master where 
clientjob_id =(select fk_jdid from  tbl_clientjdrecruitermap where  clientreq_id=st.clientreq_id and 
  st.clientstart_recuiterid=us.id and  ( date(st.created_at) between '".$fromdate."' and '".$todate."' )  order by fk_jdid asc  limit 1  )) as position_work   
from
 tbl_clientrec_start_endtime as st inner join users as us on us.id=st.clientstart_recuiterid and ( date(st.created_at) between '".$fromdate."' and '".$todate."' ) 
inner join attendanceuser  as ats on ats.empcode=us.emp_empid ".$query."  group by ats.empcode
 ");


 $pos = DB::select(DB::raw($users1));
//print_r( $pos );
foreach($pos as  $key => $value) {
    
$data[$key]["punchrecord"]=$value->punchrecord;
$data[$key]["empname"]=$value->name;
$data[$key]["empcode"]=$value->emp_empid;
$data[$key]["clientstart_starttime"]=$value->clientstart_starttime;
$data[$key]["clientstart_stoptime"]=$value->clientstart_stoptime;
$data[$key]["id"]=$value->id;
$data[$key]["clientreq_id"]=$value->clientreq_id;
$data[$key]["position_work"]=$value->position_work;

$selectresume=DB::select("select rc.id, rc.cv_upload_time,rc.cv_uploadtime  from tbl_recruiter_cv as rc where rc.recruiter_id=".$value->id." and rc.r_map_id=".$value->clientreq_id." and (date(rc.created_at) between '".$fromdate."' and '".$todate."') " ) ;
$data[$key]["resumes"]=$selectresume;

$selectclients=DB::select("select rc.* from tbl_clientrec_start_endtime as rc where rc.clientstart_recuiterid=".$value->id."  and (date(rc.created_at) between '".$fromdate."' and '".$todate."') " ) ;
$data[$key]["clientstartend"]=$selectclients;

}
/*$selectresume="select rc.id, rc.cv_uploadtime from tbl_recruiter_cv as rc where rc.recruiter_id=".$value->id." and position_id=".$value->clientreq_id." and date(rc.created_at) =".$fromdate." " ;*/

//print_r($data);
 return view('admin.prepostreports',compact('data','fromdate','todate'));
//print_r($pos);
   
     }   
    
    

 public function getresumesprepostnewreports(Request $request)
    {
//echo "hiii"; exit;
//echo $diffmin.'--'.$fk_id,'--'.$clreqid.'--'.$frtdate.'--'.$todate.'--'.$stpunch; exit;

//print_r($_POST);

$selectresume=DB::select("select rc.* from tbl_clientrec_start_endtime as rc where rc.clientstart_recuiterid=".$_POST['fk_id']."  and (date(rc.created_at) between '".$_POST['frtdate']."' and '".$_POST['todate']."') " ) ;
$resumes=$selectresume;

$html="<table width='100%' border='1'>";
$html.="<tr><td>Date</td><td>Start Time</td><td>End Time</td><td>Time Gap</td></tr> ";


  foreach($resumes as $keyus => $k){

if($keyus=='0')
{
  $k->clientstart_stoptimen=$k->clientstart_starttime;
$k->clientstart_starttime=$_POST['stpunch'];

}
else
{
 // print_r($resumes[$keyus-1]->clientstart_stoptime);
  if($resumes[$keyus-1]->clientstart_stoptime!='') {
$k->clientstart_starttime=$resumes[$keyus-1]->clientstart_stoptime;
   } else { 
$k->clientstart_starttime='09:30:00';
   }

  $k->clientstart_stoptimen=$k->clientstart_stoptime;
}

//echo  $k->clientstart_starttime."--".($keyus-1);
    if( $k->clientstart_stoptime!='') {
                   $stoprecord = strtotime( $k->clientstart_stoptimen);
                     $stoprecordn = ( $k->clientstart_stoptimen);
    }
    else
    {
        $stoprecord = strtotime( '19:00:00');
         $stoprecordn =( '19:00:00');
    }
     $startrecord = strtotime($k->clientstart_starttime);


 $startrecordnew =strtotime(date('H:s:i', $startrecord));
$stoprecordnew =strtotime(date('H:s:i', $stoprecord));

 $diff_in_minutes  =round(((($stoprecord - $startrecord) )));
  $diff_in_minutesne  =round(((($stoprecord - $startrecord) / 60)));
$seconds = $diff_in_minutes;
$ee= gmdate('H:i:s', $seconds);
if( $diff_in_minutesne>30) {
$html.="<tr><td>".date('d-m-Y', strtotime($k->created_at))."</td><td>".$k->clientstart_starttime."</td><td>".$stoprecordn."</td><td>".$ee."</td></tr> ";
}}

$html.="</table>";

echo $html;

/*
exit;

for ($i = 0; $i < (($_POST['diffmin'])); $i++) {
  $t1=30*$i;
  $t2=30*($i+1);
$time = strtotime($_POST['stpunch']);
$startTime = date("H:i:s", strtotime($t1.'minutes', $time));
$endTime = date("H:i:s", strtotime($t2.' minutes', $time));
echo $startTime.'--'.$endTime .'<br>';
}*/
}



public function getresumesprepostdatareports($fk_id,$frtdate,$todate)
    {
$selectresume=DB::select("select rc.created_at,rc.candidate_name,rc.candidate_email,rc.candidate_mob,rc.id, rc.cv_upload_time,rc.cv_uploadtime  from tbl_recruiter_cv as rc where rc.recruiter_id=".$fk_id."  and (date(rc.created_at) between '".$frtdate."' and '".$todate."') " ) ;
$resumes=$selectresume;

if(count($resumes)>0) {
$numstend=0;
$html="<table width='100%' border='1'>";
$html.="<tr><td>Name</td><td>Start Time</td><td>End Time</td><td>Time Gap</td><td>Date</td></tr> ";


  foreach($resumes as $keyus => $k){

$ketcount=($keyus-1);
 if( $keyus>0)
 {
 
                 $startrecord = strtotime($resumes[$ketcount]->cv_upload_time);
                   $stoprecord = strtotime( $k->cv_upload_time);

 $startrecordnew =strtotime(date('H:s:i', $startrecord));
$stoprecordnew =strtotime(date('H:s:i', $stoprecord));

 $diff_in_minutes  =round(((($stoprecordnew - $startrecordnew) / 60)/30)). "";
//echo $diff_in_minutes ."<br>";
if($diff_in_minutes >0)
{
   $startrecorddata =(date('H:s:i', $startrecord));
 $stoprecorddata  = (date('H:s:i', $stoprecord));

$html.="<tr><td>".$k->candidate_name."</td><td>".$startrecorddata."</td><td>".$stoprecorddata."</td><td><table>";



for ($i = 0; $i < ($diff_in_minutes); $i++) {
  $t1=30*$i;
  $t2=30*($i+1);
$time = strtotime($startrecorddata);
$startTime = date("H:i:s", strtotime($t1.'minutes', $time));
$endTime = date("H:i:s", strtotime($t2.' minutes', $time));

$html.="<tr><td>".$startTime."-".$endTime."</td></tr>";
//echo $startTime.'--'.$endTime .'<br>';
}
$html.="</table></td><td>".$k->created_at."</td></tr>";

// echo $startrecorddata."--".$stoprecorddata."<br>";
  //echo $diff_in_minutes ."<br>";
}
//echo  $startrecordnew."--".$stoprecordnew."<br>";
               

}


 }

}



//echo $fk_id,'--'.$clreqid.'--'.$frtdate.'--'.$todate; exit;


$html.="</table>";

echo $html;

 
}


 public function lateentryreports()
    {


if(isset($_GET['doend']) && $_GET['doend']!='' )
{
$fromdate=$_GET['doj'];
$todate=$_GET['doend'];
  }
  else
  {
    $fromdate=date('Y-m-d');
$todate=date('Y-m-d');
  }


$data=[];
$query="where ( date(ats.created_at) between '".$fromdate."' and '".$todate."' and ats.empcode NOT IN(502,503,303,236,305,308,532) ) ";
if(isset($_GET['user']) && $_GET['user']!='' )
{
$query .="and ats.empname='".$_GET['user']."'";
}


      $users1= ("select ats.lastpunch,ats.punchrecord ,  ats.empname , ats.empcode 
from attendanceuser  as ats  ".$query."  
 ");


 $data = DB::select(DB::raw($users1));

/*$selectresume="select rc.id, rc.cv_uploadtime from tbl_recruiter_cv as rc where rc.recruiter_id=".$value->id." and position_id=".$value->clientreq_id." and date(rc.created_at) =".$fromdate." " ;*/

//print_r($data);
 return view('admin.lateentryreports',compact('data','fromdate','todate'));
//print_r($pos);
   
    
    }  



public function searchattusers(Request $request)
        {
 $assigneeid=Auth::user()->id;
       $key = $request['query'];
        $query = "SELECT * FROM attendanceuser WHERE empname LIKE '%".$key."%' group by empname";  
        
       
       $result = DB::select(DB::raw($query));
//print_r(count($result));
  $output = '<ul class="list-unstyled">';  
   if(count($result) > 0)  
      { 
          foreach($result as $row)
          
           {  
          $output .= '<li id='.$row->empname.'>'.$row->empname.'</li>';  
           }
      }
       else  
      {  
           $output .= '<li>Users Not Found</li>';  
      } 
  
  
  
    $output .= '</ul>';  
      echo $output;  
        }
        
        
     public function invoicegenerated()
    {
       

    $usersval= DB::select("select * from users where emp_role like '%3%'");      

     //dd($_GET['user']);
    //echo date('Y-m-01',strtotime(date('Y-m-d')));

        
        if(isset($_GET['user'])){
          $name=$_GET['user'];
        }else{
          $name=-1;
        }
        if(isset($_GET['doj']) && $_GET['doj']!=''){
          $from=$_GET['doj'];
        }else{
          $from=date('Y-m-01',strtotime(date('Y-m-d')));
        }
        if(isset($_GET['doend']) && $_GET['doend']!=''){
          $to=$_GET['doend'];
        }else{
          $to=date('Y-m-t',strtotime(date('Y-m-d')));;
        }
        if(isset($_GET['spoc'])){
          $spoc=$_GET['spoc'];
        }else{
          $spoc=-1;
        }
        if(isset($_GET['client'])){
          $client=$_GET['client'];
        }else{
          $client=-1;
        }
        if(isset($_GET['position'])){
          $position=$_GET['position'];
        }else{
          $position=-1;
        }
        
    $query = "select rcv.id,c.client_id,r.expectedsalery,r.dateoj,c.comp_name,jd.clientjob_title,u.name as recruitername,uspoc.name as spocname,rcv.candidate_name ,(select (count(recuiter_invoiceid)) from tbl_recuiter_invoice where recuiter_resid=rcv.id and recuiter_invoicestatus='1' ) as invoicedet from tbl_recruiter_cvchangestatus as r join tbl_recruiter_cv as rcv on r.cvid=rcv.id join tbl_clients as c on rcv.clientsid=c.client_id join tbl_clientjd_master as jd on rcv.position_id=jd.clientjob_id join users as u on rcv.recruiter_id=u.id join users as uspoc on jd.clientjob_empid=uspoc.id where r.cv_status=6 and r.cvupdatestatus=1 and date(r.dateoj) BETWEEN '".$from."' and  '".$to."' and (u.name like concat('%','".$name."','%') or '".$name."'=-1) and (uspoc.name like concat('%','".$spoc."','%') or '".$spoc."'=-1) and (c.comp_name like concat('%','".$client."','%') or '".$client."'=-1) and (jd.clientjob_title like concat('%','".$position."','%') or '".$position."'=-1) order by r.dateoj asc
       ";
      $users = DB::select(DB::raw($query));
 // dd($users);



     

        return view('admin.invoicegenerated',compact('users','from','to'));
        //  return view('admin.adminselectrole');
    }
    
public function getjoinuplistreport($id,$nid)
    {
      $querycvamount = DB::select("select * from tbl_recuiter_invoice where recuiter_resid=".$nid." and recuiter_invoicestatus='1'");
     // echo $id.'--'.$nid;
$html="<table width='100%' border='1'>";
      $query = DB::select("select * from tbl_clientsdetails where clientsdetails_clientid=".$id."");
       $querybillingamount = DB::select("select rcha.*,rcv.candidate_name from tbl_recruiter_cvchangestatus as rcha join tbl_recruiter_cv as rcv on rcv.id=rcha.cvid where rcha.cvid=".$nid." and rcha.cvupdatestatus='1'");
     //  print_r($querybillingamount[0]->expectedsalery );
  if(count($query)>0)
  {
    foreach($query as $row)
    {
      $invpdf=  DB::table('tbl_clientsdetails as e')
->select('*')
->join('tbl_state as f','f.state_id','=','e.clientsdetails_cityid')
->whereRaw('e.clientsdetails_id='.$querycvamount[0]->recuiter_invoiceselectadd.'')
->first();
      if($querycvamount[0]->recuiter_invoiceselectadd==$row->clientsdetails_id)
        {$selectinv='checked';} else {$selectinv="";}
$html .="<tr><td><input type='radio' name='selectadd' value='".$row->clientsdetails_id."' required ".$selectinv." ></td><td>".$row->clientsdetails_location."</td><td>".$row->clientsdetails_billingaddress."</td></tr>";
}
  }
  else { echo "Address Not Entered";}
if($querybillingamount[0]->expectedsalery!='') {$querybillingamount[0]->expectedsalery=$querybillingamount[0]->expectedsalery;} else {$querybillingamount[0]->expectedsalery="Salery Not Entered";}
$html .="<tr><td colspan='2'> Invoice Number  
             :</td><td> <input type='text' name='invno' required value=".$querycvamount[0]->recuiter_invno." ></td></tr>";
 $html .="<tr><td colspan='2'>Employee Name:</td><td> ".$querybillingamount[0]->candidate_name."</td></tr>";
  $html .="<tr><td colspan='2'>Emp Id</td><td> <input type='text' name='empid'   value=".$querycvamount[0]->recuiter_empid." ></td></tr>";

  $html .="<tr><td colspan='2'>Billable CTC:</td><td> ".$querybillingamount[0]->expectedsalery."</td></tr>";
  $html .="<tr><td colspan='2'>Percentage</td><td> <input type='text' name='percentage' required value=".$querycvamount[0]->recuiter_invoicepercentage." id='txt2'  onkeyup='sum()'>
<input type='hidden' name='rid' value=".$nid.">
<input type='hidden' name='pid' value=".$querycvamount[0]->recuiter_invoiceid.">
  </td></tr>";
  
   
  $html .="<tr><td colspan='2'> Billing Amount:</td><td> <input type='text' name='amount' required value=".$querycvamount[0]->recuiter_invoiceamount." id='txt1'  onkeyup='sum()'></td></tr>";
    if(count($query)>0)
  {
  $html .="<tr><td colspan='3' id='choiceLabelup'>
<table width='100%' border='1'><tr><td style='width:39%'>CGST</td><td>".$invpdf->state_cgst."
<input type='hidden' name='statecgst' value=".$invpdf->state_cgst."></td></tr>
<tr><td>SGST</td><td>".$invpdf->state_sgst."<input type='hidden' name='statesgst' value=".$invpdf->state_sgst."></td></tr>
<tr><td>IGST</td><td>".$invpdf->state_igst."<input type='hidden' name='stateigst' value=".$invpdf->state_igst."></td></tr></table>

  </td></tr>";
   } $html .="<tr><td colspan='2'>Total Amount</td><td> <input type='text' name='billing' required id='txt3' value=".$querycvamount[0]->recuiter_invoicetotamount." >
</td></tr>";
  
 if($querycvamount[0]->recuiter_sezstatus=='1') {$sezstatus="selected";$sezstatus1="";}
 if($querycvamount[0]->recuiter_sezstatus=='2') {$sezstatus="";$sezstatus1="selected";}
 else {$sezstatus="";$sezstatus1="";}
  $html .="<tr><td colspan='2'>Sezstatus</td><td> <select name='sezstatus'>
<option value='1' ".$sezstatus.">Not SEZ</option>
<option value='2' ".$sezstatus1.">SEZ</option>
  </select>
</td></tr>";

  if(count($query)>0)
  {
  $html .="<tr><td colspan='3'><input type='submit' name='submit' class='btn btn-primary waves-effect waves-light' ></td></tr><script type='text/javascript'>
  (function (){
    var radios = document.getElementsByName('selectadd');

    for(var i = 0; i < radios.length; i++){
           radios[i].onclick = function(){
            
            $.ajax({
        type:'GET',
        url:'gettaxreport/' + this.value ,
        data:'',
        success: function(data){
         // alert(data);
            $('#choiceLabelup').empty();
            $('#choiceLabelup').append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
        }
    }
})();
</script>"; }
echo $html;  }
    
    
    
public function getjoinlistreport($id,$nid)
    {
      $rand=rand();
     // echo $id.'--'.$nid;
$html="<table width='100%' border='1'>";
        $query = DB::select("select * from tbl_clientsdetails where clientsdetails_clientid=".$id."");
     $querybillingamount = DB::select("select rcha.*,rcv.candidate_name from tbl_recruiter_cvchangestatus as rcha join tbl_recruiter_cv as rcv on rcv.id=rcha.cvid where rcha.cvid=".$nid." and rcha.cvupdatestatus='1'");
     //  print_r($querybillingamount[0]->expectedsalery );
  if(count($query)>0)
  {
    foreach($query as $row)
    {
$html .="<tr><td><input type='radio' name='selectadd' value='".$row->clientsdetails_id."' required></td><td>".$row->clientsdetails_location."</td><td>".$row->clientsdetails_billingaddress."</td></tr>";
}
  }
  else { echo "Address Not Entered";}
if($querybillingamount[0]->expectedsalery!='') {$querybillingamount[0]->expectedsalery=$querybillingamount[0]->expectedsalery;} else {$querybillingamount[0]->expectedsalery="Salery Not Entered";}
$html .="<tr><td colspan='2'> Invoice Number:</td><td> <input type='text' name='invno' required value=".$rand." ></td></tr>";
$html .="<tr><td colspan='2'>Employee Name:</td><td> ".$querybillingamount[0]->candidate_name."</td></tr>";
$html .="<tr><td colspan='2'>Emp Id</td><td> <input type='text' name='empid'   ></td></tr>";

  $html .="<tr><td colspan='2'>Billable CTC:</td><td> ".$querybillingamount[0]->expectedsalery."</td></tr>";
  $html .="<tr><td colspan='2'>Percentage</td><td> <input type='text' name='percentage' required id='txt2'  onkeyup='sum()'>
<input type='hidden' name='rid' value=".$nid."></td></tr>";
  $html .="<tr><td colspan='2'> Billing  Amount:</td><td> <input type='text' name='amount' required id='txt1'  onkeyup='sum()' ></td></tr>";
  $html .="<tr><td colspan='3' id='choiceLabel'></td></tr>";
 $html .="<tr><td colspan='2'>Total Amount</td><td> <input type='text' name='billing' required id='txt3'  >

</td></tr>";
 
   
   $html .="<tr><td colspan='2'>Sezstatus</td><td> <select name='sezstatus'>
<option value='1' >Not SEZ</option>
<option value='2' >SEZ</option>
  </select>
</td></tr>";


  if(count($query)>0)
  {
  $html .="<tr><td colspan='4'><input type='submit' name='submit' class='btn btn-primary waves-effect waves-light' ></td></tr>
<script type='text/javascript'>
  (function (){
    var radios = document.getElementsByName('selectadd');

    for(var i = 0; i < radios.length; i++){
           radios[i].onclick = function(){
            
            $.ajax({
        type:'GET',
        url:'gettaxreport/' + this.value ,
        data:'',
        success: function(data){
         // alert(data);
            $('#choiceLabel').empty();
            $('#choiceLabel').append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
        }
    }
})();
</script>
  "; }
echo $html;
    }
     public function gettaxreport($id)
    {
$invpdf=  DB::table('tbl_clientsdetails as e')
->select('*')
->join('tbl_state as f','f.state_id','=','e.clientsdetails_cityid')
->whereRaw('e.clientsdetails_id='.$id.'')
->first();
 $html ="<table width='100%' border='1'><tr><td style='width:39%'>CGST</td><td>".$invpdf->state_cgst."<input type='hidden' name='statecgst' value=".$invpdf->state_cgst."></td></tr>
<tr><td>SGST</td><td>".$invpdf->state_sgst."<input type='hidden' name='statesgst' value=".$invpdf->state_sgst."></td></tr>
<tr><td>IGST</td><td>".$invpdf->state_igst."<input type='hidden' name='stateigst' value=".$invpdf->state_igst."></td></tr></table>
 ";
 echo $html;
//print_r($invpdf);
    }
}
