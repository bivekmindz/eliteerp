<?php
/**
 * Created by PhpStorm.
 * User: mindz
 * Date: 17/2/18
 * Time: 10:07 AM
 */

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use Mail;
use Session;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class MonsterController extends Controller
{

public function __construct()
    {
      $this->middleware('auth');
    }
  
public function clientkeyslist(){
      $keys= DB::table('tbl_monsterkeys as m')
            ->select('m.*')
            ->get();
   
        return view('admin.monsterkeylist',compact('keys'));
    }

public function searchhistorylist(){
  $query = "select ma.md_catid,ma.md_createdat,u.name,u.email from tbl_monsteractivitiesdays as ma inner join users as u on u.id=ma.md_userid WHERE 
date(ma.md_createdat) BETWEEN (CURRENT_DATE() - INTERVAL 1 MONTH) AND CURRENT_DATE() order by ma.md_id desc";

    $result = DB::select(DB::raw($query));
    return view('admin.monstersearchhistorylist',compact('result'));
}

public function todaysearchlist(){
  $query = "select ma.ma_id,u.name,u.email,ma.ma_catid,ma.ma_createdat,ma.ma_status from tbl_monsteractivities as ma inner join users as u on u.id=ma.ma_userid where date(ma.ma_createdat)=CURDATE() order by ma.ma_id desc";

    $result = DB::select(DB::raw($query));
    return view('admin.monstertodaysearchlist',compact('result'));
}

public function freekey(Request $request)
{
  $indmon = $request['indmon'];

  if(empty($indmon))
  {
    Session::flash('error_msg', 'Please select atleast one employee.');
    return redirect()->route('monstertodaysearch');
  }
  else
  {
      foreach ($indmon as $key => $value) 
      {
        DB::table('tbl_monsteractivitiesdays')
        ->where('md_ma_id', '=', $value)
        ->update([
            'md_status'=>'Inactive'
        ]);

         DB::table('tbl_monsteractivities')
        ->where('ma_id', '=', $value)
        ->update([
            'ma_status'=>'Inactive'
        ]);
      }
      Session::flash('success_msg', 'Free key for employee successfull.');
      return redirect()->route('monstertodaysearch');
  }  
}



}