<?php 

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Model\Client as Client;
use App\Model\Adminmodel\Role as Role;
use App\Model\Adminmodel\Department as Department;
use App\Model\Adminmodel\Employees as Employees;

use Illuminate\Support\Facades\Auth;
use App\Model\User;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use App\Exceptions\Handler;
use Exception;

 
use Illuminate\Http\Request;
 
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $redirectTo = '/admin/dashboard';
    protected $redirectAfterLogout = '/admin/login';

    public function __construct()
    {
        $this->middleware('auth');
    }
 
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd(session(['adminsess'=>'abcd']));


        return view('admin.adashboard');
    }
    public function Reactive($id)
    {
 

            $sql = DB::update("update users set emp_status = 1 where id = $id");
            if($sql)
            {
                Session::flash('success_msg', "successfully activated");
                return redirect()->route('admin.viewadminemployes');
            }
            else{
                Session::flash('success_msg', "try again");
                return redirect()->route('admin.viewadminemployes');
            }

    }
     public function adminclient()
    {
      
          //  $clients=Client::with(['user'])->get();
if(isset($_GET['user']))
    {
     $paged=$_GET['user'];
     $clients=  DB::table('tbl_clients as c')
       ->join('users as d','c.clientsuid','=','d.id')
         ->select('*','c.email as emailclient')
            ->where('c.client_status','!=',3)
 
       ->where('c.clientsuid','=', $paged)
   

           ->orderby('c.client_id','desc')
            ->paginate(15);
 }
 else
 {
     $clients=  DB::table('tbl_clients as c')
       ->join('users as d','c.clientsuid','=','d.id')
         ->select('*','c.email as emailclient')
            ->where('c.client_status','!=',3)
            ->orderby('c.client_id','desc')
            ->paginate(15);
 }
      


$users=  DB::table('users as c')
     ->select('id','name')
     ->where('c.emp_role','!=',4)
     ->get();
     // dd($users);
      //  $clients=Client::orderBy('created_at','desc')->paginate(500);
        return view('admin.adminclient',compact('clients','users'));


        //  return view('admin.adminselectrole');
      
      
      
      
    }
    //View Role
    public function adminrole()
    {
        $roles=Role::orderBy('role_name','asc')->paginate(500);
        return view('admin.adminselectrole',compact('roles'))
            ->with('i', (request()->input('page', 1) - 1) * 500);

      //  return view('admin.adminselectrole');
    }

    //Add Role
      public function viewadminrole()
    {
        return view('admin.viewrole');
    }
    //Add Role Table
    public function addadminrole(Request $request)
    {
        $this->validate($request,[
            'role_name' => 'required',
        ],[
            'role_name.required' => 'Name field is required.',
        ]);

        $postData = $request->all();
        $client_obj= new Role;
        $client_obj->role_name=$request['role_name'];
        $client_obj->role_status = 1;
       // //  dd($client_obj);
        $client_obj->save();

        Session::flash('success_msg', 'Role Added Successfully!');

        return redirect()->route('admin.viewadminrole');
    }
//View Role
    public function admindepartment()
    {
        $departments=Department::orderBy('dept_name','asc')->paginate(50);
        return view('admin.adminselectdepartment',compact('departments'))
            ->with('i', (request()->input('page', 1) - 1) * 50);

        //  return view('admin.adminselectrole');
    }
//View Department
    public function viewadmindepartment()
    {
        return view('admin.viewdepartment');
    }
//Add Department
    public function addadmindepartment(Request $request)
    {
        $this->validate($request,[
            'department_name' => 'required',
        ],[
            'department_name.required' => 'Department Name field is required.',
        ]);

        $postData = $request->all();
        $client_obj= new Department;
        $client_obj->dept_name=$request['department_name'];
        $client_obj->dept_status = 1;
        // //  dd($client_obj);
        $client_obj->save();

        Session::flash('success_msg', 'Department Added Successfully!');

        return redirect()->route('admin.viewadmindepartment');
    }

    //View Employees
    public function adminemployes()
    {
        // $cid=Auth::user()->id;
        $employees=Employees::orderBy('name','asc')
            ->where('emp_role','!=',4)
            ->paginate(50);
        // return Datatables::of($employees)->make();
        return view('admin.adminselectemployes',compact('employees'))
            ->with('i', (request()->input('page', 1) - 1) * 50);
    }

     //
    public function employee(){
        $roles=Role::all();
        $departments=Department::all();
        //dd($roles);

        return view('admin.newemp',compact('roles','departments'));
    }

    //Add User
    public function addadminusers(Request $request)
    {
       
 // print_r($_POST); exit;

        $this->validate($request,[
            'name' => 'required',
            'phone' => 'required|numeric',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'cpassword' => 'required|same:password',
            'doj' => 'required',
            'dept' => 'required',
            'role' => 'required',
             'salary' => 'required',
              'personalemailid' => 'required',
               'address' => 'required',
                'employement' => 'required',
                 'payroll' => 'required',
                'designation' => 'required'
        ],[
            'name.required' => 'Name field is required.',
            'phone.required' => 'Phone number field is required.|Numbers Only',
            'email.required' => 'Email field is required',
            'password.required' => 'Password field is required',
            'cpassword.required' => 'Confirm Password field is required',
//            'phone.max'=>'Phone number field must be of 10 digit only',
            'doj.required' => ' Date of joining field is required',
            'dept.required' => ' Department is required',
            'role.required' => ' Role is required',
             'salary.required' => ' Salary is required',
              'personalemailid.required' => ' Personal Email Id is required',
               'address.required' => ' Address is required',
                 'employement.required' => ' Employement is required',
                     'payroll.required' => ' Pay Roll is required',
                'designation.required' => ' Designation is required'
        ]);


        $postData = $request->all();
        $role = implode(',', $request['role']);
        $pass = $request['password'];
        $cpass = $request['cpassword'];
        $user_obj= new User;
        $user_obj->name = $request['name'];
        $user_obj->emp_contactno  = $request['phone'];
        $user_obj->email = $request['email'];
        $user_obj->password = bcrypt($request['password']);
        $user_obj->emp_doj = date('Y-m-d',strtotime($request['doj']));
        $user_obj->emp_dept = $request['dept'];
         $user_obj->emp_salary = $request['salary'];
          $user_obj->emp_personalemailid = $request['personalemailid'];
           $user_obj->emp_address = $request['address'];
           $user_obj->emp_empid = $request['empid'];
        $user_obj->emp_designation = $request['designation'];
        $user_obj->emp_employement = $request['employement'];
           $user_obj->emp_payroll = $request['payroll'];
        $user_obj->emp_role = $role;
       // print_r($user_obj); die;
        $user_obj->save();

       Session::flash('success_msg', 'Employee Added Successfully!');

        return redirect()->route('admin.viewadminemployes');
    }


    public function editadminemployes($id){

        try {

            $roles = Role::all();
            $users = User::find($id);
          //  dd($users);
            $departments = Department::all();
            return view('admin.editemployee',compact('roles','departments','users'));
        
            } catch (Exception $e) {
                LOG::error('THis is admin Add  Employee display errror'.$e->getMessage());
                return $this->errorBag($e);
            }


        }

        public function updateadminemployes() {
         //    print_r($_POST); exit;
            try {
                $userid = $_POST['uid'];
                $name = $_POST['name'];
                $phone = $_POST['phone'];
                $email =  $_POST['email'];
                $doj = date('Y-m-d',strtotime($_POST['doj']));
                $password = $_POST['password'];
                $dept =$_POST['dept'];
                $role = implode(',',$_POST['role']);
 $empid =$_POST['empid'];

   $salary =$_POST['salary'];
     $personalemailid =$_POST['personalemailid'];
       $address =$_POST['address'];
         $designation =$_POST['designation'];
          $employement =$_POST['employement'];
            $payroll =$_POST['payroll'];
                if($_POST['password'] != '')
                {
                    User::where('id', $userid)->update(array(
                        'name'=>$name,
                        'emp_contactno'=>$phone,
                        'email'=>$email,
                        'emp_doj'=>$doj,
                        'emp_dept'=>$dept,
                        'emp_empid'=>$empid,
                        'emp_salary'=>$salary,
                        'emp_personalemailid'=>$personalemailid,
                        'emp_address'=>$address,
                        'emp_designation'=>$designation,
                        'emp_employement'=>$employement,
                          'emp_payroll'=>$payroll,
                        'emp_role'=>$role
                    ));
                }
                else{
    
                    User::where('id', $userid)->update(array(
                        'name'    =>  $name,
                        'emp_contactno' =>  $phone,
                        'email' => $email,
                        'emp_doj' => $doj,
                        'emp_dept' => $dept,
                         'emp_empid'=>$empid,
                          'emp_salary'=>$salary,
                        'emp_personalemailid'=>$personalemailid,
                        'emp_address'=>$address,
                        'emp_designation'=>$designation,
                        'emp_employement'=>$employement,
                         'emp_payroll'=>$payroll,
                        'emp_role' => $role
                    ));
                }
    
                return redirect()->route('admin.viewadminemployes')->with('message', 'You have successfully updated');
            } catch (Exception $e) {
                LOG::error('THis is admin Edit  Employee display errror'.$e->getMessage());
                return $this->errorBag($e);
            }    //   return Redirect::route('viewadminemployes')->with('message', 'You have successfully updated');
        }   
       
        public function checkemail(Request $request) {
            $check = DB::table('users')
                      ->select('id')
                      ->where('email',$request->email)
                      ->first();
           
            if(!empty($check)){
                echo 'false';
            }else{
                echo 'true';
            }
        }

       public function deleteadminemployes( $id ){
           
            try {

                $users = User::find($id);
                $roles=Role::all();
                $departments=Department::all();
                User::where('id', $id)->update(array('emp_status' => '0'));
                return redirect()->route('admin.viewadminemployes')->with('message', 'You have successfully Deleted');

            } catch (Exception $e) {
                LOG::error('THis is admin Delete  Employee display errror'.$e->getMessage());
                return $this->errorBag($e);
            }  
        }

        public function changepassword() {
            try {
                $adminid=Auth::user()->id;
                $users = Admin::find($adminid);
    
                return view('admin.changepass',compact('users'));
    
            } catch (Exception $e) {
                LOG::error('THis is admin Change Password errror'.$e->getMessage());
                return $this->errorBag($e);
            }
        }



   public function checkemailexist(Request $request){
  
        $check = DB::table('users')
                      ->select('id')
                      ->where('email',$request->emailid)
                      ->first();
           echo $check; exit;
            if(!empty($check)){
                echo 'false';
            } else{
                echo 'true';
            }

    }


        public function adminPosition() 
        {
             $assigneeid=Auth::user()->id;
        $departments=Department::all();
        $client=DB::table('tbl_clientspockmap as c')
            ->join('tbl_clients as t','c.fk_clientid','=','t.client_id')
            ->select('c.fk_clientid','t.comp_name')
            ->where(['c.fk_empid'=>$assigneeid])
            ->distinct('c.fk_clientid')
            ->get();

        $emp=DB::table('users')
            ->select('name','id')
            ->whereRaw('FIND_IN_SET(3,emp_role)','emp_role')
            ->get();

        $target=DB::table('tbl_clientjdrecruitermap as c')
            ->join('users as u','u.id','=','c.fk_empid')
            ->select('u.name')
            ->where(['c.fk_assigneeid'=>$assigneeid])
            ->distinct('c.fk_assigneeid')
            ->get();
        return view('admin.position',compact('departments','client','emp','target'));
        }

 public function adminposlist()
    {
        $assigneeid=Auth::user()->id;
        $pos=DB::table('tbl_clientjd_master as m')
        ->join('tbl_clients as c','m.clientjob_compid','=','c.client_id')
        ->select('m.*','c.comp_name')
        ->where(['clientjob_empid'=>$assigneeid])
        ->get();
        //dd($pos);
        return view('admin.listposition',['pos'=>$pos]);

    }

     public function adminpositionjd(Request $request)
    {
        $position = DB::table('tbl_clientjd_master')
            ->select('upload_position_jd')
            ->where(['clientjob_id' => $request->id])
            ->first();

        return json_encode($position->upload_position_jd);
    }


        public function addpositionadmin(Request $request)
    {

        $clientJd = $request->input('upload_position_jd');
        $dom = new \DomDocument();
        $dom->loadHtml($clientJd, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');
        $clientJd = $dom->saveHTML();
     


        $this->validate($request,[
            'dept' => 'required',
            'compname'=>'required',
            'jobtitle' => 'required',
            'upload_position_jd' => 'required',
            'noofpos' => 'required|numeric',
            
        ], 
        [
            'dept.required' => 'Department field is required.',
            'compname.required'=>'company name field is required list',
            'jobtitle.required' => 'Jobtitle field is required.',
            'upload_position_jd' => 'Please Upload Job Decription',
            'noofpos.required' => 'Number of Positions field is required'
        ]);
        $assigneeid=Auth::user()->id;
        DB::table('tbl_clientjd_master')->insert([
            'clientjob_deptid'=>$request['dept'],
            'clientjob_compid'=>$request['compname'],
            'clientjob_title'  => $request['jobtitle'],
            'upload_position_jd' => $clientJd,
            'clientjob_noofposition'=>$request['noofpos'],
            'drive'=>1,
            'clientjob_empid'=>$assigneeid,
            'clientjob_otherinfo'=>$request['otherinfo'],
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now()

        ]);

        Session::flash('success_msg', 'Position Added Successfully!');

        return redirect()->route('adminAddPosition');

    }

public function handover()
    {
        $data=[];
        
        $userslist  = "select * from users where emp_status=1 ";
            $emp_list = DB::select(DB::raw($userslist));
 $users  = "select * from users ";
            $result = DB::select(DB::raw($users));
   foreach($result as $keyus => $userval){
  $data[$keyus]["userid"]=$userval->id;
    $data[$keyus]["username"]=$userval->name;
$selectclients=DB::select("select *,(select count(posh_id) from tbl_positionhandover where posh_posid=jdrmap.clientreq_id ) as poshand
,(select a.name from users as a join  tbl_positionhandover as b on b.posh_newrecuiter=a.id where b.posh_posid=jdrmap.clientreq_id order by b.posh_posid desc limit 0,1 ) as poshandname 
,(select a.comp_name from tbl_clients as a where a.client_id=jdmast.clientjob_compid) as clientname 
from tbl_clientjdrecruitermap as jdrmap join tbl_clientjd_master as jdmast on jdmast.clientjob_id=jdrmap.fk_jdid where jdrmap.fk_empid=".$userval->id." and jdrmap.clientrequpdate_status='1'  " ) ;
$data[$keyus]["position"]=$selectclients;
}
  return view('Emp.handover',compact('data','emp_list'));
    }


public function inserthandover(Request $request)
    {
          $id = Auth::user()->id;
     //  print_r($_POST);
$respos  = "select fk_empid from tbl_clientjdrecruitermap where clientreq_id='".$_POST['clientreq_id']."' ";
            $resultposi = DB::select(DB::raw($respos));

              DB::table('tbl_positionhandover')
                    ->insert([
                        'posh_oldrecuiter' => $resultposi[0]->fk_empid,
                        'posh_newrecuiter' => $_POST['recruiter'],
                        'posh_assignee' =>  $id,
                        'posh_posid' => $_POST['clientreq_id'],
                        'posh_status' => '1',
                        'posh_createdat' => date('Y-m-d H:i:s'),
                        'posh_updatedat' => date('Y-m-d H:i:s'),
                     
                ]);
 DB::table('tbl_clientjdrecruitermap')
            ->where('clientreq_id',$_POST['clientreq_id'])
            ->update(['fk_empid' => $request['recruiter']]);

            DB::table('tbl_recruiter_cv')
            ->where('r_map_id',$_POST['clientreq_id'])
            ->update(['recruiter_id' => $request['recruiter']]);

  Session::flash('success_msg', 'Hand over Successfully!');

          return redirect('handover');
    }

    public function handoverdetails()
    {
        $selectclients=("select posh_createdat,(select name from  users where id=a.posh_oldrecuiter) as oldrec ,(select name from  users where id=a.posh_newrecuiter) as newrec  ,(select b.clientjob_title from  tbl_clientjd_master as b join tbl_clientjdrecruitermap as c on c.fk_jdid=b.clientjob_id where c.clientreq_id=a.posh_posid) as posname from tbl_positionhandover as a  " ) ;
         $emp_list = DB::select(DB::raw($selectclients));
         return view('Emp.handoverdetails',compact('emp_list'));
    }
     public function offeradminemployes($id){

        try {

            $roles = Role::all();
            $users = User::find($id);
          //  dd($users);
            $departments = Department::all();
            return view('admin.offeremployee',compact('roles','departments','users'));
        
            } catch (Exception $e) {
                LOG::error('THis is admin Add  Employee display errror'.$e->getMessage());
                return $this->errorBag($e);
            }


        }
public function ajaxadminemployes(Request $request)
    {
            $key = $request['key'];
            $users  = "select * from users where emp_status=1 and emp_role!=4 and (name like '%".$key."%' or email like '%".$key."%' or emp_contactno like '%".$key."%')";
            $result = DB::select(DB::raw($users));
            $i=1;
                        foreach($result as $kk => $employees){

if($employees->emp_status==0)
{
$valdata=' <a title=" Activate Now" href="reactive/'.$employees->id.'" class="btn btn-primary">Inactive </a>';
}
else
{
$valdata=' <a title="Click For Inactive" href="deleteadminemployes/'.$employees->id.'" class="btn btn-primary">Active </a>';
}

                        echo    '<tr>
                               
                                <td>'.$employees->name.'</td>
                                <td>'.$employees->emp_contactno.'</td>
                                <td>'.$employees->email.'</td>
                                <td>'.$employees->emp_doj.'</td>
                                 <td>
                                   <a href="editadminemployes/'.$employees->id.'" class="btn btn-primary">Edit </a>
                                </td>

                                <td>'.$valdata.'</td>
                                <td>
                                   <a href="/offeradminemployes/'.$employees->id.'" class="btn btn-primary" target="_blank">Offer Letter </a>
                                </td>
                               </tr>';
                          $i++; }
                        }


}
