<?php 

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Model\Client as Client;
use App\Model\Adminmodel\Role as Role;
use App\Model\Adminmodel\Department as Department;
use App\Model\Adminmodel\Employees as Employees;

use Illuminate\Support\Facades\Auth;
use App\Model\User;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use App\Exceptions\Handler;
use Exception;
use App\Component\CommonComponent;
use Carbon\Carbon;
// use Datatables;

 
use Illuminate\Http\Request;
 
class ReportsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $redirectTo = '/admin/dashboard';
    protected $redirectAfterLogout = '/admin/login';

    public function __construct()
    {
        $this->middleware('auth');
    }
 
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

      

 public function updaterecresume(Request $request)
    {
 $loginid = Auth::user()->id;
 //Array ( [_token] => dQjQyIviG5PswpB84u8Dmeiz7lvvXXgUjKhJ6uiG [sel] => 790/6/4 [expectedsaleryval] => 00 [Doj] => 2018-08-24 [expDoj] => [count] => 790 [countval] => 1 )


 //Array ( [_token] => ddBDm6HB0UMOwj73IRUhyqOduIwHuG59AObye67L [sel] => 1253/15/3 [expectedsalery] => [Doj] => [expDoj] => [count] => 1253 [countval] => 1 [placepopupval] => 1253 [keyval] => 0 [actmode] => 3 )
//print_r($_POST ); exit;
 if(isset($request['countval']))
     {

    $candidate_recruiter_array = explode('/',$request['sel']);
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$request['count']])
                        ->update(['c.expectedsalery'=>$request['expectedsalery'],'c.dateoj'=>$request['Doj'],'c.expDoj'=>$request['expDoj']]);
    //echo "ddddddd";loginid
DB::table('tbl_recruiter_cvchangestatus')
                        ->where(['cvid'=>$request['count']])
                        ->update(['cvupdatestatus'=>'0']);


DB::table('tbl_recruiter_cvchangestatus')->insert(
    ['cvid' => $request['count'],'loginid'=>$loginid,'recuiterid'=>$loginid, 'dateoj' => $request['Doj'], 'expectedsalery' => $request['expectedsalery'], 'expDoj' => $request['expDoj'], 'cvupdatestatus' => '1', 'cv_status' =>$candidate_recruiter_array[2]]
);

}


if(isset($request['sel'])) {
     
              $candidate_recruiter_array = explode('/',$request['sel'] );
//print_r( $candidate_recruiter_array); exit;

DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$candidate_recruiter_array[0]])
                        ->update(['c.cv_status'=>$candidate_recruiter_array[2]]);



$val=$candidate_recruiter_array[0];
 $rec_email = DB::table('tbl_recruiter_cv as rc')
                       ->join('users as u', 'rc.recruiter_id', '=', 'u.id')
                        ->select('u.email', 'u.name')
                        ->where(['rc.recruiter_id' => $candidate_recruiter_array[1]])
                        ->first();
                      //  dd( $rec_email);
 
                    $complete_shortlist = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->where('rc.id', $candidate_recruiter_array[0])
                         ->where('rc.cv_status', '2')
                        // ->where(['rc.id'=>$v])
                        ->get();
                      //  dd($complete_shortlist );
                    $complete_unshortlist=0;

           //        $sendMail = $this->sendMail($complete_shortlist,$complete_unshortlist,$request['poid'],$loginid,$rec_email );



}

 $assigneeid=Auth::user()->id;
       // echo $assigneeid;
        /*$candidate=DB::table('tbl_recruiter_cv as rcv')
            ->select('rcv.*','us.name as username')
             ->join('users as us','us.id','=','rcv.recruiter_id')
            ->where(['rcv.id'=>$request['placepopupval'] ])
            ->orderBy('cv_status', 'ASC')
            ->get();


         echo $candidate;*/

        $key = $_POST['keyval'];
        $act_mode = $_POST['actmode'];
        // $pos_id = $request['posid'];

        $assigneeid=Auth::user()->id;
        // $comp =DB::select(DB::raw("Select c.client_id from tbl_clients as c inner join tbl_clientjd_master as jd on jd.clientjob_compid=c.client_id where jd.clientjob_id=$pos_id"));

        if($act_mode==1){

            $result="select rcv.*,u.name,jd.clientjob_title,jd.clientjob_id,jd.drive_time,jd.shortlist_time,jd.lineup_to,c.comp_name,c.client_id from tbl_recruiter_cv as rcv inner join users as u on u.id=rcv.recruiter_id inner join tbl_clientjd_master as jd on jd.clientjob_id=rcv.position_id inner join tbl_clients as c on c.client_id=jd.clientjob_compid where rcv.candidate_name like '%".$key."%'";

        }

        else if($act_mode==2){
            $result="select rcv.*,u.name,jd.clientjob_title,jd.clientjob_id,jd.drive_time,jd.shortlist_time,jd.lineup_to,c.comp_name,c.client_id from tbl_recruiter_cv as rcv inner join users as u on u.id=rcv.recruiter_id inner join tbl_clientjd_master as jd on jd.clientjob_id=rcv.position_id inner join tbl_clients as c on c.client_id=jd.clientjob_compid where rcv.candidate_email like '%".$key."%'";

        }
        else{
            $result="select rcv.*,u.name,jd.clientjob_title,jd.clientjob_id,jd.drive_time,jd.shortlist_time,jd.lineup_to,c.comp_name,c.client_id from tbl_recruiter_cv as rcv inner join users as u on u.id=rcv.recruiter_id inner join tbl_clientjd_master as jd on jd.clientjob_id=rcv.position_id inner join tbl_clients as c on c.client_id=jd.clientjob_compid where rcv.candidate_mob like '%".$key."%'";

        }
 
        
        $pos = DB::select(DB::raw($result));



     echo "<pre>"; 
       // print_r($pos);

                echo '<tr>
                        <th>Candidate Name</th>
                        <th>Candidate Mobile</th>
                        <th>Company Name</th>
                        <th>Position Name</th>
                        <th>Recruiter Name</th>
                        <th>Action</th>
                         <th>Action</th>
                    </tr>';

      
                foreach ($pos as $key => $value) {
                    if(($pos[$key]->cv_status!=4)){


$str = $value->created_at;
              $str = explode(" ", $str);
              $aa =explode("-", $str[0]);
              // print_r($aa); 
              

              $url='/storage/app/uploadcv/'.$aa[0]."/".$aa[1]."/".$aa[2]."/".$value->cv_name;
              // print_r($url);


                  $string  = "'".$value->candidate_name."'";
                  echo  '<tr class="table-active">
                        
                        <td>'.$value->candidate_name.'</td>
                        <td>'.$value->candidate_mob.' </td>
                        <td>'.$value->comp_name.'</td>
                        <td>'.$value->clientjob_title.'</td>
                        <td>'.$value->name.'</td>
                        <td><a href="'.$url.'">Download</a>
&nbsp; <button type="button"  class="det_css one-click" data-toggle="modal" data-target="#myModal" onclick="getposiionvalue('.$value->id.'),getupdatespock('.$value->id.','.$value->recruiter_id.','.$value->cv_status.','.$value->position_id.')(this.value),getpositionspock('.$value->id.','.$value->recruiter_id.','.$value->cv_status.')" ><span class="icofont icofont-eye-alt"></span></button>
<Input type="hidden" name="keyval" value='.$key.' id="keyval">
<Input type="hidden" name="actmode" value='.$act_mode.' id="actmode">
                        </td>
                        
                        </tr>
                        ';
                          }

                      }
                    

            
} 

 public function updatespockresume(Request $request)
    {
 $loginid = Auth::user()->id;
 //Array ( [_token] => dQjQyIviG5PswpB84u8Dmeiz7lvvXXgUjKhJ6uiG [sel] => 790/6/4 [expectedsaleryval] => 00 [Doj] => 2018-08-24 [expDoj] => [count] => 790 [countval] => 1 )

 if(isset($request['countval']))
     {

    $candidate_recruiter_array = explode('/',$request['sel']);
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$request['count']])
                        ->update(['c.expectedsalery'=>$request['expectedsalery'],'c.dateoj'=>$request['Doj'],'c.expDoj'=>$request['expDoj']]);
    //echo "ddddddd";loginid
DB::table('tbl_recruiter_cvchangestatus')
                        ->where(['cvid'=>$request['count']])
                        ->update(['cvupdatestatus'=>'0']);


DB::table('tbl_recruiter_cvchangestatus')->insert(
    ['cvid' => $request['count'],'loginid'=>$loginid,'recuiterid'=>$loginid, 'dateoj' => $request['Doj'], 'expectedsalery' => $request['expectedsalery'], 'expDoj' => $request['expDoj'], 'cvupdatestatus' => '1', 'cv_status' =>$candidate_recruiter_array[2]]
);

}


if(isset($request['sel'])) {
     
              $candidate_recruiter_array = explode('/',$request['sel'] );
//print_r( $candidate_recruiter_array); exit;

DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$candidate_recruiter_array[0]])
                        ->update(['c.cv_status'=>$candidate_recruiter_array[2]]);



$val=$candidate_recruiter_array[0];
 $rec_email = DB::table('tbl_recruiter_cv as rc')
                       ->join('users as u', 'rc.recruiter_id', '=', 'u.id')
                        ->select('u.email', 'u.name')
                        ->where(['rc.recruiter_id' => $candidate_recruiter_array[1]])
                        ->first();
                      //  dd( $rec_email);
 
                    $complete_shortlist = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->where('rc.id', $candidate_recruiter_array[0])
                         ->where('rc.cv_status', '2')
                        // ->where(['rc.id'=>$v])
                        ->get();
                      //  dd($complete_shortlist );
                    $complete_unshortlist=0;

           //        $sendMail = $this->sendMail($complete_shortlist,$complete_unshortlist,$request['poid'],$loginid,$rec_email );



}

 $assigneeid=Auth::user()->id;
       // echo $assigneeid;
        $candidate=DB::table('tbl_recruiter_cv as rcv')
            ->select('rcv.*','us.name as username')
             ->join('users as us','us.id','=','rcv.recruiter_id')
            ->where(['rcv.position_id'=>$request['placepopupval'] ])
            ->orderBy('cv_status', 'ASC')
            ->get();


         echo $candidate;


}

 public function updateclientstatusrecuiter(Request $request)
    {
 $loginid = Auth::user()->id;

// print_r($request['recruiter'][0]); exit;
//print_r($request['countval']); exit;
    
     if(isset($request['countval']))
     {
for($i=0;$i<count($request['countval']);$i++)
{
    $candidate_recruiter_array = explode('/',$request['recruiter'][$i]);
//print_r($candidate_recruiter_array[2]); exit;

//print_r($request['count'][$i]); exit;
   // echo $request['expectedsalery'][$i].'--'.$request['Doj'][$i].'-----'.$request['expDoj'][$i];
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$request['count'][$i]])
                        ->update(['c.expectedsalery'=>$request['expectedsalery'][$i],'c.dateoj'=>$request['Doj'][$i],'c.expDoj'=>$request['expDoj'][$i]]);
    //echo "ddddddd";loginid
DB::table('tbl_recruiter_cvchangestatus')
                        ->where(['cvid'=>$request['count'][$i]])
                        ->update(['cvupdatestatus'=>'0']);


DB::table('tbl_recruiter_cvchangestatus')->insert(
    ['cvid' => $request['count'][$i],'loginid'=>$loginid,'recuiterid'=>$loginid, 'dateoj' => $request['Doj'][$i], 'expectedsalery' => $request['expectedsalery'][$i], 'expDoj' => $request['expDoj'][$i], 'cvupdatestatus' => '1', 'cv_status' =>$candidate_recruiter_array[2]]
);

}}


if(isset($request['recruiter'])) {
      foreach($request['recruiter'] as $kkk=>$vvv)
            {
              $candidate_recruiter_array = explode('/',$vvv);


DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$candidate_recruiter_array[0]])
                        ->update(['c.cv_status'=>$candidate_recruiter_array[2]]);



$val=$candidate_recruiter_array[0];
 $rec_email = DB::table('tbl_recruiter_cv as rc')
                       ->join('users as u', 'rc.recruiter_id', '=', 'u.id')
                        ->select('u.email', 'u.name')
                        ->where(['rc.recruiter_id' => $candidate_recruiter_array[1]])
                        ->first();
                      //  dd( $rec_email);
 
                    $complete_shortlist = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->where('rc.id', $candidate_recruiter_array[0])
                         ->where('rc.cv_status', '2')
                        // ->where(['rc.id'=>$v])
                        ->get();
                      //  dd($complete_shortlist );
                    $complete_unshortlist=0;

           //        $sendMail = $this->sendMail($complete_shortlist,$complete_unshortlist,$request['poid'],$loginid,$rec_email );



}}

     session::flash('success','Message Sent');
                return redirect('get-assigned-postion/'.$request['clientreqEncrypt']);
    }


 public function updateclientstatuspopupresume(Request $request)
    {
  //   print_r($_POST); 
$loginid = Auth::user()->id;

     if(isset($request['canddata']))
     {
for($i=0;$i<count($request['canddata']);$i++)
{ $candidate_recruiter_array = explode('/',$request['checkbox_data']);

   // echo $request['expectedsalery'][$i].'--'.$request['Doj'][$i].'-----'.$request['expDoj'][$i];
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$request['canddata'][$i]])
                        ->update(['c.expectedsalery'=>$request['expectedsalery'],'c.dateoj'=>$request['Doj'],'c.expDoj'=>$request['expDoj']]);
    //echo "ddddddd";
DB::table('tbl_recruiter_cvchangestatus')
                        ->where(['cvid'=>$request['canddata'][$i]])
                        ->update(['cvupdatestatus'=>'0']);


DB::table('tbl_recruiter_cvchangestatus')->insert(
    ['cvid' => $request['canddata'][$i],'loginid'=>$loginid,'recuiterid'=>$loginid, 'dateoj' => $request['Doj'], 'expectedsalery' => $request['expectedsalery'], 'expDoj' => $request['expDoj'], 'cvupdatestatus' => '1', 'cv_status' =>$candidate_recruiter_array[2]]
);

}}



if(isset($request['canddata'])) {
      foreach($request['canddata'] as $kkk=>$vvv)
            {
              $candidate_recruiter_array = explode('/',$request['checkbox_data']);
//echo $vvv;
//print_r( $candidate_recruiter_array );
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$vvv])
                        ->update(['c.cv_status'=>$candidate_recruiter_array[2]]);



$val=$vvv;
 $rec_email = DB::table('tbl_recruiter_cv as rc')
                       ->join('users as u', 'rc.recruiter_id', '=', 'u.id')
                        ->select('u.email', 'u.name')
                        ->where(['rc.recruiter_id' => $candidate_recruiter_array[1]])
                        ->first();
                      //  dd( $rec_email);
 
                    $complete_shortlist = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->where('rc.id', $vvv)
                         ->where('rc.cv_status', '2')
                        // ->where(['rc.id'=>$v])
                        ->get();
                      //  dd($complete_shortlist );
                    $complete_unshortlist=0;

           //        $sendMail = $this->sendMail($complete_shortlist,$complete_unshortlist,$request['poid'],$loginid,$rec_email );



}}


     session::flash('success','Message Sent');
                return redirect('shortlistcv?posid='.$request['clientreq_posid']);
    }


 public function updateclientstatuspopup(Request $request)
    {
 $loginid = Auth::user()->id;


     if(isset($request['countval']))
     {
for($i=0;$i<count($request['countval']);$i++)
{ $candidate_recruiter_array = explode('/',$request['recruiter'][$i]);
   // echo $request['expectedsalery'][$i].'--'.$request['Doj'][$i].'-----'.$request['expDoj'][$i];
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$request['count'][$i]])
                        ->update(['c.expectedsalery'=>$request['expectedsalery'][$i],'c.dateoj'=>$request['Doj'][$i],'c.expDoj'=>$request['expDoj'][$i]]);
    //echo "ddddddd";
DB::table('tbl_recruiter_cvchangestatus')
                        ->where(['cvid'=>$request['count'][$i]])
                        ->update(['cvupdatestatus'=>'0']);


DB::table('tbl_recruiter_cvchangestatus')->insert(
    ['cvid' => $request['count'][$i],'loginid'=>$loginid,'recuiterid'=>$loginid, 'dateoj' => $request['Doj'][$i], 'expectedsalery' => $request['expectedsalery'][$i], 'expDoj' => $request['expDoj'][$i], 'cvupdatestatus' => '1', 'cv_status' =>$candidate_recruiter_array[2]]
);

}}


if(isset($request['recruiter'])) {
      foreach($request['recruiter'] as $kkk=>$vvv)
            {
              $candidate_recruiter_array = explode('/',$vvv);


DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$candidate_recruiter_array[0]])
                        ->update(['c.cv_status'=>$candidate_recruiter_array[2]]);



$val=$candidate_recruiter_array[0];
 $rec_email = DB::table('tbl_recruiter_cv as rc')
                       ->join('users as u', 'rc.recruiter_id', '=', 'u.id')
                        ->select('u.email', 'u.name')
                        ->where(['rc.recruiter_id' => $candidate_recruiter_array[1]])
                        ->first();
                      //  dd( $rec_email);
 
                    $complete_shortlist = DB::table('tbl_recruiter_cv as rc')
                        ->select('rc.candidate_name')
                        ->where('rc.id', $candidate_recruiter_array[0])
                         ->where('rc.cv_status', '2')
                        // ->where(['rc.id'=>$v])
                        ->get();
                      //  dd($complete_shortlist );
                    $complete_unshortlist=0;

           //        $sendMail = $this->sendMail($complete_shortlist,$complete_unshortlist,$request['poid'],$loginid,$rec_email );



}}

     session::flash('success','Message Sent');
                return redirect('shortlistcv?posid='.$request['clientreq_posid']);
    }

    public function index(Request $request)
    {
        //dd(session(['adminsess'=>'abcd']));


        return view('admin.adashboard');
    }

     public function Sevenoclockclientreports()
    {

$data=[];


$users= DB::select("select * from users where emp_role like '%3%'");

  foreach($users as  $key => $value) {
    
$data[$key]["username"]=$value->name;
$data[$key]["empcode"]=$value->name;


$users1= DB::select("SELECT count(rv.id) as trtr from tbl_recruiter_cv as rv inner join tbl_clientjd_master 
as cm on cm.clientjob_id=rv.position_id where 
rv.recruiter_id=".$value->id." and Date(rv.created_at)=CURDATE() group by rv.position_id")

;

$data[$key]["userreport"]=$users1;

$users2= DB::select("SELECT count(rv.id) as sendclient, cm.clientjob_title as position  from tbl_recruiter_cv as rv inner join tbl_clientjd_master 
as cm on cm.clientjob_id=rv.position_id where 
rv.recruiter_id=".$value->id." and Date(rv.created_at)=CURDATE() and rv.cv_status=2  group by rv.position_id");

$data[$key]["userreport2"]=$users2;

$users3= DB::select("SELECT  cm.clientjob_title from tbl_recruiter_cv as rv inner join tbl_clientjd_master 
as cm on cm.clientjob_id=rv.position_id where 
rv.recruiter_id=".$value->id." and Date(rv.created_at)=CURDATE()  group by rv.position_id");

$data[$key]["position"]=$users3;

$users4= DB::select("SELECT  tblclient.comp_name from tbl_recruiter_cv as rv inner join tbl_clientjd_master 
as cm on cm.clientjob_id=rv.position_id  join tbl_clients as tblclient on tblclient.client_id=cm.clientjob_compid  where 
rv.recruiter_id=".$value->id." and Date(rv.created_at)=CURDATE()   group by rv.position_id");

$data[$key]["client"]=$users4;
    
}
//dd($data);

       return view('admin.Sevenoclockclientreports',compact('data'));


        //  return view('admin.adminselectrole');
    }

public function getspockposition($id)
{
  echo $id;
}

  public function getspockstatusallcv($nid,$rid,$cid,$posid)
        {
       //   echo $posid; exit;
            $html = ''; 
            if($cid=='1')
            {
              $sel1='selected';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel7='';$sel8='';$sel9='';$sel10='';
            }
             if($cid=='2')
            {
              $sel1='';$sel2='selected';$sel3='';$sel4='';$sel5='';$sel6='';$sel7='';$sel8='';$sel9='';$sel10='';
            }
            if($cid=='3')
            {
              $sel1='';$sel2='';$sel3='selected';$sel4='';$sel5='';$sel6='';$sel7='';$sel8='';$sel9='';$sel10='';
            }
            if($cid=='4')
            {
              $sel1='';$sel2='';$sel3='';$sel4='selected';$sel5='';$sel6='';$sel7='';$sel8='';$sel9='';$sel10='';
            }
            if($cid=='5')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='selected';$sel6='';$sel7='';$sel8='';$sel9='';$sel10='';
            }
            if($cid=='6')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='selected';$sel7='';$sel8='';$sel9='';$sel10='';
            }
             if($cid=='7')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel7='selected';$sel8='';$sel9='';$sel10='';
            }
             if($cid=='8')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel7='';$sel8='selected';$sel9='';$sel10='';
            }
           
             if($cid=='9')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel7='';$sel8='';$sel9='selected';$sel10='';
            }
             if($cid=='10')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';$sel7='';$sel8='';$sel9='';$sel10='selected';
            }
              $html .= '<option value="'.$id.'/'.$rid.'/1" '.$sel1.'>Not Seen</option>
              <option value="'.$id.'/'.$rid.'/2" '.$sel2.'>Send To Client</option>
              <option value="'.$id.'/'.$rid.'/3" '.$sel3.'>Rejected</option>
              <option value="'.$id.'/'.$rid.'/4" '.$sel4.'>Offer</option>
              <option value="'.$id.'/'.$rid.'/5" '.$sel5.'>Pipeline</option>
              <option value="'.$id.'/'.$rid.'/6" '.$sel6.'>Join</option>
              <option value="'.$id.'/'.$rid.'/7" '.$sel7.'>On The Way</option>
              <option value="'.$id.'/'.$rid.'/8" '.$sel8.'>Interviewed</option>
              <option value="'.$id.'/'.$rid.'/9" '.$sel9.'>Not Going</option>
                <option value="'.$id.'/'.$rid.'/10" '.$sel10.'>Declined</option>


              '



              ;

//echo $id.'----'.$rid.'---'.$cid;
echo $html;
}


    public function getspockstatus($id,$rid,$cid,$posid)
        {
            $html = ''; 
            if($cid=='1')
            {
              $sel1='selected';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';
            }
             if($cid=='2')
            {
              $sel1='';$sel2='selected';$sel3='';$sel4='';$sel5='';$sel6='';
            }
            if($cid=='3')
            {
              $sel1='';$sel2='';$sel3='selected';$sel4='';$sel5='';$sel6='';
            }
            if($cid=='4')
            {
              $sel1='';$sel2='';$sel3='';$sel4='selected';$sel5='';$sel6='';
            }
            if($cid=='5')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='selected';$sel6='';
            }
            if($cid=='6')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='selected';
            }
              $html .= '<option value="'.$id.'/'.$rid.'/1" '.$sel1.'>Not Seen</option>
              <option value="'.$id.'/'.$rid.'/2" '.$sel2.'>Send To Client</option>
              <option value="'.$id.'/'.$rid.'/3" '.$sel3.'>Rejected</option>
              <option value="'.$id.'/'.$rid.'/4" '.$sel4.'>Offer</option>
              <option value="'.$id.'/'.$rid.'/5" '.$sel5.'>Pipeline</option>
              <option value="'.$id.'/'.$rid.'/6" '.$sel6.'>Join</option>';
//echo $id.'----'.$rid.'---'.$cid;
echo $html;
}


public function Spocofferreports()
    {

 $assigneeid=Auth::user()->id;
  $usersval= DB::select("select * from users where  id=".$assigneeid.""); 
if($usersval[0]->emp_role!='4')
{
if(isset($_GET['user']) && $_GET['user']!='' )
{
$users= DB::select("select * from users where  emp_role like '%2%' and name='".$_GET['user']."'  and id=".$assigneeid."");
}
else
{
  $users= DB::select("select * from users where  emp_role like '%2%'  and id=".$assigneeid."");
}


}
else
{
if(isset($_GET['user']) && $_GET['user']!='' )
{
$users= DB::select("select * from users where  emp_role like '%2%' and name='".$_GET['user']."'");
}
else
{
  $users= DB::select("select * from users where  emp_role like '%2%'");
}
}
   
$data=[];
if(isset($_GET['doj']) and $_GET['doj']!='')
{
$start = $_GET['doj'];
$finish = (date('D') != 'Sat') ? date('Y-m-d', strtotime('next Saturday')) : date('Y-m-d');

}
if(isset($_GET['doend']) and $_GET['doend']!='')
{
    $start = $_GET['doj'];
     $finish = $_GET['doend'];
}
else
{
  
    $start = (date('D') != 'Mon') ? date('Y-m-d', strtotime('last Monday')) : date('Y-m-d');
    $finish = date('Y-m-l');
}


  foreach($users as  $key => $value) {



$data[$key]["username"]=$value->name;
$data[$key]["empcode"]=$value->id;

$users1= DB::select("select * from tbl_recruiter_cv where  spockid=".$value->id." and cv_status='4' and (dateoj between '".$start ."' and '".$finish ."') order by id desc ");
$data[$key]["resumes"]=$users1;

$users2= DB::select("
select b.r_map_id,a.clientjob_id, b.position_id, b.dateoj,a.clientjob_title from tbl_clientjdrecruitermap as c join tbl_clientjd_master as a  on a.clientjob_id=c.fk_jdid  join tbl_recruiter_cv as b on b.r_map_id=c.clientreq_id where b.spockid=".$value->id." and b.cv_status='4' and (b.dateoj between '".$start ."' and '".$finish ."') order by id desc ");
$data[$key]["positions"]=$users2;

$users3= DB::select("select a.comp_name  from  tbl_clients as a join tbl_recruiter_cv as b on a.client_id=b.clientsid where  b.spockid=".$value->id." and  b.cv_status='4'  and (b.dateoj between '".$start ."' and '".$finish ."') order by id desc ");
$data[$key]["clients"]=$users3;

}
//dd($data);

  return view('admin.spocofferreports',compact('data'));

    }





  public function recuiterofferreports()
    {

 $assigneeid=Auth::user()->id;
  $usersval= DB::select("select * from users where  id=".$assigneeid.""); 
if($usersval[0]->emp_role!='4')
{
if(isset($_GET['user']) && $_GET['user']!='' )
{
$users= DB::select("select * from users where  emp_role like '%3%' and name='".$_GET['user']."'  and id=".$assigneeid."");
}
else
{
  $users= DB::select("select * from users where  emp_role like '%3%'  and id=".$assigneeid."");
}


}
else
{
if(isset($_GET['user']) && $_GET['user']!='' )
{
$users= DB::select("select * from users where  emp_role like '%3%' and name='".$_GET['user']."'");
}
else
{
  $users= DB::select("select * from users where  emp_role like '%3%'");
}
}
   
$data=[];
if(isset($_GET['doj']) and $_GET['doj']!='')
{
$start = $_GET['doj'];
$finish = (date('D') != 'Sat') ? date('Y-m-d', strtotime('next Saturday')) : date('Y-m-d');

}
if(isset($_GET['doend']) and $_GET['doend']!='')
{
    $start = $_GET['doj'];
     $finish = $_GET['doend'];
}
else
{
  
    $start = (date('D') != 'Mon') ? date('Y-m-d', strtotime('last Monday')) : date('Y-m-d');
    $finish = date('Y-m-l');
}


  foreach($users as  $key => $value) {



$data[$key]["username"]=$value->name;
$data[$key]["empcode"]=$value->id;

$users1= DB::select("select * from tbl_recruiter_cv where  recruiter_id=".$value->id." and cv_status='4' and (dateoj between '".$start ."' and '".$finish ."') order by id desc ");
$data[$key]["resumes"]=$users1;

$users2= DB::select("
select b.r_map_id,a.clientjob_id, b.position_id, b.dateoj,a.clientjob_title from tbl_clientjdrecruitermap as c join tbl_clientjd_master as a  on a.clientjob_id=c.fk_jdid  join tbl_recruiter_cv as b on b.r_map_id=c.clientreq_id where b.recruiter_id=".$value->id." and b.cv_status='4' and (b.dateoj between '".$start ."' and '".$finish ."') order by id desc ");
$data[$key]["positions"]=$users2;

$users3= DB::select("select a.comp_name  from  tbl_clients as a join tbl_recruiter_cv as b on a.client_id=b.clientsid where  b.recruiter_id=".$value->id." and  b.cv_status='4'  and (b.dateoj between '".$start ."' and '".$finish ."') order by id desc ");
$data[$key]["clients"]=$users3;

}
//dd($data);

  return view('admin.recuiterofferreports',compact('data'));

    }



public function getresumesrecuiterofferreport($id)
    {
         $assigneeid=Auth::user()->id;

$users3= DB::select("select dateoj,date(created_at) as cdate from tbl_recruiter_cvchangestatus  where cvid=".$id." order by rcvid desc limit 0,1");
//$data[$key]["clients"]=$users3;
//Our "then" date.
$then = $users3[0]->dateoj;
 //$then = "2018-09-03";
//Convert it into a timestamp.
$then = strtotime($then);
 
//Get the current timestamp.
 $now = strtotime($users3[0]->cdate); 
 
//Calculate the difference.
$difference = $then - $now;
 
//Convert seconds into days.
$days = floor($difference / (60*60*24) );
 if(($days/7)>0)
 {
$noofdays= floor($days/7);
$users9= DB::select("select * from tbl_recruiter_cvupdateddata  where rc_resumeid=".$id." order by followup_status asc ");
$exp="";
//print_r($users9); exit;
foreach($users9 as $us9)
{
  //$exp +=implode(',',$us9->followup_status);
  $exp =$exp.$us9->followup_status.',';
 //  $exp = implode(",",$us9->followup_status);
//print_r($us9->followup_status);
}
#echo $exp; exit;
if(empty($exp))
{
  $explval=array();
}
else
{
  $explval=explode(",", trim($exp,','));
}
//$a=array(trim($exp,','));
//$a=array(0,1);
#print_r($explval); exit;
$html="";
/*if(count($users9)>0) {
foreach($users9 as $us9)
{*/
  $checkval=0;
for ($x = 0; $x < $noofdays; $x++) {

  //if($us9->followup_status==$x)
  if(in_array($x, $explval))
  {
$html .= '<td>followup '.($x+1).'
  &nbsp Completed</td>';
  
  }
  else
  {
    if(empty($checkval))
    {
      $checkval=$x+1;
    }
    else
    {
      $checkval=$checkval;
    }
    #echo $checkval;exit;
    
 if($checkval-1==$x)
 {
  $html .= '<td>followup '.($x+1).'<input type="radio" name="followupcheckbox" value="'.$x.'">
  &nbsp<input type="hidden" value="'.$noofdays.'" name="countval"><input type="hidden" value="'.$id.'" name="cvid"></td>';  
 }
 else
 {
  $html .= '<td>followup '.($x+1).'<input type="radio" name="followupcheckbox" value="'.$x.'" disabled>
  &nbsp<input type="hidden" value="'.$noofdays.'" name="countval"><input type="hidden" value="'.$id.'" name="cvid"></td>';
  }  
  }
 }
/*}
}
else
{$html .= '<td>followup '.($x+1).'<input type="radio" name="followupcheckbox" value="'.$x.'">
  &nbsp<input type="hidden" value="'.$noofdays.'" name="countval"><input type="hidden" value="'.$id.'" name="cvid"></td>';


}
*/
}

/*  $role = DB::table('tbl_recruiter_cvupdateddata')
                   ->select('*')
                   ->where('rc_resumeid','=',$id)
                   ->get();
*/
 /*$html="";
  foreach($role as $key => $val){
$html .= '  <tr><td>'.$val->rc_updatededdata.'</td><td>'.$val->created_at.'</td></tr>';

  }
echo $html;*/
     echo $html;
}

public function updaterecfollowup(Request $request)
    {
        $assigneeid=Auth::user()->id;

   //   print_r($_POST);
      for ($x = 0; $x < $_POST['countval']; $x++) {
       if($_POST['followupcheckbox']==$x)
       {
       DB::table('tbl_recruiter_cvupdateddata')->insert(
    ['rc_resumeid' => $request['cvid'],'loginid'=>$assigneeid,'recuiterid'=>$assigneeid, 'followup_status' =>$x]
);
       }
}

   return redirect('recuiterofferreports');
/*DB::table('tbl_recruiter_cvupdateddata')->insert(
    ['rc_resumeid' => $request['cvid'],'loginid'=>$assigneeid,'recuiterid'=>$assigneeid, 'followup_status' =>$request['status']]
);
*/


    }

public function updatecrecuiterdata(Request $request)
    {

  //    print_r($_POST);
         $assigneeid=Auth::user()->id;
DB::table('tbl_recruiter_cv as c')
                        ->where(['c.id'=>$request['clientreq_posid']])
                        ->update(['c.dateoj'=>$request['doj'],'c.cv_status'=>$request['status']]);

DB::table('tbl_recruiter_cvchangestatus')
                        ->where(['cvid'=>$request['clientreq_posid']])
                        ->update(['cvupdatestatus'=>'0']);



DB::table('tbl_recruiter_cvchangestatus')->insert(
    ['cvid' => $request['clientreq_posid'],'loginid'=>$assigneeid,'recuiterid'=>$assigneeid, 'dateoj' => $request['doj'], 'cvupdatestatus' => '1', 'cv_status' =>$request['status']]
);


     /* DB::table('tbl_recruiter_cvupdateddata')->insert(
    ['rc_usid' => $assigneeid, 'rc_resumeid' =>  $request->clientreq_posid, 'rc_updatededdata' =>  $request->updatetext]
);*/

   return redirect('recuiterofferreports');
    }
     public function joiningpipelinereports()
    {
          $usersval= DB::select("select * from users where emp_role like '%2%'");     
if(isset($_GET['user']) && $_GET['user']!='' ) {
 $users= DB::select("select * from users where  emp_role like '%2%' and name='".$_GET['user']."'");
}
else
{
   $users= DB::select("select * from users where  emp_role like '%2%'");
}


          
   
$data=[];




  foreach($users as  $key => $value) {



$data[$key]["username"]=$value->name;
$data[$key]["empcode"]=$value->id;

$queryval=" where ( p.clientjob_empid = ".$value->id." or par.fk_jdid =".$value->id.")";

if(isset($_GET['clients']) && $_GET['clients']!='' ) {
$queryval .=" and cli.comp_name='".$_GET['clients']."' ";

}
if(isset($_GET['doj']) && $_GET['doj']!='' ) {
    $from=$_GET['doj'];
         $to=$_GET['doend'];
$queryval .="and   ( Date(c.created_at) BETWEEN '".$from."' AND '".$to."'  )  ";

}
  else
     {
        $from = date('Y-m-01',strtotime(date('Y-m-d')));
  $to =  date('Y-m-t',strtotime(date('Y-m-d')));
$queryval .="and   ( Date(c.created_at) BETWEEN '".$from."' AND '".$to."'  )  ";

  }

$users4= DB::select("select DISTINCT(cli.comp_name), c.position_id, p.clientjob_title, p.clientjob_noofposition,cli.client_id, us.name,(select (sptarg.spoc_offeredtarget) from tbl_spoctarget as sptarg where sptarg.spoc_spid=".$value->id." and  sptarg.spoc_clientid=cli.client_id  and   ( Date(sptarg.created_at) BETWEEN '".$from."' AND '".$to."'  )) as Offeredtarget,
  (select (sptarg.spoc_joinedtarget) from tbl_spoctarget as sptarg where sptarg.spoc_spid=".$value->id." and  sptarg.spoc_clientid=cli.client_id  and   ( Date(sptarg.created_at) BETWEEN '".$from."' AND '".$to."'  )) as joinedtarget
,

(select  COUNT(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id  join tbl_clientjdrecruitermap as rjdmap on rjdmap.clientreq_id=rcv.r_map_id
join tbl_clientjd_master as jdmast on rjdmap.fk_jdid=jdmast.clientjob_id where r.cv_status=4 and rcv.recruiter_id=rjdmap.fk_empid and rjdmap.fk_assigneeid=".$value->id."  and   ( Date(r.created_at) BETWEEN '".$from."' AND '".$to."'  )) as Offered,
(select  COUNT(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id  join tbl_clientjdrecruitermap as rjdmap on rjdmap.clientreq_id=rcv.r_map_id
join tbl_clientjd_master as jdmast on rjdmap.fk_jdid=jdmast.clientjob_id where r.cv_status=5 and rcv.recruiter_id=rjdmap.fk_empid and rjdmap.fk_assigneeid=".$value->id." and   ( Date(r.created_at) BETWEEN '".$from."' AND '".$to."'  )) as piplined,
(select  COUNT(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id join tbl_clientjdrecruitermap as rjdmap on rjdmap.clientreq_id=rcv.r_map_id
join tbl_clientjd_master as jdmast on rjdmap.fk_jdid=jdmast.clientjob_id where rcv.cv_status=6 and rcv.recruiter_id=rjdmap.fk_empid and rjdmap.fk_assigneeid=".$value->id." and   ( Date(r.created_at) BETWEEN '".$from."' AND '".$to."'  )) as joined
 from tbl_recruiter_cv as c
 inner join tbl_clientjdrecruitermap as par on  par.clientreq_id=c.r_map_id inner join tbl_clientjd_master as p on   par.fk_jdid=p.clientjob_id inner join users as us on us.id = p.clientjob_empid inner join tbl_clients as cli on cli.client_id = p.clientjob_compid  ".$queryval."  group by cli.client_id");

$data[$key]["client"]=$users4;
 }
//dd($data);
       return view('admin.joiningpipelinereports',compact('data'));


        //  return view('admin.adminselectrole');
    }


      public function recuiterjoiningpipelinereports()
    {
        
    $usersval= DB::select("select * from users where emp_role like '%3%'");      
        
        if(isset($_GET['user']))
     {

  if(isset($_GET['doj']))
     {
         $from=$_GET['doj'];
         $to=$_GET['doend'];
     }
    
     else
     {
         $from = date('Y-m-01',strtotime(date('Y-m-d')));
  $to =  date('Y-m-t',strtotime(date('Y-m-d')));
     }
     
      if($_GET['user']!='') {
    

     $users= DB::select("select * ,(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=4 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as Offered ,
 (select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=5 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as piplined,
(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=6 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as joined 
      ,(select rec_offeredtarget from tbl_rectarget as rtar where rtar.rec_rid =us.id  and   ( Date(rtar.created_at) BETWEEN '".$from."' AND '".$to."')  ) as Offeredtarget 

 ,(select rec_joinedtarget from tbl_rectarget as rtar where rtar.rec_rid =us.id and   ( Date(rtar.created_at) BETWEEN '".$from."' AND '".$to."' ) ) as joinedtarget 

 from users as us where us.emp_role like '%3%' and   us.name='".$_GET['user']."'");



     }
     else
     { 
        $users= DB::select("select *,
(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=4 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as Offered ,
 (select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=5 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as piplined,
(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=6 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as joined
        ,(select rec_offeredtarget from tbl_rectarget as rtar where rtar.rec_rid =us.id  and   ( Date(rtar.created_at) BETWEEN '".$from."' AND '".$to."')  ) as Offeredtarget 

 ,(select rec_joinedtarget from tbl_rectarget as rtar where rtar.rec_rid =us.id and   ( Date(rtar.created_at) BETWEEN '".$from."' AND '".$to."' ) ) as joinedtarget 


          from users as us where us.emp_role like '%3%'");
     }
     
     
     }
     
 else
 {
      if(isset($_GET['doj']))
     {
         $from=$_GET['doj'];
         $to=$_GET['doend'];
     }
    
     else
     {
         $from = date('Y-m-01',strtotime(date('Y-m-d')));
  $to =  date('Y-m-t',strtotime(date('Y-m-d')));
     }

  $users= DB::select("select *

 ,(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=4 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as Offered ,
 (select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=5 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as piplined,
(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=6 and rcv.recruiter_id =us.id  and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as joined ,
 (select rec_offeredtarget from tbl_rectarget as rtar where rtar.rec_rid =us.id  and   ( Date(rtar.created_at) BETWEEN '".$from."' AND '".$to."')  ) as Offeredtarget 
 ,(select rec_joinedtarget from tbl_rectarget as rtar where rtar.rec_rid =us.id and   ( Date(rtar.created_at) BETWEEN '".$from."' AND '".$to."' ) ) as joinedtarget from users as us where us.emp_role like '%3%'");
 }
  
     
  // dd(  $users);

       return view('admin.recuiterjoiningpipelinereports',compact('users','from','to'));


        //  return view('admin.adminselectrole');
    }

public function lunchbreakreports()
    {

  if(isset($_GET['doj']))
     {
         $from=$_GET['doj'];
         $to=$_GET['doend'];
     }
    
     else
     {
        $from=date('Y-m-d');
        $to=date('Y-m-d');
     }


if(isset($_GET['user'])) {
    
if($_GET['user']!='') {
 $data = DB::table('tbl_clientrec_break_backtime as tcbr')
                                ->join('users as u','tcbr.clientstart_recuiterid','=','u.id')
                                ->select('tcbr.*','u.name')
                                  ->where('tcbr.created_at','>=',$from)
                                   ->where('tcbr.created_at','<=',$to)
                                      ->where('u.name','=',$_GET['user'])
                            
                                ->get();
                            }
                            else
                            {
                                $data = DB::table('tbl_clientrec_break_backtime as tcbr')
                                ->join('users as u','tcbr.clientstart_recuiterid','=','u.id')
                                ->select('tcbr.*','u.name')
                                  ->where('tcbr.created_at','>=',$from)
                                   ->where('tcbr.created_at','<=',$to)
                           
                                ->get();
                            }
     }
     else
     {
 
 $data = DB::table('tbl_clientrec_break_backtime as tcbr')
                                ->join('users as u','tcbr.clientstart_recuiterid','=','u.id')
                                ->select('tcbr.*','u.name')
                                  ->where('tcbr.created_at','>=',$from)
                                 //  ->where('tcbr.created_at','=',$to)
                                   ->get();

      }
     
 return view('admin.lunchbreakreports',compact('data'));

}

public function recuiterpositionreports()
    {

      $id = Auth::user()->id;
 /* $getPositions = DB::table('tbl_clientjdrecruitermap as tcr')
      ->join('tbl_clientjd_master as tcm', 'tcr.fk_jdid', '=', 'tcm.clientjob_id')
      ->select(DB::raw('distinct(tcm.clientjob_title) as clti') ,'tcr.fk_empid'
        ,DB::raw('(select count(id) from tbl_recruiter_cv as rcv where rcv.cv_status=5 and position_id=tcm.clientjob_id  and rcv.recruiter_id='.$id.') as Pipeline') 
,DB::raw('(select count(id) from tbl_recruiter_cv as rcv where rcv.cv_status=5 and position_id=tcm.clientjob_id and rcv.recruiter_id='.$id.') as Joining') 
,DB::raw('(select count(id) from tbl_recruiter_cv as rcv where rcv.cv_status=4 and position_id=tcm.clientjob_id and rcv.recruiter_id='.$id.') as Offer') 
,DB::raw('(select count(id) from tbl_recruiter_cv as rcv where rcv.cv_status=1 and position_id=tcm.clientjob_id and rcv.recruiter_id='.$id.') as notseen') 
,DB::raw('(select count(id) from tbl_recruiter_cv as rcv where rcv.cv_status=2 and position_id=tcm.clientjob_id and rcv.recruiter_id='.$id.') as sendtoclient') 
,DB::raw('(select count(id) from tbl_recruiter_cv as rcv where rcv.cv_status=3 and position_id=tcm.clientjob_id and rcv.recruiter_id='.$id.') as rejected') 

)
      ->where('tcr.fk_empid', '=', $id)
                  ->groupBy('tcr.fk_empid', 'tcm.clientjob_title', 'tcm.clientjob_id')
                  ->tosql();*/
$quertd=" where tcr.fk_empid = ".$id."";

if(isset($_GET['user'] ) && $_GET['user']!='')
{
$quertd .=" and tcm.clientjob_title = '".$_GET['user']."' ";  
}
 if(isset($_GET['doj']))
     {
         $from=$_GET['doj'];
         $to=$_GET['doend'];
     }
    
     else
     {
         $from = date('Y-m-01',strtotime(date('Y-m-d')));
  $to =  date('Y-m-t',strtotime(date('Y-m-d')));
     }


$sql = "select distinct(tcm.clientjob_title) as clti,tcm.clientjob_id as cltidd, tcr.fk_empid,
 
 (select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=5 and rcv.position_id=tcm.clientjob_id  and rcv.recruiter_id=".$id." and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."') ) as Pipeline,
(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=6 and rcv.position_id=tcm.clientjob_id  and rcv.recruiter_id=".$id." and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."')) as Joining,

(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=4 and rcv.position_id=tcm.clientjob_id  and rcv.recruiter_id=".$id." and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."')) as Offer,

(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=1 and rcv.position_id=tcm.clientjob_id  and rcv.recruiter_id=".$id." and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."')) as notseen,


(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=2 and rcv.position_id=tcm.clientjob_id  and rcv.recruiter_id=".$id." and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."')) as sendtoclient,


(select count(r.rcvid) from  tbl_recruiter_cvchangestatus as r
 join tbl_recruiter_cv as rcv on r.cvid =rcv.id where r.cv_status=3 and rcv.position_id=tcm.clientjob_id  and rcv.recruiter_id=".$id." and r.cvupdatestatus=1 and  ( Date(r.created_at ) BETWEEN '".$from."' AND '".$to."')) as rejected

    from tbl_clientjdrecruitermap as tcr inner join tbl_clientjd_master as tcm on tcr.fk_jdid = tcm.clientjob_id ".$quertd." group by tcr.fk_empid, tcm.clientjob_title, tcm.clientjob_id";
      $getPositions = DB::select(DB::raw($sql));

//dd($getPositions);
  return view('Emp.recuiterpositionreports',compact('getPositions','from','to'));
}

    //View Role

        
          public function getresumesreportdata($uid,$posid,$fromid,$fromdate,$todate)
        {
// $uid.'--'.$posid.'--'.$fromid.'---'.$fromdate.'--'.$todate; 
          /*   $recruiters = DB::table('tbl_recruiter_cv as u')
                    ->select('u.*')
                    ->where('u.recruiter_id','=',$uid)
                    ->where('u.position_id','=',$posid)
                     ->where('u.cv_status','=',$fromid)
                  // ->whereBetween('Date(u.created_at)', [$fromdate, $todate])
                 ->whereRaw('Date(u.created_at) between "'.$fromdate.'" and "'.$todate.'"')
                    
                    ->get();  
            */

$sql = "select u.* from  tbl_recruiter_cv as u join tbl_recruiter_cvchangestatus as r on r.cvid =u.id where  r.cvupdatestatus=1 and  u.recruiter_id=".$uid."  and u.position_id=".$posid." and r.cv_status=".$fromid." and cvupdatestatus='1' and  ( Date(r.created_at ) BETWEEN '".$fromdate."' AND '".$todate."') ";
      $recruiters = DB::select(DB::raw($sql));



        //  dd($recruiters);
            
             $html="";
           //  $html = $uid.'---'.$posid.'----'.$fromid.'----'.$toid;    
           
           //<a href="http://192.168.1.196/elitehr/storage/app/uploadcv/{{$list->cv_uploaded_date}}/{{$list->cv_name}}" margin-left: 15px;"> 
             foreach($recruiters as $key => $val){
                  if($val->cv_uploaded_date!='') {
 $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td><a href="http://115.124.98.243/~elitehr/storage/app/uploadcv/'.$val->cv_uploaded_date.'/'.$val->cv_name.'">'.$val->cv_name.'</a></td><td>'.$val->created_at.'</td>
            </tr>';
                  }
                  
                  else
                  {
                       $html .= '  <tr>
                <td>'.$val->candidate_name.'</td><td>'.$val->candidate_mob.'</td><td>'.$val->candidate_email.'</td><td>'.$val->cv_name.'</td><td>'.$val->created_at.'</td>
            </tr>';
                      
                  }
           
             }
           /* 
            $html = '';          
           
            foreach($recruiters as $key => $val){
               $html .= '<option value="'.$val->id.'">'.$val->name.'</option>';
            }*/
            echo $html;
        }

        public function getresumespopup($id)
        {
           $html="";
          $sql = "select * from  tbl_recruiter_cv where id IN (".$id.") ";

      $recruiters = DB::select(DB::raw($sql)); 
       $html .= '<table width="100%"><tr><td>Name</td><td>Mobile No</td></tr>';
  foreach($recruiters as $key => $val){
$cid=$val->cv_status;
 if($cid=='1')
            {
              $sel1='selected';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='';
            }
             if($cid=='2')
            {
              $sel1='';$sel2='selected';$sel3='';$sel4='';$sel5='';$sel6='';
            }
            if($cid=='3')
            {
              $sel1='';$sel2='';$sel3='selected';$sel4='';$sel5='';$sel6='';
            }
            if($cid=='4')
            {
              $sel1='';$sel2='';$sel3='';$sel4='selected';$sel5='';$sel6='';
            }
            if($cid=='5')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='selected';$sel6='';
            }
            if($cid=='6')
            {
              $sel1='';$sel2='';$sel3='';$sel4='';$sel5='';$sel6='selected';
            }

  /*$html .= '  <tr>
                <td>'.$val->candidate_name.'</td>  <td>'.$val->candidate_mob.'</td> 
                <td>
<select class="form-control col-xs-6" required="" id="recruiterspockup" name="checkbox_data[]" onchange="changestatusdata(this.value)"><option value="'.$val->id.'/'.$val->position_id.'/1" '.$sel1.'>Not Seen</option>
              <option value="'.$val->id.'/'.$val->position_id.'/2"  '.$sel2.'>Send To Client</option>
              <option value="'.$val->id.'/'.$val->position_id.'/3"  '.$sel3.'>Rejected</option>
              <option value="'.$val->id.'/'.$val->position_id.'/4"   '.$sel4.'>Offer</option>
              <option value="'.$val->id.'/'.$val->position_id.'/5"  '.$sel5.'>Pipeline</option>
              <option value="'.$val->id.'/'.$val->position_id.'/6"  '.$sel6.'>Join</option></select>
                </td>
                <td><div id="place'.$val->id.''.$val->position_id.'"></div></td>
            </tr>';*/

            $html .= '  <tr>
                <td>'.$val->candidate_name.'</td>  <td>'.$val->candidate_mob.'</td> 
            <input type="text" name="canddata[]" value="'.$val->id.'"> 
            </tr>';


  }

    $html .= '  <tr>
                <td><select class="form-control col-xs-6" required="" id="recruiterspockup" name="checkbox_data" onchange="changestatusdata(this.value)"><option value="'.$val->id.'/'.$val->position_id.'/1" '.$sel1.'>Not Seen</option>
              <option value="'.$val->id.'/'.$val->position_id.'/2"  '.$sel2.'>Send To Client</option>
              <option value="'.$val->id.'/'.$val->position_id.'/3"  '.$sel3.'>Rejected</option>
              <option value="'.$val->id.'/'.$val->position_id.'/4"   '.$sel4.'>Offer</option>
              <option value="'.$val->id.'/'.$val->position_id.'/5"  '.$sel5.'>Pipeline</option>
              <option value="'.$val->id.'/'.$val->position_id.'/6"  '.$sel6.'>Join</option></select></td>  <td><div id="place'.$val->id.''.$val->position_id.'"></div></td> 
             
            </tr>';


    $html .= '</table>';
echo $html;
         
        }
        
        
}
