<?php
/**
 * Created by PhpStorm.
 * User: mindz
 * Date: 24/1/18
 * Time: 11:53 AM
 */

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Model\User as User;
use App\Model\Adminmodel\Team as Team;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use \Crypt;

class TeamController extends Controller
{
	public function __construct()
    {
      $this->middleware('auth');
    }

    public function index()
    {
        return view('admin.teamassign');
    }
    
    public function team()
    {
        
        $user = User::all();
        $teamlisting = Team::all();
        
        foreach($teamlisting as $key => $val){
            $names = '';
            $name = '';

            $team_member = '';
            $team_members = '';

            if(!empty($val->team_lead_id)){
                $teamUsers = explode(',',$val->team_lead_id);
               
              
                foreach($teamUsers as $keys => $value){
                    $name = User::where('id',$value) 
                            ->select('name')
                            ->get();
                   //  print_r($name[0]->name); 
               $nametrim=  trim($name[0]->name," ");
                  
                $names .= $nametrim.',';
            //  $names .='';

                   $teamlisting[$key]->team_lead_id = trim($names,',');
                  
                }
                 
            }  
        
        
            if(!empty($val->team_members_id)){
                $teamMembers = explode(',',$val->team_members_id);
                
                foreach($teamMembers as $kes => $values){
                    
                    $team_member = User::where('id',$values) 
                            ->select('name')
                            ->get();
                   $team_members .= $team_member[0]->name.',';
                    $teamlisting[$key]->team_members_id = rtrim($team_members,',');
                }
            }
            
            
            
        } 
       // dd( "gggggggggg");
        // dd($name[0]->name);
        return view('admin.defineteam',compact('user','teamlisting'));
    }

    public function editteam(Request $request,$id)
    {
        $teamDetails = DB::table('tbl_team')
                        ->select('*')
                        ->where('id',Crypt::decrypt($id))
                        ->first();
        
        //dd($teamDet);
        $user = User::all();
        return view('admin.editteam',compact('user','teamlisting','teamDetails'));
    }

    public function teamUpdate($teamId,Request $request)
    {
        $id = Crypt::decrypt($teamId);
        
        //dd($request->teammember);
        $user = User::all();

        $team = Team::find($id);
        $team->team_lead_id = '';
        $team->team_members_id = '';
        $team->team_name = $request->teamname;

        $teamleaders = $request->teamleader;
        foreach($teamleaders as $key => $val){
            $team->team_lead_id .= $val.',';
        }
        $lead_ids = trim($team->team_lead_id,',');
        $team->team_lead_id = $lead_ids;
        
        if(!empty($request->teammember)){
            $teammember = $request->teammember;
            foreach($teammember as $key => $val){
                $team->team_members_id .= $val.',';
            }
            $member_ids = trim($team->team_members_id,',');
            $team->team_members_id = $member_ids;
        }

        $team->team_status = 1;

        $now = Carbon::now();
        $team->team_created_on = $now;
        $team->team_updated_on = $now;
        $team->save();
        
        $teamlisting = Team::all();
        foreach($teamlisting as $key => $val){
            $names = '';
            $name = '';

            $team_member = '';
            $team_members = '';

            if(!empty($val->team_lead_id)){
                $teamUsers = explode(',',$val->team_lead_id);
                foreach($teamUsers as $keys => $value){
                    $name = User::where('id',$value) 
                            ->select('name')
                            ->get();
                    $names .= $name[0]->name.',';
                    $teamlisting[$key]->team_lead_id = rtrim($names,',');
                }
            }

            if(!empty($val->team_members_id)){
                $teamMembers = explode(',',$val->team_members_id);
                foreach($teamMembers as $kes => $values){
                    $team_member = User::where('id',$values) 
                            ->select('name')
                            ->get();
                    $team_members .= $team_member[0]->name.',';
                    $teamlisting[$key]->team_members_id = rtrim($team_members,',');
                }
            }
        }
       
         return redirect('admin/team')->with('msg', 'Team updated successfully.');
    }

    public function deleteteam($id)
    {
        $id = Crypt::decrypt($id);
        $team = Team::find($id);
        $team->delete();
        return redirect('admin/team')->with('msg', 'Team deleted successfully.');
    }

    public function uniqueteam(Request $request)
    {
        $name = DB::table('tbl_team')
                  ->select('id')
                  ->where('team_name','=',json_decode($request->name))
                  ->where('id','!=',$request->id)
                  ->first();
        
        if($name){
            echo true;
        }else{
            echo false;
        }
    }

    public function teamSave(Request $request)
    {
    	//dd($request->teammember);
        $user = User::all();

        $team = new Team();
        $team->team_name = $request->teamname;

        $teamleaders = $request->teamleader;
        foreach($teamleaders as $key => $val){
            $team->team_lead_id .= $val.',';
        }
        $lead_ids = trim($team->team_lead_id,',');
        $team->team_lead_id = $lead_ids;
        
        if(!empty($request->teammember)){
            $teammember = $request->teammember;
            foreach($teammember as $key => $val){
                $team->team_members_id .= $val.',';
            }
            $member_ids = trim($team->team_members_id,',');
            $team->team_members_id = $member_ids;
        }
       
        $team->team_status = 1;

        $now = Carbon::now();
        $team->team_created_on = $now;
        $team->team_updated_on = $now;
        $team->save();

          
      //  dd($teammember );


        foreach($teamleaders as $key=>$value)
        {
            $LastInsertId = $team->id;
             $user_teamid=DB::table('users')
                   ->where('id',$value)
                   ->select('emp_team_id')
                   ->get();


                    if(!empty($user_teamid[0]->emp_team_id))
                    {
                        //dd('gfgh');
                         $teamidsss= explode(',',$user_teamid[0]->emp_team_id);
                        array_push($teamidsss,$LastInsertId);
                         $LastInsertId =implode(',',$teamidsss);

                    }
            
             //dd($LastInsertId );

            DB::table('users')
                ->where('id',$value)
                ->update(['emp_team_id'=>$LastInsertId]);


            DB::table('users')
                ->where('id',$value)
                ->update(['is_TL_TM'=>1]);

        }
        foreach($teammember as $key=>$value)
        {
              $LastInsertId = $team->id;
             $user_teamid=DB::table('users')
                   ->where('id',$value)
                   ->select('emp_team_id')
                   ->get();


                    if(!empty($user_teamid[0]->emp_team_id))
                    {
                       // dd('gfgh');
                         $teamidsss= explode(',',$user_teamid[0]->emp_team_id);
                        array_push($teamidsss,$LastInsertId);
                         $LastInsertId =implode(',',$teamidsss);

                    }
            
             //dd($LastInsertId );

            DB::table('users')
                ->where('id',$value)
                ->update(['emp_team_id'=>$LastInsertId]);

        }
        
        $teamlisting = Team::all();
        foreach($teamlisting as $key => $val){
            $names = '';
            $name = '';

            $team_member = '';
            $team_members = '';

            if(!empty($val->team_lead_id)){
                $teamUsers = explode(',',$val->team_lead_id);
                foreach($teamUsers as $keys => $value){
                    $name = User::where('id',$value) 
                            ->select('name')
                            ->get();
                    $names .= $name[0]->name.',';
                    $teamlisting[$key]->team_lead_id = rtrim($names,',');
                }
            }

            if(!empty($val->team_members_id)){
                $teamMembers = explode(',',$val->team_members_id);
                foreach($teamMembers as $kes => $values){
                    $team_member = User::where('id',$values) 
                            ->select('name')
                            ->get();
                    $team_members .= $team_member[0]->name.',';
                    $teamlisting[$key]->team_members_id = rtrim($team_members,',');
                }
            }
        }
       
        return view('admin.defineteam',compact('user','teamlisting'));
    }
    
     public function vacancy()
    {
        $vacancy = DB::table('tbl_clients')
                    ->join('tbl_clientjd_master', 'tbl_clients.client_id','=','tbl_clientjd_master.clientjob_empid')
                    ->select('tbl_clients.comp_name','tbl_clientjd_master.*')
                    ->get(); 
        
        foreach($vacancy as $key => $val){
            $totalCount = DB::table('tbl_recruiter_cv')
                          ->select('*')
                          ->where('position_id',$val->clientjob_id)
                          ->count();
            $vacancy[$key]->totalCount = $totalCount;              
        }
        return view('admin.vacancylist',compact('vacancy'));
    }

    public function candidateList()
    {
        $candidate = DB::table('tbl_recruiter_cv')
                        ->join('users','users.id','=','tbl_recruiter_cv.recruiter_id')
                        ->join('tbl_clientjd_master', 'tbl_recruiter_cv.position_id','=','tbl_clientjd_master.clientjob_id')
                        ->join('tbl_clients','tbl_clients.client_id','=','tbl_clientjd_master.clientjob_empid')
                        ->select('tbl_clients.*','users.*','tbl_recruiter_cv.*','tbl_clientjd_master.*')
                        ->get(); 
        
        return view('admin.candidatelist',compact('candidate'));
    }






}