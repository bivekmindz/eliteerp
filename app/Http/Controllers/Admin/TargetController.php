<?php 

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Model\Client as Client;
use App\Model\Adminmodel\Role as Role;
use App\Model\Adminmodel\Department as Department;
use App\Model\Adminmodel\Employees as Employees;

use Illuminate\Support\Facades\Auth;
use App\Model\User;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use App\Exceptions\Handler;
use Exception;
// use Datatables;

 
use Illuminate\Http\Request;
 
class TargetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected $redirectTo = '/admin/dashboard';
    protected $redirectAfterLogout = '/admin/login';

    public function __construct()
    {
        $this->middleware('auth');
    }
 
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     
     

 public function target(Request $request)
    {
      

$from=date('Y-m-01',strtotime(date('Y-m-d')));
$to=date('Y-m-t',strtotime(date('Y-m-d')));
        $sql = "select r.rec_tid,r.rec_offeredtarget,r.rec_joinedtarget ,u.name from tbl_rectarget as r inner join users as u on r.rec_rid=u.id where date(r.created_at) BETWEEN '".$from."' and  '".$to."'";
        $data = DB::select(DB::raw($sql));

        $sql = "select s.spoc_tid,s.spoc_offeredtarget,s.spoc_joinedtarget ,c.comp_name, u.name from tbl_spoctarget as s inner join users as u on s.spoc_spid=u.id inner join tbl_clients as c on s.spoc_clientid = c.client_id where date(s.created_at) BETWEEN '".$from."' and  '".$to."'";
        $dataa = DB::select(DB::raw($sql));
        
          // print_r($data);die;
        // dd($rec);

    //  }
      



     return view('admin.recspoctarget',compact('data','dataa')); 
 
}

public function checkrec(Request $request){
    $userid =$request['param1'];

    $sql = "SELECT count(rec_tid) as count FROM tbl_rectarget WHERE rec_rid='".$userid."' and (month(created_at)=month(curdate()))";
    // print_r($sql);
    $recheck = DB::select(DB::raw($sql)); 
    print_r($recheck[0]->count);

}

public function checkspoc(Request $request){
    $userid =$request['param1'];
    $client =$request['param2'];

    $sql = "SELECT count(spoc_tid) as count FROM tbl_spoctarget WHERE spoc_spid='".$userid."' and spoc_clientid='".$client."' and (month(created_at)=month(curdate()))";
    // print_r($sql);
    $spocheck = DB::select(DB::raw($sql)); 
    print_r($spocheck[0]->count);

}
public function insert(Request $request){

   

        $rec = DB::table('tbl_rectarget')->insertGetId(
              ['rec_rid' => $request['param2'], 
              'rec_offeredtarget' => $request['param3'],
              'rec_joinedtarget' => $request['param4']
            ]
        );

        $from=date('Y-m-01',strtotime(date('Y-m-d')));
        $to=date('Y-m-t',strtotime(date('Y-m-d')));
        $sql = "select r.rec_tid,r.rec_offeredtarget,r.rec_joinedtarget ,u.name from tbl_rectarget as r inner join users as u on r.rec_rid=u.id where date(r.created_at) BETWEEN '".$from."' and  '".$to."'";
        $data = DB::select(DB::raw($sql));
        // echo ($data);

        echo ' <table width="100%" border="1" cellspacing="1" cellpadding="1">
                <tr>
                    <th>Recruiter name</th>
                    <th>Offer Target</th>
                    <th>Join Target</th>
                    <th>Action </th>
                </tr> ' ;  
                if(!empty($data)){ 
                    foreach($data as $keyus => $k){
                        echo '<tr>
                           <td>'.$k->name.'</td>
                            <td><input type="text" name="rec_offeredtarget" id="recofferedtarget_'. $k->rec_tid.'" readonly style="border: none" value="'.$k->rec_offeredtarget.'"></td>
                            <td><input type="text" name="rec_joinedtarget" id="recjoinedtarget_'.$k->rec_tid.'" value="'.$k->rec_joinedtarget.'" readonly style="border: none"></td>
                            <td>
                                <div class="btn-group btn-group-sm" style="float: none;">
                                    <button type="button" onclick="return editrow('.$k->rec_tid.')" class="tabledit-edit-button btn btn-primary waves-effect waves-light" style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span>
                                    </button>
                                    <button type="button" onclick="return updaterow('.$k->rec_tid.')" id="update'.$k->rec_tid.'" class="tabledit-edit-button btn btn-primary waves-effect waves-light" style="float: none;margin: 5px;display: none;" >Submit
                                    </button>
                                </div>
                            </td>
                        </tr>';
                         } 
                         }
               echo '</table>';


}

public function update(Request $request){
// echo 1;die;
	 // print_r($request->all());

	 $param1 = $request['param1'];
	 $param2 = $request['param2'];
	 $param3 = $request['param3'];
	// 
   
		  $sql = "update tbl_rectarget as rec set rec.rec_offeredtarget=$param1,rec.rec_joinedtarget=$param2 where rec.rec_tid=$param3 ";
		  // print_r($sql);
		$rec = DB::select(DB::raw($sql));
        // $rec = DB::table('tbl_rectarget')->update(
        //       ['rec_rid' => $request['param2'], 
        //       'rec_offeredtarget' => $request['param3'],
        //       'rec_joinedtarget' => $request['param4']
        //     ]
        // );

        $from=date('Y-m-01',strtotime(date('Y-m-d')));
        $to=date('Y-m-t',strtotime(date('Y-m-d')));
        $sql = "select r.rec_tid,r.rec_offeredtarget,r.rec_joinedtarget ,u.name from tbl_rectarget as r inner join users as u on r.rec_rid=u.id where date(r.created_at) BETWEEN '".$from."' and  '".$to."'";
        $data = DB::select(DB::raw($sql));
        // echo ($data);

        echo ' <table width="100%" border="1" cellspacing="1" cellpadding="1">
                <tr>
                    <th>Recruiter name</th>
                    <th>Offer Target</th>
                    <th>Join Target</th>
                    <th>Action </th>
                </tr> ' ;  
                if(!empty($data)){ 
                    foreach($data as $keyus => $k){
                        echo '<tr>
                            <td>'.$k->name.'</td>
                            <td><input type="text" name="rec_offeredtarget" id="recofferedtarget_'. $k->rec_tid.'" readonly style="border: none" value="'.$k->rec_offeredtarget.'"></td>
                            <td><input type="text" name="rec_joinedtarget" id="recjoinedtarget_'.$k->rec_tid.'" value="'.$k->rec_joinedtarget.'" readonly style="border: none"></td>
                            <td>
                                <div class="btn-group btn-group-sm" style="float: none;">
                                    <button type="button" onclick="return editrow('.$k->rec_tid.')" class="tabledit-edit-button btn btn-primary waves-effect waves-light" style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span>
                                    </button>
                                    <button type="button" onclick="return updaterow('.$k->rec_tid.')" id="update'.$k->rec_tid.'" class="tabledit-edit-button btn btn-primary waves-effect waves-light" style="float: none;margin: 5px;display: none;" >Submit
                                    </button>
                                </div>
                            </td>
                        </tr>';
                         } 
                         }
               echo '</table>';

                            
                    
        


     
        

}

public function insertspoc(Request $request){

   

        $rec = DB::table('tbl_spoctarget')->insertGetId(
              ['spoc_spid' => $request['param2'], 
              
              'spoc_offeredtarget' => $request['param4'],
              'spoc_joinedtarget' => $request['param5'],
              'spoc_clientid' => $request['param6']
            ]
        );  

        $from=date('Y-m-01',strtotime(date('Y-m-d')));
        $to=date('Y-m-t',strtotime(date('Y-m-d')));
        $sql = "select s.spoc_tid,s.spoc_offeredtarget,s.spoc_joinedtarget ,c.comp_name, u.name from tbl_spoctarget as s inner join users as u on s.spoc_spid=u.id inner join tbl_clients as c on s.spoc_clientid = c.client_id where date(s.created_at) BETWEEN '".$from."' and  '".$to."'";
        $data = DB::select(DB::raw($sql));
        // echo ($data);

        echo ' <table width="100%" border="1" cellspacing="1" cellpadding="1">
                <tr>
                    <th>Spoc name</th>
                    <th>Client name</th>

                    <th>Offer Target</th>
                    <th>Join Target</th>
                    <th>Action</th>
                </tr> ' ;  
                if(!empty($data)){ 
                    foreach($data as $keyus => $k){
                        echo '<tr>
                            <td>'.$k->name.'</td>
                            <td>'.$k->comp_name.'</td>
                             <td><input type="text" name="spocofferedtarget" id="spocofferedtarget_'.$k->spoc_tid.'" value="'.$k->spoc_offeredtarget.'" readonly style="border: none"></td>
                            <td><input type="text" name="spocjoinedtarget" id="spocjoinedtarget_'.$k->spoc_tid.'" value="'.$k->spoc_joinedtarget.'" readonly style="border: none"></td>
                            <td>
                                <div class="btn-group btn-group-sm" style="float: none;">
                                    <button type="button" onclick="return editrowspoc('.$k->spoc_tid.')" id="editspoc'.$k->spoc_tid.'" class="tabledit-edit-button btn btn-primary waves-effect waves-light" style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span>
                                    </button>
                                    <button type="button" onclick="return updaterowspoc('.$k->spoc_tid.')" id="updatespoc'.$k->spoc_tid.'" class="tabledit-edit-button btn btn-primary waves-effect waves-light" style="float: none;margin: 5px;display: none;" >Submit
                                    </button>
                                </div>
                            </td>
                   
                        </tr>';
                         } 
                         }
               echo '</table>';

}

public function updatespoc(Request $request){
	// echo 1;
// print_r($request->all());die;

	 $param1 = $request['param1'];
	 $param2 = $request['param2'];
	 $param3 = $request['param3'];
	// 
   
		  $sql = "update tbl_spoctarget as spoc set spoc.spoc_offeredtarget=$param1,spoc.spoc_joinedtarget=$param2 where spoc.spoc_tid=$param3 ";
		  // print_r($sql);
		$rec = DB::select(DB::raw($sql));

        $from=date('Y-m-01',strtotime(date('Y-m-d')));
        $to=date('Y-m-t',strtotime(date('Y-m-d')));
        $sql = "select s.spoc_tid,s.spoc_offeredtarget,s.spoc_joinedtarget ,c.comp_name, u.name from tbl_spoctarget as s inner join users as u on s.spoc_spid=u.id inner join tbl_clients as c on s.spoc_clientid = c.client_id where date(s.created_at) BETWEEN '".$from."' and  '".$to."'";
        $data = DB::select(DB::raw($sql));
        // echo ($data);

        echo ' <table width="100%" border="1" cellspacing="1" cellpadding="1">
                <tr>
                    <th>Spoc name</th>
                    <th>Client name</th>
                    <th>Offer Target</th>
                    <th>Join Target</th>
                    <th>Action</th>
                </tr> ' ;  
                if(!empty($data)){ 
                    foreach($data as $keyus => $k){
                        echo '<tr>
                            <td>'.$k->name.'</td>
                            <td>'.$k->comp_name.'</td>
                            <td><input type="text" name="spocofferedtarget" id="spocofferedtarget_'.$k->spoc_tid.'" value="'.$k->spoc_offeredtarget.'" readonly style="border: none"></td>
                            <td><input type="text" name="spocjoinedtarget" id="spocjoinedtarget_'.$k->spoc_tid.'" value="'.$k->spoc_joinedtarget.'" readonly style="border: none"></td>
                            <td>
                                <div class="btn-group btn-group-sm" style="float: none;">
                                    <button type="button" onclick="return editrowspoc('.$k->spoc_tid.')" id="editspoc'.$k->spoc_tid.'" class="tabledit-edit-button btn btn-primary waves-effect waves-light" style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span>
                                    </button>
                                    <button type="button" onclick="return updaterowspoc('.$k->spoc_tid.')" id="updatespoc'.$k->spoc_tid.'" class="tabledit-edit-button btn btn-primary waves-effect waves-light" style="float: none;margin: 5px;display: none;" >Submit
                                    </button>
                                </div>
                            </td>
                        </tr>';
                         } 
                         }
               echo '</table>';

}

}
