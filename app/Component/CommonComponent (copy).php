<?php

namespace App\Component;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;

class CommonComponent extends Controller
{
    public function getRole()
    {
        $id = Auth::user()->id;
        $roles = DB::table('users')
                ->select('emp_role')
                ->where('id',$id)
                ->first();

        return $roles;
    }

     public function getPositions()
    {

    	$id = Auth::user()->id;
    	$getPositions = DB::table('tbl_clientjdrecruitermap as tcr')
			->join('tbl_clientjd_master as tcm', 'tcr.fk_jdid', '=', 'tcm.clientjob_id')
			->select('tcm.clientjob_title', 'tcr.fk_empid', 'tcm.clientjob_id')
			->where('tcr.fk_empid', '=', $id)
	                ->groupBy('tcr.fk_empid', 'tcm.clientjob_title', 'tcm.clientjob_id')
	                ->get();

        return $getPositions;    						

    }

    public function nav($role)
    {
        $menu=DB::table('tbl_menu as m')
            ->join('tbl_menu_role_assigned as ma','m.id','=','ma.menuid')
            ->select('m.menuname','m.menuparentid','m.url','m.id')
            ->where('roleid',$role)
            ->get();
       //      echo "<pre>";
       //  print_r($menu);
        return $menu;

    }


     public function adminnav()
    {
        $menu=DB::table('tbl_menu as m')
            ->select('m.menuname','m.menuparentid','m.url','m.id')
            ->get();
        return $menu;

    }

        public function breadcrumbs()
    {
      $url_segment = \Request::segment(1);

         $breadcrumb=DB::table('tbl_menu as m')
                       ->join('tbl_menu as m1','m1.id','=','m.menuparentid')
                       ->select('m.menuname','m1.menuname as parentmenu')
                       ->where('m.url', '=', $url_segment)
                       ->get();
                       return $breadcrumb;
       //dd($breadcrumb);
    }


}
