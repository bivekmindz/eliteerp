<?php

namespace App\Component;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use DB;

class CommonComponent extends Controller
{
    public function getRole()
    {
        $id = Auth::user()->id;
        $roles = DB::table('users')
                ->select('emp_role')
                ->where('id',$id)
                ->first();

        return $roles;
    }

     public function getPositions()
    {

    	$id = Auth::user()->id;
    	
    	     
$users1= ("  select (select clientstart_starttime from tbl_clientrec_start_endtime where clientstart_stoptime IS NULL and clientstart_jdid=tcm.clientjob_id and (date(created_at)='".date("Y-m-d")."')order by clientstartid limit 0,1) as timedata , tcm.clientjob_title, tcr.fk_empid, tcm.clientjob_id, tcm.drive_shortlist, tcm.drive_time, tcm.lineup_to, tcm.shortlist_time from tbl_clientjdrecruitermap as tcr inner join tbl_clientjd_master as tcm on tcr.fk_jdid = tcm.clientjob_id where tcr.fk_empid = ".$id." and tcr.clientrequpdate_status = 1 group by tcr.fk_empid, tcm.clientjob_title, tcm.clientjob_id ");


 $getPositions = DB::select(DB::raw($users1));
 /*
    	$getPositions = DB::table('tbl_clientjdrecruitermap as tcr')
			->join('tbl_clientjd_master as tcm', 'tcr.fk_jdid', '=', 'tcm.clientjob_id')
			->select('tcm.clientjob_title', 'tcr.fk_empid', 'tcm.clientjob_id','tcm.drive_shortlist','tcm.drive_time','tcm.lineup_to','tcm.shortlist_time')
			->where('tcr.fk_empid', '=', $id)
				->where('tcr.clientrequpdate_status', '=', 1)
	                ->groupBy('tcr.fk_empid', 'tcm.clientjob_title', 'tcm.clientjob_id')
	                ->get();
*/
        return $getPositions;    						

    }

    public function nav($role)
    {
        $menu=DB::table('tbl_menu as m')
            ->join('tbl_menu_role_assigned as ma','m.id','=','ma.menuid')
            ->select('m.menuname','m.menuparentid','m.url','m.id')
            ->where('roleid',$role)
            ->get();
        return $menu;

    }


     public function adminnav()
    {
        $menu=DB::table('tbl_menu as m')
            ->select('m.menuname','m.menuparentid','m.url','m.id')
            ->get();
        return $menu;

    }

        public function breadcrumbs()
    {
      $url_segment = \Request::segment(1);

         $breadcrumb=DB::table('tbl_menu as m')
                       ->join('tbl_menu as m1','m1.id','=','m.menuparentid')
                       ->select('m.menuname','m1.menuname as parentmenu')
                       ->where('m.url', '=', $url_segment)
                       ->get();
                       return $breadcrumb;
       //dd($breadcrumb);
    }

 public function navigation(){
      // $assigneeid=Auth::user()->emp_id;
    
    $roles= Auth::user()->emp_role;
    // print_r($roles);
    // $emp_role = explode(',',$roles);
    // print_r($emp_role);die;

    $sql = "select m.menuname,m.menuparentid,m.url,m.id from tbl_menu as m inner join tbl_menu_role_assigned as ma on m.id=ma.menuid where ma.roleid IN($roles) and m.status = 1 GROUP BY m.menuname";
    // dd($sql);

    $menu=DB::select(DB::raw($sql));

         return $menu;
      }
        

}
