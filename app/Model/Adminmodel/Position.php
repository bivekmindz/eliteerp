<?php

namespace App\Model\Adminmodel;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table="tbl_clientjd_master";
    
    protected $fillable=['clientjob_deptid','clientjob_title','clientjob_opendate','clientjob_closedate','clientjob_noofposition','clientjob_place','clientjob_minexp','clientjob_maxexp','clientjob_agestart','clientjob_ageend','clientjob_salarystart','clientjob_salaryend','clientjob_education','clientjob_keyskill','clientjob_jobdescription','clientjob_otherinfo','clientjob_compprofile'];
}
