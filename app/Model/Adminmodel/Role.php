<?php

namespace App\Model\Adminmodel;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table="tbl_role";
    protected $fillable=['role_name','role_status'];

}
