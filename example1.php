
        <link href='style.css' rel='stylesheet' type='text/css'>
        <script src='jquery-3.0.0.js' type='text/javascript'></script>
        
        <script>
        $(document).ready(function(){
            $('#search').keyup(function(){
            
            // Search text
            var text = $(this).val();
            
            // Hide all content class element
            $('.content').hide();

            // Search and show
            $('.content:contains("'+text+'")').show();
            
            });
        });
        </script>
   
        <div class='container'>

            <div>
                <input type='text' id='search' placeholder='Search Text'>
            </div>

            <div class='content'>
                <div class='title'>
                    <a href='http://makitweb.com/dynamically-add-and-remove-element-with-jquery/' >Dynamically add and remove element with jQuery</a> 
                </div>
                <p>In this post, I show how you can add and remove new element within your web page with jQuery...</p>
            </div>

            <div class='content'>
                <div class='title'><a href='http://makitweb.com/how-to-delete-value-from-array-in-javascript/'>How to Delete a value from an Array in JavaScript</a></div>
                <p>There are many cases when we have to remove some unused values from the Array which no longer needed within the program...</p>
            </div>

            <div class='content'>
                <div class='title'><a href='http://makitweb.com/read-and-get-parameters-from-url-with-javascript/'>Read and Get Parameters from URL with JavaScript</a></div>
                <p>If you are familiar with PHP where for reading parameters from URL you can use either $_GET or $_REQUEST which take the name of the argument and return value of it...</p>
            </div>

            <div class='content'>
                <div class='title'><a href='http://makitweb.com/html-how-to-show-text-above-image-on-hover/'>HTML – How to Show Text Above Image on Hover</a></div>
                <p>In this quick tutorial, I show How you can show text above Image when the user hovers the image using only HTML and CSS, no jQuery and JavaScript...</p>
            </div>

        </div>
