<script src="<?php echo e(URL::asset('js/jquery-3.1.1.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/tether.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/waves.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/main.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/menu.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/jquery-1.9.1.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/bootstrap3-typeahead.min.js')); ?>"></script>
<!-- <script src="<?php echo e(URL::asset('js/ckeditor.js')); ?>"></script> -->
<script src="<?php echo e(URL::asset('js/jquery.slimscroll.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/jquery.nicescroll.min.js')); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>-->
<script src="<?php echo e(URL::asset('js/jquery.Datatable.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/datdataTables.bootstrap.min.js')); ?>"></script>

<!--
 <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> -->


<script type="text/javascript">
    $(document).ready(function() {
        $('#ajaxhr').multiselect({
            enableCaseInsensitiveFiltering:true,
        includeSelectAllOption: true,
        buttonWidth: 250,
        enableFiltering: true
    });
    });

    $(document).ready(function() {
        $('#boot-multiselect-demo').multiselect({
            enableCaseInsensitiveFiltering:true,
        includeSelectAllOption: true,
        buttonWidth: 250,
        enableFiltering: true
    });
    });
</script>
<script>
    window.ParsleyConfig = {
        errorsWrapper: '<div></div>',
        errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>',
        errorClass: 'has-error',
        successClass: 'has-success'
    };
</script>

<script type="text/javascript">
    $(function() {
        $('.multiselect-ui').multiselect({
            includeSelectAllOption: true
        });
    });
</script>
<script type="text/javascript">
$(".one-click").click(function(){
    $(".hide_css").slideToggle(700);
});
</script>
<script src="<?php echo e(URL::asset('js/sel.js')); ?>"></script>
 <script>
                $(function()
                {
                    $(document).on('click', '.btn-add', function(e)
                    {
                        e.preventDefault();

                        var controlForm = $('.controls form:first'),
                            currentEntry = $(this).parents('.entry:first'),
                            newEntry = $(currentEntry.clone()).appendTo(controlForm);

                        newEntry.find('input').val('');
                        controlForm.find('.entry:not(:last) .btn-add')
                            .removeClass('btn-add').addClass('btn-remove')
                            .removeClass('btn-success').addClass('btn-danger')
                            .html('<span class="icofont icofont-minus"></span>');
                    }).on('click', '.btn-remove', function(e)
                    {
                        $(this).parents('.entry:first').remove();

                        e.preventDefault();
                        return false;
                    });
                });

            </script>
            
            <script>
                $(function()
                {
                    $(document).on('click', '.btn-add', function(e)
                    {
                        e.preventDefault();

                        var controlForm = $('.controls table:first'),
                            currentEntry = $(this).parents('.entry:first'),
                            newEntry = $(currentEntry.clone()).appendTo(controlForm);

                        newEntry.find('input').val('');
                        controlForm.find('.entry:not(:last) .btn-add')
                            .removeClass('btn-add').addClass('btn-remove')
                            .removeClass('btn-success').addClass('btn-danger')
                            .html('<span class="icofont icofont-minus"></span>');
                    }).on('click', '.btn-remove', function(e)
                    {
                        $(this).parents('.entry:first').remove();

                        e.preventDefault();
                        return false;
                    });
                });

            </script>

<script>
    $('#summernote').summernote('code');
</script>

<script>
$(".flo").click(function(){
    $(".hid").slideToggle(700);
});
</script>

<script>
$(".op").click(function(){
    $(".dt").slideToggle(700);
});
</script>

<script>
$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});
</script>
