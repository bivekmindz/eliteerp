<label>Recruiter</label>

    <li class="treeview">
        <a class="waves-effect waves-dark" href="">
        <i class="icon-speedometer"></i><span> My Positions</span><i class="icon-arrow-down"></i>
    </a>
    <?php $position = app('App\Component\CommonComponent'); ?>

    <ul class="treeview-menu">
        <?php  
            $positions = $position->getPositions();
         ?>
<!-- <?PHP ECHO "<PRE>"; PRINT_R( $positions);?>
 -->

        <?php if(!empty($positions)): ?>
        <?php $__currentLoopData = $positions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <li class="active" id="unclick">
            <a class="waves-effect waves-dark posNavName" href="<?php echo e(url::to('get-assigned-postion/'.Crypt::encrypt($p->clientjob_id) )); ?>">
            <i class="icon-arrow-right"></i>
            <span><?php echo e($p->clientjob_title); ?> </span> 
            </a>
        </li>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </ul>
</li>
