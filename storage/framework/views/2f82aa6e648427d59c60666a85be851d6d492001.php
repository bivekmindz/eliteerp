<?php $__env->startSection('content'); ?>
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>

"<style>
.ir{    overflow: overlay;}
</style>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Assign Menu</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Menu</a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Assign Menu</a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
            <!-- Textual inputs starts -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h5 class="card-header-text">Assign Menu</h5>

                    </div>


                    <div class="card-block">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group row">
                                    <?php if(Session::has('success_msg')): ?>
                                        <div class="alert alert-success">
                                            <?php echo e(Session::get('success_msg')); ?>

                                        </div>
                                    <?php endif; ?>

                                    <?php echo Form::open([ 'action'=>'Admin\MenuController@assignmbyrole', 'method'=>'post','data-parsley-validate' ]); ?>

                                     <?php if(count($errors)): ?>
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.
                                    <br/>
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                    <div class="col-sm-4">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Role Type </label>
                                        <div class="col-sm-8">
                                        <div class="form-group <?php echo e($errors->has('roleid') ? 'has-error' : ''); ?>">

                                            <select   name="roleid" class="form-control" id="roleid" onchange="getparentmenu();" required>
                                             <option value="">Select Role</option>

                                                <?php $__currentLoopData = $role; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($value->role_id); ?>"><?php echo e($value->role_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                              <span class="text-danger"><?php echo e($errors->first('roleid')); ?></span>
                                        </div>
                                        </div>

                                    </div>




                                    <div class="col-sm-4" id="parent" style="display: none">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Parent Menu </label>
                                        <div class="col-sm-8">
                                        <div class="form-group  <?php echo e($errors->has('parentid') ? 'has-error' : ''); ?>">

                                            <select  name="parentid[]" class="form-control" id="parentid" onchange="getchildmenu();" required>
                                            <option value="">Select parent menu</option>
                                                
                                                    
                                                
                                            </select>
                                            </div>
                                              <span class="text-danger"><?php echo e($errors->first('parentid')); ?></span>
                                        </div>
                                    </div>




                                    <div class="col-sm-4"  id="child" style="display: none" >
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Child Menus </label>
                                        <div class="col-sm-8">
                                        <div class="form-group  <?php echo e($errors->has('childid') ? 'has-error' : ''); ?>">

                                            <select multiple="multiple" name="childid[]" class="form-control" id="childid"  required>


                                            </select>
 
  </div>
                                        </div>
                                          <span class="text-danger"><?php echo e($errors->first('childid')); ?></span>
                                    </div>


                                     <div class="col-sm-4">
                                     <div class="col-sm-8">
                                        <div class="form-group">
                                    <input type="submit" class="btn btn-primary waves-effect waves-light" name="rolesubmit" value="Save"  id="rolesubmit"  >
                                    </input>
                                    </div>
                                    </div>
                                   

                                </div>
                                 <?php echo Form::close(); ?>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php $__env->stopSection(); ?>
        <script>
            function getchildmenu()
            {
                //alert('gfdh');
                var p=$('#parentid').val();
               // alert(p);
                var url_data = p;
                if($('#parentid').val()==0){
                    $('#child').hide();
                }else{
                    $('#child').show();
                }

                $.ajax({
                    url: 'childmenu/'+url_data,
                    type: 'get',
                    success: function(data){
                      //alert(data);
                       // $('#childid').empty();
                        $("#childid").html(data);

                    }
                });



            }

            function getparentmenu()
            {
               var role=$('#roleid').val();

               var url_data=role;
               if($('#roleid').val()==0)
               {
                   $('#parent').hide();
               }
               else
               {
                   $('#parent').show();
               }

               $.ajax({
                   url: 'parentmenu/'+url_data,
                   type: 'get',
                   success:function(data)
                   {
                       //console.log(data);
                      // alert(data);
                       $('#parentid').html(data);

                   }

               });

               }


        </script>


<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>