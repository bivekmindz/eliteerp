
<ul class="sidebar-menu">
    <li class="nav-level"><a href="<?php echo e(URL::to(''.'dashboard')); ?>">Dashboard</a></li>


    <?php $userRole = app('App\Component\CommonComponent'); ?>
    <?php $position = app('App\Component\CommonComponent'); ?>
    <?php
    $roles = $userRole->getRole();
  //print_r($roles);
    $arr=(array) $roles->emp_role;
    $d=''
;    foreach($arr as $k){
        $d .= $k;
    }

    if (strpos($d, '3') !== false) {
    // echo 'true';
    ?>
<li class="treeview">
        <a class="waves-effect waves-dark" href="">
        <i class="icon-speedometer"></i><span> My Positions</span><i class="icon-arrow-down"></i>
    </a>
        <ul class="treeview-menu">
            <?php  
                $positions = $position->getPositions();
                $mytime = date('Y-m-d');
      


             ?>
    

            <?php if(!empty($positions)): ?>
            <?php $__currentLoopData = $positions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($p->drive_shortlist=='Drive'){?>
                 
                <li class="active" id="unclick">
                    <?php if(($p->drive_time <$mytime) &&($p->drive_time!='')){?>
                       <a class="waves-effect waves-dark posNavName" href="javascript:void(0);" onclick="modalopen()">
                        <i class="icon-arrow-right"></i>
                        <span><?php echo e($p->clientjob_title); ?> </span> 
                        </a>
                   <?php } else {?>

                        <a class="waves-effect waves-dark posNavName" href="<?php echo e(url::to('get-assigned-postion/'.Crypt::encrypt($p->clientjob_id) )); ?>">
                        <i class="icon-arrow-right"></i>
                        <span><?php echo e($p->clientjob_title); ?> </span> 
                        </a>
                    <?php } ?>
                </li>

            
            <?php } ?>
            <?php if($p->drive_shortlist=='DaliyLineUp'){?>
                 
                <li class="active" id="unclick">
                    <?php if(($p->lineup_to <$mytime) &&($p->lineup_to!='')){?>
                       <a class="waves-effect waves-dark posNavName" href="javascript:void(0);" onclick="modalopen()">
                        <i class="icon-arrow-right"></i>
                        <span><?php echo e($p->clientjob_title); ?> </span> 
                        </a>
                   <?php } else {?>

                     <a class="waves-effect waves-dark posNavName" href="<?php echo e(url::to('get-assigned-postion/'.Crypt::encrypt($p->clientjob_id) )); ?>">
                    <i class="icon-arrow-right"></i>
                    <span><?php echo e($p->clientjob_title); ?> </span> 
                    </a>
                    <?php } ?>
                </li>

            
            <?php } ?>
            <?php if($p->drive_shortlist=='Shortlist'){?>
                 
                <li class="active" id="unclick">
                    <?php if(($p->shortlist_time <$mytime) &&($p->shortlist_time!='')){?>
                        <a class="waves-effect waves-dark posNavName" href="javascript:void(0);" onclick="modalopen()">
                        <i class="icon-arrow-right"></i>
                        <span><?php echo e($p->clientjob_title); ?> </span> 
                        </a>
                    <?php } else {?>

                        <a class="waves-effect waves-dark posNavName" href="<?php echo e(url::to('get-assigned-postion/'.Crypt::encrypt($p->clientjob_id) )); ?>">
                        <i class="icon-arrow-right"></i>
                        <span><?php echo e($p->clientjob_title); ?> </span> 
                        </a>
                    <?php } ?>
                </li>

            
            <?php } ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
        </ul>
    </li>
 



   
    <?php } ?>
    
    <?php
        $nav = $userRole->navigation();
        foreach($nav as $key=>$value)
        { ?>
            <li class="treeview">
            <?php if($value->menuparentid==0)
            { ?>
                <a class="waves-effect waves-dark" href="<?php echo e(URL::to(''.$value->url)); ?>">
                    <i class="icon-speedometer"></i><span><?php echo e($value->menuname); ?></span><i class="icon-arrow-down"></i>
                </a>
            <?php }
                foreach($nav as $k=>$v)
                { 
                    if($v->menuparentid==$value->id)
                    { ?>
                      <ul class="treeview-menu">
                        <li class="active"><a class="waves-effect waves-dark" href="<?php echo e(URL::to(''.$v->url)); ?>"><i class="icon-arrow-right"></i><span><?php echo e($v->menuname); ?> </span></a></li>

                        </ul>  
                    <?php }
                    
                } ?>
                    
            </li> 
        <?php }
       
        ?>

 <!-- <li class="nav-level"><a href="<?php echo e(URL::to(''.'dashboard')); ?>">Hi</a></li> -->


</ul>
<!-- <div class="modal fade in" id="myModal" role="dialog" >
    <div class="modal-dialog">
<div class="modal-content">
   <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">×</button>
      <h5 class="modal-title">Position Expire</h5>
   </div>
   <form method="post" action="">
      <div class="modal-body">
         <div class="form-group row">
            <div class="col-sm-9">
               <div class="form-group">
                  <p>Position already expire </p>
                
                
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         <input type="submit" class="btn btn-default" value="Submit">
      </div>
   </form>
</div>
    </div>
  </div> -->

<script type="text/javascript">
    function modalopen() {
        alert('Position already expire please contact your SPOC for new position');
        // echo 111;
                
               // $('#myModal').modal('show');
            }

</script>

</section>
</aside>
<!-- Sidebar chat start -->

<div class="showChat_inner">
    <div class="media chat-inner-header">
        <a class="back_chatBox">
            <i class="icofont icofont-rounded-left"></i> Josephin Doe
        </a>
    </div>
    <div class="media chat-messages">
        <a class="media-left photo-table" href="#!">
            <img class="media-object img-circle m-t-5" src="assets/images/avatar-1.png" alt="Generic placeholder image">
            <div class="live-status bg-success"></div>
        </a>
        <div class="media-body chat-menu-content">
            <div class="">
                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                <p class="chat-time">8:20 a.m.</p>
            </div>
        </div>
    </div>
    <div class="media chat-messages">
        <div class="media-body chat-menu-reply">
            <div class="">
                <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                <p class="chat-time">8:20 a.m.</p>
            </div>
        </div>
        <div class="media-right photo-table">
            <a href="#!">
                <img class="media-object img-circle m-t-5" src="assets/images/avatar-2.png" alt="Generic placeholder image">
                <div class="live-status bg-success"></div>
            </a>
        </div>
    </div>
    <div class="media chat-reply-box">
        <div class="md-input-wrapper">
            <input type="text" class="md-form-control" id="inputEmail" name="inputEmail" >
            <label>Share your thoughts</label>
            <span class="highlight"></span>
            <span class="bar"></span>  <button type="button" class="chat-send waves-effect waves-light">
                <i class="icofont icofont-location-arrow f-20 "></i>
            </button>

            <button type="button" class="chat-send waves-effect waves-light">
                <i class="icofont icofont-location-arrow f-20 "></i>
            </button>
        </div>

    </div>
</div>
<!-- Sidebar chat end-->






























