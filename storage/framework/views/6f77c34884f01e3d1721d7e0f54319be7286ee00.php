<script src="<?php echo e(URL::asset('js/jquery-ui.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/tether.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/waves.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/bootstrap-clockpicker.min.js')); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
<script type="text/javascript" src="<?php echo e(URL::asset('js/main.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/jquery.slimscroll.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/jquery.nicescroll.min.js')); ?>"></script>
<script src="<?php echo e(URL::asset('js/menu.js')); ?>"></script>



<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>-->
<script src="<?php echo e(URL::asset('js/jquery.Datatable.min.js')); ?>"></script>
<!--<script src="<?php echo e(URL::asset('js/datdataTables.bootstrap.min.js')); ?>"></script>-->

<script>
  
    function breakTime()
    {
        $('#breakTimeBack').show();
        $.ajax({
            type:"GET",
            url:'<?php echo e(URL::to('breaktime')); ?>',
            data:{ '_token': "<?php echo e(csrf_token()); ?>",
            },
            success: function(data){
               // location.reload(true);
            },
            error: function(data){
              // location.reload(true);
            }

        });
    }

    function mybreakbacktime(){   
        localStorage.removeItem("popopen");
        $('#myBreakTimeModal').modal('show');

        $(".overlay").hide();
        $.ajax({
            type:"GET",
            url:'<?php echo e(URL::to('breaktimeback')); ?>',
            data:{ 
               '_token': "<?php echo e(csrf_token()); ?>",
            },
            success: function(data){
                $("#minutes").html('');
                if(data == 0){
                    $("#minutes").append('Hi,happy to have you back.');
                }else{
                    $("#minutes").append('Hi,happy to have you back.But you are ' + data + ' minutes late.');
                }    
            },
            error: function(data){
               // location.reload(true);
            }

        });
    }
</script>

<script>
$(function() {
$('.multiselect-ui').multiselect({
   enableClickableOptGroups: true,
            enableFiltering: true,
            enableCaseInsensitiveFiltering : true,
includeSelectAllOption: true
});
});
</script>
<script>
$(".flo").click(function(){
    $(".hid").slideToggle(700);
});
</script>
<script>
    $(".one-click").click(function(){
        $(".hide_css").slideToggle(700);
    });
</script>
<script src="<?php echo e(URL::asset('js/sel.js')); ?>"></script>

<script>
$(function()
{
    $(document).on('click', '.btn-add', function(e)
    {
        e.preventDefault();

        var controlForm = $('.controls table:first'),
            currentEntry = $(this).parents('.entry:first'),
            newEntry = $(currentEntry.clone()).appendTo(controlForm);

        newEntry.find('input').val('');
        controlForm.find('.entry:not(:last) .btn-add')
            .removeClass('btn-add').addClass('btn-remove')
            .removeClass('btn-success').addClass('btn-danger')
            .html('<span class="icofont icofont-minus"></span>');
    }).on('click', '.btn-remove', function(e)
    {
        $(this).parents('.entry:first').remove();

        e.preventDefault();
        return false;
    });
    
});

</script>


<script type="text/javascript">
$('.clockpicker').clockpicker()
    .find('input').change(function(){
        console.log(this.value);
    });
    var input = $('#single-input,#single-input2').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    });

$('.clockpicker-with-callbacks').clockpicker({
        donetext: 'Done',
        init: function() { 
            console.log("colorpicker initiated");
        },
        beforeShow: function() {
            console.log("before show");
        },
        afterShow: function() {
            console.log("after show");
        },
        beforeHide: function() {
            console.log("before hide");
        },
        afterHide: function() {
            console.log("after hide");
        },
        beforeHourSelect: function() {
            console.log("before hour selected");
        },
        afterHourSelect: function() {
            console.log("after hour selected");
        },
        beforeDone: function() {
            console.log("before done");
        },
        afterDone: function() {
            console.log("after done");
        }
    })
    .find('input').change(function(){
        console.log(this.value);
    });

// Manually toggle to the minutes view
$('#check-minutes').click(function(e){
    // Have to stop propagation here
    e.stopPropagation();
    input.clockpicker('show')
            .clockpicker('toggleView', 'minutes');
});
if (/mobile/i.test(navigator.userAgent)) {
    $('input').prop('readOnly', true);
}
</script>
<script>
    window.ParsleyConfig = {
        errorsWrapper: '<div></div>',
        errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>',
        errorClass: 'has-error',
        successClass: 'has-success'
    };
</script>
<script>
    $('#summernote').summernote('code');
</script>
<script>
    var input = $('#input-a');
    input.clockpicker({
        autoclose: true
    });

    // Manual operations
    $('#button-a').click(function(e){
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show')
                .clockpicker('toggleView', 'minutes');
    });
    $('#button-b').click(function(e){
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show')
                .clockpicker('toggleView', 'hours');
    });
</script>


