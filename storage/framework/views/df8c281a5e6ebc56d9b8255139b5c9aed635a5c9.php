<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View All Position Allocation To Recruiter</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Positions</a>
                </li>
                <li class="breadcrumb-item"><a href="">Position assigned to Recuriter</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">View All Position Assigned To Recruiter</h5>
                    <form action="<?php echo e(URL::to('excellistassignrecruiterdownload')); ?>"  method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <input type="submit" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel" value="Download Excel" name="downexcel">
                                             
                                            </input>
                                        </form>
                </div>
                <?php if(Session::has('success_msg')): ?>
                <div class="alert alert-success">
                    <?php echo e(Session::get('success_msg')); ?>

                </div>
                <?php endif; ?>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                            <table class="table" id="search_filter">
                                <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Position Name</th>
                                    <th>Company Name</th>
                                    <th>Drive</th>
                                    <th>No Of Positions</th>
                                    <th>Assigned Recuriter Name</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                $i=1;
                                 ?>
                                <?php $__currentLoopData = $pos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr class="table-active">
                                    <td><?php echo e($i++); ?></td>
                                    <td><?php echo e(!empty($p->clientjob_title) ? $p->clientjob_title : 'N/A'); ?></td>
                                    <td><?php echo e(!empty($p->comp_name) ? $p->comp_name : 'N/A'); ?></td>
                                    <td>N/A</td>
                                    <td><?php echo e(!empty($p->clientjob_noofposition) ? $p->clientjob_noofposition : 'N/A'); ?></td>
                                    <td><?php echo e(!empty($p->name) ? $p->name : 'N/A'); ?></td>
                                    <td>
                                        
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModal" onclick="getrecruiterlist(<?php echo $p->fk_jdid;?>);getclientreq_id(<?php echo $p->clientreq_id;?>);">Assign</button> | 
                                        <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#myModalUnassigned" onclick="getrecruiterunassignedlist(<?php echo $p->fk_jdid;?>);">UnAssign</button>
                                    </td>

                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
</script>
<?php $__env->stopSection(); ?>
<div class="container">
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Assigned Recruiter</h4>
        </div>
        <form method="post" action="<?php echo e(URL::to('updateassrecruiters')); ?>">
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="form-group">
                          <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Select Recruiter:</label>
                          <select class="multiselect-ui form-control col-xs-6" multiple="multiple" required id="recruiter" name="recruiter[]">
                             <option value="">Select Recruiters</option>                   
                          </select>
                          <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
                          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-default" value="Submit">
            </div>
        </form>
      </div>      
    </div>
  </div> 
</div>

<div class="container">
   <div class="modal fade" id="myModalUnassigned" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Assigned Recruiter</h4>
        </div>
        <form method="post" action="<?php echo e(URL::to('delassrecruiters')); ?>">
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="form-group">
                          <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Select Recruiter:</label>
                          <select class="multiselect-ui form-control col-xs-6" multiple="multiple" required id="unassignedrecruiter" name="recruiter[]">
                             <option value="">Select Recruiters</option>                   
                          </select>
                          <input type="hidden" name="fk_jdid" id="fk_jdid" value="">
                          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-default" value="Submit">
            </div>
        </form>
      </div>      
    </div>
  </div> 
</div>

<script>
function getrecruiterlist(fk_jdid)
{
    var position_id = fk_jdid;
    $.ajax({
        type:"GET",
        url:"getrecruiters/" + position_id,
        data:'',
        success: function(data){
            $("#recruiter").empty();
            $("#recruiter").append(data);
            $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}

function getrecruiterunassignedlist(fk_jdid)
{
    var position_id = fk_jdid;
    $("#fk_jdid").val(fk_jdid);
    $.ajax({
        type:"GET",
        url:"getunassignedrecruiters/" + position_id,
        data:'',
        success: function(data){
            $("#unassignedrecruiter").empty();
            $("#unassignedrecruiter").append(data);
            $('#unassignedrecruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}

function getclientreq_id(clientreq_id)
{
    $("#clientreq_id").val(clientreq_id);
}
</script>
<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>