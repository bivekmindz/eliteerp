
<?php $__env->startSection('content'); ?>

<style>
.form-iu{ width:100%; float:left;}
.form-iu a{ float:right;}
.form-iu .
sui{    border: none;
    padding: 8px 25px;
    background-color: #2a653d;
    color: #fff;}
</style>
    <div class="container-fluid">
        <div class="row">
            <!-- Textual inputs starts -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">First Work Employee Data</h5>
                    </div>
                    <?php if(Session::has('success_msg')): ?>
                        <div class="alert alert-success">
                            <?php echo e(Session::get('success_msg')); ?>

                        </div>
                    <?php endif; ?>
                    <?php if(Session::has('error_msg')): ?>
                        <div class="alert alert-danger">
                            <?php echo e(Session::get('error_msg')); ?>

                        </div>
                    <?php endif; ?>
                   
                </div>
            </div>

            <!-- Textual inputs ends -->
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    
                    <div class="form-iu">
                    
                 
                      <form action="" method="get">
                      <div class="col-md-4">
                        <input class="form-control document" type="text" value="" id="dojnew" name="doj" />
 </div>                       <div class="col-md-4">
                        <input type="submit" class="sui" name="formsubmit">
                        </div>
                        </form>
                   
                    <div class="col-md-4">
                    
                    <a href="excelattendancedownload">
                                                <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel">Download Excel
                                                    <span class="badge"><i class="icofont icofont-file-excel"></i></span>
                                                </button>
                                            </a>
                    </div>
                          
                      
                        </div>
                        <h5 class="card-header-text"></h5>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Emp id</th>
                                         <th>Emp Code</th>
                                        <th>Name</th>
                                          <th>Punching  time</th>
                                        <th>Login  time</th>
                                        <th>Work Start Time</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $i = 0;
                                     ?>
                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="table-active">
                                        <td><?php echo e(++$i); ?></td>
                                        <td><?php echo e($val->empcode); ?></td>
                                        <td><?php echo e($val->empname); ?>(<?php echo e($val->usersloginname); ?>)</td>
                                        
                                        <td><?php 
                                            $vpunchrec=explode(',', $val->punchrecord)
                                             ?>

                                            <?php echo e($vpunchrec[0]); ?></td>
                                            <td><?php echo e($val->userslogintime); ?></td>
                                             <td><?php echo e($val->clientstart_starttime); ?></td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>




    </div>


   <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">
    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>
<script>
     $(document).ready(function () {  
          $('#dojnew').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>