


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Daily Submission report</title>
    <style>
        .main{

            max-width: 850px;
            width: 100%;
            overflow: hidden;
            border: solid 1px #CCCCCC;
            border-right: solid 1px #CCCCCC;
            font-family: Rubik, sans-serif;
            font-size: 13px;
            margin: 0 auto;background-color: #f5f5f5;}
        .main p{    line-height: 25px;}
        .main .logo{ float:right;}
        .main .head-f{ color:#333;     font-weight: 600;}
        .main .logo img{     width: 108px;
            padding: 10px 13px;}
        .pad_le{ padding:15px 20px;}
        .table-mail-data{ width: 100%; float:left; padding:15px 0px;}
    </style>
</head>

<body>
<div class="main">

    <div class="pad_le">

        <div class="logo"> <img src="assets/images/logo.png" /></div>
         <p class="head-f">Seven O Clock Mail </p>
        
       
  <?php if(!empty($mail_data)): ?>
            <?php $__currentLoopData = $mail_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
 <?php 
 $countdata=count($value->clientjob_title);

          ?>
       <?php if( $countdata>0): ?>
<table width="100%" border="1" cellspacing="0" cellpadding="0">

<tr>
    <td width="10%" style="border:none;" height="30" align="center"><strong><?php echo e($value->name); ?>,</strong></td>
    <td colspan="3" style="border:none;"><p>&nbsp;</p>
    <p>&nbsp;</p></td>
    <td width="24%" style="border:none;" align="center"><strong>Total Submissions: <?php echo e($value->upload_cvcount2status); ?></strong></td>
  </tr>
  
  <tr>
    <th height="40" scope="col">Client</th>
    <th width="19%" scope="col">Position Name</th>
    <th width="29%" scope="col">Number of uploaded cv</th>
    <th width="18%" scope="col">Sent to Client</th>
    <th scope="col">Submissions</th>
  </tr>
    <?php 
         $clientjobtitle=explode(',',$value->clientjob_title);
          $posd=explode(',',$value->pos);
           $uploadd_cv=explode(',',$value->upload_cv);
         
            $sent_to_clientd=explode(',',$value->sent_to_client);
          ?>
         
          <?php $__currentLoopData = $clientjobtitle; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $valuejob): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <tr>
    <td height="33" ><?php echo e($valuejob); ?></td>
    <td><?php echo e($posd[$key]); ?></td>
    <td><?php echo e($uploadd_cv[$key]); ?></td>
    <td><?php echo e($sent_to_clientd[$key]); ?></td>
  <td><?php echo e($sent_to_clientd[$key]); ?></td>
  </tr>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</table>


  <?php endif; ?>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            
        <p> Please get in touch for any clarifications.</p>


        <p class="head-f">Regards,</p>

        <p>Elite Software</p>
    </div>
</div>

</body>
</html>

