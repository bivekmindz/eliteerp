


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Daily Cv uploaded report</title>
    <style>
        .main{

            max-width: 650px;
            width: 100%;
            overflow: hidden;
            border: solid 1px #CCCCCC;
            border-right: solid 1px #CCCCCC;
            font-family: Rubik, sans-serif;
            font-size: 13px;
            margin: 0 auto;background-color: #f5f5f5;}
        .main p{    line-height: 25px;}
        .main .logo{ float:right;}
        .main .head-f{ color:#333;     font-weight: 600;}
        .main .logo img{     width: 108px;
            padding: 10px 13px;}
        .pad_le{ padding:15px 20px;}
    </style>
</head>

<body>
<div class="main">

    <div class="pad_le">

        <div class="logo"> <img src="public/images/logo.png" /></div>

        <p class="head-f">Dear Team,</p>

       <table border="1">
             <thead>
             <tr>
             <th>Company Name</th>
              <th>Position Name</th>
               <th>Spoc Name</th>
                 <th>Recuriter Name</th>
                
                
                 <th>Number of uploaded cv</th>
                 <th>Sent to Client</th>
                 <th>Submissions</th>
                 <th>Total Submission Post</th>
             </tr>
             </thead>
            <tbody>

            <?php if(!empty($mail_data['report'])): ?>
            <?php $__currentLoopData = $mail_data['report']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <tr class="table-active">
                <td><?php echo e($value->comp_name); ?></td>
                 <td><?php echo e($value->clientjob_title); ?></td>
                <td><?php echo e($value->spoc); ?></td>
               
             <td><table >
<?php if($value->recruiter_name != ""): ?> <?php $__currentLoopData = explode(',', $value->recruiter_name); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $info): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
   <tr><td><?php echo e($info); ?></td></tr><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
</table> </td>
             
              <td><table >
<?php if($value->upload_cv != ""): ?> <?php $__currentLoopData = explode(',', $value->upload_cv); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $infocv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
   <tr><td><?php echo e($infocv); ?></td></tr><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
</table> </td>

<td><table >
<?php if($value->sent_to_client != ""): ?> <?php $__currentLoopData = explode(',', $value->sent_to_client); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $infoclient): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
   <tr><td><?php echo e($infoclient); ?></td></tr><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
</table> </td>
<td><table >
<?php if($value->sent_to_client != ""): ?> <?php $__currentLoopData = explode(',', $value->sent_to_client); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $infoclient): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
   <tr><td><?php echo e($infoclient); ?></td></tr><?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
</table> </td>

                        <td><?php echo e($value->totalupload_cv); ?></td>

                </tr>

                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            </tbody>
         </table>



        <p> Please get in touch for any clarifications.</p>


        <p class="head-f">Regards,</p>

        <p>Elite Software</p>
    </div>
</div>

</body>
</html>

