<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>TEAM</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Team</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Edit Team</a>
                    </li>
                </ol>
            </div>
        </div>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#teamleader').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });

            $('#teammember').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });
        });
    </script>
    <div class="row">
        <!-- Textual inputs starts -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">Edit Team</h5>
                </div>

                <!-- end of modal -->
                <div class="card-block">
                    <form action="<?php echo e(URL::to('admin/editteam/'.Crypt::encrypt($teamDetails->id))); ?>" onsubmit="return validateForm();" id="team" method="post">
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Team Name<span class="error"> * </span></label>
                            <div class="col-sm-5">
                                <input class="form-control" type="text" onchange="onChangeTeam()" name="teamname" value="<?php echo e($teamDetails->team_name); ?>" id="teamname">
                                <input type="hidden" value="<?php echo e($teamDetails->id); ?>" id="team_id">
                            </div>
                            <span class="error" style="display:none;" id="team_msg">Please enter team name.</span>
                        </div>
                        <?php 
                            $teamLeads = explode(',',$teamDetails->team_lead_id);
                            $selected = '';
                         ?>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Team Leader<span class="error"> * </span></label>
                            <div class="col-sm-5">
                                <div class="">
                                    <select id="teamleader" name="teamleader[]" class="multiselect-ui form-control" multiple="multiple">
                                        <?php $__currentLoopData = $user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $users): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php 
                                                if(in_array($users->id,$teamLeads))
                                                {
                                                  $selected = 'selected';
                                                } else {
                                                  $selected = '';
                                                }
                                             ?>
                                            <option <?php echo e($selected); ?> value="<?php echo e($users->id); ?>"><?php echo e($users->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <?php 
                            $teamMembers = explode(',',$teamDetails->team_members_id);
                            $selected = '';
                         ?>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Team Member</label>
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <select id="teammember" name="teammember[]" class="multiselect-ui form-control" multiple="multiple">
                                        <?php $__currentLoopData = $user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $users): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php 
                                                if(in_array($users->id,$teamMembers))
                                                {
                                                  $selected = 'selected';
                                                } else {
                                                  $selected = '';
                                                }
                                             ?>
                                            <option <?php echo e($selected); ?> value="<?php echo e($users->id); ?>"><?php echo e($users->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="md-input-wrapper">
                            <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light">Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Textual inputs ends -->
    </div>
<?php $__env->stopSection(); ?>


<style>
    .error{
        color:red;
    }
</style>

<script type="text/javascript">
      function onChangeTeam() {
        var teamname = $("#teamname").val();
         $.ajax({
                method: 'post',
                url: '<?php echo e(route('teamuniquetitle')); ?>',
                data: {
                    'id': $("#team_id").val(),
                    'name': JSON.stringify(teamname.trim()),
                    '_token':'<?php echo e(csrf_token()); ?>',
                },
                success: function(respnose){
                    if(respnose == 1){
                        alert('Please enter unique team name.');
                        $('#teamname').focus();
                        $("#submit").attr('disabled',true);
                    }else{
                        $("#submit").attr('disabled',false);
                    }
                },
                error: function(data){
                    console.log(data);
                    alert("fail" + ' ' + this.data)
                },
            });
      }
</script>

<script>
    function validateForm() {

        var teamname = $("#teamname").val();
        if(teamname == '') {
            alert('Please enter team name.');
            return false;
        }
        
        var teamleader = $("#teamleader").val();
        var lea = teamleader.toString();
        var tealead = lea.split(',');

        if(teamleader == '') {
            alert('Please enter team leader.');
            return false;
        }else if(teamleader.length > 2){
            alert('Please enter team leader less than 2.');
            return false;
        }

        // var teammember = $("#teammember").val();
        // if(teammember == '') {
        //     alert('Please enter team member.');
        //     return false;
        // }

        // var member = teammember.toString();
        // var teamem = member.split(',');
        // var j = 0;
        // for(var i = 0;i<tealead.length;i++){
        //     console.log(tealead[i]);
        //     var index = teamem.indexOf(tealead[i]);
        //     if(index != -1){
        //         j++;
        //     }
        // }

        // if(j!=0){
        //     alert('PLease select team member different from team leaders.');
        //     return false;
        // }
    }
</script>


<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>