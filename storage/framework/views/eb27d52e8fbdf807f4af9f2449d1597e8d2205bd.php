

<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>My Profile</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Elitehr</a>
                </li>
                <li class="breadcrumb-item"><a href="#">Profile</a>
                </li>
            </ol>
        </div>
    </div>
    <?php if(Session::has('success_msg')): ?>
        <div class="alert alert-success">
            <?php echo e(Session::get('success_msg')); ?>

        </div>
    <?php endif; ?>   
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                   <a id="reset" href="javascript:void(0);" onClick="reset();" class="btn btn-success">Reset Password</a>
                </div>
                       
                <div class="card-block">
                   <span id="failuremsg" style="color:green;"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<script>
function reset(){
    $.ajax({
        url: "<?php echo e(route('reset')); ?>",
        type: "GET",
        data: '',
        success: function(response) {
           if(response == 1){
              $("#failuremsg").html('A link has been sent to your mail.Please go through your mail.'); 
              setTimeout(function(){$('#failuremsg').fadeOut();}, 5000);
           }
        },
        error: function(xhr) {
            alert("An error occured: " + xhr.status + " " + xhr.statusText);
        }
    });
}
</script>


<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>