<!DOCTYPE html>
<html lang="en">

<head>
 <?php echo $__env->make('Emp.layouts.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body class="sidebar-mini fixed" onload="show();">
<?php echo $__env->make('Emp.layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <!-- Sidebar Menu-->
<?php echo $__env->make('Emp.layouts.nav', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Sidebar chat end-->

<div class="content-wrapper">
        <?php echo $__env->yieldContent('content'); ?>
</div>
<?php echo $__env->make('Emp.layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
$('#summary').summernote({

});

</script>

</body>


</html>