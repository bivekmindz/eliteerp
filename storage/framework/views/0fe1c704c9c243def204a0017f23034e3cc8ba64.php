<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        
        <style>
            .autogen{    width: 100%;
    float: left;
    max-height: 137px;
    overflow-y: auto;
    z-index: 999;
    background-color: #fff;}
    .autogen ul {padding: 0px;
    line-height: 35px; border: 1px solid  #e5e5e5;}
     .autogen ul li {padding: 0px 8px; border-bottom: 1px solid #e5e5e5;}
     .autogen ul li:last-child{ border-bottom:none;}
        </style>
           
           <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
           <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>  
        
        <div class="row">
            <div class="main-header">
                <h4>Lunch Break Report</h4>
             
            </div>
        </div>



       
            <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Lunch Break Report</h5>
                           
                        </div>
                        <div class="card-block">


                              <div class="card-block">
                                 <form action=""  method="get" enctype="multipart/form-data"  autocomplete="off">
                                <div class="row">

     <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">User </label>
                                        <div class="col-sm-8">
   
         
            <input type="text" name="user" id="country" class="form-control" placeholder="Enter user Name"  autocomplete="off" />  
                <div id="countryList" class="autogen"></div> 
          
          
          </div>
                                    </div>
                                </div>


 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Start Date </label>
                                        <div class="col-sm-8">
                                            <input class="form-control" type="text" value="" id="dojnew" name="doj" class="document"/> </div>
                                    </div>
                                </div>

 <div class="col-lg-6">
                                <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">End Date </label>
                                        <div class="col-sm-8">
                                             <input class="form-control" type="text" value="" id="doend" name="doend" class="document"/>
                                    </div>
                                </div>
                                
                                

                                 <div class="col-lg-6">
                                 <div class="form-group row">

                                        <div class="col-sm-12">
                                        
                                        </div>
                                        
                                          <div class="col-sm-12">
                                       
                                        </div>
                                    </div>
                                    
                                  
                                 
                                    </div>
                                 </div>

                                </div>



  <div class="md-input-wrapper">
                                <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>
                                
                                <a href="lunchbreakreports"  class="btn btn-primary waves-effect waves-light">Cancel</a>
                            </div>




                                 



                                </form>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
              




<table width="100%" border="1" cellspacing="0" cellpadding="0">

   <tr>
                     <th>Employee Name</th>
                     <th>Break Start</th>
                     <th>End Time</th>
                     <th>Total Break Time</th>
                     <th>Date</th>
                 </tr>

 <?php if(!empty($data)): ?>
            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php
if(empty($val->clientstart_backtime)){
                $clientstart_backtime = 'still has not come.';
                $clientstart_breaktime=date('Y-m-d H:i:s',$val->clientstart_breaktime);
                $breakdelay = '';
            }else{
                $differenceTime = ($val->clientstart_backtime - $val->clientstart_breaktime);
                $diffTimeMinutes = number_format((float)($differenceTime/60), 2, '.', '');
                $clientstart_backtime = date('Y-m-d H:i:s',$val->clientstart_backtime);
                   $clientstart_breaktime=date('Y-m-d H:i:s',$val->clientstart_breaktime);
                $breakdelay = $diffTimeMinutes.' minutes';
            }
           
            ?>
          <tr class="table-active">
                        <td><?php echo e($val->name); ?></td>
                        <td><?php echo e($clientstart_breaktime); ?></td>
                        <td><?php echo e($clientstart_backtime); ?></td>
                        <td><?php echo e($breakdelay); ?></td>
                          <td><?php echo e($val->created_at); ?></td>
                    </tr>
 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endif; ?>
            
              </table>



                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
             </div>
             
             
             
            
            </div>
            
            <div class="container">
   <div class="modal fade" id="myModalnnn" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Resumes</h4>
        </div>
           <div class="modal-body" >
       <table width="90%" border="1" cellspacing="0" cellpadding="0">
           <thead>
           <tr>
           <th> Name</th><th>Mobile Id</th>   <th>Email Id</th> <th>Resume Name</th> <th>Created On</th>
            </tr>
            </thead>
            <tbody id="recruiter">
          
            </tbody>
             
            
        </table>
        </div>   
      </div>      
    </div>
  </div> 
</div>



        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Jd Position Details</h4>
                    </div>
                    <div class="modal-body" id="positionJd">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <script>
            function upload_position_jd(clientjob_id) {
                $.ajax({
                    url: 'adminpositionjd',
                    data: { id:clientjob_id },
                    success: function(result){
                        $("#positionJd").html(result);
                    }
                });
                $('#myModal').modal('show');
            }
    </script>
     <script>
     
function getresumelistreport(fk_id,fk_posid,fk_from,fk_to)
{
    var useridd = fk_id;
    var position_id = fk_posid;
    var fromdate = fk_from;
    var todate = fk_to;

    $.ajax({
        type:"GET",
        url:"getresumesreport/" + useridd + "/"  + position_id + "/" + fromdate + "/" + todate ,
        data:'',
        success: function(data){
          //  alert(data);
            $("#recruiter").empty();
            $("#recruiter").append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
    });
}    </script>

 <script>  
 $(document).ready(function(){  
      $('#country').keyup(function(){  
           var query = $(this).val();  
           if(query != '')  
           {  
                  $.ajax({
                      type: 'POST',
                      url: '<?php echo e(URL::route('searchusers')); ?>',
                      data:{
                        '_token': "<?php echo e(csrf_token()); ?>",
                        'query':query,
                      },
                      success: function(data){
                      // alert(data);
                         $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                      }
                    });
                    
                    
              /*  $.ajax({  
                     method:"POST",  
                     url:"searchusers",  
                    
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                     }  
                });  */
           }  
      });  
      $(document).on('click', 'li', function(){  
           $('#country').val($(this).text());  
           $('#countryList').fadeOut();  
      });  
 });  
 </script>  
        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
         rel = "stylesheet">

    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>


<script>
     $(document).ready(function () {  
          $('#doend').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>
<script>
     $(document).ready(function () {  
          $('#dojnew').datepicker({
              dateFormat: "yy-mm-dd"
          });  
      
      });
</script>


</div>

<script src="<?php echo e(URL::asset('js/jquery-ui.min.js')); ?>"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#ajaxhr').multiselect({
            enableCaseInsensitiveFiltering:true,
        includeSelectAllOption: true,
        buttonWidth: 250,
        enableFiltering: true
    });
    });

    $(document).ready(function() {
        $('#boot-multiselect-demo').multiselect({
            enableCaseInsensitiveFiltering:true,
        includeSelectAllOption: true,
        buttonWidth: 250,
        enableFiltering: true
    });
    });
</script>
<script>
    window.ParsleyConfig = {
        errorsWrapper: '<div></div>',
        errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>',
        errorClass: 'has-error',
        successClass: 'has-success'
    };
</script>

<script type="text/javascript">
    $(function() {
        $('.multiselect-ui').multiselect({
            includeSelectAllOption: true
        });
    });
</script>
<script type="text/javascript">
$(".one-click").click(function(){
    $(".hide_css").slideToggle(700);
});
</script>
<script src="http://115.124.98.243/~elitehr/js/sel.js"></script>
 <script>
                $(function()
                {
                    $(document).on('click', '.btn-add', function(e)
                    {
                        e.preventDefault();

                        var controlForm = $('.controls form:first'),
                            currentEntry = $(this).parents('.entry:first'),
                            newEntry = $(currentEntry.clone()).appendTo(controlForm);

                        newEntry.find('input').val('');
                        controlForm.find('.entry:not(:last) .btn-add')
                            .removeClass('btn-add').addClass('btn-remove')
                            .removeClass('btn-success').addClass('btn-danger')
                            .html('<span class="icofont icofont-minus"></span>');
                    }).on('click', '.btn-remove', function(e)
                    {
                        $(this).parents('.entry:first').remove();

                        e.preventDefault();
                        return false;
                    });
                });

            </script>
            
            <script>
                $(function()
                {
                    $(document).on('click', '.btn-add', function(e)
                    {
                        e.preventDefault();

                        var controlForm = $('.controls table:first'),
                            currentEntry = $(this).parents('.entry:first'),
                            newEntry = $(currentEntry.clone()).appendTo(controlForm);

                        newEntry.find('input').val('');
                        controlForm.find('.entry:not(:last) .btn-add')
                            .removeClass('btn-add').addClass('btn-remove')
                            .removeClass('btn-success').addClass('btn-danger')
                            .html('<span class="icofont icofont-minus"></span>');
                    }).on('click', '.btn-remove', function(e)
                    {
                        $(this).parents('.entry:first').remove();

                        e.preventDefault();
                        return false;
                    });
                });

            </script>

<script>
    $('#summernote').summernote('code');
</script>

<script>
$(".flo").click(function(){
    $(".hid").slideToggle(700);
});
</script>

<script>
$(".op").click(function(){
    $(".dt").slideToggle(700);
});
</script>



<?php $__env->stopSection(); ?>




<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>