<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row">
            <!-- Textual inputs starts -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Employee Excel Upload</h5>
                    </div>
                    <?php if(Session::has('success_msg')): ?>
                        <div class="alert alert-success">
                            <?php echo e(Session::get('success_msg')); ?>

                        </div>
                    <?php endif; ?>
                    <?php if(Session::has('error_msg')): ?>
                        <div class="alert alert-danger">
                            <?php echo e(Session::get('error_msg')); ?>

                        </div>
                    <?php endif; ?>
                    <!-- end of modal -->
                    <div class="card-block">
                            <div class="row">
                               <div class="col-lg-6">
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label"></label>
                                        <div class="col-sm-8">
                                            <a href="<?php echo e(URL::to('admin/exceldownload')); ?>">
                                                <button type="button" class="btn  btn-success  waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel">Download Excel
                                                    <span class="badge"><i class="icofont icofont-file-excel"></i></span>
                                                </button>
                                            </a>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <form action="<?php echo e(route('excelup')); ?>"  method="post" enctype="multipart/form-data">
                                            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Upload Excel</label>
                                            <div class="col-sm-8">
                                               <!-- <label for="file" class="custom-file">
                                                    <input type="file" id="file" name="upfile"> 
                                                    <span class="custom-file-control"></span>
                                                </label> -->
                                                   <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                   <input type="file" id="file" name="upfile">
                                                <span id="lblError" style="color: red;"></span>
                                            </div>
                                    </div>
                                </div>
                            </div>


                            <div class="md-input-wrapper">
                                <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light">Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Textual inputs ends -->
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">Employee List</h5>
                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 table-responsive">
                                <table class="table" id="search_filter">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Name</th>
                                        <th>Contact Number</th>
                                        <th>Email</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $i = 0;
                                     ?>
                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="table-active">
                                        <td><?php echo e(++$i); ?></td>
                                        <td><?php echo e($val->name); ?></td>
                                        <td><?php echo e($val->emp_contactno); ?></td>
                                        <td><?php echo e($val->email); ?></td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>




    </div>


    <script type="text/javascript">
    $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
    });
</script>
<script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script> 


<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>