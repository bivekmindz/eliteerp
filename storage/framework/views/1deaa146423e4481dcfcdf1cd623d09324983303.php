<?php $__env->startSection('content'); ?>


<style type="text/css">
  .green-o{    color: #03561e;
    font-weight: 600; }
   .red-o{     color: #9a0f0f;
    font-weight: 600;}
</style>
<!-- Container-fluid starts -->
<!-- Main content starts -->
<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>Shortlist Profile</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href=""><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="">Positions</a>
                </li>
                <li class="breadcrumb-item"><a href="">Shortlisting of CV</a>
                </li>
            </ol>
        </div>



    </div>

    <div class="row">

        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">SPOC Shortlisting</h5>

                </div>
                <?php if(Session::has('success')): ?>
                <div class="alert alert-success">
                    <?php echo e(Session::get('success')); ?>

                </div>
                <?php endif; ?>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Position Name</th>
                                         <th>Company Name</th>
                                        <th>No. Of Positions</th>
                                         <th>Spoc Name</th>
                                        <th>Total Uploaded Profile</th>
                                        <th>View</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody>

                                    <?php 
                                    $i=1;
                                     ?>
                                    <?php $__currentLoopData = $pos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr >
                                        <td><?php echo e($i++); ?></td>
                                        <td><?php echo e($value->clientjob_title); ?>

                                            <input type="hidden" value="<?php echo e($value->clientjob_title); ?>"  id="pos">
                                        </td>
                                         <td><?php echo e($value->comp_name); ?></td>
                                        <td><?php echo e($value->clientjob_noofposition); ?></td>
                                          <td><?php echo e($value->name); ?></td>

                                        <td><?php echo e($value->total_upload_profile); ?></td>

                                        <td><button type="button" value="<?php echo e($value->position_id); ?>" class="det_css one-click " onclick="getcandidatedetails(this.value),getposition('<?php echo $value->clientjob_title ?>')" ><span class="icofont icofont-eye-alt"></span></button> </td>



                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="row">

        <div class="col-md-12">

            <div class="card hide_css">
                <div class="card-header">
                    <h5 class="card-header-text">All Candidate List</h5>

                </div>
                <?php echo Form::open([ 'action'=>'Emp\CVController@savecandidate', 'method'=>'post', 'files'=>true ]); ?>

                <div class="card-block">
                    <div class="row" >
                        <div class="col-sm-12 table-responsive">
                            <table class="table"  >
                                <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Candidate's Name</th>
                                        <th>CV Uploaded Time</th>
                                        <th>Mobile Number</th>
                                        <th>Download CV</th>
                                        <th>Qulification</th>
                                        <th>Total Experience</th>
                                        <th>Domain Experience </th>
                                        <th>Primary Skill</th>
                                        <th>Secondary Skill</th>
                                        <th>Current ORG</th>
                                        <th>Current CTC </th>
                                        <th>Expected CTC</th>
                                        <th>NP</th>
                                        <th>Reason For Change</th>
                                        <th>Skills For Rating</th>
                                        <th>Relocation</th>
                                        <th>DOJ</th>
                                        <th>Remark</th>
                                        <th>Status</th>


                                    </tr>
                                </thead>
                                <tbody id="candilist">

                                </tbody>
                                <input type="hidden" value="" id="poid" name="poid">
                            </table>

                        </div>


                    </div>
                    <div class="md-input-wrapper">

                        <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit">
                    </input>
                </div>
            </div>
            <?php echo Form::close(); ?>

        </div>

    </div>

</div>





</div>
<script>

   function getposition(id){
    var ids  = id;
    $('#poid').val(ids);

}

function getcandidatedetails(val)
{


 var  id=val;

 $.ajax({
    url: 'getcandidatedetails/'+id,
    type:'GET',
    error: function(xhr) {
        alert("An error occured: " + xhr.status + " " + xhr.statusText);
    },
    success: function(data)
    {
     var data1= JSON.parse(data);
                   // console.log(data.length);

                   var trHTML = '';
                   var j=1;


                  //  $.each(data, function (i, data) {
                    for (i = 0; i < data1.length; i++) {
                       var str = data1[i]["created_at"];
                       var str = str.split(" ");
                       var aa =str[0].split("-");

                       var con="<?php echo e(config('constants.APP_URLS')); ?>";

                       var url=con+'/storage/app/uploadcv/'+aa[0]+"/"+aa[1]+"/"+aa[2]+"/"+data1[i]["cv_name"];


if(data1[i]["cv_status"]=='3')
{
data1[i]["cv_nameaa"]="<span class='red-o'>Rejected</span>";
}
if(data1[i]["cv_status"]=='2')
{
data1[i]["cv_nameaa"]="<span class='green-o'>Send To Client</span>";
}
if(data1[i]["cv_status"]=='1')
{
  data1[i]["s1"]=1;
  data1[i]["s2"]=2;
  data1[i]["s3"]=3;
data1[i]["cv_nameaa"]='<select name= "checkbox_data[]"><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s1"]+'>Not Seen</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s2"]+'>Send To Client</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s3"]+'>Rejected</option></select>';
}

if(data1[i]["remark"]=='Null')
{
data1[i]["remarka"]='';
}
else
{
    data1[i]["remarka"]=data1[i]["remark"];
}



                       trHTML += "<tr>" +
                       "<td>"+j+++"</td>" +
                       "<td>"+data1[i]["candidate_name"]+"</td>" +
                        "<td>"+data1[i]["cv_uploadtime"]+"</td>" +
                       "<td>"+data1[i]["candidate_mob"]+"</td>" +
                       '<td><a href='+url+' download>   ' +'Download </a></td>' +
                       "<td>"+data1[i]["highest_qulification"]+"</td>" +
                       "<td>"+data1[i]["total_exp"]+"</td>" +
                       "<td>"+data1[i]["domain_exp"]+"</td>" +
                       "<td>"+data1[i]["primary_skill"]+"</td>" +
                       "<td>"+data1[i]["secondary_skill"]+"</td>" +
                       "<td>"+data1[i]["current_org"]+"</td>" +
                       "<td>"+data1[i]["current_ctc"]+"</td>" +
                       "<td>"+data1[i]["expected_ctc"]+"</td>" +
                       "<td>"+data1[i]["np"]+"</td>" +
                       "<td>"+data1[i]["reason_for_change"]+"</td>" +
                       "<td>"+data1[i]["communication_skills_rating"]+"</td>" +
                       "<td>"+data1[i]["relocation"]+"</td>" +
                      "<td>"+data1[i]["doj"]+"</td>" +
                    // "<td>"+!empty(data1[i]["doj"]) ? data1[i]["doj"] :' ' +"</td>" +
                       "<td>"+data1[i]["remarka"]+"</td>" +
                      "<td>"+data1[i]["cv_nameaa"]+"</td>" +
                       '</tr>';
                   }
             //      });

             $('#candilist').html(trHTML);




         }




     });

}
</script>
<script type="text/javascript">

    $("input:checkbox:not(:checked)").each(function () {
        alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
    });

</script>




<?php $__env->stopSection(); ?>
<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>