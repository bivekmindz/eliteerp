<?php $__env->startSection('content'); ?>
<style>
.error{
  color:red;
}
.form-control.error{
  color:black;
}
</style>


  <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
  <link href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">






  <div class="container-fluid">
    <div class="row">
      <div class="main-header" style="margin-top: 0px;">
       <h4>Edit Position</h4>
       <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
        <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
        </li>
        <li class="breadcrumb-item"><a href="javascript:void(0)">Position</a>
        </li>
        <li class="breadcrumb-item"><a href="">Edit Position</a>
        </li>
      </ol>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="card">
       <div class="card-header"><h5 class="card-header-text">Edit Position Form</h5>

       </div>

       <div class="card-block">
         <!--<form method="POST" action="http://192.168.1.196:8080/addposition" accept-charset="UTF-8" name="position" enctype="multipart/form-data"><input name="_token" type="hidden" value="lJcKadyIetHS6t2Bm383bMwGRXkQrWvRHReYqZh2">-->
          <!--  <form method="post" name="position" action="http://192.168.1.196:8080/editposition/" enctype="multipart/form-data" novalidate="novalidate"> -->

            <form method="post" name="position" action="<?php echo e(URL::to('editposition/'.($client_details[0]->clientjob_id))); ?>" enctype="multipart/form-data" novalidate="novalidate">

              <div class="row">
               <input type="hidden" name="_token" value="lJcKadyIetHS6t2Bm383bMwGRXkQrWvRHReYqZh2">

               <div class="col-lg-6">
                 <div class="form-group row">
                  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Type<span class="error"> * </span> </label>
                  <div class="col-sm-8">
                   <select class="form-control" id="dept" name="dept">
                     <option value="">Please Select Department</option>
                     <option value="1" <?php if($client_details[0]->clientjob_deptid == '1'): ?> Selected <?php endif; ?> >IT</option>
                     <option value="2" <?php if($client_details[0]->clientjob_deptid == '2'): ?> Selected <?php endif; ?> >Non-IT</option>
                     <option value="3" <?php if($client_details[0]->clientjob_deptid == '3'): ?> Selected <?php endif; ?> >Sales</option>



                   </select>
                 </div>
               </div>
             </div>
  <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
 

             <div class="col-lg-6">
               <div class="form-group row">
                <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Company Name<span class="error"> * </span></label>
                <div class="col-sm-8">
                  <select class="form-control" id="compname" name="compname">
                   <option value="" id="jobtitle">Please Select Company</option>
                   <?php $__currentLoopData = $client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   <option value="<?php echo e($value->fk_clientid); ?>"  <?php if($client_details[0]->clientjob_compid == $value->fk_clientid): ?> Selected <?php endif; ?> ><?php echo e($value->comp_name); ?></option>
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                 </select>
               </div>
             </div>
           </div>

         </div>

         <div class="row">
          <div class="col-lg-6">
           <div class="form-group row">
            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Position Title <span class="error"> * </span></label>
            <div class="col-sm-8">
              <input class="form-control" type="text" value="<?php echo e((Input::old('clientjob_title')) ? Input::old('clientjob_title') : $client_details[0]->clientjob_title); ?>" id="jobtitle" placeholder="Position Title" name="jobtitle">
            </div>
          </div>
        </div>


      </div>

      <div class="row">
        <div class="col-lg-6">
         <div class="form-group row">
          <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Number of Positions<span class="error"> * </span> </label>
          <div class="col-sm-8">
           <input class="form-control" type="number" maxlength="11" value="<?php echo e((Input::old('clientjob_noofposition')) ? Input::old('clientjob_noofposition') : $client_details[0]->clientjob_noofposition); ?>" placeholder="Number of Positions" id="no_of_position" name="noofpos">
         </div>
       </div>
     </div>
     <div class="col-lg-6">
       <div class="form-group row">
        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">

         <div class="checkbox-color checkbox-success ">
          <input id="checkbox3" type="checkbox" name="drive">
          <label for="checkbox3">
           Drive
         </label>
       </div>
     </label>
     <div class="col-sm-8">
      <div class="form-group">
        <div class="input-group date" id="drivedate">
          <input type="text" class="form-control" placeholder="Drive Date" id="drivedate">
          <span class="input-group-addon">
            <span class="icofont icofont-ui-calendar"></span>
          </span>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<!--Line 1 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Experience<span class="error"> * </span> </label>
    <div class="col-sm-8">
     <input class="form-control" type="text" value="<?php echo e((Input::old('experience')) ? Input::old('experience') : $client_details[0]->experience); ?>" id="noofpos" name="experience" placeholder="Experience">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Relevant/Domain Experience</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" value="<?php echo e((Input::old('  domain_experience')) ? Input::old(' domain_experience') : $client_details[0]->  domain_experience); ?>" id="noofpos" name="domain_experience" placeholder="Domain Experience">
 </div>
</div>
</div>
</div>
<!--Line 2 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Primary Skills</label>
    <div class="col-sm-8">
     <input class="form-control" type="text" value="<?php echo e((Input::old('primary_skills')) ? Input::old('primary_skills') : $client_details[0]->primary_skills); ?>" id="noofpos" name="primary_skill" placeholder=" Primary Skills">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Secondary Skills</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" value="<?php echo e((Input::old('secondary_skills  ')) ? Input::old('secondary_skills  ') : $client_details[0]->secondary_skills); ?>" id="noofpos" name="secondary_skill" placeholder=" Primary Skills">
 </div>
</div>
</div>
</div>

<!--Line 3 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Reporting Person Name</label>
    <div class="col-sm-8">
     <input class="form-control" type="text" value="<?php echo e((Input::old('rpn ')) ? Input::old('rpn ') : $client_details[0]->rpn); ?>" id="" name="rpn" placeholder="Reporting Person Name">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Reporting Person Designation</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" name="rpd" value="<?php echo e((Input::old(' rpd')) ? Input::old(' rpd') : $client_details[0]->  rpd); ?>" placeholder="Reporting Person Designation">
 </div>
</div>
</div>
</div>
<!--Line 4 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Reporting Person Phone</label>
    <div class="col-sm-8">
     <input class="form-control" type="number" maxlength="11" name="rpp" value="" placeholder="Reporting Person Phone">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Reporting Person Email</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" name="rpe" value="<?php echo e((Input::old('rpe')) ? Input::old('rpe') : $client_details[0]->rpe); ?>" placeholder="Reporting Person Email">
 </div>
</div>
</div>
</div>
<!--Line 5 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">POC Name</label>
    <div class="col-sm-8">
     <input class="form-control" type="text" name="pocn" value="<?php echo e((Input::old('  pocn')) ? Input::old('  pocn') : $client_details[0]->   pocn); ?>" placeholder="POC Name">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">POC Designation</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" name="pocd" value="<?php echo e((Input::old('  pocd')) ? Input::old('  pocd') : $client_details[0]->   pocd); ?>" placeholder="POC Designation">
 </div>
</div>
</div>
</div>
<!--Line 6 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">POC Phone</label>
    <div class="col-sm-8">
     <input class="form-control" type="number" maxlength="11" name="pocp" value="<?php echo e((Input::old(' pocp')) ? Input::old(' pocp') : $client_details[0]->  pocp); ?>" placeholder="POC Phone">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">POC Email</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" name="poce" value="<?php echo e((Input::old('  poce')) ? Input::old('  poce') : $client_details[0]->   poce); ?>" placeholder="POC Email">
 </div>
</div>
</div>
</div>

<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Hiring Manger Name</label>
    <div class="col-sm-8">
     <input class="form-control" type="text" name="hmn" value="<?php echo e((Input::old('   hmn')) ? Input::old('   hmn') : $client_details[0]->    hmn); ?>" placeholder="Hiring Manger Name">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Hiring Manger Designation</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" name="hmd" value="<?php echo e((Input::old('   hmd')) ? Input::old('   hmd') : $client_details[0]->    hmd); ?>" placeholder="Hiring Manger Designation">
 </div>
</div>
</div>
</div>
<!--Line 7 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form -group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Hiring Manger Phone</label>
    <div class="col-sm-8">
     <input class="form-control" type="number" maxlength="11" name="hmp" value="<?php echo e((Input::old(' hmp')) ? Input::old(' hmp') : $client_details[0]->  hmp); ?>" placeholder="Hiring Manger Phone">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form -group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Hiring Manger Email</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" name="hme" value="<?php echo e((Input::old(' hme')) ? Input::old(' hme') : $client_details[0]->  hme); ?>" placeholder="Hiring Manger Email">
 </div>
</div>
</div>
</div>
<!--Line 8 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
   <div class="col-sm-4">
      <label for="example-text-input" class="col-form-label form-control-label">Drive</label>
     <input type="radio"  value="Drive" name="report" <?php if($client_details[0]->drive_shortlist == 'Drive'): ?> checked <?php endif; ?> >
   </div>
   <div class="col-sm-4">
    <label for="example-text-input" class="col-form-label form-control-label">Shortlist</label>
    <input  type="radio" value="Shortlist" name="report" <?php if($client_details[0]->drive_shortlist == 'Shortlist'): ?> checked <?php endif; ?> >
  </div>
   <div class="col-sm-4">
      <label for="example-text-input" class="col-form-label form-control-label">Daily Line Up</label>
      <input  type="radio" value="DaliyLineUp" name="report" <?php if($client_details[0]->drive_shortlist == 'DaliyLineUp'): ?> checked <?php endif; ?> >
  </div>
</div>


</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Severity of the requirements</label>
  <div class="col-sm-8">
   <select class="form-control" name="severity">
    <option value="Low" <?php if($client_details[0]->severity == 'Low'): ?> Selected <?php endif; ?>>Low</option>
    <option value="Medium" <?php if($client_details[0]->severity == 'Medium'): ?> Selected <?php endif; ?>>Medium</option>
    <option value="High" <?php if($client_details[0]->severity == 'High'): ?> Selected <?php endif; ?>>High</option>
  </select>
</div>
</div>
</div>
</div>

<div class="row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Add JD</label>
</div>

<div class="row">
  <textarea name="upload_position_jd" id="summernote" class="summernote form-control"> <?php echo e((Input::old(' clientjob_jobdescription')) ? Input::old(' clientjob_jobdescription') : $client_details[0]->  clientjob_jobdescription); ?></textarea>
</div>   

<!-- </form> -->

</div>

<div class="md-input-wrapper">
  <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
  </button>
</div>
</form>

</div>
</div>
</div>
<!-- Textual inputs ends -->
</div>



<style>
input[type=number]::-webkit-inner-spin-button {
  -webkit-appearance: none!important;
}
</style>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>

<script>
  $('# '). ();

    // $('#summernote').summernote({
    //     placeholder: 'Hello bootstrap 4',
    //     tabsize: 2,
    //     height: 100
    //   });
  </script>
  <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
  <script>
    $(function() {
      $("form[name='position']").validate({
        ignore: [],
        rules: {
          dept: "required",
          compname: "required",
          jobtitle: "required",
          noofpos: "required",
          upload_position_jd: "required",
        },
        messages: {
          dept: "Please enter department.",
          compname: "Please enter company name.",
          jobtitle: "Please enter company url.",
          noofpos: "Please enter number of position.",
          upload_position_jd: "Please enter upload position jd.",
        },
        submitHandler: function(form) {
          form.submit();
        }
      });
    });

    $(document).ready(function() {
      $("#noofpos").keydown(function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
          (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
          (e.keyCode >= 35 && e.keyCode <= 40)) {
         return;
     }
     if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  });
    });
  </script>
  <?php $__env->stopSection(); ?>
<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>