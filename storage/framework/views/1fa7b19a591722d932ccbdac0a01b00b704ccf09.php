<?php $__env->startSection('content'); ?>

<?php $counts = $singnature[0]->count; ?>



<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>Dashboard</h4>
        </div>
    </div>
<?php $role = explode(',', Auth::user()->emp_role);?>

    <!-- 4-blocks row start -->
    <div class="row m-b-30 dashboard-header">
    <?php if(in_array(1, $role)): ?>
        <div class="col-lg-4 col-sm-6">
            <div class="dashboard-primary bg-primary">
                <div class="sales-primary">
                    
                    <div class="top-icon">
                      
                    <i class="icon-bubbles"></i>
                    <h1><?php echo e($tot_client[0]->clientcount); ?></h1>
                    
                    <p>TOTAL CLIENTS</p>
                    </div>
                   
                </div>

               
                <div class="bg-dark-primary">
                 
                    <p><a href="<?php echo e(URL::to(''.'clientlists')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">

                <div class="top-icon">

                 <i class="icon-basket-loaded"></i>
                 <h1><?php echo e($tot_assignclient[0]->assignclientcount); ?></h1>
                 <p>TOTAL ASSIGN SPOC TO CLIENTS</p>


                </div>
                    
                    
                </div>
                <div class="bg-dark-warning">
                    
                    <p><a href="<?php echo e(URL::to(''.'clientspoc')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                 <div class="top-icon">
                      
                   <!-- <i class="icon-clock"></i> -->
                    <h1></h1>
                    
                    <p>Client Listing</p>
                    </div>
                    <div class="dast-bot-ti">
                        <table>
                            <thead>
                            <tr>
                                
                                <th>Company Name</th>
                                <th>Created on</th>
                                 
                            </tr>
                            </thead>
                            <tbody>

                            <?php 
                            $i=1;
                            $temp = $adminclientdata ;
                             ?> 
                            <?php $__currentLoopData = $adminclientdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr>
                                <td><?php echo e($c->comp_name); ?></td>
                                <td><?php echo e($c->created_at); ?></td>
                               </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                                        
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <!-- <p ><a href="<?php echo e(URL::to(''.'listallposition')); ?>">VIEW DETAILS</a></p> -->
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if(in_array(2, $role)): ?>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-success dashboard-success">
                <div class="sales-success">



                   <div class="top-icon">
                      
                    <i class="icon-speedometer"></i>
                    <h1><?php echo e($tot_spoc_assignclient[0]->spocclientcount); ?></h1>
                    
                    <p>TOTAL ASSIGNED CLIENTS</p>
                    </div>


                   
                   
                </div>
                <div class="bg-dark-success">
                    
                    <p><a href="<?php echo e(URL::to(''.'assignclient')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">


                   <div class="top-icon">
                      
                    <i class="icon-clock"></i>
                    <h1><?php echo e($tot_position[0]->spoctotpos); ?></h1>
                    
                    <p>TOTAL ADDED POSITIONS</p>
                    </div>

                    
                   
                </div>
                <div class="bg-dark-facebook">
                   
                    <p><a href="<?php echo e(URL::to(''.'positionlist')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">
                 <div class="top-icon">
                      
                   <i class="icon-basket-loaded"></i>
                    <h1><?php echo e($tot_assignpostion[0]->spoctotassignpos); ?></h1>
                    
                    <p>TOTAL POSITION ALLOCATE TO RECRUITER</p>
                    </div>
                   
                   
                </div>
                <div class="bg-dark-warning">
                   
                    <p><a href="<?php echo e(URL::to(''.'viewallassignedrecuiterposition')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                 <div class="top-icon">
                      
                   <!-- <i class="icon-clock"></i> -->
                    <h1></h1>
                    
                    <p>Position Listing</p>
                    </div>
                    <div class="dast-bot-ti">
                        <table>
                            <thead>
                            <tr>
                                
                                <th>Position Name</th>
                                <th>Created on</th>
                                 
                            </tr>
                            </thead>
                            <tbody>

                            <?php 
                            $i=1;
                            $temp = $tot_positionlist ;
                             ?> 
                            <?php $__currentLoopData = $tot_positionlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr>

                                
                                <td><?php echo e($c->clientjob_title); ?></td>
                                <td><?php echo e($c->created_at); ?></td>
                               </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                                        
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <!-- <p ><a href="<?php echo e(URL::to(''.'listallposition')); ?>">VIEW DETAILS</a></p> -->
                </div>
            </div>
        </div>
        
    <?php endif; ?>
    <?php if(in_array(3, $role)): ?>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">

                  <div class="top-icon">
                      
                      <i class="icon-basket-loaded"></i>
                    <h1><?php echo e($tot_assignpostion_rec[0]->totassignpos_rec); ?></h1>
                    
                    <p>TOTAL POSITION ASSIGNED TO RECRUITER BY SPOC</p>
                    </div>
                 
                  
                </div>
                <div class="bg-dark-warning">
                  
                    <p><a href="<?php echo e(URL::to(''.'positionassignedreqbyspoc')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-success dashboard-success">
                <div class="sales-success">
 <div class="top-icon">
                      
                    <i class="icon-speedometer"></i>
                    <h1><?php echo e($tot_recuit_cv[0]->recuit_cv); ?></h1>
                    
                    <p>TOTAL CV</p>
                    </div>


                  
                </div>
                <div class="bg-dark-success">
                   
                    <p><a href="<?php echo e(URL::to(''.'cvlist')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                  <div class="top-icon">
                      
                   <i class="icon-clock"></i>
                    <h1><?php echo e($tot_recuit_cv_submission[0]->recuit_cv_submission); ?></h1>
                    
                    <p>TOTAL SUBMISSION</p>
                    </div>
                    
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <p><a href="<?php echo e(URL::to(''.'submissionlist')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if(in_array(4, $role)): ?>
    <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">
                  

                   <div class="top-icon">
                      
                   <i class="icon-basket-loaded"></i>
                    <h1><?php echo e($admin_tot_client[0]->adminclient); ?></h1>
                    
                    <p>TOTAL CLIENTS</p>
                    </div>

                    
                  
                </div>
                <div class="bg-dark-warning">
                    
                    <p ><a href="<?php echo e(URL::to(''.'admin/adminclient')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-success dashboard-success">
                <div class="sales-success">

                        <div class="top-icon">
                      
                   <i class="icon-speedometer"></i>
                    <h1><?php echo e($admin_tot_emp[0]->adminemp); ?></h1>
                    
                    <p>TOTAL EMPLOYEES</p>
                    </div>

                    
                    
                </div>
                <div class="bg-dark-success">
                  
                    <p ><a href="<?php echo e(URL::to(''.'admin/viewadminemployes')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">


                 <div class="top-icon">
                      
                 <i class="icon-clock"></i>
                    <h1><?php echo e($admin_tot_dept[0]->admindept); ?></h1>
                    
                    <p>TOTAL DEPARTMENTS</p>
                    </div>



                    
                   
                </div>
                <div class="bg-dark-facebook">
                    
                    <p><a href="<?php echo e(URL::to(''.'admin/admindepartment')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="dashboard-primary bg-primary">
                <div class="sales-primary">


                 <div class="top-icon">
                      
                   <i class="icon-bubbles"></i>
                    <h1><?php echo e($admin_tot_team[0]->adminteam); ?></h1>
                    
                    <p>TOTAL TEAMS</p>
                    </div>
                    
                    
                </div>
                <div class="bg-dark-primary">
                    
                    <p><a href="<?php echo e(URL::to(''.'admin/team')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                 <div class="top-icon">
                      
                   <i class="icon-clock"></i>
                    <h1><?php echo e($admin_tot_position[0]->adminposition); ?></h1>
                    
                    <p>TOTAL POSITIONS</p>
                    </div>

                   
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <p ><a href="<?php echo e(URL::to(''.'listallposition')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">


                  <div class="top-icon">
                      
                       <i class="icon-basket-loaded"></i>
                    <h1><?php echo e($admin_tot_recuit_cv[0]->admin_recuit_cv); ?></h1>
                    
                    <p>TOTAL SHORTLISTED CV</p>
                    </div>

                 
                   
                </div>
                <div class="bg-dark-warning">
                   
                    <p><a href="<?php echo e(URL::to(''.'shortlistcv')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>  

        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                 <div class="top-icon">
                      
                   <!-- <i class="icon-clock"></i> -->
                    <h1></h1>
                    
                    <p>Client Listing</p>
                    </div>
                    <div class="dast-bot-ti">
                        <table>
                            <thead>
                            <tr>
                                
                                <th>Company Name</th>
                                <th>Created on</th>
                                 
                            </tr>
                            </thead>
                            <tbody>

                            <?php 
                            $i=1;
                            $temp = $adminclientdata ;
                             ?> 
                            <?php $__currentLoopData = $adminclientdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr>

                                
                                <td><?php echo e($c->comp_name); ?></td>
                                <td><?php echo e($c->created_at); ?></td>
                               </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                                        
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <!-- <p ><a href="<?php echo e(URL::to(''.'listallposition')); ?>">VIEW DETAILS</a></p> -->
                </div>
            </div>
        </div>


        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                 <div class="top-icon">
                      
                   <!-- <i class="icon-clock"></i> -->
                    <h1></h1>
                    
                    <p>Position Listing</p>
                    </div>
                    <div class="dast-bot-ti">
                        <table>
                            <thead>
                            <tr>
                                
                                <th>Position Name</th>
                                <th>Created on</th>
                                 
                            </tr>
                            </thead>
                            <tbody>

                            <?php 
                            $i=1;
                            $temp = $admin_tot_positionlist ;
                             ?> 
                            <?php $__currentLoopData = $admin_tot_positionlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr>

                                
                                <td><?php echo e($c->clientjob_title); ?></td>
                                <td><?php echo e($c->created_at); ?></td>
                               </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                                        
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <!-- <p ><a href="<?php echo e(URL::to(''.'listallposition')); ?>">VIEW DETAILS</a></p> -->
                </div>
            </div>
        </div>



    <?php endif; ?>    
    </div>
  
    
   <!--  <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Client Listing</h5>
                           
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 ">
                                
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>S.N</th>
                                            <th>Category</th>
                                            <th>Company Name</th>
                                            <th>Contact Person Name</th>
                                            <th>Designation</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                             <th>Created by</th>
                                             
                                        </tr>
                                        </thead>
                                        <tbody>

                                           <?php 
                                $i=1;
                                $temp = $adminclientdata ;
                             ?> 
                                            <?php $__currentLoopData = $adminclientdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>


                                <td> <?php echo e($i++); ?></td>
                                <td> <?php if($c->client_catid==1): ?>
                                         <?php echo e("IT"); ?>

                                         <?php else: ?>
                                          <?php echo e("NON IT"); ?>

                                         <?php endif; ?>


                                </td>
                                <td><?php echo e($c->comp_name); ?></td>
                                <td><?php echo e($c->contact_name); ?></td>
                                <td><?php echo e($c->designation); ?></td>
                                <td><?php echo e($c->phone); ?></td>
                                <td><?php echo e($c->email); ?></td>
                                <td><?php echo e($c->name); ?></td>
                               </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        
                                        

                                        </tbody>
                                    </table>
                                </div>
                                </div>    
                            </div>
                        </div>
                    </div>
             </div>
            </div> -->
</div>
</div>
    <style>
        .input{
            border: 1px solid #9a9a9a;border-radius: 0px;
        }
        form label {
          display: inline-block;
          width: 100px;
      }
      form div {
          margin-bottom: 10px;
      }
      .error {
          color: red;
          margin-left: 5px;
      }
      .intro {
        border:1px solid red;
    }

    label.error {
      display: inline;
  }   
</style>




    <?php $x = 1;
    if($counts>0){?>
  <!-- Modal -->
  <div class="modal fade in" style="opacity: 1;display:block" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Please Provide information</h4>
      </div>
      <div class="modal-body">
          <form class="form-horizontal" action="dashboard" method="post" id="first_form">
            <div class="form-group">
              <label style="text-align:left"; class="control-label col-sm-4" for="email">Date Of Birth:</label>
              <div class="col-sm-8">

                  <input type="hidden" name="_token"  value="<?php echo e(csrf_token()); ?>">
                  <input type="text" required autocomplete ="off" class="form-control input" id="dob" placeholder="Enter Date of Birth" name="dob">
              </div>
          </div>
          <div class="form-group">
              <label style="text-align:left"; class="control-label col-sm-4" for="pwd">Date of Joining:</label>
              <div class="col-sm-8">          
                <input type="text" autocomplete="off" required class="form-control input" id="doj" placeholder="Enter Date Of Joining" name="doj">
            </div>
        </div>
        <div class="form-group">        
          <div class="col-sm-offset-2 col-sm-12"  style="margin-left:0px">
            <input type="submit" name="singnature" id="singnature" class="btn btn-success" style="float:right" >
        </div>
    </div>
</form>
</div>
<div class="modal-footer">

</div>
</div>

</div>
</div>
<?php }?>

<!--Model 2 Birth day -->
<style type="text/css">
    .birthday{display:block;opacity: 1;padding-top: 50px}
    .intro{display:none!important;}
    .modal-footer,.modal-header{ border:none!important; }
    .modal-header{padding: 0px!important}
</style>
<script>
$(document).ready(function(){
    $("#close").click(function(){
        $("#default-Modal").addClass("intro");
    });
});
</script>
    <?php
      // $id = Auth::user()->id;
      // $ser = "select count(id) as BirthDayGirl from users where emp_dob like '%".date('d-m')."%' and id='".$id."'";
      // $c = DB::select(DB::raw($ser));
      // $hbd = $c[0]->BirthDayGirl;
      if($hbd==1){
        $cookie_name = "BirthDay";
        $cookie_value = "Birthday";

      if(!isset($_COOKIE[$cookie_name])) {

        ?>
                            <div class="modal fade modal-flex birthday" id="default-Modal" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="background-image: url('');   ">
                                        <div class="modal-header">
                                            <h4 class="modal-title">&nbsp;</h4>
                                        </div>
                                        <div class="modal-body"  >
                                            <h4 style="font-family:'URW Chancery L',cursive;"> Team Elite Wishing you Happy Birth Day</h4>
                                        <div class="modal-footer">
                                            <button style="float:left" type="button" id="close" class="btn btn-primary waves-effect">Thank You</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

<?php
setcookie($cookie_name, $cookie_value, time() + (80000), "/"); // 86400 = 1 day
 }}
?>
<!--End Model two -->


<!--Model 3 Birth day -->
<style type="text/css">
    .birthdays{display:block;opacity: 1;padding-top: 50px}
    .intros{display:none!important;}
    .modal-footer,.modal-header{ border:none!important; }
    .modal-header{padding: 0px!important}
</style>
<script>
$(document).ready(function(){
    $("#closes").click(function(){
        $("#default-Modals").addClass("intros");
    });
});
</script>
    <?php

      // $date  = date('d-m-Y');
      // $date2  = date('d-m', strtotime($date));

      // $sqli = "select count(id) as CC from users where emp_doj like '%".$date2."%' and emp_doj < '".$date."' and id='".$id."'";
      // $querys = DB::select(DB::raw($sqli));
      // $han = $querys[0]->CC;

      if($han==1){
        $cookie_names = "Anversary";
        $cookie_values = "Anversary";

      if(!isset($_COOKIE[$cookie_names])) {

        ?>
                            <div class="modal fade modal-flex birthdays" id="default-Modals" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="background-image: url('');   ">
                                        <div class="modal-header">
                                            <h4 class="modal-title">&nbsp;</h4>
                                        </div>
                                        <div class="modal-body"  >
                                            <h4 style="font-family:'URW Chancery L',cursive;"> Team Elite  Wishing you Happy Anversary</h4>
                                        <div class="modal-footer">
                                            <button style="float:left" type="button" id="closes" class="btn btn-primary waves-effect">Thank You</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

<?php
setcookie($cookie_names, $cookie_values, time() + (80000), "/"); // 86400 = 1 day
 }}
?>

<!--Model 4 Birth day -->
<style type="text/css">
{display:block;opacity: 1;padding-top: 50px}
    .intross{display:none!important;}
    .modal-footer,.modal-header{ border:none!important; }
    .modal-header{padding: 0px!important}
</style>
<script>
$(document).ready(function(){
    $("#closess").click(function(){
        $("#default-Modalss").addClass("intross");
    });
});
</script>
    <?php

        $cookie_namess = "Wish";
        $cookie_valuess = "Wish";
         $time = "11:59";
         $timer = date('H:i', strtotime($time));
         $date = date('H:i');

      if(!isset($_COOKIE[$cookie_namess])){
        if($date < $timer){

        ?>
                            <div class="modal fade modal-flex birthdays" id="default-Modalss" tabindex="-1" role="dialog">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content" style="background-image: url('');   ">
                                        <div class="modal-header">
                                            <h4 class="modal-title">&nbsp;</h4>
                                        </div>
                                        <div class="modal-body"  >
                                            <h4 style="font-family:'URW Chancery L',cursive;"> Good Morning Have A Nice Day</h4>
                                        <div class="modal-footer">
                                            <button style="float:left" type="button" id="closess" class="btn btn-primary waves-effect">Thank You</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

<?php
setcookie($cookie_namess, $cookie_valuess, time() + (80000), "/"); // 86400 = 1 day
 }}
?>




<script>
    $(document).ready(function() {
      $('#singnature').click(function(e) {
        var dob = $('#dob').val();
        var doj = $('#doj').val();
        $(".error").remove();

        if (dob.length < 1) {
          $('#dob').addClass("intro");
          $('#dob').after('<span class="error">This field is required!  </span>');

      }
      if (doj.length < 1) {
          $('#doj').addClass("intro");
          $('#doj').after('<span class="error">This field is required!</span>');

      }

  })
});

    $(document).ready(function(){
        $("input").click(function(){
            $(this).removeClass("intro");
        });
    });


</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
   $( "#dob" ).datepicker({
       changeMonth: true,
       changeYear: true,
       yearRange: "-40:+0",
       //maxDate: 0,
       dateFormat: 'dd-mm-yy',
   });

   $( "#doj" ).datepicker({
       changeMonth: true,
       changeYear: true,
       yearRange: "-10:+0",
    //   maxDate: 0,
       dateFormat: 'dd-mm-yy',
   });
</script>

</body>
</html>


    <?php $__env->stopSection(); ?>
    
    
    
<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>