<?php $__env->startSection('content'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>Dashboard</h4>
        </div>
    </div>
<?php $role = explode(',', Auth::user()->emp_role);
    //dd(Auth::user()->emp_role);
  
    ?>
    <!-- 
        
    
    
    
    
     -->

    <!-- 4-blocks row start -->
    <div class="row m-b-30 dashboard-header">
    <?php if(in_array(1, $role)): ?>
        <div class="col-lg-4 col-sm-6">
            <div class="dashboard-primary bg-primary">
                <div class="sales-primary">
                    
                    <div class="top-icon">
                      
                    <i class="icon-bubbles"></i>
                    <h1><?php echo e($tot_client[0]->clientcount); ?></h1>
                    
                    <p>TOTAL CLIENTS</p>
                    </div>
                   
                </div>

               
                <div class="bg-dark-primary">
                 
                    <p><a href="<?php echo e(URL::to(''.'clientlists')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">

                <div class="top-icon">

                 <i class="icon-basket-loaded"></i>
                 <h1><?php echo e($tot_assignclient[0]->assignclientcount); ?></h1>
                 <p>TOTAL ASSIGN SPOC TO CLIENTS</p>


                </div>
                    
                    
                </div>
                <div class="bg-dark-warning">
                    
                    <p><a href="<?php echo e(URL::to(''.'clientspoc')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                 <div class="top-icon">
                      
                   <!-- <i class="icon-clock"></i> -->
                    <h1></h1>
                    
                    <p>Client Listing</p>
                    </div>
                    <div class="dast-bot-ti">
                        <table>
                            <thead>
                            <tr>
                                
                                <th>Company Name</th>
                                <th>Created on</th>
                                 
                            </tr>
                            </thead>
                            <tbody>

                            <?php 
                            $i=1;
                            $temp = $adminclientdata ;
                             ?> 
                            <?php $__currentLoopData = $adminclientdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr>

                                
                                <td><?php echo e($c->comp_name); ?></td>
                                <td><?php echo e($c->created_at); ?></td>
                               </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                                        
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <!-- <p ><a href="<?php echo e(URL::to(''.'listallposition')); ?>">VIEW DETAILS</a></p> -->
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if(in_array(2, $role)): ?>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-success dashboard-success">
                <div class="sales-success">



                   <div class="top-icon">
                      
                    <i class="icon-speedometer"></i>
                    <h1><?php echo e($tot_spoc_assignclient[0]->spocclientcount); ?></h1>
                    
                    <p>TOTAL ASSIGNED CLIENTS</p>
                    </div>


                   
                   
                </div>
                <div class="bg-dark-success">
                    
                    <p><a href="<?php echo e(URL::to(''.'assignclient')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">


                   <div class="top-icon">
                      
                    <i class="icon-clock"></i>
                    <h1><?php echo e($tot_position[0]->spoctotpos); ?></h1>
                    
                    <p>TOTAL ADDED POSITIONS</p>
                    </div>

                    
                   
                </div>
                <div class="bg-dark-facebook">
                   
                    <p><a href="<?php echo e(URL::to(''.'positionlist')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">
                 <div class="top-icon">
                      
                   <i class="icon-basket-loaded"></i>
                    <h1><?php echo e($tot_assignpostion[0]->spoctotassignpos); ?></h1>
                    
                    <p>TOTAL POSITION ALLOCATE TO RECRUITER</p>
                    </div>
                   
                   
                </div>
                <div class="bg-dark-warning">
                   
                    <p><a href="<?php echo e(URL::to(''.'viewallassignedrecuiterposition')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>

        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                 <div class="top-icon">
                      
                   <!-- <i class="icon-clock"></i> -->
                    <h1></h1>
                    
                    <p>Position Listing</p>
                    </div>
                    <div class="dast-bot-ti">
                        <table>
                            <thead>
                            <tr>
                                
                                <th>Position Name</th>
                                <th>Created on</th>
                                 
                            </tr>
                            </thead>
                            <tbody>

                            <?php 
                            $i=1;
                            $temp = $tot_positionlist ;
                             ?> 
                            <?php $__currentLoopData = $tot_positionlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr>

                                
                                <td><?php echo e($c->clientjob_title); ?></td>
                                <td><?php echo e($c->created_at); ?></td>
                               </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                                        
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <!-- <p ><a href="<?php echo e(URL::to(''.'listallposition')); ?>">VIEW DETAILS</a></p> -->
                </div>
            </div>
        </div>
        
    <?php endif; ?>
    <?php if(in_array(3, $role)): ?>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">

                  <div class="top-icon">
                      
                      <i class="icon-basket-loaded"></i>
                    <h1><?php echo e($tot_assignpostion_rec[0]->totassignpos_rec); ?></h1>
                    
                    <p>TOTAL POSITION ASSIGNED TO RECRUITER BY SPOC</p>
                    </div>
                 
                  
                </div>
                <div class="bg-dark-warning">
                  
                    <p><a href="<?php echo e(URL::to(''.'positionassignedreqbyspoc')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-success dashboard-success">
                <div class="sales-success">
 <div class="top-icon">
                      
                    <i class="icon-speedometer"></i>
                    <h1><?php echo e($tot_recuit_cv[0]->recuit_cv); ?></h1>
                    
                    <p>TOTAL CV</p>
                    </div>


                  
                </div>
                <div class="bg-dark-success">
                   
                    <p><a href="<?php echo e(URL::to(''.'cvlist')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                  <div class="top-icon">
                      
                   <i class="icon-clock"></i>
                    <h1><?php echo e($tot_recuit_cv_submission[0]->recuit_cv_submission); ?></h1>
                    
                    <p>TOTAL SUBMISSION</p>
                    </div>
                    
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <p><a href="<?php echo e(URL::to(''.'submissionlist')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if(in_array(4, $role)): ?>
    <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">
                  

                   <div class="top-icon">
                      
                   <i class="icon-basket-loaded"></i>
                    <h1><?php echo e($admin_tot_client[0]->adminclient); ?></h1>
                    
                    <p>TOTAL CLIENTS</p>
                    </div>

                    
                  
                </div>
                <div class="bg-dark-warning">
                    
                    <p ><a href="<?php echo e(URL::to(''.'admin/adminclient')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-success dashboard-success">
                <div class="sales-success">

                        <div class="top-icon">
                      
                   <i class="icon-speedometer"></i>
                    <h1><?php echo e($admin_tot_emp[0]->adminemp); ?></h1>
                    
                    <p>TOTAL EMPLOYEES</p>
                    </div>

                    
                    
                </div>
                <div class="bg-dark-success">
                  
                    <p ><a href="<?php echo e(URL::to(''.'admin/viewadminemployes')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">


                 <div class="top-icon">
                      
                 <i class="icon-clock"></i>
                    <h1><?php echo e($admin_tot_dept[0]->admindept); ?></h1>
                    
                    <p>TOTAL DEPARTMENTS</p>
                    </div>



                    
                   
                </div>
                <div class="bg-dark-facebook">
                    
                    <p><a href="<?php echo e(URL::to(''.'admin/admindepartment')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="dashboard-primary bg-primary">
                <div class="sales-primary">


                 <div class="top-icon">
                      
                   <i class="icon-bubbles"></i>
                    <h1><?php echo e($admin_tot_team[0]->adminteam); ?></h1>
                    
                    <p>TOTAL TEAMS</p>
                    </div>
                    
                    
                </div>
                <div class="bg-dark-primary">
                    
                    <p><a href="<?php echo e(URL::to(''.'admin/team')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                 <div class="top-icon">
                      
                   <i class="icon-clock"></i>
                    <h1><?php echo e($admin_tot_position[0]->adminposition); ?></h1>
                    
                    <p>TOTAL POSITIONS</p>
                    </div>

                   
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <p ><a href="<?php echo e(URL::to(''.'listallposition')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6">
            <div class="bg-warning dasboard-warning">
                <div class="sales-warning">


                  <div class="top-icon">
                      
                       <i class="icon-basket-loaded"></i>
                    <h1><?php echo e($admin_tot_recuit_cv[0]->admin_recuit_cv); ?></h1>
                    
                    <p>TOTAL SHORTLISTED CV</p>
                    </div>

                 
                   
                </div>
                <div class="bg-dark-warning">
                   
                    <p><a href="<?php echo e(URL::to(''.'shortlistcv')); ?>">VIEW DETAILS</a></p>
                </div>
            </div>
        </div>  

        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                 <div class="top-icon">
                      
                   <!-- <i class="icon-clock"></i> -->
                    <h1></h1>
                    
                    <p>Client Listing</p>
                    </div>
                    <div class="dast-bot-ti">
                        <table>
                            <thead>
                            <tr>
                                
                                <th>Company Name</th>
                                <th>Created on</th>
                                 
                            </tr>
                            </thead>
                            <tbody>

                            <?php 
                            $i=1;
                            $temp = $adminclientdata ;
                             ?> 
                            <?php $__currentLoopData = $adminclientdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr>

                                
                                <td><?php echo e($c->comp_name); ?></td>
                                <td><?php echo e($c->created_at); ?></td>
                               </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                                        
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <!-- <p ><a href="<?php echo e(URL::to(''.'listallposition')); ?>">VIEW DETAILS</a></p> -->
                </div>
            </div>
        </div>


        <div class="col-lg-4 col-sm-6">
            <div class="bg-facebook dashboard-facebook">
                <div class="sales-facebook">

                 <div class="top-icon">
                      
                   <!-- <i class="icon-clock"></i> -->
                    <h1></h1>
                    
                    <p>Position Listing</p>
                    </div>
                    <div class="dast-bot-ti">
                        <table>
                            <thead>
                            <tr>
                                
                                <th>Position Name</th>
                                <th>Created on</th>
                                 
                            </tr>
                            </thead>
                            <tbody>

                            <?php 
                            $i=1;
                            $temp = $admin_tot_positionlist ;
                             ?> 
                            <?php $__currentLoopData = $admin_tot_positionlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr>

                                
                                <td><?php echo e($c->clientjob_title); ?></td>
                                <td><?php echo e($c->created_at); ?></td>
                               </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                                        
                   
                </div>
                <div class="bg-dark-facebook">
                  
                    <!-- <p ><a href="<?php echo e(URL::to(''.'listallposition')); ?>">VIEW DETAILS</a></p> -->
                </div>
            </div>
        </div>



    <?php endif; ?>    
    </div>
  
    
   <!--  <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Client Listing</h5>
                           
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 ">
                                
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>S.N</th>
                                            <th>Category</th>
                                            <th>Company Name</th>
                                            <th>Contact Person Name</th>
                                            <th>Designation</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                             <th>Created by</th>
                                             
                                        </tr>
                                        </thead>
                                        <tbody>

                                           <?php 
                                $i=1;
                                $temp = $adminclientdata ;
                             ?> 
                                            <?php $__currentLoopData = $adminclientdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>


                                <td> <?php echo e($i++); ?></td>
                                <td> <?php if($c->client_catid==1): ?>
                                         <?php echo e("IT"); ?>

                                         <?php else: ?>
                                          <?php echo e("NON IT"); ?>

                                         <?php endif; ?>


                                </td>
                                <td><?php echo e($c->comp_name); ?></td>
                                <td><?php echo e($c->contact_name); ?></td>
                                <td><?php echo e($c->designation); ?></td>
                                <td><?php echo e($c->phone); ?></td>
                                <td><?php echo e($c->email); ?></td>
                                <td><?php echo e($c->name); ?></td>
                               </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        
                                        

                                        </tbody>
                                    </table>
                                </div>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
             </div>
             
             
             
            
            </div> -->

</div>



</div>



    <?php $__env->stopSection(); ?>
    
    
    
<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>