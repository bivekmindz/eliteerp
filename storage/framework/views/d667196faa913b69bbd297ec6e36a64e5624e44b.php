<?php $__env->startSection('content'); ?>

<style type="text/css">
  #search_filter_filter{float:left!important;}
</style>


<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View CV List</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Client Call CV List</a>
                </li>
                <li class="breadcrumb-item"><a href="">Allocated CVs List</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               <?php if(Session::has('success_msg')): ?>
                <div class="alert alert-success">
                    <?php echo e(Session::get('success_msg')); ?>

                </div>
                <?php endif; ?>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                        <div id="search_filter_filter" class="dataTables_filter"><label>Search: <input type="search" id="filter_area" style="border: 1px solid #968f8f;" class="" placeholder="" aria-controls="search_filter" autofocus></label></div>
                            <table class="table" id="">
                                <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>Company Name</th>
                                    <th>Position Name</th>
                                    <th>Assigned</th>
                                    <th>No of Line UP</th>
                                    <!--<th>FollowUp Status</th>-->
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="suggesstion-box">
                                <?php 
                                $i=1;
                                 ?>
                                <?php $__currentLoopData = $client_pos_resumes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr class="table-active" >
                                    <td><?php echo e($i++); ?></td>
                                    <td><?php echo e(!empty($p->comp_name) ? $p->comp_name : 'N/A'); ?></td>
                                    <td><?php echo e(!empty($p->clientjob_title) ? $p->clientjob_title : 'N/A'); ?></td>
                                    <td><?php echo e(!empty($p->name) ? $p->name : 'N/A'); ?></td>
                                    <td><?php echo e(!empty($p->total_rec_cv) ? $p->total_rec_cv : 'N/A'); ?></td>
                                    <!--<td><?php echo e($p->tca_followupstatus); ?></td>-->
                                    <td>
                                        <a href="<?php echo e(url('viewallocatedcvdetail', [$p->tca_assignfrom, $p->tca_assignto])); ?>"><button type="button" class="btn btn-info btn-sm">View Details</button></a>  
                                    </td>

                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>