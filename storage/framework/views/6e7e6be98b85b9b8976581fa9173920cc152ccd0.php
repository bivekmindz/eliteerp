<?php $__env->startSection('content'); ?>
    <style type="text/css">
        .parsley-errors-list li {color:#f00;}
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>view Role</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Role</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Add All Roles</a>
                    </li>
                </ol>
            </div>
        </div>

    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <div class="card-header"><h5 class="card-header-text">Add Role</h5>

                </div>
                <?php if(Session::has('success_msg')): ?>
                    <div class="alert alert-success">
                        <?php echo e(Session::get('success_msg')); ?>

                    </div>
            <?php endif; ?>

            <!-- end of modal -->
                <div class="card-block">
                    <form  action="<?php echo e(route('admin.addadminrole')); ?>" name="role" method="POST" data-parsley-validate  onsubmit="return validaterole();">
                        <?php if(count($errors)): ?>
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.
                                <br/>
                                <ul>
                                    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <li><?php echo e($error); ?></li>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">


                        <div class="form-group row <?php echo e($errors->has('role_name') ? 'has-error' : ''); ?>">
                            <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Role Name<span class="error"> * </span></label>
                            <div class="col-sm-5">
                                <input class="form-control addrole" type="text" placeholder="Enter Role Name" value="" id="example-text-input"   name="role_name">
                                <span class="rolename"></span>
                                <span class="text-danger"><?php echo e($errors->first('role_name')); ?></span>
                            </div>
                        </div>


                        <div class="md-input-wrapper">
                            <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit">
                            </input>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!-- Textual inputs ends -->
    </div>
    </div>
    </div>
   <script src="http://parsleyjs.org/dist/parsley.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
 <script>
 
    function validaterole()
    {
        var role=$('.addrole').val();
       // alert(role); return false;
        if(role=="")
    {
     $(".rolename").html("Please enter role name!.").css({ 'color': 'red' });
     return false;
   }else{
     $(".rolename").html(" ").css({ 'color': 'red' });
   }
    }
</script> 
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>