<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
          
                <h4>Submissions Listing</h4>
              
            </div>
        </div>



       
            <div class="row">
            
             <div class="col-md-12">
             
             <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">All Submissions</h5>
                           
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table" id="search_filter">
                                        <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Candidate Name</th>
                                            <th> Mobile</th>
                                            <th>Email</th>
                                             <th>Cv Upload Time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                           $i=1;
                                          ?>
                                          <?php $__currentLoopData = $submissionlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="table-active">
                                           
                                             <td><?php echo e($i++); ?></td>
                                         <td><?php echo e($value->candidate_name); ?></td>
                                            <td><?php echo e($value->candidate_mob); ?></td>
                                               <td><?php echo e($value->candidate_email); ?></td>
                                                
                                                     <td><?php echo e($value->created_at); ?></td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                        </tbody>
                                    </table>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
             </div>
             
             
             
            
    </div>
    
    <script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script>
      
<?php $__env->stopSection(); ?>




<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>