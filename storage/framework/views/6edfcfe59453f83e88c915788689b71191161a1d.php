<?php $__env->startSection('content'); ?>
<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View All Position Allocation To Recruiter</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="javascript:void(0)">Position</a>
                </li>
                <li class="breadcrumb-item"><a href="">View Position Assigned</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">View All Position Assigned To Recruiter</h5>
                </div>
                <?php if(Session::has('success_msg')): ?>
                <div class="alert alert-success">
                    <?php echo e(Session::get('success_msg')); ?>

                </div>
                <?php endif; ?>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                            <table class="table" id="search_filter">
                                <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Position Name</th>
                                    <th>Company Name</th>
                                    <th>Drive</th>
                                    <th>No Of Positions</th>
                                    <th>Assigned Recuriter Name</th>
                                      <th>Assigned Date</th>
                                    
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                $i=1;
                                 ?>
                                <?php $__currentLoopData = $pos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr class="table-active">
                                    <td><?php echo e($i++); ?></td>
                                    <td><?php echo e(!empty($p->clientjob_title) ? $p->clientjob_title : 'N/A'); ?></td>
                                    <td><?php echo e(!empty($p->comp_name) ? $p->comp_name : 'N/A'); ?></td>
                                    <td>N/A</td>
                                    <td><?php echo e(!empty($p->clientjob_noofposition) ? $p->clientjob_noofposition : 'N/A'); ?></td>
                                    <td><?php echo e(!empty($p->name) ? $p->name : 'N/A'); ?></td>
                                    <td><?php echo e(!empty($p->clientreq_createdon) ?
                                    
                                    date("d-m-Y", strtotime($p->clientreq_createdon) ) 
                                     : 'N/A'); ?></td>

                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>