<div class="loader-bg">
    <div class="loader-bar">
    </div>
</div>
<div class="wrapper">
<script type="text/javascript">
$(document).ready(function(){
    var popopen;
    popopen = localStorage.getItem("popopen");
    if(popopen == 1){
        $(".overlay").show();
    }
    $(".pop_click").click(function(){
        localStorage.setItem("popopen", "1");
        $(".overlay").show();
    });
});
</script>

<style type="text/css">
.overlay {
    position: absolute;
    z-index: 9999;
    background: rgba(51, 51, 51, 0.79);
    left: 0;
    right: 0;
    bottom: 0;
    top: 0;
    display: none;
}

.white_pop {
    width: 500px;
    margin: auto;
    height: 250px;
    background: #fff;
    position: fixed;
    left: 0;
    right: 0;
    bottom: 0;
    top: 0;
}
.png_back {
    margin-top: 105px;
    margin-left: 180px;
}
.pop_click {
    margin-right:400px;
    margin-top: 8px;
}

</style>
<div id="myNav">
    <div class="overlay">
        <div class="white_pop"> 
            <a href="#" class="start_align png_back">
                    <input class="btn btn-success col-md-12 col-sm-12 col-xs-12" id="breakTimeBack" onclick="mybreakbacktime();" name="breakTimeBack" type="button" value="i am back"/>
            </a>  
        </div>
    </div>
</div> 
    <!-- Navbar-->
<div class="container">
  <div class="modal fade" id="myBreakTimeModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group" id="minutes">
                    
                </div>
            </div>
        <div class="modal-footer">
          
        </div>
      </div>
    </div>
  </div> 
</div>
    <header class="main-header-top hidden-print">

        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button--><a href="#!" data-toggle="offcanvas" class="sidebar-toggle"></a>
            <!-- Navbar Right Menu-->
            <div class="navbar-custom-menu">
                <ul class="top-nav">
                    <!--Notification Menu-->
                     <a href="#" class="start_align">
                        <div class="pop_click">
                            <input class="btn btn-success col-md-12 col-sm-12 col-xs-12" id="breakTime" onClick="breakTime();" name="breakTime" type="button" value="Break"/>
                        </div>
                    </a>
                    <li class="dropdown pc-rheader-submenu message-notification search-toggle">
                        <a href="#!" id="morphsearch-search" class="drop icon-circle txt-white">
                            <i class="icofont icofont-search-alt-1"></i>
                        </a>
                    </li>
                    <li class="dropdown notification-menu">
                        <a href="#!" data-toggle="dropdown" aria-expanded="false" class="dropdown-toggle">
                            <i class="icon-bell"></i>
                            <span class="badge badge-danger header-badge">9</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="not-head">You have <b class="text-primary">4</b> new notifications.</li>
                            <li class="bell-notification">
                                <a href="javascript:;" class="media">
                    <span class="media-left media-icon">
                    <img class="img-circle" src="<?php echo e(asset("images/avatar-1.png")); ?> alt="UserImage">
                  </span>
                                    <div class="media-body"><span class="block">Lisa sent you a mail</span><span class="text-muted block-time">2min ago</span></div></a>
                            </li>
                            <li class="bell-notification">
                                <a href="javascript:;" class="media">
                    <span class="media-left media-icon">
                    <img class="img-circle" src="<?php echo e(asset("images/avatar-2.png")); ?> alt="User Image">
                  </span>
                                    <div class="media-body"><span class="block">Server Not Working</span><span class="text-muted block-time">20min ago</span></div></a>
                            </li>
                            <li class="bell-notification">
                                <a href="javascript:;" class="media"><span class="media-left media-icon">
                    <img class="img-circle" src=<?php echo e(asset("images/avatar-3.png")); ?> alt="UserImage">
                  </span>
                                    <div class="media-body"><span class="block">Transaction xyz complete</span><span class="text-muted block-time">3 hours ago</span></div></a>
                            </li>
                            <li class="not-footer">
                                <a href="#!">See all notifications.</a>
                            </li>
                        </ul>
                    </li>
                    <!-- chat dropdown -->
                    <li class="pc-rheader-submenu ">
                        <a href="#!" class="drop icon-circle displayChatbox">
                            <i class="icon-bubbles"></i>
                            <span class="badge badge-danger header-badge blink">5</span>
                        </a>

                    </li>


                    <!-- User Menu-->
                    <li class="dropdown">
                        <a href="#!" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle drop icon-circle drop-image">
                            
                            <span><?php echo e(Auth::user()->name); ?>  <i class=" icofont icofont-simple-down"></i></span>

                        </a>
                        <ul class="dropdown-menu settings-menu">
                            <li><a href="#!"><i class="icon-settings"></i> Settings</a></li>
                            <li><a href="<?php echo e(URL::to('profile')); ?>"><i class="icon-user"></i> Profile</a></li>
                            <li><a href="message.html"><i class="icon-envelope-open"></i> My Messages</a></li>
                            <li class="p-0">
                                <div class="dropdown-divider m-0"></div>
                            </li>
                             <li><a href="<?php echo e(URL::to('logout')); ?>"><i class="icon-logout"></i> Logout</a></li>
                            <li><a href="lock-screen.html"><i class="icon-lock"></i> Lock Screen</a></li>
                           

                        </ul>
                    </li>
                </ul>


            </div>
        </nav>
    </header>
    <!-- Side-Nav-->
    <aside class="main-sidebar hidden-print " >
        <section class="sidebar" id="sidebar-scroll">
            <div class="user-panel">
                <div class=" image"><img src="<?php echo e(asset("images/logo.png")); ?>" alt="User Image"></div>
                <a href="<?php echo e(route('home')); ?>" />

            </div>
            <!-- sidebar profile Menu-->
            <ul class="nav sidebar-menu extra-profile-list">
                <li>
                    <a class="waves-effect waves-dark" href="profile.html">
                        <i class="icon-user"></i>
                        <span class="menu-text">View Profile</span>
                        <span class="selected"></span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="javascript:void(0)">
                        <i class="icon-settings"></i>
                        <span class="menu-text">Settings</span>
                        <span class="selected"></span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="javascript:void(0)">
                        <i class="icon-logout"></i>
                        <span class="menu-text">Logout</span>
                        <span class="selected"></span>
                    </a>
                </li>
            </ul>