<?php $__env->startSection('content'); ?>
<head>
    <meta charset="UTF-8">
    <title>bootstrap4</title>
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
    <style type="text/css">
        .parsley-errors-list li {color:#f00;}
        .red-star {
            color: red;
        }
        .poc_text {
    font-weight: normal;
    color: #409a5d;
    font-size: 15px;
    margin-bottom: 7px;
}
    </style>
</head>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
            
                <h4>Add New Client</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Clients</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Add New Client</a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header"><h5 class="card-header-text">Add Client</h5>

                    </div>
                    <?php if(Session::has('success_msg')): ?>
                        <div class="alert alert-success">
                            <?php echo e(Session::get('success_msg')); ?>

                        </div>
                <?php endif; ?>

                <div class="card-block">
                    <?php echo Form::open([ 'action'=>'Emp\DashController@saveclient', 'data-parsley-validate','method'=>'post', 'files'=>true ]); ?>

                            <?php if(count($errors)): ?>
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.
                                    <br/>
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <div class="form-group row <?php echo e($errors->has('deptid') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Category</label>
                                <div class="col-sm-5">
                                    <select class="form-control" id="deptid" name="deptid" required>
                                        <?php $__currentLoopData = $dept; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($value->dept_id); ?>"><?php echo e($value->dept_name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </select>
                                    <span class="text-danger"><?php echo e($errors->first('deptid')); ?></span>
                                </div>
                            </div>

                            <div class="form-group row <?php echo e($errors->has('cname') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Company Name<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="<?php echo e(old('cname')); ?>" name="cname" id="example-text-input" data-parsley-required-message ="Please enter company name" placeholder="Enter Company Name" required>
                                    <span class="text-danger"><?php echo e($errors->first('cname')); ?></span>
                                </div>
                            </div>
                            <div class="form-group row  <?php echo e($errors->has('industry') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Branch</label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="<?php echo e(old('headquarters')); ?>" id="example-text-input" placeholder="Enter Branch Name" name="headquarters">
                                    <span class="text-danger"><?php echo e($errors->first('headquarters')); ?></span>
                                </div>
                            </div>
                            <div class="form-group row <?php echo e($errors->has('cpn') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Contact Person Name<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="<?php echo e(old('cpn')); ?>" id="example-text-input" data-parsley-required-message ="Please enter Person Name" placeholder="Enter Contact Person Name" name="cpn" required>
                                    <span class="text-danger"><?php echo e($errors->first('cpn')); ?></span>

                                </div>
                            </div>
                            <div class="form-group row   <?php echo e($errors->has('desig') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Designation</label>
                                <div class="col-sm-5">
                                    <input class="form-control" placeholder="Enter Designation" type="text" value="<?php echo e(old('desig')); ?>" id="example-text-input" name="desig"  id="desig">
                                    <span class="text-danger"><?php echo e($errors->first('desig')); ?></span>

                                </div>
                            </div>

                            <div class="form-group row   <?php echo e($errors->has('phone') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Mobile<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" data-parsley-required-message ="Please enter Mobile Number" placeholder="Enter Mobile" value="<?php echo e(old('phone')); ?>" id="example-text-input" name="phone" minlength="10" maxlength="15" required>
                                    <span class="text-danger"><?php echo e($errors->first('phone')); ?></span>
                                </div>
                            </div>


                            <div class="form-group row  <?php echo e($errors->has('email') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Email<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" placeholder="Enter Email" type="text" value="<?php echo e(old('email')); ?>" data-parsley-required-message ="Please enter Email" id="example-text-input" name="email" required>
                                    <span class="text-danger"><?php echo e($errors->first('email')); ?></span>
                                </div>
                            </div>

                            <div class="poc_text">POC1</div>

                            <div class="form-group row <?php echo e($errors->has('cpn1') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Contact Person Name<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="<?php echo e(old('cpn1')); ?>" id="example-text-input" data-parsley-required-message ="Please enter Person Name" placeholder="Enter Contact Person Name" name="cpn1" required>
                                    <span class="text-danger"><?php echo e($errors->first('cpn1')); ?></span>

                                </div>
                            </div>
                            <div class="form-group row   <?php echo e($errors->has('desig1') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Designation</label>
                                <div class="col-sm-5">
                                    <input class="form-control" placeholder="Enter Designation" type="text" value="<?php echo e(old('desig1')); ?>" id="example-text-input" name="desig1"  id="desig1">
                                    <span class="text-danger"><?php echo e($errors->first('desig1')); ?></span>

                                </div>
                            </div>

                            <div class="form-group row   <?php echo e($errors->has('phone1') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Mobile<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" data-parsley-required-message ="Please enter Mobile Number" placeholder="Enter Mobile" minlength="10" maxlength="15" value="<?php echo e(old('phone1')); ?>" id="example-text-input" name="phone1" required>
                                    <span class="text-danger"><?php echo e($errors->first('phone1')); ?></span>
                                </div>
                            </div>


                            <div class="form-group row  <?php echo e($errors->has('email1') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Email<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" placeholder="Enter Email" type="text" value="<?php echo e(old('email1')); ?>" data-parsley-required-message ="Please enter Email" id="example-text-input" name="email1" required>
                                    <span class="text-danger"><?php echo e($errors->first('email1')); ?></span>
                                </div>
                            </div>

                            <div class="poc_text">POC2</div>

                            <div class="form-group row <?php echo e($errors->has('cpn2') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Contact Person Name<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="<?php echo e(old('cpn2')); ?>" id="example-text-input" data-parsley-required-message ="Please enter Person Name" placeholder="Enter Contact Person Name" name="cpn2" required>
                                    <span class="text-danger"><?php echo e($errors->first('cpn2')); ?></span>

                                </div>
                            </div>
                            <div class="form-group row   <?php echo e($errors->has('desig2') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Designation</label>
                                <div class="col-sm-5">
                                    <input class="form-control" placeholder="Enter Designation" type="text" value="<?php echo e(old('desig2')); ?>" id="example-text-input" name="desig2"  id="desig2">
                                    <span class="text-danger"><?php echo e($errors->first('desig2')); ?></span>

                                </div>
                            </div>

                            <div class="form-group row   <?php echo e($errors->has('phone2') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Mobile<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" data-parsley-required-message ="Please enter Mobile Number" placeholder="Enter Mobile" minlength="10" maxlength="15" value="<?php echo e(old('phone2')); ?>" id="example-text-input" name="phone2" required>
                                    <span class="text-danger"><?php echo e($errors->first('phone2')); ?></span>
                                </div>
                            </div>


                            <div class="form-group row  <?php echo e($errors->has('email2') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Email<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" placeholder="Enter Email" type="text" value="<?php echo e(old('email2')); ?>" data-parsley-required-message ="Please enter Email" id="example-text-input" name="email2" required>
                                    <span class="text-danger"><?php echo e($errors->first('email2')); ?></span>
                                </div>
                            </div> 


                            <div class="form-group row  <?php echo e($errors->has('company_size') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Company Size</label>
                                <div class="col-sm-5">
                                    <input class="form-control" placeholder="Enter Company Size" type="text" value="<?php echo e(old('company_size')); ?>" id="example-text-input" name="company_size">
                                    <span class="text-danger"><?php echo e($errors->first('company_size')); ?></span>
                                </div>
                            </div>





                            <div class="form-group row  <?php echo e($errors->has('timings') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">From Time</label>
                                <div class="col-sm-5">
                                
                                
                                <div class="form-group">
                                    <div class="clearfix">
                                        <div class="pull-center clearfix" style="margin-bottom:10px;">
                                            <input class="form-control pull-right" value="<?php echo e(old('from_time')); ?>" id="single-input" name="from_time" placeholder="Enter From Time">
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>

                            <div class="form-group row  <?php echo e($errors->has('timings') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">To Time</label>
                                <div class="col-sm-5">
                                
                                
                                <div class="form-group">
                                    <div class="clearfix">
                                        <div class="pull-center clearfix" style="margin-bottom:10px;">
                                            <input class="form-control pull-right" value="<?php echo e(old('to_time')); ?>" id="single-input2" name="to_time" placeholder="Enter To Time">
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>  

                            <div class="form-group row  <?php echo e($errors->has('timings') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Days Working</label>
                                <div class="col-sm-5">
                                    <input class="form-control" placeholder="Enter No of Days" maxlength="1" type="text" value="<?php echo e(old('days_working')); ?>" id="example-text-input" name="days_working">
                                    <span class="text-danger"><?php echo e($errors->first('days_working')); ?></span>
                                </div>
                            </div>
                            <div class="form-group row  <?php echo e($errors->has('industry') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Industry</label>
                                <div class="col-sm-5">
                                    <input class="form-control" placeholder="Enter Industry" type="text" value="<?php echo e(old('industry')); ?>" id="example-text-input" name="industry">
                                    <span class="text-danger"><?php echo e($errors->first('industry')); ?></span>
                                </div>
                            </div>
                            <div class="form-group row  <?php echo e($errors->has('industry') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Company Specialities<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="<?php echo e(old('company_specialties')); ?>" data-parsley-required-message ="Please enter Company Specialities" id="example-text-input" placeholder="Enter Company Specialities" name="company_specialties" required>
                                    <span class="text-danger"><?php echo e($errors->first('company_specialties')); ?></span>
                                </div>
                            </div>
                            <div class="form-group row  <?php echo e($errors->has('industry') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Office Address<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control" type="text" value="<?php echo e(old('office_address')); ?>" data-parsley-required-message ="Please enter Office Address" placeholder="Office Address" id="example-text-input" name="office_address" required >
                                    <span class="text-danger"><?php echo e($errors->first('office_address')); ?></span>
                                </div>
                            </div>
                            <div class="form-group row  <?php echo e($errors->has('industry') ? 'has-error' : ''); ?>">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Alternate Phone</label>
                                <div class="col-sm-5">
                                    <input class="form-control"  type="text" value="<?php echo e(old('alternate_phone')); ?>" placeholder="Enter Alter Phone No" id="example-text-input" name="alternate_phone">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Selling Points</label>
                                <div class="col-md-8">
                                    <textarea name="selling_point" id="summernote" class="summernote form-control"></textarea>
                                </div>
                            </div> 

                            <div class="form-group row">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Summary About The Client<span class="red-star">*</span></label>
                                <div class="col-md-8">
                                    <textarea name="summary" id="summary" class="summernote form-control"></textarea>
                                </div>
                            </div> 

                            <div class="form-group row">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Choose Logo<span class="red-star">*</span></label>
                                <div class="col-md-3">
                                   <input data-preview="#preview" name="input_img" type="file" id="imageInput">
                                <img class="col-sm-6" id="preview"  src="" ></img>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Contractual SLA<span class="red-star">*</span></label>
                                <div class="col-sm-5">
                                    <input class="form-control"  type="text" value="<?php echo e(old('contractual_sla')); ?>" placeholder="Enter SLA" id="example-text-input" name="contractual_sla">
                                </div>
                            </div>  

                            <div class="md-input-wrapper">
                                <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit">
                                </input>
                            </div>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> -->
<script src="http://parsleyjs.org/dist/parsley.js"></script>
<?php $__env->stopSection(); ?>




<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>