<?php $__env->startSection('content'); ?>
<style>
  .error{
    color:red;
  }
  .form-control.error{
    color:black;
  }
</style>
<head>
  <meta charset="UTF-8">
  <title>bootstrap4</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
  <link href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet">

</head>

<div class="container-fluid">
  <div class="row">
    <div class="main-header">
     <?php $position = app('App\Component\CommonComponent'); ?>
     <?php  
     $breadcrumb = $position->breadcrumbs();
     //print_r($breadcrumb); exit;
     //print_r($breadcrumb[0]->menuname); exit;
      ?>
     <h4>Add Position</h4>
     <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
      <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
      </li>
      <li class="breadcrumb-item"><a href="javascript:void(0)"><?php echo e($breadcrumb[0]->parentmenu); ?></a>
      </li>
      <li class="breadcrumb-item"><a href=""><?php echo e($breadcrumb[0]->menuname); ?></a>
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="card">
     <div class="card-header"><h5 class="card-header-text">Add Position Form</h5>

     </div>
     <?php if(Session::has('success_msg')): ?>
     <div class="alert alert-success">
       <?php echo e(Session::get('success_msg')); ?>

     </div>
     <?php endif; ?>

     <div class="card-block">
       <!--<?php echo Form::open([ 'action'=>'Emp\PositionController@addposition', 'method'=>'post','name'=>'position', 'files'=>true ]); ?>-->
       <form method="post" name="position" action="<?php echo e(URL::to('addposition')); ?>" enctype="multipart/form-data">
        <div class="row">
          <?php if(count($errors)): ?>
          <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.
            <br/>
            <ul>
             <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             <li><?php echo e($error); ?></li>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
           </ul>
         </div>
         <?php endif; ?>
         <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

         <div class="col-lg-6">
           <div class="form-group row">
            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Type<span class="error"> * </span> </label>
            <div class="col-sm-8">
             <select class="form-control" id="dept" name="dept">
               <option value="">Please Select Department</option>
               <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <option value="<?php echo e($value->dept_id); ?>"><?php echo e($value->dept_name); ?></option>
               <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
             </select>
           </div>
         </div>
       </div>


       <div class="col-lg-6">
         <div class="form-group row">
          <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Company Name<span class="error"> * </span></label>
          <div class="col-sm-8">
           <select class="form-control" id="compname" name="compname">
             <option value="">Please Select Company</option>
             <?php $__currentLoopData = $client; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
             <option value="<?php echo e($value->fk_clientid); ?>"><?php echo e($value->comp_name); ?></option>
             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
           </select>
         </div>
       </div>
     </div>

   </div>

   <div class="row">
    <div class="col-lg-6">
     <div class="form-group row">
      <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Position Title <span class="error"> * </span></label>
      <div class="col-sm-8">
        <input class="form-control" type="text" value="" id="jobtitle" placeholder="Position Title" name="jobtitle">
      </div>
    </div>
  </div>


</div>

<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Number of Positions<span class="error"> * </span> </label>
    <div class="col-sm-8">
     <input class="form-control" type="number" maxlength="11" value="" placeholder="Number of Positions" id="no_of_position" name="noofpos">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">

   <div class="checkbox-color checkbox-success ">
    <input id="checkbox3" type="checkbox" name="drive">
    <label for="checkbox3">
     Drive
   </label>
 </div>
</label>
<div class="col-sm-8">
  <div class="form-group">
    <div class="input-group date" id="drivedate">
      <input type="text" class="form-control" placeholder="Drive Date" id="drivedate">
      <span class="input-group-addon">
        <span class="icofont icofont-ui-calendar"></span>
      </span>
    </div>
  </div>
</div>
</div>
</div>
</div>

<!--Line 1 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Experience<span class="error"> * </span> </label>
    <div class="col-sm-8">
     <input class="form-control" type="text" value="" id="noofpos" name="experience" placeholder="Experience">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Relevant/Domain Experience</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" value="" id="noofpos" name="domain_experience" placeholder="Domain Experience">
 </div>
</div>
</div>
</div>
<!--Line 2 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Primary Skills</label>
    <div class="col-sm-8">
     <input class="form-control" type="text" value="" id="noofpos" name="primary_skill" placeholder=" Primary Skills">
  </div>
</div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Secondary Skills</label>
  <div class="col-sm-8">
     <input class="form-control" type="text" value="" id="noofpos" name="secondary_skill" placeholder=" Primary Skills">
</div>
</div>
</div>
</div>

<!--Line 3 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Reporting Person Name</label>
    <div class="col-sm-8">
     <input class="form-control" type="text" value="" id="" name="rpn" placeholder="Reporting Person Name">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Reporting Person Designation</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" name="rpd" placeholder="Reporting Person Designation">
 </div>
</div>
</div>
</div>
<!--Line 4 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Reporting Person Phone</label>
    <div class="col-sm-8">
     <input class="form-control" type="number" maxlength="11" name="rpp" placeholder="Reporting Person Phone">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Reporting Person Email</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" name="rpe" placeholder="Reporting Person Email">
 </div>
</div>
</div>
</div>
<!--Line 5 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">POC Name</label>
    <div class="col-sm-8">
     <input class="form-control" type="text" name="pocn" placeholder="POC Name">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">POC Designation</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" name="pocd" placeholder="POC Designation">
 </div>
</div>
</div>
</div>
<!--Line 6 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">POC Phone</label>
    <div class="col-sm-8">
     <input class="form-control" type="number" maxlength="11" name="pocp" placeholder="POC Phone">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">POC Email</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" name="poce" placeholder="POC Email">
 </div>
</div>
</div>
</div>

<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Hiring Manger Name</label>
    <div class="col-sm-8">
     <input class="form-control" type="text" name="hmn" placeholder="Hiring Manger Name">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Hiring Manger Designation</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" name="hmd" placeholder="Hiring Manger Designation">
 </div>
</div>
</div>
</div>
<!--Line 7 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form -group row">
    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Hiring Manger Phone</label>
    <div class="col-sm-8">
     <input class="form-control" type="number" maxlength="11" name="hmp" placeholder="Hiring Manger Phone">
   </div>
 </div>
</div>
<div class="col-lg-6">
 <div class="form -group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Hiring Manger Email</label>
  <div class="col-sm-8">
   <input class="form-control" type="text" name="hme" placeholder="Hiring Manger Email">
 </div>
</div>
</div>
</div>
<!--Line 8 -->
<div class="row">
  <div class="col-lg-6">
   <div class="form-group row">
     <div class="col-sm-4">
        <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Drive</label>
        <input class="form-control" type="radio" value="Drive" name="report" checked placeholder="">
     </div>
   
       <div class="col-sm-4">
            <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Shortlist</label>
           <input class="form-control" type="radio" value="Shortlist" name="report" placeholder="">
      </div>
       <div class="col-sm-4">
            <label for="example-text-input" class=" col-form-label form-control-label">Daily Line Up</label>
            <input  type="radio" value="DaliyLineUp" name="report" placeholder="">
       </div>
</div>


</div>
<div class="col-lg-6">
 <div class="form-group row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Severity of the requirements</label>
  <div class="col-sm-8">
   <select class="form-control" name="severity">
    <option value="Low" >Low</option>
    <option value="Medium" >Medium</option>
    <option value="High" >High</option>
  </select>
</div>
</div>
</div>
</div>

<div class="row">
  <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Add JD</label>
</div>  
<div class="row">
  <textarea name="upload_position_jd" id="summernote" class="summernote form-control"></textarea>
</div> 
</div>

<div class="md-input-wrapper">
  <button type="submit" class="btn btn-primary waves-effect waves-light">Submit
  </button>
</div>
<!-- <?php echo Form::close(); ?> -->
</form>
</div>
</div>
</div>
<!-- Textual inputs ends -->
</div>

</div>
</div>
<style>
input[type=number]::-webkit-inner-spin-button {
  -webkit-appearance: none!important;
}
</style>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>

<script>
  $('# '). ();

    // $('#summernote').summernote({
    //     placeholder: 'Hello bootstrap 4',
    //     tabsize: 2,
    //     height: 100
    //   });
  </script>
  <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>
  <script>
    $(function() {
      $("form[name='position']").validate({
        ignore: [],
        rules: {
          dept: "required",
          compname: "required",
          jobtitle: "required",
          noofpos: "required",
          upload_position_jd: "required",
        },
        messages: {
          dept: "Please enter department.",
          compname: "Please enter company name.",
          jobtitle: "Please enter company url.",
          noofpos: "Please enter number of position.",
          upload_position_jd: "Please enter upload position jd.",
        },
        submitHandler: function(form) {
          form.submit();
        }
      });
    });

//     $(document).ready(function() {
//       $("#noofpos").keydown(function (e) {
//         if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
//           (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
//           (e.keyCode >= 35 && e.keyCode <= 40)) {
//          return;
//      }
//      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
//       e.preventDefault();
//     }
//   });
//     });
  </script>

  <?php $__env->stopSection(); ?>

<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>