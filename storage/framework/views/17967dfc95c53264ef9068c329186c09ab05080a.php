

<?php $__env->startSection('content'); ?>


<style type="text/css">
  .green-o{    color: #03561e;
    font-weight: 600; }
   .red-o{     color: #9a0f0f;
    font-weight: 600;}
  
  .view-spoc{ width:100%;     max-height: 350px;     overflow-y: auto;float:left;}
.view-spoc .vasp{    width: 1300px;
    float: left;}
.view-spoc .vasp table{ width:100%; float:left;}
.view-spoc .vasp table tr{    border: 1px solid #cecccc;}
.view-spoc .vasp table tr th{    padding: 0px 5px;
    border: 1px solid;
    background-color: #43a061;
    color: #fff;
    line-height: 42px; }
.view-spoc .vasp table tr td{     padding: 8px 9px;    border: 1px solid #cecccc;}

.dateclass.placeholderclass::before {
  width: 100%;
  content: attr(placeholder);
}

.dateclass.placeholderclass:hover::before {
  width: 0%;
  content: "";
}
.form-group {
    margin-bottom: 100px;
}.form-control {
   
    margin: 10px;
}
</style>
<script>

$(document).ready(function(){
    $("#filter_area").keyup(function(){
        var xxx = $(this).val();

       // alert(xxx);

          $.ajax({
                      type: 'POST',
                      url: '<?php echo e(URL::route('cvshortlisting')); ?>',
                      data:{
                        '_token': "<?php echo e(csrf_token()); ?>",
                        'key':xxx,
                      },
                      success: function(data){
                     //  alert(data);
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);

                         
                      }
                    });



    
    });
});



// function selectCountry(val) {
// $("#search-box").val(val);
// $("#suggesstion-box").hide();
// }
</script><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
<!-- Container-fluid starts -->
<!-- Main content starts -->
<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>Shortlist Profile</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href=""><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="">Positions</a>
                </li>
                <li class="breadcrumb-item"><a href="">Shortlisting of CV</a>
                </li>
            </ol>
        </div>

 

    </div>

<div class="container">
  <div class="modal hide fade" id="myModalcvstattus" role="dialog">
      <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select  Resume Status</h4>
        </div>

           <form method="post" action="<?php echo e(URL::to('updateclientstatuspopupresume')); ?>">
            <div class="modal-body">
              <table width="100%" border="1">
               
<tr><td id="recruiterspockupcvstattus"></td></tr>
              </table>
                
                        <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
                    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                </div>
                 <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="submit" class="btn btn-default" value="Submit">
            </div>
            </div>
           
        </form>
      
      </div>      
    </div>
</div> 
</div><!-- 

    <div class="row">

        <div class="col-md-12">

            <div class="card">
                <div class="card-header">
                    <h5 class="card-header-text">SPOC Shortlisting</h5>

                </div>
                <?php if(Session::has('success')): ?>
                <div class="alert alert-success">
                    <?php echo e(Session::get('success')); ?>

                </div>
                <?php endif; ?>
                <div class="card-block">
  <!-- <input type="text" class="" autofocus placeholder="" id="filter_area" >  -->
       
        <script>
        $(document).ready(function(){
            $('#search').keyup(function(){
            
            // Search text
            var text = $(this).val();
            
            // Hide all content class element
            $('.content').hide();

            // Search and show
            $('.content:contains("'+text+'")').show();
            
            });
        });
        </script>
   
                     <input type='text' id='search' placeholder='Search Text'>
           

           
                    <div class="row">
                        <div class="col-sm-12">
                        
                        <div class="view-spoc">
                        
                          <div class="vasp">
                          
                         

         
                            <table>
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Position Name</th>
                                         <th>Company Name</th>
                                        <th>No. Of Positions</th>
                                         <th>Spoc Name</th>
                                        <th>Total Uploaded Profile</th>
                                        <th>View</th>
                                        <th></th>

                                    </tr>
                                </thead>
                                <tbody id="myTable">

                                    <?php 
                                    $i=1;
                                     ?>
                                    <?php $__currentLoopData = $pos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class='content'>
                                        <td class='title'><?php echo e($i++); ?></td>
                                        <td class='title'><?php echo e($value->clientjob_title); ?>

                                            <input type="hidden" value="<?php echo e($value->clientjob_title); ?>"  id="pos">
                                        </td>
                                         <td class='title'><?php echo e($value->comp_name); ?></td>
                                        <td class='title'><?php echo e($value->clientjob_noofposition); ?></td>
                                          <td class='title'><?php echo e($value->name); ?></td>

                                        <td class='title'><?php echo e($value->total_upload_profile); ?></td>

                                        <td>
<button type="button" value="<?php echo e($value->position_id); ?>" class="det_css one-click<?php echo e($value->position_id); ?>" onclick="getcandidatedetails<?php echo e($value->position_id); ?>(this.value),getposition('<?php echo $value->clientjob_title ?>')" ><span class="icofont icofont-eye-alt"></span></button> 


                                         </td>



                                    </tr>

<!-- </tbody> -->

               <tr><td  colspan="7">

<style>
 .hide_css<?php echo e($value->position_id); ?>{ width:100%; float:left; display:none;}
 
 </style>
 <?php echo Form::open([ 'action'=>'Emp\CVController@savecandidate', 'method'=>'post', 'files'=>true ]); ?>


<table   class="table card hide_css<?php echo e($value->position_id); ?>"  >
                              
                                    <tr>
                                       <th></th>
                                        <th>S.No</th>
                                         <th>Client Status</th>
                                          <th colspan="2">Status</th>
                                          <th>Recuiter Name</th>
                                        <th>Candidate's Name</th>
                                        <th>CV Uploaded Time</th>
                                        <th>Mobile Number</th>
                                        <th>Download CV</th>
                                        <th>Qulification</th>
                                        <th>Total Experience</th>
                                        <th>Domain Experience </th>
                                        <th>Primary Skill</th>
                                        <th>Secondary Skill</th>
                                        <th>Current ORG</th>
                                        <th>Current CTC </th>
                                        <th>Expected CTC</th>
                                        <th>NP</th>
                                        <th>Designation</th>
                                        <th>Location</th>
                                        <th>Reason For Change</th>
                                        <th>Skills For Rating</th>
                                        <th>Relocation</th>
                                        <th>DOJ</th>
                                         
                                        <th>Remark</th>
                                       
                                        

                                    </tr>
                            
                                <tbody id="candilist<?php echo e($value->position_id); ?>">

                                </tbody>
                                <tr><td colspan="24" style="text-align: left">
                                <input type="hidden" value="" id="poid" name="poid">
   <button type="button"  id="buttonClass<?php echo e($value->position_id); ?>" class="btn btn-primary waves-effect waves-light">Submit</button>
                  <!--  <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit">-->
                   </td></tr>
               
                            </table>
  

  <?php echo Form::close(); ?>

          </td></tr>   

          

<script type="text/javascript">
    $(document).ready(function() {
        $("#buttonClass<?php echo e($value->position_id); ?>").click(function() {
            var favorite = [];
            $.each($("input[name='checkboxid']:checked"), function(){            
                favorite.push($(this).val());
            });
            var valuejoin=favorite.join(",");
           // alert( valuejoin);
  $.ajax({
        type:"GET",
        url:"getresumespopup/"+valuejoin,
        data:'',
        success: function(data){
           $('#myModalcvstattus').modal('show');
     //    alert(data);
            $("#recruiterspockupcvstattus").empty();
            $("#recruiterspockupcvstattus").append(data);
          // $('#recruiterspockup').multiselect('rebuild');
        },
        error: function(data){

        }
    });

        });
    });
</script>
                        
<script>

   function getposition(id){
    var ids  = id;
   // alert(ids);
    $('#poid').val(ids);

}

function getcandidatedetails<?php echo e($value->position_id); ?>(val)
{


 var  id=val;

 $.ajax({
    url: 'getcandidatedetails/'+id,
    type:'GET',
   error: function(xhr) {
        alert("An error occured: " + xhr.status + " " + xhr.statusText);
    },
    success: function(data)
    {
      $(".hide_css<?php echo e($value->position_id); ?>").slideToggle(700);
     var data1= JSON.parse(data);

     var trHTML = '';
        var j=1;


                  //  $.each(data, function (i, data) {
                    for (i = 0; i < data1.length; i++) {
                       var str = data1[i]["created_at"];
                       var str = str.split(" ");
                       var aa =str[0].split("-");

                       var con="<?php echo e(config('constants.APP_URLS')); ?>";

                       var url=con+'/storage/app/uploadcv/'+aa[0]+"/"+aa[1]+"/"+aa[2]+"/"+data1[i]["cv_name"];
//alert(data1[i]["cv_status"]);
 data1[i]["s1"]=1;
  data1[i]["s2"]=2;
  data1[i]["s3"]=3;
  data1[i]["s4"]=4;
   data1[i]["s5"]=5;
    data1[i]["s6"]=6;
if(data1[i]["cv_status"]=='3')
{
    data1[i]["clientstattus"]='Rejected';
data1[i]["cv_nameaa"]='<select name= "checkbox_data[]" onchange="changestatusdata(this.value)"  id="foo"><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s1"]+'>Not Seen</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s2"]+'>Send To Client</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s3"]+' selected>Rejected</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s4"]+' >Offered</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s5"]+' >Pipelined</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s6"]+'>Joined</option></select>';
}
if(data1[i]["cv_status"]=='4')
{
     data1[i]["clientstattus"]='Offer';
data1[i]["cv_nameaa"]='<select name= "checkbox_data[]" onchange="changestatusdata(this.value)"  id="foo"><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s1"]+'>Not Seen</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s2"]+'>Send To Client</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s3"]+'>Rejected</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s4"]+' selected>Offered</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s5"]+' >Pipelined</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s6"]+'>Joined</option></select>';
}if(data1[i]["cv_status"]=='5')
{
     data1[i]["clientstattus"]='Pipeline';
data1[i]["cv_nameaa"]='<select name= "checkbox_data[]" onchange="changestatusdata(this.value)"  id="foo"><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s1"]+'>Not Seen</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s2"]+'>Send To Client</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s3"]+'>Rejected</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s4"]+'>Offered</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s5"]+' selected>Pipelined</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s6"]+'>Joined</option></select>';
}
if(data1[i]["cv_status"]=='2')
{
      data1[i]["clientstattus"]='Send To Client';
  data1[i]["s1"]=1;
  data1[i]["s2"]=2;
  data1[i]["s3"]=3;
  data1[i]["s4"]=4;
   data1[i]["s5"]=5;
    data1[i]["s6"]=6;
data1[i]["cv_nameaa"]='<select name= "checkbox_data[]" onchange="changestatusdata(this.value)"  id="foo"><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s1"]+'>Not Seen</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s2"]+' selected>Send To Client</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s3"]+'>Rejected</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s4"]+'>Offered</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s5"]+' >Pipelined</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s6"]+'>Joined</option></select>';
}
if(data1[i]["cv_status"]=='1')
{
        data1[i]["clientstattus"]='Not Seen';
  data1[i]["s1"]=1;
  data1[i]["s2"]=2;
  data1[i]["s3"]=3;
  data1[i]["s4"]=4;
   data1[i]["s5"]=5;
    data1[i]["s6"]=6;
data1[i]["cv_nameaa"]='<select name= "checkbox_data[]" onchange="changestatusdata(this.value)"  id="foo"><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s1"]+'>Not Seen</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s2"]+'>Send To Client</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s3"]+'>Rejected</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s4"]+'>Offered</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s5"]+'>Pipelined</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s6"]+'>Joined</option></select>';
}
                       trHTML += "<tr class='content'>" +
                       "<td>"+j+++"</td>" +
                         "<td><input type='checkbox' name='checkboxid' value="+data1[i]["id"]+"></td>" +
                          "<td class='title'>"+data1[i]["clientstattus"]+"</td>" +
                           "<td class='title'><button type='button'  class='det_css one-click' data-toggle='modal' data-target='#myModal' onclick='getposiionvalue("+data1[i]["position_id"]+"),getupdatespock("+data1[i]["id"]+","+data1[i]["recruiter_id"]+","+data1[i]["cv_status"]+","+data1[i]["position_id"]+")(this.value),getpositionspock("+data1[i]["id"]+","+data1[i]["recruiter_id"]+","+data1[i]["cv_status"]+")' ><span class='icofont icofont-eye-alt'></span></button> </td>" +
                        "<td class='title'>"+data1[i]["username"]+"</td>" +
                       "<td class='title'>"+data1[i]["candidate_name"]+"</td>" +
                       "<td class='title'>"+data1[i]["cv_uploadtime"]+"</td>" +
                       "<td class='title'>"+data1[i]["candidate_mob"]+"</td>" +
                       '<td class="title"><a href='+url+' download>   ' +'Download </a></td>' +
                       "<td class='title'>"+data1[i]["highest_qulification"]+"</td>" +
                       "<td class='title'>"+data1[i]["total_exp"]+"</td>" +
                       "<td class='title'>"+data1[i]["domain_exp"]+"</td>" +
                       "<td class='title'>"+data1[i]["primary_skill"]+"</td>" +
                       "<td class='title'>"+data1[i]["secondary_skill"]+"</td>" +
                       "<td class='title'>"+data1[i]["current_org"]+"</td>" +
                       "<td class='title'>"+data1[i]["current_ctc"]+"</td>" +
                       "<td class='title'>"+data1[i]["expected_ctc"]+"</td>" +
                       "<td class='title'>"+data1[i]["np"]+"</td>" +
                       "<td class='title'>"+data1[i]["desng"]+"</td>" +
                       "<td class='title'>"+data1[i]["location"]+"</td>" +
                       "<td class='title'>"+data1[i]["reason_for_change"]+"</td>" +
                       "<td class='title'>"+data1[i]["communication_skills_rating"]+"</td>" +
                       "<td class='title'>"+data1[i]["relocation"]+"</td>" +
                       "<td class='title'>"+data1[i]["doj"]+"</td>" +
                         
                       "<td class='title'>"+data1[i]["remark"]+"</td>" +
                   /*    "<td>"+data1[i]["cv_nameaa"]+"<div id='place"+data1[i]["id"]+""+data1[i]["recruiter_id"]+"'></div></td>" +*/
                      
                       '</tr>';
                   }
             //      });
                   // console.log(data.length);
             $('#candilist'+id).html(trHTML);
 
 /*$(".hide_css<?php echo e($value->position_id); ?>").slideToggle(700);*/


         }




     });
$('#foo').on("change",function(){
    var dataid = $("#foo option:selected").attr('data-id');
  //  alert(dataid)
});
}
/*
$(".one-click<?php echo e($value->position_id); ?>").click(function(){

//  alert("dddddddddddd");
        $(".hide_css<?php echo e($value->position_id); ?>").slideToggle(700);
    });
*/

function getupdatespock(id,recruiter_id,cv_status,posid)
{

 var ids = id;
 var recruiterid = recruiter_id;
 var cvstatus = cv_status;
 var posid = posid;
    $.ajax({
        type:"GET",
        url:"getspockstatus/"+ids+"/"+recruiterid+"/"+cvstatus+"/"+posid+"/",
        data:'',
        success: function(data){
        // alert(data);
            $("#recruiterspockup").empty();
            $("#recruiterspockup").append(data);
          // $('#recruiterspockup').multiselect('rebuild');
        },
        error: function(data){

        }
    });



      

}
function getposiionvalue(id)
{

 var ids = id;
 //alert(ids);
    $.ajax({
        type:"GET",
        url:"getspockposition/"+ids,
        data:'',
        success: function(data){
        // alert(data);
        document.getElementById("placepopupval").value = data;

        },
        error: function(data){

        }
    });



      

}


function changestatuspopupdata(id)
{
var str = id;
var res = str.split("/");
//alert(str);

//Then read the values from the array where 0 is the first
var myvar = res[0] + ":" + res[1] + ":" + res[2];
  // d = document.getElementById("select_id").value;
  if(res[2]=='4')
  {
 document.getElementById("placepopup").innerHTML="<input type='text' name='expectedsalery[]'  placeholder='Expected Salary'  class='form-control' id='expectedsalery' ><input type='date' name='Doj[]' class='form-control'  placeholder='Date Of Joining' id='Doj'><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining' id='expDoj' class='form-control'  ><input type='hidden' name='count[]' class='form-control col-xs-6'  value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  else

{


   if(res[2]=='5')
  {
 document.getElementById("placepopup").innerHTML="<input type='hidden' name='expectedsalery[]' class='form-control'  placeholder='Expected Salary' id='expectedsalery'><input type='hidden'class='form-control'  name='Doj[]'  placeholder='Date Of Joining'   id='Doj'><input type='date'  class='form-control' name='expDoj[]'  placeholder='Expected Date Of Joining'  id='expDoj'  ><input type='hidden' name='count[]' value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  else
  {


   if(res[2]=='6')
  {
    document.getElementById("placepopup").innerHTML="<input type='text' name='expectedsalery[]' class='form-control'  placeholder='Expected Salary'  id='expectedsalery'><input type='date' name='Doj[]'   class='form-control'  placeholder='Date Of Joining' id='Doj' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'    id='expDoj' ><input type='hidden' name='count[]' value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  else
  {
    document.getElementById("placepopup").innerHTML="<input type='hidden' name='expectedsalery[]' class='form-control col-xs-6' placeholder='Expected Salary' id='expectedsalery' ><input type='hidden' name='Doj[]'  placeholder='Date Of Joining'  id='Doj' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'  id='expDoj' ><input type='hidden' name='count[]' value="+res[0]+" id='count'><input type='hidden' name='countval[]' value='1' id='countval'>"
  }
  }
}
 }



function changestatusdata(id)
{
var str = id;
var res = str.split("/");
//alert(str);

//Then read the values from the array where 0 is the first
var myvar = res[0] + ":" + res[1] + ":" + res[2];
  // d = document.getElementById("select_id").value;
  if(res[2]=='4')
  {
 document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='text' name='expectedsalery'  placeholder='Expected Salary' class='form-control' ><input type='date' name='Doj' class='form-control'  placeholder='Date Of Joining' ><input type='hidden' name='expDoj'  placeholder='Expected Date Of Joining'   ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else

{


   if(res[2]=='5')
  {
 document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='hidden' name='expectedsalery'   placeholder='Expected Salary' ><input type='hidden' name='Doj'  placeholder='Date Of Joining'   ><input type='date' class='form-control'  name='expDoj'  placeholder='Expected Date Of Joining'   ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else
  {


   if(res[2]=='6')
  {
    document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='text' name='expectedsalery' placeholder='Expected Salary' class='form-control'  ><input type='date' name='Doj'   class='form-control'   placeholder='Date Of Joining' ><input type='hidden' name='expDoj'  placeholder='Expected Date Of Joining'    ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else
  {
    document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='hidden' name='expectedsalery'  placeholder='Expected Salary'class='form-control'  ><input type='hidden' name='Doj'  placeholder='Date Of Joining'   ><input type='hidden' name='expDoj'  placeholder='Expected Date Of Joining' ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  }
}
 }


 function changestatusdatasuccess(id)
{
var str = id;
var res = str.split("/");
//alert(str);

//Then read the values from the array where 0 is the first
var myvar = res[0] + ":" + res[1] + ":" + res[2];
  // d = document.getElementById("select_id").value;
  if(res[2]=='4')
  {
 document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='text' name='expectedsalery[]'   placeholder='Expected Salary' ><input type='date' name='Doj[]'  class='dateclass placeholderclass'    placeholder='Date Of Joining' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'   ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else

{


   if(res[2]=='5')
  {
 document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='hidden' name='expectedsalery[]'   placeholder='Expected Salary' ><input type='date' name='Doj[]'  class='dateclass placeholderclass' placeholder='Date Of Joining'   ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'   ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else
  {
     if(res[2]=='6')
  {
    document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='text' name='expectedsalery[]'  placeholder='Salary' ><input type='date' name='Doj[]'   class='dateclass placeholderclass'   placeholder='Date Of Joining' ><input type='hidden' name='expDoj[]'  placeholder='Expected Date Of Joining'    ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  else
  {
    document.getElementById("place"+res[0]+res[1]).innerHTML="<input type='hidden' name='expectedsalery[]'   placeholder='Expected Salary' ><input type='hidden' name='Doj[]'  placeholder='Date Of Joining'   ><input type='hidden' name='expDoj[]'   placeholder='Expected Date Of Joining' ><input type='hidden' name='count[]' value="+res[0]+"><input type='hidden' name='countval[]' value='1'>"
  }
  }
}
 }
</script>

<script >
  
$(".one-click<?php echo e($value->position_id); ?>").click(function(){

  //alert("dddddddddddd");
        //$(".hide_css<?php echo e($value->position_id); ?>").slideToggle(700);
    });

</script>
<!-- 
<?php if(isset($_GET['posid']) && $_GET['posid']==$value->position_id) {?>
<script >
 
//  alert("dddddddddddd");
        $(".hide_css<?php echo e($value->position_id); ?>").slideToggle(700);
  

</script>
<?php } else {?>

<script >
  
$(".one-click<?php echo e($value->position_id); ?>").click(function(){

//  alert("dddddddddddd");
        $(".hide_css<?php echo e($value->position_id); ?>").slideToggle(700);
    });

</script><?php } ?> -->
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                               
                            </table>
                             </div>
                             </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>




</div><script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script>

<script type="text/javascript">

    $("input:checkbox:not(:checked)").each(function () {
       // alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
    });

</script>

<div class="container">
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select Status</h4>
        </div>

           <form method="post" action="<?php echo e(URL::to('updateclientstatuspopup')); ?>">
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-9">
                        <div class="form-group">
                          <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Select Status:

                          </label>
                          <select class="form-control col-xs-6" required id="recruiterspockup" name="recruiter[]"  onchange="changestatuspopupdata(this.value)" >
                             <option value="">Select Status</option>                   
                          </select>
                            <input type="text" name="clientreq_posid" id="placepopupval" >
                          <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
                          <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        </div>
                        <div id="placepopup"></div>
                     
                    </div>
                </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <input type="button" class="btn btn-default" value="Submit" id="buttonClassstpop">
            </div>
        </form>



        <div id=""></div>
      
      </div>      
    </div>
  </div> 

<script>
   $(document).ready(function() {
        $("#buttonClassstpop").click(function() {

var sel = document.getElementById('recruiterspockup').value;
var expectedsaleryval = document.getElementById('expectedsalery').value;
var Doj = document.getElementById('Doj').value;
var expDoj = document.getElementById('expDoj').value;
var count = document.getElementById('count').value;
var countval = document.getElementById('countval').value;
var placepopupval = document.getElementById('placepopupval').value;


   $.ajax({
                      type: 'POST',
                      url: '<?php echo e(URL::route('updatespockresume')); ?>',
                      data:{
                        '_token': "<?php echo e(csrf_token()); ?>",
                        'sel':sel,
                        'expectedsalery':expectedsaleryval,
                        'Doj':Doj,
                        'expDoj':expDoj,
                        'count':count,
                          'countval':countval,
                        'placepopupval':placepopupval,
                      },
                      success: function(data){
                      // alert(data);
                       $('#myModal').modal('hide');
                     
                    var data1= JSON.parse(data);


     var trHTML = '';
        var j=1;


                  //  $.each(data, function (i, data) {
                    for (i = 0; i < data1.length; i++) {
                       var str = data1[i]["created_at"];
                       var str = str.split(" ");
                       var aa =str[0].split("-");

                       var con="<?php echo e(config('constants.APP_URLS')); ?>";

                       var url=con+'/storage/app/uploadcv/'+aa[0]+"/"+aa[1]+"/"+aa[2]+"/"+data1[i]["cv_name"];
//alert(data1[i]["cv_status"]);
 data1[i]["s1"]=1;
  data1[i]["s2"]=2;
  data1[i]["s3"]=3;
  data1[i]["s4"]=4;
   data1[i]["s5"]=5;
    data1[i]["s6"]=6;
if(data1[i]["cv_status"]=='3')
{
    data1[i]["clientstattus"]='Rejected';
data1[i]["cv_nameaa"]='<select name= "checkbox_data[]" onchange="changestatusdata(this.value)"  id="foo"><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s1"]+'>Not Seen</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s2"]+'>Send To Client</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s3"]+' selected>Rejected</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s4"]+' >Offered</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s5"]+' >Pipelined</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s6"]+'>Joined</option></select>';
}
if(data1[i]["cv_status"]=='4')
{
     data1[i]["clientstattus"]='Offer';
data1[i]["cv_nameaa"]='<select name= "checkbox_data[]" onchange="changestatusdata(this.value)"  id="foo"><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s1"]+'>Not Seen</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s2"]+'>Send To Client</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s3"]+'>Rejected</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s4"]+' selected>Offered</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s5"]+' >Pipelined</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s6"]+'>Joined</option></select>';
}if(data1[i]["cv_status"]=='5')
{
     data1[i]["clientstattus"]='Pipeline';
data1[i]["cv_nameaa"]='<select name= "checkbox_data[]" onchange="changestatusdata(this.value)"  id="foo"><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s1"]+'>Not Seen</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s2"]+'>Send To Client</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s3"]+'>Rejected</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s4"]+'>Offered</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s5"]+' selected>Pipelined</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s6"]+'>Joined</option></select>';
}
if(data1[i]["cv_status"]=='2')
{
      data1[i]["clientstattus"]='Send To Client';
  data1[i]["s1"]=1;
  data1[i]["s2"]=2;
  data1[i]["s3"]=3;
  data1[i]["s4"]=4;
   data1[i]["s5"]=5;
    data1[i]["s6"]=6;
data1[i]["cv_nameaa"]='<select name= "checkbox_data[]" onchange="changestatusdata(this.value)"  id="foo"><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s1"]+'>Not Seen</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s2"]+' selected>Send To Client</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s3"]+'>Rejected</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s4"]+'>Offered</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s5"]+' >Pipelined</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s6"]+'>Joined</option></select>';
}
if(data1[i]["cv_status"]=='1')
{
        data1[i]["clientstattus"]='Not Seen';
  data1[i]["s1"]=1;
  data1[i]["s2"]=2;
  data1[i]["s3"]=3;
  data1[i]["s4"]=4;
   data1[i]["s5"]=5;
    data1[i]["s6"]=6;
data1[i]["cv_nameaa"]='<select name= "checkbox_data[]" onchange="changestatusdata(this.value)"  id="foo"><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s1"]+'>Not Seen</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s2"]+'>Send To Client</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s3"]+'>Rejected</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s4"]+'>Offered</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s5"]+'>Pipelined</option><option value='+data1[i]["id"]+'/'+data1[i]["recruiter_id"]+'/'+data1[i]["s6"]+'>Joined</option></select>';
}
                       trHTML += "<tr  class='content'>" +
                       "<td>"+j+++"</td>" +
                         "<td  class='title'><input type='checkbox' name='checkboxid' value="+data1[i]["id"]+"></td>" +
                           "<td  class='title'><button type='button'  class='det_css one-click' data-toggle='modal' data-target='#myModal' onclick='getposiionvalue("+data1[i]["position_id"]+"),getupdatespock("+data1[i]["id"]+","+data1[i]["recruiter_id"]+","+data1[i]["cv_status"]+","+data1[i]["position_id"]+")(this.value),getpositionspock("+data1[i]["id"]+","+data1[i]["recruiter_id"]+","+data1[i]["cv_status"]+")' ><span class='icofont icofont-eye-alt'></span></button> </td>" +
                           "<td  class='title'>"+data1[i]["clientstattus"]+"</td>" +

                        "<td  class='title'>"+data1[i]["username"]+"</td>" +
                       "<td  class='title'>"+data1[i]["candidate_name"]+"</td>" +
                       "<td  class='title'>"+data1[i]["cv_uploadtime"]+"</td>" +
                       "<td  class='title'>"+data1[i]["candidate_mob"]+"</td>" +
                       '<td  class="title"><a href='+url+' download>   ' +'Download </a></td>' +
                       "<td  class='title'>"+data1[i]["highest_qulification"]+"</td>" +
                       "<td  class='title'>"+data1[i]["total_exp"]+"</td>" +
                       "<td  class='title'>"+data1[i]["domain_exp"]+"</td>" +
                       "<td  class='title'>"+data1[i]["primary_skill"]+"</td>" +
                       "<td  class='title'>"+data1[i]["secondary_skill"]+"</td>" +
                       "<td  class='title'>"+data1[i]["current_org"]+"</td>" +
                       "<td  class='title'>"+data1[i]["current_ctc"]+"</td>" +
                       "<td  class='title'>"+data1[i]["expected_ctc"]+"</td>" +
                       "<td  class='title'>"+data1[i]["np"]+"</td>" +
                       "<td  class='title'>"+data1[i]["desng"]+"</td>" +
                       "<td  class='title'>"+data1[i]["location"]+"</td>" +
                       "<td  class='title'>"+data1[i]["reason_for_change"]+"</td>" +
                       "<td class='title'>"+data1[i]["relocation"]+"</td>" +
                       "<td class='title'>"+data1[i]["doj"]+"</td>" +
                        
                       "<td class='title'>"+data1[i]["remark"]+"</td>" +
                   /*    "<td>"+data1[i]["cv_nameaa"]+"<div id='place"+data1[i]["id"]+""+data1[i]["recruiter_id"]+"'></div></td>" +*/
                     
                   }
             //      });
                   // console.log(data.length);
             $('#candilist'+placepopupval).html(trHTML);

  //$('#candilist'+placepopupval).html(data); 





                         
                      }
                    });


 

           });
    });

</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>