<?php $__env->startSection('content'); ?>


<!-- Container-fluid starts -->
<!-- Main content starts -->
<div class="container-fluid">
  <div class="row">
    <div class="main-header">
      <h4>Shortlist Profile</h4>
      <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
        <li class="breadcrumb-item"><a href=""><i class="icofont icofont-home"></i></a>
        </li>
        <li class="breadcrumb-item"><a href="">Positions</a>
        </li>
        <li class="breadcrumb-item"><a href="">Shortlisting of CV</a>
        </li>
      </ol>
    </div>

  </div>


 

  <div class="row">

    <div class="col-md-12">

      <!-- <div class="card hide_css"> -->
      <div class="card">
        <div class="card-header">
          <h5 class="card-header-text">All Candidate List</h5>
        </div>
         <?php if(Session::has('success_msg')): ?>
          <div class="alert alert-success">
             <?php echo e(Session::get('success_msg')); ?>

          </div>
        <?php endif; ?>
         <?php echo Form::open(); ?> 
        <div class="card-block">
          <div class="row" >
            <div class="col-sm-12 table-responsive">
              <table class="table"  id="search_filter">
                <thead>
                  <tr>
                    <th>S.N</th>
                    <th>Candidate's Name</th>
                    <th>Mobile Number</th>
                    <th>Email</th>
                    <th>Qulification</th>
                    <th>Total Experience</th>
                    <th>Domain Experience </th>
                    <th>Primary Skill</th>
                    <th>Secondary Skill</th>
                    <th>Current ORG</th>
                    <th>Current CTC </th>
                    <th>Expected CTC</th>
                    <th>NP</th>
                    <th>Reason For Change</th>
                    <th>Skills For Rating</th>
                    <th>Relocation</th>
                    <th>DOJ</th>
                    <th>Remark</th>
                    <th>Cv Upload</th>
                    <!-- <th>Status</th> -->


                  </tr>
                </thead>
                <tbody id="candilist">
                    <?php 
                  $i=1;
                   ?>
                  <?php $__currentLoopData = $pos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <tr >
                    <td><?php echo e($i++); ?></td>
                    <td><?php echo e($value->candidate_name); ?></td>
                    <td><?php echo e($value->candidate_mob); ?></td>
                    <td><?php echo e($value->candidate_email); ?></td>
                    <td><?php echo e($value->highest_qulification); ?></td>
                    <td><?php echo e($value->total_exp); ?></td>
                    <td><?php echo e($value->domain_exp); ?></td>
                    <td><?php echo e($value->primary_skill); ?></td>
                    <td><?php echo e($value->secondary_skill); ?></td>
                    <td><?php echo e($value->current_org); ?></td>
                    <td><?php echo e($value->current_ctc); ?></td>
                    <td><?php echo e($value->expected_ctc); ?></td>
                    <td><?php echo e($value->np); ?></td>
                    <td><?php echo e($value->reason_for_change); ?></td>
                    <td><?php echo e($value->communication_skills_rating); ?></td>
                    <td><?php echo e($value->relocation); ?></td>
                    <td><?php echo e($value->doj); ?></td>
                    <td><?php echo e($value->remark); ?></td>
                    <td>
                       <button type="button" class="btn btn-info btn-sm" onClick="cvupload('<?php echo $value->id;?>');" data-toggle="modal" data-target="#myModal">Upload</button>
                    </td>
                    </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
                <input type="hidden" value="" id="poid" name="poid">
              </table>

            </div>
          </div>

          </div>
          <!-- <div class="md-input-wrapper">
            <input type="submit" class="btn btn-primary waves-effect waves-light" value="Submit">
          </div> -->
      </div>
      <?php echo Form::close(); ?> 
    </div>

  </div>

</div>
<div class="container">
 <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
       <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload CV</h4>
        </div>
        <div class="modal-body">
           <form method="post" action="<?php echo e(URL::to('cvupload')); ?>" enctype="multipart/form-data">
              <input type="hidden" name="recruiter_cv_id" id="recruiter_cv_id" value="">
              <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
              <input type="file" name="cv_name" id="cv_name" required/></br>
              <input type="submit" class="btn btn-primary" name="submit" value="submit"/>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- </div>



</div> -->
<!-- <script>

 function getposition(id){
  var ids  = id;
  $('#poid').val(ids);

}



$(document).ready(function(){
  $("button").click(function(){
    var x = $(this).val();
    alert(1);
    $.ajax({url: "demo_test.txt", success: function(result){
      $("#div1").html(result);
    }});

  })
})
</script -->
<script type="text/javascript">

  $("input:checkbox:not(:checked)").each(function () {
    alert("Id: " + $(this).attr("id") + " Value: " + $(this).val());
  });
  
   function cvupload(id){
    $("#recruiter_cv_id").val(id);
  }

</script>
<script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>