<?php $__env->startSection('content'); ?>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#boot-multiselect-demo').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });
        });
    </script>

    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
              <?php $position = app('App\Component\CommonComponent'); ?>
       <?php  
            $breadcrumb = $position->breadcrumbs();
           
         ?>
                <h4>All Assign Client List</h4>
                
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)"><?php echo e($breadcrumb[0]->parentmenu); ?></a>
                    </li>
                    <li class="breadcrumb-item"><a href=""><?php echo e($breadcrumb[0]->menuname); ?></a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">All Assign Clients</h5>
                        <form action="<?php echo e(URL::to('excellistassignclientdownload')); ?>"  method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <input type="submit" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel" value="Download Excel" name="downexcel">
                                             
                                            </input>
                                        </form>
                    </div>
                    <div class="card-block">
                        <div class="">
                            <div class="col-sm-12 table-responsive">
                             <div id="search_filter_filter" class="dataTables_filter"><label>Search:<input type="text" id="filter_area" style="border: 1px solid #968f8f;" class="" placeholder="" aria-controls="search_filter"></label></div>
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Company Name</th>
                                        <th>Contact Person Name</th>
                                        <th>Email</th>
                                        <th>Mobile No</th>
                                        <th>Contact Person Name1</th>
                                        <th>Email1</th>
                                        <th>Mobile No1</th>
                                        <th>Contact Person Name2</th>
                                        <th>Email2</th>
                                        <th>Mobile No2</th>
                                        <th>Summary</th>
                                        <th>Constractual_sla</th>
                                        <th>Assigned Employee</th> 


                                    </tr>
                                    </thead>
                                    <tbody id="suggesstion-box">
                                    <?php 
                                        $i = 1;
                                     ?>
                                    <?php $__currentLoopData = $assignlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="table-active">
                                            <td><?php echo e($i++); ?></td>
                                            <td><?php echo e($val->comp_name); ?></td>
                                            <td><?php echo e($val->contact_name); ?></td>
                                            <td><?php echo e($val->email); ?></td>
                                            <td><?php echo e($val->phone); ?></td>
                                            <td><?php echo e($val->contact_name1); ?></td>
                                            <td><?php echo e($val->email1); ?></td>
                                            <td><?php echo e($val->phone1); ?></td>
                                            <td><?php echo e($val->contact_name2); ?></td>
                                            <td><?php echo e(!empty($val->email2) ? $val->email2 :'N/a'); ?></td>
                                            <td><?php echo e($val->phone2); ?></td>
                                            <td><?php echo e(!empty(strip_tags($val->summary)) ? strip_tags($val->summary):'na'); ?></td>
                                            <td><?php echo e($val->contractual_sla); ?></td>
                                            <td><?php echo e($val->name); ?></td>

                                            <!--  <td>
                                                 <div class="btn-group btn-group-sm" style="float: none;">
                                                     <button type="button"  class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span></button>
                                                 </div>
                                             </td> -->
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script> 

<script>

$(document).ready(function(){
    $("#filter_area").keyup(function(){
        var xxx = $(this).val();
        //alert(xxx);
          $.ajax({
                      type: 'POST',
                      url: '<?php echo e(URL::route('ajaxassignclient')); ?>',
                      data:{
                        '_token': "<?php echo e(csrf_token()); ?>",
                        'key':xxx,
                      },
                      
                      success: function(data){
                     // alert(data);
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                         
                      }
                    });

    });
});
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>