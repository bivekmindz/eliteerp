<?php $__env->startSection('content'); ?>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#boot-multiselect-demo').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });
        });
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
              <?php $position = app('App\Component\CommonComponent'); ?>
       <?php  
            $breadcrumb = $position->breadcrumbs();
           
         ?>
                <h4>All Assign Client List</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)"><?php echo e($breadcrumb[0]->parentmenu); ?></a>
                    </li>
                    <li class="breadcrumb-item"><a href=""><?php echo e($breadcrumb[0]->menuname); ?></a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-header-text">All Assign Clients</h5>

                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12 table-responsive">
                                <table class="table" id="search_filter">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Company Name</th>
                                        <th>Contact Person Name</th>
                                        <th>Email</th>
                                        <th>Mobile No</th>
                                        <th>Contact Person Name1</th>
                                        <th>Email1</th>
                                        <th>Mobile No1</th>
                                        <th>Contact Person Name2</th>
                                        <th>Email2</th>
                                        <th>Mobile No2</th>
                                        <th>Summary</th>
                                        <th>Constractual_sla</th>
                                        <th>Assigned Employee</th> 


                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        $i = 1;
                                     ?>
                                    <?php $__currentLoopData = $assignlist; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="table-active">
                                            <td><?php echo e($i++); ?></td>
                                            <td><?php echo e($val->comp_name); ?></td>
                                            <td><?php echo e($val->contact_name); ?></td>
                                            <td><?php echo e($val->email); ?></td>
                                            <td><?php echo e($val->phone); ?></td>
                                            <td><?php echo e($val->contact_name1); ?></td>
                                            <td><?php echo e($val->email1); ?></td>
                                            <td><?php echo e($val->phone1); ?></td>
                                            <td><?php echo e($val->contact_name2); ?></td>
                                            <td><?php echo e($val->email2); ?></td>
                                            <td><?php echo e($val->phone2); ?></td>
                                            <td><?php echo e(strip_tags($val->summary)); ?></td>
                                            <td><?php echo e($val->contractual_sla); ?></td>
                                            <td><?php echo e($val->name); ?></td>

                                            <!--  <td>
                                                 <div class="btn-group btn-group-sm" style="float: none;">
                                                     <button type="button"  class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;"><span class="icofont icofont-ui-edit"></span></button>
                                                 </div>
                                             </td> -->
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script> 

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>