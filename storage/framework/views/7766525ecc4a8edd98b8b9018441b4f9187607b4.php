<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               <div class="main-header">
            <h4>Positions</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
               <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
               </li>
               <li class="breadcrumb-item"><a href="javascript:void(0)">Position</a>
               </li>
               <li class="breadcrumb-item"><a href="">Allocate Position to Recuiter</a>
               </li>
            </ol>
         </div>
                <?php if(Session::has('success_msg')): ?>
                    <div class="alert alert-success">
                        <?php echo e(Session::get('success_msg')); ?>

                    </div>
                <?php endif; ?>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                            <table class="table" id="search_filter">
                                <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Position Name</th>
                                    <th>Company Name</th>
                                    <th>Drive</th>
                                    <th>No Of Positions</th>
                                    <th>SPOC</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    $i=1;
                                 ?>

                                <?php if(!empty($positions)): ?>
                                <?php $__currentLoopData = $positions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr class="table-active">
                                        <td><?php echo e($i++); ?></td>
                                        <td><?php echo e(!empty($p->clientjob_title) ? $p->clientjob_title : 'N/A'); ?></td>
                                        <td><?php echo e(!empty($p->comp_name) ? $p->comp_name : 'N/A'); ?></td>
                                        <td>N/A</td>
                                        <td><?php echo e(!empty($p->clientjob_noofposition) ? $p->clientjob_noofposition : 'N/A'); ?></td>
                                        <td><?php echo e(!empty($p->name) ? $p->name : 'N/A'); ?></td>
                                        <td>
                                            <div class="btn-group btn-group-sm" style="float: none;">
                                                <button type="button" onclick="assignposition(<?php echo e($p->clientjob_id); ?>,'<?php echo e($p->clientjob_title); ?>')" href="#success" data-toggle="modal" class="tabledit-edit-button btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;">
                                                    <span class="icofont icofont-ui-edit"></span>
                                                </button>
                                            </div>
                                        </td>    
                                        </tr>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                </tbody>
                </table>
                        </div>
                    </div>
                </div>
                
                
                
            </div>
        </div>
    </div>
    </div>
<?php if(!empty($p)): ?>
<?php $__currentLoopData = $positions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    
<div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h1><i class="glyphicon glyphicon-thumbs-up"></i>Assign RECRUITER</h1>
                </div>
                <div class="modal-body">
                    <?php echo Form::open(array('action' => "Admin\RecruiterController@postRecruiter", 'method' => 'post')); ?>

                    <div class="form-group row">
                        <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Position Name</label>
                        <div class="col-sm-9">
                            <input class="form-control" id="pop_company_name" name="clientjob_title" type="text" value="<?php echo e($p->clientjob_title); ?>" id="example-text-input">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Assign Recruiter</label>
                        <div class="col-sm-9">
                            <div class="form-group">
                                <select id="ajaxhr" multiple="multiple"  name="assignposition[]" class="multiselect-ui form-control " required>
                                    <!-- <?php $__currentLoopData = $employees; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$emp): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($emp->id); ?>"><?php echo e($emp->name); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> -->
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="md-input-wrapper">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-primary waves-effect waves-light" value="submit">
                            <input type="hidden" name="clientjobid" id="clientjobid" value="<?php echo e($p->clientjob_id); ?>">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        </div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>

<script>
   
   function assignposition(id,c_name){
    $.ajax({

           url: "<?php echo e(route('getemployees')); ?>",
           type: "GET",
           data: 'data='+id,
           beforeSend: function() {
           // alert('gurpreet1');
           },        
           success: function(response) {
            //alert(response);
            $('#clientjobid').val(id);
            $('#pop_company_name').val(c_name);
            $('#ajaxhr').html(response);
            $('#ajaxhr').multiselect('rebuild');
           }, 
           error: function() {
              alert("Something Went Wrong");
           }
        });

    }
</script>   

<script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script>   
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>