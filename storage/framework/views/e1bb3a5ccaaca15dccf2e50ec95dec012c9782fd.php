<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Client List</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Clients</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">View All Clients</a>
                    </li>
                </ol>
            </div>
        </div>
    <div class="row">

    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Client Listing</h5>

            </div>
            <div class="card-block">

                <div class="row">

                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                          <div class="form-iu">
                    
                 
                      <form action="" method="get">
                      <div class="col-md-4">

                      <select name="user" class="form-control document" >
                         
                            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           <option value="<?php echo e($c->id); ?>"><?php echo e($c->name); ?></option>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                       </select>
                      
 </div>                       <div class="col-md-4">
    <?php 

    //dd($_GET);
    if(isset($_GET['page']))
    {
     $paged=$_GET['page'];


    }
    else
    {
     $paged=1; 
     $user=0;  
    }
    
        if(isset($_GET['user']))
    {
$user=$_GET['user'];
 }
    else
    {
      $user=0;  
    }
    
    
     ?>
    <input type="hidden" name="page" value="<?php echo e($paged); ?>">
                        <input type="submit" class="sui" name="formsubmit">
                        </div>
                        </form>
                   
                    <div class="col-md-4">
                    
                    <a href="excelclientdownload?user=<?php echo e($user); ?>&page=<?php echo e($paged); ?>">
                                                <button type="button" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel">Download Excel
                                                    <span class="badge"><i class="icofont icofont-file-excel"></i></span>
                                                </button>
                                            </a>
                    </div>
                          
                      
                        </div>  </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                             <div class="pull-right;"></div>
                        </div>
                    </div>

                    <div class="col-sm-12 table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Category</th>
                                <th>Company Name</th>
                                <th>Contact Person Name</th>
                                <th>Designation</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Created by</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                                $i=1;
                                $temp = $clients ;
                             ?>
                            <?php $__currentLoopData = $clients; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="table-active">


                                <td> <?php echo e($i++); ?></td>
                                <td> <?php if($c->client_catid==1): ?>
                                         <?php echo e("IT"); ?>

                                         <?php else: ?>
                                          <?php echo e("NON IT"); ?>

                                         <?php endif; ?>


                                </td>
                                <td><?php echo e($c->comp_name); ?></td>
                                <td><?php echo e($c->contact_name); ?></td>
                                <td><?php echo e($c->designation); ?></td>
                                <td><?php echo e($c->phone); ?></td>
                                <td><?php echo e($c->email); ?></td>
                                <td><?php echo e($c->name); ?></td>
                               </tr>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </tbody>
                        </table>
<div class="row">
                        <div class="col-sm-4 col-md-4">

                           <div><?php echo e($clients->links()); ?></div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                            <div class="pull-right;"></div>
                        </div>
                    </div>                    

                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>