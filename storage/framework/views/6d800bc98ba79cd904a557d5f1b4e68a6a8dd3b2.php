<?php $__env->startSection('content'); ?>
    <section class="login p-fixed d-flex text-center bg-primary common-img-bg">
        <!-- Container-fluid starts -->
        <div class="container-fluid">
            <div class="row">

                <div class="col-sm-12">
                    <div class="login-card card-block">
                        <form class="md-float-material" id="resetpassword" method="POST" action="<?php echo e(route('passwordreset')); ?>">
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                            <div class="text-center">
                                <img src="images/logo.png"> 
                            </div>
                            <h3 class="text-center txt-primary">
                                Reset Password
                            </h3>

                            <div class="form-group">
                                <label for="password" >Password</label>
                                <div class="md-input-wrapper">
                                    <input id="password" type="password" class="md-form-control" name="password" required>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="confirmpassword" >Confirm Password</label>
                                <div class="md-input-wrapper">
                                    <input id="confirmpassword" type="password" class="md-form-control" name="confirmpassword" required>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <input type="submit" class="btn btn-primary" id="btnSubmit" name="Submit" value="Submit">
                                </div>
                            </div>
                         </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<script src="<?php echo e(URL::asset('js/jquery-3.1.1.min.js')); ?>"></script>
<script>
 $(function () {
    $("#btnSubmit").click(function () {
        var password = $("#password").val();
        var confirmPassword = $("#confirmpassword").val();
        
         if (password.length <= 5) {
            alert("Passwords length should be greater than 5.");
            return false;
        }
        
        if (password != confirmPassword) {
            alert("Passwords do not match.");
            return false;
        }
   
   
        return true;
    });
 });
</script>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>