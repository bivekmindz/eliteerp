

<?php $__env->startSection('content'); ?>

        <!-- Container-fluid starts -->
        <!-- Main content starts -->
        <div class="container-fluid">
            <div class="row">
                <div class="main-header">
                    <h4>Advance Search</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                                <li class="breadcrumb-item"><a href="javascript:void(0)"><i class="icofont icofont-home"></i></a>
                                </li>
                                <li class="breadcrumb-item"><a href="/cvparsing">Advance Search</a>
                                </li>
                                <li class="breadcrumb-item"><a href="javascript:void(0)"><?php echo e($_GET['all_keywords']); ?></a>
                                </li>
                               
                               
                            </ol> 
                </div>
                
               
                
            </div>
           
           
           
          <div class="row">
                <div class="col-xl-3 col-lg-4">
                    <div class="card faq-left">
                        
                        <div class="left-menu-dro">
                          <form action="<?php echo e(URL::to('adsearch')); ?>" method="get"> 
                          <div id='cssmenu'>

<ul>
   <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">  
   <li class='has-sub'><a href='#'>Keywords</a>
      <ul>
      <input type="text" class="form-control" placeholder="Search Keywords" name="all_keywords">      
         <li class='has-sub'><a href='#'>Exclude Keywords</a>
            <ul>
               <input type="text" class="form-control" name="exc_keywords" placeholder="Search Keywords">   
            </ul>
         </li>
      </ul>
   </li>
   
   <li class='has-sub'><a href='#'>Company</a>
      <ul>
      <input type="text" class="form-control" placeholder="Search Company" name="company">      
         <li class='has-sub'><a href='#'>Exclude Company</a>
            <ul>
               <input type="text" class="form-control" placeholder="Search Company" name="exc_company">   
            </ul>
         </li>
      </ul>
   </li>
   
   <li class='has-sub'><a href='#'>Designation</a>
      <ul>
      <input type="text" class="form-control" placeholder="Search Designation" name="designation">      
           
      </ul>
   </li>
   
   <li class='has-sub'><a href='#'>Experience ( in year )</a>
      <ul>
         <select class="form-control" name="exp_min">
        <option value="">Min</option>
        <?php for ($y=0; $y < 21; $y++) { 
            echo '<option value="'.$y.'.0">'.$y.' Year</option>'; } ?>
        </select>
      To
       <select class="form-control" name="exp_max">
    <option value="">Max</option>
    <?php for ($m=0; $m < 12; $m++) { 
        echo '<option value="'.$m.'.0">'.$m.' Year</option>'; } ?>
    </select>  
        
      </ul>
   </li>

   <li class='has-sub'><a href='#'>CTC (INR in Lac)</a>
      <ul>
      From 
     <select class="form-control" name="sal_min">
    <option value="">Lac</option>
    <?php for ($l=0; $l < 21; $l++) { 
        echo '<option value="'.$l.'">'.$l.' Lac</option>'; } ?>
    </select>
       <select class="form-control" name="sal_min1">
    <option value="">Thousand</option>
    <?php $t=0;
        for ($t=0; $t < 100; $t++) {
        echo '<option value="'.$t.'">'.$t.' Thousand</option>'; 
    $t=$t+9;} ?>
    </select> 
      To
      <select class="form-control" name="sal_max">
    <option value="">Lac</option>
    <?php for ($l1=0; $l1 < 21; $l1++) { 
        echo '<option value="'.$l1.'">'.$l1.' Lac</option>'; } ?>
    </select>
       <select class="form-control" name="sal_max1">
    <option value="">Thousand</option>
    <?php $t1=0;
        for ($t1=0; $t1 < 100; $t1++) {
        echo '<option value="'.$t1.'">'.$t1.' Thousand</option>'; 
    $t1=$t1+9;} ?>
    </select> 

         
      </ul>
   </li>
   
</ul>

</div>

 <div class="faq-profile-btn">
<button type="submit" class="btn btn-primary waves-effect waves-light space01">Search
</button>

</div>
</form>
                            
                           

                        </div>
                    </div>
                  
                </div>
                
                <div class="col-xl-9 col-lg-8">
              
                   <div class="card">
                            <div class="card-header">
                           
                           <div class="top-menu-ji">
                           
                               <div class="check-box-i">
                               
                               <div class="rkmd-checkbox checkbox-rotate">
                        		<label class="input-checkbox checkbox-primary">
                            		<input type="checkbox" id="checkbox11" class="myCheckbox" onclick="myCheckbox()">
                            			<span class="checkbox"></span>
                        		</label>
                        	
                        </div>
                               </div>
                               
                               <ul>
                               
                                  
                                     <!--      <li> <a href="javascript:void(0)" class="left_space_10">                                
                                     <i class="icofont icofont-copy-alt coiy-en no_icon_space"></i> Add To <i class=" icofont icofont-simple-down"></i></a>
                                       
                                      
                                    <ul class="sub-drop">
                                      <li><a href="">Folder</a></li>
                                    </ul>
                                  
                                  </li> -->
                                   <li><a href="javascript:void(0)"><i class="icofont icofont-envelope coiy-en no_icon_space"></i> Email<i class=" icofont icofont-simple-down"></i></a>
                                     <ul class="sub-drop">
                                      <li><a href="javascript:void(0)" onclick="sendtomail()">Send To</a></li>
                                    </ul>
                                   </li>
                                 <li> <span id="sendmail"></span></li>
                               
                               </ul>
                           
                           </div>
                                
                            </div>
                            
                            <!-- end of modal -->
                            
                        </div>
                        
                         <!-------------------------- Start --------------------------------> 
                        
                           <?php if(empty($getpostion)){ echo 'Data not found!'; } ?>

                        <?php if(!empty($getpostion)): ?>
                        <?php $__currentLoopData = $getpostion; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <div class="card">
                        
                        <div class="to-do-list widget-to-do-list widget_list1" style=" padding-top:15px;">
                    		<div class="rkmd-checkbox checkbox-rotate">
                        		<label class="input-checkbox checkbox-primary">
                            		<input type="checkbox" id="checkbox11" name="myCheckboxInd" class="myCheckboxInd" value="<?php echo e($list->id); ?>">
                            			<span class="checkbox"></span>
                        		</label>
                        	<label><?php echo e($list->candidate_name); ?> </label> (<?php echo e($list->candidate_email); ?>)
                        </div>
                    </div>
                            
                            
                            <!-- end of modal -->
                            <div class="card-block">
                            
                            <div class="col-md-8">
                            
                              <div class="left-link left_link">
                               
                                 <ul>
                                   
                                    <li><i class="icofont icofont-bag"></i><?php echo e($list->total_exp); ?></li>
                                     <li><i class="icon-wallet"></i> <i class="icofont icofont-cur-rupee no_icon_space"></i><?php echo e($list->current_ctc); ?></li>
                                     <!--  <li><i class="icon-location-pin"></i>Chennai</li> -->
                                   
                                 </ul>
                                 
                                 <div class="pro-dcre">
                                    
                                    <ul>
                                      <li><span>Current Organization</span> <?php echo e($list->current_org); ?></li></br>
                                       <li><span>Education </span> <?php echo e($list->highest_qulification); ?></li></br>
                                        <li><span>Key Skills </span> <?php echo e($list->primary_skill); ?></li></br>
                                        <li><span>Notice Period </span> <?php echo e($list->np); ?></li></br>
                                         <!-- <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                          <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li>
                                           <li><span>Current </span> MS SQL Server Database Administrator at Capgemini</li> -->
                                    </ul>
                                    
                                 </div>
                                 
                                 <h5>Similar Resumes</h5>
                              
                              
                              </div>
                            
                            </div>
                            
                             <div class="col-md-4">
                              <div class="right-link">
                              
                             <!--    <div class="top-oiu">
                                
                                <img src="assets/images/avatar-4.png" alt="Logo">
                                
                                </div> -->
                                
                                 <div class="dat-down">
                                  <p><strong><?php echo e($list->primary_skill); ?></strong> with <?php echo e($list->highest_qulification); ?> <!-- currently living in Chennai --></p>

<div class="form-group row">
                                            
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-btn" id="btn-addon1"><button type="submit" class="btn btn-success shadow-none addon-btn waves-effect waves-light"><i class="icofont icofont-ui-call"></i></button></span>
                                                    <input type="text" id="btnaddon1" class="form-control" aria-describedby="btn-addon1" value="<?php echo e($list->candidate_mob); ?>" readonly="">
                                                </div>
                                            </div>
                                            
                                            
                                        </div>
                                        
                                        <ul>
                                              <li><div class="to-do-list widget-to-do-list">
                    <div class="rkmd-checkbox checkbox-rotate">
                        <label class="input-checkbox checkbox-primary">
                            <input type="checkbox" id="checkbox11">
                            <span class="checkbox"></span>
                        </label>
                        <label style="font-size:12px;">Verified Phone</label>
                        </div>
                    </div></li>
                    
                    
                                            </ul>
                                 </div>
                              
                              
                              </div>
                                
                            
                            </div>
                            
                            
                                 
                            </div>
                        </div>

                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                       <!-------------------------- End --------------------------------> 

                    <div></div>
                   
                </div>
            </div>

         
            
          
        </div>
      
    <script>
function myCheckbox()
{ 
  if($('.myCheckbox').prop('checked')) 
  {
    $('.myCheckboxInd').prop('checked', true);
  } 
  else 
  {
    $('.myCheckboxInd').prop('checked', false);
  }
}


function sendtomail()
{
  var selectedLanguage = new Array();
$('input[name="myCheckboxInd"]:checked').each(function() {
selectedLanguage.push(this.value);
});
//alert("Number of selected Languages: "+selectedLanguage.length+"\n"+"And, they are: "+selectedLanguage);
if(selectedLanguage.length > 0)
{ 
  //alert(selectedLanguage);
  $('#sendmail').css('color','green').html('Please wait...');
  $.ajax({
     url: '<?php echo e(URL::route('advancesearchmail')); ?>',
      type: 'POST',
     data: {'mailid':selectedLanguage, '_token': "<?php echo e(csrf_token()); ?>",},
     success:function(data){ //alert(data);return false;
      if(data==1)
      {
        $('#sendmail').css('color','green').html('Successfully send mail!');return false;
      } 
      else
      {
        $('#sendmail').css('color','red').html('Not send mail! please try again!');return false;
      }
    }
  });
}
else
{
  $('#sendmail').css('color','red').html('Please select atleast one profile!');
}


}





    (function($){
$(document).ready(function(){

$('#cssmenu li.active').addClass('open').children('ul').show();
	$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp(200);
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown(200);
			element.siblings('li').children('ul').slideUp(200);
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp(200);
		}
	});

});
})(jQuery);

    </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>