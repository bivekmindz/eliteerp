<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Client List</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Department</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">View All Department</a>
                    </li>
                </ol>
            </div>
        </div>
    <div class="row">

    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <h5 class="card-header-text">Department  Listing</h5>

            </div>
            <div class="card-block">
                <div class="row">

                <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                        <div class="pull-right;"><?php echo e($departments->links()); ?></div>              
                        </div>
                    </div>
                    <div class="col-sm-12 table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Department Name</th>



                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                                $i=1;
                                    $temp = $departments ; 
                             ?>
                            <?php $__currentLoopData = $departments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $departments): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr class="table-active">


                                <td> <?php echo e($i++); ?></td>

                                <td><?php echo e($departments->dept_name); ?></td>

                               </tr>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </tbody>
                        </table>

                       <div class="row">
                        <div class="col-sm-4 col-md-4">
                        </div>
                        <div class="col-sm-4 col-md-4">
                        </div>                    
                        <div class="col-sm-4 col-md-4">
                        <div class="pull-right;"><?php echo e($temp->links()); ?></div>              
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>