<?php $__env->startSection('content'); ?>

<style type="text/css">
  #search_filter_filter{float:left!important;}
</style>


<div class="container-fluid">
    <div class="row">
        <div class="main-header">
            <h4>View CV List</h4>
            <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                </li>
                <li class="breadcrumb-item"><a href="#">Client Call CV List</a>
                </li>
                <li class="breadcrumb-item"><a href="">Allocated CVs List Details</a>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
               <?php if(Session::has('success_msg')): ?>
                <div class="alert alert-success">
                    <?php echo e(Session::get('success_msg')); ?>

                </div>
                <?php endif; ?>
                <div class="card-block">
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                        <div id="search_filter_filter" class="dataTables_filter"><label>Search: <input type="search" id="filter_area" style="border: 1px solid #968f8f;" class="" placeholder="" aria-controls="search_filter" autofocus></label></div>
                            <table class="table" id="">
                                <thead>
                                <tr>
                                    <th>S.No.</th>
                                    <th>CandidateName</th>
                                    <th>CV Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>FollowUp Status</th>
                                   <!--    <th>After Confirm FollowUp Status</th> -->
                                    <!-- <th>Action</th> -->
                                </tr>
                                </thead>
                                <tbody id="suggesstion-box">
                                <?php 
                                $i=1;
                                 ?>
                                <?php $__currentLoopData = $cvdetails; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr class="table-active" >
                                    <td><?php echo e($i++); ?></td>
                                    <td><?php echo e(!empty($p->candidate_name) ? $p->candidate_name : 'N/A'); ?></td>
                                    <td><?php echo e(!empty($p->cv_name) ? $p->cv_name : 'N/A'); ?></td>
                                    <td><?php echo e(!empty($p->candidate_email) ? $p->candidate_email : 'N/A'); ?></td>
                                    <td><?php echo e(!empty($p->candidate_mob) ? $p->candidate_mob : 'N/A'); ?></td>
                                    <td><?php echo e($p->tca_followupstatus); ?></td>
                                      <!-- <td><?php echo e($p->tca_afterfollowupstatus); ?></td> -->
                                    <!-- <td>
                                        <a href="<?php echo e(url('viewallocatedcvdetail', [$p->tca_assignfrom, $p->tca_assignto])); ?>"><button type="button" class="btn btn-info btn-sm">View Details</button></a>  
                                    </td> -->

                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script type="text/javascript">
        $(document).ready(function() {
        $('#search_filter').DataTable();
        } );
</script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>