<title>Employee</title>
<!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<!-- Favicon icon -->


<!-- Google font-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

<!-- iconfont -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('icon/icofont/css/icofont.css')); ?>">

<!-- simple line icon -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('icon/simple-line-icons/css/simple-line-icons.css')); ?>">

<!-- Required Fremwork -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/bootstrap.min.css')); ?>">


<!-- Chartlist chart css -->
<!--<link rel="stylesheet" href="<?php echo e(asset('plugins/charts/chartlist/css/chartlist.css')); ?>" type="text/css" media="all">
-->
<!-- Weather css -->
<!--<link href="<?php echo e(asset('css/svg-weather.css')); ?>" rel="stylesheet">
-->
<!-- Echart js -->
<!--<script src="<?php echo e(asset('plugins/charts/echarts/js/echarts-all.js')); ?>"></script>
-->
<!-- Style.css -->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/main.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/bootstrap-clockpicker.min.css')); ?>">

<!-- Responsive.css-->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/responsive.css')); ?>">

<link rel="stylesheet" href="<?php echo e(asset('css/bootstrap-multiselect.css')); ?>" />
<link rel="stylesheet" href="<?php echo e(asset('css/multi-select.css')); ?>" />
<script src="<?php echo e(URL::asset('js/jquery-3.1.1.min.js')); ?>"></script>

<!--color css-->