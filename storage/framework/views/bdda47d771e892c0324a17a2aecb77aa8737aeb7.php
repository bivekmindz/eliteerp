<?php $__env->startSection('content'); ?>
<script>

$(document).ready(function(){
    $("#filter_area").keyup(function(){
        var xxx = $(this).val();
        document.getElementById("filter_area").focus();
        //alert(xxx);
          $.ajax({
                      type: 'POST',
                      url: '<?php echo e(URL::route('ajaxcandidate-list')); ?>',
                      data:{
                        '_token': "<?php echo e(csrf_token()); ?>",
                        'key':xxx,
                      },
                      success: function(data){
                      //alert(data);
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                      }
                    });

    });
});

</script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#boot-multiselect-demo').multiselect({
                includeSelectAllOption: true,
                buttonWidth: 250,
                enableFiltering: true
            });
        });
    </script>
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>Candidate Details</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#">Positions</a>
                    </li>
                    <li class="breadcrumb-item"><a href="">Candidate list</a>
                    </li>
                </ol>
            </div>
        </div>
        <div class="row">
             <div class="col-md-12">
                <div class="card">
                        <div class="card-header">
                            <h5 class="card-header-text">Candidate Status</h5>
                        </div>
                        <div class="card-block">
                            <div class="row">
                                <div class="col-sm-12 table-responsive">
                                    <table class="table" id="search_filter">
                                        <thead>
                                            <tr>
                                                <th>S No</th>
                                                <th>Candidate Name</th>
                                                <th>Mobile No</th>
                                                <th>CV Download</th>
                                                <th>Position</th>
                                                <th>Company Name</th>
                                                <th>Recruiter Name</th>
                                                <th>Status</th>    
                                            </tr>
                                        </thead>
                                        <tbody id="suggesstion-box">
                                        <?php 
                                            $i = 1;
                                         ?>
                                        <?php $__currentLoopData = $candidate; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr class="table-active">
                                            <td><?php echo e($i++); ?></td>
                                            <td><?php echo e($val->candidate_name); ?></td>
                                            <td><?php echo e($val->candidate_mob); ?></td>
                                            <td>
                                                <?php 
                                                $da = $val->cv_upload_time;
                                                $da1 = substr($da,0,10);
                                                $a1 = explode('-',$da1);
                                                
                                                $candidateCv = 'uploadcv';
                                                $dirStorage = DIRECTORY_SEPARATOR . $candidateCv . DIRECTORY_SEPARATOR . $a1[0] . DIRECTORY_SEPARATOR 
                                                . $a1[1] . DIRECTORY_SEPARATOR . $a1[2];
                                                $con = config('constants.APP_URLS');
                                                 ?>

                                                <a href="<?php echo e($con.'/storage/app'.$dirStorage.'/'.$val->cv_name); ?>" download>
                                                    Download
                                                </a>
                                           </td>
                                            <td><?php echo e($val->clientjob_title); ?></td>
                                            <td><?php echo e($val->comp_name); ?></td>
                                            <td><?php echo e($val->name); ?></td>
                                            <td> 
                                                <select class="form-control form-control-sm" id="exampleSelect3">
                                                    <option>---</option>
                                                    <option>Telephonic</option>
                                                    <option>Skype</option>
                                                    <option>Round 3</option>
                                                    <option>Final Round</option>
                                                    <option>Closure </option>
                                                    <option>Document Share</option>
                                                    <option>On Hold</option>
                                                    <option>Joined</option>
                                                    <option>Screen Reject</option>
                                                    <option>Drop</option>  
                                                </select>
                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                       </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


<script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>