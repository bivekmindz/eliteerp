
<?php $__env->startSection('content'); ?>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<div class="container-fluid">

  <style>
    .autogen{    width: 100%;
      float: left;
      max-height: 137px;
      overflow-y: auto;
      z-index: 999;
      background-color: #fff;}
      .autogen ul {padding: 0px;
        line-height: 35px; border: 1px solid  #e5e5e5;}
        .autogen ul li {padding: 0px 8px; border-bottom: 1px solid #e5e5e5;}
        .autogen ul li:last-child{ border-bottom:none;}
      </style>
      <style>

        button
        {

          background-color:#fff;
          border:none;
          cursor:pointer;
        }

        button:hover
        {

          background-color:#fff;
          border:none;
          cursor:pointer;
          color:green;
        }
        .table td, .table th {
          font-size: 12px;
          text-align: left;
          border: 1px solid #43a060;
          padding-left: 2px;
        }

        .table th{
          text-align: left;
          background: #2a653d;
          color: #fff;
          font-weight: normal;
        }

        .det_css {
          font-size: 12px;
        }
    </style>


      <div class="row">
        <div class="main-header">
          <h4>Recuiter Offer  Report</h4>

        </div>
      </div>
      <div class="row">
       <div class="col-md-12">
         <div class="card">
          <div class="card-header">
            <h5 class="card-header-text">Recuiter Offer Report</h5>
          </div>
          <div class="card-block">
            <div class="card-block">
             <form action=""  method="get" enctype="multipart/form-data"  autocomplete="off">
              <div class="row">
                <?php if(($usersval[0]->emp_role)!='4') { } else { ?>
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">User </label>
                    <div class="col-sm-8">
                      <input type="text" name="user" id="country" class="form-control" placeholder="Enter user Name"  autocomplete="off"  />  
                      <div id="countryList" class="autogen"></div> 
                    </div>
                  </div>
                </div> 

                <?php } ?>
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Start Date </label>
                    <div class="col-sm-8">
                      <input class="form-control document" type="text" value="" id="dojnew" name="doj"/> </div>
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group row">
                      <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">End Date </label>
                      <div class="col-sm-8">
                       <input class="form-control document" type="text" value="" id="doend" name="doend" />
                     </div>
                   </div>
                 </div>

                 <div class="col-lg-6">
                  <div class="form-group row">
                    <label for="example-text-input" class="col-xs-4 col-form-label form-control-label">Sort By  </label>
              
                  </div>
                </div>
              </div>
              <div class="md-input-wrapper">
               <button type="submit" id="submit" class="btn btn-primary waves-effect waves-light">Submit
               </button>  
               <a href="recuiterofferreports"  class="btn btn-primary waves-effect waves-light">Cancel</a>
             </div>
           </form>
           <!-- Script -->
         </div>
       </div>
       <div class="card-block">
        <div class="row" >
          <div class="col-sm-12 table-responsive">
          
          <div class="require-iu">
         
            <table id="example" class="display" >
              <thead style="border: none;color: #fff;background: #43a060;">
                <tr>
                  <th>Position</th>
                  <th>Client</th>
                  <th> Candidate Name</th>
                  <th>Candidate Email</th>
                  <th> Candidate Phone No</th>
                  <th>Total Experience</th>
                  <th>Salary</th>
                  <th>Recruiter</th>
                  <th>Status</th>
                  <th>Offer Date</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php if(!empty($data)): ?>
                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php 
                $subm=0;
                if(count($value['positions'])>0) {
                  ?>
                  <?php foreach($value['resumes'] as $keyus => $k){ ?>

                    <?php if($value['resumes'][$keyus]->cv_status ==4){
                      $val = 'Offered';
                      $style = '';
                    }
                    elseif($value['resumes'][$keyus]->cv_status ==6){
                      $val = 'Joined';
                      $style = 'border: 1px solid green; padding: 6px; color: green; font-weight: bold';
                    }elseif($value['resumes'][$keyus]->cv_status ==6){
                      $val = 'Drop';
                      $style = 'border: 1px solid red; padding: 6px; color: red; font-weight: bold';
                    }?>
                  <tr <?php if($value['state']==0){ echo 'style="color:red;font-weight:bold"';}?>  >
                   <td><?php echo e($value['positions'][$keyus]->clientjob_title); ?> </td>
                   <td><?php echo e($value['clients'][$keyus]->comp_name); ?> </td>
                   <td><?php echo e($value['resumes'][$keyus]->candidate_name); ?> </td>
                   <td><?php echo e($value['resumes'][$keyus]->candidate_email); ?> </td>
                   <td><?php echo e($value['resumes'][$keyus]->candidate_mob); ?> </td>
                   <td><?php echo e($value['resumes'][$keyus]->total_exp); ?> </td>
                   <td><?php echo e($value['resumes'][$keyus]->expectedsalery); ?> </td>
                   <td><?php echo e($value['username']); ?></td>
                   <td><span style="<?php echo $style;?>"> <?php echo e($val); ?> </span></td>
                   <td><?php echo e(date('m-d-Y',strtotime($value['resumes'][$keyus]->dateoj))); ?> </td>
                   <td>
                     <button type='button'  class='det_css' data-toggle='modal' data-target='#myModal' onclick='getposiionvalue(<?php echo $value['resumes'][$keyus]->id; ?>,<?php echo $value['resumes'][$keyus]->cv_status; ?>)' >Update</button>     
                     <button type='button'  class='det_css' data-toggle='modal' data-target='#myModalview' onclick='getresumelistreport(<?php echo $value['resumes'][$keyus]->id; ?>)' >Follow up</button>     
                   </td>
                 </tr>
                 <?php }  } ?>
                 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                 <?php endif; ?>
               </tbody>
             </table>
         
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<div class="container">
 <div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Select Status</h4>
      </div>
      <form method="post" action="<?php echo e(URL::to('updatecrecuiterdata')); ?>">
        <div class="modal-body">
          <div class="form-group row">
            <div class="col-sm-9">
              <div class="form-group">
                <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">
                  Add Status
                </label>
                <div id="status"></div>
               <input type="date" name="doj" class="form-control" required=""> 
               <input type="hidden" name="clientreq_posid" id="placepopupval" >
               <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
               <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
             </div>
           </div>
         </div>
       </div>
       <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input type="submit" class="btn btn-default" value="Submit" >
      </div>
    </form>
    <div id=""></div>
  </div>      
</div>
</div> 
</div>
<div class="container">
 <div class="modal fade" id="myModalview" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Display All Updates</h4>
      </div>
      <form method="post" action="<?php echo e(URL::to('updaterecfollowup')); ?>">
       <div class="modal-body" >
         <table width="90%" border="1" cellspacing="0" cellpadding="0">
           <thead>
           </thead>
           <tbody id="recruiter">
           </tbody>
         </table>
         <input type="hidden" name="clientreq_id" id="clientreq_id" value="">
         <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         <input type="submit" class="btn btn-default" value="Submit" >
       </div>   
     </form>
   </div>      
 </div>
</div> 
</div>
<script>

  function getresumelistreport(fk_id)
  {
    var useridd = fk_id;

    $.ajax({
      type:"GET",
      url:"getresumesrecuiterofferreport/" + useridd ,
      data:'',
      success: function(data){
          //  alert(data);
          $("#recruiter").empty();
          $("#recruiter").append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
      });
  }  

  function getposiionvalue(id,sid)
  {

   var ids = id;
   var sid = sid;
   // alert(sid);
   
   // document.getElementById("statusid").value = sid;  

   $.ajax({
      type:"POST",
      url:"cvstatus",
      data:{ 'id':id ,
            'sid':sid,
            '_token': "<?php echo e(csrf_token()); ?>"},
      success: function(data){
           alert(data);
           $("#status").empty();
           $("#status").append(data);
          //  $('#recruiter').multiselect('rebuild');
        },
        error: function(data){

        }
      }); 
   document.getElementById("placepopupval").value = id;

 }

 function upload_position_jd(clientjob_id) {
  $.ajax({
    url: 'adminpositionjd',
    data: { id:clientjob_id },
    success: function(result){
      $("#positionJd").html(result);
    }
  });
  $('#myModal').modal('show');
}
</script>
<script>
</script>

<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
rel = "stylesheet">

<script type="text/javascript">
  $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
      });
    </script>
    <script>  
     $(document).ready(function(){  
      $('#clients').keyup(function(){  
       var query = $(this).val();  
          // alert("hiii");
          if(query != '')  
          {  
           $.ajax({
            type: 'POST',
            url: '<?php echo e(URL::route('searchclients')); ?>',
            data:{
              '_token': "<?php echo e(csrf_token()); ?>",
              'query':query,
            },
            success: function(data){
                      // alert(data);
                      $('#ClientList').fadeIn();  
                      $('#ClientList').html(data);  
                    }
                  });


              /*  $.ajax({  
                     method:"POST",  
                     url:"searchusers",  
                    
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                     }  
                   });  */
                 }  
               });  
      $(document).on('click', 'ul.list-unstyledclients li', function(){  
        $('#clients').val($(this).text());  
        $('#ClientList').fadeOut();  
      });  
    });  
  </script>  


  <script>  
   $(document).ready(function(){  
    $('#country').keyup(function(){

   //   alert("biii");
   var query = $(this).val();  
   if(query != '')  
   {  
     $.ajax({
      type: 'POST',
      url: '<?php echo e(URL::route('searchusers')); ?>',
      data:{
        '_token': "<?php echo e(csrf_token()); ?>",
        'query':query,
      },
      success: function(data){
                      // alert(data);
                      $('#countryList').fadeIn();  
                      $('#countryList').html(data);  
                    }
                  });


              /*  $.ajax({  
                     method:"POST",  
                     url:"searchusers",  
                    
                     data:{query:query},  
                     success:function(data)  
                     {  
                          $('#countryList').fadeIn();  
                          $('#countryList').html(data);  
                     }  
                   });  */
                 }  
               });  
    $(document).on('click', 'ul.list-unstyled li', function(){  
     //   alert('hiii');
    // alert($(this).text());
    $('#country').val($(this).text());  
    $('#countryList').fadeOut();  
  });  
  });  
</script>  
<link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css"
rel = "stylesheet">

<script type="text/javascript">
  $("body").on("click", "#submit", function () {
/*        var allowedFiles = [".xls", ".xlsx", ".csv"];
        var fileUpload = $("#file");
        var lblError = $("#lblError");
        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
        if (!regex.test(fileUpload.val().toLowerCase())) {
            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
            return false;
        }
        lblError.html('');
        return true;*/
      });
    </script>


    <script>
     $(document).ready(function () {  
      $('#doend').datepicker({
        dateFormat: "yy-mm-dd"
      });  
      
    });
  </script>
  <script>
   $(document).ready(function () {  
    $('#dojnew').datepicker({
      dateFormat: "yy-mm-dd"
    });  

  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable({
      "pageLength": 200
    });
  } );
</script>
<script type="text/javascript">

</script>

<?php $__env->stopSection(); ?>




<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>