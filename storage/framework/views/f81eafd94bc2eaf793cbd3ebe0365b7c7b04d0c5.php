

<?php $__env->startSection('content'); ?>
<script>
$(document).ready(function(){
 document.getElementById("filter_area").focus();
});
$(document).ready(function(){
    $("#filter_area").keyup(function(){
        var xxx = $(this).val();
        document.getElementById("filter_area").focus();
       // alert(xxx);
          $.ajax({
                      type: 'POST',
                      url: '<?php echo e(URL::route('ajaxclientspoc')); ?>',
                      data:{
                        '_token': "<?php echo e(csrf_token()); ?>",
                        'key':xxx,
                      },
                      success: function(data){
                       //alert(data);
                        $("#suggesstion-box").show();
                        $("#suggesstion-box").html(data);
                      }
                    });

    });
});

</script>

    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
              <?php $position = app('App\Component\CommonComponent'); ?>
    <?php  
            $breadcrumb = $position->breadcrumbs();
            //print_r($breadcrumb); exit;
            //print_r($breadcrumb[0]->menuname); exit;
         ?>
                <h4>Client Assigned to SPOC</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li class="breadcrumb-item"><a href="index.html"><i class="icofont icofont-home"></i></a>
                    </li>
                    <li class="breadcrumb-item"><a href="#"><?php echo e($breadcrumb[0]->parentmenu); ?></a>
                    </li>
                    <li class="breadcrumb-item"><a href=""><?php echo e($breadcrumb[0]->menuname); ?></a>
                    </li>
                </ol>
            </div>
        </div>



        <div class="row">

            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                    
                        <h5 class="card-header-text">Client With Assigned spoc</h5>
                        <form action="<?php echo e(URL::to('excellistclientspocdownload')); ?>"  method="post" enctype="multipart/form-data">
                                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <input type="submit" class="btn btn-success waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download Excel" value="Download Excel" name="downexcel">
                                             
                                            </input>
                                        </form>

                    </div>
                    <div class="card-block">
                        <div class="row">
                            <div class="col-sm-12">
                            
                            <div class="tab-spok">

                                         <div id="search_filter_filter" style="float:left" class="dataTables_filter"><label>Search:<input type="search" id="filter_area" style="border: 1px solid #968f8f;" class="" placeholder="" aria-controls="search_filter"></label></div>

                                <table class="table" id="">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <!-- <th>Category</th> -->
                                        <th>Company Name</th>
                                        <!-- <th>Contact Person Name</th> -->

                                        <th>Assigned SPOC</th>
                                        <th>Edit SPOC</th>

                                    </tr>
                                    </thead>
                                    <tbody id="suggesstion-box">

                                    <?php 
                                        $i=1;
                                     ?>
                                    <?php $__currentLoopData = $clientemp; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $kk => $clients): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr >
                                            <td><?php echo e($i++); ?></td>
                                            <td><?php echo e($clients->comp_name); ?></td>
                                            <td><?php echo e($clients->name); ?></td>
                                            <td>
                                                <div class="btn-group btn-group-sm" style="float: none;">
    <button onclick="return test('<?php echo e($clients->fk_clientid); ?>','<?php echo e($clients->comp_name); ?>'),getspocclient('<?php echo $clients->fk_clientid ?>');" type="button" href="#success" data-toggle="modal" class="tabledit-edit-button sl_refresh btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;"><span  class=""></span>Add</button></div>
    

    <div class="btn-group btn-group-sm">
    <button onclick="return testrmv('<?php echo e($clients->fk_clientid); ?>','<?php echo e($clients->comp_name); ?>'),getassignspocclient('<?php echo $clients->fk_clientid ?>');" type="button" href="#unassign" data-toggle="modal" class="tabledit-edit-button sl_refresh btn btn-primary waves-effect waves-light"  style="float: none;margin: 5px;"><span  class=""></span>Remove</button>
    </div>
    </td>
                                        </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </tbody>
                                </table>
                                </div>
                            </div>

  
                      </div>
                    </div>
                </div>
            </div>

<script type="text/javascript">
$(document).ready(function() {
$('#search_filter').DataTable();
} );
</script> 


        </div> 
        <?php foreach($clientemp as $a=> $c){//echo "<pre>"; print_r($c->clientrole_id);}?>


         <?php if(!empty($c)): ?>
       

    <div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header modal-header-success">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h1><i class="glyphicon glyphicon-thumbs-up"></i>Assign SPOC</h1>
    </div>
    <div class="modal-body">
    <form action="<?php echo e(route('updateassignspoc')); ?> " method="POST"  name="assign" >
    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
    <div class="form-group row">
    <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Company Name</label>
    <div class="col-sm-9">
    <input class="form-control" type="text"  id="pop_company_name" name="cname" id="cname" value=" " >
    <input type="hidden"  id="client_id" name="client_id"  value="" >
    </div>
    </div>


 <div class="form-group row">
    <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Employee List</label>
      <div class="col-sm-9">
       <div class="form-group">
         <select id="empid"  multiple="multiple"  name="empid[]" class="multiselect-ui form-control " required>
 </select>
    <span class="text-danger"><?php echo e($errors->first('empid')); ?></span>



    </div>

    </div>

    </div>

    </div>

    <div class="modal-footer">

    <div class="md-input-wrapper">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <input type="submit" class="btn btn-primary waves-effect waves-light" value="submit" onclick="checkdata('<?php echo e($c->fk_clientid); ?>')">
    </input>

    </div>
    </div>

    </form>
    </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <?php endif; ?>
<?php }?>




 <?php foreach($clientemp as $a=> $c){//echo "<pre>"; print_r($c->clientrole_id);}?>


         <?php if(!empty($c)): ?>
       

    <div class="modal fade" id="unassign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header modal-header-success">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h1><i class="glyphicon glyphicon-thumbs-up"></i>UnAssign SPOC</h1>
    </div>
    <div class="modal-body">
    <form action="<?php echo e(route('updateunassignspoc')); ?> " method="post"  name="unassign" >
    <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
    <div class="form-group row">
    <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Company Name</label>
    <div class="col-sm-9">
    <input class="form-control" type="text"  id="unassign_pop_company_name" name="cname" id="cname" value=" " >
    <input type="hidden"  id="unassign_client_id" name="client_id"  value="" >
    </div>
    </div>


 <div class="form-group row">
    <label for="example-text-input" class="col-xs-3 col-form-label form-control-label">Employee List</label>
      <div class="col-sm-9">
       <div class="form-group">
         <select id="unassignempid"  multiple="multiple"  name="empid[]" class="multiselect-ui form-control " required>
 </select>
    <span class="text-danger"><?php echo e($errors->first('empid')); ?></span>



    </div>

    </div>

    </div>

    </div>

    <div class="modal-footer">

    <div class="md-input-wrapper">
    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
    <input type="submit" class="btn btn-primary waves-effect waves-light" value="submit" onclick="checkdatarmv('<?php echo e($c->fk_empid); ?>','<?php echo e($clients->fk_clientid); ?>')">
    </input>

    </div>
    </div>

    </form>
    </div><!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <?php endif; ?>
<?php }?>
 



        <link href = "https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel = "stylesheet">
        <script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.js"></script>


<script type="text/javascript">
    

            function test(id,c_name){
                // alert("c_name");
                $.ajax({
                    url: "<?php echo e(route('clientspoc')); ?>",
                    type: "GET",
                    data: 'data='+id,
                    beforeSend: function() {
                        //alert('vandana1');

                    },
                    success: function(response) {

                        $('#pop_company_name').val(c_name);
                        $('#client_id').val(id);
                        ('.fetched-data').html(response);
                    },
                    error: function(xhr) {
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    }
                });



            }

            function testrmv(id,c_name){
                // alert("c_name");
                $.ajax({
                    url: "<?php echo e(route('clientspoc')); ?>",
                    type: "GET",
                    data: 'data='+id,
                    beforeSend: function() {
                        //alert('vandana1');

                    },
                    success: function(response) {

                        $('#unassign_pop_company_name').val(c_name);
                        $('#unassign_client_id').val(id);
                        ('.fetched-data').html(response);
                    },
                    error: function(xhr) {
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    }
                });



            }

            function getspocclient(id)
            { 

                $.ajax({
                    url: 'getspoctoclient/'+id,
                    type:'GET',
                    error: function(xhr) {
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    },
                    success: function(data)
                    {
                        // alert(data);
//                    debugger;
                        $('#empid').html(data);

                        $('#empid').multiselect('rebuild');

                        //alert(data);
                    }
                });
            }

             function getassignspocclient(id)
            { 

                $.ajax({
                    url: 'getassignspoctoclient/'+id,
                    type:'GET',
                    error: function(xhr) {
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    },
                    success: function(data)
                    {
                        // alert(data);
//                    debugger;
                        $('#unassignempid').html(data);

                        $('#unassignempid').multiselect('rebuild');

                        //alert(data);
                    }
                });
            }


</script>




        <script>


            




            $(function() {
                $("form[name='assign']").validate({
                    rules: {
                        empid: {
                            required: true,
                        },
                    },
                    messages: {
                        empid: "Please Select Spoc",
                    },
                    submitHandler: function(form) {
                        form.submit();
                    }
                })
            });





            

function getemployee(cid){
                var cid = cid;
                $.ajax({
                    url: 'getemployeebyclientid/'+cid,
                    type:'GET',
                    error: function(xhr) {
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    },
                    success: function(data)
                    {

                        $('#empid').html(data);
                        $('#empid').multiselect('rebuild');

                    }
                });
            }

function checkdata(clientid)
            {

                var eid=$('#empid').val();


                if(eid!='')
                {
                    $.ajax({
                        url: 'updateassignspoc',
                        type: 'POST',
                        data:  $('assign').serialize(),
                        success: function(data)
                        {



                        }
                    });

                }
                else
                {


                }


            }

            function checkdatarmv(clientid,empid)
            {

                var eid=$('#empid').val(empid);
                var cid=$('#unassign_client_id').val(clientid);

                if(eid!='')
                {
                    $.ajax({
                        url: 'updateunassignspoc',
                        type: 'POST',
                        data:  $('unassign').serialize(),
                        success: function(data)
                        {



                        }
                    });

                }
                else
                {


                }


            }

            
            

        <?php $__env->stopSection(); ?>
<?php echo $__env->make('Emp.layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>